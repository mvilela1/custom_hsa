create or replace PACKAGE BODY HON_CONSOLIDA_REL
----------------------------------------------------------------------------------------------------
-- PACKAGE BODY: HON_CONSOLIDA_REL
--
-- Objetivo: Esse package tem por finalidade a verificação de inconsistências na consolidação de dados
--           das 4 unidades de negócio
--
-- Histórico:
--   24/04/2018 - MVS - Implementação
--
----------------------------------------------------------------------------------------------------
IS
----------------------------------------------------------------------------------------------------
FUNCTION calcula_tempo (
  p_tempo IN OUT NUMBER
) RETURN NUMBER
IS
  v_ret NUMBER;
BEGIN
  v_ret   := ROUND(((dbms_utility.get_time+power(2,32)) - p_tempo)/100);
  p_tempo := (dbms_utility.get_time+power(2,32));
  RETURN v_ret;
END;
----------------------------------------------------------------------------------------------------
FUNCTION checa_colunas (
  p_tabela    IN VARCHAR2
) RETURN VARCHAR2
IS
  c        SYS_REFCURSOR;

  v_sql    VARCHAR2(32000);
  v_coluna VARCHAR2(50);
  v_ret    VARCHAR2(32000);
BEGIN
  v_ret := NULL;
  v_sql := NULL;
  v_sql := v_sql || 'SELECT column_name ';
  v_sql := v_sql || '  FROM user_tab_columns';
  v_sql := v_sql || ' WHERE table_name  = :a ';
  v_sql := v_sql || '   AND column_name not like ''HON_CONSOLIDA%''';
  v_sql := v_sql || ' MINUS ';

  --
  g_dblink := cor_le_inicializacao(null, 'HON_DBLINK_MAO', null);
  v_sql := v_sql || ' SELECT column_name ';
  v_sql := v_sql || '  FROM user_tab_columns'||'@'||g_dblink;
  v_sql := v_sql || ' WHERE table_name = :b ';
  v_sql := v_sql || '   AND column_name not like ''HON_CONSOLIDA%''';
  v_sql := v_sql || ' MINUS ';

  g_dblink := cor_le_inicializacao(null, 'HON_DBLINK_HAB', null);
  v_sql := v_sql || ' SELECT column_name ';
  v_sql := v_sql || '  FROM user_tab_columns'||'@'||g_dblink;
  v_sql := v_sql || ' WHERE table_name = :c ';
  v_sql := v_sql || '   AND column_name not like ''HON_CONSOLIDA%''';
  v_sql := v_sql || ' MINUS ';

  g_dblink := cor_le_inicializacao(null, 'HON_DBLINK_PECAS', null);
  v_sql := v_sql || ' SELECT column_name ';
  v_sql := v_sql || '  FROM user_tab_columns'||'@'||g_dblink;
  v_sql := v_sql || ' WHERE table_name = :d ';
  v_sql := v_sql || '   AND column_name not like ''HON_CONSOLIDA%''';


  OPEN c FOR v_sql USING p_tabela, p_tabela, p_tabela, p_tabela;
  LOOP

    FETCH c INTO v_coluna;
    EXIT WHEN c%NOTFOUND;

    IF v_ret IS NOT NULL THEN
      v_ret := v_ret || ', ';
    ELSE
      v_ret := ' \n '||p_tabela||': ';
    END IF;
    v_ret := v_ret || v_coluna;
  END LOOP;
  IF c%ISOPEN THEN CLOSE c; END IF;

  RETURN v_ret;
EXCEPTION
  WHEN OTHERS THEN
    IF c%ISOPEN THEN CLOSE c; END IF;
    RAISE;
END;
----------------------------------------------------------------------------------------------------
FUNCTION get_colunas (
  p_tabela    IN VARCHAR2
) RETURN VARCHAR2
IS
  v_ret VARCHAR2(32000);
  v_sql VARCHAR2(4000);

  c     SYS_REFCURSOR;

  v_coluna_org  VARCHAR2(50);
BEGIN
  v_ret := NULL;
  v_sql := NULL;
  v_sql := v_sql || 'SELECT column_name ';
  v_sql := v_sql || '  FROM user_tab_columns';
  v_sql := v_sql || ' WHERE table_name  = :a ';
  v_sql := v_sql || ' ORDER BY 1 ';

  OPEN c FOR v_sql USING p_tabela;
  LOOP
    FETCH c INTO v_coluna_org;
    EXIT WHEN c%NOTFOUND;

    IF v_ret IS NOT NULL THEN
      v_ret := v_ret || ', ';
    END IF;
    v_ret := v_ret || 't.'|| v_coluna_org || ' ' || v_coluna_org;
  END LOOP;
  IF c%ISOPEN THEN CLOSE c; END IF;

  RETURN v_ret;
EXCEPTION
  WHEN OTHERS THEN
    IF c%ISOPEN THEN CLOSE c; END IF;
    RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE drop_tabela (
  p_tabela IN VARCHAR2
)
IS
BEGIN
  BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE '||p_tabela;
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE cria_tabela (
  p_tabela_temp     IN VARCHAR2,
  p_tabela          IN VARCHAR2,
  p_select          IN VARCHAR2
)
IS
  v_sql  VARCHAR2(32000);
BEGIN
  v_sql := 'create global temporary table '||p_tabela_temp||' on commit preserve rows '||
           '  AS SELECT '||p_select|| ' FROM '||p_tabela||' t WHERE 1=2';
  BEGIN
    EXECUTE IMMEDIATE v_sql;
  EXCEPTION WHEN OTHERS THEN
    RAISE;
  END;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_mao;
         commit;
	     update cor_pessoa@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_mao; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_pecas;
         commit;
	     update cor_pessoa@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_pecas; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_hab;
         commit;
	     update cor_pessoa@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_hab; 
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_vig
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_pessoa_vigencia@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_pessoa_vigencia@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_pessoa_vigencia@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_loc
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_mao;
         commit;
	     update cor_localidade_pessoa@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_mao; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_pecas;
         commit;
	     update cor_localidade_pessoa@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_pecas; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_hab;
         commit;
	     update cor_localidade_pessoa@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_hab; 
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_locvig
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_localidade_vigencia@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_localidade_vigencia@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_localidade_vigencia@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_merc
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_MERC_IDF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
        MERC_CODIGO
 FROM HON_CONSOLIDA_MERC_IDF_V; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_mercadoria@lk_mao
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_mercadoria@lk_pecas
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_mercadoria@lk_hab
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_filtro
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V
 -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_mao;
         commit;
	     update cor_pessoa@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_mao; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_pecas;
         commit;
	     update cor_pessoa@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_pecas; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
         cor_pessoa_cons_pkg.disable@lk_hab;
         commit;
	     update cor_pessoa@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_pessoa_cons_pkg.enable@lk_hab; 
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_vig_filtro
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V
 -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_pessoa_vigencia@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_pessoa_vigencia@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_pessoa_vigencia@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_loc_filtro
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_mao;
         commit;
	     update cor_localidade_pessoa@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_mao; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_pecas;
         commit;
	     update cor_localidade_pessoa@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_pecas; 
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
         cor_localidade_pessoa_cons_pkg.disable@lk_hab;
         commit;
	     update cor_localidade_pessoa@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo;
         cor_localidade_pessoa_cons_pkg.enable@lk_hab; 
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_pfj_locvig_filtro
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_PFJ_DOF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
PFJ_CODIGO
 FROM HON_CONSOLIDA_PFJ_DOF_V
 -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_localidade_vigencia@lk_mao
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_localidade_vigencia@lk_pecas
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_localidade_vigencia@lk_hab
		    set hon_consolida_status = '0'
		  where pfj_codigo = v_tab(i).pfj_codigo
		    and dt_fim is null;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE atualiza_merc_filtro
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF HON_CONSOLIDA_MERC_IDF_V%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT HON_CONSOLIDA_ORIGEM,
        MERC_CODIGO
 FROM HON_CONSOLIDA_MERC_IDF_V
 -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
	  -- VERIFICA ORIGEM PARA ATUALIZAR REGISTRO
	  IF v_tab(i).hon_consolida_origem = 'MAO'
	  THEN
	     update cor_mercadoria@lk_mao
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'PECAS'
	  THEN
	     update cor_mercadoria@lk_pecas
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
	  IF v_tab(i).hon_consolida_origem = 'HAB'
	  THEN
	     update cor_mercadoria@lk_hab
		    set hon_consolida_status = '0'
		  where merc_codigo = v_tab(i).merc_codigo;
         commit;
	  END IF;
    end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_pfj (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF hon_gttemp_pessoa%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT ALTERADO_EM,
ALTERADO_POR,
CHAVE_ORIGEM,
CHAVE_ORIGEM_PAI,
CODIGO_USUAL,
COD_NATUREZA_JURIDICA,
HON_CLIENTE_JDE,
HON_CONSOLIDA_ORIGEM,
HON_FORNECEDOR_JDE,
HON_IDENTIFICA_SECRETARIA,
IND_ARREDONDA_VLRS,
IND_CCOR,
IND_CFPFJ,
IND_CLIENTE,
IND_COMPHIER_FILHO,
IND_COMPHIER_PAI,
IND_CONTABILISTA,
IND_ESTABELECIMENTO,
IND_FISICA_JURIDICA,
IND_FORNECEDOR,
IND_NACIONAL_ESTRANGEIRA,
IND_NAT_PESSOA_EFD,
IND_ORGAO_GOVERNAMENTAL,
IND_PFJMERC,
IND_PONTO_ALFANDEGADO,
IND_PRODUTOR,
IND_PROF,
IND_RGPFJUF,
IND_TRANSPORTADOR,
LOC_CODIGO_COBRANCA,
LOC_CODIGO_TRANSP,
MNEMONICO,
ORG_ID,
ORIGEM,
PFJ_CHAVE_ORIGEM_PAI,
PFJ_CODIGO,
PFJ_CODIGO_COBRANCA,
PFJ_CODIGO_ENTREGA,
PFJ_CODIGO_RETIRADA,
PFJ_CODIGO_TRANSP,
REVISAO
 FROM hon_consolida_pfj_v; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_PESSOA (ALTERADO_EM,
ALTERADO_POR,
CHAVE_ORIGEM,
CHAVE_ORIGEM_PAI,
CODIGO_USUAL,
COD_NATUREZA_JURIDICA,
HON_CLIENTE_JDE,
HON_CONSOLIDA_ORIGEM,
HON_FORNECEDOR_JDE,
HON_IDENTIFICA_SECRETARIA,
IND_ARREDONDA_VLRS,
IND_CCOR,
IND_CFPFJ,
IND_CLIENTE,
IND_COMPHIER_FILHO,
IND_COMPHIER_PAI,
IND_CONTABILISTA,
IND_ESTABELECIMENTO,
IND_FISICA_JURIDICA,
IND_FORNECEDOR,
IND_NACIONAL_ESTRANGEIRA,
IND_NAT_PESSOA_EFD,
IND_ORGAO_GOVERNAMENTAL,
IND_PFJMERC,
IND_PONTO_ALFANDEGADO,
IND_PRODUTOR,
IND_PROF,
IND_RGPFJUF,
IND_TRANSPORTADOR,
LOC_CODIGO_COBRANCA,
LOC_CODIGO_TRANSP,
MNEMONICO,
ORG_ID,
ORIGEM,
PFJ_CHAVE_ORIGEM_PAI,
PFJ_CODIGO,
PFJ_CODIGO_COBRANCA,
PFJ_CODIGO_ENTREGA,
PFJ_CODIGO_RETIRADA,
PFJ_CODIGO_TRANSP,
REVISAO)
     VALUES ( v_tab(i).ALTERADO_EM, v_tab(i).ALTERADO_POR, v_tab(i).CHAVE_ORIGEM, v_tab(i).CHAVE_ORIGEM_PAI, v_tab(i).CODIGO_USUAL,
              v_tab(i).COD_NATUREZA_JURIDICA, v_tab(i).HON_CLIENTE_JDE, v_tab(i).HON_CONSOLIDA_ORIGEM, v_tab(i).HON_FORNECEDOR_JDE,
              v_tab(i).HON_IDENTIFICA_SECRETARIA, v_tab(i).IND_ARREDONDA_VLRS, v_tab(i).IND_CCOR, v_tab(i).IND_CFPFJ, v_tab(i).IND_CLIENTE,
              v_tab(i).IND_COMPHIER_FILHO, v_tab(i).IND_COMPHIER_PAI, v_tab(i).IND_CONTABILISTA, v_tab(i).IND_ESTABELECIMENTO,
              v_tab(i).IND_FISICA_JURIDICA, v_tab(i).IND_FORNECEDOR, v_tab(i).IND_NACIONAL_ESTRANGEIRA, v_tab(i).IND_NAT_PESSOA_EFD,
              v_tab(i).IND_ORGAO_GOVERNAMENTAL, v_tab(i).IND_PFJMERC, v_tab(i).IND_PONTO_ALFANDEGADO, v_tab(i).IND_PRODUTOR, v_tab(i).IND_PROF,
              v_tab(i).IND_RGPFJUF, v_tab(i).IND_TRANSPORTADOR, v_tab(i).LOC_CODIGO_COBRANCA, v_tab(i).LOC_CODIGO_TRANSP, v_tab(i).MNEMONICO,
              v_tab(i).ORG_ID, v_tab(i).ORIGEM, v_tab(i).PFJ_CHAVE_ORIGEM_PAI, v_tab(i).PFJ_CODIGO, v_tab(i).PFJ_CODIGO_COBRANCA,
              v_tab(i).PFJ_CODIGO_ENTREGA, v_tab(i).PFJ_CODIGO_RETIRADA, v_tab(i).PFJ_CODIGO_TRANSP, v_tab(i).REVISAO);
  exception when DUP_VAL_ON_INDEX 
            then 
			   begin
			       UPDATE  COR_PESSOA 
				      SET MNEMONICO = v_tab(i).MNEMONICO
                         ,IND_FISICA_JURIDICA = v_tab(i).IND_FISICA_JURIDICA
                         ,IND_NACIONAL_ESTRANGEIRA = v_tab(i).IND_NACIONAL_ESTRANGEIRA
                         ,IND_CLIENTE = v_tab(i).IND_CLIENTE
                         ,IND_ESTABELECIMENTO = v_tab(i).IND_ESTABELECIMENTO
                         ,IND_FORNECEDOR = v_tab(i).IND_FORNECEDOR
                         ,IND_PRODUTOR = v_tab(i).IND_PRODUTOR
                         ,CODIGO_USUAL = v_tab(i).CODIGO_USUAL
                         ,ORIGEM = v_tab(i).ORIGEM
                         ,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
						 --
						 ,ALTERADO_EM = v_tab(i).ALTERADO_EM
						 ,ALTERADO_POR = v_tab(i).ALTERADO_POR
						 ,CHAVE_ORIGEM_PAI = v_tab(i).CHAVE_ORIGEM_PAI
						 ,COD_NATUREZA_JURIDICA = v_tab(i).COD_NATUREZA_JURIDICA
						 ,HON_CLIENTE_JDE = v_tab(i).HON_CLIENTE_JDE
						 ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						 ,HON_FORNECEDOR_JDE = v_tab(i).HON_FORNECEDOR_JDE
						 ,HON_IDENTIFICA_SECRETARIA = v_tab(i).HON_IDENTIFICA_SECRETARIA
						 ,IND_ARREDONDA_VLRS = v_tab(i).IND_ARREDONDA_VLRS
						 ,IND_CCOR = v_tab(i).IND_CCOR
						 ,IND_CFPFJ = v_tab(i).IND_CFPFJ
						 ,IND_COMPHIER_FILHO = v_tab(i).IND_COMPHIER_FILHO
						 ,IND_COMPHIER_PAI = v_tab(i).IND_COMPHIER_PAI
						 ,IND_CONTABILISTA = v_tab(i).IND_CONTABILISTA
                         ,IND_NAT_PESSOA_EFD = v_tab(i).IND_NAT_PESSOA_EFD
						 ,IND_ORGAO_GOVERNAMENTAL = v_tab(i).IND_ORGAO_GOVERNAMENTAL
						 ,IND_PFJMERC = v_tab(i).IND_PFJMERC
						 ,IND_PONTO_ALFANDEGADO = v_tab(i).IND_PONTO_ALFANDEGADO
						 ,IND_PROF = v_tab(i).IND_PROF
						 ,IND_RGPFJUF = v_tab(i).IND_RGPFJUF
						 ,IND_TRANSPORTADOR = v_tab(i).IND_TRANSPORTADOR
						 ,LOC_CODIGO_COBRANCA = v_tab(i).LOC_CODIGO_COBRANCA
						 ,LOC_CODIGO_TRANSP = v_tab(i).LOC_CODIGO_TRANSP
						 --,ORG_ID = v_tab(i).ORG_ID
						 ,PFJ_CHAVE_ORIGEM_PAI = v_tab(i).PFJ_CHAVE_ORIGEM_PAI
						 ,PFJ_CODIGO_COBRANCA = v_tab(i).PFJ_CODIGO_COBRANCA
						 ,PFJ_CODIGO_ENTREGA = v_tab(i).PFJ_CODIGO_ENTREGA
						 ,PFJ_CODIGO_RETIRADA = v_tab(i).PFJ_CODIGO_RETIRADA
						 ,PFJ_CODIGO_TRANSP = v_tab(i).PFJ_CODIGO_TRANSP
						 ,REVISAO = v_tab(i).REVISAO			   
                    WHERE PFJ_CODIGO = v_tab(i).PFJ_CODIGO;
               exception when others
                 then
                     r_put_line('Erro ao alterar PFJ - ' || v_tab(i).pfj_codigo || ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'E');  
                     commit;
                     end;
               end;
            when others
                 then
                     r_put_line('Erro ao incluir PFJ - ' || v_tab(i).pfj_codigo || ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_pfj_vig (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF hon_gttemp_pessoa_vig%ROWTYPE;
  v_tab    typ_tab;
  mdata    CORAPI_PFJ_VIG.data;
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,CAEN_CODIGO_PRINCIPAL,COD_CLASSIF_TRIB,CODIGO_CPRB,COD_INSTAL_ANP,CPF_CGC,
 CRT_CODIGO,CTRL_DESCARGA,DT_FIM,DT_INICIO,E_MAIL, HON_CONSOLIDA_ORIGEM,ID,IND_ACORDO_INTERNACIONAL,
 IND_BENEF_DISPENSADO_NIF,IND_CONTR_ICMS,IND_CONTR_IPI,IND_DESON_FOLHA_CPRB,IND_ECD,IND_REGIME_LUCRO,
 IND_SIMPLES_NACIONAL,IND_SITUACAO_PJ,IND_SUBSTITUTO_ICMS,NIF,NOME_FANTASIA,NUM_DEPENDENTES_IR,ORG_ID,ORIGEM,
 PFJ_CODIGO,RAZAO_SOCIAL,REL_PAGADOR_BENEF, REVISAO,TICO_CODIGO,VL_INSS_DEDUCAO,VL_PENSAO,WEB_SITE
 FROM hon_consolida_pfj_vig_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
        mdata.v.PFJ_CODIGO            := v_tab(i).PFJ_CODIGO;
        mdata.v.DT_INICIO             := v_tab(i).DT_INICIO;
        mdata.v.DT_FIM                := v_tab(i).DT_FIM;
        mdata.v.IND_CONTR_ICMS        := v_tab(i).IND_CONTR_ICMS;
        mdata.v.IND_CONTR_IPI         := v_tab(i).IND_CONTR_IPI;
        mdata.v.RAZAO_SOCIAL          := v_tab(i).RAZAO_SOCIAL;
        mdata.v.NOME_FANTASIA         := v_tab(i).NOME_FANTASIA;
        mdata.v.CPF_CGC               := v_tab(i).CPF_CGC;
        mdata.v.CAEN_CODIGO_PRINCIPAL := v_tab(i).CAEN_CODIGO_PRINCIPAL;
        mdata.v.WEB_SITE              := v_tab(i).WEB_SITE;
        mdata.v.E_MAIL                := v_tab(i).E_MAIL;
        mdata.v.TICO_CODIGO           := v_tab(i).TICO_CODIGO;
        mdata.v.ORIGEM                := v_tab(i).ORIGEM;
        mdata.v.IND_SUBSTITUTO_ICMS   := v_tab(i).IND_SUBSTITUTO_ICMS;
        mdata.v.IND_SIMPLES_NACIONAL  := v_tab(i).IND_SIMPLES_NACIONAL;
        mdata.v.CRT_CODIGO            := v_tab(i).CRT_CODIGO;
        --
        mdata.v.ALTERADO_EM              := v_tab(i).ALTERADO_EM;
        mdata.v.ALTERADO_POR             := v_tab(i).ALTERADO_POR;
        mdata.v.COD_CLASSIF_TRIB         := v_tab(i).COD_CLASSIF_TRIB;
        mdata.v.CODIGO_CPRB              := v_tab(i).CODIGO_CPRB;
        mdata.v.COD_INSTAL_ANP           := v_tab(i).COD_INSTAL_ANP;
        mdata.v.HON_CONSOLIDA_ORIGEM     := v_tab(i).HON_CONSOLIDA_ORIGEM;
        mdata.v.IND_ACORDO_INTERNACIONAL := v_tab(i).IND_ACORDO_INTERNACIONAL;
        mdata.v.IND_BENEF_DISPENSADO_NIF := v_tab(i).IND_BENEF_DISPENSADO_NIF;
        mdata.v.IND_DESON_FOLHA_CPRB     := v_tab(i).IND_DESON_FOLHA_CPRB;
        mdata.v.IND_ECD                  := v_tab(i).IND_ECD;
        mdata.v.IND_REGIME_LUCRO         := v_tab(i).IND_REGIME_LUCRO;
        mdata.v.IND_SITUACAO_PJ          := v_tab(i).IND_SITUACAO_PJ;
        mdata.v.NIF                      := v_tab(i).NIF;
        mdata.v.NUM_DEPENDENTES_IR       := v_tab(i).NUM_DEPENDENTES_IR;
        mdata.v.ORG_ID                   := v_tab(i).ORG_ID;
        mdata.v.REL_PAGADOR_BENEF        := v_tab(i).REL_PAGADOR_BENEF;
        mdata.v.REVISAO                  := v_tab(i).REVISAO;
        mdata.v.VL_INSS_DEDUCAO          := v_tab(i).VL_INSS_DEDUCAO;
        mdata.v.VL_PENSAO                := v_tab(i).VL_PENSAO;
        CORAPI_PFJ_VIG.ins(mdata);
  exception when DUP_VAL_ON_INDEX 
            then 
			   begin
			      mdata.v.PFJ_CODIGO := v_tab(i).PFJ_CODIGO;
                  mdata.v.DT_INICIO := v_tab(i).DT_INICIO;
                  mdata.v.DT_FIM := v_tab(i).DT_FIM;
                  mdata.v.IND_CONTR_ICMS := v_tab(i).IND_CONTR_ICMS;
                  mdata.v.IND_CONTR_IPI := v_tab(i).IND_CONTR_IPI;
                  mdata.v.RAZAO_SOCIAL := v_tab(i).RAZAO_SOCIAL;
                  mdata.v.NOME_FANTASIA := v_tab(i).NOME_FANTASIA;
                  mdata.v.CPF_CGC := v_tab(i).CPF_CGC;
                  mdata.v.CAEN_CODIGO_PRINCIPAL := v_tab(i).CAEN_CODIGO_PRINCIPAL;
                  mdata.v.WEB_SITE := v_tab(i).WEB_SITE;
                  mdata.v.E_MAIL := v_tab(i).E_MAIL;
                  mdata.v.TICO_CODIGO := v_tab(i).TICO_CODIGO;
                  mdata.v.ORIGEM := v_tab(i).ORIGEM;
                  mdata.v.IND_SUBSTITUTO_ICMS := v_tab(i).IND_SUBSTITUTO_ICMS;
                  mdata.v.IND_SIMPLES_NACIONAL := v_tab(i).IND_SIMPLES_NACIONAL;
                  mdata.v.CRT_CODIGO := v_tab(i).CRT_CODIGO;
                  mdata.i.PFJ_CODIGO := TRUE;
                  mdata.i.DT_INICIO := TRUE;
                  mdata.i.DT_FIM := TRUE;
                  mdata.i.IND_CONTR_ICMS := TRUE;
                  mdata.i.IND_CONTR_IPI := TRUE;
                  mdata.i.RAZAO_SOCIAL := TRUE;
                  mdata.i.NOME_FANTASIA := TRUE;
                  mdata.i.CPF_CGC := TRUE;
                  mdata.i.CAEN_CODIGO_PRINCIPAL := TRUE;
                  mdata.i.WEB_SITE := TRUE;
                  mdata.i.E_MAIL := TRUE;
                  mdata.i.TICO_CODIGO := TRUE;
                  mdata.i.ORIGEM := TRUE;
                  mdata.i.IND_SUBSTITUTO_ICMS := TRUE;
                  mdata.i.IND_SIMPLES_NACIONAL := TRUE;
                  mdata.i.CRT_CODIGO := TRUE;
                  CORAPI_PFJ_VIG.upd(mdata);
			   exception when others then
                     r_put_line('Erro ao alterar PFJ_VIG DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir PFJ_VIG - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_loc_pfj (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_loc_pfj%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
      ALTERADO_EM,ALTERADO_POR,CHAVE_ORIGEM,CODIGO_USUAL,HON_CONSOLIDA_ORIGEM,IND_DESTINATARIA,IND_ENTREGA,
      IND_GERAL,IND_REMETENTE,IND_RETIRADA,
        IND_TRANSPORTADORA,LOC_CODIGO,MNEMONICO,ORG_ID,ORIGEM,PFJ_CODIGO,REVISAO
 FROM hon_consolida_loc_pfj_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_LOCALIDADE_PESSOA ( ALTERADO_EM,ALTERADO_POR,CHAVE_ORIGEM,CODIGO_USUAL,
     HON_CONSOLIDA_ORIGEM,IND_DESTINATARIA,IND_ENTREGA,IND_GERAL,IND_REMETENTE,IND_RETIRADA,
     IND_TRANSPORTADORA,LOC_CODIGO,MNEMONICO,ORG_ID,ORIGEM,PFJ_CODIGO,REVISAO)
   VALUES (v_tab(i).ALTERADO_EM,v_tab(i).ALTERADO_POR,v_tab(i).CHAVE_ORIGEM,v_tab(i).CODIGO_USUAL,
           v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_DESTINATARIA,v_tab(i).IND_ENTREGA,
           v_tab(i).IND_GERAL,v_tab(i).IND_REMETENTE,v_tab(i).IND_RETIRADA,v_tab(i).IND_TRANSPORTADORA,
           v_tab(i).LOC_CODIGO,v_tab(i).MNEMONICO,v_tab(i).ORG_ID,v_tab(i).ORIGEM,v_tab(i).PFJ_CODIGO,
           v_tab(i).REVISAO);
    exception when DUP_VAL_ON_INDEX 
	          then 
				 begin
                 UPDATE  COR_LOCALIDADE_PESSOA 
				    SET MNEMONICO = v_tab(i).MNEMONICO
                       ,IND_GERAL = v_tab(i).IND_GERAL
                       ,CODIGO_USUAL = v_tab(i).CODIGO_USUAL
                       ,ORIGEM = v_tab(i).ORIGEM
					   --
                       ,ALTERADO_EM = v_tab(i).ALTERADO_EM
					   ,ALTERADO_POR = v_tab(i).ALTERADO_POR
					   ,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
                       ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
					   ,IND_DESTINATARIA = v_tab(i).IND_DESTINATARIA
					   ,IND_ENTREGA = v_tab(i).IND_ENTREGA
                       ,IND_REMETENTE = v_tab(i).IND_REMETENTE
					   ,IND_RETIRADA = v_tab(i).IND_RETIRADA
					   ,IND_TRANSPORTADORA = v_tab(i).IND_TRANSPORTADORA
                       --,ORG_ID = v_tab(i).ORG_ID
                       ,REVISAO = v_tab(i).REVISAO					   
                 WHERE PFJ_CODIGO = v_tab(i).PFJ_CODIGO
                   AND LOC_CODIGO = v_tab(i).LOC_CODIGO;				 
				 exception when others then
                     r_put_line('Erro ao alterar LOC_PFJ DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_PESSOA', 'E');  
                     commit;
                     end;
				 end; 
            when others
                 then
                     r_put_line('Erro ao incluir LOC_PFJ - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_loc_vig (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_loc_vig%ROWTYPE;
  v_tab typ_tab;
  mdata CORAPI_LOC_PFJ_VIG.data;  
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,BAIRRO,CAIXA_POSTAL,CCM,CEP,CEP_CP,COMPLEMENTO,DT_FIM,DT_INICIO,E_MAIL,FAX,HON_CONSOLIDA_ORIGEM,ID,INSCR_ESTADUAL,INSCR_INSS,
INSCR_SUFRAMA,LOC_CODIGO,LOGRADOURO,MUN_CODIGO,MUNICIPIO,NIRE,NUMERO,NUM_REGIME_ISS,NUM_REGIME_NF_ELET,ORG_ID,ORIGEM,PAIS,PFJ_CODIGO,PIS,POSTO_FISCAL,REVISAO,
TELEFONE1,TELEFONE2,TELEX,UNIDADE_FEDERATIVA,WEB_SITE
 FROM hon_consolida_loc_vig_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
       mdata.v.PFJ_CODIGO         := v_tab(i).PFJ_CODIGO;
       mdata.v.LOC_CODIGO         := v_tab(i).LOC_CODIGO;
       mdata.v.DT_INICIO          := v_tab(i).DT_INICIO;
       mdata.v.DT_FIM             := v_tab(i).DT_FIM;
       mdata.v.LOGRADOURO         := v_tab(i).LOGRADOURO;
       mdata.v.NUMERO             := v_tab(i).NUMERO;
       mdata.v.COMPLEMENTO        := v_tab(i).COMPLEMENTO;
       mdata.v.BAIRRO             := v_tab(i).BAIRRO;
       mdata.v.CEP                := v_tab(i).CEP;
       mdata.v.MUN_CODIGO         := v_tab(i).MUN_CODIGO;
       mdata.v.TELEFONE1          := v_tab(i).TELEFONE1;
       mdata.v.TELEFONE2          := v_tab(i).TELEFONE2;
       mdata.v.FAX                := v_tab(i).FAX;
       mdata.v.TELEX              := v_tab(i).TELEX;
       mdata.v.INSCR_ESTADUAL     := v_tab(i).INSCR_ESTADUAL;
       mdata.v.E_MAIL             := v_tab(i).E_MAIL;
       mdata.v.WEB_SITE           := v_tab(i).WEB_SITE;
       mdata.v.MUNICIPIO          := v_tab(i).MUNICIPIO;
       mdata.v.UNIDADE_FEDERATIVA := v_tab(i).UNIDADE_FEDERATIVA;
       mdata.v.PAIS               := v_tab(i).PAIS;
       mdata.v.ORIGEM             := v_tab(i).ORIGEM;
       mdata.v.CCM                := v_tab(i).CCM;
       mdata.v.INSCR_SUFRAMA      := v_tab(i).INSCR_SUFRAMA;
       mdata.v.CAIXA_POSTAL       := v_tab(i).CAIXA_POSTAL;
       mdata.v.CEP_CP             := v_tab(i).CEP_CP;
	   --
	   mdata.v.ALTERADO_EM          := v_tab(i).ALTERADO_EM;
	   mdata.v.ALTERADO_POR         := v_tab(i).ALTERADO_POR;
	   mdata.v.HON_CONSOLIDA_ORIGEM := v_tab(i).HON_CONSOLIDA_ORIGEM;
	   mdata.v.INSCR_INSS           := v_tab(i).INSCR_INSS;
	   mdata.v.NIRE                 := v_tab(i).NIRE;
	   mdata.v.NUM_REGIME_ISS       := v_tab(i).NUM_REGIME_ISS;
	   mdata.v.NUM_REGIME_NF_ELET   := v_tab(i).NUM_REGIME_NF_ELET;
	   mdata.v.ORG_ID               := v_tab(i).ORG_ID;
	   mdata.v.PIS                  := v_tab(i).PIS;
	   mdata.v.POSTO_FISCAL         := v_tab(i).POSTO_FISCAL;
	   mdata.v.REVISAO              := v_tab(i).REVISAO;
       CORAPI_LOC_PFJ_VIG.ins(mdata);
    exception
      when DUP_VAL_ON_INDEX 
	        then 
				begin
                   mdata.v.PFJ_CODIGO := v_tab(i).PFJ_CODIGO;
                   mdata.v.LOC_CODIGO := v_tab(i).LOC_CODIGO;
                   mdata.v.DT_INICIO := v_tab(i).DT_INICIO;
                   mdata.v.DT_FIM := v_tab(i).DT_FIM;
                   mdata.v.LOGRADOURO := v_tab(i).LOGRADOURO;
                   mdata.v.NUMERO := v_tab(i).NUMERO;
                   mdata.v.COMPLEMENTO := v_tab(i).COMPLEMENTO;
                   mdata.v.BAIRRO := v_tab(i).BAIRRO;
                   mdata.v.CEP := v_tab(i).CEP;
                   mdata.v.MUN_CODIGO := v_tab(i).MUN_CODIGO;
                   mdata.v.TELEFONE1 := v_tab(i).TELEFONE1;
                   mdata.v.TELEFONE2 := v_tab(i).TELEFONE2;
                   mdata.v.FAX := v_tab(i).FAX;
                   mdata.v.TELEX := v_tab(i).TELEX;
                   mdata.v.INSCR_ESTADUAL := v_tab(i).INSCR_ESTADUAL;
                   mdata.v.E_MAIL := v_tab(i).E_MAIL;
                   mdata.v.WEB_SITE := v_tab(i).WEB_SITE;
                   mdata.v.MUNICIPIO := v_tab(i).MUNICIPIO;
                   mdata.v.UNIDADE_FEDERATIVA := v_tab(i).UNIDADE_FEDERATIVA;
                   mdata.v.PAIS := v_tab(i).PAIS;
                   mdata.v.ORIGEM := v_tab(i).ORIGEM;
                   mdata.v.CCM := v_tab(i).CCM;
                   mdata.v.INSCR_SUFRAMA := v_tab(i).INSCR_SUFRAMA;
                   mdata.v.CAIXA_POSTAL := v_tab(i).CAIXA_POSTAL;
                   mdata.v.CEP_CP := v_tab(i).CEP_CP;
                   mdata.i.PFJ_CODIGO := TRUE;
                   mdata.i.LOC_CODIGO := TRUE;
                   mdata.i.DT_INICIO := TRUE;
                   mdata.i.DT_FIM := TRUE;
                   mdata.i.LOGRADOURO := TRUE;
                   mdata.i.NUMERO := TRUE;
                   mdata.i.COMPLEMENTO := TRUE;
                   mdata.i.BAIRRO := TRUE;
                   mdata.i.CEP := TRUE;
                   mdata.i.MUN_CODIGO := TRUE;
                   mdata.i.TELEFONE1 := TRUE;
                   mdata.i.TELEFONE2 := TRUE;
                   mdata.i.FAX := TRUE;
                   mdata.i.TELEX := TRUE;
                   mdata.i.INSCR_ESTADUAL := TRUE;
                   mdata.i.E_MAIL := TRUE;
                   mdata.i.WEB_SITE := TRUE;
                   mdata.i.MUNICIPIO := TRUE;
                   mdata.i.UNIDADE_FEDERATIVA := TRUE;
                   mdata.i.PAIS := TRUE;
                   mdata.i.ORIGEM := TRUE;
                   mdata.i.CCM := TRUE;
                   mdata.i.INSCR_SUFRAMA := TRUE;
                   mdata.i.CAIXA_POSTAL := TRUE;
                   mdata.i.CEP_CP := TRUE;
                   CORAPI_LOC_PFJ_VIG.upd(mdata);
				exception when others
				     then
                     r_put_line('Erro ao alterar LOC_PFJ_VIG DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_VIGENCIA', 'E');  
                     commit;
                     end;
				end; 
            when others
                 then
                     r_put_line('Erro ao incluir LOC_PFJ_VIG - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_VIGENCIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_cla_pfj (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_clas_pfj%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT CP_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,PFJ_CODIGO
 FROM hon_consolida_clas_pfj_v; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_CLASSIFICACAO_PESSOA (CP_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,PFJ_CODIGO)
   VALUES (v_tab(i).CP_CODIGO,v_tab(i).DT_FIM,v_tab(i).DT_INICIO,upper(v_tab(i).ELEMENTO),v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).PFJ_CODIGO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update COR_CLASSIFICACAO_PESSOA
				     set DT_FIM = v_tab(i).DT_FIM
					    ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
           	   where elemento = upper(v_tab(i).ELEMENTO)
				     and cp_codigo = v_tab(i).CP_CODIGO
					 and pfj_codigo = v_tab(i).PFJ_CODIGO
					 and dt_inicio =  v_tab(i).DT_INICIO;
			  exception when others then
                     r_put_line('Erro ao alterar Classe PFJ DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir Classe PFJ - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_merc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_merc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT AGLU_CLASSE_STF_MVA_CODIGO,AGLU_STF_PAUTA_CODIGO,ALTERADO_EM,ALTERADO_POR,CEST_CODIGO,CHAVE_ORIGEM,COD_ANT_ITEM,COD_BARRA,COD_COMB,COD_GTIN,
 CODIGO_COMBUSTIVEIS_SOLVENTES,CODIGO_USUAL,COD_PROPRIO,DEPART_CODIGO,DESCRICAO,DFLT_AM_CODIGO_ENTRADA,DFLT_AM_CODIGO_SAIDA,DFLT_NBM_CODIGO,DFLT_OM_CODIGO,
 DFLT_PRECO_UNITARIO_SAIDA,DFLT_UNI_CODIGO_ENTRADA,DFLT_UNI_CODIGO_ESTOQUE,DFLT_UNI_CODIGO_SAIDA,EXEMPLO_PARA_MANUAL,FAMILIA_CODIGO,GRUPO_CODIGO,HON_CONSOLIDA_ORIGEM,
 IND_SIMILAR_NACIONAL,ITEM_COLETADO_ANP,MAPA_FISIOGRAFICO,MERCADOLOGICO,MERC_CODIGO,NOME,ORIGEM,REVISAO,SELO_IPI_CODIGO,SETOR_CODIGO,SUBFAMILIA_CODIGO,TESTE,TESTE1,
 TIPO_ITEM
 FROM hon_consolida_merc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_MERCADORIA (AGLU_CLASSE_STF_MVA_CODIGO,AGLU_STF_PAUTA_CODIGO,ALTERADO_EM,ALTERADO_POR,
                 CEST_CODIGO,CHAVE_ORIGEM,COD_ANT_ITEM,COD_BARRA,COD_COMB,COD_GTIN,CODIGO_COMBUSTIVEIS_SOLVENTES,
                 CODIGO_USUAL,COD_PROPRIO,DEPART_CODIGO,DESCRICAO,DFLT_AM_CODIGO_ENTRADA,DFLT_AM_CODIGO_SAIDA,
                 DFLT_NBM_CODIGO,DFLT_OM_CODIGO,DFLT_PRECO_UNITARIO_SAIDA,DFLT_UNI_CODIGO_ENTRADA,
                 DFLT_UNI_CODIGO_ESTOQUE,DFLT_UNI_CODIGO_SAIDA,EXEMPLO_PARA_MANUAL,FAMILIA_CODIGO,GRUPO_CODIGO,
                 HON_CONSOLIDA_ORIGEM,IND_SIMILAR_NACIONAL,ITEM_COLETADO_ANP,MAPA_FISIOGRAFICO,MERCADOLOGICO,
                 MERC_CODIGO,NOME,ORIGEM,REVISAO,SELO_IPI_CODIGO,SETOR_CODIGO,SUBFAMILIA_CODIGO,TESTE,TESTE1,
                 TIPO_ITEM)
   VALUES (v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO,v_tab(i).AGLU_STF_PAUTA_CODIGO,v_tab(i).ALTERADO_EM,
           v_tab(i).ALTERADO_POR,v_tab(i).CEST_CODIGO,v_tab(i).CHAVE_ORIGEM,v_tab(i).COD_ANT_ITEM,
           v_tab(i).COD_BARRA, v_tab(i).COD_COMB,v_tab(i).COD_GTIN,v_tab(i).CODIGO_COMBUSTIVEIS_SOLVENTES,
           v_tab(i).CODIGO_USUAL,v_tab(i).COD_PROPRIO,v_tab(i).DEPART_CODIGO,v_tab(i).DESCRICAO,
           v_tab(i).DFLT_AM_CODIGO_ENTRADA,v_tab(i).DFLT_AM_CODIGO_SAIDA,v_tab(i).DFLT_NBM_CODIGO,
           v_tab(i).DFLT_OM_CODIGO, v_tab(i).DFLT_PRECO_UNITARIO_SAIDA,v_tab(i).DFLT_UNI_CODIGO_ENTRADA,
           v_tab(i).DFLT_UNI_CODIGO_ESTOQUE,v_tab(i).DFLT_UNI_CODIGO_SAIDA,v_tab(i).EXEMPLO_PARA_MANUAL,
           v_tab(i).FAMILIA_CODIGO,v_tab(i).GRUPO_CODIGO,v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).IND_SIMILAR_NACIONAL,v_tab(i).ITEM_COLETADO_ANP, v_tab(i).MAPA_FISIOGRAFICO,
           v_tab(i).MERCADOLOGICO,v_tab(i).MERC_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).REVISAO,
           v_tab(i).SELO_IPI_CODIGO,v_tab(i).SETOR_CODIGO,v_tab(i).SUBFAMILIA_CODIGO,v_tab(i).TESTE,
           v_tab(i).TESTE1,v_tab(i).TIPO_ITEM);
    exception when DUP_VAL_ON_INDEX 
	        then 
			   begin
                  UPDATE  COR_MERCADORIA 
				     SET NOME = v_tab(i).nome
                        ,DESCRICAO = v_tab(i).DESCRICAO
                        ,DFLT_AM_CODIGO_ENTRADA = v_tab(i).DFLT_AM_CODIGO_ENTRADA
                        ,DFLT_AM_CODIGO_SAIDA = v_tab(i).DFLT_AM_CODIGO_SAIDA
                        ,DFLT_OM_CODIGO = v_tab(i).DFLT_OM_CODIGO
                        ,DFLT_NBM_CODIGO = v_tab(i).DFLT_NBM_CODIGO
                        ,DFLT_UNI_CODIGO_ENTRADA = v_tab(i).DFLT_UNI_CODIGO_ENTRADA
                        ,DFLT_UNI_CODIGO_SAIDA = v_tab(i).DFLT_UNI_CODIGO_SAIDA
                        ,DFLT_UNI_CODIGO_ESTOQUE = v_tab(i).DFLT_UNI_CODIGO_ESTOQUE
                        --,CODIGO_USUAL = v_tab(i).codigo_usual
                        --,ORIGEM = v_tab(i).ORIGEM
                        --,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
                        ,TIPO_ITEM = v_tab(i).TIPO_ITEM
                        ,COD_BARRA = v_tab(i).COD_BARRA
						--
                        ,AGLU_CLASSE_STF_MVA_CODIGO = v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO
						,AGLU_STF_PAUTA_CODIGO = v_tab(i).AGLU_STF_PAUTA_CODIGO
						,ALTERADO_EM = v_tab(i).ALTERADO_EM
						,ALTERADO_POR = v_tab(i).ALTERADO_POR
						,CEST_CODIGO = v_tab(i).CEST_CODIGO
						,COD_ANT_ITEM = v_tab(i).COD_ANT_ITEM
						,COD_COMB = v_tab(i).COD_COMB
						,COD_GTIN = v_tab(i).COD_GTIN
						,CODIGO_COMBUSTIVEIS_SOLVENTES = v_tab(i).CODIGO_COMBUSTIVEIS_SOLVENTES
						,COD_PROPRIO = v_tab(i).COD_PROPRIO
						,DEPART_CODIGO = v_tab(i).DEPART_CODIGO
						,DFLT_PRECO_UNITARIO_SAIDA = v_tab(i).DFLT_PRECO_UNITARIO_SAIDA
						,EXEMPLO_PARA_MANUAL = v_tab(i).EXEMPLO_PARA_MANUAL
						,FAMILIA_CODIGO = v_tab(i).FAMILIA_CODIGO
						,GRUPO_CODIGO = v_tab(i).GRUPO_CODIGO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,IND_SIMILAR_NACIONAL = v_tab(i).IND_SIMILAR_NACIONAL
						,ITEM_COLETADO_ANP = v_tab(i).ITEM_COLETADO_ANP
						,MAPA_FISIOGRAFICO = v_tab(i).MAPA_FISIOGRAFICO
						,MERCADOLOGICO = v_tab(i).MERCADOLOGICO
						,REVISAO = v_tab(i).REVISAO
						,SELO_IPI_CODIGO = v_tab(i).SELO_IPI_CODIGO
						,SETOR_CODIGO = v_tab(i).SETOR_CODIGO
						,SUBFAMILIA_CODIGO = v_tab(i).SUBFAMILIA_CODIGO
						,TESTE = v_tab(i).TESTE
						,TESTE1 = v_tab(i).TESTE1
                     WHERE MERC_CODIGO = v_tab(i).MERC_CODIGO;
			   exception when others then			   
                     r_put_line('Erro ao alterar Mercadoria DUP. - ' || v_tab(i).merc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_MERCADORIA', 'E');  
                     commit;
                     end;
			   end;			   
            when others
                 then
                     r_put_line('Erro ao incluir Mercadoria - ' || v_tab(i).merc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_MERCADORIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_cla_merc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_clas_merc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGLU_CLASSE_STF_MVA_CODIGO,CMERC_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,ID,
  IND_AGLUTINADOR,MERC_CODIGO,UF_CODIGO
 FROM hon_consolida_clas_merc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
     INSERT INTO COR_CLASSIFICACAO_MERCADORIA (AGLU_CLASSE_STF_MVA_CODIGO,CMERC_CODIGO,DT_FIM,DT_INICIO,
                 ELEMENTO,HON_CONSOLIDA_ORIGEM,IND_AGLUTINADOR,MERC_CODIGO,UF_CODIGO)
   VALUES (v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO,v_tab(i).CMERC_CODIGO,v_tab(i).DT_FIM,v_tab(i).DT_INICIO,
           v_tab(i).ELEMENTO,v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_AGLUTINADOR,v_tab(i).MERC_CODIGO,
           v_tab(i).UF_CODIGO);
       exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
                 UPDATE  COR_CLASSIFICACAO_MERCADORIA 
				    SET DT_FIM = v_tab(i).DT_FIM
					  , AGLU_CLASSE_STF_MVA_CODIGO = v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO
					  , HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
					  , IND_AGLUTINADOR = v_tab(i).IND_AGLUTINADOR	  
                  WHERE ELEMENTO = v_tab(i).ELEMENTO
                    AND CMERC_CODIGO = v_tab(i).CMERC_CODIGO
                    AND MERC_CODIGO = v_tab(i).MERC_CODIGO
                    AND DT_INICIO = v_tab(i).DT_INICIO
                    AND UF_CODIGO = v_tab(i).UF_CODIGO ;
			  exception when others then
                     r_put_line('Erro ao alterar Classe Merc. DUP. - ' || v_tab(i).cmerc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_MERCADORIA', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir Classe Merc. - ' || v_tab(i).cmerc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_MERCADORIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_prest (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_prest%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT CHAVE_ORIGEM,CLASSIF_CODIGO,DESCRICAO,HON_CONSOLIDA_ORIGEM,NBS_CODIGO,NOME,ORIGEM,PRES_CODIGO,REVISAO
 FROM hon_consolida_prest_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_PRESTACAO (CHAVE_ORIGEM,CLASSIF_CODIGO,DESCRICAO,HON_CONSOLIDA_ORIGEM,NBS_CODIGO,
                 NOME,ORIGEM,PRES_CODIGO,REVISAO)
   VALUES (v_tab(i).CHAVE_ORIGEM,v_tab(i).CLASSIF_CODIGO,v_tab(i).DESCRICAO,v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).NBS_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).PRES_CODIGO,v_tab(i).REVISAO);
      exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_prestacao
				     set CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
					    --,CLASSIF_CODIGO = v_tab(i).CLASSIF_CODIGO
						,DESCRICAO = v_tab(i).DESCRICAO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,NBS_CODIGO = v_tab(i).NBS_CODIGO
						,NOME = v_tab(i).NOME
						,ORIGEM = v_tab(i).ORIGEM
						,REVISAO = v_tab(i).REVISAO
				   where pres_codigo = v_tab(i).PRES_CODIGO;
			  exception when others then
                     r_put_line('Erro ao alterar Prestacao DUP. - ' || v_tab(i).pres_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PRESTACAO', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir Prestacao - ' || v_tab(i).pres_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PRESTACAO', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_iss (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_iss%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,CBO,CHAVE_ORIGEM,CLASSIF_CODIGO,CODRET_COP,CODRET_FIS,CODRET_JUR,
        DESCRICAO,DFLT_UNI_CODIGO,HON_CONSOLIDA_ORIGEM,IND_LC116,IND_RESP_IRRF,NBS_CODIGO,NOME,ORIGEM,
        REVISAO,SISS_CODIGO,SISS_CODIGO_LC116,TIPO_SERVICO
 FROM hon_consolida_serv_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
     INSERT INTO COR_SERVICO_ISS (ALTERADO_EM,ALTERADO_POR,CBO,CHAVE_ORIGEM,CLASSIF_CODIGO,CODRET_COP,
                 CODRET_FIS,CODRET_JUR,DESCRICAO,DFLT_UNI_CODIGO,HON_CONSOLIDA_ORIGEM,IND_LC116,IND_RESP_IRRF,
                 NBS_CODIGO,NOME,ORIGEM,REVISAO,SISS_CODIGO,SISS_CODIGO_LC116,TIPO_SERVICO)
   VALUES (v_tab(i).ALTERADO_EM,v_tab(i).ALTERADO_POR,v_tab(i).CBO,v_tab(i).CHAVE_ORIGEM,v_tab(i).CLASSIF_CODIGO,
           v_tab(i).CODRET_COP,v_tab(i).CODRET_FIS,v_tab(i).CODRET_JUR,v_tab(i).DESCRICAO,
           v_tab(i).DFLT_UNI_CODIGO,v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_LC116,v_tab(i).IND_RESP_IRRF,
           v_tab(i).NBS_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).REVISAO,v_tab(i).SISS_CODIGO,
           v_tab(i).SISS_CODIGO_LC116,v_tab(i).TIPO_SERVICO);
    exception when DUP_VAL_ON_INDEX 
	        then 
			   begin
			      update cor_servico_iss
				     set ALTERADO_EM = v_tab(i).ALTERADO_EM
					    ,ALTERADO_POR = v_tab(i).ALTERADO_POR
						,CBO = v_tab(i).CBO
						,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
						--,CLASSIF_CODIGO = v_tab(i).CLASSIF_CODIGO
						,CODRET_COP = v_tab(i).CODRET_COP
						,CODRET_FIS = v_tab(i).CODRET_FIS
						,CODRET_JUR = v_tab(i).CODRET_JUR
						,DESCRICAO = v_tab(i).DESCRICAO
						,DFLT_UNI_CODIGO = v_tab(i).DFLT_UNI_CODIGO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,IND_LC116 = v_tab(i).IND_LC116
						,IND_RESP_IRRF = v_tab(i).IND_RESP_IRRF
						,NBS_CODIGO = v_tab(i).NBS_CODIGO
						,NOME = v_tab(i).NOME
						,ORIGEM = v_tab(i).ORIGEM
						,REVISAO = v_tab(i).REVISAO
						,SISS_CODIGO_LC116 = v_tab(i).SISS_CODIGO_LC116
						,TIPO_SERVICO = v_tab(i).TIPO_SERVICO
				   where siss_codigo =  v_tab(i).SISS_CODIGO;
               exception when others then
                     r_put_line('Erro ao alterar Serviço DUP. - ' || v_tab(i).siss_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'E');  
                     commit;
                     end;
			   end; 
            when others
                 then
                     r_put_line('Erro ao incluir Serviço - ' || v_tab(i).siss_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_dof (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
CODIGO_DO_SITE,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DOF_SEQUENCE,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
ID,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID
 FROM hon_consolida_dof_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_DOF (AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID)
   VALUES (v_tab(i).AGENTE,
v_tab(i).ANO_MES_EMISSAO,
v_tab(i).ANO_VEICULO,
v_tab(i).AUTENTICACAO_DIGITAL,
v_tab(i).CAD_ESPECIFICO_INSS_CEI,
v_tab(i).CERT_VEICULO,
v_tab(i).CERT_VEICULO_UF,
v_tab(i).CFOP_CODIGO,
v_tab(i).CFPS,
v_tab(i).CNPJ_COL,
v_tab(i).CNPJ_CRED_CARTAO,
v_tab(i).CNPJ_ENTG,
v_tab(i).CNX,
v_tab(i).COBRANCA_LOC_CODIGO,
v_tab(i).COBRANCA_PFJ_CODIGO,
v_tab(i).COD_AUT,
v_tab(i).COD_GRUPO_TENSAO,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).CODIGO_CONSUMO,
v_tab(i).COD_LINHA,
v_tab(i).COD_MOTIVO_CANC_SYN,
v_tab(i).COD_MUN_COL,
v_tab(i).COD_MUN_ENTG,
v_tab(i).COD_OPER_CARTAO,
v_tab(i).COD_REG_ESP_TRIBUTACAO,
v_tab(i).COMP_PRESTACAO_NOME,
v_tab(i).COMP_PRESTACAO_VALOR,
v_tab(i).CONSIG_PFJ_CODIGO,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPAG_CODIGO,
v_tab(i).CPF,
v_tab(i).CRT_CODIGO,
v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
v_tab(i).CTRL_CONTEUDO,
v_tab(i).CTRL_DT_INCLUSAO,
v_tab(i).CTRL_DT_ULT_ALTERACAO,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_FASE,
v_tab(i).CTRL_FASE_FISCAL,
v_tab(i).CTRL_OBS_CALCULO,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_RAT_FRETE,
v_tab(i).CTRL_OBS_RAT_M,
v_tab(i).CTRL_OBS_RAT_ODA,
v_tab(i).CTRL_OBS_RAT_P,
v_tab(i).CTRL_OBS_RAT_S,
v_tab(i).CTRL_OBS_RAT_SEGURO,
v_tab(i).CTRL_OBS_TOT_M,
v_tab(i).CTRL_OBS_TOT_O,
v_tab(i).CTRL_OBS_TOT_P,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_S,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_OBS_VL_PARCELAS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_tab(i).CTRL_PROCESSAMENTO,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_SITUACAO_DOF,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DEM_PTA,
v_tab(i).DESC_TITULO,
v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
v_tab(i).DESTINATARIO_LOC_CODIGO,
v_tab(i).DESTINATARIO_PFJ_CODIGO,
v_tab(i).DEST_PRINC_EST_CODIGO,
v_tab(i).DH_EMISSAO,
v_tab(i).DH_PROCESS_MANAGER,
v_tab(i).DOF_IMPORT_NUMERO,
v_tab(i).DT_CONVERSAO_MOEDA_NAC,
v_tab(i).DT_EMISSAO_TOMADOR,
v_tab(i).DT_EXE_SERV,
v_tab(i).DT_FATO_GERADOR_ATE,
v_tab(i).DT_FATO_GERADOR_IMPOSTO,
v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
v_tab(i).DT_LEITURA,
v_tab(i).DT_LIMITE_EMISSAO,
v_tab(i).DT_LIMITE_SAIDA,
v_tab(i).DT_PREV_CHEGADA,
v_tab(i).DT_PREV_EMBARQUE,
v_tab(i).DT_RECEBIMENTO_DESTINO,
v_tab(i).DT_REF_CALC_IMP,
v_tab(i).DT_REF_CPAG,
v_tab(i).DT_REFER_EXT,
v_tab(i).DT_ROMANEIO,
v_tab(i).DT_SAIDA_MERCADORIAS,
v_tab(i).EDOF_CODIGO,
v_tab(i).EDOF_CODIGO_RES,
v_tab(i).EMITENTE_LOC_CODIGO,
v_tab(i).EMITENTE_PFJ_CODIGO,
v_tab(i).ENTREGA_LOC_CODIGO,
v_tab(i).ENTREGA_PFJ_CODIGO,
v_tab(i).ESPECIALIZACAO,
v_tab(i).FATOR_FINANCIAMENTO,
v_tab(i).FERROV_SUBST_LOC_CODIGO,
v_tab(i).FERROV_SUBST_PFJ_CODIGO,
v_tab(i).FORMA_PAGTO_TRANSPORTE,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_NATUREZA_OPERACAO,
v_tab(i).HON_PRACA_PAGTO,
v_tab(i).HON_PRESTACAO_SERVICO,
v_tab(i).HON_VALOR_DESCONTO,
v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
v_tab(i).IE_COL,
v_tab(i).IE_ENTG,
v_tab(i).IM_COL,
v_tab(i).IM_ENTG,
v_tab(i).IND_ALTERACAO_TOMADOR,
v_tab(i).IND_BALSA,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_CIOT,
v_tab(i).IND_CLASSE,
v_tab(i).IND_COBRANCA_BANCARIA,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_EIX,
v_tab(i).IND_ENTRADA_SAIDA,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXPORTA_PARCELAS,
v_tab(i).IND_F0,
v_tab(i).IND_GABARITO,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LOTACAO,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_NAT_FRETE,
v_tab(i).IND_NAV,
v_tab(i).IND_NFE_AJUSTE,
v_tab(i).IND_PRECO_AJUSTADO_CPAG,
v_tab(i).IND_PRECOS_MOEDA_NAC,
v_tab(i).IND_PRES_COMPRADOR,
v_tab(i).IND_RATEIO,
v_tab(i).IND_REPERCUSSAO_FISCAL,
v_tab(i).IND_RESP_FRETE,
v_tab(i).IND_RESP_ISS_RET,
v_tab(i).IND_SIMPLES_ME_EPP,
v_tab(i).IND_SIT_TRIBUT_DOF,
v_tab(i).IND_TFA,
v_tab(i).IND_TIT,
v_tab(i).IND_TOT_VL_FATURADO,
v_tab(i).IND_USA_IF_CALC_IMP,
v_tab(i).IND_USA_IF_ESCRIT,
v_tab(i).IND_VEIC,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).INFORMANTE_EST_CODIGO,
v_tab(i).LINHA,
v_tab(i).LOCAL_EMBARQUE,
v_tab(i).LOCVIG_ID_DESTINATARIO,
v_tab(i).LOCVIG_ID_REMETENTE,
v_tab(i).MARCA_VEICULO,
v_tab(i).MDOF_CODIGO,
v_tab(i).MODELO_VEICULO,
v_tab(i).MODO_EMISSAO,
v_tab(i).MP_CODIGO,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_CODIGO_ISS,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).MUN_PRES_SERVICO,
v_tab(i).NAT_VOLUME,
v_tab(i).NFE_COD_SITUACAO,
v_tab(i).NFE_COD_SITUACAO_SYN,
v_tab(i).NFE_CONTINGENCIA,
v_tab(i).NFE_DESC_SITUACAO,
v_tab(i).NFE_ENVIADA,
v_tab(i).NFE_HORA_GMT_EMI,
v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NFE_PROTOCOLO,
v_tab(i).NF_IMPRESSORA_PADRAO,
v_tab(i).NFORM_FINAL,
v_tab(i).NFORM_INICIAL,
v_tab(i).NOME_MOTORISTA,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_APOLICE_SEGURO,
v_tab(i).NUM_AUTOR_OPER_CARTAO,
v_tab(i).NUM_CONTAINER,
v_tab(i).NUM_EMBARQUE,
v_tab(i).NUMERO,
v_tab(i).NUMERO_ATE,
v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
v_tab(i).NUM_FIM_RES,
v_tab(i).NUM_INI_RES,
v_tab(i).NUM_PASSE,
v_tab(i).NUM_PEDIDO,
v_tab(i).NUM_PLACA_VEICULO,
v_tab(i).NUM_REGISTRO_SECEX,
v_tab(i).NUM_RE_SISCOMEX,
v_tab(i).NUM_ROMANEIO,
v_tab(i).NUM_TITULO,
v_tab(i).NUM_VEICULO,
v_tab(i).NUM_VIAGEM,
v_tab(i).NUM_VOLUME_COMP,
v_tab(i).OBSERVACAO,
v_tab(i).ORDEM_EMBARQUE,
v_tab(i).OTM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
v_tab(i).PESO_BRT_COMP,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQ_COMP,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_REEMBALAGEM_KG,
v_tab(i).PFJ_CODIGO_REDESPACHANTE,
v_tab(i).PFJVIG_ID_DESTINATARIO,
v_tab(i).PFJVIG_ID_REMETENTE,
v_tab(i).PLACA_VEICULO_UF_CODIGO,
v_tab(i).POLT_CAB,
v_tab(i).PORTADOR_BANCO_NUM,
v_tab(i).PRECO_TOTAL_M,
v_tab(i).PRECO_TOTAL_O,
v_tab(i).PRECO_TOTAL_P,
v_tab(i).PRECO_TOTAL_S,
v_tab(i).PRODUTO_PREDOMINANTE,
v_tab(i).QTD_CANC,
v_tab(i).QTD_CANC_RES,
v_tab(i).QTD_CONSUMO,
v_tab(i).QTD_PASS_ORIG,
v_tab(i).QTD_PRESTACOES,
v_tab(i).REDESPACHANTE_LOC_CODIGO,
v_tab(i).REMETENTE_LOC_CODIGO,
v_tab(i).REMETENTE_PFJ_CODIGO,
v_tab(i).REM_PRINC_EST_CODIGO,
v_tab(i).RESP_FRETE_REDESP,
v_tab(i).RETIRADA_LOC_CODIGO,
v_tab(i).RETIRADA_PFJ_CODIGO,
v_tab(i).RETIRA_MERC_DESTINO,
v_tab(i).REVISAO,
v_tab(i).RNTC,
v_tab(i).RNTC_COMP,
v_tab(i).RNTC_COMP_2,
v_tab(i).SEQ_CALCULO,
v_tab(i).SERIE_SUBSERIE,
v_tab(i).SERIE_SUBSERIE_RES,
v_tab(i).SIGLA_MOEDA,
v_tab(i).SIS_CODIGO,
v_tab(i).SIS_CODIGO_CONTABILIZOU,
v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
v_tab(i).TEMPERATURA,
v_tab(i).TIPO,
v_tab(i).TIPO_CTE,
v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_PGTO,
v_tab(i).TIPO_SERVICO_CTE,
v_tab(i).TIPO_TARIFA,
v_tab(i).TIPO_TOMADOR,
v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
v_tab(i).TP_ASSINANTE,
v_tab(i).TP_INT_PAG,
v_tab(i).TP_LIGACAO,
v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
v_tab(i).TRANSPORTADOR_LOC_CODIGO,
v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
v_tab(i).TX_CONVERSAO_MOEDA_NAC,
v_tab(i).TXT_INSTRUCAO_BLOQUETO,
v_tab(i).UF_CODIGO_DESTINO,
v_tab(i).UF_CODIGO_EMITENTE,
v_tab(i).UF_CODIGO_ENTREGA,
v_tab(i).UF_CODIGO_ORIGEM,
v_tab(i).UF_CODIGO_RETIRADA,
v_tab(i).UF_CODIGO_SUBST,
v_tab(i).UF_CODIGO_TRANSP,
v_tab(i).UF_EMBARQUE,
v_tab(i).UF_VEIC_COMP,
v_tab(i).UF_VEIC_COMP_2,
v_tab(i).USUARIO_INCLUSAO,
v_tab(i).USUARIO_ULT_ALTERACAO,
v_tab(i).VEIC_DESCR,
v_tab(i).VEIC_ID_COMP,
v_tab(i).VEIC_ID_COMP_2,
v_tab(i).VL_ABAT_NT,
v_tab(i).VL_ADU,
v_tab(i).VL_ADU_ICMS,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
v_tab(i).VL_BASE_CT_STF,
v_tab(i).VL_BASE_INSS_RET_PER,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CT_STF,
v_tab(i).VL_DESCONTO_NF,
v_tab(i).VL_DESPACHO,
v_tab(i).VL_DESP_CAR_DESC,
v_tab(i).VL_DESP_PORT,
v_tab(i).VL_FISCAL_M,
v_tab(i).VL_FISCAL_O,
v_tab(i).VL_FISCAL_P,
v_tab(i).VL_FISCAL_S,
v_tab(i).VL_FRETE,
v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
v_tab(i).VL_FRETE_LIQ,
v_tab(i).VL_FRETE_MM,
v_tab(i).VL_FRETE_TRAF_MUTUO,
v_tab(i).VL_FRT_PV,
v_tab(i).VL_GRIS,
v_tab(i).VL_INSS,
v_tab(i).VL_OUTRAS_DESPESAS,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_PEDAGIO,
v_tab(i).VL_PESO_TX,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_SEC_CAT,
v_tab(i).VL_SEGURO,
v_tab(i).VL_SERV_NT,
v_tab(i).VL_TAXA_ADV,
v_tab(i).VL_TAXA_TERRESTRE,
v_tab(i).VL_TERC,
v_tab(i).VL_TFA,
v_tab(i).VL_TOTAL_BASE_COFINS,
v_tab(i).VL_TOTAL_BASE_COFINS_RET,
v_tab(i).VL_TOTAL_BASE_CSLL_RET,
v_tab(i).VL_TOTAL_BASE_ICMS,
v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
v_tab(i).VL_TOTAL_BASE_INSS,
v_tab(i).VL_TOTAL_BASE_INSS_RET,
v_tab(i).VL_TOTAL_BASE_IPI,
v_tab(i).VL_TOTAL_BASE_IRRF,
v_tab(i).VL_TOTAL_BASE_ISS,
v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
v_tab(i).VL_TOTAL_BASE_PIS_RET,
v_tab(i).VL_TOTAL_BASE_SENAT,
v_tab(i).VL_TOTAL_BASE_SEST,
v_tab(i).VL_TOTAL_BASE_STF,
v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_BASE_STF_IDO,
v_tab(i).VL_TOTAL_BASE_STT,
v_tab(i).VL_TOTAL_CARGA,
v_tab(i).VL_TOTAL_COFINS,
v_tab(i).VL_TOTAL_COFINS_RET,
v_tab(i).VL_TOTAL_CONTABIL,
v_tab(i).VL_TOTAL_CSLL_RET,
v_tab(i).VL_TOTAL_FATURADO,
v_tab(i).VL_TOTAL_FCP,
v_tab(i).VL_TOTAL_FCP_ST,
v_tab(i).VL_TOTAL_ICMS,
v_tab(i).VL_TOTAL_ICMS_DIFA,
v_tab(i).VL_TOTAL_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_ICMS_PART_REM,
v_tab(i).VL_TOTAL_II,
v_tab(i).VL_TOTAL_INSS,
v_tab(i).VL_TOTAL_INSS_RET,
v_tab(i).VL_TOTAL_INSS_RET_PER,
v_tab(i).VL_TOTAL_IOF,
v_tab(i).VL_TOTAL_IPI,
v_tab(i).VL_TOTAL_IRRF,
v_tab(i).VL_TOTAL_ISENTO_ICMS,
v_tab(i).VL_TOTAL_ISS,
v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_MAT_TERC,
v_tab(i).VL_TOTAL_OUTROS_ICMS,
v_tab(i).VL_TOTAL_PIS_PASEP,
v_tab(i).VL_TOTAL_PIS_RET,
v_tab(i).VL_TOTAL_SENAT,
v_tab(i).VL_TOTAL_SEST,
v_tab(i).VL_TOTAL_STF,
v_tab(i).VL_TOTAL_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_STF_IDO,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
v_tab(i).VL_TOTAL_STT,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TROCO,
v_tab(i).VL_TX_RED,
v_tab(i).VOO_CODIGO,
v_tab(i).VT_CODIGO,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_dof
				     set AGENTE = v_tab(i).AGENTE,
                          ANO_MES_EMISSAO = v_tab(i).ANO_MES_EMISSAO,
                          ANO_VEICULO = v_tab(i).ANO_VEICULO,
                          AUTENTICACAO_DIGITAL = v_tab(i).AUTENTICACAO_DIGITAL,
                          CAD_ESPECIFICO_INSS_CEI = v_tab(i).CAD_ESPECIFICO_INSS_CEI,
                          CERT_VEICULO = v_tab(i).CERT_VEICULO,
                          CERT_VEICULO_UF = v_tab(i).CERT_VEICULO_UF,
                          CFOP_CODIGO = v_tab(i).CFOP_CODIGO,
                          CFPS = v_tab(i).CFPS,
                          CNPJ_COL = v_tab(i).CNPJ_COL,
                          CNPJ_CRED_CARTAO = v_tab(i).CNPJ_CRED_CARTAO,
                          CNPJ_ENTG = v_tab(i).CNPJ_ENTG,
                          CNX = v_tab(i).CNX,
                          COBRANCA_LOC_CODIGO = v_tab(i).COBRANCA_LOC_CODIGO,
                          COBRANCA_PFJ_CODIGO = v_tab(i).COBRANCA_PFJ_CODIGO,
                          COD_AUT = v_tab(i).COD_AUT,
                          COD_GRUPO_TENSAO = v_tab(i).COD_GRUPO_TENSAO,
                          COD_IATA_FIM = v_tab(i).COD_IATA_FIM,
                          COD_IATA_INI = v_tab(i).COD_IATA_INI,
                          CODIGO_CONSUMO = v_tab(i).CODIGO_CONSUMO,
                          COD_LINHA = v_tab(i).COD_LINHA,
                          COD_MOTIVO_CANC_SYN = v_tab(i).COD_MOTIVO_CANC_SYN,
                          COD_MUN_COL = v_tab(i).COD_MUN_COL,
                          COD_MUN_ENTG = v_tab(i).COD_MUN_ENTG,
                          COD_OPER_CARTAO = v_tab(i).COD_OPER_CARTAO,
                          COD_REG_ESP_TRIBUTACAO = v_tab(i).COD_REG_ESP_TRIBUTACAO,
                          COMP_PRESTACAO_NOME = v_tab(i).COMP_PRESTACAO_NOME,
                          COMP_PRESTACAO_VALOR = v_tab(i).COMP_PRESTACAO_VALOR,
                          CONSIG_PFJ_CODIGO = v_tab(i).CONSIG_PFJ_CODIGO,
                          CONTA_CONTABIL = v_tab(i).CONTA_CONTABIL,
                          CPAG_CODIGO = v_tab(i).CPAG_CODIGO,
                          CPF = v_tab(i).CPF,
                          CRT_CODIGO = v_tab(i).CRT_CODIGO,
                          CTE_LOCALIZADOR_ORIGINAL = v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
                          CTRL_CONTEUDO = v_tab(i).CTRL_CONTEUDO,
                          CTRL_DT_INCLUSAO = v_tab(i).CTRL_DT_INCLUSAO,
                          CTRL_DT_ULT_ALTERACAO = v_tab(i).CTRL_DT_ULT_ALTERACAO,
                          CTRL_ENTRADA_ST = v_tab(i).CTRL_ENTRADA_ST,
                          CTRL_FASE = v_tab(i).CTRL_FASE,
                          CTRL_FASE_FISCAL = v_tab(i).CTRL_FASE_FISCAL,
                          CTRL_OBS_CALCULO = v_tab(i).CTRL_OBS_CALCULO,
                          CTRL_OBS_ENQ = v_tab(i).CTRL_OBS_ENQ,
                          CTRL_OBS_RAT_FRETE = v_tab(i).CTRL_OBS_RAT_FRETE,
                          CTRL_OBS_RAT_M = v_tab(i).CTRL_OBS_RAT_M,
                          CTRL_OBS_RAT_ODA = v_tab(i).CTRL_OBS_RAT_ODA,
                          CTRL_OBS_RAT_P = v_tab(i).CTRL_OBS_RAT_P,
                          CTRL_OBS_RAT_S = v_tab(i).CTRL_OBS_RAT_S,
                          CTRL_OBS_RAT_SEGURO = v_tab(i).CTRL_OBS_RAT_SEGURO,
                          CTRL_OBS_TOT_M = v_tab(i).CTRL_OBS_TOT_M,
                          CTRL_OBS_TOT_O = v_tab(i).CTRL_OBS_TOT_O,
                          CTRL_OBS_TOT_P = v_tab(i).CTRL_OBS_TOT_P,
                          CTRL_OBS_TOT_PESO = v_tab(i).CTRL_OBS_TOT_PESO,
                          CTRL_OBS_TOT_S = v_tab(i).CTRL_OBS_TOT_S,
                          CTRL_OBS_VL_FISCAL = v_tab(i).CTRL_OBS_VL_FISCAL,
                          CTRL_OBS_VL_PARCELAS = v_tab(i).CTRL_OBS_VL_PARCELAS,
                          CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
                          CTRL_PROCESSAMENTO = v_tab(i).CTRL_PROCESSAMENTO,
                          CTRL_SAIDA_ST = v_tab(i).CTRL_SAIDA_ST,
                          CTRL_SITUACAO_DOF = v_tab(i).CTRL_SITUACAO_DOF,
                          C4_IND_TEM_REAL = v_tab(i).C4_IND_TEM_REAL,
                          DEM_PTA = v_tab(i).DEM_PTA,
                          DESC_TITULO = v_tab(i).DESC_TITULO,
                          DESTINATARIO_CTE_LOC_CODIGO = v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
                          DESTINATARIO_CTE_PFJ_CODIGO = v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
                          DESTINATARIO_LOC_CODIGO = v_tab(i).DESTINATARIO_LOC_CODIGO,
                          DESTINATARIO_PFJ_CODIGO = v_tab(i).DESTINATARIO_PFJ_CODIGO,
                          DEST_PRINC_EST_CODIGO = v_tab(i).DEST_PRINC_EST_CODIGO,
                          DH_EMISSAO = v_tab(i).DH_EMISSAO,
                          DH_PROCESS_MANAGER = v_tab(i).DH_PROCESS_MANAGER,
                          DT_CONVERSAO_MOEDA_NAC = v_tab(i).DT_CONVERSAO_MOEDA_NAC,
                          DT_EMISSAO_TOMADOR = v_tab(i).DT_EMISSAO_TOMADOR,
                          DT_EXE_SERV = v_tab(i).DT_EXE_SERV,
                          DT_FATO_GERADOR_ATE = v_tab(i).DT_FATO_GERADOR_ATE,
                          DT_FATO_GERADOR_IMPOSTO = v_tab(i).DT_FATO_GERADOR_IMPOSTO,
                          DT_FIM_PROGRAMACAO_ENTREGA = v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
                          DT_INI_PROGRAMACAO_ENTREGA = v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
                          DT_LEITURA = v_tab(i).DT_LEITURA,
                          DT_LIMITE_EMISSAO = v_tab(i).DT_LIMITE_EMISSAO,
                          DT_LIMITE_SAIDA = v_tab(i).DT_LIMITE_SAIDA,
                          DT_PREV_CHEGADA = v_tab(i).DT_PREV_CHEGADA,
                          DT_PREV_EMBARQUE = v_tab(i).DT_PREV_EMBARQUE,
                          DT_RECEBIMENTO_DESTINO = v_tab(i).DT_RECEBIMENTO_DESTINO,
                          DT_REF_CALC_IMP = v_tab(i).DT_REF_CALC_IMP,
                          DT_REF_CPAG = v_tab(i).DT_REF_CPAG,
                          DT_REFER_EXT = v_tab(i).DT_REFER_EXT,
                          DT_ROMANEIO = v_tab(i).DT_ROMANEIO,
                          DT_SAIDA_MERCADORIAS = v_tab(i).DT_SAIDA_MERCADORIAS,
                          EDOF_CODIGO = v_tab(i).EDOF_CODIGO,
                          EDOF_CODIGO_RES = v_tab(i).EDOF_CODIGO_RES,
                          EMITENTE_LOC_CODIGO = v_tab(i).EMITENTE_LOC_CODIGO,
                          EMITENTE_PFJ_CODIGO = v_tab(i).EMITENTE_PFJ_CODIGO,
                          ENTREGA_LOC_CODIGO = v_tab(i).ENTREGA_LOC_CODIGO,
                          ENTREGA_PFJ_CODIGO = v_tab(i).ENTREGA_PFJ_CODIGO,
                          ESPECIALIZACAO = v_tab(i).ESPECIALIZACAO,
                          FATOR_FINANCIAMENTO = v_tab(i).FATOR_FINANCIAMENTO,
                          FERROV_SUBST_LOC_CODIGO = v_tab(i).FERROV_SUBST_LOC_CODIGO,
                          FERROV_SUBST_PFJ_CODIGO = v_tab(i).FERROV_SUBST_PFJ_CODIGO,
                          FORMA_PAGTO_TRANSPORTE = v_tab(i).FORMA_PAGTO_TRANSPORTE,
                          HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
                          HON_NATUREZA_OPERACAO = v_tab(i).HON_NATUREZA_OPERACAO,
                          HON_PRACA_PAGTO = v_tab(i).HON_PRACA_PAGTO,
                          HON_PRESTACAO_SERVICO = v_tab(i).HON_PRESTACAO_SERVICO,
                          HON_VALOR_DESCONTO = v_tab(i).HON_VALOR_DESCONTO,
                          HR_FIM_PROGRAMACAO_ENTREGA = v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
                          HR_INI_PROGRAMACAO_ENTREGA = v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
                          IE_COL = v_tab(i).IE_COL,
                          IE_ENTG = v_tab(i).IE_ENTG,
                          IM_COL = v_tab(i).IM_COL,
                          IM_ENTG = v_tab(i).IM_ENTG,
                          IND_ALTERACAO_TOMADOR = v_tab(i).IND_ALTERACAO_TOMADOR,
                          IND_BALSA = v_tab(i).IND_BALSA,
                          IND_BLOQUEADO = v_tab(i).IND_BLOQUEADO,
                          IND_CIOT = v_tab(i).IND_CIOT,
                          IND_CLASSE = v_tab(i).IND_CLASSE,
                          IND_COBRANCA_BANCARIA = v_tab(i).IND_COBRANCA_BANCARIA,
                          IND_CONTABILIZACAO = v_tab(i).IND_CONTABILIZACAO,
                          IND_EIX = v_tab(i).IND_EIX,
                          IND_ENTRADA_SAIDA = v_tab(i).IND_ENTRADA_SAIDA,
                          IND_ESCRITURACAO = v_tab(i).IND_ESCRITURACAO,
                          IND_EXPORTA_PARCELAS = v_tab(i).IND_EXPORTA_PARCELAS,
                          IND_F0 = v_tab(i).IND_F0,
                          IND_GABARITO = v_tab(i).IND_GABARITO,
                          IND_INCIDENCIA_COFINS_RET = v_tab(i).IND_INCIDENCIA_COFINS_RET,
                          IND_INCIDENCIA_CSLL_RET = v_tab(i).IND_INCIDENCIA_CSLL_RET,
                          IND_INCIDENCIA_INSS_RET = v_tab(i).IND_INCIDENCIA_INSS_RET,
                          IND_INCIDENCIA_IRRF = v_tab(i).IND_INCIDENCIA_IRRF,
                          IND_INCIDENCIA_PIS_RET = v_tab(i).IND_INCIDENCIA_PIS_RET,
                          IND_ISS_RETIDO_FONTE = v_tab(i).IND_ISS_RETIDO_FONTE,
                          IND_LOTACAO = v_tab(i).IND_LOTACAO,
                          IND_MOVIMENTA_ESTOQUE = v_tab(i).IND_MOVIMENTA_ESTOQUE,
                          IND_NAT_FRETE = v_tab(i).IND_NAT_FRETE,
                          IND_NAV = v_tab(i).IND_NAV,
                          IND_NFE_AJUSTE = v_tab(i).IND_NFE_AJUSTE,
                          IND_PRECO_AJUSTADO_CPAG = v_tab(i).IND_PRECO_AJUSTADO_CPAG,
                          IND_PRECOS_MOEDA_NAC = v_tab(i).IND_PRECOS_MOEDA_NAC,
                          IND_PRES_COMPRADOR = v_tab(i).IND_PRES_COMPRADOR,
                          IND_RATEIO = v_tab(i).IND_RATEIO,
                          IND_REPERCUSSAO_FISCAL = v_tab(i).IND_REPERCUSSAO_FISCAL,
                          IND_RESP_FRETE = v_tab(i).IND_RESP_FRETE,
                          IND_RESP_ISS_RET = v_tab(i).IND_RESP_ISS_RET,
                          IND_SIMPLES_ME_EPP = v_tab(i).IND_SIMPLES_ME_EPP,
                          IND_SIT_TRIBUT_DOF = v_tab(i).IND_SIT_TRIBUT_DOF,
                          IND_TFA = v_tab(i).IND_TFA,
                          IND_TIT = v_tab(i).IND_TIT,
                          IND_TOT_VL_FATURADO = v_tab(i).IND_TOT_VL_FATURADO,
                          IND_USA_IF_CALC_IMP = v_tab(i).IND_USA_IF_CALC_IMP,
                          IND_USA_IF_ESCRIT = v_tab(i).IND_USA_IF_ESCRIT,
                          IND_VEIC = v_tab(i).IND_VEIC,
                          IND_VERIFICA_PEDIDO = v_tab(i).IND_VERIFICA_PEDIDO,
                          LINHA = v_tab(i).LINHA,
                          LOCAL_EMBARQUE = v_tab(i).LOCAL_EMBARQUE,
                          LOCVIG_ID_DESTINATARIO = v_tab(i).LOCVIG_ID_DESTINATARIO,
                          LOCVIG_ID_REMETENTE = v_tab(i).LOCVIG_ID_REMETENTE,
                          MARCA_VEICULO = v_tab(i).MARCA_VEICULO,
                          MDOF_CODIGO = v_tab(i).MDOF_CODIGO,
                          MODELO_VEICULO = v_tab(i).MODELO_VEICULO,
                          MODO_EMISSAO = v_tab(i).MODO_EMISSAO,
                          MP_CODIGO = v_tab(i).MP_CODIGO,
                          MUN_COD_DESTINO = v_tab(i).MUN_COD_DESTINO,
                          MUN_CODIGO_ISS = v_tab(i).MUN_CODIGO_ISS,
                          MUN_COD_ORIGEM = v_tab(i).MUN_COD_ORIGEM,
                          MUN_PRES_SERVICO = v_tab(i).MUN_PRES_SERVICO,
                          NAT_VOLUME = v_tab(i).NAT_VOLUME,
                          NFE_COD_SITUACAO = v_tab(i).NFE_COD_SITUACAO,
                          NFE_COD_SITUACAO_SYN = v_tab(i).NFE_COD_SITUACAO_SYN,
                          NFE_CONTINGENCIA = v_tab(i).NFE_CONTINGENCIA,
                          NFE_DESC_SITUACAO = v_tab(i).NFE_DESC_SITUACAO,
                          NFE_ENVIADA = v_tab(i).NFE_ENVIADA,
                          NFE_HORA_GMT_EMI = v_tab(i).NFE_HORA_GMT_EMI,
                          NFE_JUSTIFICATIVA_CANCELAMENTO= v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
                          NFE_LOCALIZADOR = v_tab(i).NFE_LOCALIZADOR,
                          NFE_PROTOCOLO = v_tab(i).NFE_PROTOCOLO,
                          NF_IMPRESSORA_PADRAO = v_tab(i).NF_IMPRESSORA_PADRAO,
                          NFORM_FINAL = v_tab(i).NFORM_FINAL,
                          NFORM_INICIAL = v_tab(i).NFORM_INICIAL,
                          NOME_MOTORISTA = v_tab(i).NOME_MOTORISTA,
                          NOP_CODIGO = v_tab(i).NOP_CODIGO,
                          NUM_APOLICE_SEGURO = v_tab(i).NUM_APOLICE_SEGURO,
                          NUM_AUTOR_OPER_CARTAO = v_tab(i).NUM_AUTOR_OPER_CARTAO,
                          NUM_CONTAINER = v_tab(i).NUM_CONTAINER,
                          NUM_EMBARQUE = v_tab(i).NUM_EMBARQUE,
                          NUMERO = v_tab(i).NUMERO,
                          NUMERO_ATE = v_tab(i).NUMERO_ATE,
                          NUMERO_CONTRATO_FERROVIARIO = v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
                          NUM_FIM_RES = v_tab(i).NUM_FIM_RES,
                          NUM_INI_RES = v_tab(i).NUM_INI_RES,
                          NUM_PASSE = v_tab(i).NUM_PASSE,
                          NUM_PEDIDO = v_tab(i).NUM_PEDIDO,
                          NUM_PLACA_VEICULO = v_tab(i).NUM_PLACA_VEICULO,
                          NUM_REGISTRO_SECEX = v_tab(i).NUM_REGISTRO_SECEX,
                          NUM_RE_SISCOMEX = v_tab(i).NUM_RE_SISCOMEX,
                          NUM_ROMANEIO = v_tab(i).NUM_ROMANEIO,
                          NUM_TITULO = v_tab(i).NUM_TITULO,
                          NUM_VEICULO = v_tab(i).NUM_VEICULO,
                          NUM_VIAGEM = v_tab(i).NUM_VIAGEM,
                          NUM_VOLUME_COMP = v_tab(i).NUM_VOLUME_COMP,
                          OBSERVACAO = v_tab(i).OBSERVACAO,
                          ORDEM_EMBARQUE = v_tab(i).ORDEM_EMBARQUE,
                          OTM = v_tab(i).OTM,
                          PERC_AJUSTE_PRECO_TOTAL_M = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
                          PERC_AJUSTE_PRECO_TOTAL_P = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
                          PERC_AJUSTE_PRECO_TOTAL_S = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
                          PESO_BRT_COMP = v_tab(i).PESO_BRT_COMP,
                          PESO_BRUTO_KG = v_tab(i).PESO_BRUTO_KG,
                          PESO_LIQ_COMP = v_tab(i).PESO_LIQ_COMP,
                          PESO_LIQUIDO_KG = v_tab(i).PESO_LIQUIDO_KG,
                          PESO_REEMBALAGEM_KG = v_tab(i).PESO_REEMBALAGEM_KG,
                          PFJ_CODIGO_REDESPACHANTE = v_tab(i).PFJ_CODIGO_REDESPACHANTE,
                          PFJVIG_ID_DESTINATARIO = v_tab(i).PFJVIG_ID_DESTINATARIO,
                          PFJVIG_ID_REMETENTE = v_tab(i).PFJVIG_ID_REMETENTE,
                          PLACA_VEICULO_UF_CODIGO = v_tab(i).PLACA_VEICULO_UF_CODIGO,
                          POLT_CAB = v_tab(i).POLT_CAB,
                          PORTADOR_BANCO_NUM = v_tab(i).PORTADOR_BANCO_NUM,
                          PRECO_TOTAL_M = v_tab(i).PRECO_TOTAL_M,
                          PRECO_TOTAL_O = v_tab(i).PRECO_TOTAL_O,
                          PRECO_TOTAL_P = v_tab(i).PRECO_TOTAL_P,
                          PRECO_TOTAL_S = v_tab(i).PRECO_TOTAL_S,
                          PRODUTO_PREDOMINANTE = v_tab(i).PRODUTO_PREDOMINANTE,
                          QTD_CANC = v_tab(i).QTD_CANC,
                          QTD_CANC_RES = v_tab(i).QTD_CANC_RES,
                          QTD_CONSUMO = v_tab(i).QTD_CONSUMO,
                          QTD_PASS_ORIG = v_tab(i).QTD_PASS_ORIG,
                          QTD_PRESTACOES = v_tab(i).QTD_PRESTACOES,
                          REDESPACHANTE_LOC_CODIGO = v_tab(i).REDESPACHANTE_LOC_CODIGO,
                          REMETENTE_LOC_CODIGO = v_tab(i).REMETENTE_LOC_CODIGO,
                          REMETENTE_PFJ_CODIGO = v_tab(i).REMETENTE_PFJ_CODIGO,
                          REM_PRINC_EST_CODIGO = v_tab(i).REM_PRINC_EST_CODIGO,
                          RESP_FRETE_REDESP = v_tab(i).RESP_FRETE_REDESP,
                          RETIRADA_LOC_CODIGO = v_tab(i).RETIRADA_LOC_CODIGO,
                          RETIRADA_PFJ_CODIGO = v_tab(i).RETIRADA_PFJ_CODIGO,
                          RETIRA_MERC_DESTINO = v_tab(i).RETIRA_MERC_DESTINO,
                          REVISAO = v_tab(i).REVISAO,
                          RNTC = v_tab(i).RNTC,
                          RNTC_COMP = v_tab(i).RNTC_COMP,
                          RNTC_COMP_2 = v_tab(i).RNTC_COMP_2,
                          SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
                          SERIE_SUBSERIE = v_tab(i).SERIE_SUBSERIE,
                          SERIE_SUBSERIE_RES = v_tab(i).SERIE_SUBSERIE_RES,
                          SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
                          SIS_CODIGO = v_tab(i).SIS_CODIGO,
                          SIS_CODIGO_CONTABILIZOU = v_tab(i).SIS_CODIGO_CONTABILIZOU,
                          SUBSTITUIDO_PFJ_CODIGO = v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
                          TEMPERATURA = v_tab(i).TEMPERATURA,
                          TIPO = v_tab(i).TIPO,
                          TIPO_CTE = v_tab(i).TIPO_CTE,
                          TIPO_DT_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
                          TIPO_HR_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
                          TIPO_PGTO = v_tab(i).TIPO_PGTO,
                          TIPO_SERVICO_CTE = v_tab(i).TIPO_SERVICO_CTE,
                          TIPO_TARIFA = v_tab(i).TIPO_TARIFA,
                          TIPO_TOMADOR = v_tab(i).TIPO_TOMADOR,
                          TIPO_TRAFEGO_FERROVIARIO = v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
                          TOMADOR_OUTROS_PFJ_CODIGO = v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
                          TP_ASSINANTE = v_tab(i).TP_ASSINANTE,
                          TP_INT_PAG = v_tab(i).TP_INT_PAG,
                          TP_LIGACAO = v_tab(i).TP_LIGACAO,
                          TRAF_MUTUO_FERROV_EMITENTE = v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
                          TRAF_MUTUO_FLUXO_FERROV = v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
                          TRAF_MUTUO_IDENTIF_TREM = v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
                          TRAF_MUTUO_RESP_FATURAMENTO = v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
                          TRANSF_DT_FATO_GER_IMPOSTO = v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
                          TRANSF_NOP_CODIGO_ENTRADA = v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
                          TRANSPORTADOR_LOC_CODIGO = v_tab(i).TRANSPORTADOR_LOC_CODIGO,
                          TRANSPORTADOR_PFJ_CODIGO = v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
                          TX_CONVERSAO_MOEDA_NAC = v_tab(i).TX_CONVERSAO_MOEDA_NAC,
                          TXT_INSTRUCAO_BLOQUETO = v_tab(i).TXT_INSTRUCAO_BLOQUETO,
                          UF_CODIGO_DESTINO = v_tab(i).UF_CODIGO_DESTINO,
                          UF_CODIGO_EMITENTE = v_tab(i).UF_CODIGO_EMITENTE,
                          UF_CODIGO_ENTREGA = v_tab(i).UF_CODIGO_ENTREGA,
                          UF_CODIGO_ORIGEM = v_tab(i).UF_CODIGO_ORIGEM,
                          UF_CODIGO_RETIRADA = v_tab(i).UF_CODIGO_RETIRADA,
                          UF_CODIGO_SUBST = v_tab(i).UF_CODIGO_SUBST,
                          UF_CODIGO_TRANSP = v_tab(i).UF_CODIGO_TRANSP,
                          UF_EMBARQUE = v_tab(i).UF_EMBARQUE,
                          UF_VEIC_COMP = v_tab(i).UF_VEIC_COMP,
                          UF_VEIC_COMP_2 = v_tab(i).UF_VEIC_COMP_2,
                          USUARIO_INCLUSAO = v_tab(i).USUARIO_INCLUSAO,
                          USUARIO_ULT_ALTERACAO = v_tab(i).USUARIO_ULT_ALTERACAO,
                          VEIC_DESCR = v_tab(i).VEIC_DESCR,
                          VEIC_ID_COMP = v_tab(i).VEIC_ID_COMP,
                          VEIC_ID_COMP_2 = v_tab(i).VEIC_ID_COMP_2,
                          VL_ABAT_NT = v_tab(i).VL_ABAT_NT,
                          VL_ADU = v_tab(i).VL_ADU,
                          VL_ADU_ICMS = v_tab(i).VL_ADU_ICMS,
                          VL_AJUSTE_PRECO_TOTAL_M = v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
                          VL_AJUSTE_PRECO_TOTAL_P = v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
                          VL_AJUSTE_PRECO_TOTAL_S = v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
                          VL_BASE_CT_STF = v_tab(i).VL_BASE_CT_STF,
                          VL_BASE_INSS_RET_PER = v_tab(i).VL_BASE_INSS_RET_PER,
                          VL_COFINS_ST = v_tab(i).VL_COFINS_ST,
                          VL_CT_STF = v_tab(i).VL_CT_STF,
                          VL_DESCONTO_NF = v_tab(i).VL_DESCONTO_NF,
                          VL_DESPACHO = v_tab(i).VL_DESPACHO,
                          VL_DESP_CAR_DESC = v_tab(i).VL_DESP_CAR_DESC,
                          VL_DESP_PORT = v_tab(i).VL_DESP_PORT,
                          VL_FISCAL_M = v_tab(i).VL_FISCAL_M,
                          VL_FISCAL_O = v_tab(i).VL_FISCAL_O,
                          VL_FISCAL_P = v_tab(i).VL_FISCAL_P,
                          VL_FISCAL_S = v_tab(i).VL_FISCAL_S,
                          VL_FRETE = v_tab(i).VL_FRETE,
                          VL_FRETE_FERR_SUBSTITUIDA = v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
                          VL_FRETE_LIQ = v_tab(i).VL_FRETE_LIQ,
                          VL_FRETE_MM = v_tab(i).VL_FRETE_MM,
                          VL_FRETE_TRAF_MUTUO = v_tab(i).VL_FRETE_TRAF_MUTUO,
                          VL_FRT_PV = v_tab(i).VL_FRT_PV,
                          VL_GRIS = v_tab(i).VL_GRIS,
                          VL_INSS = v_tab(i).VL_INSS,
                          VL_OUTRAS_DESPESAS = v_tab(i).VL_OUTRAS_DESPESAS,
                          VL_OUTROS_ABAT = v_tab(i).VL_OUTROS_ABAT,
                          VL_PEDAGIO = v_tab(i).VL_PEDAGIO,
                          VL_PESO_TX = v_tab(i).VL_PESO_TX,
                          VL_PIS_ST = v_tab(i).VL_PIS_ST,
                          VL_SEC_CAT = v_tab(i).VL_SEC_CAT,
                          VL_SEGURO = v_tab(i).VL_SEGURO,
                          VL_SERV_NT = v_tab(i).VL_SERV_NT,
                          VL_TAXA_ADV = v_tab(i).VL_TAXA_ADV,
                          VL_TAXA_TERRESTRE = v_tab(i).VL_TAXA_TERRESTRE,
                          VL_TERC = v_tab(i).VL_TERC,
                          VL_TFA = v_tab(i).VL_TFA,
                          VL_TOTAL_BASE_COFINS = v_tab(i).VL_TOTAL_BASE_COFINS,
                          VL_TOTAL_BASE_COFINS_RET = v_tab(i).VL_TOTAL_BASE_COFINS_RET,
                          VL_TOTAL_BASE_CSLL_RET = v_tab(i).VL_TOTAL_BASE_CSLL_RET,
                          VL_TOTAL_BASE_ICMS = v_tab(i).VL_TOTAL_BASE_ICMS,
                          VL_TOTAL_BASE_ICMS_DIFA = v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
                          VL_TOTAL_BASE_ICMS_FCP = v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
		                  VL_TOTAL_BASE_ICMS_FCP_ST = v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,				  
                          VL_TOTAL_BASE_ICMS_PART_DEST = v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
                          VL_TOTAL_BASE_ICMS_PART_REM = v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
                          VL_TOTAL_BASE_INSS = v_tab(i).VL_TOTAL_BASE_INSS,
                          VL_TOTAL_BASE_INSS_RET = v_tab(i).VL_TOTAL_BASE_INSS_RET,
                          VL_TOTAL_BASE_IPI = v_tab(i).VL_TOTAL_BASE_IPI,
                          VL_TOTAL_BASE_IRRF = v_tab(i).VL_TOTAL_BASE_IRRF,
                          VL_TOTAL_BASE_ISS = v_tab(i).VL_TOTAL_BASE_ISS,
                          VL_TOTAL_BASE_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
                          VL_TOTAL_BASE_PIS_PASEP = v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
                          VL_TOTAL_BASE_PIS_RET = v_tab(i).VL_TOTAL_BASE_PIS_RET,
                          VL_TOTAL_BASE_SENAT = v_tab(i).VL_TOTAL_BASE_SENAT,
                          VL_TOTAL_BASE_SEST = v_tab(i).VL_TOTAL_BASE_SEST,
                          VL_TOTAL_BASE_STF = v_tab(i).VL_TOTAL_BASE_STF,
                          VL_TOTAL_BASE_STF_FRONTEIRA = v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
                          VL_TOTAL_BASE_STF_IDO = v_tab(i).VL_TOTAL_BASE_STF_IDO,
                          VL_TOTAL_BASE_STT = v_tab(i).VL_TOTAL_BASE_STT,
                          VL_TOTAL_CARGA = v_tab(i).VL_TOTAL_CARGA,
                          VL_TOTAL_COFINS = v_tab(i).VL_TOTAL_COFINS,
                          VL_TOTAL_COFINS_RET = v_tab(i).VL_TOTAL_COFINS_RET,
                          VL_TOTAL_CONTABIL = v_tab(i).VL_TOTAL_CONTABIL,
                          VL_TOTAL_CSLL_RET = v_tab(i).VL_TOTAL_CSLL_RET,
                          VL_TOTAL_FATURADO = v_tab(i).VL_TOTAL_FATURADO,
                          VL_TOTAL_FCP = v_tab(i).VL_TOTAL_FCP,
                          VL_TOTAL_FCP_ST = v_tab(i).VL_TOTAL_FCP_ST,
                          VL_TOTAL_ICMS = v_tab(i).VL_TOTAL_ICMS,
                          VL_TOTAL_ICMS_DIFA = v_tab(i).VL_TOTAL_ICMS_DIFA,
                          VL_TOTAL_ICMS_PART_DEST = v_tab(i).VL_TOTAL_ICMS_PART_DEST,
                          VL_TOTAL_ICMS_PART_REM = v_tab(i).VL_TOTAL_ICMS_PART_REM,
                          VL_TOTAL_II = v_tab(i).VL_TOTAL_II,
                          VL_TOTAL_INSS = v_tab(i).VL_TOTAL_INSS,
                          VL_TOTAL_INSS_RET = v_tab(i).VL_TOTAL_INSS_RET,
                          VL_TOTAL_INSS_RET_PER = v_tab(i).VL_TOTAL_INSS_RET_PER,
                          VL_TOTAL_IOF = v_tab(i).VL_TOTAL_IOF,
                          VL_TOTAL_IPI = v_tab(i).VL_TOTAL_IPI,
                          VL_TOTAL_IRRF = v_tab(i).VL_TOTAL_IRRF,
                          VL_TOTAL_ISENTO_ICMS = v_tab(i).VL_TOTAL_ISENTO_ICMS,
                          VL_TOTAL_ISS = v_tab(i).VL_TOTAL_ISS,
                          VL_TOTAL_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
                          VL_TOTAL_MAT_TERC = v_tab(i).VL_TOTAL_MAT_TERC,
                          VL_TOTAL_OUTROS_ICMS = v_tab(i).VL_TOTAL_OUTROS_ICMS,
                          VL_TOTAL_PIS_PASEP = v_tab(i).VL_TOTAL_PIS_PASEP,
                          VL_TOTAL_PIS_RET = v_tab(i).VL_TOTAL_PIS_RET,
                          VL_TOTAL_SENAT = v_tab(i).VL_TOTAL_SENAT,
                          VL_TOTAL_SEST = v_tab(i).VL_TOTAL_SEST,
                          VL_TOTAL_STF = v_tab(i).VL_TOTAL_STF,
                          VL_TOTAL_STF_FRONTEIRA = v_tab(i).VL_TOTAL_STF_FRONTEIRA,
                          VL_TOTAL_STF_IDO = v_tab(i).VL_TOTAL_STF_IDO,
                          VL_TOTAL_STF_SUBSTITUIDO = v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
                          VL_TOTAL_STT = v_tab(i).VL_TOTAL_STT,
                          VL_TRIBUTAVEL_DIFA = v_tab(i).VL_TRIBUTAVEL_DIFA,
                          VL_TROCO = v_tab(i).VL_TROCO,
                          VL_TX_RED = v_tab(i).VL_TX_RED,
                          VOO_CODIGO = v_tab(i).VOO_CODIGO,
                          VT_CODIGO = v_tab(i).VT_CODIGO,
                          XINF_ID= v_tab(i).XINF_ID
				   where dof_import_numero = v_tab(i).dof_import_numero
				     and informante_est_codigo = v_tab(i).informante_est_codigo;
			  exception when others then
                     r_put_line('Erro ao alterar DOF DUP. - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_idf (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql            VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_idf%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_idf_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
     begin
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      exception when others then 
	         null;
              end;
      begin
     INSERT INTO COR_IDF (ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID)
   VALUES (v_tab(i).ALIQ_ANTECIPACAO,
v_tab(i).ALIQ_ANTECIP_ICMS,
v_tab(i).ALIQ_COFINS,
v_tab(i).ALIQ_COFINS_RET,
v_tab(i).ALIQ_COFINS_ST,
v_tab(i).ALIQ_CSLL_RET,
v_tab(i).ALIQ_DIFA,
v_tab(i).ALIQ_DIFA_ICMS_PART,
v_tab(i).ALIQ_ICMS,
v_tab(i).ALIQ_ICMS_DESC_L,
v_tab(i).ALIQ_ICMS_FCP,
v_tab(i).ALIQ_ICMS_FCPST,
v_tab(i).ALIQ_ICMS_PART_DEST,
v_tab(i).ALIQ_ICMS_PART_REM,
v_tab(i).ALIQ_ICMS_STF60,
v_tab(i).ALIQ_II,
v_tab(i).ALIQ_INSS,
v_tab(i).ALIQ_INSS_AE15_RET,
v_tab(i).ALIQ_INSS_AE20_RET,
v_tab(i).ALIQ_INSS_AE25_RET,
v_tab(i).ALIQ_INSS_RET,
v_tab(i).ALIQ_IPI,
v_tab(i).ALIQ_ISS,
v_tab(i).ALIQ_ISS_BITRIBUTADO,
v_tab(i).ALIQ_PIS,
v_tab(i).ALIQ_PIS_RET,
v_tab(i).ALIQ_PIS_ST,
v_tab(i).ALIQ_SENAT,
v_tab(i).ALIQ_SEST,
v_tab(i).ALIQ_STF,
v_tab(i).ALIQ_STT,
v_tab(i).AM_CODIGO,
v_tab(i).CCA_CODIGO,
v_tab(i).CEST_CODIGO,
v_tab(i).CFOP_CODIGO,
v_tab(i).CHASSI_VEIC,
v_tab(i).CLI_CODIGO,
v_tab(i).CNPJ_FABRICANTE,
v_tab(i).CNPJ_PAR,
v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
v_tab(i).CNPJ_SUBEMPREITEIRO,
v_tab(i).COD_AREA,
v_tab(i).COD_BC_CREDITO,
v_tab(i).COD_BENEFICIO_FISCAL,
v_tab(i).COD_CCUS,
v_tab(i).COD_CONJ_KIT,
v_tab(i).COD_FISC_SERV_MUN,
v_tab(i).COD_GTIN,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).COD_IBGE,
v_site,
v_tab(i).CODIGO_ISS_MUNICIPIO,
v_tab(i).CODIGO_RETENCAO,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_INSS,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).COD_NVE,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPF_PAR,
v_tab(i).CSOSN_CODIGO,
v_tab(i).CTRL_DESCARGA,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_OBS_CALC_ICMS,
v_tab(i).CTRL_OBS_CALC_INSS,
v_tab(i).CTRL_OBS_CALC_IPI,
v_tab(i).CTRL_OBS_CALC_IRRF,
v_tab(i).CTRL_OBS_CALC_ISS,
v_tab(i).CTRL_OBS_CALC_STF,
v_tab(i).CTRL_OBS_CALC_STT,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_SETUP_ICMS,
v_tab(i).CTRL_OBS_SETUP_IPI,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_PRECO,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_TOLERANCIA_PED,
v_tab(i).CUSTO_ADICAO,
v_tab(i).CUSTO_REDUCAO,
v_tab(i).C4_SALDO_REF,
v_tab(i).DESCRICAO_ARMA,
v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
v_tab(i).DESONERACAO_CODIGO,
v_dof,
v_sequence,
v_tab(i).DT_DI,
v_tab(i).DT_FAB_MED,
v_tab(i).DT_FIN_SERV,
v_tab(i).DT_INI_SERV,
v_tab(i).DT_REF_CALC_IMP_IDF,
v_tab(i).DT_VAL,
v_tab(i).EMERC_CODIGO,
v_tab(i).ENQ_IPI_CODIGO,
v_tab(i).ENTSAI_UNI_CODIGO,
v_tab(i).ESTOQUE_UNI_CODIGO,
v_tab(i).FAT_BASE_CALCULO_STF,
v_tab(i).FAT_CONV_UNI_ESTOQUE,
v_tab(i).FAT_CONV_UNI_FISCAL,
v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
v_tab(i).FCI_NUMERO,
v_tab(i).FIN_CODIGO,
v_tab(i).FLAG_SUFRAMA,
v_tab(i).HON_COFINS_RET,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_CSLL_RET,
v_tab(i).HON_PIS_RET,
v_tab(i).IDF_NUM,
v_tab(i).IDF_NUM_PAI,
v_tab(i).IDF_TEXTO_COMPLEMENTAR,
v_tab(i).IE_PAR,
v_tab(i).IM_SUBCONTRATACAO,
v_tab(i).IND_ANTECIP_ICMS,
v_tab(i).IND_ARM,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_COFINS_ST,
v_tab(i).IND_COMPROVA_OPERACAO,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_ESCALA_RELEVANTE,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXIGIBILIDADE_ISS,
v_tab(i).IND_FCP_ST,
v_tab(i).IND_INCENTIVO_FISCAL_ISS,
v_tab(i).IND_INCIDENCIA_COFINS,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_COFINS_ST,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_ICMS,
v_tab(i).IND_INCIDENCIA_INSS,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IPI,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_ISS,
v_tab(i).IND_INCIDENCIA_PIS,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_INCIDENCIA_PIS_ST,
v_tab(i).IND_INCIDENCIA_SENAT,
v_tab(i).IND_INCIDENCIA_SEST,
v_tab(i).IND_INCIDENCIA_STF,
v_tab(i).IND_INCIDENCIA_STT,
v_tab(i).IND_IPI_BASE_ICMS,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LAN_FISCAL_ICMS,
v_tab(i).IND_LAN_FISCAL_IPI,
v_tab(i).IND_LAN_FISCAL_STF,
v_tab(i).IND_LAN_FISCAL_STT,
v_tab(i).IND_LAN_IMP,
v_tab(i).IND_MED,
v_tab(i).IND_MOVIMENTACAO_CIAP,
v_tab(i).IND_MOVIMENTACAO_CPC,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_MP_DO_BEM,
v_tab(i).IND_NAT_FRT_PISCOFINS,
v_tab(i).IND_OUTROS_ICMS,
v_tab(i).IND_OUTROS_IPI,
v_tab(i).IND_PAUTA_STF_MVA_MP,
v_tab(i).IND_PIS_ST,
v_tab(i).IND_SERV,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).IND_VL_FISC_VL_CONT,
v_tab(i).IND_VL_FISC_VL_FAT,
v_tab(i).IND_VL_ICMS_NO_PRECO,
v_tab(i).IND_VL_ICMS_VL_CONT,
v_tab(i).IND_VL_ICMS_VL_FAT,
v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
v_tab(i).IND_ZFM_ALC,
v_tab(i).LOTE_MED,
v_tab(i).MERC_CODIGO,
v_tab(i).MOD_BASE_ICMS_CODIGO,
v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
v_tab(i).MUN_CODIGO,
v_tab(i).NAT_REC_PISCOFINS,
v_tab(i).NAT_REC_PISCOFINS_DESCR,
v_tab(i).NBM_CODIGO,
v_tab(i).NBS_CODIGO,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_ARM,
v_tab(i).NUM_CANO,
v_tab(i).NUM_DI,
v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
v_tab(i).NUM_TANQUE,
v_tab(i).OM_CODIGO,
v_tab(i).ORD_IMPRESSAO,
v_tab(i).PARTICIPANTE_PFJ_CODIGO,
v_tab(i).PAUTA_FLUT_ICMS,
v_tab(i).PEDIDO_NUMERO,
v_tab(i).PEDIDO_NUMERO_ITEM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
v_tab(i).PERC_ICMS_PART_DEST,
v_tab(i).PERC_ICMS_PART_REM,
v_tab(i).PERC_IRRF,
v_tab(i).PERC_ISENTO_ICMS,
v_tab(i).PERC_ISENTO_IPI,
v_tab(i).PERC_OUTROS_ABAT,
v_tab(i).PERC_OUTROS_ICMS,
v_tab(i).PERC_OUTROS_IPI,
v_tab(i).PERC_PART_CI,
v_tab(i).PERC_TRIBUTAVEL_COFINS,
v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
v_tab(i).PERC_TRIBUTAVEL_ICMS,
v_tab(i).PERC_TRIBUTAVEL_INSS,
v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
v_tab(i).PERC_TRIBUTAVEL_IPI,
v_tab(i).PERC_TRIBUTAVEL_IRRF,
v_tab(i).PERC_TRIBUTAVEL_ISS,
v_tab(i).PERC_TRIBUTAVEL_PIS,
v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
v_tab(i).PERC_TRIBUTAVEL_SENAT,
v_tab(i).PERC_TRIBUTAVEL_SEST,
v_tab(i).PERC_TRIBUTAVEL_STF,
v_tab(i).PERC_TRIBUTAVEL_STT,
v_tab(i).PER_FISCAL,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PFJ_CODIGO_FORNECEDOR,
v_tab(i).PFJ_CODIGO_TERCEIRO,
v_tab(i).PRECO_NET_UNIT,
v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
v_tab(i).PRECO_TOTAL,
v_tab(i).PRECO_UNITARIO,
v_tab(i).PRES_CODIGO,
v_tab(i).QTD,
v_tab(i).QTD_BASE_COFINS,
v_tab(i).QTD_BASE_PIS,
v_tab(i).QTD_EMB,
v_tab(i).QTD_KIT,
v_tab(i).REVISAO,
v_tab(i).SELO_CODIGO,
v_tab(i).SELO_QTDE,
v_tab(i).SISS_CODIGO,
v_tab(i).STA_CODIGO,
v_tab(i).STC_CODIGO,
v_tab(i).STI_CODIGO,
v_tab(i).STM_CODIGO,
v_tab(i).STN_CODIGO,
v_tab(i).STP_CODIGO,
v_tab(i).SUBCLASSE_IDF,
v_tab(i).TERMINAL,
v_tab(i).TIPO_COMPLEMENTO,
v_tab(i).TIPO_OPER_VEIC,
v_tab(i).TIPO_PROD_MED,
v_tab(i).TIPO_RECEITA,
v_tab(i).TIPO_STF,
v_tab(i).UF_PAR,
v_tab(i).UNI_CODIGO_FISCAL,
v_tab(i).UNI_FISCAL_CODIGO,
v_tab(i).VL_ABAT_LEGAL_INSS_RET,
v_tab(i).VL_ABAT_LEGAL_IRRF,
v_tab(i).VL_ABAT_LEGAL_ISS,
v_tab(i).VL_ADICIONAL_RET_AE,
v_tab(i).VL_AJUSTE_PRECO_TOTAL,
v_tab(i).VL_ALIMENTACAO,
v_tab(i).VL_ALIQ_COFINS,
v_tab(i).VL_ALIQ_PIS,
v_tab(i).VL_ANTECIPACAO,
v_tab(i).VL_ANTECIP_ICMS,
v_tab(i).VL_BASE_COFINS,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_COFINS_ST,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_ICMS,
v_tab(i).VL_BASE_ICMS_DESC_L,
v_tab(i).VL_BASE_ICMS_FCP,
v_tab(i).VL_BASE_ICMS_FCPST,
v_tab(i).VL_BASE_ICMS_PART_DEST,
v_tab(i).VL_BASE_ICMS_PART_REM,
v_tab(i).VL_BASE_II,
v_tab(i).VL_BASE_INSS,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IPI,
v_tab(i).VL_BASE_IRRF,
v_tab(i).VL_BASE_ISS,
v_tab(i).VL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_BASE_PIS,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_PIS_ST,
v_tab(i).VL_BASE_SENAT,
v_tab(i).VL_BASE_SEST,
v_tab(i).VL_BASE_STF,
v_tab(i).VL_BASE_STF_FRONTEIRA,
v_tab(i).VL_BASE_STF_IDO,
v_tab(i).VL_BASE_STF60,
v_tab(i).VL_BASE_STT,
v_tab(i).VL_BASE_SUFRAMA,
v_tab(i).VL_COFINS,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CONTABIL,
v_tab(i).VL_CRED_COFINS_REC_EXPO,
v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
v_tab(i).VL_CRED_COFINS_REC_TRIB,
v_tab(i).VL_CRED_PIS_REC_EXPO,
v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
v_tab(i).VL_CRED_PIS_REC_TRIB,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_INSS,
v_tab(i).VL_DEDUCAO_IRRF_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESC_CP,
v_tab(i).VL_DIFA,
v_tab(i).VL_FATURADO,
v_tab(i).VL_FISCAL,
v_tab(i).VL_GILRAT,
v_tab(i).VL_ICMS,
v_tab(i).VL_ICMS_DESC_L,
v_tab(i).VL_ICMS_FCP,
v_tab(i).VL_ICMS_FCPST,
v_tab(i).VL_ICMS_PART_DEST,
v_tab(i).VL_ICMS_PART_REM,
v_tab(i).VL_ICMS_PROPRIO_RECUP,
v_tab(i).VL_ICMS_SIMPLES_NAC,
v_tab(i).VL_ICMS_ST_RECUP,
v_tab(i).VL_II,
v_tab(i).VL_IMP_FCI,
v_tab(i).VL_IMPOSTO_COFINS,
v_tab(i).VL_IMPOSTO_PIS,
v_tab(i).VL_INSS,
v_tab(i).VL_INSS_AE15_NAO_RET,
v_tab(i).VL_INSS_AE15_RET,
v_tab(i).VL_INSS_AE20_NAO_RET,
v_tab(i).VL_INSS_AE20_RET,
v_tab(i).VL_INSS_AE25_NAO_RET,
v_tab(i).VL_INSS_AE25_RET,
v_tab(i).VL_INSS_NAO_RET,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IOF,
v_tab(i).VL_IPI,
v_tab(i).VL_IRRF,
v_tab(i).VL_IRRF_NAO_RET,
v_tab(i).VL_ISENTO_ICMS,
v_tab(i).VL_ISENTO_IPI,
v_tab(i).VL_ISS,
v_tab(i).VL_ISS_BITRIBUTADO,
v_tab(i).VL_ISS_DESC_CONDICIONADO,
v_tab(i).VL_ISS_DESC_INCONDICIONADO,
v_tab(i).VL_ISS_OUTRAS_RETENCOES,
v_tab(i).VL_MATERIAS_EQUIP,
v_tab(i).VL_NAO_RETIDO_CP,
v_tab(i).VL_NAO_RETIDO_CP_AE,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_OUTROS_ICMS,
v_tab(i).VL_OUTROS_IMPOSTOS,
v_tab(i).VL_OUTROS_IPI,
v_tab(i).VL_PIS,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_RATEIO_AJUSTE_PRECO,
v_tab(i).VL_RATEIO_BASE_CT_STF,
v_tab(i).VL_RATEIO_CT_STF,
v_tab(i).VL_RATEIO_FRETE,
v_tab(i).VL_RATEIO_ODA,
v_tab(i).VL_RATEIO_SEGURO,
v_tab(i).VL_RECUPERADO,
v_tab(i).VL_RETIDO_SUBEMPREITADA,
v_tab(i).VL_SENAR,
v_tab(i).VL_SENAT,
v_tab(i).VL_SERVICO_AE15,
v_tab(i).VL_SERVICO_AE20,
v_tab(i).VL_SERVICO_AE25,
v_tab(i).VL_SEST,
v_tab(i).VL_STF,
v_tab(i).VL_STF_FRONTEIRA,
v_tab(i).VL_STF_IDO,
v_tab(i).VL_ST_FRONTEIRA,
v_tab(i).VL_STF60,
v_tab(i).VL_STT,
v_tab(i).VL_SUFRAMA,
v_tab(i).VL_TAB_MAX,
v_tab(i).VL_TRANSPORTE,
v_tab(i).VL_TRIBUTAVEL_ANTEC,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TRIBUTAVEL_ICMS,
v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
v_tab(i).VL_TRIBUTAVEL_IPI,
v_tab(i).VL_TRIBUTAVEL_STF,
v_tab(i).VL_TRIBUTAVEL_STT,
v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
v_tab(i).XCON_ID,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_idf
				    set
ALIQ_ANTECIPACAO =  v_tab(i).ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS =  v_tab(i).ALIQ_ANTECIP_ICMS,
ALIQ_COFINS =  v_tab(i).ALIQ_COFINS,
ALIQ_COFINS_RET =  v_tab(i).ALIQ_COFINS_RET,
ALIQ_COFINS_ST =  v_tab(i).ALIQ_COFINS_ST,
ALIQ_CSLL_RET =  v_tab(i).ALIQ_CSLL_RET,
ALIQ_DIFA =  v_tab(i).ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART =  v_tab(i).ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS =  v_tab(i).ALIQ_ICMS,
ALIQ_ICMS_DESC_L =  v_tab(i).ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP =  v_tab(i).ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST =  v_tab(i).ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST =  v_tab(i).ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM =  v_tab(i).ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60 =  v_tab(i).ALIQ_ICMS_STF60,
ALIQ_II =  v_tab(i).ALIQ_II,
ALIQ_INSS =  v_tab(i).ALIQ_INSS,
ALIQ_INSS_AE15_RET =  v_tab(i).ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET =  v_tab(i).ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET =  v_tab(i).ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET =  v_tab(i).ALIQ_INSS_RET,
ALIQ_IPI =  v_tab(i).ALIQ_IPI,
ALIQ_ISS =  v_tab(i).ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO =  v_tab(i).ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS =  v_tab(i).ALIQ_PIS,
ALIQ_PIS_RET =  v_tab(i).ALIQ_PIS_RET,
ALIQ_PIS_ST =  v_tab(i).ALIQ_PIS_ST,
ALIQ_SENAT =  v_tab(i).ALIQ_SENAT,
ALIQ_SEST =  v_tab(i).ALIQ_SEST,
ALIQ_STF =  v_tab(i).ALIQ_STF,
ALIQ_STT =  v_tab(i).ALIQ_STT,
AM_CODIGO =  v_tab(i).AM_CODIGO,
CCA_CODIGO =  v_tab(i).CCA_CODIGO,
CEST_CODIGO =  v_tab(i).CEST_CODIGO,
CFOP_CODIGO =  v_tab(i).CFOP_CODIGO,
CHASSI_VEIC =  v_tab(i).CHASSI_VEIC,
CLI_CODIGO =  v_tab(i).CLI_CODIGO,
CNPJ_FABRICANTE =  v_tab(i).CNPJ_FABRICANTE,
CNPJ_PAR =  v_tab(i).CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT =  v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO =  v_tab(i).CNPJ_SUBEMPREITEIRO,
COD_AREA =  v_tab(i).COD_AREA,
COD_BC_CREDITO =  v_tab(i).COD_BC_CREDITO,
COD_BENEFICIO_FISCAL =  v_tab(i).COD_BENEFICIO_FISCAL,
COD_CCUS =  v_tab(i).COD_CCUS,
COD_CONJ_KIT =  v_tab(i).COD_CONJ_KIT,
COD_FISC_SERV_MUN =  v_tab(i).COD_FISC_SERV_MUN,
COD_GTIN =  v_tab(i).COD_GTIN,
COD_IATA_FIM =  v_tab(i).COD_IATA_FIM,
COD_IATA_INI =  v_tab(i).COD_IATA_INI,
COD_IBGE =  v_tab(i).COD_IBGE,
CODIGO_ISS_MUNICIPIO =  v_tab(i).CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO =  v_tab(i).CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS =  v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL =  v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS =  v_tab(i).CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC =  v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS =  v_tab(i).CODIGO_RETENCAO_PIS,
COD_NVE =  v_tab(i).COD_NVE,
CONTA_CONTABIL =  v_tab(i).CONTA_CONTABIL,
CPF_PAR =  v_tab(i).CPF_PAR,
CSOSN_CODIGO =  v_tab(i).CSOSN_CODIGO,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
CTRL_ENTRADA_ST =  v_tab(i).CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS =  v_tab(i).CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS =  v_tab(i).CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI =  v_tab(i).CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF =  v_tab(i).CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS =  v_tab(i).CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF =  v_tab(i).CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT =  v_tab(i).CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ =  v_tab(i).CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS =  v_tab(i).CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI =  v_tab(i).CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO =  v_tab(i).CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO =  v_tab(i).CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL =  v_tab(i).CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST =  v_tab(i).CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED =  v_tab(i).CTRL_TOLERANCIA_PED,
CUSTO_ADICAO =  v_tab(i).CUSTO_ADICAO,
CUSTO_REDUCAO =  v_tab(i).CUSTO_REDUCAO,
C4_SALDO_REF =  v_tab(i).C4_SALDO_REF,
DESCRICAO_ARMA =  v_tab(i).DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO =  v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO =  v_tab(i).DESONERACAO_CODIGO,
DT_DI =  v_tab(i).DT_DI,
DT_FAB_MED =  v_tab(i).DT_FAB_MED,
DT_FIN_SERV =  v_tab(i).DT_FIN_SERV,
DT_INI_SERV =  v_tab(i).DT_INI_SERV,
DT_REF_CALC_IMP_IDF =  v_tab(i).DT_REF_CALC_IMP_IDF,
DT_VAL =  v_tab(i).DT_VAL,
EMERC_CODIGO =  v_tab(i).EMERC_CODIGO,
ENQ_IPI_CODIGO =  v_tab(i).ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO =  v_tab(i).ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO =  v_tab(i).ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF =  v_tab(i).FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE =  v_tab(i).FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL =  v_tab(i).FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO =  v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO =  v_tab(i).FCI_NUMERO,
FIN_CODIGO =  v_tab(i).FIN_CODIGO,
FLAG_SUFRAMA =  v_tab(i).FLAG_SUFRAMA,
HON_COFINS_RET =  v_tab(i).HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET =  v_tab(i).HON_CSLL_RET,
HON_PIS_RET =  v_tab(i).HON_PIS_RET,
IDF_NUM =  v_tab(i).IDF_NUM,
IDF_NUM_PAI =  v_tab(i).IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR =  v_tab(i).IDF_TEXTO_COMPLEMENTAR,
IE_PAR =  v_tab(i).IE_PAR,
IM_SUBCONTRATACAO =  v_tab(i).IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS =  v_tab(i).IND_ANTECIP_ICMS,
IND_ARM =  v_tab(i).IND_ARM,
IND_BLOQUEADO =  v_tab(i).IND_BLOQUEADO,
IND_COFINS_ST =  v_tab(i).IND_COFINS_ST,
IND_COMPROVA_OPERACAO =  v_tab(i).IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO =  v_tab(i).IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE =  v_tab(i).IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO =  v_tab(i).IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS =  v_tab(i).IND_EXIGIBILIDADE_ISS,
IND_FCP_ST =  v_tab(i).IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS =  v_tab(i).IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS =  v_tab(i).IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET =  v_tab(i).IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST =  v_tab(i).IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET =  v_tab(i).IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS =  v_tab(i).IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS =  v_tab(i).IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET =  v_tab(i).IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI =  v_tab(i).IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF =  v_tab(i).IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS =  v_tab(i).IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS =  v_tab(i).IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET =  v_tab(i).IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST =  v_tab(i).IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT =  v_tab(i).IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST =  v_tab(i).IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF =  v_tab(i).IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT =  v_tab(i).IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS =  v_tab(i).IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE =  v_tab(i).IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS =  v_tab(i).IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI =  v_tab(i).IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF =  v_tab(i).IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT =  v_tab(i).IND_LAN_FISCAL_STT,
IND_LAN_IMP =  v_tab(i).IND_LAN_IMP,
IND_MED =  v_tab(i).IND_MED,
IND_MOVIMENTACAO_CIAP =  v_tab(i).IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC =  v_tab(i).IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE =  v_tab(i).IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM =  v_tab(i).IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS =  v_tab(i).IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS =  v_tab(i).IND_OUTROS_ICMS,
IND_OUTROS_IPI =  v_tab(i).IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP =  v_tab(i).IND_PAUTA_STF_MVA_MP,
IND_PIS_ST =  v_tab(i).IND_PIS_ST,
IND_SERV =  v_tab(i).IND_SERV,
IND_VERIFICA_PEDIDO =  v_tab(i).IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT =  v_tab(i).IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT =  v_tab(i).IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO =  v_tab(i).IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT =  v_tab(i).IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT =  v_tab(i).IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO =  v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO =  v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC =  v_tab(i).IND_ZFM_ALC,
LOTE_MED =  v_tab(i).LOTE_MED,
MERC_CODIGO =  v_tab(i).MERC_CODIGO,
MOD_BASE_ICMS_CODIGO =  v_tab(i).MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO =  v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO =  v_tab(i).MUN_CODIGO,
NAT_REC_PISCOFINS =  v_tab(i).NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR =  v_tab(i).NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO =  v_tab(i).NBM_CODIGO,
NBS_CODIGO =  v_tab(i).NBS_CODIGO,
NOP_CODIGO =  v_tab(i).NOP_CODIGO,
NUM_ARM =  v_tab(i).NUM_ARM,
NUM_CANO =  v_tab(i).NUM_CANO,
NUM_DI =  v_tab(i).NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE =  v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE =  v_tab(i).NUM_TANQUE,
OM_CODIGO =  v_tab(i).OM_CODIGO,
ORD_IMPRESSAO =  v_tab(i).ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO =  v_tab(i).PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS =  v_tab(i).PAUTA_FLUT_ICMS,
PEDIDO_NUMERO =  v_tab(i).PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM =  v_tab(i).PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL =  v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST =  v_tab(i).PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM =  v_tab(i).PERC_ICMS_PART_REM,
PERC_IRRF =  v_tab(i).PERC_IRRF,
PERC_ISENTO_ICMS =  v_tab(i).PERC_ISENTO_ICMS,
PERC_ISENTO_IPI =  v_tab(i).PERC_ISENTO_IPI,
PERC_OUTROS_ABAT =  v_tab(i).PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS =  v_tab(i).PERC_OUTROS_ICMS,
PERC_OUTROS_IPI =  v_tab(i).PERC_OUTROS_IPI,
PERC_PART_CI =  v_tab(i).PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS =  v_tab(i).PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST =  v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS =  v_tab(i).PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS =  v_tab(i).PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET =  v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI =  v_tab(i).PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF =  v_tab(i).PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS =  v_tab(i).PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS =  v_tab(i).PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST =  v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT =  v_tab(i).PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST =  v_tab(i).PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF =  v_tab(i).PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT =  v_tab(i).PERC_TRIBUTAVEL_STT,
PER_FISCAL =  v_tab(i).PER_FISCAL,
PESO_BRUTO_KG =  v_tab(i).PESO_BRUTO_KG,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR =  v_tab(i).PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO =  v_tab(i).PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT =  v_tab(i).PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS =  v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL =  v_tab(i).PRECO_TOTAL,
PRECO_UNITARIO =  v_tab(i).PRECO_UNITARIO,
PRES_CODIGO =  v_tab(i).PRES_CODIGO,
QTD =  v_tab(i).QTD,
QTD_BASE_COFINS =  v_tab(i).QTD_BASE_COFINS,
QTD_BASE_PIS =  v_tab(i).QTD_BASE_PIS,
QTD_EMB =  v_tab(i).QTD_EMB,
QTD_KIT =  v_tab(i).QTD_KIT,
REVISAO =  v_tab(i).REVISAO,
SELO_CODIGO =  v_tab(i).SELO_CODIGO,
SELO_QTDE =  v_tab(i).SELO_QTDE,
SISS_CODIGO =  v_tab(i).SISS_CODIGO,
STA_CODIGO =  v_tab(i).STA_CODIGO,
STC_CODIGO =  v_tab(i).STC_CODIGO,
STI_CODIGO =  v_tab(i).STI_CODIGO,
STM_CODIGO =  v_tab(i).STM_CODIGO,
STN_CODIGO =  v_tab(i).STN_CODIGO,
STP_CODIGO =  v_tab(i).STP_CODIGO,
SUBCLASSE_IDF =  v_tab(i).SUBCLASSE_IDF,
TERMINAL =  v_tab(i).TERMINAL,
TIPO_COMPLEMENTO =  v_tab(i).TIPO_COMPLEMENTO,
TIPO_OPER_VEIC =  v_tab(i).TIPO_OPER_VEIC,
TIPO_PROD_MED =  v_tab(i).TIPO_PROD_MED,
TIPO_RECEITA =  v_tab(i).TIPO_RECEITA,
TIPO_STF =  v_tab(i).TIPO_STF,
UF_PAR =  v_tab(i).UF_PAR,
UNI_CODIGO_FISCAL =  v_tab(i).UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO =  v_tab(i).UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET =  v_tab(i).VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF =  v_tab(i).VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS =  v_tab(i).VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE =  v_tab(i).VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL =  v_tab(i).VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO =  v_tab(i).VL_ALIMENTACAO,
VL_ALIQ_COFINS =  v_tab(i).VL_ALIQ_COFINS,
VL_ALIQ_PIS =  v_tab(i).VL_ALIQ_PIS,
VL_ANTECIPACAO =  v_tab(i).VL_ANTECIPACAO,
VL_ANTECIP_ICMS =  v_tab(i).VL_ANTECIP_ICMS,
VL_BASE_COFINS =  v_tab(i).VL_BASE_COFINS,
VL_BASE_COFINS_RET =  v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST =  v_tab(i).VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET =  v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_ICMS =  v_tab(i).VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L =  v_tab(i).VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP =  v_tab(i).VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST =  v_tab(i).VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST =  v_tab(i).VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM =  v_tab(i).VL_BASE_ICMS_PART_REM,
VL_BASE_II =  v_tab(i).VL_BASE_II,
VL_BASE_INSS =  v_tab(i).VL_BASE_INSS,
VL_BASE_INSS_RET =  v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IPI =  v_tab(i).VL_BASE_IPI,
VL_BASE_IRRF =  v_tab(i).VL_BASE_IRRF,
VL_BASE_ISS =  v_tab(i).VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO =  v_tab(i).VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS =  v_tab(i).VL_BASE_PIS,
VL_BASE_PIS_RET =  v_tab(i).VL_BASE_PIS_RET,
VL_BASE_PIS_ST =  v_tab(i).VL_BASE_PIS_ST,
VL_BASE_SENAT =  v_tab(i).VL_BASE_SENAT,
VL_BASE_SEST =  v_tab(i).VL_BASE_SEST,
VL_BASE_STF =  v_tab(i).VL_BASE_STF,
VL_BASE_STF_FRONTEIRA =  v_tab(i).VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO =  v_tab(i).VL_BASE_STF_IDO,
VL_BASE_STF60 =  v_tab(i).VL_BASE_STF60,
VL_BASE_STT =  v_tab(i).VL_BASE_STT,
VL_BASE_SUFRAMA =  v_tab(i).VL_BASE_SUFRAMA,
VL_COFINS =  v_tab(i).VL_COFINS,
VL_COFINS_RET =  v_tab(i).VL_COFINS_RET,
VL_COFINS_ST =  v_tab(i).VL_COFINS_ST,
VL_CONTABIL =  v_tab(i).VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO =  v_tab(i).VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB =  v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB =  v_tab(i).VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO =  v_tab(i).VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB =  v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB =  v_tab(i).VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET =  v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG =  v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS =  v_tab(i).VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG =  v_tab(i).VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG =  v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP =  v_tab(i).VL_DESC_CP,
VL_DIFA =  v_tab(i).VL_DIFA,
VL_FATURADO =  v_tab(i).VL_FATURADO,
VL_FISCAL =  v_tab(i).VL_FISCAL,
VL_GILRAT =  v_tab(i).VL_GILRAT,
VL_ICMS =  v_tab(i).VL_ICMS,
VL_ICMS_DESC_L =  v_tab(i).VL_ICMS_DESC_L,
VL_ICMS_FCP =  v_tab(i).VL_ICMS_FCP,
VL_ICMS_FCPST =  v_tab(i).VL_ICMS_FCPST,
VL_ICMS_PART_DEST =  v_tab(i).VL_ICMS_PART_DEST,
VL_ICMS_PART_REM =  v_tab(i).VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP =  v_tab(i).VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC =  v_tab(i).VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP =  v_tab(i).VL_ICMS_ST_RECUP,
VL_II =  v_tab(i).VL_II,
VL_IMP_FCI =  v_tab(i).VL_IMP_FCI,
VL_IMPOSTO_COFINS =  v_tab(i).VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS =  v_tab(i).VL_IMPOSTO_PIS,
VL_INSS =  v_tab(i).VL_INSS,
VL_INSS_AE15_NAO_RET =  v_tab(i).VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET =  v_tab(i).VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET =  v_tab(i).VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET =  v_tab(i).VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET =  v_tab(i).VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET =  v_tab(i).VL_INSS_AE25_RET,
VL_INSS_NAO_RET =  v_tab(i).VL_INSS_NAO_RET,
VL_INSS_RET =  v_tab(i).VL_INSS_RET,
VL_IOF =  v_tab(i).VL_IOF,
VL_IPI =  v_tab(i).VL_IPI,
VL_IRRF =  v_tab(i).VL_IRRF,
VL_IRRF_NAO_RET =  v_tab(i).VL_IRRF_NAO_RET,
VL_ISENTO_ICMS =  v_tab(i).VL_ISENTO_ICMS,
VL_ISENTO_IPI =  v_tab(i).VL_ISENTO_IPI,
VL_ISS =  v_tab(i).VL_ISS,
VL_ISS_BITRIBUTADO =  v_tab(i).VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO =  v_tab(i).VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO =  v_tab(i).VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES =  v_tab(i).VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP =  v_tab(i).VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP =  v_tab(i).VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE =  v_tab(i).VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT =  v_tab(i).VL_OUTROS_ABAT,
VL_OUTROS_ICMS =  v_tab(i).VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS =  v_tab(i).VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI =  v_tab(i).VL_OUTROS_IPI,
VL_PIS =  v_tab(i).VL_PIS,
VL_PIS_RET =  v_tab(i).VL_PIS_RET,
VL_PIS_ST =  v_tab(i).VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO =  v_tab(i).VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF =  v_tab(i).VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF =  v_tab(i).VL_RATEIO_CT_STF,
VL_RATEIO_FRETE =  v_tab(i).VL_RATEIO_FRETE,
VL_RATEIO_ODA =  v_tab(i).VL_RATEIO_ODA,
VL_RATEIO_SEGURO =  v_tab(i).VL_RATEIO_SEGURO,
VL_RECUPERADO =  v_tab(i).VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA =  v_tab(i).VL_RETIDO_SUBEMPREITADA,
VL_SENAR =  v_tab(i).VL_SENAR,
VL_SENAT =  v_tab(i).VL_SENAT,
VL_SERVICO_AE15 =  v_tab(i).VL_SERVICO_AE15,
VL_SERVICO_AE20 =  v_tab(i).VL_SERVICO_AE20,
VL_SERVICO_AE25 =  v_tab(i).VL_SERVICO_AE25,
VL_SEST =  v_tab(i).VL_SEST,
VL_STF =  v_tab(i).VL_STF,
VL_STF_FRONTEIRA =  v_tab(i).VL_STF_FRONTEIRA,
VL_STF_IDO =  v_tab(i).VL_STF_IDO,
VL_ST_FRONTEIRA =  v_tab(i).VL_ST_FRONTEIRA,
VL_STF60 =  v_tab(i).VL_STF60,
VL_STT =  v_tab(i).VL_STT,
VL_SUFRAMA =  v_tab(i).VL_SUFRAMA,
VL_TAB_MAX =  v_tab(i).VL_TAB_MAX,
VL_TRANSPORTE =  v_tab(i).VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC =  v_tab(i).VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA =  v_tab(i).VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS =  v_tab(i).VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L =  v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI =  v_tab(i).VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF =  v_tab(i).VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT =  v_tab(i).VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA =  v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
XCON_ID =  v_tab(i).XCON_ID,
XINF_ID =  v_tab(i).XINF_ID
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and idf_num = v_tab(i).idf_num;
			  exception when others then
                     r_put_line('Erro ao alterar IDF DUP. - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir IDF - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
  end;
  else
     delete from hon_gttemp_idf
      where id   = v_tab(i).id;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_dof_assoc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof_assoc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_assoc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_ASSOCIADO (ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO)
   VALUES (v_tab(i).ANO_MES_EMISSAO_ASSOC,
v_site,
v_tab(i).CTRL_DESCARGA,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DESPACHO,
v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
v_tab(i).DH_EMISSAO_ASSOC,
v_tab(i).DOF_ASSOC_SEQUENCE,
v_sequence,
v_tab(i).EDOF_CODIGO_ASSOC,
v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_dof,
v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
v_tab(i).MDOF_CODIGO_ASSOC,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NUMERO_ASSOC,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_TOTAL,
v_tab(i).QTD_VOLUMES,
v_tab(i).SEQUENCIA,
v_tab(i).SERIE_SUBSERIE_ASSOC,
v_tab(i).TADOC_CODIGO,
v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
v_tab(i).TIPO_PRODUTO,
v_tab(i).VL_MERCADORIAS,
v_tab(i).VL_RATEIO,
v_tab(i).VL_TOTAL_DOF_ASSOC,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_dof_associado
				    set 
ANO_MES_EMISSAO_ASSOC =  v_tab(i).ANO_MES_EMISSAO_ASSOC,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
C4_IND_TEM_REAL =  v_tab(i).C4_IND_TEM_REAL,
DESPACHO =  v_tab(i).DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC =  v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC =  v_tab(i).DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE =  v_tab(i).DOF_ASSOC_SEQUENCE,
EDOF_CODIGO_ASSOC =  v_tab(i).EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC =  v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
IND_ENTRADA_SAIDA_ASSOC =  v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC =  v_tab(i).MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO =  v_tab(i).MUN_COD_DESTINO,
MUN_COD_ORIGEM =  v_tab(i).MUN_COD_ORIGEM,
NFE_LOCALIZADOR =  v_tab(i).NFE_LOCALIZADOR,
NUMERO_ASSOC =  v_tab(i).NUMERO_ASSOC,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PESO_TOTAL =  v_tab(i).PESO_TOTAL,
QTD_VOLUMES =  v_tab(i).QTD_VOLUMES,
SEQUENCIA =  v_tab(i).SEQUENCIA,
SERIE_SUBSERIE_ASSOC =  v_tab(i).SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO =  v_tab(i).TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC =  v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO =  v_tab(i).TIPO_PRODUTO,
VL_MERCADORIAS =  v_tab(i).VL_MERCADORIAS,
VL_RATEIO =  v_tab(i).VL_RATEIO,
VL_TOTAL_DOF_ASSOC =  v_tab(i).VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO =  v_tab(i).VL_TOTAL_STF_SUBSTITUIDO
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and dof_assoc_sequence = v_tab(i).dof_assoc_sequence;
			  exception when others then
                     r_put_line('Erro ao alterar DOF_ASSOC DUP. - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_ASSOC - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_dof_parcela (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_parcela%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_parc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    v_site     := 0;
    v_sequence := 0;
    v_dof      := 0;
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_PARCELA (BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC)
   VALUES (v_tab(i).BLOQ_SEQ,
v_site,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_IRRF,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_sequence,
v_tab(i).DT_EFETIVO_PAGTO,
v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
v_tab(i).DT_VENCIMENTO_PARCELA,
v_tab(i).DUPL_SEQ,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_VENCTO_SAP,
v_tab(i).HON_VL_SAP,
v_tab(i).IND_ADIANTAMENTO,
v_tab(i).IND_SUSPEITA,
v_tab(i).MP_CODIGO,
v_tab(i).NUM_PARCELA,
v_tab(i).OBS_DESC_CONDICIONAL,
v_tab(i).PERC_DESC_CONDICIONAL,
v_tab(i).PERC_VALOR_NF,
v_tab(i).REVISAO,
v_tab(i).SEQ_CALCULO,
v_tab(i).SIGLA_MOEDA,
v_tab(i).VL_ABATIDO_RESIDUO_PCC,
v_tab(i).VL_BASE_ACUM_COFINS_RET,
v_tab(i).VL_BASE_ACUM_CSLL_RET,
v_tab(i).VL_BASE_ACUM_PCC,
v_tab(i).VL_BASE_ACUM_PIS_RET,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IRRF_RET,
v_tab(i).VL_BASE_ISS_RET,
v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_REDUZIDA_IRRF,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESCONTO_CONDICIONAL,
v_tab(i).VL_FISCAL,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IRRF_RET,
v_tab(i).VL_ISS_RET,
v_tab(i).VL_PARCELA,
v_tab(i).VL_PIS_COFINS_CSLL_RET,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_RESIDUO_PCC);
    exception when DUP_VAL_ON_INDEX 
	       then 
			   begin
			   update cor_dof_parcela
set BLOQ_SEQ = v_tab(i).BLOQ_SEQ,
CODIGO_RETENCAO_COFINS = v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL = v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF = v_tab(i).CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC = v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS = v_tab(i).CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
DT_EFETIVO_PAGTO = v_tab(i).DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL = v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA = v_tab(i).DT_VENCIMENTO_PARCELA,
DUPL_SEQ = v_tab(i).DUPL_SEQ,
HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP = v_tab(i).HON_VENCTO_SAP,
HON_VL_SAP = v_tab(i).HON_VL_SAP,
IND_ADIANTAMENTO = v_tab(i).IND_ADIANTAMENTO,
IND_SUSPEITA = v_tab(i).IND_SUSPEITA,
MP_CODIGO = v_tab(i).MP_CODIGO,
OBS_DESC_CONDICIONAL = v_tab(i).OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL = v_tab(i).PERC_DESC_CONDICIONAL,
PERC_VALOR_NF = v_tab(i).PERC_VALOR_NF,
REVISAO = v_tab(i).REVISAO,
SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC = v_tab(i).VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET = v_tab(i).VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET = v_tab(i).VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC = v_tab(i).VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET = v_tab(i).VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET = v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET = v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_INSS_RET = v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IRRF_RET = v_tab(i).VL_BASE_IRRF_RET,
VL_BASE_ISS_RET = v_tab(i).VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET = v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET = v_tab(i).VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF = v_tab(i).VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET = v_tab(i).VL_COFINS_RET,
VL_CSLL_RET = v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG = v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG = v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL = v_tab(i).VL_DESCONTO_CONDICIONAL,
VL_FISCAL = v_tab(i).VL_FISCAL,
VL_INSS_RET = v_tab(i).VL_INSS_RET,
VL_IRRF_RET = v_tab(i).VL_IRRF_RET,
VL_ISS_RET = v_tab(i).VL_ISS_RET,
VL_PARCELA = v_tab(i).VL_PARCELA,
VL_PIS_COFINS_CSLL_RET = v_tab(i).VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET = v_tab(i).VL_PIS_RET,
VL_RESIDUO_PCC = v_tab(i).VL_RESIDUO_PCC			   
where num_parcela = v_tab(i).num_parcela
  and dof_sequence = v_sequence
  and codigo_do_site =  v_site;
			   exception when others then
                     r_put_line('Erro ao alterar DOF_PARCELA DUP. - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem 
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_PARCELA - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_pfj_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF hon_gttemp_pessoa%ROWTYPE;
  v_tab    typ_tab;
CURSOR c IS
 SELECT ALTERADO_EM,
ALTERADO_POR,
CHAVE_ORIGEM,
CHAVE_ORIGEM_PAI,
CODIGO_USUAL,
COD_NATUREZA_JURIDICA,
HON_CLIENTE_JDE,
HON_CONSOLIDA_ORIGEM,
HON_FORNECEDOR_JDE,
HON_IDENTIFICA_SECRETARIA,
IND_ARREDONDA_VLRS,
IND_CCOR,
IND_CFPFJ,
IND_CLIENTE,
IND_COMPHIER_FILHO,
IND_COMPHIER_PAI,
IND_CONTABILISTA,
IND_ESTABELECIMENTO,
IND_FISICA_JURIDICA,
IND_FORNECEDOR,
IND_NACIONAL_ESTRANGEIRA,
IND_NAT_PESSOA_EFD,
IND_ORGAO_GOVERNAMENTAL,
IND_PFJMERC,
IND_PONTO_ALFANDEGADO,
IND_PRODUTOR,
IND_PROF,
IND_RGPFJUF,
IND_TRANSPORTADOR,
LOC_CODIGO_COBRANCA,
LOC_CODIGO_TRANSP,
MNEMONICO,
ORG_ID,
ORIGEM,
PFJ_CHAVE_ORIGEM_PAI,
PFJ_CODIGO,
PFJ_CODIGO_COBRANCA,
PFJ_CODIGO_ENTREGA,
PFJ_CODIGO_RETIRADA,
PFJ_CODIGO_TRANSP,
REVISAO
 FROM hon_consolida_pfj_v
 -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_PESSOA (ALTERADO_EM,
ALTERADO_POR,
CHAVE_ORIGEM,
CHAVE_ORIGEM_PAI,
CODIGO_USUAL,
COD_NATUREZA_JURIDICA,
HON_CLIENTE_JDE,
HON_CONSOLIDA_ORIGEM,
HON_FORNECEDOR_JDE,
HON_IDENTIFICA_SECRETARIA,
IND_ARREDONDA_VLRS,
IND_CCOR,
IND_CFPFJ,
IND_CLIENTE,
IND_COMPHIER_FILHO,
IND_COMPHIER_PAI,
IND_CONTABILISTA,
IND_ESTABELECIMENTO,
IND_FISICA_JURIDICA,
IND_FORNECEDOR,
IND_NACIONAL_ESTRANGEIRA,
IND_NAT_PESSOA_EFD,
IND_ORGAO_GOVERNAMENTAL,
IND_PFJMERC,
IND_PONTO_ALFANDEGADO,
IND_PRODUTOR,
IND_PROF,
IND_RGPFJUF,
IND_TRANSPORTADOR,
LOC_CODIGO_COBRANCA,
LOC_CODIGO_TRANSP,
MNEMONICO,
ORG_ID,
ORIGEM,
PFJ_CHAVE_ORIGEM_PAI,
PFJ_CODIGO,
PFJ_CODIGO_COBRANCA,
PFJ_CODIGO_ENTREGA,
PFJ_CODIGO_RETIRADA,
PFJ_CODIGO_TRANSP,
REVISAO)
     VALUES ( v_tab(i).ALTERADO_EM, v_tab(i).ALTERADO_POR, v_tab(i).CHAVE_ORIGEM, v_tab(i).CHAVE_ORIGEM_PAI, v_tab(i).CODIGO_USUAL,
              v_tab(i).COD_NATUREZA_JURIDICA, v_tab(i).HON_CLIENTE_JDE, v_tab(i).HON_CONSOLIDA_ORIGEM, v_tab(i).HON_FORNECEDOR_JDE,
              v_tab(i).HON_IDENTIFICA_SECRETARIA, v_tab(i).IND_ARREDONDA_VLRS, v_tab(i).IND_CCOR, v_tab(i).IND_CFPFJ, v_tab(i).IND_CLIENTE,
              v_tab(i).IND_COMPHIER_FILHO, v_tab(i).IND_COMPHIER_PAI, v_tab(i).IND_CONTABILISTA, v_tab(i).IND_ESTABELECIMENTO,
              v_tab(i).IND_FISICA_JURIDICA, v_tab(i).IND_FORNECEDOR, v_tab(i).IND_NACIONAL_ESTRANGEIRA, v_tab(i).IND_NAT_PESSOA_EFD,
              v_tab(i).IND_ORGAO_GOVERNAMENTAL, v_tab(i).IND_PFJMERC, v_tab(i).IND_PONTO_ALFANDEGADO, v_tab(i).IND_PRODUTOR, v_tab(i).IND_PROF,
              v_tab(i).IND_RGPFJUF, v_tab(i).IND_TRANSPORTADOR, v_tab(i).LOC_CODIGO_COBRANCA, v_tab(i).LOC_CODIGO_TRANSP, v_tab(i).MNEMONICO,
              v_tab(i).ORG_ID, v_tab(i).ORIGEM, v_tab(i).PFJ_CHAVE_ORIGEM_PAI, v_tab(i).PFJ_CODIGO, v_tab(i).PFJ_CODIGO_COBRANCA,
              v_tab(i).PFJ_CODIGO_ENTREGA, v_tab(i).PFJ_CODIGO_RETIRADA, v_tab(i).PFJ_CODIGO_TRANSP, v_tab(i).REVISAO);
  exception when DUP_VAL_ON_INDEX 
            then 
			   begin
			       UPDATE  COR_PESSOA 
				      SET MNEMONICO = v_tab(i).MNEMONICO
                         ,IND_FISICA_JURIDICA = v_tab(i).IND_FISICA_JURIDICA
                         ,IND_NACIONAL_ESTRANGEIRA = v_tab(i).IND_NACIONAL_ESTRANGEIRA
                         ,IND_CLIENTE = v_tab(i).IND_CLIENTE
                         ,IND_ESTABELECIMENTO = v_tab(i).IND_ESTABELECIMENTO
                         ,IND_FORNECEDOR = v_tab(i).IND_FORNECEDOR
                         ,IND_PRODUTOR = v_tab(i).IND_PRODUTOR
                         ,CODIGO_USUAL = v_tab(i).CODIGO_USUAL
                         ,ORIGEM = v_tab(i).ORIGEM
                         ,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
						 --
						 ,ALTERADO_EM = v_tab(i).ALTERADO_EM
						 ,ALTERADO_POR = v_tab(i).ALTERADO_POR
						 ,CHAVE_ORIGEM_PAI = v_tab(i).CHAVE_ORIGEM_PAI
						 ,COD_NATUREZA_JURIDICA = v_tab(i).COD_NATUREZA_JURIDICA
						 ,HON_CLIENTE_JDE = v_tab(i).HON_CLIENTE_JDE
						 ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						 ,HON_FORNECEDOR_JDE = v_tab(i).HON_FORNECEDOR_JDE
						 ,HON_IDENTIFICA_SECRETARIA = v_tab(i).HON_IDENTIFICA_SECRETARIA
						 ,IND_ARREDONDA_VLRS = v_tab(i).IND_ARREDONDA_VLRS
						 ,IND_CCOR = v_tab(i).IND_CCOR
						 ,IND_CFPFJ = v_tab(i).IND_CFPFJ
						 ,IND_COMPHIER_FILHO = v_tab(i).IND_COMPHIER_FILHO
						 ,IND_COMPHIER_PAI = v_tab(i).IND_COMPHIER_PAI
						 ,IND_CONTABILISTA = v_tab(i).IND_CONTABILISTA
                         ,IND_NAT_PESSOA_EFD = v_tab(i).IND_NAT_PESSOA_EFD
						 ,IND_ORGAO_GOVERNAMENTAL = v_tab(i).IND_ORGAO_GOVERNAMENTAL
						 ,IND_PFJMERC = v_tab(i).IND_PFJMERC
						 ,IND_PONTO_ALFANDEGADO = v_tab(i).IND_PONTO_ALFANDEGADO
						 ,IND_PROF = v_tab(i).IND_PROF
						 ,IND_RGPFJUF = v_tab(i).IND_RGPFJUF
						 ,IND_TRANSPORTADOR = v_tab(i).IND_TRANSPORTADOR
						 ,LOC_CODIGO_COBRANCA = v_tab(i).LOC_CODIGO_COBRANCA
						 ,LOC_CODIGO_TRANSP = v_tab(i).LOC_CODIGO_TRANSP
						 --,ORG_ID = v_tab(i).ORG_ID
						 ,PFJ_CHAVE_ORIGEM_PAI = v_tab(i).PFJ_CHAVE_ORIGEM_PAI
						 ,PFJ_CODIGO_COBRANCA = v_tab(i).PFJ_CODIGO_COBRANCA
						 ,PFJ_CODIGO_ENTREGA = v_tab(i).PFJ_CODIGO_ENTREGA
						 ,PFJ_CODIGO_RETIRADA = v_tab(i).PFJ_CODIGO_RETIRADA
						 ,PFJ_CODIGO_TRANSP = v_tab(i).PFJ_CODIGO_TRANSP
						 ,REVISAO = v_tab(i).REVISAO			   
                    WHERE PFJ_CODIGO = v_tab(i).PFJ_CODIGO;
               exception when others
                 then
                     r_put_line('Erro ao alterar PFJ - ' || v_tab(i).pfj_codigo || ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'E');  
                     commit;
                     end;
               end;
            when others
                 then
                     r_put_line('Erro ao incluir PFJ - ' || v_tab(i).pfj_codigo || ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_pfj_vig_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE     typ_tab IS TABLE OF hon_gttemp_pessoa_vig%ROWTYPE;
  v_tab    typ_tab;
  mdata    CORAPI_PFJ_VIG.data;
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,CAEN_CODIGO_PRINCIPAL,COD_CLASSIF_TRIB,CODIGO_CPRB,COD_INSTAL_ANP,CPF_CGC,
 CRT_CODIGO,CTRL_DESCARGA,DT_FIM,DT_INICIO,E_MAIL, HON_CONSOLIDA_ORIGEM,ID,IND_ACORDO_INTERNACIONAL,
 IND_BENEF_DISPENSADO_NIF,IND_CONTR_ICMS,IND_CONTR_IPI,IND_DESON_FOLHA_CPRB,IND_ECD,IND_REGIME_LUCRO,
 IND_SIMPLES_NACIONAL,IND_SITUACAO_PJ,IND_SUBSTITUTO_ICMS,NIF,NOME_FANTASIA,NUM_DEPENDENTES_IR,ORG_ID,ORIGEM,
 PFJ_CODIGO,RAZAO_SOCIAL,REL_PAGADOR_BENEF, REVISAO,TICO_CODIGO,VL_INSS_DEDUCAO,VL_PENSAO,WEB_SITE
 FROM hon_consolida_pfj_vig_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
        mdata.v.PFJ_CODIGO            := v_tab(i).PFJ_CODIGO;
        mdata.v.DT_INICIO             := v_tab(i).DT_INICIO;
        mdata.v.DT_FIM                := v_tab(i).DT_FIM;
        mdata.v.IND_CONTR_ICMS        := v_tab(i).IND_CONTR_ICMS;
        mdata.v.IND_CONTR_IPI         := v_tab(i).IND_CONTR_IPI;
        mdata.v.RAZAO_SOCIAL          := v_tab(i).RAZAO_SOCIAL;
        mdata.v.NOME_FANTASIA         := v_tab(i).NOME_FANTASIA;
        mdata.v.CPF_CGC               := v_tab(i).CPF_CGC;
        mdata.v.CAEN_CODIGO_PRINCIPAL := v_tab(i).CAEN_CODIGO_PRINCIPAL;
        mdata.v.WEB_SITE              := v_tab(i).WEB_SITE;
        mdata.v.E_MAIL                := v_tab(i).E_MAIL;
        mdata.v.TICO_CODIGO           := v_tab(i).TICO_CODIGO;
        mdata.v.ORIGEM                := v_tab(i).ORIGEM;
        mdata.v.IND_SUBSTITUTO_ICMS   := v_tab(i).IND_SUBSTITUTO_ICMS;
        mdata.v.IND_SIMPLES_NACIONAL  := v_tab(i).IND_SIMPLES_NACIONAL;
        mdata.v.CRT_CODIGO            := v_tab(i).CRT_CODIGO;
        --
        mdata.v.ALTERADO_EM              := v_tab(i).ALTERADO_EM;
        mdata.v.ALTERADO_POR             := v_tab(i).ALTERADO_POR;
        mdata.v.COD_CLASSIF_TRIB         := v_tab(i).COD_CLASSIF_TRIB;
        mdata.v.CODIGO_CPRB              := v_tab(i).CODIGO_CPRB;
        mdata.v.COD_INSTAL_ANP           := v_tab(i).COD_INSTAL_ANP;
        mdata.v.HON_CONSOLIDA_ORIGEM     := v_tab(i).HON_CONSOLIDA_ORIGEM;
        mdata.v.IND_ACORDO_INTERNACIONAL := v_tab(i).IND_ACORDO_INTERNACIONAL;
        mdata.v.IND_BENEF_DISPENSADO_NIF := v_tab(i).IND_BENEF_DISPENSADO_NIF;
        mdata.v.IND_DESON_FOLHA_CPRB     := v_tab(i).IND_DESON_FOLHA_CPRB;
        mdata.v.IND_ECD                  := v_tab(i).IND_ECD;
        mdata.v.IND_REGIME_LUCRO         := v_tab(i).IND_REGIME_LUCRO;
        mdata.v.IND_SITUACAO_PJ          := v_tab(i).IND_SITUACAO_PJ;
        mdata.v.NIF                      := v_tab(i).NIF;
        mdata.v.NUM_DEPENDENTES_IR       := v_tab(i).NUM_DEPENDENTES_IR;
        mdata.v.ORG_ID                   := v_tab(i).ORG_ID;
        mdata.v.REL_PAGADOR_BENEF        := v_tab(i).REL_PAGADOR_BENEF;
        mdata.v.REVISAO                  := v_tab(i).REVISAO;
        mdata.v.VL_INSS_DEDUCAO          := v_tab(i).VL_INSS_DEDUCAO;
        mdata.v.VL_PENSAO                := v_tab(i).VL_PENSAO;
        CORAPI_PFJ_VIG.ins(mdata);
  exception when DUP_VAL_ON_INDEX 
            then 
			   begin
			      mdata.v.PFJ_CODIGO := v_tab(i).PFJ_CODIGO;
                  mdata.v.DT_INICIO := v_tab(i).DT_INICIO;
                  mdata.v.DT_FIM := v_tab(i).DT_FIM;
                  mdata.v.IND_CONTR_ICMS := v_tab(i).IND_CONTR_ICMS;
                  mdata.v.IND_CONTR_IPI := v_tab(i).IND_CONTR_IPI;
                  mdata.v.RAZAO_SOCIAL := v_tab(i).RAZAO_SOCIAL;
                  mdata.v.NOME_FANTASIA := v_tab(i).NOME_FANTASIA;
                  mdata.v.CPF_CGC := v_tab(i).CPF_CGC;
                  mdata.v.CAEN_CODIGO_PRINCIPAL := v_tab(i).CAEN_CODIGO_PRINCIPAL;
                  mdata.v.WEB_SITE := v_tab(i).WEB_SITE;
                  mdata.v.E_MAIL := v_tab(i).E_MAIL;
                  mdata.v.TICO_CODIGO := v_tab(i).TICO_CODIGO;
                  mdata.v.ORIGEM := v_tab(i).ORIGEM;
                  mdata.v.IND_SUBSTITUTO_ICMS := v_tab(i).IND_SUBSTITUTO_ICMS;
                  mdata.v.IND_SIMPLES_NACIONAL := v_tab(i).IND_SIMPLES_NACIONAL;
                  mdata.v.CRT_CODIGO := v_tab(i).CRT_CODIGO;
                  mdata.i.PFJ_CODIGO := TRUE;
                  mdata.i.DT_INICIO := TRUE;
                  mdata.i.DT_FIM := TRUE;
                  mdata.i.IND_CONTR_ICMS := TRUE;
                  mdata.i.IND_CONTR_IPI := TRUE;
                  mdata.i.RAZAO_SOCIAL := TRUE;
                  mdata.i.NOME_FANTASIA := TRUE;
                  mdata.i.CPF_CGC := TRUE;
                  mdata.i.CAEN_CODIGO_PRINCIPAL := TRUE;
                  mdata.i.WEB_SITE := TRUE;
                  mdata.i.E_MAIL := TRUE;
                  mdata.i.TICO_CODIGO := TRUE;
                  mdata.i.ORIGEM := TRUE;
                  mdata.i.IND_SUBSTITUTO_ICMS := TRUE;
                  mdata.i.IND_SIMPLES_NACIONAL := TRUE;
                  mdata.i.CRT_CODIGO := TRUE;
                  CORAPI_PFJ_VIG.upd(mdata);
			   exception when others then
                     r_put_line('Erro ao alterar PFJ_VIG DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir PFJ_VIG - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_loc_pfj_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_loc_pfj%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
      ALTERADO_EM,ALTERADO_POR,CHAVE_ORIGEM,CODIGO_USUAL,HON_CONSOLIDA_ORIGEM,IND_DESTINATARIA,IND_ENTREGA,
      IND_GERAL,IND_REMETENTE,IND_RETIRADA,
        IND_TRANSPORTADORA,LOC_CODIGO,MNEMONICO,ORG_ID,ORIGEM,PFJ_CODIGO,REVISAO
 FROM hon_consolida_loc_pfj_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_LOCALIDADE_PESSOA ( ALTERADO_EM,ALTERADO_POR,CHAVE_ORIGEM,CODIGO_USUAL,
     HON_CONSOLIDA_ORIGEM,IND_DESTINATARIA,IND_ENTREGA,IND_GERAL,IND_REMETENTE,IND_RETIRADA,
     IND_TRANSPORTADORA,LOC_CODIGO,MNEMONICO,ORG_ID,ORIGEM,PFJ_CODIGO,REVISAO)
   VALUES (v_tab(i).ALTERADO_EM,v_tab(i).ALTERADO_POR,v_tab(i).CHAVE_ORIGEM,v_tab(i).CODIGO_USUAL,
           v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_DESTINATARIA,v_tab(i).IND_ENTREGA,
           v_tab(i).IND_GERAL,v_tab(i).IND_REMETENTE,v_tab(i).IND_RETIRADA,v_tab(i).IND_TRANSPORTADORA,
           v_tab(i).LOC_CODIGO,v_tab(i).MNEMONICO,v_tab(i).ORG_ID,v_tab(i).ORIGEM,v_tab(i).PFJ_CODIGO,
           v_tab(i).REVISAO);
    exception when DUP_VAL_ON_INDEX 
	          then 
				 begin
                 UPDATE  COR_LOCALIDADE_PESSOA 
				    SET MNEMONICO = v_tab(i).MNEMONICO
                       ,IND_GERAL = v_tab(i).IND_GERAL
                       ,CODIGO_USUAL = v_tab(i).CODIGO_USUAL
                       ,ORIGEM = v_tab(i).ORIGEM
					   --
                       ,ALTERADO_EM = v_tab(i).ALTERADO_EM
					   ,ALTERADO_POR = v_tab(i).ALTERADO_POR
					   ,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
                       ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
					   ,IND_DESTINATARIA = v_tab(i).IND_DESTINATARIA
					   ,IND_ENTREGA = v_tab(i).IND_ENTREGA
                       ,IND_REMETENTE = v_tab(i).IND_REMETENTE
					   ,IND_RETIRADA = v_tab(i).IND_RETIRADA
					   ,IND_TRANSPORTADORA = v_tab(i).IND_TRANSPORTADORA
                       --,ORG_ID = v_tab(i).ORG_ID
                       ,REVISAO = v_tab(i).REVISAO					   
                 WHERE PFJ_CODIGO = v_tab(i).PFJ_CODIGO
                   AND LOC_CODIGO = v_tab(i).LOC_CODIGO;				 
				 exception when others then
                     r_put_line('Erro ao alterar LOC_PFJ DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_PESSOA', 'E');  
                     commit;
                     end;
				 end; 
            when others
                 then
                     r_put_line('Erro ao incluir LOC_PFJ - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_loc_vig_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_loc_vig%ROWTYPE;
  v_tab typ_tab;
  mdata CORAPI_LOC_PFJ_VIG.data;  
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,BAIRRO,CAIXA_POSTAL,CCM,CEP,CEP_CP,COMPLEMENTO,DT_FIM,DT_INICIO,E_MAIL,FAX,HON_CONSOLIDA_ORIGEM,ID,INSCR_ESTADUAL,INSCR_INSS,
INSCR_SUFRAMA,LOC_CODIGO,LOGRADOURO,MUN_CODIGO,MUNICIPIO,NIRE,NUMERO,NUM_REGIME_ISS,NUM_REGIME_NF_ELET,ORG_ID,ORIGEM,PAIS,PFJ_CODIGO,PIS,POSTO_FISCAL,REVISAO,
TELEFONE1,TELEFONE2,TELEX,UNIDADE_FEDERATIVA,WEB_SITE
 FROM hon_consolida_loc_vig_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
       mdata.v.PFJ_CODIGO         := v_tab(i).PFJ_CODIGO;
       mdata.v.LOC_CODIGO         := v_tab(i).LOC_CODIGO;
       mdata.v.DT_INICIO          := v_tab(i).DT_INICIO;
       mdata.v.DT_FIM             := v_tab(i).DT_FIM;
       mdata.v.LOGRADOURO         := v_tab(i).LOGRADOURO;
       mdata.v.NUMERO             := v_tab(i).NUMERO;
       mdata.v.COMPLEMENTO        := v_tab(i).COMPLEMENTO;
       mdata.v.BAIRRO             := v_tab(i).BAIRRO;
       mdata.v.CEP                := v_tab(i).CEP;
       mdata.v.MUN_CODIGO         := v_tab(i).MUN_CODIGO;
       mdata.v.TELEFONE1          := v_tab(i).TELEFONE1;
       mdata.v.TELEFONE2          := v_tab(i).TELEFONE2;
       mdata.v.FAX                := v_tab(i).FAX;
       mdata.v.TELEX              := v_tab(i).TELEX;
       mdata.v.INSCR_ESTADUAL     := v_tab(i).INSCR_ESTADUAL;
       mdata.v.E_MAIL             := v_tab(i).E_MAIL;
       mdata.v.WEB_SITE           := v_tab(i).WEB_SITE;
       mdata.v.MUNICIPIO          := v_tab(i).MUNICIPIO;
       mdata.v.UNIDADE_FEDERATIVA := v_tab(i).UNIDADE_FEDERATIVA;
       mdata.v.PAIS               := v_tab(i).PAIS;
       mdata.v.ORIGEM             := v_tab(i).ORIGEM;
       mdata.v.CCM                := v_tab(i).CCM;
       mdata.v.INSCR_SUFRAMA      := v_tab(i).INSCR_SUFRAMA;
       mdata.v.CAIXA_POSTAL       := v_tab(i).CAIXA_POSTAL;
       mdata.v.CEP_CP             := v_tab(i).CEP_CP;
	   --
	   mdata.v.ALTERADO_EM          := v_tab(i).ALTERADO_EM;
	   mdata.v.ALTERADO_POR         := v_tab(i).ALTERADO_POR;
	   mdata.v.HON_CONSOLIDA_ORIGEM := v_tab(i).HON_CONSOLIDA_ORIGEM;
	   mdata.v.INSCR_INSS           := v_tab(i).INSCR_INSS;
	   mdata.v.NIRE                 := v_tab(i).NIRE;
	   mdata.v.NUM_REGIME_ISS       := v_tab(i).NUM_REGIME_ISS;
	   mdata.v.NUM_REGIME_NF_ELET   := v_tab(i).NUM_REGIME_NF_ELET;
	   mdata.v.ORG_ID               := v_tab(i).ORG_ID;
	   mdata.v.PIS                  := v_tab(i).PIS;
	   mdata.v.POSTO_FISCAL         := v_tab(i).POSTO_FISCAL;
	   mdata.v.REVISAO              := v_tab(i).REVISAO;
       CORAPI_LOC_PFJ_VIG.ins(mdata);
    exception
      when DUP_VAL_ON_INDEX 
	        then 
				begin
                   mdata.v.PFJ_CODIGO := v_tab(i).PFJ_CODIGO;
                   mdata.v.LOC_CODIGO := v_tab(i).LOC_CODIGO;
                   mdata.v.DT_INICIO := v_tab(i).DT_INICIO;
                   mdata.v.DT_FIM := v_tab(i).DT_FIM;
                   mdata.v.LOGRADOURO := v_tab(i).LOGRADOURO;
                   mdata.v.NUMERO := v_tab(i).NUMERO;
                   mdata.v.COMPLEMENTO := v_tab(i).COMPLEMENTO;
                   mdata.v.BAIRRO := v_tab(i).BAIRRO;
                   mdata.v.CEP := v_tab(i).CEP;
                   mdata.v.MUN_CODIGO := v_tab(i).MUN_CODIGO;
                   mdata.v.TELEFONE1 := v_tab(i).TELEFONE1;
                   mdata.v.TELEFONE2 := v_tab(i).TELEFONE2;
                   mdata.v.FAX := v_tab(i).FAX;
                   mdata.v.TELEX := v_tab(i).TELEX;
                   mdata.v.INSCR_ESTADUAL := v_tab(i).INSCR_ESTADUAL;
                   mdata.v.E_MAIL := v_tab(i).E_MAIL;
                   mdata.v.WEB_SITE := v_tab(i).WEB_SITE;
                   mdata.v.MUNICIPIO := v_tab(i).MUNICIPIO;
                   mdata.v.UNIDADE_FEDERATIVA := v_tab(i).UNIDADE_FEDERATIVA;
                   mdata.v.PAIS := v_tab(i).PAIS;
                   mdata.v.ORIGEM := v_tab(i).ORIGEM;
                   mdata.v.CCM := v_tab(i).CCM;
                   mdata.v.INSCR_SUFRAMA := v_tab(i).INSCR_SUFRAMA;
                   mdata.v.CAIXA_POSTAL := v_tab(i).CAIXA_POSTAL;
                   mdata.v.CEP_CP := v_tab(i).CEP_CP;
                   mdata.i.PFJ_CODIGO := TRUE;
                   mdata.i.LOC_CODIGO := TRUE;
                   mdata.i.DT_INICIO := TRUE;
                   mdata.i.DT_FIM := TRUE;
                   mdata.i.LOGRADOURO := TRUE;
                   mdata.i.NUMERO := TRUE;
                   mdata.i.COMPLEMENTO := TRUE;
                   mdata.i.BAIRRO := TRUE;
                   mdata.i.CEP := TRUE;
                   mdata.i.MUN_CODIGO := TRUE;
                   mdata.i.TELEFONE1 := TRUE;
                   mdata.i.TELEFONE2 := TRUE;
                   mdata.i.FAX := TRUE;
                   mdata.i.TELEX := TRUE;
                   mdata.i.INSCR_ESTADUAL := TRUE;
                   mdata.i.E_MAIL := TRUE;
                   mdata.i.WEB_SITE := TRUE;
                   mdata.i.MUNICIPIO := TRUE;
                   mdata.i.UNIDADE_FEDERATIVA := TRUE;
                   mdata.i.PAIS := TRUE;
                   mdata.i.ORIGEM := TRUE;
                   mdata.i.CCM := TRUE;
                   mdata.i.INSCR_SUFRAMA := TRUE;
                   mdata.i.CAIXA_POSTAL := TRUE;
                   mdata.i.CEP_CP := TRUE;
                   CORAPI_LOC_PFJ_VIG.upd(mdata);
				exception when others
				     then
                     r_put_line('Erro ao alterar LOC_PFJ_VIG DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_VIGENCIA', 'E');  
                     commit;
                     end;
				end; 
            when others
                 then
                     r_put_line('Erro ao incluir LOC_PFJ_VIG - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_LOCALIDADE_VIGENCIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_cla_pfj_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_clas_pfj%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT CP_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,PFJ_CODIGO
 FROM hon_consolida_clas_pfj_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_CLASSIFICACAO_PESSOA (CP_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,PFJ_CODIGO)
   VALUES (v_tab(i).CP_CODIGO,v_tab(i).DT_FIM,v_tab(i).DT_INICIO,upper(v_tab(i).ELEMENTO),v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).PFJ_CODIGO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update COR_CLASSIFICACAO_PESSOA
				     set DT_FIM = v_tab(i).DT_FIM
					    ,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
           	   where elemento = upper(v_tab(i).ELEMENTO)
				     and cp_codigo = v_tab(i).CP_CODIGO
					 and pfj_codigo = v_tab(i).PFJ_CODIGO
					 and dt_inicio =  v_tab(i).DT_INICIO;
			  exception when others then
                     r_put_line('Erro ao alterar Classe PFJ DUP. - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir Classe PFJ - ' || v_tab(i).pfj_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_merc_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_merc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT AGLU_CLASSE_STF_MVA_CODIGO,AGLU_STF_PAUTA_CODIGO,ALTERADO_EM,ALTERADO_POR,CEST_CODIGO,CHAVE_ORIGEM,COD_ANT_ITEM,COD_BARRA,COD_COMB,COD_GTIN,
 CODIGO_COMBUSTIVEIS_SOLVENTES,CODIGO_USUAL,COD_PROPRIO,DEPART_CODIGO,DESCRICAO,DFLT_AM_CODIGO_ENTRADA,DFLT_AM_CODIGO_SAIDA,DFLT_NBM_CODIGO,DFLT_OM_CODIGO,
 DFLT_PRECO_UNITARIO_SAIDA,DFLT_UNI_CODIGO_ENTRADA,DFLT_UNI_CODIGO_ESTOQUE,DFLT_UNI_CODIGO_SAIDA,EXEMPLO_PARA_MANUAL,FAMILIA_CODIGO,GRUPO_CODIGO,HON_CONSOLIDA_ORIGEM,
 IND_SIMILAR_NACIONAL,ITEM_COLETADO_ANP,MAPA_FISIOGRAFICO,MERCADOLOGICO,MERC_CODIGO,NOME,ORIGEM,REVISAO,SELO_IPI_CODIGO,SETOR_CODIGO,SUBFAMILIA_CODIGO,TESTE,TESTE1,
 TIPO_ITEM
 FROM hon_consolida_merc_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_MERCADORIA (AGLU_CLASSE_STF_MVA_CODIGO,AGLU_STF_PAUTA_CODIGO,ALTERADO_EM,ALTERADO_POR,
                 CEST_CODIGO,CHAVE_ORIGEM,COD_ANT_ITEM,COD_BARRA,COD_COMB,COD_GTIN,CODIGO_COMBUSTIVEIS_SOLVENTES,
                 CODIGO_USUAL,COD_PROPRIO,DEPART_CODIGO,DESCRICAO,DFLT_AM_CODIGO_ENTRADA,DFLT_AM_CODIGO_SAIDA,
                 DFLT_NBM_CODIGO,DFLT_OM_CODIGO,DFLT_PRECO_UNITARIO_SAIDA,DFLT_UNI_CODIGO_ENTRADA,
                 DFLT_UNI_CODIGO_ESTOQUE,DFLT_UNI_CODIGO_SAIDA,EXEMPLO_PARA_MANUAL,FAMILIA_CODIGO,GRUPO_CODIGO,
                 HON_CONSOLIDA_ORIGEM,IND_SIMILAR_NACIONAL,ITEM_COLETADO_ANP,MAPA_FISIOGRAFICO,MERCADOLOGICO,
                 MERC_CODIGO,NOME,ORIGEM,REVISAO,SELO_IPI_CODIGO,SETOR_CODIGO,SUBFAMILIA_CODIGO,TESTE,TESTE1,
                 TIPO_ITEM)
   VALUES (v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO,v_tab(i).AGLU_STF_PAUTA_CODIGO,v_tab(i).ALTERADO_EM,
           v_tab(i).ALTERADO_POR,v_tab(i).CEST_CODIGO,v_tab(i).CHAVE_ORIGEM,v_tab(i).COD_ANT_ITEM,
           v_tab(i).COD_BARRA, v_tab(i).COD_COMB,v_tab(i).COD_GTIN,v_tab(i).CODIGO_COMBUSTIVEIS_SOLVENTES,
           v_tab(i).CODIGO_USUAL,v_tab(i).COD_PROPRIO,v_tab(i).DEPART_CODIGO,v_tab(i).DESCRICAO,
           v_tab(i).DFLT_AM_CODIGO_ENTRADA,v_tab(i).DFLT_AM_CODIGO_SAIDA,v_tab(i).DFLT_NBM_CODIGO,
           v_tab(i).DFLT_OM_CODIGO, v_tab(i).DFLT_PRECO_UNITARIO_SAIDA,v_tab(i).DFLT_UNI_CODIGO_ENTRADA,
           v_tab(i).DFLT_UNI_CODIGO_ESTOQUE,v_tab(i).DFLT_UNI_CODIGO_SAIDA,v_tab(i).EXEMPLO_PARA_MANUAL,
           v_tab(i).FAMILIA_CODIGO,v_tab(i).GRUPO_CODIGO,v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).IND_SIMILAR_NACIONAL,v_tab(i).ITEM_COLETADO_ANP, v_tab(i).MAPA_FISIOGRAFICO,
           v_tab(i).MERCADOLOGICO,v_tab(i).MERC_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).REVISAO,
           v_tab(i).SELO_IPI_CODIGO,v_tab(i).SETOR_CODIGO,v_tab(i).SUBFAMILIA_CODIGO,v_tab(i).TESTE,
           v_tab(i).TESTE1,v_tab(i).TIPO_ITEM);
    exception when DUP_VAL_ON_INDEX 
	        then 
			   begin
                  UPDATE  COR_MERCADORIA 
				     SET NOME = v_tab(i).nome
                        ,DESCRICAO = v_tab(i).DESCRICAO
                        ,DFLT_AM_CODIGO_ENTRADA = v_tab(i).DFLT_AM_CODIGO_ENTRADA
                        ,DFLT_AM_CODIGO_SAIDA = v_tab(i).DFLT_AM_CODIGO_SAIDA
                        ,DFLT_OM_CODIGO = v_tab(i).DFLT_OM_CODIGO
                        ,DFLT_NBM_CODIGO = v_tab(i).DFLT_NBM_CODIGO
                        ,DFLT_UNI_CODIGO_ENTRADA = v_tab(i).DFLT_UNI_CODIGO_ENTRADA
                        ,DFLT_UNI_CODIGO_SAIDA = v_tab(i).DFLT_UNI_CODIGO_SAIDA
                        ,DFLT_UNI_CODIGO_ESTOQUE = v_tab(i).DFLT_UNI_CODIGO_ESTOQUE
                        --,CODIGO_USUAL = v_tab(i).codigo_usual
                        --,ORIGEM = v_tab(i).ORIGEM
                        --,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
                        ,TIPO_ITEM = v_tab(i).TIPO_ITEM
                        ,COD_BARRA = v_tab(i).COD_BARRA
						--
                        ,AGLU_CLASSE_STF_MVA_CODIGO = v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO
						,AGLU_STF_PAUTA_CODIGO = v_tab(i).AGLU_STF_PAUTA_CODIGO
						,ALTERADO_EM = v_tab(i).ALTERADO_EM
						,ALTERADO_POR = v_tab(i).ALTERADO_POR
						,CEST_CODIGO = v_tab(i).CEST_CODIGO
						,COD_ANT_ITEM = v_tab(i).COD_ANT_ITEM
						,COD_COMB = v_tab(i).COD_COMB
						,COD_GTIN = v_tab(i).COD_GTIN
						,CODIGO_COMBUSTIVEIS_SOLVENTES = v_tab(i).CODIGO_COMBUSTIVEIS_SOLVENTES
						,COD_PROPRIO = v_tab(i).COD_PROPRIO
						,DEPART_CODIGO = v_tab(i).DEPART_CODIGO
						,DFLT_PRECO_UNITARIO_SAIDA = v_tab(i).DFLT_PRECO_UNITARIO_SAIDA
						,EXEMPLO_PARA_MANUAL = v_tab(i).EXEMPLO_PARA_MANUAL
						,FAMILIA_CODIGO = v_tab(i).FAMILIA_CODIGO
						,GRUPO_CODIGO = v_tab(i).GRUPO_CODIGO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,IND_SIMILAR_NACIONAL = v_tab(i).IND_SIMILAR_NACIONAL
						,ITEM_COLETADO_ANP = v_tab(i).ITEM_COLETADO_ANP
						,MAPA_FISIOGRAFICO = v_tab(i).MAPA_FISIOGRAFICO
						,MERCADOLOGICO = v_tab(i).MERCADOLOGICO
						,REVISAO = v_tab(i).REVISAO
						,SELO_IPI_CODIGO = v_tab(i).SELO_IPI_CODIGO
						,SETOR_CODIGO = v_tab(i).SETOR_CODIGO
						,SUBFAMILIA_CODIGO = v_tab(i).SUBFAMILIA_CODIGO
						,TESTE = v_tab(i).TESTE
						,TESTE1 = v_tab(i).TESTE1
                     WHERE MERC_CODIGO = v_tab(i).MERC_CODIGO;
			   exception when others then			   
                     r_put_line('Erro ao alterar Mercadoria DUP. - ' || v_tab(i).merc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_MERCADORIA', 'E');  
                     commit;
                     end;
			   end;			   
            when others
                 then
                     r_put_line('Erro ao incluir Mercadoria - ' || v_tab(i).merc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_MERCADORIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_cla_merc_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_clas_merc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGLU_CLASSE_STF_MVA_CODIGO,CMERC_CODIGO,DT_FIM,DT_INICIO,ELEMENTO,HON_CONSOLIDA_ORIGEM,ID,
  IND_AGLUTINADOR,MERC_CODIGO,UF_CODIGO
 FROM hon_consolida_clas_merc_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
     INSERT INTO COR_CLASSIFICACAO_MERCADORIA (AGLU_CLASSE_STF_MVA_CODIGO,CMERC_CODIGO,DT_FIM,DT_INICIO,
                 ELEMENTO,HON_CONSOLIDA_ORIGEM,IND_AGLUTINADOR,MERC_CODIGO,UF_CODIGO)
   VALUES (v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO,v_tab(i).CMERC_CODIGO,v_tab(i).DT_FIM,v_tab(i).DT_INICIO,
           v_tab(i).ELEMENTO,v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_AGLUTINADOR,v_tab(i).MERC_CODIGO,
           v_tab(i).UF_CODIGO);
       exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
                 UPDATE  COR_CLASSIFICACAO_MERCADORIA 
				    SET DT_FIM = v_tab(i).DT_FIM
					  , AGLU_CLASSE_STF_MVA_CODIGO = v_tab(i).AGLU_CLASSE_STF_MVA_CODIGO
					  , HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
					  , IND_AGLUTINADOR = v_tab(i).IND_AGLUTINADOR	  
                  WHERE ELEMENTO = v_tab(i).ELEMENTO
                    AND CMERC_CODIGO = v_tab(i).CMERC_CODIGO
                    AND MERC_CODIGO = v_tab(i).MERC_CODIGO
                    AND DT_INICIO = v_tab(i).DT_INICIO
                    AND UF_CODIGO = v_tab(i).UF_CODIGO ;
			  exception when others then
                     r_put_line('Erro ao alterar Classe Merc. DUP. - ' || v_tab(i).cmerc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_MERCADORIA', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir Classe Merc. - ' || v_tab(i).cmerc_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_MERCADORIA', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_prest_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_prest%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT CHAVE_ORIGEM,CLASSIF_CODIGO,DESCRICAO,HON_CONSOLIDA_ORIGEM,NBS_CODIGO,NOME,ORIGEM,PRES_CODIGO,REVISAO
 FROM hon_consolida_prest_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_PRESTACAO (CHAVE_ORIGEM,CLASSIF_CODIGO,DESCRICAO,HON_CONSOLIDA_ORIGEM,NBS_CODIGO,
                 NOME,ORIGEM,PRES_CODIGO,REVISAO)
   VALUES (v_tab(i).CHAVE_ORIGEM,v_tab(i).CLASSIF_CODIGO,v_tab(i).DESCRICAO,v_tab(i).HON_CONSOLIDA_ORIGEM,
           v_tab(i).NBS_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).PRES_CODIGO,v_tab(i).REVISAO);
      exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_prestacao
				     set CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
					    --,CLASSIF_CODIGO = v_tab(i).CLASSIF_CODIGO
						,DESCRICAO = v_tab(i).DESCRICAO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,NBS_CODIGO = v_tab(i).NBS_CODIGO
						,NOME = v_tab(i).NOME
						,ORIGEM = v_tab(i).ORIGEM
						,REVISAO = v_tab(i).REVISAO
				   where pres_codigo = v_tab(i).PRES_CODIGO;
			  exception when others then
                     r_put_line('Erro ao alterar Prestacao DUP. - ' || v_tab(i).pres_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PRESTACAO', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir Prestacao - ' || v_tab(i).pres_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_PRESTACAO', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_iss_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_iss%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT ALTERADO_EM,ALTERADO_POR,CBO,CHAVE_ORIGEM,CLASSIF_CODIGO,CODRET_COP,CODRET_FIS,CODRET_JUR,
        DESCRICAO,DFLT_UNI_CODIGO,HON_CONSOLIDA_ORIGEM,IND_LC116,IND_RESP_IRRF,NBS_CODIGO,NOME,ORIGEM,
        REVISAO,SISS_CODIGO,SISS_CODIGO_LC116,TIPO_SERVICO
 FROM hon_consolida_serv_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     begin
     INSERT INTO COR_SERVICO_ISS (ALTERADO_EM,ALTERADO_POR,CBO,CHAVE_ORIGEM,CLASSIF_CODIGO,CODRET_COP,
                 CODRET_FIS,CODRET_JUR,DESCRICAO,DFLT_UNI_CODIGO,HON_CONSOLIDA_ORIGEM,IND_LC116,IND_RESP_IRRF,
                 NBS_CODIGO,NOME,ORIGEM,REVISAO,SISS_CODIGO,SISS_CODIGO_LC116,TIPO_SERVICO)
   VALUES (v_tab(i).ALTERADO_EM,v_tab(i).ALTERADO_POR,v_tab(i).CBO,v_tab(i).CHAVE_ORIGEM,v_tab(i).CLASSIF_CODIGO,
           v_tab(i).CODRET_COP,v_tab(i).CODRET_FIS,v_tab(i).CODRET_JUR,v_tab(i).DESCRICAO,
           v_tab(i).DFLT_UNI_CODIGO,v_tab(i).HON_CONSOLIDA_ORIGEM,v_tab(i).IND_LC116,v_tab(i).IND_RESP_IRRF,
           v_tab(i).NBS_CODIGO,v_tab(i).NOME,v_tab(i).ORIGEM,v_tab(i).REVISAO,v_tab(i).SISS_CODIGO,
           v_tab(i).SISS_CODIGO_LC116,v_tab(i).TIPO_SERVICO);
    exception when DUP_VAL_ON_INDEX 
	        then 
			   begin
			      update cor_servico_iss
				     set ALTERADO_EM = v_tab(i).ALTERADO_EM
					    ,ALTERADO_POR = v_tab(i).ALTERADO_POR
						,CBO = v_tab(i).CBO
						,CHAVE_ORIGEM = v_tab(i).CHAVE_ORIGEM
						--,CLASSIF_CODIGO = v_tab(i).CLASSIF_CODIGO
						,CODRET_COP = v_tab(i).CODRET_COP
						,CODRET_FIS = v_tab(i).CODRET_FIS
						,CODRET_JUR = v_tab(i).CODRET_JUR
						,DESCRICAO = v_tab(i).DESCRICAO
						,DFLT_UNI_CODIGO = v_tab(i).DFLT_UNI_CODIGO
						,HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM
						,IND_LC116 = v_tab(i).IND_LC116
						,IND_RESP_IRRF = v_tab(i).IND_RESP_IRRF
						,NBS_CODIGO = v_tab(i).NBS_CODIGO
						,NOME = v_tab(i).NOME
						,ORIGEM = v_tab(i).ORIGEM
						,REVISAO = v_tab(i).REVISAO
						,SISS_CODIGO_LC116 = v_tab(i).SISS_CODIGO_LC116
						,TIPO_SERVICO = v_tab(i).TIPO_SERVICO
				   where siss_codigo =  v_tab(i).SISS_CODIGO;
               exception when others then
                     r_put_line('Erro ao alterar Serviço DUP. - ' || v_tab(i).siss_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'E');  
                     commit;
                     end;
			   end; 
            when others
                 then
                     r_put_line('Erro ao incluir Serviço - ' || v_tab(i).siss_codigo|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
CODIGO_DO_SITE,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DOF_SEQUENCE,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
ID,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID
 FROM hon_consolida_dof_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem
;
BEGIN
  OPEN c;
  LOOP  
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_DOF (AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID)
   VALUES (v_tab(i).AGENTE,
v_tab(i).ANO_MES_EMISSAO,
v_tab(i).ANO_VEICULO,
v_tab(i).AUTENTICACAO_DIGITAL,
v_tab(i).CAD_ESPECIFICO_INSS_CEI,
v_tab(i).CERT_VEICULO,
v_tab(i).CERT_VEICULO_UF,
v_tab(i).CFOP_CODIGO,
v_tab(i).CFPS,
v_tab(i).CNPJ_COL,
v_tab(i).CNPJ_CRED_CARTAO,
v_tab(i).CNPJ_ENTG,
v_tab(i).CNX,
v_tab(i).COBRANCA_LOC_CODIGO,
v_tab(i).COBRANCA_PFJ_CODIGO,
v_tab(i).COD_AUT,
v_tab(i).COD_GRUPO_TENSAO,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).CODIGO_CONSUMO,
v_tab(i).COD_LINHA,
v_tab(i).COD_MOTIVO_CANC_SYN,
v_tab(i).COD_MUN_COL,
v_tab(i).COD_MUN_ENTG,
v_tab(i).COD_OPER_CARTAO,
v_tab(i).COD_REG_ESP_TRIBUTACAO,
v_tab(i).COMP_PRESTACAO_NOME,
v_tab(i).COMP_PRESTACAO_VALOR,
v_tab(i).CONSIG_PFJ_CODIGO,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPAG_CODIGO,
v_tab(i).CPF,
v_tab(i).CRT_CODIGO,
v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
v_tab(i).CTRL_CONTEUDO,
v_tab(i).CTRL_DT_INCLUSAO,
v_tab(i).CTRL_DT_ULT_ALTERACAO,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_FASE,
v_tab(i).CTRL_FASE_FISCAL,
v_tab(i).CTRL_OBS_CALCULO,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_RAT_FRETE,
v_tab(i).CTRL_OBS_RAT_M,
v_tab(i).CTRL_OBS_RAT_ODA,
v_tab(i).CTRL_OBS_RAT_P,
v_tab(i).CTRL_OBS_RAT_S,
v_tab(i).CTRL_OBS_RAT_SEGURO,
v_tab(i).CTRL_OBS_TOT_M,
v_tab(i).CTRL_OBS_TOT_O,
v_tab(i).CTRL_OBS_TOT_P,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_S,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_OBS_VL_PARCELAS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_tab(i).CTRL_PROCESSAMENTO,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_SITUACAO_DOF,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DEM_PTA,
v_tab(i).DESC_TITULO,
v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
v_tab(i).DESTINATARIO_LOC_CODIGO,
v_tab(i).DESTINATARIO_PFJ_CODIGO,
v_tab(i).DEST_PRINC_EST_CODIGO,
v_tab(i).DH_EMISSAO,
v_tab(i).DH_PROCESS_MANAGER,
v_tab(i).DOF_IMPORT_NUMERO,
v_tab(i).DT_CONVERSAO_MOEDA_NAC,
v_tab(i).DT_EMISSAO_TOMADOR,
v_tab(i).DT_EXE_SERV,
v_tab(i).DT_FATO_GERADOR_ATE,
v_tab(i).DT_FATO_GERADOR_IMPOSTO,
v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
v_tab(i).DT_LEITURA,
v_tab(i).DT_LIMITE_EMISSAO,
v_tab(i).DT_LIMITE_SAIDA,
v_tab(i).DT_PREV_CHEGADA,
v_tab(i).DT_PREV_EMBARQUE,
v_tab(i).DT_RECEBIMENTO_DESTINO,
v_tab(i).DT_REF_CALC_IMP,
v_tab(i).DT_REF_CPAG,
v_tab(i).DT_REFER_EXT,
v_tab(i).DT_ROMANEIO,
v_tab(i).DT_SAIDA_MERCADORIAS,
v_tab(i).EDOF_CODIGO,
v_tab(i).EDOF_CODIGO_RES,
v_tab(i).EMITENTE_LOC_CODIGO,
v_tab(i).EMITENTE_PFJ_CODIGO,
v_tab(i).ENTREGA_LOC_CODIGO,
v_tab(i).ENTREGA_PFJ_CODIGO,
v_tab(i).ESPECIALIZACAO,
v_tab(i).FATOR_FINANCIAMENTO,
v_tab(i).FERROV_SUBST_LOC_CODIGO,
v_tab(i).FERROV_SUBST_PFJ_CODIGO,
v_tab(i).FORMA_PAGTO_TRANSPORTE,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_NATUREZA_OPERACAO,
v_tab(i).HON_PRACA_PAGTO,
v_tab(i).HON_PRESTACAO_SERVICO,
v_tab(i).HON_VALOR_DESCONTO,
v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
v_tab(i).IE_COL,
v_tab(i).IE_ENTG,
v_tab(i).IM_COL,
v_tab(i).IM_ENTG,
v_tab(i).IND_ALTERACAO_TOMADOR,
v_tab(i).IND_BALSA,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_CIOT,
v_tab(i).IND_CLASSE,
v_tab(i).IND_COBRANCA_BANCARIA,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_EIX,
v_tab(i).IND_ENTRADA_SAIDA,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXPORTA_PARCELAS,
v_tab(i).IND_F0,
v_tab(i).IND_GABARITO,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LOTACAO,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_NAT_FRETE,
v_tab(i).IND_NAV,
v_tab(i).IND_NFE_AJUSTE,
v_tab(i).IND_PRECO_AJUSTADO_CPAG,
v_tab(i).IND_PRECOS_MOEDA_NAC,
v_tab(i).IND_PRES_COMPRADOR,
v_tab(i).IND_RATEIO,
v_tab(i).IND_REPERCUSSAO_FISCAL,
v_tab(i).IND_RESP_FRETE,
v_tab(i).IND_RESP_ISS_RET,
v_tab(i).IND_SIMPLES_ME_EPP,
v_tab(i).IND_SIT_TRIBUT_DOF,
v_tab(i).IND_TFA,
v_tab(i).IND_TIT,
v_tab(i).IND_TOT_VL_FATURADO,
v_tab(i).IND_USA_IF_CALC_IMP,
v_tab(i).IND_USA_IF_ESCRIT,
v_tab(i).IND_VEIC,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).INFORMANTE_EST_CODIGO,
v_tab(i).LINHA,
v_tab(i).LOCAL_EMBARQUE,
v_tab(i).LOCVIG_ID_DESTINATARIO,
v_tab(i).LOCVIG_ID_REMETENTE,
v_tab(i).MARCA_VEICULO,
v_tab(i).MDOF_CODIGO,
v_tab(i).MODELO_VEICULO,
v_tab(i).MODO_EMISSAO,
v_tab(i).MP_CODIGO,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_CODIGO_ISS,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).MUN_PRES_SERVICO,
v_tab(i).NAT_VOLUME,
v_tab(i).NFE_COD_SITUACAO,
v_tab(i).NFE_COD_SITUACAO_SYN,
v_tab(i).NFE_CONTINGENCIA,
v_tab(i).NFE_DESC_SITUACAO,
v_tab(i).NFE_ENVIADA,
v_tab(i).NFE_HORA_GMT_EMI,
v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NFE_PROTOCOLO,
v_tab(i).NF_IMPRESSORA_PADRAO,
v_tab(i).NFORM_FINAL,
v_tab(i).NFORM_INICIAL,
v_tab(i).NOME_MOTORISTA,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_APOLICE_SEGURO,
v_tab(i).NUM_AUTOR_OPER_CARTAO,
v_tab(i).NUM_CONTAINER,
v_tab(i).NUM_EMBARQUE,
v_tab(i).NUMERO,
v_tab(i).NUMERO_ATE,
v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
v_tab(i).NUM_FIM_RES,
v_tab(i).NUM_INI_RES,
v_tab(i).NUM_PASSE,
v_tab(i).NUM_PEDIDO,
v_tab(i).NUM_PLACA_VEICULO,
v_tab(i).NUM_REGISTRO_SECEX,
v_tab(i).NUM_RE_SISCOMEX,
v_tab(i).NUM_ROMANEIO,
v_tab(i).NUM_TITULO,
v_tab(i).NUM_VEICULO,
v_tab(i).NUM_VIAGEM,
v_tab(i).NUM_VOLUME_COMP,
v_tab(i).OBSERVACAO,
v_tab(i).ORDEM_EMBARQUE,
v_tab(i).OTM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
v_tab(i).PESO_BRT_COMP,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQ_COMP,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_REEMBALAGEM_KG,
v_tab(i).PFJ_CODIGO_REDESPACHANTE,
v_tab(i).PFJVIG_ID_DESTINATARIO,
v_tab(i).PFJVIG_ID_REMETENTE,
v_tab(i).PLACA_VEICULO_UF_CODIGO,
v_tab(i).POLT_CAB,
v_tab(i).PORTADOR_BANCO_NUM,
v_tab(i).PRECO_TOTAL_M,
v_tab(i).PRECO_TOTAL_O,
v_tab(i).PRECO_TOTAL_P,
v_tab(i).PRECO_TOTAL_S,
v_tab(i).PRODUTO_PREDOMINANTE,
v_tab(i).QTD_CANC,
v_tab(i).QTD_CANC_RES,
v_tab(i).QTD_CONSUMO,
v_tab(i).QTD_PASS_ORIG,
v_tab(i).QTD_PRESTACOES,
v_tab(i).REDESPACHANTE_LOC_CODIGO,
v_tab(i).REMETENTE_LOC_CODIGO,
v_tab(i).REMETENTE_PFJ_CODIGO,
v_tab(i).REM_PRINC_EST_CODIGO,
v_tab(i).RESP_FRETE_REDESP,
v_tab(i).RETIRADA_LOC_CODIGO,
v_tab(i).RETIRADA_PFJ_CODIGO,
v_tab(i).RETIRA_MERC_DESTINO,
v_tab(i).REVISAO,
v_tab(i).RNTC,
v_tab(i).RNTC_COMP,
v_tab(i).RNTC_COMP_2,
v_tab(i).SEQ_CALCULO,
v_tab(i).SERIE_SUBSERIE,
v_tab(i).SERIE_SUBSERIE_RES,
v_tab(i).SIGLA_MOEDA,
v_tab(i).SIS_CODIGO,
v_tab(i).SIS_CODIGO_CONTABILIZOU,
v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
v_tab(i).TEMPERATURA,
v_tab(i).TIPO,
v_tab(i).TIPO_CTE,
v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_PGTO,
v_tab(i).TIPO_SERVICO_CTE,
v_tab(i).TIPO_TARIFA,
v_tab(i).TIPO_TOMADOR,
v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
v_tab(i).TP_ASSINANTE,
v_tab(i).TP_INT_PAG,
v_tab(i).TP_LIGACAO,
v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
v_tab(i).TRANSPORTADOR_LOC_CODIGO,
v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
v_tab(i).TX_CONVERSAO_MOEDA_NAC,
v_tab(i).TXT_INSTRUCAO_BLOQUETO,
v_tab(i).UF_CODIGO_DESTINO,
v_tab(i).UF_CODIGO_EMITENTE,
v_tab(i).UF_CODIGO_ENTREGA,
v_tab(i).UF_CODIGO_ORIGEM,
v_tab(i).UF_CODIGO_RETIRADA,
v_tab(i).UF_CODIGO_SUBST,
v_tab(i).UF_CODIGO_TRANSP,
v_tab(i).UF_EMBARQUE,
v_tab(i).UF_VEIC_COMP,
v_tab(i).UF_VEIC_COMP_2,
v_tab(i).USUARIO_INCLUSAO,
v_tab(i).USUARIO_ULT_ALTERACAO,
v_tab(i).VEIC_DESCR,
v_tab(i).VEIC_ID_COMP,
v_tab(i).VEIC_ID_COMP_2,
v_tab(i).VL_ABAT_NT,
v_tab(i).VL_ADU,
v_tab(i).VL_ADU_ICMS,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
v_tab(i).VL_BASE_CT_STF,
v_tab(i).VL_BASE_INSS_RET_PER,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CT_STF,
v_tab(i).VL_DESCONTO_NF,
v_tab(i).VL_DESPACHO,
v_tab(i).VL_DESP_CAR_DESC,
v_tab(i).VL_DESP_PORT,
v_tab(i).VL_FISCAL_M,
v_tab(i).VL_FISCAL_O,
v_tab(i).VL_FISCAL_P,
v_tab(i).VL_FISCAL_S,
v_tab(i).VL_FRETE,
v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
v_tab(i).VL_FRETE_LIQ,
v_tab(i).VL_FRETE_MM,
v_tab(i).VL_FRETE_TRAF_MUTUO,
v_tab(i).VL_FRT_PV,
v_tab(i).VL_GRIS,
v_tab(i).VL_INSS,
v_tab(i).VL_OUTRAS_DESPESAS,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_PEDAGIO,
v_tab(i).VL_PESO_TX,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_SEC_CAT,
v_tab(i).VL_SEGURO,
v_tab(i).VL_SERV_NT,
v_tab(i).VL_TAXA_ADV,
v_tab(i).VL_TAXA_TERRESTRE,
v_tab(i).VL_TERC,
v_tab(i).VL_TFA,
v_tab(i).VL_TOTAL_BASE_COFINS,
v_tab(i).VL_TOTAL_BASE_COFINS_RET,
v_tab(i).VL_TOTAL_BASE_CSLL_RET,
v_tab(i).VL_TOTAL_BASE_ICMS,
v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
v_tab(i).VL_TOTAL_BASE_INSS,
v_tab(i).VL_TOTAL_BASE_INSS_RET,
v_tab(i).VL_TOTAL_BASE_IPI,
v_tab(i).VL_TOTAL_BASE_IRRF,
v_tab(i).VL_TOTAL_BASE_ISS,
v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
v_tab(i).VL_TOTAL_BASE_PIS_RET,
v_tab(i).VL_TOTAL_BASE_SENAT,
v_tab(i).VL_TOTAL_BASE_SEST,
v_tab(i).VL_TOTAL_BASE_STF,
v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_BASE_STF_IDO,
v_tab(i).VL_TOTAL_BASE_STT,
v_tab(i).VL_TOTAL_CARGA,
v_tab(i).VL_TOTAL_COFINS,
v_tab(i).VL_TOTAL_COFINS_RET,
v_tab(i).VL_TOTAL_CONTABIL,
v_tab(i).VL_TOTAL_CSLL_RET,
v_tab(i).VL_TOTAL_FATURADO,
v_tab(i).VL_TOTAL_FCP,
v_tab(i).VL_TOTAL_FCP_ST,
v_tab(i).VL_TOTAL_ICMS,
v_tab(i).VL_TOTAL_ICMS_DIFA,
v_tab(i).VL_TOTAL_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_ICMS_PART_REM,
v_tab(i).VL_TOTAL_II,
v_tab(i).VL_TOTAL_INSS,
v_tab(i).VL_TOTAL_INSS_RET,
v_tab(i).VL_TOTAL_INSS_RET_PER,
v_tab(i).VL_TOTAL_IOF,
v_tab(i).VL_TOTAL_IPI,
v_tab(i).VL_TOTAL_IRRF,
v_tab(i).VL_TOTAL_ISENTO_ICMS,
v_tab(i).VL_TOTAL_ISS,
v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_MAT_TERC,
v_tab(i).VL_TOTAL_OUTROS_ICMS,
v_tab(i).VL_TOTAL_PIS_PASEP,
v_tab(i).VL_TOTAL_PIS_RET,
v_tab(i).VL_TOTAL_SENAT,
v_tab(i).VL_TOTAL_SEST,
v_tab(i).VL_TOTAL_STF,
v_tab(i).VL_TOTAL_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_STF_IDO,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
v_tab(i).VL_TOTAL_STT,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TROCO,
v_tab(i).VL_TX_RED,
v_tab(i).VOO_CODIGO,
v_tab(i).VT_CODIGO,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_dof
				     set AGENTE = v_tab(i).AGENTE,
                          ANO_MES_EMISSAO = v_tab(i).ANO_MES_EMISSAO,
                          ANO_VEICULO = v_tab(i).ANO_VEICULO,
                          AUTENTICACAO_DIGITAL = v_tab(i).AUTENTICACAO_DIGITAL,
                          CAD_ESPECIFICO_INSS_CEI = v_tab(i).CAD_ESPECIFICO_INSS_CEI,
                          CERT_VEICULO = v_tab(i).CERT_VEICULO,
                          CERT_VEICULO_UF = v_tab(i).CERT_VEICULO_UF,
                          CFOP_CODIGO = v_tab(i).CFOP_CODIGO,
                          CFPS = v_tab(i).CFPS,
                          CNPJ_COL = v_tab(i).CNPJ_COL,
                          CNPJ_CRED_CARTAO = v_tab(i).CNPJ_CRED_CARTAO,
                          CNPJ_ENTG = v_tab(i).CNPJ_ENTG,
                          CNX = v_tab(i).CNX,
                          COBRANCA_LOC_CODIGO = v_tab(i).COBRANCA_LOC_CODIGO,
                          COBRANCA_PFJ_CODIGO = v_tab(i).COBRANCA_PFJ_CODIGO,
                          COD_AUT = v_tab(i).COD_AUT,
                          COD_GRUPO_TENSAO = v_tab(i).COD_GRUPO_TENSAO,
                          COD_IATA_FIM = v_tab(i).COD_IATA_FIM,
                          COD_IATA_INI = v_tab(i).COD_IATA_INI,
                          CODIGO_CONSUMO = v_tab(i).CODIGO_CONSUMO,
                          COD_LINHA = v_tab(i).COD_LINHA,
                          COD_MOTIVO_CANC_SYN = v_tab(i).COD_MOTIVO_CANC_SYN,
                          COD_MUN_COL = v_tab(i).COD_MUN_COL,
                          COD_MUN_ENTG = v_tab(i).COD_MUN_ENTG,
                          COD_OPER_CARTAO = v_tab(i).COD_OPER_CARTAO,
                          COD_REG_ESP_TRIBUTACAO = v_tab(i).COD_REG_ESP_TRIBUTACAO,
                          COMP_PRESTACAO_NOME = v_tab(i).COMP_PRESTACAO_NOME,
                          COMP_PRESTACAO_VALOR = v_tab(i).COMP_PRESTACAO_VALOR,
                          CONSIG_PFJ_CODIGO = v_tab(i).CONSIG_PFJ_CODIGO,
                          CONTA_CONTABIL = v_tab(i).CONTA_CONTABIL,
                          CPAG_CODIGO = v_tab(i).CPAG_CODIGO,
                          CPF = v_tab(i).CPF,
                          CRT_CODIGO = v_tab(i).CRT_CODIGO,
                          CTE_LOCALIZADOR_ORIGINAL = v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
                          CTRL_CONTEUDO = v_tab(i).CTRL_CONTEUDO,
                          CTRL_DT_INCLUSAO = v_tab(i).CTRL_DT_INCLUSAO,
                          CTRL_DT_ULT_ALTERACAO = v_tab(i).CTRL_DT_ULT_ALTERACAO,
                          CTRL_ENTRADA_ST = v_tab(i).CTRL_ENTRADA_ST,
                          CTRL_FASE = v_tab(i).CTRL_FASE,
                          CTRL_FASE_FISCAL = v_tab(i).CTRL_FASE_FISCAL,
                          CTRL_OBS_CALCULO = v_tab(i).CTRL_OBS_CALCULO,
                          CTRL_OBS_ENQ = v_tab(i).CTRL_OBS_ENQ,
                          CTRL_OBS_RAT_FRETE = v_tab(i).CTRL_OBS_RAT_FRETE,
                          CTRL_OBS_RAT_M = v_tab(i).CTRL_OBS_RAT_M,
                          CTRL_OBS_RAT_ODA = v_tab(i).CTRL_OBS_RAT_ODA,
                          CTRL_OBS_RAT_P = v_tab(i).CTRL_OBS_RAT_P,
                          CTRL_OBS_RAT_S = v_tab(i).CTRL_OBS_RAT_S,
                          CTRL_OBS_RAT_SEGURO = v_tab(i).CTRL_OBS_RAT_SEGURO,
                          CTRL_OBS_TOT_M = v_tab(i).CTRL_OBS_TOT_M,
                          CTRL_OBS_TOT_O = v_tab(i).CTRL_OBS_TOT_O,
                          CTRL_OBS_TOT_P = v_tab(i).CTRL_OBS_TOT_P,
                          CTRL_OBS_TOT_PESO = v_tab(i).CTRL_OBS_TOT_PESO,
                          CTRL_OBS_TOT_S = v_tab(i).CTRL_OBS_TOT_S,
                          CTRL_OBS_VL_FISCAL = v_tab(i).CTRL_OBS_VL_FISCAL,
                          CTRL_OBS_VL_PARCELAS = v_tab(i).CTRL_OBS_VL_PARCELAS,
                          CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
                          CTRL_PROCESSAMENTO = v_tab(i).CTRL_PROCESSAMENTO,
                          CTRL_SAIDA_ST = v_tab(i).CTRL_SAIDA_ST,
                          CTRL_SITUACAO_DOF = v_tab(i).CTRL_SITUACAO_DOF,
                          C4_IND_TEM_REAL = v_tab(i).C4_IND_TEM_REAL,
                          DEM_PTA = v_tab(i).DEM_PTA,
                          DESC_TITULO = v_tab(i).DESC_TITULO,
                          DESTINATARIO_CTE_LOC_CODIGO = v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
                          DESTINATARIO_CTE_PFJ_CODIGO = v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
                          DESTINATARIO_LOC_CODIGO = v_tab(i).DESTINATARIO_LOC_CODIGO,
                          DESTINATARIO_PFJ_CODIGO = v_tab(i).DESTINATARIO_PFJ_CODIGO,
                          DEST_PRINC_EST_CODIGO = v_tab(i).DEST_PRINC_EST_CODIGO,
                          DH_EMISSAO = v_tab(i).DH_EMISSAO,
                          DH_PROCESS_MANAGER = v_tab(i).DH_PROCESS_MANAGER,
                          DT_CONVERSAO_MOEDA_NAC = v_tab(i).DT_CONVERSAO_MOEDA_NAC,
                          DT_EMISSAO_TOMADOR = v_tab(i).DT_EMISSAO_TOMADOR,
                          DT_EXE_SERV = v_tab(i).DT_EXE_SERV,
                          DT_FATO_GERADOR_ATE = v_tab(i).DT_FATO_GERADOR_ATE,
                          DT_FATO_GERADOR_IMPOSTO = v_tab(i).DT_FATO_GERADOR_IMPOSTO,
                          DT_FIM_PROGRAMACAO_ENTREGA = v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
                          DT_INI_PROGRAMACAO_ENTREGA = v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
                          DT_LEITURA = v_tab(i).DT_LEITURA,
                          DT_LIMITE_EMISSAO = v_tab(i).DT_LIMITE_EMISSAO,
                          DT_LIMITE_SAIDA = v_tab(i).DT_LIMITE_SAIDA,
                          DT_PREV_CHEGADA = v_tab(i).DT_PREV_CHEGADA,
                          DT_PREV_EMBARQUE = v_tab(i).DT_PREV_EMBARQUE,
                          DT_RECEBIMENTO_DESTINO = v_tab(i).DT_RECEBIMENTO_DESTINO,
                          DT_REF_CALC_IMP = v_tab(i).DT_REF_CALC_IMP,
                          DT_REF_CPAG = v_tab(i).DT_REF_CPAG,
                          DT_REFER_EXT = v_tab(i).DT_REFER_EXT,
                          DT_ROMANEIO = v_tab(i).DT_ROMANEIO,
                          DT_SAIDA_MERCADORIAS = v_tab(i).DT_SAIDA_MERCADORIAS,
                          EDOF_CODIGO = v_tab(i).EDOF_CODIGO,
                          EDOF_CODIGO_RES = v_tab(i).EDOF_CODIGO_RES,
                          EMITENTE_LOC_CODIGO = v_tab(i).EMITENTE_LOC_CODIGO,
                          EMITENTE_PFJ_CODIGO = v_tab(i).EMITENTE_PFJ_CODIGO,
                          ENTREGA_LOC_CODIGO = v_tab(i).ENTREGA_LOC_CODIGO,
                          ENTREGA_PFJ_CODIGO = v_tab(i).ENTREGA_PFJ_CODIGO,
                          ESPECIALIZACAO = v_tab(i).ESPECIALIZACAO,
                          FATOR_FINANCIAMENTO = v_tab(i).FATOR_FINANCIAMENTO,
                          FERROV_SUBST_LOC_CODIGO = v_tab(i).FERROV_SUBST_LOC_CODIGO,
                          FERROV_SUBST_PFJ_CODIGO = v_tab(i).FERROV_SUBST_PFJ_CODIGO,
                          FORMA_PAGTO_TRANSPORTE = v_tab(i).FORMA_PAGTO_TRANSPORTE,
                          HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
                          HON_NATUREZA_OPERACAO = v_tab(i).HON_NATUREZA_OPERACAO,
                          HON_PRACA_PAGTO = v_tab(i).HON_PRACA_PAGTO,
                          HON_PRESTACAO_SERVICO = v_tab(i).HON_PRESTACAO_SERVICO,
                          HON_VALOR_DESCONTO = v_tab(i).HON_VALOR_DESCONTO,
                          HR_FIM_PROGRAMACAO_ENTREGA = v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
                          HR_INI_PROGRAMACAO_ENTREGA = v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
                          IE_COL = v_tab(i).IE_COL,
                          IE_ENTG = v_tab(i).IE_ENTG,
                          IM_COL = v_tab(i).IM_COL,
                          IM_ENTG = v_tab(i).IM_ENTG,
                          IND_ALTERACAO_TOMADOR = v_tab(i).IND_ALTERACAO_TOMADOR,
                          IND_BALSA = v_tab(i).IND_BALSA,
                          IND_BLOQUEADO = v_tab(i).IND_BLOQUEADO,
                          IND_CIOT = v_tab(i).IND_CIOT,
                          IND_CLASSE = v_tab(i).IND_CLASSE,
                          IND_COBRANCA_BANCARIA = v_tab(i).IND_COBRANCA_BANCARIA,
                          IND_CONTABILIZACAO = v_tab(i).IND_CONTABILIZACAO,
                          IND_EIX = v_tab(i).IND_EIX,
                          IND_ENTRADA_SAIDA = v_tab(i).IND_ENTRADA_SAIDA,
                          IND_ESCRITURACAO = v_tab(i).IND_ESCRITURACAO,
                          IND_EXPORTA_PARCELAS = v_tab(i).IND_EXPORTA_PARCELAS,
                          IND_F0 = v_tab(i).IND_F0,
                          IND_GABARITO = v_tab(i).IND_GABARITO,
                          IND_INCIDENCIA_COFINS_RET = v_tab(i).IND_INCIDENCIA_COFINS_RET,
                          IND_INCIDENCIA_CSLL_RET = v_tab(i).IND_INCIDENCIA_CSLL_RET,
                          IND_INCIDENCIA_INSS_RET = v_tab(i).IND_INCIDENCIA_INSS_RET,
                          IND_INCIDENCIA_IRRF = v_tab(i).IND_INCIDENCIA_IRRF,
                          IND_INCIDENCIA_PIS_RET = v_tab(i).IND_INCIDENCIA_PIS_RET,
                          IND_ISS_RETIDO_FONTE = v_tab(i).IND_ISS_RETIDO_FONTE,
                          IND_LOTACAO = v_tab(i).IND_LOTACAO,
                          IND_MOVIMENTA_ESTOQUE = v_tab(i).IND_MOVIMENTA_ESTOQUE,
                          IND_NAT_FRETE = v_tab(i).IND_NAT_FRETE,
                          IND_NAV = v_tab(i).IND_NAV,
                          IND_NFE_AJUSTE = v_tab(i).IND_NFE_AJUSTE,
                          IND_PRECO_AJUSTADO_CPAG = v_tab(i).IND_PRECO_AJUSTADO_CPAG,
                          IND_PRECOS_MOEDA_NAC = v_tab(i).IND_PRECOS_MOEDA_NAC,
                          IND_PRES_COMPRADOR = v_tab(i).IND_PRES_COMPRADOR,
                          IND_RATEIO = v_tab(i).IND_RATEIO,
                          IND_REPERCUSSAO_FISCAL = v_tab(i).IND_REPERCUSSAO_FISCAL,
                          IND_RESP_FRETE = v_tab(i).IND_RESP_FRETE,
                          IND_RESP_ISS_RET = v_tab(i).IND_RESP_ISS_RET,
                          IND_SIMPLES_ME_EPP = v_tab(i).IND_SIMPLES_ME_EPP,
                          IND_SIT_TRIBUT_DOF = v_tab(i).IND_SIT_TRIBUT_DOF,
                          IND_TFA = v_tab(i).IND_TFA,
                          IND_TIT = v_tab(i).IND_TIT,
                          IND_TOT_VL_FATURADO = v_tab(i).IND_TOT_VL_FATURADO,
                          IND_USA_IF_CALC_IMP = v_tab(i).IND_USA_IF_CALC_IMP,
                          IND_USA_IF_ESCRIT = v_tab(i).IND_USA_IF_ESCRIT,
                          IND_VEIC = v_tab(i).IND_VEIC,
                          IND_VERIFICA_PEDIDO = v_tab(i).IND_VERIFICA_PEDIDO,
                          LINHA = v_tab(i).LINHA,
                          LOCAL_EMBARQUE = v_tab(i).LOCAL_EMBARQUE,
                          LOCVIG_ID_DESTINATARIO = v_tab(i).LOCVIG_ID_DESTINATARIO,
                          LOCVIG_ID_REMETENTE = v_tab(i).LOCVIG_ID_REMETENTE,
                          MARCA_VEICULO = v_tab(i).MARCA_VEICULO,
                          MDOF_CODIGO = v_tab(i).MDOF_CODIGO,
                          MODELO_VEICULO = v_tab(i).MODELO_VEICULO,
                          MODO_EMISSAO = v_tab(i).MODO_EMISSAO,
                          MP_CODIGO = v_tab(i).MP_CODIGO,
                          MUN_COD_DESTINO = v_tab(i).MUN_COD_DESTINO,
                          MUN_CODIGO_ISS = v_tab(i).MUN_CODIGO_ISS,
                          MUN_COD_ORIGEM = v_tab(i).MUN_COD_ORIGEM,
                          MUN_PRES_SERVICO = v_tab(i).MUN_PRES_SERVICO,
                          NAT_VOLUME = v_tab(i).NAT_VOLUME,
                          NFE_COD_SITUACAO = v_tab(i).NFE_COD_SITUACAO,
                          NFE_COD_SITUACAO_SYN = v_tab(i).NFE_COD_SITUACAO_SYN,
                          NFE_CONTINGENCIA = v_tab(i).NFE_CONTINGENCIA,
                          NFE_DESC_SITUACAO = v_tab(i).NFE_DESC_SITUACAO,
                          NFE_ENVIADA = v_tab(i).NFE_ENVIADA,
                          NFE_HORA_GMT_EMI = v_tab(i).NFE_HORA_GMT_EMI,
                          NFE_JUSTIFICATIVA_CANCELAMENTO= v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
                          NFE_LOCALIZADOR = v_tab(i).NFE_LOCALIZADOR,
                          NFE_PROTOCOLO = v_tab(i).NFE_PROTOCOLO,
                          NF_IMPRESSORA_PADRAO = v_tab(i).NF_IMPRESSORA_PADRAO,
                          NFORM_FINAL = v_tab(i).NFORM_FINAL,
                          NFORM_INICIAL = v_tab(i).NFORM_INICIAL,
                          NOME_MOTORISTA = v_tab(i).NOME_MOTORISTA,
                          NOP_CODIGO = v_tab(i).NOP_CODIGO,
                          NUM_APOLICE_SEGURO = v_tab(i).NUM_APOLICE_SEGURO,
                          NUM_AUTOR_OPER_CARTAO = v_tab(i).NUM_AUTOR_OPER_CARTAO,
                          NUM_CONTAINER = v_tab(i).NUM_CONTAINER,
                          NUM_EMBARQUE = v_tab(i).NUM_EMBARQUE,
                          NUMERO = v_tab(i).NUMERO,
                          NUMERO_ATE = v_tab(i).NUMERO_ATE,
                          NUMERO_CONTRATO_FERROVIARIO = v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
                          NUM_FIM_RES = v_tab(i).NUM_FIM_RES,
                          NUM_INI_RES = v_tab(i).NUM_INI_RES,
                          NUM_PASSE = v_tab(i).NUM_PASSE,
                          NUM_PEDIDO = v_tab(i).NUM_PEDIDO,
                          NUM_PLACA_VEICULO = v_tab(i).NUM_PLACA_VEICULO,
                          NUM_REGISTRO_SECEX = v_tab(i).NUM_REGISTRO_SECEX,
                          NUM_RE_SISCOMEX = v_tab(i).NUM_RE_SISCOMEX,
                          NUM_ROMANEIO = v_tab(i).NUM_ROMANEIO,
                          NUM_TITULO = v_tab(i).NUM_TITULO,
                          NUM_VEICULO = v_tab(i).NUM_VEICULO,
                          NUM_VIAGEM = v_tab(i).NUM_VIAGEM,
                          NUM_VOLUME_COMP = v_tab(i).NUM_VOLUME_COMP,
                          OBSERVACAO = v_tab(i).OBSERVACAO,
                          ORDEM_EMBARQUE = v_tab(i).ORDEM_EMBARQUE,
                          OTM = v_tab(i).OTM,
                          PERC_AJUSTE_PRECO_TOTAL_M = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
                          PERC_AJUSTE_PRECO_TOTAL_P = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
                          PERC_AJUSTE_PRECO_TOTAL_S = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
                          PESO_BRT_COMP = v_tab(i).PESO_BRT_COMP,
                          PESO_BRUTO_KG = v_tab(i).PESO_BRUTO_KG,
                          PESO_LIQ_COMP = v_tab(i).PESO_LIQ_COMP,
                          PESO_LIQUIDO_KG = v_tab(i).PESO_LIQUIDO_KG,
                          PESO_REEMBALAGEM_KG = v_tab(i).PESO_REEMBALAGEM_KG,
                          PFJ_CODIGO_REDESPACHANTE = v_tab(i).PFJ_CODIGO_REDESPACHANTE,
                          PFJVIG_ID_DESTINATARIO = v_tab(i).PFJVIG_ID_DESTINATARIO,
                          PFJVIG_ID_REMETENTE = v_tab(i).PFJVIG_ID_REMETENTE,
                          PLACA_VEICULO_UF_CODIGO = v_tab(i).PLACA_VEICULO_UF_CODIGO,
                          POLT_CAB = v_tab(i).POLT_CAB,
                          PORTADOR_BANCO_NUM = v_tab(i).PORTADOR_BANCO_NUM,
                          PRECO_TOTAL_M = v_tab(i).PRECO_TOTAL_M,
                          PRECO_TOTAL_O = v_tab(i).PRECO_TOTAL_O,
                          PRECO_TOTAL_P = v_tab(i).PRECO_TOTAL_P,
                          PRECO_TOTAL_S = v_tab(i).PRECO_TOTAL_S,
                          PRODUTO_PREDOMINANTE = v_tab(i).PRODUTO_PREDOMINANTE,
                          QTD_CANC = v_tab(i).QTD_CANC,
                          QTD_CANC_RES = v_tab(i).QTD_CANC_RES,
                          QTD_CONSUMO = v_tab(i).QTD_CONSUMO,
                          QTD_PASS_ORIG = v_tab(i).QTD_PASS_ORIG,
                          QTD_PRESTACOES = v_tab(i).QTD_PRESTACOES,
                          REDESPACHANTE_LOC_CODIGO = v_tab(i).REDESPACHANTE_LOC_CODIGO,
                          REMETENTE_LOC_CODIGO = v_tab(i).REMETENTE_LOC_CODIGO,
                          REMETENTE_PFJ_CODIGO = v_tab(i).REMETENTE_PFJ_CODIGO,
                          REM_PRINC_EST_CODIGO = v_tab(i).REM_PRINC_EST_CODIGO,
                          RESP_FRETE_REDESP = v_tab(i).RESP_FRETE_REDESP,
                          RETIRADA_LOC_CODIGO = v_tab(i).RETIRADA_LOC_CODIGO,
                          RETIRADA_PFJ_CODIGO = v_tab(i).RETIRADA_PFJ_CODIGO,
                          RETIRA_MERC_DESTINO = v_tab(i).RETIRA_MERC_DESTINO,
                          REVISAO = v_tab(i).REVISAO,
                          RNTC = v_tab(i).RNTC,
                          RNTC_COMP = v_tab(i).RNTC_COMP,
                          RNTC_COMP_2 = v_tab(i).RNTC_COMP_2,
                          SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
                          SERIE_SUBSERIE = v_tab(i).SERIE_SUBSERIE,
                          SERIE_SUBSERIE_RES = v_tab(i).SERIE_SUBSERIE_RES,
                          SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
                          SIS_CODIGO = v_tab(i).SIS_CODIGO,
                          SIS_CODIGO_CONTABILIZOU = v_tab(i).SIS_CODIGO_CONTABILIZOU,
                          SUBSTITUIDO_PFJ_CODIGO = v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
                          TEMPERATURA = v_tab(i).TEMPERATURA,
                          TIPO = v_tab(i).TIPO,
                          TIPO_CTE = v_tab(i).TIPO_CTE,
                          TIPO_DT_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
                          TIPO_HR_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
                          TIPO_PGTO = v_tab(i).TIPO_PGTO,
                          TIPO_SERVICO_CTE = v_tab(i).TIPO_SERVICO_CTE,
                          TIPO_TARIFA = v_tab(i).TIPO_TARIFA,
                          TIPO_TOMADOR = v_tab(i).TIPO_TOMADOR,
                          TIPO_TRAFEGO_FERROVIARIO = v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
                          TOMADOR_OUTROS_PFJ_CODIGO = v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
                          TP_ASSINANTE = v_tab(i).TP_ASSINANTE,
                          TP_INT_PAG = v_tab(i).TP_INT_PAG,
                          TP_LIGACAO = v_tab(i).TP_LIGACAO,
                          TRAF_MUTUO_FERROV_EMITENTE = v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
                          TRAF_MUTUO_FLUXO_FERROV = v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
                          TRAF_MUTUO_IDENTIF_TREM = v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
                          TRAF_MUTUO_RESP_FATURAMENTO = v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
                          TRANSF_DT_FATO_GER_IMPOSTO = v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
                          TRANSF_NOP_CODIGO_ENTRADA = v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
                          TRANSPORTADOR_LOC_CODIGO = v_tab(i).TRANSPORTADOR_LOC_CODIGO,
                          TRANSPORTADOR_PFJ_CODIGO = v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
                          TX_CONVERSAO_MOEDA_NAC = v_tab(i).TX_CONVERSAO_MOEDA_NAC,
                          TXT_INSTRUCAO_BLOQUETO = v_tab(i).TXT_INSTRUCAO_BLOQUETO,
                          UF_CODIGO_DESTINO = v_tab(i).UF_CODIGO_DESTINO,
                          UF_CODIGO_EMITENTE = v_tab(i).UF_CODIGO_EMITENTE,
                          UF_CODIGO_ENTREGA = v_tab(i).UF_CODIGO_ENTREGA,
                          UF_CODIGO_ORIGEM = v_tab(i).UF_CODIGO_ORIGEM,
                          UF_CODIGO_RETIRADA = v_tab(i).UF_CODIGO_RETIRADA,
                          UF_CODIGO_SUBST = v_tab(i).UF_CODIGO_SUBST,
                          UF_CODIGO_TRANSP = v_tab(i).UF_CODIGO_TRANSP,
                          UF_EMBARQUE = v_tab(i).UF_EMBARQUE,
                          UF_VEIC_COMP = v_tab(i).UF_VEIC_COMP,
                          UF_VEIC_COMP_2 = v_tab(i).UF_VEIC_COMP_2,
                          USUARIO_INCLUSAO = v_tab(i).USUARIO_INCLUSAO,
                          USUARIO_ULT_ALTERACAO = v_tab(i).USUARIO_ULT_ALTERACAO,
                          VEIC_DESCR = v_tab(i).VEIC_DESCR,
                          VEIC_ID_COMP = v_tab(i).VEIC_ID_COMP,
                          VEIC_ID_COMP_2 = v_tab(i).VEIC_ID_COMP_2,
                          VL_ABAT_NT = v_tab(i).VL_ABAT_NT,
                          VL_ADU = v_tab(i).VL_ADU,
                          VL_ADU_ICMS = v_tab(i).VL_ADU_ICMS,
                          VL_AJUSTE_PRECO_TOTAL_M = v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
                          VL_AJUSTE_PRECO_TOTAL_P = v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
                          VL_AJUSTE_PRECO_TOTAL_S = v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
                          VL_BASE_CT_STF = v_tab(i).VL_BASE_CT_STF,
                          VL_BASE_INSS_RET_PER = v_tab(i).VL_BASE_INSS_RET_PER,
                          VL_COFINS_ST = v_tab(i).VL_COFINS_ST,
                          VL_CT_STF = v_tab(i).VL_CT_STF,
                          VL_DESCONTO_NF = v_tab(i).VL_DESCONTO_NF,
                          VL_DESPACHO = v_tab(i).VL_DESPACHO,
                          VL_DESP_CAR_DESC = v_tab(i).VL_DESP_CAR_DESC,
                          VL_DESP_PORT = v_tab(i).VL_DESP_PORT,
                          VL_FISCAL_M = v_tab(i).VL_FISCAL_M,
                          VL_FISCAL_O = v_tab(i).VL_FISCAL_O,
                          VL_FISCAL_P = v_tab(i).VL_FISCAL_P,
                          VL_FISCAL_S = v_tab(i).VL_FISCAL_S,
                          VL_FRETE = v_tab(i).VL_FRETE,
                          VL_FRETE_FERR_SUBSTITUIDA = v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
                          VL_FRETE_LIQ = v_tab(i).VL_FRETE_LIQ,
                          VL_FRETE_MM = v_tab(i).VL_FRETE_MM,
                          VL_FRETE_TRAF_MUTUO = v_tab(i).VL_FRETE_TRAF_MUTUO,
                          VL_FRT_PV = v_tab(i).VL_FRT_PV,
                          VL_GRIS = v_tab(i).VL_GRIS,
                          VL_INSS = v_tab(i).VL_INSS,
                          VL_OUTRAS_DESPESAS = v_tab(i).VL_OUTRAS_DESPESAS,
                          VL_OUTROS_ABAT = v_tab(i).VL_OUTROS_ABAT,
                          VL_PEDAGIO = v_tab(i).VL_PEDAGIO,
                          VL_PESO_TX = v_tab(i).VL_PESO_TX,
                          VL_PIS_ST = v_tab(i).VL_PIS_ST,
                          VL_SEC_CAT = v_tab(i).VL_SEC_CAT,
                          VL_SEGURO = v_tab(i).VL_SEGURO,
                          VL_SERV_NT = v_tab(i).VL_SERV_NT,
                          VL_TAXA_ADV = v_tab(i).VL_TAXA_ADV,
                          VL_TAXA_TERRESTRE = v_tab(i).VL_TAXA_TERRESTRE,
                          VL_TERC = v_tab(i).VL_TERC,
                          VL_TFA = v_tab(i).VL_TFA,
                          VL_TOTAL_BASE_COFINS = v_tab(i).VL_TOTAL_BASE_COFINS,
                          VL_TOTAL_BASE_COFINS_RET = v_tab(i).VL_TOTAL_BASE_COFINS_RET,
                          VL_TOTAL_BASE_CSLL_RET = v_tab(i).VL_TOTAL_BASE_CSLL_RET,
                          VL_TOTAL_BASE_ICMS = v_tab(i).VL_TOTAL_BASE_ICMS,
                          VL_TOTAL_BASE_ICMS_DIFA = v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
                          VL_TOTAL_BASE_ICMS_FCP = v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
                          VL_TOTAL_BASE_ICMS_FCP_ST = v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
                          VL_TOTAL_BASE_ICMS_PART_DEST = v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
                          VL_TOTAL_BASE_ICMS_PART_REM = v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
                          VL_TOTAL_BASE_INSS = v_tab(i).VL_TOTAL_BASE_INSS,
                          VL_TOTAL_BASE_INSS_RET = v_tab(i).VL_TOTAL_BASE_INSS_RET,
                          VL_TOTAL_BASE_IPI = v_tab(i).VL_TOTAL_BASE_IPI,
                          VL_TOTAL_BASE_IRRF = v_tab(i).VL_TOTAL_BASE_IRRF,
                          VL_TOTAL_BASE_ISS = v_tab(i).VL_TOTAL_BASE_ISS,
                          VL_TOTAL_BASE_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
                          VL_TOTAL_BASE_PIS_PASEP = v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
                          VL_TOTAL_BASE_PIS_RET = v_tab(i).VL_TOTAL_BASE_PIS_RET,
                          VL_TOTAL_BASE_SENAT = v_tab(i).VL_TOTAL_BASE_SENAT,
                          VL_TOTAL_BASE_SEST = v_tab(i).VL_TOTAL_BASE_SEST,
                          VL_TOTAL_BASE_STF = v_tab(i).VL_TOTAL_BASE_STF,
                          VL_TOTAL_BASE_STF_FRONTEIRA = v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
                          VL_TOTAL_BASE_STF_IDO = v_tab(i).VL_TOTAL_BASE_STF_IDO,
                          VL_TOTAL_BASE_STT = v_tab(i).VL_TOTAL_BASE_STT,
                          VL_TOTAL_CARGA = v_tab(i).VL_TOTAL_CARGA,
                          VL_TOTAL_COFINS = v_tab(i).VL_TOTAL_COFINS,
                          VL_TOTAL_COFINS_RET = v_tab(i).VL_TOTAL_COFINS_RET,
                          VL_TOTAL_CONTABIL = v_tab(i).VL_TOTAL_CONTABIL,
                          VL_TOTAL_CSLL_RET = v_tab(i).VL_TOTAL_CSLL_RET,
                          VL_TOTAL_FATURADO = v_tab(i).VL_TOTAL_FATURADO,
                          VL_TOTAL_FCP = v_tab(i).VL_TOTAL_FCP,
                          VL_TOTAL_FCP_ST = v_tab(i).VL_TOTAL_FCP_ST,						  
                          VL_TOTAL_ICMS = v_tab(i).VL_TOTAL_ICMS,
                          VL_TOTAL_ICMS_DIFA = v_tab(i).VL_TOTAL_ICMS_DIFA,
                          VL_TOTAL_ICMS_PART_DEST = v_tab(i).VL_TOTAL_ICMS_PART_DEST,
                          VL_TOTAL_ICMS_PART_REM = v_tab(i).VL_TOTAL_ICMS_PART_REM,
                          VL_TOTAL_II = v_tab(i).VL_TOTAL_II,
                          VL_TOTAL_INSS = v_tab(i).VL_TOTAL_INSS,
                          VL_TOTAL_INSS_RET = v_tab(i).VL_TOTAL_INSS_RET,
                          VL_TOTAL_INSS_RET_PER = v_tab(i).VL_TOTAL_INSS_RET_PER,
                          VL_TOTAL_IOF = v_tab(i).VL_TOTAL_IOF,
                          VL_TOTAL_IPI = v_tab(i).VL_TOTAL_IPI,
                          VL_TOTAL_IRRF = v_tab(i).VL_TOTAL_IRRF,
                          VL_TOTAL_ISENTO_ICMS = v_tab(i).VL_TOTAL_ISENTO_ICMS,
                          VL_TOTAL_ISS = v_tab(i).VL_TOTAL_ISS,
                          VL_TOTAL_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
                          VL_TOTAL_MAT_TERC = v_tab(i).VL_TOTAL_MAT_TERC,
                          VL_TOTAL_OUTROS_ICMS = v_tab(i).VL_TOTAL_OUTROS_ICMS,
                          VL_TOTAL_PIS_PASEP = v_tab(i).VL_TOTAL_PIS_PASEP,
                          VL_TOTAL_PIS_RET = v_tab(i).VL_TOTAL_PIS_RET,
                          VL_TOTAL_SENAT = v_tab(i).VL_TOTAL_SENAT,
                          VL_TOTAL_SEST = v_tab(i).VL_TOTAL_SEST,
                          VL_TOTAL_STF = v_tab(i).VL_TOTAL_STF,
                          VL_TOTAL_STF_FRONTEIRA = v_tab(i).VL_TOTAL_STF_FRONTEIRA,
                          VL_TOTAL_STF_IDO = v_tab(i).VL_TOTAL_STF_IDO,
                          VL_TOTAL_STF_SUBSTITUIDO = v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
                          VL_TOTAL_STT = v_tab(i).VL_TOTAL_STT,
                          VL_TRIBUTAVEL_DIFA = v_tab(i).VL_TRIBUTAVEL_DIFA,
                          VL_TROCO = v_tab(i).VL_TROCO,
                          VL_TX_RED = v_tab(i).VL_TX_RED,
                          VOO_CODIGO = v_tab(i).VOO_CODIGO,
                          VT_CODIGO = v_tab(i).VT_CODIGO,
                          XINF_ID= v_tab(i).XINF_ID
				   where dof_import_numero = v_tab(i).dof_import_numero
				     and informante_est_codigo = v_tab(i).informante_est_codigo;
			  exception when others then
                     r_put_line('Erro ao alterar DOF DUP. - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_fserv (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
CODIGO_DO_SITE,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DOF_SEQUENCE,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
ID,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID
 FROM hon_consolida_dof_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem
   and edof_codigo in ('NFS','NFS-E')
;
BEGIN
  OPEN c;
  LOOP  
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_DOF (AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID)
   VALUES (v_tab(i).AGENTE,
v_tab(i).ANO_MES_EMISSAO,
v_tab(i).ANO_VEICULO,
v_tab(i).AUTENTICACAO_DIGITAL,
v_tab(i).CAD_ESPECIFICO_INSS_CEI,
v_tab(i).CERT_VEICULO,
v_tab(i).CERT_VEICULO_UF,
v_tab(i).CFOP_CODIGO,
v_tab(i).CFPS,
v_tab(i).CNPJ_COL,
v_tab(i).CNPJ_CRED_CARTAO,
v_tab(i).CNPJ_ENTG,
v_tab(i).CNX,
v_tab(i).COBRANCA_LOC_CODIGO,
v_tab(i).COBRANCA_PFJ_CODIGO,
v_tab(i).COD_AUT,
v_tab(i).COD_GRUPO_TENSAO,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).CODIGO_CONSUMO,
v_tab(i).COD_LINHA,
v_tab(i).COD_MOTIVO_CANC_SYN,
v_tab(i).COD_MUN_COL,
v_tab(i).COD_MUN_ENTG,
v_tab(i).COD_OPER_CARTAO,
v_tab(i).COD_REG_ESP_TRIBUTACAO,
v_tab(i).COMP_PRESTACAO_NOME,
v_tab(i).COMP_PRESTACAO_VALOR,
v_tab(i).CONSIG_PFJ_CODIGO,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPAG_CODIGO,
v_tab(i).CPF,
v_tab(i).CRT_CODIGO,
v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
v_tab(i).CTRL_CONTEUDO,
v_tab(i).CTRL_DT_INCLUSAO,
v_tab(i).CTRL_DT_ULT_ALTERACAO,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_FASE,
v_tab(i).CTRL_FASE_FISCAL,
v_tab(i).CTRL_OBS_CALCULO,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_RAT_FRETE,
v_tab(i).CTRL_OBS_RAT_M,
v_tab(i).CTRL_OBS_RAT_ODA,
v_tab(i).CTRL_OBS_RAT_P,
v_tab(i).CTRL_OBS_RAT_S,
v_tab(i).CTRL_OBS_RAT_SEGURO,
v_tab(i).CTRL_OBS_TOT_M,
v_tab(i).CTRL_OBS_TOT_O,
v_tab(i).CTRL_OBS_TOT_P,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_S,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_OBS_VL_PARCELAS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_tab(i).CTRL_PROCESSAMENTO,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_SITUACAO_DOF,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DEM_PTA,
v_tab(i).DESC_TITULO,
v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
v_tab(i).DESTINATARIO_LOC_CODIGO,
v_tab(i).DESTINATARIO_PFJ_CODIGO,
v_tab(i).DEST_PRINC_EST_CODIGO,
v_tab(i).DH_EMISSAO,
v_tab(i).DH_PROCESS_MANAGER,
v_tab(i).DOF_IMPORT_NUMERO,
v_tab(i).DT_CONVERSAO_MOEDA_NAC,
v_tab(i).DT_EMISSAO_TOMADOR,
v_tab(i).DT_EXE_SERV,
v_tab(i).DT_FATO_GERADOR_ATE,
v_tab(i).DT_FATO_GERADOR_IMPOSTO,
v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
v_tab(i).DT_LEITURA,
v_tab(i).DT_LIMITE_EMISSAO,
v_tab(i).DT_LIMITE_SAIDA,
v_tab(i).DT_PREV_CHEGADA,
v_tab(i).DT_PREV_EMBARQUE,
v_tab(i).DT_RECEBIMENTO_DESTINO,
v_tab(i).DT_REF_CALC_IMP,
v_tab(i).DT_REF_CPAG,
v_tab(i).DT_REFER_EXT,
v_tab(i).DT_ROMANEIO,
v_tab(i).DT_SAIDA_MERCADORIAS,
v_tab(i).EDOF_CODIGO,
v_tab(i).EDOF_CODIGO_RES,
v_tab(i).EMITENTE_LOC_CODIGO,
v_tab(i).EMITENTE_PFJ_CODIGO,
v_tab(i).ENTREGA_LOC_CODIGO,
v_tab(i).ENTREGA_PFJ_CODIGO,
v_tab(i).ESPECIALIZACAO,
v_tab(i).FATOR_FINANCIAMENTO,
v_tab(i).FERROV_SUBST_LOC_CODIGO,
v_tab(i).FERROV_SUBST_PFJ_CODIGO,
v_tab(i).FORMA_PAGTO_TRANSPORTE,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_NATUREZA_OPERACAO,
v_tab(i).HON_PRACA_PAGTO,
v_tab(i).HON_PRESTACAO_SERVICO,
v_tab(i).HON_VALOR_DESCONTO,
v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
v_tab(i).IE_COL,
v_tab(i).IE_ENTG,
v_tab(i).IM_COL,
v_tab(i).IM_ENTG,
v_tab(i).IND_ALTERACAO_TOMADOR,
v_tab(i).IND_BALSA,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_CIOT,
v_tab(i).IND_CLASSE,
v_tab(i).IND_COBRANCA_BANCARIA,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_EIX,
v_tab(i).IND_ENTRADA_SAIDA,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXPORTA_PARCELAS,
v_tab(i).IND_F0,
v_tab(i).IND_GABARITO,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LOTACAO,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_NAT_FRETE,
v_tab(i).IND_NAV,
v_tab(i).IND_NFE_AJUSTE,
v_tab(i).IND_PRECO_AJUSTADO_CPAG,
v_tab(i).IND_PRECOS_MOEDA_NAC,
v_tab(i).IND_PRES_COMPRADOR,
v_tab(i).IND_RATEIO,
v_tab(i).IND_REPERCUSSAO_FISCAL,
v_tab(i).IND_RESP_FRETE,
v_tab(i).IND_RESP_ISS_RET,
v_tab(i).IND_SIMPLES_ME_EPP,
v_tab(i).IND_SIT_TRIBUT_DOF,
v_tab(i).IND_TFA,
v_tab(i).IND_TIT,
v_tab(i).IND_TOT_VL_FATURADO,
v_tab(i).IND_USA_IF_CALC_IMP,
v_tab(i).IND_USA_IF_ESCRIT,
v_tab(i).IND_VEIC,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).INFORMANTE_EST_CODIGO,
v_tab(i).LINHA,
v_tab(i).LOCAL_EMBARQUE,
v_tab(i).LOCVIG_ID_DESTINATARIO,
v_tab(i).LOCVIG_ID_REMETENTE,
v_tab(i).MARCA_VEICULO,
v_tab(i).MDOF_CODIGO,
v_tab(i).MODELO_VEICULO,
v_tab(i).MODO_EMISSAO,
v_tab(i).MP_CODIGO,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_CODIGO_ISS,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).MUN_PRES_SERVICO,
v_tab(i).NAT_VOLUME,
v_tab(i).NFE_COD_SITUACAO,
v_tab(i).NFE_COD_SITUACAO_SYN,
v_tab(i).NFE_CONTINGENCIA,
v_tab(i).NFE_DESC_SITUACAO,
v_tab(i).NFE_ENVIADA,
v_tab(i).NFE_HORA_GMT_EMI,
v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NFE_PROTOCOLO,
v_tab(i).NF_IMPRESSORA_PADRAO,
v_tab(i).NFORM_FINAL,
v_tab(i).NFORM_INICIAL,
v_tab(i).NOME_MOTORISTA,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_APOLICE_SEGURO,
v_tab(i).NUM_AUTOR_OPER_CARTAO,
v_tab(i).NUM_CONTAINER,
v_tab(i).NUM_EMBARQUE,
v_tab(i).NUMERO,
v_tab(i).NUMERO_ATE,
v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
v_tab(i).NUM_FIM_RES,
v_tab(i).NUM_INI_RES,
v_tab(i).NUM_PASSE,
v_tab(i).NUM_PEDIDO,
v_tab(i).NUM_PLACA_VEICULO,
v_tab(i).NUM_REGISTRO_SECEX,
v_tab(i).NUM_RE_SISCOMEX,
v_tab(i).NUM_ROMANEIO,
v_tab(i).NUM_TITULO,
v_tab(i).NUM_VEICULO,
v_tab(i).NUM_VIAGEM,
v_tab(i).NUM_VOLUME_COMP,
v_tab(i).OBSERVACAO,
v_tab(i).ORDEM_EMBARQUE,
v_tab(i).OTM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
v_tab(i).PESO_BRT_COMP,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQ_COMP,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_REEMBALAGEM_KG,
v_tab(i).PFJ_CODIGO_REDESPACHANTE,
v_tab(i).PFJVIG_ID_DESTINATARIO,
v_tab(i).PFJVIG_ID_REMETENTE,
v_tab(i).PLACA_VEICULO_UF_CODIGO,
v_tab(i).POLT_CAB,
v_tab(i).PORTADOR_BANCO_NUM,
v_tab(i).PRECO_TOTAL_M,
v_tab(i).PRECO_TOTAL_O,
v_tab(i).PRECO_TOTAL_P,
v_tab(i).PRECO_TOTAL_S,
v_tab(i).PRODUTO_PREDOMINANTE,
v_tab(i).QTD_CANC,
v_tab(i).QTD_CANC_RES,
v_tab(i).QTD_CONSUMO,
v_tab(i).QTD_PASS_ORIG,
v_tab(i).QTD_PRESTACOES,
v_tab(i).REDESPACHANTE_LOC_CODIGO,
v_tab(i).REMETENTE_LOC_CODIGO,
v_tab(i).REMETENTE_PFJ_CODIGO,
v_tab(i).REM_PRINC_EST_CODIGO,
v_tab(i).RESP_FRETE_REDESP,
v_tab(i).RETIRADA_LOC_CODIGO,
v_tab(i).RETIRADA_PFJ_CODIGO,
v_tab(i).RETIRA_MERC_DESTINO,
v_tab(i).REVISAO,
v_tab(i).RNTC,
v_tab(i).RNTC_COMP,
v_tab(i).RNTC_COMP_2,
v_tab(i).SEQ_CALCULO,
v_tab(i).SERIE_SUBSERIE,
v_tab(i).SERIE_SUBSERIE_RES,
v_tab(i).SIGLA_MOEDA,
v_tab(i).SIS_CODIGO,
v_tab(i).SIS_CODIGO_CONTABILIZOU,
v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
v_tab(i).TEMPERATURA,
v_tab(i).TIPO,
v_tab(i).TIPO_CTE,
v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_PGTO,
v_tab(i).TIPO_SERVICO_CTE,
v_tab(i).TIPO_TARIFA,
v_tab(i).TIPO_TOMADOR,
v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
v_tab(i).TP_ASSINANTE,
v_tab(i).TP_INT_PAG,
v_tab(i).TP_LIGACAO,
v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
v_tab(i).TRANSPORTADOR_LOC_CODIGO,
v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
v_tab(i).TX_CONVERSAO_MOEDA_NAC,
v_tab(i).TXT_INSTRUCAO_BLOQUETO,
v_tab(i).UF_CODIGO_DESTINO,
v_tab(i).UF_CODIGO_EMITENTE,
v_tab(i).UF_CODIGO_ENTREGA,
v_tab(i).UF_CODIGO_ORIGEM,
v_tab(i).UF_CODIGO_RETIRADA,
v_tab(i).UF_CODIGO_SUBST,
v_tab(i).UF_CODIGO_TRANSP,
v_tab(i).UF_EMBARQUE,
v_tab(i).UF_VEIC_COMP,
v_tab(i).UF_VEIC_COMP_2,
v_tab(i).USUARIO_INCLUSAO,
v_tab(i).USUARIO_ULT_ALTERACAO,
v_tab(i).VEIC_DESCR,
v_tab(i).VEIC_ID_COMP,
v_tab(i).VEIC_ID_COMP_2,
v_tab(i).VL_ABAT_NT,
v_tab(i).VL_ADU,
v_tab(i).VL_ADU_ICMS,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
v_tab(i).VL_BASE_CT_STF,
v_tab(i).VL_BASE_INSS_RET_PER,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CT_STF,
v_tab(i).VL_DESCONTO_NF,
v_tab(i).VL_DESPACHO,
v_tab(i).VL_DESP_CAR_DESC,
v_tab(i).VL_DESP_PORT,
v_tab(i).VL_FISCAL_M,
v_tab(i).VL_FISCAL_O,
v_tab(i).VL_FISCAL_P,
v_tab(i).VL_FISCAL_S,
v_tab(i).VL_FRETE,
v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
v_tab(i).VL_FRETE_LIQ,
v_tab(i).VL_FRETE_MM,
v_tab(i).VL_FRETE_TRAF_MUTUO,
v_tab(i).VL_FRT_PV,
v_tab(i).VL_GRIS,
v_tab(i).VL_INSS,
v_tab(i).VL_OUTRAS_DESPESAS,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_PEDAGIO,
v_tab(i).VL_PESO_TX,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_SEC_CAT,
v_tab(i).VL_SEGURO,
v_tab(i).VL_SERV_NT,
v_tab(i).VL_TAXA_ADV,
v_tab(i).VL_TAXA_TERRESTRE,
v_tab(i).VL_TERC,
v_tab(i).VL_TFA,
v_tab(i).VL_TOTAL_BASE_COFINS,
v_tab(i).VL_TOTAL_BASE_COFINS_RET,
v_tab(i).VL_TOTAL_BASE_CSLL_RET,
v_tab(i).VL_TOTAL_BASE_ICMS,
v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
v_tab(i).VL_TOTAL_BASE_INSS,
v_tab(i).VL_TOTAL_BASE_INSS_RET,
v_tab(i).VL_TOTAL_BASE_IPI,
v_tab(i).VL_TOTAL_BASE_IRRF,
v_tab(i).VL_TOTAL_BASE_ISS,
v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
v_tab(i).VL_TOTAL_BASE_PIS_RET,
v_tab(i).VL_TOTAL_BASE_SENAT,
v_tab(i).VL_TOTAL_BASE_SEST,
v_tab(i).VL_TOTAL_BASE_STF,
v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_BASE_STF_IDO,
v_tab(i).VL_TOTAL_BASE_STT,
v_tab(i).VL_TOTAL_CARGA,
v_tab(i).VL_TOTAL_COFINS,
v_tab(i).VL_TOTAL_COFINS_RET,
v_tab(i).VL_TOTAL_CONTABIL,
v_tab(i).VL_TOTAL_CSLL_RET,
v_tab(i).VL_TOTAL_FATURADO,
v_tab(i).VL_TOTAL_FCP,
v_tab(i).VL_TOTAL_FCP_ST,
v_tab(i).VL_TOTAL_ICMS,
v_tab(i).VL_TOTAL_ICMS_DIFA,
v_tab(i).VL_TOTAL_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_ICMS_PART_REM,
v_tab(i).VL_TOTAL_II,
v_tab(i).VL_TOTAL_INSS,
v_tab(i).VL_TOTAL_INSS_RET,
v_tab(i).VL_TOTAL_INSS_RET_PER,
v_tab(i).VL_TOTAL_IOF,
v_tab(i).VL_TOTAL_IPI,
v_tab(i).VL_TOTAL_IRRF,
v_tab(i).VL_TOTAL_ISENTO_ICMS,
v_tab(i).VL_TOTAL_ISS,
v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_MAT_TERC,
v_tab(i).VL_TOTAL_OUTROS_ICMS,
v_tab(i).VL_TOTAL_PIS_PASEP,
v_tab(i).VL_TOTAL_PIS_RET,
v_tab(i).VL_TOTAL_SENAT,
v_tab(i).VL_TOTAL_SEST,
v_tab(i).VL_TOTAL_STF,
v_tab(i).VL_TOTAL_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_STF_IDO,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
v_tab(i).VL_TOTAL_STT,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TROCO,
v_tab(i).VL_TX_RED,
v_tab(i).VOO_CODIGO,
v_tab(i).VT_CODIGO,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_dof
				     set AGENTE = v_tab(i).AGENTE,
                          ANO_MES_EMISSAO = v_tab(i).ANO_MES_EMISSAO,
                          ANO_VEICULO = v_tab(i).ANO_VEICULO,
                          AUTENTICACAO_DIGITAL = v_tab(i).AUTENTICACAO_DIGITAL,
                          CAD_ESPECIFICO_INSS_CEI = v_tab(i).CAD_ESPECIFICO_INSS_CEI,
                          CERT_VEICULO = v_tab(i).CERT_VEICULO,
                          CERT_VEICULO_UF = v_tab(i).CERT_VEICULO_UF,
                          CFOP_CODIGO = v_tab(i).CFOP_CODIGO,
                          CFPS = v_tab(i).CFPS,
                          CNPJ_COL = v_tab(i).CNPJ_COL,
                          CNPJ_CRED_CARTAO = v_tab(i).CNPJ_CRED_CARTAO,
                          CNPJ_ENTG = v_tab(i).CNPJ_ENTG,
                          CNX = v_tab(i).CNX,
                          COBRANCA_LOC_CODIGO = v_tab(i).COBRANCA_LOC_CODIGO,
                          COBRANCA_PFJ_CODIGO = v_tab(i).COBRANCA_PFJ_CODIGO,
                          COD_AUT = v_tab(i).COD_AUT,
                          COD_GRUPO_TENSAO = v_tab(i).COD_GRUPO_TENSAO,
                          COD_IATA_FIM = v_tab(i).COD_IATA_FIM,
                          COD_IATA_INI = v_tab(i).COD_IATA_INI,
                          CODIGO_CONSUMO = v_tab(i).CODIGO_CONSUMO,
                          COD_LINHA = v_tab(i).COD_LINHA,
                          COD_MOTIVO_CANC_SYN = v_tab(i).COD_MOTIVO_CANC_SYN,
                          COD_MUN_COL = v_tab(i).COD_MUN_COL,
                          COD_MUN_ENTG = v_tab(i).COD_MUN_ENTG,
                          COD_OPER_CARTAO = v_tab(i).COD_OPER_CARTAO,
                          COD_REG_ESP_TRIBUTACAO = v_tab(i).COD_REG_ESP_TRIBUTACAO,
                          COMP_PRESTACAO_NOME = v_tab(i).COMP_PRESTACAO_NOME,
                          COMP_PRESTACAO_VALOR = v_tab(i).COMP_PRESTACAO_VALOR,
                          CONSIG_PFJ_CODIGO = v_tab(i).CONSIG_PFJ_CODIGO,
                          CONTA_CONTABIL = v_tab(i).CONTA_CONTABIL,
                          CPAG_CODIGO = v_tab(i).CPAG_CODIGO,
                          CPF = v_tab(i).CPF,
                          CRT_CODIGO = v_tab(i).CRT_CODIGO,
                          CTE_LOCALIZADOR_ORIGINAL = v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
                          CTRL_CONTEUDO = v_tab(i).CTRL_CONTEUDO,
                          CTRL_DT_INCLUSAO = v_tab(i).CTRL_DT_INCLUSAO,
                          CTRL_DT_ULT_ALTERACAO = v_tab(i).CTRL_DT_ULT_ALTERACAO,
                          CTRL_ENTRADA_ST = v_tab(i).CTRL_ENTRADA_ST,
                          CTRL_FASE = v_tab(i).CTRL_FASE,
                          CTRL_FASE_FISCAL = v_tab(i).CTRL_FASE_FISCAL,
                          CTRL_OBS_CALCULO = v_tab(i).CTRL_OBS_CALCULO,
                          CTRL_OBS_ENQ = v_tab(i).CTRL_OBS_ENQ,
                          CTRL_OBS_RAT_FRETE = v_tab(i).CTRL_OBS_RAT_FRETE,
                          CTRL_OBS_RAT_M = v_tab(i).CTRL_OBS_RAT_M,
                          CTRL_OBS_RAT_ODA = v_tab(i).CTRL_OBS_RAT_ODA,
                          CTRL_OBS_RAT_P = v_tab(i).CTRL_OBS_RAT_P,
                          CTRL_OBS_RAT_S = v_tab(i).CTRL_OBS_RAT_S,
                          CTRL_OBS_RAT_SEGURO = v_tab(i).CTRL_OBS_RAT_SEGURO,
                          CTRL_OBS_TOT_M = v_tab(i).CTRL_OBS_TOT_M,
                          CTRL_OBS_TOT_O = v_tab(i).CTRL_OBS_TOT_O,
                          CTRL_OBS_TOT_P = v_tab(i).CTRL_OBS_TOT_P,
                          CTRL_OBS_TOT_PESO = v_tab(i).CTRL_OBS_TOT_PESO,
                          CTRL_OBS_TOT_S = v_tab(i).CTRL_OBS_TOT_S,
                          CTRL_OBS_VL_FISCAL = v_tab(i).CTRL_OBS_VL_FISCAL,
                          CTRL_OBS_VL_PARCELAS = v_tab(i).CTRL_OBS_VL_PARCELAS,
                          CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
                          CTRL_PROCESSAMENTO = v_tab(i).CTRL_PROCESSAMENTO,
                          CTRL_SAIDA_ST = v_tab(i).CTRL_SAIDA_ST,
                          CTRL_SITUACAO_DOF = v_tab(i).CTRL_SITUACAO_DOF,
                          C4_IND_TEM_REAL = v_tab(i).C4_IND_TEM_REAL,
                          DEM_PTA = v_tab(i).DEM_PTA,
                          DESC_TITULO = v_tab(i).DESC_TITULO,
                          DESTINATARIO_CTE_LOC_CODIGO = v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
                          DESTINATARIO_CTE_PFJ_CODIGO = v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
                          DESTINATARIO_LOC_CODIGO = v_tab(i).DESTINATARIO_LOC_CODIGO,
                          DESTINATARIO_PFJ_CODIGO = v_tab(i).DESTINATARIO_PFJ_CODIGO,
                          DEST_PRINC_EST_CODIGO = v_tab(i).DEST_PRINC_EST_CODIGO,
                          DH_EMISSAO = v_tab(i).DH_EMISSAO,
                          DH_PROCESS_MANAGER = v_tab(i).DH_PROCESS_MANAGER,
                          DT_CONVERSAO_MOEDA_NAC = v_tab(i).DT_CONVERSAO_MOEDA_NAC,
                          DT_EMISSAO_TOMADOR = v_tab(i).DT_EMISSAO_TOMADOR,
                          DT_EXE_SERV = v_tab(i).DT_EXE_SERV,
                          DT_FATO_GERADOR_ATE = v_tab(i).DT_FATO_GERADOR_ATE,
                          DT_FATO_GERADOR_IMPOSTO = v_tab(i).DT_FATO_GERADOR_IMPOSTO,
                          DT_FIM_PROGRAMACAO_ENTREGA = v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
                          DT_INI_PROGRAMACAO_ENTREGA = v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
                          DT_LEITURA = v_tab(i).DT_LEITURA,
                          DT_LIMITE_EMISSAO = v_tab(i).DT_LIMITE_EMISSAO,
                          DT_LIMITE_SAIDA = v_tab(i).DT_LIMITE_SAIDA,
                          DT_PREV_CHEGADA = v_tab(i).DT_PREV_CHEGADA,
                          DT_PREV_EMBARQUE = v_tab(i).DT_PREV_EMBARQUE,
                          DT_RECEBIMENTO_DESTINO = v_tab(i).DT_RECEBIMENTO_DESTINO,
                          DT_REF_CALC_IMP = v_tab(i).DT_REF_CALC_IMP,
                          DT_REF_CPAG = v_tab(i).DT_REF_CPAG,
                          DT_REFER_EXT = v_tab(i).DT_REFER_EXT,
                          DT_ROMANEIO = v_tab(i).DT_ROMANEIO,
                          DT_SAIDA_MERCADORIAS = v_tab(i).DT_SAIDA_MERCADORIAS,
                          EDOF_CODIGO = v_tab(i).EDOF_CODIGO,
                          EDOF_CODIGO_RES = v_tab(i).EDOF_CODIGO_RES,
                          EMITENTE_LOC_CODIGO = v_tab(i).EMITENTE_LOC_CODIGO,
                          EMITENTE_PFJ_CODIGO = v_tab(i).EMITENTE_PFJ_CODIGO,
                          ENTREGA_LOC_CODIGO = v_tab(i).ENTREGA_LOC_CODIGO,
                          ENTREGA_PFJ_CODIGO = v_tab(i).ENTREGA_PFJ_CODIGO,
                          ESPECIALIZACAO = v_tab(i).ESPECIALIZACAO,
                          FATOR_FINANCIAMENTO = v_tab(i).FATOR_FINANCIAMENTO,
                          FERROV_SUBST_LOC_CODIGO = v_tab(i).FERROV_SUBST_LOC_CODIGO,
                          FERROV_SUBST_PFJ_CODIGO = v_tab(i).FERROV_SUBST_PFJ_CODIGO,
                          FORMA_PAGTO_TRANSPORTE = v_tab(i).FORMA_PAGTO_TRANSPORTE,
                          HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
                          HON_NATUREZA_OPERACAO = v_tab(i).HON_NATUREZA_OPERACAO,
                          HON_PRACA_PAGTO = v_tab(i).HON_PRACA_PAGTO,
                          HON_PRESTACAO_SERVICO = v_tab(i).HON_PRESTACAO_SERVICO,
                          HON_VALOR_DESCONTO = v_tab(i).HON_VALOR_DESCONTO,
                          HR_FIM_PROGRAMACAO_ENTREGA = v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
                          HR_INI_PROGRAMACAO_ENTREGA = v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
                          IE_COL = v_tab(i).IE_COL,
                          IE_ENTG = v_tab(i).IE_ENTG,
                          IM_COL = v_tab(i).IM_COL,
                          IM_ENTG = v_tab(i).IM_ENTG,
                          IND_ALTERACAO_TOMADOR = v_tab(i).IND_ALTERACAO_TOMADOR,
                          IND_BALSA = v_tab(i).IND_BALSA,
                          IND_BLOQUEADO = v_tab(i).IND_BLOQUEADO,
                          IND_CIOT = v_tab(i).IND_CIOT,
                          IND_CLASSE = v_tab(i).IND_CLASSE,
                          IND_COBRANCA_BANCARIA = v_tab(i).IND_COBRANCA_BANCARIA,
                          IND_CONTABILIZACAO = v_tab(i).IND_CONTABILIZACAO,
                          IND_EIX = v_tab(i).IND_EIX,
                          IND_ENTRADA_SAIDA = v_tab(i).IND_ENTRADA_SAIDA,
                          IND_ESCRITURACAO = v_tab(i).IND_ESCRITURACAO,
                          IND_EXPORTA_PARCELAS = v_tab(i).IND_EXPORTA_PARCELAS,
                          IND_F0 = v_tab(i).IND_F0,
                          IND_GABARITO = v_tab(i).IND_GABARITO,
                          IND_INCIDENCIA_COFINS_RET = v_tab(i).IND_INCIDENCIA_COFINS_RET,
                          IND_INCIDENCIA_CSLL_RET = v_tab(i).IND_INCIDENCIA_CSLL_RET,
                          IND_INCIDENCIA_INSS_RET = v_tab(i).IND_INCIDENCIA_INSS_RET,
                          IND_INCIDENCIA_IRRF = v_tab(i).IND_INCIDENCIA_IRRF,
                          IND_INCIDENCIA_PIS_RET = v_tab(i).IND_INCIDENCIA_PIS_RET,
                          IND_ISS_RETIDO_FONTE = v_tab(i).IND_ISS_RETIDO_FONTE,
                          IND_LOTACAO = v_tab(i).IND_LOTACAO,
                          IND_MOVIMENTA_ESTOQUE = v_tab(i).IND_MOVIMENTA_ESTOQUE,
                          IND_NAT_FRETE = v_tab(i).IND_NAT_FRETE,
                          IND_NAV = v_tab(i).IND_NAV,
                          IND_NFE_AJUSTE = v_tab(i).IND_NFE_AJUSTE,
                          IND_PRECO_AJUSTADO_CPAG = v_tab(i).IND_PRECO_AJUSTADO_CPAG,
                          IND_PRECOS_MOEDA_NAC = v_tab(i).IND_PRECOS_MOEDA_NAC,
                          IND_PRES_COMPRADOR = v_tab(i).IND_PRES_COMPRADOR,
                          IND_RATEIO = v_tab(i).IND_RATEIO,
                          IND_REPERCUSSAO_FISCAL = v_tab(i).IND_REPERCUSSAO_FISCAL,
                          IND_RESP_FRETE = v_tab(i).IND_RESP_FRETE,
                          IND_RESP_ISS_RET = v_tab(i).IND_RESP_ISS_RET,
                          IND_SIMPLES_ME_EPP = v_tab(i).IND_SIMPLES_ME_EPP,
                          IND_SIT_TRIBUT_DOF = v_tab(i).IND_SIT_TRIBUT_DOF,
                          IND_TFA = v_tab(i).IND_TFA,
                          IND_TIT = v_tab(i).IND_TIT,
                          IND_TOT_VL_FATURADO = v_tab(i).IND_TOT_VL_FATURADO,
                          IND_USA_IF_CALC_IMP = v_tab(i).IND_USA_IF_CALC_IMP,
                          IND_USA_IF_ESCRIT = v_tab(i).IND_USA_IF_ESCRIT,
                          IND_VEIC = v_tab(i).IND_VEIC,
                          IND_VERIFICA_PEDIDO = v_tab(i).IND_VERIFICA_PEDIDO,
                          LINHA = v_tab(i).LINHA,
                          LOCAL_EMBARQUE = v_tab(i).LOCAL_EMBARQUE,
                          LOCVIG_ID_DESTINATARIO = v_tab(i).LOCVIG_ID_DESTINATARIO,
                          LOCVIG_ID_REMETENTE = v_tab(i).LOCVIG_ID_REMETENTE,
                          MARCA_VEICULO = v_tab(i).MARCA_VEICULO,
                          MDOF_CODIGO = v_tab(i).MDOF_CODIGO,
                          MODELO_VEICULO = v_tab(i).MODELO_VEICULO,
                          MODO_EMISSAO = v_tab(i).MODO_EMISSAO,
                          MP_CODIGO = v_tab(i).MP_CODIGO,
                          MUN_COD_DESTINO = v_tab(i).MUN_COD_DESTINO,
                          MUN_CODIGO_ISS = v_tab(i).MUN_CODIGO_ISS,
                          MUN_COD_ORIGEM = v_tab(i).MUN_COD_ORIGEM,
                          MUN_PRES_SERVICO = v_tab(i).MUN_PRES_SERVICO,
                          NAT_VOLUME = v_tab(i).NAT_VOLUME,
                          NFE_COD_SITUACAO = v_tab(i).NFE_COD_SITUACAO,
                          NFE_COD_SITUACAO_SYN = v_tab(i).NFE_COD_SITUACAO_SYN,
                          NFE_CONTINGENCIA = v_tab(i).NFE_CONTINGENCIA,
                          NFE_DESC_SITUACAO = v_tab(i).NFE_DESC_SITUACAO,
                          NFE_ENVIADA = v_tab(i).NFE_ENVIADA,
                          NFE_HORA_GMT_EMI = v_tab(i).NFE_HORA_GMT_EMI,
                          NFE_JUSTIFICATIVA_CANCELAMENTO= v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
                          NFE_LOCALIZADOR = v_tab(i).NFE_LOCALIZADOR,
                          NFE_PROTOCOLO = v_tab(i).NFE_PROTOCOLO,
                          NF_IMPRESSORA_PADRAO = v_tab(i).NF_IMPRESSORA_PADRAO,
                          NFORM_FINAL = v_tab(i).NFORM_FINAL,
                          NFORM_INICIAL = v_tab(i).NFORM_INICIAL,
                          NOME_MOTORISTA = v_tab(i).NOME_MOTORISTA,
                          NOP_CODIGO = v_tab(i).NOP_CODIGO,
                          NUM_APOLICE_SEGURO = v_tab(i).NUM_APOLICE_SEGURO,
                          NUM_AUTOR_OPER_CARTAO = v_tab(i).NUM_AUTOR_OPER_CARTAO,
                          NUM_CONTAINER = v_tab(i).NUM_CONTAINER,
                          NUM_EMBARQUE = v_tab(i).NUM_EMBARQUE,
                          NUMERO = v_tab(i).NUMERO,
                          NUMERO_ATE = v_tab(i).NUMERO_ATE,
                          NUMERO_CONTRATO_FERROVIARIO = v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
                          NUM_FIM_RES = v_tab(i).NUM_FIM_RES,
                          NUM_INI_RES = v_tab(i).NUM_INI_RES,
                          NUM_PASSE = v_tab(i).NUM_PASSE,
                          NUM_PEDIDO = v_tab(i).NUM_PEDIDO,
                          NUM_PLACA_VEICULO = v_tab(i).NUM_PLACA_VEICULO,
                          NUM_REGISTRO_SECEX = v_tab(i).NUM_REGISTRO_SECEX,
                          NUM_RE_SISCOMEX = v_tab(i).NUM_RE_SISCOMEX,
                          NUM_ROMANEIO = v_tab(i).NUM_ROMANEIO,
                          NUM_TITULO = v_tab(i).NUM_TITULO,
                          NUM_VEICULO = v_tab(i).NUM_VEICULO,
                          NUM_VIAGEM = v_tab(i).NUM_VIAGEM,
                          NUM_VOLUME_COMP = v_tab(i).NUM_VOLUME_COMP,
                          OBSERVACAO = v_tab(i).OBSERVACAO,
                          ORDEM_EMBARQUE = v_tab(i).ORDEM_EMBARQUE,
                          OTM = v_tab(i).OTM,
                          PERC_AJUSTE_PRECO_TOTAL_M = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
                          PERC_AJUSTE_PRECO_TOTAL_P = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
                          PERC_AJUSTE_PRECO_TOTAL_S = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
                          PESO_BRT_COMP = v_tab(i).PESO_BRT_COMP,
                          PESO_BRUTO_KG = v_tab(i).PESO_BRUTO_KG,
                          PESO_LIQ_COMP = v_tab(i).PESO_LIQ_COMP,
                          PESO_LIQUIDO_KG = v_tab(i).PESO_LIQUIDO_KG,
                          PESO_REEMBALAGEM_KG = v_tab(i).PESO_REEMBALAGEM_KG,
                          PFJ_CODIGO_REDESPACHANTE = v_tab(i).PFJ_CODIGO_REDESPACHANTE,
                          PFJVIG_ID_DESTINATARIO = v_tab(i).PFJVIG_ID_DESTINATARIO,
                          PFJVIG_ID_REMETENTE = v_tab(i).PFJVIG_ID_REMETENTE,
                          PLACA_VEICULO_UF_CODIGO = v_tab(i).PLACA_VEICULO_UF_CODIGO,
                          POLT_CAB = v_tab(i).POLT_CAB,
                          PORTADOR_BANCO_NUM = v_tab(i).PORTADOR_BANCO_NUM,
                          PRECO_TOTAL_M = v_tab(i).PRECO_TOTAL_M,
                          PRECO_TOTAL_O = v_tab(i).PRECO_TOTAL_O,
                          PRECO_TOTAL_P = v_tab(i).PRECO_TOTAL_P,
                          PRECO_TOTAL_S = v_tab(i).PRECO_TOTAL_S,
                          PRODUTO_PREDOMINANTE = v_tab(i).PRODUTO_PREDOMINANTE,
                          QTD_CANC = v_tab(i).QTD_CANC,
                          QTD_CANC_RES = v_tab(i).QTD_CANC_RES,
                          QTD_CONSUMO = v_tab(i).QTD_CONSUMO,
                          QTD_PASS_ORIG = v_tab(i).QTD_PASS_ORIG,
                          QTD_PRESTACOES = v_tab(i).QTD_PRESTACOES,
                          REDESPACHANTE_LOC_CODIGO = v_tab(i).REDESPACHANTE_LOC_CODIGO,
                          REMETENTE_LOC_CODIGO = v_tab(i).REMETENTE_LOC_CODIGO,
                          REMETENTE_PFJ_CODIGO = v_tab(i).REMETENTE_PFJ_CODIGO,
                          REM_PRINC_EST_CODIGO = v_tab(i).REM_PRINC_EST_CODIGO,
                          RESP_FRETE_REDESP = v_tab(i).RESP_FRETE_REDESP,
                          RETIRADA_LOC_CODIGO = v_tab(i).RETIRADA_LOC_CODIGO,
                          RETIRADA_PFJ_CODIGO = v_tab(i).RETIRADA_PFJ_CODIGO,
                          RETIRA_MERC_DESTINO = v_tab(i).RETIRA_MERC_DESTINO,
                          REVISAO = v_tab(i).REVISAO,
                          RNTC = v_tab(i).RNTC,
                          RNTC_COMP = v_tab(i).RNTC_COMP,
                          RNTC_COMP_2 = v_tab(i).RNTC_COMP_2,
                          SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
                          SERIE_SUBSERIE = v_tab(i).SERIE_SUBSERIE,
                          SERIE_SUBSERIE_RES = v_tab(i).SERIE_SUBSERIE_RES,
                          SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
                          SIS_CODIGO = v_tab(i).SIS_CODIGO,
                          SIS_CODIGO_CONTABILIZOU = v_tab(i).SIS_CODIGO_CONTABILIZOU,
                          SUBSTITUIDO_PFJ_CODIGO = v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
                          TEMPERATURA = v_tab(i).TEMPERATURA,
                          TIPO = v_tab(i).TIPO,
                          TIPO_CTE = v_tab(i).TIPO_CTE,
                          TIPO_DT_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
                          TIPO_HR_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
                          TIPO_PGTO = v_tab(i).TIPO_PGTO,
                          TIPO_SERVICO_CTE = v_tab(i).TIPO_SERVICO_CTE,
                          TIPO_TARIFA = v_tab(i).TIPO_TARIFA,
                          TIPO_TOMADOR = v_tab(i).TIPO_TOMADOR,
                          TIPO_TRAFEGO_FERROVIARIO = v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
                          TOMADOR_OUTROS_PFJ_CODIGO = v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
                          TP_ASSINANTE = v_tab(i).TP_ASSINANTE,
                          TP_INT_PAG = v_tab(i).TP_INT_PAG,
                          TP_LIGACAO = v_tab(i).TP_LIGACAO,
                          TRAF_MUTUO_FERROV_EMITENTE = v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
                          TRAF_MUTUO_FLUXO_FERROV = v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
                          TRAF_MUTUO_IDENTIF_TREM = v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
                          TRAF_MUTUO_RESP_FATURAMENTO = v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
                          TRANSF_DT_FATO_GER_IMPOSTO = v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
                          TRANSF_NOP_CODIGO_ENTRADA = v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
                          TRANSPORTADOR_LOC_CODIGO = v_tab(i).TRANSPORTADOR_LOC_CODIGO,
                          TRANSPORTADOR_PFJ_CODIGO = v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
                          TX_CONVERSAO_MOEDA_NAC = v_tab(i).TX_CONVERSAO_MOEDA_NAC,
                          TXT_INSTRUCAO_BLOQUETO = v_tab(i).TXT_INSTRUCAO_BLOQUETO,
                          UF_CODIGO_DESTINO = v_tab(i).UF_CODIGO_DESTINO,
                          UF_CODIGO_EMITENTE = v_tab(i).UF_CODIGO_EMITENTE,
                          UF_CODIGO_ENTREGA = v_tab(i).UF_CODIGO_ENTREGA,
                          UF_CODIGO_ORIGEM = v_tab(i).UF_CODIGO_ORIGEM,
                          UF_CODIGO_RETIRADA = v_tab(i).UF_CODIGO_RETIRADA,
                          UF_CODIGO_SUBST = v_tab(i).UF_CODIGO_SUBST,
                          UF_CODIGO_TRANSP = v_tab(i).UF_CODIGO_TRANSP,
                          UF_EMBARQUE = v_tab(i).UF_EMBARQUE,
                          UF_VEIC_COMP = v_tab(i).UF_VEIC_COMP,
                          UF_VEIC_COMP_2 = v_tab(i).UF_VEIC_COMP_2,
                          USUARIO_INCLUSAO = v_tab(i).USUARIO_INCLUSAO,
                          USUARIO_ULT_ALTERACAO = v_tab(i).USUARIO_ULT_ALTERACAO,
                          VEIC_DESCR = v_tab(i).VEIC_DESCR,
                          VEIC_ID_COMP = v_tab(i).VEIC_ID_COMP,
                          VEIC_ID_COMP_2 = v_tab(i).VEIC_ID_COMP_2,
                          VL_ABAT_NT = v_tab(i).VL_ABAT_NT,
                          VL_ADU = v_tab(i).VL_ADU,
                          VL_ADU_ICMS = v_tab(i).VL_ADU_ICMS,
                          VL_AJUSTE_PRECO_TOTAL_M = v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
                          VL_AJUSTE_PRECO_TOTAL_P = v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
                          VL_AJUSTE_PRECO_TOTAL_S = v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
                          VL_BASE_CT_STF = v_tab(i).VL_BASE_CT_STF,
                          VL_BASE_INSS_RET_PER = v_tab(i).VL_BASE_INSS_RET_PER,
                          VL_COFINS_ST = v_tab(i).VL_COFINS_ST,
                          VL_CT_STF = v_tab(i).VL_CT_STF,
                          VL_DESCONTO_NF = v_tab(i).VL_DESCONTO_NF,
                          VL_DESPACHO = v_tab(i).VL_DESPACHO,
                          VL_DESP_CAR_DESC = v_tab(i).VL_DESP_CAR_DESC,
                          VL_DESP_PORT = v_tab(i).VL_DESP_PORT,
                          VL_FISCAL_M = v_tab(i).VL_FISCAL_M,
                          VL_FISCAL_O = v_tab(i).VL_FISCAL_O,
                          VL_FISCAL_P = v_tab(i).VL_FISCAL_P,
                          VL_FISCAL_S = v_tab(i).VL_FISCAL_S,
                          VL_FRETE = v_tab(i).VL_FRETE,
                          VL_FRETE_FERR_SUBSTITUIDA = v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
                          VL_FRETE_LIQ = v_tab(i).VL_FRETE_LIQ,
                          VL_FRETE_MM = v_tab(i).VL_FRETE_MM,
                          VL_FRETE_TRAF_MUTUO = v_tab(i).VL_FRETE_TRAF_MUTUO,
                          VL_FRT_PV = v_tab(i).VL_FRT_PV,
                          VL_GRIS = v_tab(i).VL_GRIS,
                          VL_INSS = v_tab(i).VL_INSS,
                          VL_OUTRAS_DESPESAS = v_tab(i).VL_OUTRAS_DESPESAS,
                          VL_OUTROS_ABAT = v_tab(i).VL_OUTROS_ABAT,
                          VL_PEDAGIO = v_tab(i).VL_PEDAGIO,
                          VL_PESO_TX = v_tab(i).VL_PESO_TX,
                          VL_PIS_ST = v_tab(i).VL_PIS_ST,
                          VL_SEC_CAT = v_tab(i).VL_SEC_CAT,
                          VL_SEGURO = v_tab(i).VL_SEGURO,
                          VL_SERV_NT = v_tab(i).VL_SERV_NT,
                          VL_TAXA_ADV = v_tab(i).VL_TAXA_ADV,
                          VL_TAXA_TERRESTRE = v_tab(i).VL_TAXA_TERRESTRE,
                          VL_TERC = v_tab(i).VL_TERC,
                          VL_TFA = v_tab(i).VL_TFA,
                          VL_TOTAL_BASE_COFINS = v_tab(i).VL_TOTAL_BASE_COFINS,
                          VL_TOTAL_BASE_COFINS_RET = v_tab(i).VL_TOTAL_BASE_COFINS_RET,
                          VL_TOTAL_BASE_CSLL_RET = v_tab(i).VL_TOTAL_BASE_CSLL_RET,
                          VL_TOTAL_BASE_ICMS = v_tab(i).VL_TOTAL_BASE_ICMS,
                          VL_TOTAL_BASE_ICMS_DIFA = v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
                          VL_TOTAL_BASE_ICMS_FCP = v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
                          VL_TOTAL_BASE_ICMS_FCP_ST = v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
                          VL_TOTAL_BASE_ICMS_PART_DEST = v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
                          VL_TOTAL_BASE_ICMS_PART_REM = v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
                          VL_TOTAL_BASE_INSS = v_tab(i).VL_TOTAL_BASE_INSS,
                          VL_TOTAL_BASE_INSS_RET = v_tab(i).VL_TOTAL_BASE_INSS_RET,
                          VL_TOTAL_BASE_IPI = v_tab(i).VL_TOTAL_BASE_IPI,
                          VL_TOTAL_BASE_IRRF = v_tab(i).VL_TOTAL_BASE_IRRF,
                          VL_TOTAL_BASE_ISS = v_tab(i).VL_TOTAL_BASE_ISS,
                          VL_TOTAL_BASE_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
                          VL_TOTAL_BASE_PIS_PASEP = v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
                          VL_TOTAL_BASE_PIS_RET = v_tab(i).VL_TOTAL_BASE_PIS_RET,
                          VL_TOTAL_BASE_SENAT = v_tab(i).VL_TOTAL_BASE_SENAT,
                          VL_TOTAL_BASE_SEST = v_tab(i).VL_TOTAL_BASE_SEST,
                          VL_TOTAL_BASE_STF = v_tab(i).VL_TOTAL_BASE_STF,
                          VL_TOTAL_BASE_STF_FRONTEIRA = v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
                          VL_TOTAL_BASE_STF_IDO = v_tab(i).VL_TOTAL_BASE_STF_IDO,
                          VL_TOTAL_BASE_STT = v_tab(i).VL_TOTAL_BASE_STT,
                          VL_TOTAL_CARGA = v_tab(i).VL_TOTAL_CARGA,
                          VL_TOTAL_COFINS = v_tab(i).VL_TOTAL_COFINS,
                          VL_TOTAL_COFINS_RET = v_tab(i).VL_TOTAL_COFINS_RET,
                          VL_TOTAL_CONTABIL = v_tab(i).VL_TOTAL_CONTABIL,
                          VL_TOTAL_CSLL_RET = v_tab(i).VL_TOTAL_CSLL_RET,
                          VL_TOTAL_FATURADO = v_tab(i).VL_TOTAL_FATURADO,
                          VL_TOTAL_FCP = v_tab(i).VL_TOTAL_FCP,
                          VL_TOTAL_FCP_ST = v_tab(i).VL_TOTAL_FCP_ST,						  
                          VL_TOTAL_ICMS = v_tab(i).VL_TOTAL_ICMS,
                          VL_TOTAL_ICMS_DIFA = v_tab(i).VL_TOTAL_ICMS_DIFA,
                          VL_TOTAL_ICMS_PART_DEST = v_tab(i).VL_TOTAL_ICMS_PART_DEST,
                          VL_TOTAL_ICMS_PART_REM = v_tab(i).VL_TOTAL_ICMS_PART_REM,
                          VL_TOTAL_II = v_tab(i).VL_TOTAL_II,
                          VL_TOTAL_INSS = v_tab(i).VL_TOTAL_INSS,
                          VL_TOTAL_INSS_RET = v_tab(i).VL_TOTAL_INSS_RET,
                          VL_TOTAL_INSS_RET_PER = v_tab(i).VL_TOTAL_INSS_RET_PER,
                          VL_TOTAL_IOF = v_tab(i).VL_TOTAL_IOF,
                          VL_TOTAL_IPI = v_tab(i).VL_TOTAL_IPI,
                          VL_TOTAL_IRRF = v_tab(i).VL_TOTAL_IRRF,
                          VL_TOTAL_ISENTO_ICMS = v_tab(i).VL_TOTAL_ISENTO_ICMS,
                          VL_TOTAL_ISS = v_tab(i).VL_TOTAL_ISS,
                          VL_TOTAL_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
                          VL_TOTAL_MAT_TERC = v_tab(i).VL_TOTAL_MAT_TERC,
                          VL_TOTAL_OUTROS_ICMS = v_tab(i).VL_TOTAL_OUTROS_ICMS,
                          VL_TOTAL_PIS_PASEP = v_tab(i).VL_TOTAL_PIS_PASEP,
                          VL_TOTAL_PIS_RET = v_tab(i).VL_TOTAL_PIS_RET,
                          VL_TOTAL_SENAT = v_tab(i).VL_TOTAL_SENAT,
                          VL_TOTAL_SEST = v_tab(i).VL_TOTAL_SEST,
                          VL_TOTAL_STF = v_tab(i).VL_TOTAL_STF,
                          VL_TOTAL_STF_FRONTEIRA = v_tab(i).VL_TOTAL_STF_FRONTEIRA,
                          VL_TOTAL_STF_IDO = v_tab(i).VL_TOTAL_STF_IDO,
                          VL_TOTAL_STF_SUBSTITUIDO = v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
                          VL_TOTAL_STT = v_tab(i).VL_TOTAL_STT,
                          VL_TRIBUTAVEL_DIFA = v_tab(i).VL_TRIBUTAVEL_DIFA,
                          VL_TROCO = v_tab(i).VL_TROCO,
                          VL_TX_RED = v_tab(i).VL_TX_RED,
                          VOO_CODIGO = v_tab(i).VOO_CODIGO,
                          VT_CODIGO = v_tab(i).VT_CODIGO,
                          XINF_ID= v_tab(i).XINF_ID
				   where dof_import_numero = v_tab(i).dof_import_numero
				     and informante_est_codigo = v_tab(i).informante_est_codigo;
			  exception when others then
                     r_put_line('Erro ao alterar DOF DUP. - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_fmerc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
CODIGO_DO_SITE,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DOF_SEQUENCE,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
ID,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID
 FROM hon_consolida_dof_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem
   and edof_codigo not in ('NFS','NFS-E')
;
BEGIN
  OPEN c;
  LOOP  
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    begin
     INSERT INTO COR_DOF (AGENTE,
ANO_MES_EMISSAO,
ANO_VEICULO,
AUTENTICACAO_DIGITAL,
CAD_ESPECIFICO_INSS_CEI,
CERT_VEICULO,
CERT_VEICULO_UF,
CFOP_CODIGO,
CFPS,
CNPJ_COL,
CNPJ_CRED_CARTAO,
CNPJ_ENTG,
CNX,
COBRANCA_LOC_CODIGO,
COBRANCA_PFJ_CODIGO,
COD_AUT,
COD_GRUPO_TENSAO,
COD_IATA_FIM,
COD_IATA_INI,
CODIGO_CONSUMO,
COD_LINHA,
COD_MOTIVO_CANC_SYN,
COD_MUN_COL,
COD_MUN_ENTG,
COD_OPER_CARTAO,
COD_REG_ESP_TRIBUTACAO,
COMP_PRESTACAO_NOME,
COMP_PRESTACAO_VALOR,
CONSIG_PFJ_CODIGO,
CONTA_CONTABIL,
CPAG_CODIGO,
CPF,
CRT_CODIGO,
CTE_LOCALIZADOR_ORIGINAL,
CTRL_CONTEUDO,
CTRL_DT_INCLUSAO,
CTRL_DT_ULT_ALTERACAO,
CTRL_ENTRADA_ST,
CTRL_FASE,
CTRL_FASE_FISCAL,
CTRL_OBS_CALCULO,
CTRL_OBS_ENQ,
CTRL_OBS_RAT_FRETE,
CTRL_OBS_RAT_M,
CTRL_OBS_RAT_ODA,
CTRL_OBS_RAT_P,
CTRL_OBS_RAT_S,
CTRL_OBS_RAT_SEGURO,
CTRL_OBS_TOT_M,
CTRL_OBS_TOT_O,
CTRL_OBS_TOT_P,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_S,
CTRL_OBS_VL_FISCAL,
CTRL_OBS_VL_PARCELAS,
CTRL_ORIGEM_REGISTRO,
CTRL_PROCESSAMENTO,
CTRL_SAIDA_ST,
CTRL_SITUACAO_DOF,
C4_IND_TEM_REAL,
DEM_PTA,
DESC_TITULO,
DESTINATARIO_CTE_LOC_CODIGO,
DESTINATARIO_CTE_PFJ_CODIGO,
DESTINATARIO_LOC_CODIGO,
DESTINATARIO_PFJ_CODIGO,
DEST_PRINC_EST_CODIGO,
DH_EMISSAO,
DH_PROCESS_MANAGER,
DOF_IMPORT_NUMERO,
DT_CONVERSAO_MOEDA_NAC,
DT_EMISSAO_TOMADOR,
DT_EXE_SERV,
DT_FATO_GERADOR_ATE,
DT_FATO_GERADOR_IMPOSTO,
DT_FIM_PROGRAMACAO_ENTREGA,
DT_INI_PROGRAMACAO_ENTREGA,
DT_LEITURA,
DT_LIMITE_EMISSAO,
DT_LIMITE_SAIDA,
DT_PREV_CHEGADA,
DT_PREV_EMBARQUE,
DT_RECEBIMENTO_DESTINO,
DT_REF_CALC_IMP,
DT_REF_CPAG,
DT_REFER_EXT,
DT_ROMANEIO,
DT_SAIDA_MERCADORIAS,
EDOF_CODIGO,
EDOF_CODIGO_RES,
EMITENTE_LOC_CODIGO,
EMITENTE_PFJ_CODIGO,
ENTREGA_LOC_CODIGO,
ENTREGA_PFJ_CODIGO,
ESPECIALIZACAO,
FATOR_FINANCIAMENTO,
FERROV_SUBST_LOC_CODIGO,
FERROV_SUBST_PFJ_CODIGO,
FORMA_PAGTO_TRANSPORTE,
HON_CONSOLIDA_ORIGEM,
HON_NATUREZA_OPERACAO,
HON_PRACA_PAGTO,
HON_PRESTACAO_SERVICO,
HON_VALOR_DESCONTO,
HR_FIM_PROGRAMACAO_ENTREGA,
HR_INI_PROGRAMACAO_ENTREGA,
IE_COL,
IE_ENTG,
IM_COL,
IM_ENTG,
IND_ALTERACAO_TOMADOR,
IND_BALSA,
IND_BLOQUEADO,
IND_CIOT,
IND_CLASSE,
IND_COBRANCA_BANCARIA,
IND_CONTABILIZACAO,
IND_EIX,
IND_ENTRADA_SAIDA,
IND_ESCRITURACAO,
IND_EXPORTA_PARCELAS,
IND_F0,
IND_GABARITO,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_PIS_RET,
IND_ISS_RETIDO_FONTE,
IND_LOTACAO,
IND_MOVIMENTA_ESTOQUE,
IND_NAT_FRETE,
IND_NAV,
IND_NFE_AJUSTE,
IND_PRECO_AJUSTADO_CPAG,
IND_PRECOS_MOEDA_NAC,
IND_PRES_COMPRADOR,
IND_RATEIO,
IND_REPERCUSSAO_FISCAL,
IND_RESP_FRETE,
IND_RESP_ISS_RET,
IND_SIMPLES_ME_EPP,
IND_SIT_TRIBUT_DOF,
IND_TFA,
IND_TIT,
IND_TOT_VL_FATURADO,
IND_USA_IF_CALC_IMP,
IND_USA_IF_ESCRIT,
IND_VEIC,
IND_VERIFICA_PEDIDO,
INFORMANTE_EST_CODIGO,
LINHA,
LOCAL_EMBARQUE,
LOCVIG_ID_DESTINATARIO,
LOCVIG_ID_REMETENTE,
MARCA_VEICULO,
MDOF_CODIGO,
MODELO_VEICULO,
MODO_EMISSAO,
MP_CODIGO,
MUN_COD_DESTINO,
MUN_CODIGO_ISS,
MUN_COD_ORIGEM,
MUN_PRES_SERVICO,
NAT_VOLUME,
NFE_COD_SITUACAO,
NFE_COD_SITUACAO_SYN,
NFE_CONTINGENCIA,
NFE_DESC_SITUACAO,
NFE_ENVIADA,
NFE_HORA_GMT_EMI,
NFE_JUSTIFICATIVA_CANCELAMENTO,
NFE_LOCALIZADOR,
NFE_PROTOCOLO,
NF_IMPRESSORA_PADRAO,
NFORM_FINAL,
NFORM_INICIAL,
NOME_MOTORISTA,
NOP_CODIGO,
NUM_APOLICE_SEGURO,
NUM_AUTOR_OPER_CARTAO,
NUM_CONTAINER,
NUM_EMBARQUE,
NUMERO,
NUMERO_ATE,
NUMERO_CONTRATO_FERROVIARIO,
NUM_FIM_RES,
NUM_INI_RES,
NUM_PASSE,
NUM_PEDIDO,
NUM_PLACA_VEICULO,
NUM_REGISTRO_SECEX,
NUM_RE_SISCOMEX,
NUM_ROMANEIO,
NUM_TITULO,
NUM_VEICULO,
NUM_VIAGEM,
NUM_VOLUME_COMP,
OBSERVACAO,
ORDEM_EMBARQUE,
OTM,
PERC_AJUSTE_PRECO_TOTAL_M,
PERC_AJUSTE_PRECO_TOTAL_P,
PERC_AJUSTE_PRECO_TOTAL_S,
PESO_BRT_COMP,
PESO_BRUTO_KG,
PESO_LIQ_COMP,
PESO_LIQUIDO_KG,
PESO_REEMBALAGEM_KG,
PFJ_CODIGO_REDESPACHANTE,
PFJVIG_ID_DESTINATARIO,
PFJVIG_ID_REMETENTE,
PLACA_VEICULO_UF_CODIGO,
POLT_CAB,
PORTADOR_BANCO_NUM,
PRECO_TOTAL_M,
PRECO_TOTAL_O,
PRECO_TOTAL_P,
PRECO_TOTAL_S,
PRODUTO_PREDOMINANTE,
QTD_CANC,
QTD_CANC_RES,
QTD_CONSUMO,
QTD_PASS_ORIG,
QTD_PRESTACOES,
REDESPACHANTE_LOC_CODIGO,
REMETENTE_LOC_CODIGO,
REMETENTE_PFJ_CODIGO,
REM_PRINC_EST_CODIGO,
RESP_FRETE_REDESP,
RETIRADA_LOC_CODIGO,
RETIRADA_PFJ_CODIGO,
RETIRA_MERC_DESTINO,
REVISAO,
RNTC,
RNTC_COMP,
RNTC_COMP_2,
SEQ_CALCULO,
SERIE_SUBSERIE,
SERIE_SUBSERIE_RES,
SIGLA_MOEDA,
SIS_CODIGO,
SIS_CODIGO_CONTABILIZOU,
SUBSTITUIDO_PFJ_CODIGO,
TEMPERATURA,
TIPO,
TIPO_CTE,
TIPO_DT_PROGRAMACAO_ENTREGA,
TIPO_HR_PROGRAMACAO_ENTREGA,
TIPO_PGTO,
TIPO_SERVICO_CTE,
TIPO_TARIFA,
TIPO_TOMADOR,
TIPO_TRAFEGO_FERROVIARIO,
TOMADOR_OUTROS_PFJ_CODIGO,
TP_ASSINANTE,
TP_INT_PAG,
TP_LIGACAO,
TRAF_MUTUO_FERROV_EMITENTE,
TRAF_MUTUO_FLUXO_FERROV,
TRAF_MUTUO_IDENTIF_TREM,
TRAF_MUTUO_RESP_FATURAMENTO,
TRANSF_DT_FATO_GER_IMPOSTO,
TRANSF_NOP_CODIGO_ENTRADA,
TRANSPORTADOR_LOC_CODIGO,
TRANSPORTADOR_PFJ_CODIGO,
TX_CONVERSAO_MOEDA_NAC,
TXT_INSTRUCAO_BLOQUETO,
UF_CODIGO_DESTINO,
UF_CODIGO_EMITENTE,
UF_CODIGO_ENTREGA,
UF_CODIGO_ORIGEM,
UF_CODIGO_RETIRADA,
UF_CODIGO_SUBST,
UF_CODIGO_TRANSP,
UF_EMBARQUE,
UF_VEIC_COMP,
UF_VEIC_COMP_2,
USUARIO_INCLUSAO,
USUARIO_ULT_ALTERACAO,
VEIC_DESCR,
VEIC_ID_COMP,
VEIC_ID_COMP_2,
VL_ABAT_NT,
VL_ADU,
VL_ADU_ICMS,
VL_AJUSTE_PRECO_TOTAL_M,
VL_AJUSTE_PRECO_TOTAL_P,
VL_AJUSTE_PRECO_TOTAL_S,
VL_BASE_CT_STF,
VL_BASE_INSS_RET_PER,
VL_COFINS_ST,
VL_CT_STF,
VL_DESCONTO_NF,
VL_DESPACHO,
VL_DESP_CAR_DESC,
VL_DESP_PORT,
VL_FISCAL_M,
VL_FISCAL_O,
VL_FISCAL_P,
VL_FISCAL_S,
VL_FRETE,
VL_FRETE_FERR_SUBSTITUIDA,
VL_FRETE_LIQ,
VL_FRETE_MM,
VL_FRETE_TRAF_MUTUO,
VL_FRT_PV,
VL_GRIS,
VL_INSS,
VL_OUTRAS_DESPESAS,
VL_OUTROS_ABAT,
VL_PEDAGIO,
VL_PESO_TX,
VL_PIS_ST,
VL_SEC_CAT,
VL_SEGURO,
VL_SERV_NT,
VL_TAXA_ADV,
VL_TAXA_TERRESTRE,
VL_TERC,
VL_TFA,
VL_TOTAL_BASE_COFINS,
VL_TOTAL_BASE_COFINS_RET,
VL_TOTAL_BASE_CSLL_RET,
VL_TOTAL_BASE_ICMS,
VL_TOTAL_BASE_ICMS_DIFA,
VL_TOTAL_BASE_ICMS_FCP,
VL_TOTAL_BASE_ICMS_FCP_ST,
VL_TOTAL_BASE_ICMS_PART_DEST,
VL_TOTAL_BASE_ICMS_PART_REM,
VL_TOTAL_BASE_INSS,
VL_TOTAL_BASE_INSS_RET,
VL_TOTAL_BASE_IPI,
VL_TOTAL_BASE_IRRF,
VL_TOTAL_BASE_ISS,
VL_TOTAL_BASE_ISS_BITRIBUTADO,
VL_TOTAL_BASE_PIS_PASEP,
VL_TOTAL_BASE_PIS_RET,
VL_TOTAL_BASE_SENAT,
VL_TOTAL_BASE_SEST,
VL_TOTAL_BASE_STF,
VL_TOTAL_BASE_STF_FRONTEIRA,
VL_TOTAL_BASE_STF_IDO,
VL_TOTAL_BASE_STT,
VL_TOTAL_CARGA,
VL_TOTAL_COFINS,
VL_TOTAL_COFINS_RET,
VL_TOTAL_CONTABIL,
VL_TOTAL_CSLL_RET,
VL_TOTAL_FATURADO,
VL_TOTAL_FCP,
VL_TOTAL_FCP_ST,
VL_TOTAL_ICMS,
VL_TOTAL_ICMS_DIFA,
VL_TOTAL_ICMS_PART_DEST,
VL_TOTAL_ICMS_PART_REM,
VL_TOTAL_II,
VL_TOTAL_INSS,
VL_TOTAL_INSS_RET,
VL_TOTAL_INSS_RET_PER,
VL_TOTAL_IOF,
VL_TOTAL_IPI,
VL_TOTAL_IRRF,
VL_TOTAL_ISENTO_ICMS,
VL_TOTAL_ISS,
VL_TOTAL_ISS_BITRIBUTADO,
VL_TOTAL_MAT_TERC,
VL_TOTAL_OUTROS_ICMS,
VL_TOTAL_PIS_PASEP,
VL_TOTAL_PIS_RET,
VL_TOTAL_SENAT,
VL_TOTAL_SEST,
VL_TOTAL_STF,
VL_TOTAL_STF_FRONTEIRA,
VL_TOTAL_STF_IDO,
VL_TOTAL_STF_SUBSTITUIDO,
VL_TOTAL_STT,
VL_TRIBUTAVEL_DIFA,
VL_TROCO,
VL_TX_RED,
VOO_CODIGO,
VT_CODIGO,
XINF_ID)
   VALUES (v_tab(i).AGENTE,
v_tab(i).ANO_MES_EMISSAO,
v_tab(i).ANO_VEICULO,
v_tab(i).AUTENTICACAO_DIGITAL,
v_tab(i).CAD_ESPECIFICO_INSS_CEI,
v_tab(i).CERT_VEICULO,
v_tab(i).CERT_VEICULO_UF,
v_tab(i).CFOP_CODIGO,
v_tab(i).CFPS,
v_tab(i).CNPJ_COL,
v_tab(i).CNPJ_CRED_CARTAO,
v_tab(i).CNPJ_ENTG,
v_tab(i).CNX,
v_tab(i).COBRANCA_LOC_CODIGO,
v_tab(i).COBRANCA_PFJ_CODIGO,
v_tab(i).COD_AUT,
v_tab(i).COD_GRUPO_TENSAO,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).CODIGO_CONSUMO,
v_tab(i).COD_LINHA,
v_tab(i).COD_MOTIVO_CANC_SYN,
v_tab(i).COD_MUN_COL,
v_tab(i).COD_MUN_ENTG,
v_tab(i).COD_OPER_CARTAO,
v_tab(i).COD_REG_ESP_TRIBUTACAO,
v_tab(i).COMP_PRESTACAO_NOME,
v_tab(i).COMP_PRESTACAO_VALOR,
v_tab(i).CONSIG_PFJ_CODIGO,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPAG_CODIGO,
v_tab(i).CPF,
v_tab(i).CRT_CODIGO,
v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
v_tab(i).CTRL_CONTEUDO,
v_tab(i).CTRL_DT_INCLUSAO,
v_tab(i).CTRL_DT_ULT_ALTERACAO,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_FASE,
v_tab(i).CTRL_FASE_FISCAL,
v_tab(i).CTRL_OBS_CALCULO,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_RAT_FRETE,
v_tab(i).CTRL_OBS_RAT_M,
v_tab(i).CTRL_OBS_RAT_ODA,
v_tab(i).CTRL_OBS_RAT_P,
v_tab(i).CTRL_OBS_RAT_S,
v_tab(i).CTRL_OBS_RAT_SEGURO,
v_tab(i).CTRL_OBS_TOT_M,
v_tab(i).CTRL_OBS_TOT_O,
v_tab(i).CTRL_OBS_TOT_P,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_S,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_OBS_VL_PARCELAS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_tab(i).CTRL_PROCESSAMENTO,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_SITUACAO_DOF,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DEM_PTA,
v_tab(i).DESC_TITULO,
v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
v_tab(i).DESTINATARIO_LOC_CODIGO,
v_tab(i).DESTINATARIO_PFJ_CODIGO,
v_tab(i).DEST_PRINC_EST_CODIGO,
v_tab(i).DH_EMISSAO,
v_tab(i).DH_PROCESS_MANAGER,
v_tab(i).DOF_IMPORT_NUMERO,
v_tab(i).DT_CONVERSAO_MOEDA_NAC,
v_tab(i).DT_EMISSAO_TOMADOR,
v_tab(i).DT_EXE_SERV,
v_tab(i).DT_FATO_GERADOR_ATE,
v_tab(i).DT_FATO_GERADOR_IMPOSTO,
v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
v_tab(i).DT_LEITURA,
v_tab(i).DT_LIMITE_EMISSAO,
v_tab(i).DT_LIMITE_SAIDA,
v_tab(i).DT_PREV_CHEGADA,
v_tab(i).DT_PREV_EMBARQUE,
v_tab(i).DT_RECEBIMENTO_DESTINO,
v_tab(i).DT_REF_CALC_IMP,
v_tab(i).DT_REF_CPAG,
v_tab(i).DT_REFER_EXT,
v_tab(i).DT_ROMANEIO,
v_tab(i).DT_SAIDA_MERCADORIAS,
v_tab(i).EDOF_CODIGO,
v_tab(i).EDOF_CODIGO_RES,
v_tab(i).EMITENTE_LOC_CODIGO,
v_tab(i).EMITENTE_PFJ_CODIGO,
v_tab(i).ENTREGA_LOC_CODIGO,
v_tab(i).ENTREGA_PFJ_CODIGO,
v_tab(i).ESPECIALIZACAO,
v_tab(i).FATOR_FINANCIAMENTO,
v_tab(i).FERROV_SUBST_LOC_CODIGO,
v_tab(i).FERROV_SUBST_PFJ_CODIGO,
v_tab(i).FORMA_PAGTO_TRANSPORTE,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_NATUREZA_OPERACAO,
v_tab(i).HON_PRACA_PAGTO,
v_tab(i).HON_PRESTACAO_SERVICO,
v_tab(i).HON_VALOR_DESCONTO,
v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
v_tab(i).IE_COL,
v_tab(i).IE_ENTG,
v_tab(i).IM_COL,
v_tab(i).IM_ENTG,
v_tab(i).IND_ALTERACAO_TOMADOR,
v_tab(i).IND_BALSA,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_CIOT,
v_tab(i).IND_CLASSE,
v_tab(i).IND_COBRANCA_BANCARIA,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_EIX,
v_tab(i).IND_ENTRADA_SAIDA,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXPORTA_PARCELAS,
v_tab(i).IND_F0,
v_tab(i).IND_GABARITO,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LOTACAO,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_NAT_FRETE,
v_tab(i).IND_NAV,
v_tab(i).IND_NFE_AJUSTE,
v_tab(i).IND_PRECO_AJUSTADO_CPAG,
v_tab(i).IND_PRECOS_MOEDA_NAC,
v_tab(i).IND_PRES_COMPRADOR,
v_tab(i).IND_RATEIO,
v_tab(i).IND_REPERCUSSAO_FISCAL,
v_tab(i).IND_RESP_FRETE,
v_tab(i).IND_RESP_ISS_RET,
v_tab(i).IND_SIMPLES_ME_EPP,
v_tab(i).IND_SIT_TRIBUT_DOF,
v_tab(i).IND_TFA,
v_tab(i).IND_TIT,
v_tab(i).IND_TOT_VL_FATURADO,
v_tab(i).IND_USA_IF_CALC_IMP,
v_tab(i).IND_USA_IF_ESCRIT,
v_tab(i).IND_VEIC,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).INFORMANTE_EST_CODIGO,
v_tab(i).LINHA,
v_tab(i).LOCAL_EMBARQUE,
v_tab(i).LOCVIG_ID_DESTINATARIO,
v_tab(i).LOCVIG_ID_REMETENTE,
v_tab(i).MARCA_VEICULO,
v_tab(i).MDOF_CODIGO,
v_tab(i).MODELO_VEICULO,
v_tab(i).MODO_EMISSAO,
v_tab(i).MP_CODIGO,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_CODIGO_ISS,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).MUN_PRES_SERVICO,
v_tab(i).NAT_VOLUME,
v_tab(i).NFE_COD_SITUACAO,
v_tab(i).NFE_COD_SITUACAO_SYN,
v_tab(i).NFE_CONTINGENCIA,
v_tab(i).NFE_DESC_SITUACAO,
v_tab(i).NFE_ENVIADA,
v_tab(i).NFE_HORA_GMT_EMI,
v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NFE_PROTOCOLO,
v_tab(i).NF_IMPRESSORA_PADRAO,
v_tab(i).NFORM_FINAL,
v_tab(i).NFORM_INICIAL,
v_tab(i).NOME_MOTORISTA,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_APOLICE_SEGURO,
v_tab(i).NUM_AUTOR_OPER_CARTAO,
v_tab(i).NUM_CONTAINER,
v_tab(i).NUM_EMBARQUE,
v_tab(i).NUMERO,
v_tab(i).NUMERO_ATE,
v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
v_tab(i).NUM_FIM_RES,
v_tab(i).NUM_INI_RES,
v_tab(i).NUM_PASSE,
v_tab(i).NUM_PEDIDO,
v_tab(i).NUM_PLACA_VEICULO,
v_tab(i).NUM_REGISTRO_SECEX,
v_tab(i).NUM_RE_SISCOMEX,
v_tab(i).NUM_ROMANEIO,
v_tab(i).NUM_TITULO,
v_tab(i).NUM_VEICULO,
v_tab(i).NUM_VIAGEM,
v_tab(i).NUM_VOLUME_COMP,
v_tab(i).OBSERVACAO,
v_tab(i).ORDEM_EMBARQUE,
v_tab(i).OTM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
v_tab(i).PESO_BRT_COMP,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQ_COMP,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_REEMBALAGEM_KG,
v_tab(i).PFJ_CODIGO_REDESPACHANTE,
v_tab(i).PFJVIG_ID_DESTINATARIO,
v_tab(i).PFJVIG_ID_REMETENTE,
v_tab(i).PLACA_VEICULO_UF_CODIGO,
v_tab(i).POLT_CAB,
v_tab(i).PORTADOR_BANCO_NUM,
v_tab(i).PRECO_TOTAL_M,
v_tab(i).PRECO_TOTAL_O,
v_tab(i).PRECO_TOTAL_P,
v_tab(i).PRECO_TOTAL_S,
v_tab(i).PRODUTO_PREDOMINANTE,
v_tab(i).QTD_CANC,
v_tab(i).QTD_CANC_RES,
v_tab(i).QTD_CONSUMO,
v_tab(i).QTD_PASS_ORIG,
v_tab(i).QTD_PRESTACOES,
v_tab(i).REDESPACHANTE_LOC_CODIGO,
v_tab(i).REMETENTE_LOC_CODIGO,
v_tab(i).REMETENTE_PFJ_CODIGO,
v_tab(i).REM_PRINC_EST_CODIGO,
v_tab(i).RESP_FRETE_REDESP,
v_tab(i).RETIRADA_LOC_CODIGO,
v_tab(i).RETIRADA_PFJ_CODIGO,
v_tab(i).RETIRA_MERC_DESTINO,
v_tab(i).REVISAO,
v_tab(i).RNTC,
v_tab(i).RNTC_COMP,
v_tab(i).RNTC_COMP_2,
v_tab(i).SEQ_CALCULO,
v_tab(i).SERIE_SUBSERIE,
v_tab(i).SERIE_SUBSERIE_RES,
v_tab(i).SIGLA_MOEDA,
v_tab(i).SIS_CODIGO,
v_tab(i).SIS_CODIGO_CONTABILIZOU,
v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
v_tab(i).TEMPERATURA,
v_tab(i).TIPO,
v_tab(i).TIPO_CTE,
v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
v_tab(i).TIPO_PGTO,
v_tab(i).TIPO_SERVICO_CTE,
v_tab(i).TIPO_TARIFA,
v_tab(i).TIPO_TOMADOR,
v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
v_tab(i).TP_ASSINANTE,
v_tab(i).TP_INT_PAG,
v_tab(i).TP_LIGACAO,
v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
v_tab(i).TRANSPORTADOR_LOC_CODIGO,
v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
v_tab(i).TX_CONVERSAO_MOEDA_NAC,
v_tab(i).TXT_INSTRUCAO_BLOQUETO,
v_tab(i).UF_CODIGO_DESTINO,
v_tab(i).UF_CODIGO_EMITENTE,
v_tab(i).UF_CODIGO_ENTREGA,
v_tab(i).UF_CODIGO_ORIGEM,
v_tab(i).UF_CODIGO_RETIRADA,
v_tab(i).UF_CODIGO_SUBST,
v_tab(i).UF_CODIGO_TRANSP,
v_tab(i).UF_EMBARQUE,
v_tab(i).UF_VEIC_COMP,
v_tab(i).UF_VEIC_COMP_2,
v_tab(i).USUARIO_INCLUSAO,
v_tab(i).USUARIO_ULT_ALTERACAO,
v_tab(i).VEIC_DESCR,
v_tab(i).VEIC_ID_COMP,
v_tab(i).VEIC_ID_COMP_2,
v_tab(i).VL_ABAT_NT,
v_tab(i).VL_ADU,
v_tab(i).VL_ADU_ICMS,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
v_tab(i).VL_BASE_CT_STF,
v_tab(i).VL_BASE_INSS_RET_PER,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CT_STF,
v_tab(i).VL_DESCONTO_NF,
v_tab(i).VL_DESPACHO,
v_tab(i).VL_DESP_CAR_DESC,
v_tab(i).VL_DESP_PORT,
v_tab(i).VL_FISCAL_M,
v_tab(i).VL_FISCAL_O,
v_tab(i).VL_FISCAL_P,
v_tab(i).VL_FISCAL_S,
v_tab(i).VL_FRETE,
v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
v_tab(i).VL_FRETE_LIQ,
v_tab(i).VL_FRETE_MM,
v_tab(i).VL_FRETE_TRAF_MUTUO,
v_tab(i).VL_FRT_PV,
v_tab(i).VL_GRIS,
v_tab(i).VL_INSS,
v_tab(i).VL_OUTRAS_DESPESAS,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_PEDAGIO,
v_tab(i).VL_PESO_TX,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_SEC_CAT,
v_tab(i).VL_SEGURO,
v_tab(i).VL_SERV_NT,
v_tab(i).VL_TAXA_ADV,
v_tab(i).VL_TAXA_TERRESTRE,
v_tab(i).VL_TERC,
v_tab(i).VL_TFA,
v_tab(i).VL_TOTAL_BASE_COFINS,
v_tab(i).VL_TOTAL_BASE_COFINS_RET,
v_tab(i).VL_TOTAL_BASE_CSLL_RET,
v_tab(i).VL_TOTAL_BASE_ICMS,
v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
v_tab(i).VL_TOTAL_BASE_INSS,
v_tab(i).VL_TOTAL_BASE_INSS_RET,
v_tab(i).VL_TOTAL_BASE_IPI,
v_tab(i).VL_TOTAL_BASE_IRRF,
v_tab(i).VL_TOTAL_BASE_ISS,
v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
v_tab(i).VL_TOTAL_BASE_PIS_RET,
v_tab(i).VL_TOTAL_BASE_SENAT,
v_tab(i).VL_TOTAL_BASE_SEST,
v_tab(i).VL_TOTAL_BASE_STF,
v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_BASE_STF_IDO,
v_tab(i).VL_TOTAL_BASE_STT,
v_tab(i).VL_TOTAL_CARGA,
v_tab(i).VL_TOTAL_COFINS,
v_tab(i).VL_TOTAL_COFINS_RET,
v_tab(i).VL_TOTAL_CONTABIL,
v_tab(i).VL_TOTAL_CSLL_RET,
v_tab(i).VL_TOTAL_FATURADO,
v_tab(i).VL_TOTAL_FCP,
v_tab(i).VL_TOTAL_FCP_ST,
v_tab(i).VL_TOTAL_ICMS,
v_tab(i).VL_TOTAL_ICMS_DIFA,
v_tab(i).VL_TOTAL_ICMS_PART_DEST,
v_tab(i).VL_TOTAL_ICMS_PART_REM,
v_tab(i).VL_TOTAL_II,
v_tab(i).VL_TOTAL_INSS,
v_tab(i).VL_TOTAL_INSS_RET,
v_tab(i).VL_TOTAL_INSS_RET_PER,
v_tab(i).VL_TOTAL_IOF,
v_tab(i).VL_TOTAL_IPI,
v_tab(i).VL_TOTAL_IRRF,
v_tab(i).VL_TOTAL_ISENTO_ICMS,
v_tab(i).VL_TOTAL_ISS,
v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
v_tab(i).VL_TOTAL_MAT_TERC,
v_tab(i).VL_TOTAL_OUTROS_ICMS,
v_tab(i).VL_TOTAL_PIS_PASEP,
v_tab(i).VL_TOTAL_PIS_RET,
v_tab(i).VL_TOTAL_SENAT,
v_tab(i).VL_TOTAL_SEST,
v_tab(i).VL_TOTAL_STF,
v_tab(i).VL_TOTAL_STF_FRONTEIRA,
v_tab(i).VL_TOTAL_STF_IDO,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
v_tab(i).VL_TOTAL_STT,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TROCO,
v_tab(i).VL_TX_RED,
v_tab(i).VOO_CODIGO,
v_tab(i).VT_CODIGO,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			      update cor_dof
				     set AGENTE = v_tab(i).AGENTE,
                          ANO_MES_EMISSAO = v_tab(i).ANO_MES_EMISSAO,
                          ANO_VEICULO = v_tab(i).ANO_VEICULO,
                          AUTENTICACAO_DIGITAL = v_tab(i).AUTENTICACAO_DIGITAL,
                          CAD_ESPECIFICO_INSS_CEI = v_tab(i).CAD_ESPECIFICO_INSS_CEI,
                          CERT_VEICULO = v_tab(i).CERT_VEICULO,
                          CERT_VEICULO_UF = v_tab(i).CERT_VEICULO_UF,
                          CFOP_CODIGO = v_tab(i).CFOP_CODIGO,
                          CFPS = v_tab(i).CFPS,
                          CNPJ_COL = v_tab(i).CNPJ_COL,
                          CNPJ_CRED_CARTAO = v_tab(i).CNPJ_CRED_CARTAO,
                          CNPJ_ENTG = v_tab(i).CNPJ_ENTG,
                          CNX = v_tab(i).CNX,
                          COBRANCA_LOC_CODIGO = v_tab(i).COBRANCA_LOC_CODIGO,
                          COBRANCA_PFJ_CODIGO = v_tab(i).COBRANCA_PFJ_CODIGO,
                          COD_AUT = v_tab(i).COD_AUT,
                          COD_GRUPO_TENSAO = v_tab(i).COD_GRUPO_TENSAO,
                          COD_IATA_FIM = v_tab(i).COD_IATA_FIM,
                          COD_IATA_INI = v_tab(i).COD_IATA_INI,
                          CODIGO_CONSUMO = v_tab(i).CODIGO_CONSUMO,
                          COD_LINHA = v_tab(i).COD_LINHA,
                          COD_MOTIVO_CANC_SYN = v_tab(i).COD_MOTIVO_CANC_SYN,
                          COD_MUN_COL = v_tab(i).COD_MUN_COL,
                          COD_MUN_ENTG = v_tab(i).COD_MUN_ENTG,
                          COD_OPER_CARTAO = v_tab(i).COD_OPER_CARTAO,
                          COD_REG_ESP_TRIBUTACAO = v_tab(i).COD_REG_ESP_TRIBUTACAO,
                          COMP_PRESTACAO_NOME = v_tab(i).COMP_PRESTACAO_NOME,
                          COMP_PRESTACAO_VALOR = v_tab(i).COMP_PRESTACAO_VALOR,
                          CONSIG_PFJ_CODIGO = v_tab(i).CONSIG_PFJ_CODIGO,
                          CONTA_CONTABIL = v_tab(i).CONTA_CONTABIL,
                          CPAG_CODIGO = v_tab(i).CPAG_CODIGO,
                          CPF = v_tab(i).CPF,
                          CRT_CODIGO = v_tab(i).CRT_CODIGO,
                          CTE_LOCALIZADOR_ORIGINAL = v_tab(i).CTE_LOCALIZADOR_ORIGINAL,
                          CTRL_CONTEUDO = v_tab(i).CTRL_CONTEUDO,
                          CTRL_DT_INCLUSAO = v_tab(i).CTRL_DT_INCLUSAO,
                          CTRL_DT_ULT_ALTERACAO = v_tab(i).CTRL_DT_ULT_ALTERACAO,
                          CTRL_ENTRADA_ST = v_tab(i).CTRL_ENTRADA_ST,
                          CTRL_FASE = v_tab(i).CTRL_FASE,
                          CTRL_FASE_FISCAL = v_tab(i).CTRL_FASE_FISCAL,
                          CTRL_OBS_CALCULO = v_tab(i).CTRL_OBS_CALCULO,
                          CTRL_OBS_ENQ = v_tab(i).CTRL_OBS_ENQ,
                          CTRL_OBS_RAT_FRETE = v_tab(i).CTRL_OBS_RAT_FRETE,
                          CTRL_OBS_RAT_M = v_tab(i).CTRL_OBS_RAT_M,
                          CTRL_OBS_RAT_ODA = v_tab(i).CTRL_OBS_RAT_ODA,
                          CTRL_OBS_RAT_P = v_tab(i).CTRL_OBS_RAT_P,
                          CTRL_OBS_RAT_S = v_tab(i).CTRL_OBS_RAT_S,
                          CTRL_OBS_RAT_SEGURO = v_tab(i).CTRL_OBS_RAT_SEGURO,
                          CTRL_OBS_TOT_M = v_tab(i).CTRL_OBS_TOT_M,
                          CTRL_OBS_TOT_O = v_tab(i).CTRL_OBS_TOT_O,
                          CTRL_OBS_TOT_P = v_tab(i).CTRL_OBS_TOT_P,
                          CTRL_OBS_TOT_PESO = v_tab(i).CTRL_OBS_TOT_PESO,
                          CTRL_OBS_TOT_S = v_tab(i).CTRL_OBS_TOT_S,
                          CTRL_OBS_VL_FISCAL = v_tab(i).CTRL_OBS_VL_FISCAL,
                          CTRL_OBS_VL_PARCELAS = v_tab(i).CTRL_OBS_VL_PARCELAS,
                          CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
                          CTRL_PROCESSAMENTO = v_tab(i).CTRL_PROCESSAMENTO,
                          CTRL_SAIDA_ST = v_tab(i).CTRL_SAIDA_ST,
                          CTRL_SITUACAO_DOF = v_tab(i).CTRL_SITUACAO_DOF,
                          C4_IND_TEM_REAL = v_tab(i).C4_IND_TEM_REAL,
                          DEM_PTA = v_tab(i).DEM_PTA,
                          DESC_TITULO = v_tab(i).DESC_TITULO,
                          DESTINATARIO_CTE_LOC_CODIGO = v_tab(i).DESTINATARIO_CTE_LOC_CODIGO,
                          DESTINATARIO_CTE_PFJ_CODIGO = v_tab(i).DESTINATARIO_CTE_PFJ_CODIGO,
                          DESTINATARIO_LOC_CODIGO = v_tab(i).DESTINATARIO_LOC_CODIGO,
                          DESTINATARIO_PFJ_CODIGO = v_tab(i).DESTINATARIO_PFJ_CODIGO,
                          DEST_PRINC_EST_CODIGO = v_tab(i).DEST_PRINC_EST_CODIGO,
                          DH_EMISSAO = v_tab(i).DH_EMISSAO,
                          DH_PROCESS_MANAGER = v_tab(i).DH_PROCESS_MANAGER,
                          DT_CONVERSAO_MOEDA_NAC = v_tab(i).DT_CONVERSAO_MOEDA_NAC,
                          DT_EMISSAO_TOMADOR = v_tab(i).DT_EMISSAO_TOMADOR,
                          DT_EXE_SERV = v_tab(i).DT_EXE_SERV,
                          DT_FATO_GERADOR_ATE = v_tab(i).DT_FATO_GERADOR_ATE,
                          DT_FATO_GERADOR_IMPOSTO = v_tab(i).DT_FATO_GERADOR_IMPOSTO,
                          DT_FIM_PROGRAMACAO_ENTREGA = v_tab(i).DT_FIM_PROGRAMACAO_ENTREGA,
                          DT_INI_PROGRAMACAO_ENTREGA = v_tab(i).DT_INI_PROGRAMACAO_ENTREGA,
                          DT_LEITURA = v_tab(i).DT_LEITURA,
                          DT_LIMITE_EMISSAO = v_tab(i).DT_LIMITE_EMISSAO,
                          DT_LIMITE_SAIDA = v_tab(i).DT_LIMITE_SAIDA,
                          DT_PREV_CHEGADA = v_tab(i).DT_PREV_CHEGADA,
                          DT_PREV_EMBARQUE = v_tab(i).DT_PREV_EMBARQUE,
                          DT_RECEBIMENTO_DESTINO = v_tab(i).DT_RECEBIMENTO_DESTINO,
                          DT_REF_CALC_IMP = v_tab(i).DT_REF_CALC_IMP,
                          DT_REF_CPAG = v_tab(i).DT_REF_CPAG,
                          DT_REFER_EXT = v_tab(i).DT_REFER_EXT,
                          DT_ROMANEIO = v_tab(i).DT_ROMANEIO,
                          DT_SAIDA_MERCADORIAS = v_tab(i).DT_SAIDA_MERCADORIAS,
                          EDOF_CODIGO = v_tab(i).EDOF_CODIGO,
                          EDOF_CODIGO_RES = v_tab(i).EDOF_CODIGO_RES,
                          EMITENTE_LOC_CODIGO = v_tab(i).EMITENTE_LOC_CODIGO,
                          EMITENTE_PFJ_CODIGO = v_tab(i).EMITENTE_PFJ_CODIGO,
                          ENTREGA_LOC_CODIGO = v_tab(i).ENTREGA_LOC_CODIGO,
                          ENTREGA_PFJ_CODIGO = v_tab(i).ENTREGA_PFJ_CODIGO,
                          ESPECIALIZACAO = v_tab(i).ESPECIALIZACAO,
                          FATOR_FINANCIAMENTO = v_tab(i).FATOR_FINANCIAMENTO,
                          FERROV_SUBST_LOC_CODIGO = v_tab(i).FERROV_SUBST_LOC_CODIGO,
                          FERROV_SUBST_PFJ_CODIGO = v_tab(i).FERROV_SUBST_PFJ_CODIGO,
                          FORMA_PAGTO_TRANSPORTE = v_tab(i).FORMA_PAGTO_TRANSPORTE,
                          HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
                          HON_NATUREZA_OPERACAO = v_tab(i).HON_NATUREZA_OPERACAO,
                          HON_PRACA_PAGTO = v_tab(i).HON_PRACA_PAGTO,
                          HON_PRESTACAO_SERVICO = v_tab(i).HON_PRESTACAO_SERVICO,
                          HON_VALOR_DESCONTO = v_tab(i).HON_VALOR_DESCONTO,
                          HR_FIM_PROGRAMACAO_ENTREGA = v_tab(i).HR_FIM_PROGRAMACAO_ENTREGA,
                          HR_INI_PROGRAMACAO_ENTREGA = v_tab(i).HR_INI_PROGRAMACAO_ENTREGA,
                          IE_COL = v_tab(i).IE_COL,
                          IE_ENTG = v_tab(i).IE_ENTG,
                          IM_COL = v_tab(i).IM_COL,
                          IM_ENTG = v_tab(i).IM_ENTG,
                          IND_ALTERACAO_TOMADOR = v_tab(i).IND_ALTERACAO_TOMADOR,
                          IND_BALSA = v_tab(i).IND_BALSA,
                          IND_BLOQUEADO = v_tab(i).IND_BLOQUEADO,
                          IND_CIOT = v_tab(i).IND_CIOT,
                          IND_CLASSE = v_tab(i).IND_CLASSE,
                          IND_COBRANCA_BANCARIA = v_tab(i).IND_COBRANCA_BANCARIA,
                          IND_CONTABILIZACAO = v_tab(i).IND_CONTABILIZACAO,
                          IND_EIX = v_tab(i).IND_EIX,
                          IND_ENTRADA_SAIDA = v_tab(i).IND_ENTRADA_SAIDA,
                          IND_ESCRITURACAO = v_tab(i).IND_ESCRITURACAO,
                          IND_EXPORTA_PARCELAS = v_tab(i).IND_EXPORTA_PARCELAS,
                          IND_F0 = v_tab(i).IND_F0,
                          IND_GABARITO = v_tab(i).IND_GABARITO,
                          IND_INCIDENCIA_COFINS_RET = v_tab(i).IND_INCIDENCIA_COFINS_RET,
                          IND_INCIDENCIA_CSLL_RET = v_tab(i).IND_INCIDENCIA_CSLL_RET,
                          IND_INCIDENCIA_INSS_RET = v_tab(i).IND_INCIDENCIA_INSS_RET,
                          IND_INCIDENCIA_IRRF = v_tab(i).IND_INCIDENCIA_IRRF,
                          IND_INCIDENCIA_PIS_RET = v_tab(i).IND_INCIDENCIA_PIS_RET,
                          IND_ISS_RETIDO_FONTE = v_tab(i).IND_ISS_RETIDO_FONTE,
                          IND_LOTACAO = v_tab(i).IND_LOTACAO,
                          IND_MOVIMENTA_ESTOQUE = v_tab(i).IND_MOVIMENTA_ESTOQUE,
                          IND_NAT_FRETE = v_tab(i).IND_NAT_FRETE,
                          IND_NAV = v_tab(i).IND_NAV,
                          IND_NFE_AJUSTE = v_tab(i).IND_NFE_AJUSTE,
                          IND_PRECO_AJUSTADO_CPAG = v_tab(i).IND_PRECO_AJUSTADO_CPAG,
                          IND_PRECOS_MOEDA_NAC = v_tab(i).IND_PRECOS_MOEDA_NAC,
                          IND_PRES_COMPRADOR = v_tab(i).IND_PRES_COMPRADOR,
                          IND_RATEIO = v_tab(i).IND_RATEIO,
                          IND_REPERCUSSAO_FISCAL = v_tab(i).IND_REPERCUSSAO_FISCAL,
                          IND_RESP_FRETE = v_tab(i).IND_RESP_FRETE,
                          IND_RESP_ISS_RET = v_tab(i).IND_RESP_ISS_RET,
                          IND_SIMPLES_ME_EPP = v_tab(i).IND_SIMPLES_ME_EPP,
                          IND_SIT_TRIBUT_DOF = v_tab(i).IND_SIT_TRIBUT_DOF,
                          IND_TFA = v_tab(i).IND_TFA,
                          IND_TIT = v_tab(i).IND_TIT,
                          IND_TOT_VL_FATURADO = v_tab(i).IND_TOT_VL_FATURADO,
                          IND_USA_IF_CALC_IMP = v_tab(i).IND_USA_IF_CALC_IMP,
                          IND_USA_IF_ESCRIT = v_tab(i).IND_USA_IF_ESCRIT,
                          IND_VEIC = v_tab(i).IND_VEIC,
                          IND_VERIFICA_PEDIDO = v_tab(i).IND_VERIFICA_PEDIDO,
                          LINHA = v_tab(i).LINHA,
                          LOCAL_EMBARQUE = v_tab(i).LOCAL_EMBARQUE,
                          LOCVIG_ID_DESTINATARIO = v_tab(i).LOCVIG_ID_DESTINATARIO,
                          LOCVIG_ID_REMETENTE = v_tab(i).LOCVIG_ID_REMETENTE,
                          MARCA_VEICULO = v_tab(i).MARCA_VEICULO,
                          MDOF_CODIGO = v_tab(i).MDOF_CODIGO,
                          MODELO_VEICULO = v_tab(i).MODELO_VEICULO,
                          MODO_EMISSAO = v_tab(i).MODO_EMISSAO,
                          MP_CODIGO = v_tab(i).MP_CODIGO,
                          MUN_COD_DESTINO = v_tab(i).MUN_COD_DESTINO,
                          MUN_CODIGO_ISS = v_tab(i).MUN_CODIGO_ISS,
                          MUN_COD_ORIGEM = v_tab(i).MUN_COD_ORIGEM,
                          MUN_PRES_SERVICO = v_tab(i).MUN_PRES_SERVICO,
                          NAT_VOLUME = v_tab(i).NAT_VOLUME,
                          NFE_COD_SITUACAO = v_tab(i).NFE_COD_SITUACAO,
                          NFE_COD_SITUACAO_SYN = v_tab(i).NFE_COD_SITUACAO_SYN,
                          NFE_CONTINGENCIA = v_tab(i).NFE_CONTINGENCIA,
                          NFE_DESC_SITUACAO = v_tab(i).NFE_DESC_SITUACAO,
                          NFE_ENVIADA = v_tab(i).NFE_ENVIADA,
                          NFE_HORA_GMT_EMI = v_tab(i).NFE_HORA_GMT_EMI,
                          NFE_JUSTIFICATIVA_CANCELAMENTO= v_tab(i).NFE_JUSTIFICATIVA_CANCELAMENTO,
                          NFE_LOCALIZADOR = v_tab(i).NFE_LOCALIZADOR,
                          NFE_PROTOCOLO = v_tab(i).NFE_PROTOCOLO,
                          NF_IMPRESSORA_PADRAO = v_tab(i).NF_IMPRESSORA_PADRAO,
                          NFORM_FINAL = v_tab(i).NFORM_FINAL,
                          NFORM_INICIAL = v_tab(i).NFORM_INICIAL,
                          NOME_MOTORISTA = v_tab(i).NOME_MOTORISTA,
                          NOP_CODIGO = v_tab(i).NOP_CODIGO,
                          NUM_APOLICE_SEGURO = v_tab(i).NUM_APOLICE_SEGURO,
                          NUM_AUTOR_OPER_CARTAO = v_tab(i).NUM_AUTOR_OPER_CARTAO,
                          NUM_CONTAINER = v_tab(i).NUM_CONTAINER,
                          NUM_EMBARQUE = v_tab(i).NUM_EMBARQUE,
                          NUMERO = v_tab(i).NUMERO,
                          NUMERO_ATE = v_tab(i).NUMERO_ATE,
                          NUMERO_CONTRATO_FERROVIARIO = v_tab(i).NUMERO_CONTRATO_FERROVIARIO,
                          NUM_FIM_RES = v_tab(i).NUM_FIM_RES,
                          NUM_INI_RES = v_tab(i).NUM_INI_RES,
                          NUM_PASSE = v_tab(i).NUM_PASSE,
                          NUM_PEDIDO = v_tab(i).NUM_PEDIDO,
                          NUM_PLACA_VEICULO = v_tab(i).NUM_PLACA_VEICULO,
                          NUM_REGISTRO_SECEX = v_tab(i).NUM_REGISTRO_SECEX,
                          NUM_RE_SISCOMEX = v_tab(i).NUM_RE_SISCOMEX,
                          NUM_ROMANEIO = v_tab(i).NUM_ROMANEIO,
                          NUM_TITULO = v_tab(i).NUM_TITULO,
                          NUM_VEICULO = v_tab(i).NUM_VEICULO,
                          NUM_VIAGEM = v_tab(i).NUM_VIAGEM,
                          NUM_VOLUME_COMP = v_tab(i).NUM_VOLUME_COMP,
                          OBSERVACAO = v_tab(i).OBSERVACAO,
                          ORDEM_EMBARQUE = v_tab(i).ORDEM_EMBARQUE,
                          OTM = v_tab(i).OTM,
                          PERC_AJUSTE_PRECO_TOTAL_M = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_M,
                          PERC_AJUSTE_PRECO_TOTAL_P = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_P,
                          PERC_AJUSTE_PRECO_TOTAL_S = v_tab(i).PERC_AJUSTE_PRECO_TOTAL_S,
                          PESO_BRT_COMP = v_tab(i).PESO_BRT_COMP,
                          PESO_BRUTO_KG = v_tab(i).PESO_BRUTO_KG,
                          PESO_LIQ_COMP = v_tab(i).PESO_LIQ_COMP,
                          PESO_LIQUIDO_KG = v_tab(i).PESO_LIQUIDO_KG,
                          PESO_REEMBALAGEM_KG = v_tab(i).PESO_REEMBALAGEM_KG,
                          PFJ_CODIGO_REDESPACHANTE = v_tab(i).PFJ_CODIGO_REDESPACHANTE,
                          PFJVIG_ID_DESTINATARIO = v_tab(i).PFJVIG_ID_DESTINATARIO,
                          PFJVIG_ID_REMETENTE = v_tab(i).PFJVIG_ID_REMETENTE,
                          PLACA_VEICULO_UF_CODIGO = v_tab(i).PLACA_VEICULO_UF_CODIGO,
                          POLT_CAB = v_tab(i).POLT_CAB,
                          PORTADOR_BANCO_NUM = v_tab(i).PORTADOR_BANCO_NUM,
                          PRECO_TOTAL_M = v_tab(i).PRECO_TOTAL_M,
                          PRECO_TOTAL_O = v_tab(i).PRECO_TOTAL_O,
                          PRECO_TOTAL_P = v_tab(i).PRECO_TOTAL_P,
                          PRECO_TOTAL_S = v_tab(i).PRECO_TOTAL_S,
                          PRODUTO_PREDOMINANTE = v_tab(i).PRODUTO_PREDOMINANTE,
                          QTD_CANC = v_tab(i).QTD_CANC,
                          QTD_CANC_RES = v_tab(i).QTD_CANC_RES,
                          QTD_CONSUMO = v_tab(i).QTD_CONSUMO,
                          QTD_PASS_ORIG = v_tab(i).QTD_PASS_ORIG,
                          QTD_PRESTACOES = v_tab(i).QTD_PRESTACOES,
                          REDESPACHANTE_LOC_CODIGO = v_tab(i).REDESPACHANTE_LOC_CODIGO,
                          REMETENTE_LOC_CODIGO = v_tab(i).REMETENTE_LOC_CODIGO,
                          REMETENTE_PFJ_CODIGO = v_tab(i).REMETENTE_PFJ_CODIGO,
                          REM_PRINC_EST_CODIGO = v_tab(i).REM_PRINC_EST_CODIGO,
                          RESP_FRETE_REDESP = v_tab(i).RESP_FRETE_REDESP,
                          RETIRADA_LOC_CODIGO = v_tab(i).RETIRADA_LOC_CODIGO,
                          RETIRADA_PFJ_CODIGO = v_tab(i).RETIRADA_PFJ_CODIGO,
                          RETIRA_MERC_DESTINO = v_tab(i).RETIRA_MERC_DESTINO,
                          REVISAO = v_tab(i).REVISAO,
                          RNTC = v_tab(i).RNTC,
                          RNTC_COMP = v_tab(i).RNTC_COMP,
                          RNTC_COMP_2 = v_tab(i).RNTC_COMP_2,
                          SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
                          SERIE_SUBSERIE = v_tab(i).SERIE_SUBSERIE,
                          SERIE_SUBSERIE_RES = v_tab(i).SERIE_SUBSERIE_RES,
                          SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
                          SIS_CODIGO = v_tab(i).SIS_CODIGO,
                          SIS_CODIGO_CONTABILIZOU = v_tab(i).SIS_CODIGO_CONTABILIZOU,
                          SUBSTITUIDO_PFJ_CODIGO = v_tab(i).SUBSTITUIDO_PFJ_CODIGO,
                          TEMPERATURA = v_tab(i).TEMPERATURA,
                          TIPO = v_tab(i).TIPO,
                          TIPO_CTE = v_tab(i).TIPO_CTE,
                          TIPO_DT_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_DT_PROGRAMACAO_ENTREGA,
                          TIPO_HR_PROGRAMACAO_ENTREGA = v_tab(i).TIPO_HR_PROGRAMACAO_ENTREGA,
                          TIPO_PGTO = v_tab(i).TIPO_PGTO,
                          TIPO_SERVICO_CTE = v_tab(i).TIPO_SERVICO_CTE,
                          TIPO_TARIFA = v_tab(i).TIPO_TARIFA,
                          TIPO_TOMADOR = v_tab(i).TIPO_TOMADOR,
                          TIPO_TRAFEGO_FERROVIARIO = v_tab(i).TIPO_TRAFEGO_FERROVIARIO,
                          TOMADOR_OUTROS_PFJ_CODIGO = v_tab(i).TOMADOR_OUTROS_PFJ_CODIGO,
                          TP_ASSINANTE = v_tab(i).TP_ASSINANTE,
                          TP_INT_PAG = v_tab(i).TP_INT_PAG,
                          TP_LIGACAO = v_tab(i).TP_LIGACAO,
                          TRAF_MUTUO_FERROV_EMITENTE = v_tab(i).TRAF_MUTUO_FERROV_EMITENTE,
                          TRAF_MUTUO_FLUXO_FERROV = v_tab(i).TRAF_MUTUO_FLUXO_FERROV,
                          TRAF_MUTUO_IDENTIF_TREM = v_tab(i).TRAF_MUTUO_IDENTIF_TREM,
                          TRAF_MUTUO_RESP_FATURAMENTO = v_tab(i).TRAF_MUTUO_RESP_FATURAMENTO,
                          TRANSF_DT_FATO_GER_IMPOSTO = v_tab(i).TRANSF_DT_FATO_GER_IMPOSTO,
                          TRANSF_NOP_CODIGO_ENTRADA = v_tab(i).TRANSF_NOP_CODIGO_ENTRADA,
                          TRANSPORTADOR_LOC_CODIGO = v_tab(i).TRANSPORTADOR_LOC_CODIGO,
                          TRANSPORTADOR_PFJ_CODIGO = v_tab(i).TRANSPORTADOR_PFJ_CODIGO,
                          TX_CONVERSAO_MOEDA_NAC = v_tab(i).TX_CONVERSAO_MOEDA_NAC,
                          TXT_INSTRUCAO_BLOQUETO = v_tab(i).TXT_INSTRUCAO_BLOQUETO,
                          UF_CODIGO_DESTINO = v_tab(i).UF_CODIGO_DESTINO,
                          UF_CODIGO_EMITENTE = v_tab(i).UF_CODIGO_EMITENTE,
                          UF_CODIGO_ENTREGA = v_tab(i).UF_CODIGO_ENTREGA,
                          UF_CODIGO_ORIGEM = v_tab(i).UF_CODIGO_ORIGEM,
                          UF_CODIGO_RETIRADA = v_tab(i).UF_CODIGO_RETIRADA,
                          UF_CODIGO_SUBST = v_tab(i).UF_CODIGO_SUBST,
                          UF_CODIGO_TRANSP = v_tab(i).UF_CODIGO_TRANSP,
                          UF_EMBARQUE = v_tab(i).UF_EMBARQUE,
                          UF_VEIC_COMP = v_tab(i).UF_VEIC_COMP,
                          UF_VEIC_COMP_2 = v_tab(i).UF_VEIC_COMP_2,
                          USUARIO_INCLUSAO = v_tab(i).USUARIO_INCLUSAO,
                          USUARIO_ULT_ALTERACAO = v_tab(i).USUARIO_ULT_ALTERACAO,
                          VEIC_DESCR = v_tab(i).VEIC_DESCR,
                          VEIC_ID_COMP = v_tab(i).VEIC_ID_COMP,
                          VEIC_ID_COMP_2 = v_tab(i).VEIC_ID_COMP_2,
                          VL_ABAT_NT = v_tab(i).VL_ABAT_NT,
                          VL_ADU = v_tab(i).VL_ADU,
                          VL_ADU_ICMS = v_tab(i).VL_ADU_ICMS,
                          VL_AJUSTE_PRECO_TOTAL_M = v_tab(i).VL_AJUSTE_PRECO_TOTAL_M,
                          VL_AJUSTE_PRECO_TOTAL_P = v_tab(i).VL_AJUSTE_PRECO_TOTAL_P,
                          VL_AJUSTE_PRECO_TOTAL_S = v_tab(i).VL_AJUSTE_PRECO_TOTAL_S,
                          VL_BASE_CT_STF = v_tab(i).VL_BASE_CT_STF,
                          VL_BASE_INSS_RET_PER = v_tab(i).VL_BASE_INSS_RET_PER,
                          VL_COFINS_ST = v_tab(i).VL_COFINS_ST,
                          VL_CT_STF = v_tab(i).VL_CT_STF,
                          VL_DESCONTO_NF = v_tab(i).VL_DESCONTO_NF,
                          VL_DESPACHO = v_tab(i).VL_DESPACHO,
                          VL_DESP_CAR_DESC = v_tab(i).VL_DESP_CAR_DESC,
                          VL_DESP_PORT = v_tab(i).VL_DESP_PORT,
                          VL_FISCAL_M = v_tab(i).VL_FISCAL_M,
                          VL_FISCAL_O = v_tab(i).VL_FISCAL_O,
                          VL_FISCAL_P = v_tab(i).VL_FISCAL_P,
                          VL_FISCAL_S = v_tab(i).VL_FISCAL_S,
                          VL_FRETE = v_tab(i).VL_FRETE,
                          VL_FRETE_FERR_SUBSTITUIDA = v_tab(i).VL_FRETE_FERR_SUBSTITUIDA,
                          VL_FRETE_LIQ = v_tab(i).VL_FRETE_LIQ,
                          VL_FRETE_MM = v_tab(i).VL_FRETE_MM,
                          VL_FRETE_TRAF_MUTUO = v_tab(i).VL_FRETE_TRAF_MUTUO,
                          VL_FRT_PV = v_tab(i).VL_FRT_PV,
                          VL_GRIS = v_tab(i).VL_GRIS,
                          VL_INSS = v_tab(i).VL_INSS,
                          VL_OUTRAS_DESPESAS = v_tab(i).VL_OUTRAS_DESPESAS,
                          VL_OUTROS_ABAT = v_tab(i).VL_OUTROS_ABAT,
                          VL_PEDAGIO = v_tab(i).VL_PEDAGIO,
                          VL_PESO_TX = v_tab(i).VL_PESO_TX,
                          VL_PIS_ST = v_tab(i).VL_PIS_ST,
                          VL_SEC_CAT = v_tab(i).VL_SEC_CAT,
                          VL_SEGURO = v_tab(i).VL_SEGURO,
                          VL_SERV_NT = v_tab(i).VL_SERV_NT,
                          VL_TAXA_ADV = v_tab(i).VL_TAXA_ADV,
                          VL_TAXA_TERRESTRE = v_tab(i).VL_TAXA_TERRESTRE,
                          VL_TERC = v_tab(i).VL_TERC,
                          VL_TFA = v_tab(i).VL_TFA,
                          VL_TOTAL_BASE_COFINS = v_tab(i).VL_TOTAL_BASE_COFINS,
                          VL_TOTAL_BASE_COFINS_RET = v_tab(i).VL_TOTAL_BASE_COFINS_RET,
                          VL_TOTAL_BASE_CSLL_RET = v_tab(i).VL_TOTAL_BASE_CSLL_RET,
                          VL_TOTAL_BASE_ICMS = v_tab(i).VL_TOTAL_BASE_ICMS,
                          VL_TOTAL_BASE_ICMS_DIFA = v_tab(i).VL_TOTAL_BASE_ICMS_DIFA,
                          VL_TOTAL_BASE_ICMS_FCP = v_tab(i).VL_TOTAL_BASE_ICMS_FCP,
                          VL_TOTAL_BASE_ICMS_FCP_ST = v_tab(i).VL_TOTAL_BASE_ICMS_FCP_ST,						  
                          VL_TOTAL_BASE_ICMS_PART_DEST = v_tab(i).VL_TOTAL_BASE_ICMS_PART_DEST,
                          VL_TOTAL_BASE_ICMS_PART_REM = v_tab(i).VL_TOTAL_BASE_ICMS_PART_REM,
                          VL_TOTAL_BASE_INSS = v_tab(i).VL_TOTAL_BASE_INSS,
                          VL_TOTAL_BASE_INSS_RET = v_tab(i).VL_TOTAL_BASE_INSS_RET,
                          VL_TOTAL_BASE_IPI = v_tab(i).VL_TOTAL_BASE_IPI,
                          VL_TOTAL_BASE_IRRF = v_tab(i).VL_TOTAL_BASE_IRRF,
                          VL_TOTAL_BASE_ISS = v_tab(i).VL_TOTAL_BASE_ISS,
                          VL_TOTAL_BASE_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_BASE_ISS_BITRIBUTADO,
                          VL_TOTAL_BASE_PIS_PASEP = v_tab(i).VL_TOTAL_BASE_PIS_PASEP,
                          VL_TOTAL_BASE_PIS_RET = v_tab(i).VL_TOTAL_BASE_PIS_RET,
                          VL_TOTAL_BASE_SENAT = v_tab(i).VL_TOTAL_BASE_SENAT,
                          VL_TOTAL_BASE_SEST = v_tab(i).VL_TOTAL_BASE_SEST,
                          VL_TOTAL_BASE_STF = v_tab(i).VL_TOTAL_BASE_STF,
                          VL_TOTAL_BASE_STF_FRONTEIRA = v_tab(i).VL_TOTAL_BASE_STF_FRONTEIRA,
                          VL_TOTAL_BASE_STF_IDO = v_tab(i).VL_TOTAL_BASE_STF_IDO,
                          VL_TOTAL_BASE_STT = v_tab(i).VL_TOTAL_BASE_STT,
                          VL_TOTAL_CARGA = v_tab(i).VL_TOTAL_CARGA,
                          VL_TOTAL_COFINS = v_tab(i).VL_TOTAL_COFINS,
                          VL_TOTAL_COFINS_RET = v_tab(i).VL_TOTAL_COFINS_RET,
                          VL_TOTAL_CONTABIL = v_tab(i).VL_TOTAL_CONTABIL,
                          VL_TOTAL_CSLL_RET = v_tab(i).VL_TOTAL_CSLL_RET,
                          VL_TOTAL_FATURADO = v_tab(i).VL_TOTAL_FATURADO,
                          VL_TOTAL_FCP = v_tab(i).VL_TOTAL_FCP,
                          VL_TOTAL_FCP_ST = v_tab(i).VL_TOTAL_FCP_ST,						  
                          VL_TOTAL_ICMS = v_tab(i).VL_TOTAL_ICMS,
                          VL_TOTAL_ICMS_DIFA = v_tab(i).VL_TOTAL_ICMS_DIFA,
                          VL_TOTAL_ICMS_PART_DEST = v_tab(i).VL_TOTAL_ICMS_PART_DEST,
                          VL_TOTAL_ICMS_PART_REM = v_tab(i).VL_TOTAL_ICMS_PART_REM,
                          VL_TOTAL_II = v_tab(i).VL_TOTAL_II,
                          VL_TOTAL_INSS = v_tab(i).VL_TOTAL_INSS,
                          VL_TOTAL_INSS_RET = v_tab(i).VL_TOTAL_INSS_RET,
                          VL_TOTAL_INSS_RET_PER = v_tab(i).VL_TOTAL_INSS_RET_PER,
                          VL_TOTAL_IOF = v_tab(i).VL_TOTAL_IOF,
                          VL_TOTAL_IPI = v_tab(i).VL_TOTAL_IPI,
                          VL_TOTAL_IRRF = v_tab(i).VL_TOTAL_IRRF,
                          VL_TOTAL_ISENTO_ICMS = v_tab(i).VL_TOTAL_ISENTO_ICMS,
                          VL_TOTAL_ISS = v_tab(i).VL_TOTAL_ISS,
                          VL_TOTAL_ISS_BITRIBUTADO = v_tab(i).VL_TOTAL_ISS_BITRIBUTADO,
                          VL_TOTAL_MAT_TERC = v_tab(i).VL_TOTAL_MAT_TERC,
                          VL_TOTAL_OUTROS_ICMS = v_tab(i).VL_TOTAL_OUTROS_ICMS,
                          VL_TOTAL_PIS_PASEP = v_tab(i).VL_TOTAL_PIS_PASEP,
                          VL_TOTAL_PIS_RET = v_tab(i).VL_TOTAL_PIS_RET,
                          VL_TOTAL_SENAT = v_tab(i).VL_TOTAL_SENAT,
                          VL_TOTAL_SEST = v_tab(i).VL_TOTAL_SEST,
                          VL_TOTAL_STF = v_tab(i).VL_TOTAL_STF,
                          VL_TOTAL_STF_FRONTEIRA = v_tab(i).VL_TOTAL_STF_FRONTEIRA,
                          VL_TOTAL_STF_IDO = v_tab(i).VL_TOTAL_STF_IDO,
                          VL_TOTAL_STF_SUBSTITUIDO = v_tab(i).VL_TOTAL_STF_SUBSTITUIDO,
                          VL_TOTAL_STT = v_tab(i).VL_TOTAL_STT,
                          VL_TRIBUTAVEL_DIFA = v_tab(i).VL_TRIBUTAVEL_DIFA,
                          VL_TROCO = v_tab(i).VL_TROCO,
                          VL_TX_RED = v_tab(i).VL_TX_RED,
                          VOO_CODIGO = v_tab(i).VOO_CODIGO,
                          VT_CODIGO = v_tab(i).VT_CODIGO,
                          XINF_ID= v_tab(i).XINF_ID
				   where dof_import_numero = v_tab(i).dof_import_numero
				     and informante_est_codigo = v_tab(i).informante_est_codigo;
			  exception when others then
                     r_put_line('Erro ao alterar DOF DUP. - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF - ' || v_tab(i).dof_import_numero|| ' - ' || v_tab(i).hon_consolida_origem);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'E');  
                     commit;
                     end;
  end;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_idf_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql            VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_idf%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_idf_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
     begin
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      exception when others then 
	         null;
              end;
      begin
     INSERT INTO COR_IDF (ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID)
   VALUES (v_tab(i).ALIQ_ANTECIPACAO,
v_tab(i).ALIQ_ANTECIP_ICMS,
v_tab(i).ALIQ_COFINS,
v_tab(i).ALIQ_COFINS_RET,
v_tab(i).ALIQ_COFINS_ST,
v_tab(i).ALIQ_CSLL_RET,
v_tab(i).ALIQ_DIFA,
v_tab(i).ALIQ_DIFA_ICMS_PART,
v_tab(i).ALIQ_ICMS,
v_tab(i).ALIQ_ICMS_DESC_L,
v_tab(i).ALIQ_ICMS_FCP,
v_tab(i).ALIQ_ICMS_FCPST,
v_tab(i).ALIQ_ICMS_PART_DEST,
v_tab(i).ALIQ_ICMS_PART_REM,
v_tab(i).ALIQ_ICMS_STF60,
v_tab(i).ALIQ_II,
v_tab(i).ALIQ_INSS,
v_tab(i).ALIQ_INSS_AE15_RET,
v_tab(i).ALIQ_INSS_AE20_RET,
v_tab(i).ALIQ_INSS_AE25_RET,
v_tab(i).ALIQ_INSS_RET,
v_tab(i).ALIQ_IPI,
v_tab(i).ALIQ_ISS,
v_tab(i).ALIQ_ISS_BITRIBUTADO,
v_tab(i).ALIQ_PIS,
v_tab(i).ALIQ_PIS_RET,
v_tab(i).ALIQ_PIS_ST,
v_tab(i).ALIQ_SENAT,
v_tab(i).ALIQ_SEST,
v_tab(i).ALIQ_STF,
v_tab(i).ALIQ_STT,
v_tab(i).AM_CODIGO,
v_tab(i).CCA_CODIGO,
v_tab(i).CEST_CODIGO,
v_tab(i).CFOP_CODIGO,
v_tab(i).CHASSI_VEIC,
v_tab(i).CLI_CODIGO,
v_tab(i).CNPJ_FABRICANTE,
v_tab(i).CNPJ_PAR,
v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
v_tab(i).CNPJ_SUBEMPREITEIRO,
v_tab(i).COD_AREA,
v_tab(i).COD_BC_CREDITO,
v_tab(i).COD_BENEFICIO_FISCAL,
v_tab(i).COD_CCUS,
v_tab(i).COD_CONJ_KIT,
v_tab(i).COD_FISC_SERV_MUN,
v_tab(i).COD_GTIN,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).COD_IBGE,
v_site,
v_tab(i).CODIGO_ISS_MUNICIPIO,
v_tab(i).CODIGO_RETENCAO,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_INSS,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).COD_NVE,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPF_PAR,
v_tab(i).CSOSN_CODIGO,
v_tab(i).CTRL_DESCARGA,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_OBS_CALC_ICMS,
v_tab(i).CTRL_OBS_CALC_INSS,
v_tab(i).CTRL_OBS_CALC_IPI,
v_tab(i).CTRL_OBS_CALC_IRRF,
v_tab(i).CTRL_OBS_CALC_ISS,
v_tab(i).CTRL_OBS_CALC_STF,
v_tab(i).CTRL_OBS_CALC_STT,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_SETUP_ICMS,
v_tab(i).CTRL_OBS_SETUP_IPI,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_PRECO,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_TOLERANCIA_PED,
v_tab(i).CUSTO_ADICAO,
v_tab(i).CUSTO_REDUCAO,
v_tab(i).C4_SALDO_REF,
v_tab(i).DESCRICAO_ARMA,
v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
v_tab(i).DESONERACAO_CODIGO,
v_dof,
v_sequence,
v_tab(i).DT_DI,
v_tab(i).DT_FAB_MED,
v_tab(i).DT_FIN_SERV,
v_tab(i).DT_INI_SERV,
v_tab(i).DT_REF_CALC_IMP_IDF,
v_tab(i).DT_VAL,
v_tab(i).EMERC_CODIGO,
v_tab(i).ENQ_IPI_CODIGO,
v_tab(i).ENTSAI_UNI_CODIGO,
v_tab(i).ESTOQUE_UNI_CODIGO,
v_tab(i).FAT_BASE_CALCULO_STF,
v_tab(i).FAT_CONV_UNI_ESTOQUE,
v_tab(i).FAT_CONV_UNI_FISCAL,
v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
v_tab(i).FCI_NUMERO,
v_tab(i).FIN_CODIGO,
v_tab(i).FLAG_SUFRAMA,
v_tab(i).HON_COFINS_RET,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_CSLL_RET,
v_tab(i).HON_PIS_RET,
v_tab(i).IDF_NUM,
v_tab(i).IDF_NUM_PAI,
v_tab(i).IDF_TEXTO_COMPLEMENTAR,
v_tab(i).IE_PAR,
v_tab(i).IM_SUBCONTRATACAO,
v_tab(i).IND_ANTECIP_ICMS,
v_tab(i).IND_ARM,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_COFINS_ST,
v_tab(i).IND_COMPROVA_OPERACAO,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_ESCALA_RELEVANTE,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXIGIBILIDADE_ISS,
v_tab(i).IND_FCP_ST,
v_tab(i).IND_INCENTIVO_FISCAL_ISS,
v_tab(i).IND_INCIDENCIA_COFINS,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_COFINS_ST,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_ICMS,
v_tab(i).IND_INCIDENCIA_INSS,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IPI,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_ISS,
v_tab(i).IND_INCIDENCIA_PIS,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_INCIDENCIA_PIS_ST,
v_tab(i).IND_INCIDENCIA_SENAT,
v_tab(i).IND_INCIDENCIA_SEST,
v_tab(i).IND_INCIDENCIA_STF,
v_tab(i).IND_INCIDENCIA_STT,
v_tab(i).IND_IPI_BASE_ICMS,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LAN_FISCAL_ICMS,
v_tab(i).IND_LAN_FISCAL_IPI,
v_tab(i).IND_LAN_FISCAL_STF,
v_tab(i).IND_LAN_FISCAL_STT,
v_tab(i).IND_LAN_IMP,
v_tab(i).IND_MED,
v_tab(i).IND_MOVIMENTACAO_CIAP,
v_tab(i).IND_MOVIMENTACAO_CPC,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_MP_DO_BEM,
v_tab(i).IND_NAT_FRT_PISCOFINS,
v_tab(i).IND_OUTROS_ICMS,
v_tab(i).IND_OUTROS_IPI,
v_tab(i).IND_PAUTA_STF_MVA_MP,
v_tab(i).IND_PIS_ST,
v_tab(i).IND_SERV,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).IND_VL_FISC_VL_CONT,
v_tab(i).IND_VL_FISC_VL_FAT,
v_tab(i).IND_VL_ICMS_NO_PRECO,
v_tab(i).IND_VL_ICMS_VL_CONT,
v_tab(i).IND_VL_ICMS_VL_FAT,
v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
v_tab(i).IND_ZFM_ALC,
v_tab(i).LOTE_MED,
v_tab(i).MERC_CODIGO,
v_tab(i).MOD_BASE_ICMS_CODIGO,
v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
v_tab(i).MUN_CODIGO,
v_tab(i).NAT_REC_PISCOFINS,
v_tab(i).NAT_REC_PISCOFINS_DESCR,
v_tab(i).NBM_CODIGO,
v_tab(i).NBS_CODIGO,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_ARM,
v_tab(i).NUM_CANO,
v_tab(i).NUM_DI,
v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
v_tab(i).NUM_TANQUE,
v_tab(i).OM_CODIGO,
v_tab(i).ORD_IMPRESSAO,
v_tab(i).PARTICIPANTE_PFJ_CODIGO,
v_tab(i).PAUTA_FLUT_ICMS,
v_tab(i).PEDIDO_NUMERO,
v_tab(i).PEDIDO_NUMERO_ITEM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
v_tab(i).PERC_ICMS_PART_DEST,
v_tab(i).PERC_ICMS_PART_REM,
v_tab(i).PERC_IRRF,
v_tab(i).PERC_ISENTO_ICMS,
v_tab(i).PERC_ISENTO_IPI,
v_tab(i).PERC_OUTROS_ABAT,
v_tab(i).PERC_OUTROS_ICMS,
v_tab(i).PERC_OUTROS_IPI,
v_tab(i).PERC_PART_CI,
v_tab(i).PERC_TRIBUTAVEL_COFINS,
v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
v_tab(i).PERC_TRIBUTAVEL_ICMS,
v_tab(i).PERC_TRIBUTAVEL_INSS,
v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
v_tab(i).PERC_TRIBUTAVEL_IPI,
v_tab(i).PERC_TRIBUTAVEL_IRRF,
v_tab(i).PERC_TRIBUTAVEL_ISS,
v_tab(i).PERC_TRIBUTAVEL_PIS,
v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
v_tab(i).PERC_TRIBUTAVEL_SENAT,
v_tab(i).PERC_TRIBUTAVEL_SEST,
v_tab(i).PERC_TRIBUTAVEL_STF,
v_tab(i).PERC_TRIBUTAVEL_STT,
v_tab(i).PER_FISCAL,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PFJ_CODIGO_FORNECEDOR,
v_tab(i).PFJ_CODIGO_TERCEIRO,
v_tab(i).PRECO_NET_UNIT,
v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
v_tab(i).PRECO_TOTAL,
v_tab(i).PRECO_UNITARIO,
v_tab(i).PRES_CODIGO,
v_tab(i).QTD,
v_tab(i).QTD_BASE_COFINS,
v_tab(i).QTD_BASE_PIS,
v_tab(i).QTD_EMB,
v_tab(i).QTD_KIT,
v_tab(i).REVISAO,
v_tab(i).SELO_CODIGO,
v_tab(i).SELO_QTDE,
v_tab(i).SISS_CODIGO,
v_tab(i).STA_CODIGO,
v_tab(i).STC_CODIGO,
v_tab(i).STI_CODIGO,
v_tab(i).STM_CODIGO,
v_tab(i).STN_CODIGO,
v_tab(i).STP_CODIGO,
v_tab(i).SUBCLASSE_IDF,
v_tab(i).TERMINAL,
v_tab(i).TIPO_COMPLEMENTO,
v_tab(i).TIPO_OPER_VEIC,
v_tab(i).TIPO_PROD_MED,
v_tab(i).TIPO_RECEITA,
v_tab(i).TIPO_STF,
v_tab(i).UF_PAR,
v_tab(i).UNI_CODIGO_FISCAL,
v_tab(i).UNI_FISCAL_CODIGO,
v_tab(i).VL_ABAT_LEGAL_INSS_RET,
v_tab(i).VL_ABAT_LEGAL_IRRF,
v_tab(i).VL_ABAT_LEGAL_ISS,
v_tab(i).VL_ADICIONAL_RET_AE,
v_tab(i).VL_AJUSTE_PRECO_TOTAL,
v_tab(i).VL_ALIMENTACAO,
v_tab(i).VL_ALIQ_COFINS,
v_tab(i).VL_ALIQ_PIS,
v_tab(i).VL_ANTECIPACAO,
v_tab(i).VL_ANTECIP_ICMS,
v_tab(i).VL_BASE_COFINS,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_COFINS_ST,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_ICMS,
v_tab(i).VL_BASE_ICMS_DESC_L,
v_tab(i).VL_BASE_ICMS_FCP,
v_tab(i).VL_BASE_ICMS_FCPST,
v_tab(i).VL_BASE_ICMS_PART_DEST,
v_tab(i).VL_BASE_ICMS_PART_REM,
v_tab(i).VL_BASE_II,
v_tab(i).VL_BASE_INSS,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IPI,
v_tab(i).VL_BASE_IRRF,
v_tab(i).VL_BASE_ISS,
v_tab(i).VL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_BASE_PIS,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_PIS_ST,
v_tab(i).VL_BASE_SENAT,
v_tab(i).VL_BASE_SEST,
v_tab(i).VL_BASE_STF,
v_tab(i).VL_BASE_STF_FRONTEIRA,
v_tab(i).VL_BASE_STF_IDO,
v_tab(i).VL_BASE_STF60,
v_tab(i).VL_BASE_STT,
v_tab(i).VL_BASE_SUFRAMA,
v_tab(i).VL_COFINS,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CONTABIL,
v_tab(i).VL_CRED_COFINS_REC_EXPO,
v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
v_tab(i).VL_CRED_COFINS_REC_TRIB,
v_tab(i).VL_CRED_PIS_REC_EXPO,
v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
v_tab(i).VL_CRED_PIS_REC_TRIB,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_INSS,
v_tab(i).VL_DEDUCAO_IRRF_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESC_CP,
v_tab(i).VL_DIFA,
v_tab(i).VL_FATURADO,
v_tab(i).VL_FISCAL,
v_tab(i).VL_GILRAT,
v_tab(i).VL_ICMS,
v_tab(i).VL_ICMS_DESC_L,
v_tab(i).VL_ICMS_FCP,
v_tab(i).VL_ICMS_FCPST,
v_tab(i).VL_ICMS_PART_DEST,
v_tab(i).VL_ICMS_PART_REM,
v_tab(i).VL_ICMS_PROPRIO_RECUP,
v_tab(i).VL_ICMS_SIMPLES_NAC,
v_tab(i).VL_ICMS_ST_RECUP,
v_tab(i).VL_II,
v_tab(i).VL_IMP_FCI,
v_tab(i).VL_IMPOSTO_COFINS,
v_tab(i).VL_IMPOSTO_PIS,
v_tab(i).VL_INSS,
v_tab(i).VL_INSS_AE15_NAO_RET,
v_tab(i).VL_INSS_AE15_RET,
v_tab(i).VL_INSS_AE20_NAO_RET,
v_tab(i).VL_INSS_AE20_RET,
v_tab(i).VL_INSS_AE25_NAO_RET,
v_tab(i).VL_INSS_AE25_RET,
v_tab(i).VL_INSS_NAO_RET,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IOF,
v_tab(i).VL_IPI,
v_tab(i).VL_IRRF,
v_tab(i).VL_IRRF_NAO_RET,
v_tab(i).VL_ISENTO_ICMS,
v_tab(i).VL_ISENTO_IPI,
v_tab(i).VL_ISS,
v_tab(i).VL_ISS_BITRIBUTADO,
v_tab(i).VL_ISS_DESC_CONDICIONADO,
v_tab(i).VL_ISS_DESC_INCONDICIONADO,
v_tab(i).VL_ISS_OUTRAS_RETENCOES,
v_tab(i).VL_MATERIAS_EQUIP,
v_tab(i).VL_NAO_RETIDO_CP,
v_tab(i).VL_NAO_RETIDO_CP_AE,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_OUTROS_ICMS,
v_tab(i).VL_OUTROS_IMPOSTOS,
v_tab(i).VL_OUTROS_IPI,
v_tab(i).VL_PIS,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_RATEIO_AJUSTE_PRECO,
v_tab(i).VL_RATEIO_BASE_CT_STF,
v_tab(i).VL_RATEIO_CT_STF,
v_tab(i).VL_RATEIO_FRETE,
v_tab(i).VL_RATEIO_ODA,
v_tab(i).VL_RATEIO_SEGURO,
v_tab(i).VL_RECUPERADO,
v_tab(i).VL_RETIDO_SUBEMPREITADA,
v_tab(i).VL_SENAR,
v_tab(i).VL_SENAT,
v_tab(i).VL_SERVICO_AE15,
v_tab(i).VL_SERVICO_AE20,
v_tab(i).VL_SERVICO_AE25,
v_tab(i).VL_SEST,
v_tab(i).VL_STF,
v_tab(i).VL_STF_FRONTEIRA,
v_tab(i).VL_STF_IDO,
v_tab(i).VL_ST_FRONTEIRA,
v_tab(i).VL_STF60,
v_tab(i).VL_STT,
v_tab(i).VL_SUFRAMA,
v_tab(i).VL_TAB_MAX,
v_tab(i).VL_TRANSPORTE,
v_tab(i).VL_TRIBUTAVEL_ANTEC,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TRIBUTAVEL_ICMS,
v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
v_tab(i).VL_TRIBUTAVEL_IPI,
v_tab(i).VL_TRIBUTAVEL_STF,
v_tab(i).VL_TRIBUTAVEL_STT,
v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
v_tab(i).XCON_ID,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_idf
				    set
ALIQ_ANTECIPACAO =  v_tab(i).ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS =  v_tab(i).ALIQ_ANTECIP_ICMS,
ALIQ_COFINS =  v_tab(i).ALIQ_COFINS,
ALIQ_COFINS_RET =  v_tab(i).ALIQ_COFINS_RET,
ALIQ_COFINS_ST =  v_tab(i).ALIQ_COFINS_ST,
ALIQ_CSLL_RET =  v_tab(i).ALIQ_CSLL_RET,
ALIQ_DIFA =  v_tab(i).ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART =  v_tab(i).ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS =  v_tab(i).ALIQ_ICMS,
ALIQ_ICMS_DESC_L =  v_tab(i).ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP =  v_tab(i).ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST =  v_tab(i).ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST =  v_tab(i).ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM =  v_tab(i).ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60 =  v_tab(i).ALIQ_ICMS_STF60,
ALIQ_II =  v_tab(i).ALIQ_II,
ALIQ_INSS =  v_tab(i).ALIQ_INSS,
ALIQ_INSS_AE15_RET =  v_tab(i).ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET =  v_tab(i).ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET =  v_tab(i).ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET =  v_tab(i).ALIQ_INSS_RET,
ALIQ_IPI =  v_tab(i).ALIQ_IPI,
ALIQ_ISS =  v_tab(i).ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO =  v_tab(i).ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS =  v_tab(i).ALIQ_PIS,
ALIQ_PIS_RET =  v_tab(i).ALIQ_PIS_RET,
ALIQ_PIS_ST =  v_tab(i).ALIQ_PIS_ST,
ALIQ_SENAT =  v_tab(i).ALIQ_SENAT,
ALIQ_SEST =  v_tab(i).ALIQ_SEST,
ALIQ_STF =  v_tab(i).ALIQ_STF,
ALIQ_STT =  v_tab(i).ALIQ_STT,
AM_CODIGO =  v_tab(i).AM_CODIGO,
CCA_CODIGO =  v_tab(i).CCA_CODIGO,
CEST_CODIGO =  v_tab(i).CEST_CODIGO,
CFOP_CODIGO =  v_tab(i).CFOP_CODIGO,
CHASSI_VEIC =  v_tab(i).CHASSI_VEIC,
CLI_CODIGO =  v_tab(i).CLI_CODIGO,
CNPJ_FABRICANTE =  v_tab(i).CNPJ_FABRICANTE,
CNPJ_PAR =  v_tab(i).CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT =  v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO =  v_tab(i).CNPJ_SUBEMPREITEIRO,
COD_AREA =  v_tab(i).COD_AREA,
COD_BC_CREDITO =  v_tab(i).COD_BC_CREDITO,
COD_BENEFICIO_FISCAL =  v_tab(i).COD_BENEFICIO_FISCAL,
COD_CCUS =  v_tab(i).COD_CCUS,
COD_CONJ_KIT =  v_tab(i).COD_CONJ_KIT,
COD_FISC_SERV_MUN =  v_tab(i).COD_FISC_SERV_MUN,
COD_GTIN =  v_tab(i).COD_GTIN,
COD_IATA_FIM =  v_tab(i).COD_IATA_FIM,
COD_IATA_INI =  v_tab(i).COD_IATA_INI,
COD_IBGE =  v_tab(i).COD_IBGE,
CODIGO_ISS_MUNICIPIO =  v_tab(i).CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO =  v_tab(i).CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS =  v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL =  v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS =  v_tab(i).CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC =  v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS =  v_tab(i).CODIGO_RETENCAO_PIS,
COD_NVE =  v_tab(i).COD_NVE,
CONTA_CONTABIL =  v_tab(i).CONTA_CONTABIL,
CPF_PAR =  v_tab(i).CPF_PAR,
CSOSN_CODIGO =  v_tab(i).CSOSN_CODIGO,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
CTRL_ENTRADA_ST =  v_tab(i).CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS =  v_tab(i).CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS =  v_tab(i).CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI =  v_tab(i).CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF =  v_tab(i).CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS =  v_tab(i).CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF =  v_tab(i).CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT =  v_tab(i).CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ =  v_tab(i).CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS =  v_tab(i).CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI =  v_tab(i).CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO =  v_tab(i).CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO =  v_tab(i).CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL =  v_tab(i).CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST =  v_tab(i).CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED =  v_tab(i).CTRL_TOLERANCIA_PED,
CUSTO_ADICAO =  v_tab(i).CUSTO_ADICAO,
CUSTO_REDUCAO =  v_tab(i).CUSTO_REDUCAO,
C4_SALDO_REF =  v_tab(i).C4_SALDO_REF,
DESCRICAO_ARMA =  v_tab(i).DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO =  v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO =  v_tab(i).DESONERACAO_CODIGO,
DT_DI =  v_tab(i).DT_DI,
DT_FAB_MED =  v_tab(i).DT_FAB_MED,
DT_FIN_SERV =  v_tab(i).DT_FIN_SERV,
DT_INI_SERV =  v_tab(i).DT_INI_SERV,
DT_REF_CALC_IMP_IDF =  v_tab(i).DT_REF_CALC_IMP_IDF,
DT_VAL =  v_tab(i).DT_VAL,
EMERC_CODIGO =  v_tab(i).EMERC_CODIGO,
ENQ_IPI_CODIGO =  v_tab(i).ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO =  v_tab(i).ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO =  v_tab(i).ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF =  v_tab(i).FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE =  v_tab(i).FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL =  v_tab(i).FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO =  v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO =  v_tab(i).FCI_NUMERO,
FIN_CODIGO =  v_tab(i).FIN_CODIGO,
FLAG_SUFRAMA =  v_tab(i).FLAG_SUFRAMA,
HON_COFINS_RET =  v_tab(i).HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET =  v_tab(i).HON_CSLL_RET,
HON_PIS_RET =  v_tab(i).HON_PIS_RET,
IDF_NUM =  v_tab(i).IDF_NUM,
IDF_NUM_PAI =  v_tab(i).IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR =  v_tab(i).IDF_TEXTO_COMPLEMENTAR,
IE_PAR =  v_tab(i).IE_PAR,
IM_SUBCONTRATACAO =  v_tab(i).IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS =  v_tab(i).IND_ANTECIP_ICMS,
IND_ARM =  v_tab(i).IND_ARM,
IND_BLOQUEADO =  v_tab(i).IND_BLOQUEADO,
IND_COFINS_ST =  v_tab(i).IND_COFINS_ST,
IND_COMPROVA_OPERACAO =  v_tab(i).IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO =  v_tab(i).IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE =  v_tab(i).IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO =  v_tab(i).IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS =  v_tab(i).IND_EXIGIBILIDADE_ISS,
IND_FCP_ST =  v_tab(i).IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS =  v_tab(i).IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS =  v_tab(i).IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET =  v_tab(i).IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST =  v_tab(i).IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET =  v_tab(i).IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS =  v_tab(i).IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS =  v_tab(i).IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET =  v_tab(i).IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI =  v_tab(i).IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF =  v_tab(i).IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS =  v_tab(i).IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS =  v_tab(i).IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET =  v_tab(i).IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST =  v_tab(i).IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT =  v_tab(i).IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST =  v_tab(i).IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF =  v_tab(i).IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT =  v_tab(i).IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS =  v_tab(i).IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE =  v_tab(i).IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS =  v_tab(i).IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI =  v_tab(i).IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF =  v_tab(i).IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT =  v_tab(i).IND_LAN_FISCAL_STT,
IND_LAN_IMP =  v_tab(i).IND_LAN_IMP,
IND_MED =  v_tab(i).IND_MED,
IND_MOVIMENTACAO_CIAP =  v_tab(i).IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC =  v_tab(i).IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE =  v_tab(i).IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM =  v_tab(i).IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS =  v_tab(i).IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS =  v_tab(i).IND_OUTROS_ICMS,
IND_OUTROS_IPI =  v_tab(i).IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP =  v_tab(i).IND_PAUTA_STF_MVA_MP,
IND_PIS_ST =  v_tab(i).IND_PIS_ST,
IND_SERV =  v_tab(i).IND_SERV,
IND_VERIFICA_PEDIDO =  v_tab(i).IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT =  v_tab(i).IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT =  v_tab(i).IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO =  v_tab(i).IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT =  v_tab(i).IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT =  v_tab(i).IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO =  v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO =  v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC =  v_tab(i).IND_ZFM_ALC,
LOTE_MED =  v_tab(i).LOTE_MED,
MERC_CODIGO =  v_tab(i).MERC_CODIGO,
MOD_BASE_ICMS_CODIGO =  v_tab(i).MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO =  v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO =  v_tab(i).MUN_CODIGO,
NAT_REC_PISCOFINS =  v_tab(i).NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR =  v_tab(i).NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO =  v_tab(i).NBM_CODIGO,
NBS_CODIGO =  v_tab(i).NBS_CODIGO,
NOP_CODIGO =  v_tab(i).NOP_CODIGO,
NUM_ARM =  v_tab(i).NUM_ARM,
NUM_CANO =  v_tab(i).NUM_CANO,
NUM_DI =  v_tab(i).NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE =  v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE =  v_tab(i).NUM_TANQUE,
OM_CODIGO =  v_tab(i).OM_CODIGO,
ORD_IMPRESSAO =  v_tab(i).ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO =  v_tab(i).PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS =  v_tab(i).PAUTA_FLUT_ICMS,
PEDIDO_NUMERO =  v_tab(i).PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM =  v_tab(i).PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL =  v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST =  v_tab(i).PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM =  v_tab(i).PERC_ICMS_PART_REM,
PERC_IRRF =  v_tab(i).PERC_IRRF,
PERC_ISENTO_ICMS =  v_tab(i).PERC_ISENTO_ICMS,
PERC_ISENTO_IPI =  v_tab(i).PERC_ISENTO_IPI,
PERC_OUTROS_ABAT =  v_tab(i).PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS =  v_tab(i).PERC_OUTROS_ICMS,
PERC_OUTROS_IPI =  v_tab(i).PERC_OUTROS_IPI,
PERC_PART_CI =  v_tab(i).PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS =  v_tab(i).PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST =  v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS =  v_tab(i).PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS =  v_tab(i).PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET =  v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI =  v_tab(i).PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF =  v_tab(i).PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS =  v_tab(i).PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS =  v_tab(i).PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST =  v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT =  v_tab(i).PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST =  v_tab(i).PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF =  v_tab(i).PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT =  v_tab(i).PERC_TRIBUTAVEL_STT,
PER_FISCAL =  v_tab(i).PER_FISCAL,
PESO_BRUTO_KG =  v_tab(i).PESO_BRUTO_KG,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR =  v_tab(i).PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO =  v_tab(i).PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT =  v_tab(i).PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS =  v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL =  v_tab(i).PRECO_TOTAL,
PRECO_UNITARIO =  v_tab(i).PRECO_UNITARIO,
PRES_CODIGO =  v_tab(i).PRES_CODIGO,
QTD =  v_tab(i).QTD,
QTD_BASE_COFINS =  v_tab(i).QTD_BASE_COFINS,
QTD_BASE_PIS =  v_tab(i).QTD_BASE_PIS,
QTD_EMB =  v_tab(i).QTD_EMB,
QTD_KIT =  v_tab(i).QTD_KIT,
REVISAO =  v_tab(i).REVISAO,
SELO_CODIGO =  v_tab(i).SELO_CODIGO,
SELO_QTDE =  v_tab(i).SELO_QTDE,
SISS_CODIGO =  v_tab(i).SISS_CODIGO,
STA_CODIGO =  v_tab(i).STA_CODIGO,
STC_CODIGO =  v_tab(i).STC_CODIGO,
STI_CODIGO =  v_tab(i).STI_CODIGO,
STM_CODIGO =  v_tab(i).STM_CODIGO,
STN_CODIGO =  v_tab(i).STN_CODIGO,
STP_CODIGO =  v_tab(i).STP_CODIGO,
SUBCLASSE_IDF =  v_tab(i).SUBCLASSE_IDF,
TERMINAL =  v_tab(i).TERMINAL,
TIPO_COMPLEMENTO =  v_tab(i).TIPO_COMPLEMENTO,
TIPO_OPER_VEIC =  v_tab(i).TIPO_OPER_VEIC,
TIPO_PROD_MED =  v_tab(i).TIPO_PROD_MED,
TIPO_RECEITA =  v_tab(i).TIPO_RECEITA,
TIPO_STF =  v_tab(i).TIPO_STF,
UF_PAR =  v_tab(i).UF_PAR,
UNI_CODIGO_FISCAL =  v_tab(i).UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO =  v_tab(i).UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET =  v_tab(i).VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF =  v_tab(i).VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS =  v_tab(i).VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE =  v_tab(i).VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL =  v_tab(i).VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO =  v_tab(i).VL_ALIMENTACAO,
VL_ALIQ_COFINS =  v_tab(i).VL_ALIQ_COFINS,
VL_ALIQ_PIS =  v_tab(i).VL_ALIQ_PIS,
VL_ANTECIPACAO =  v_tab(i).VL_ANTECIPACAO,
VL_ANTECIP_ICMS =  v_tab(i).VL_ANTECIP_ICMS,
VL_BASE_COFINS =  v_tab(i).VL_BASE_COFINS,
VL_BASE_COFINS_RET =  v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST =  v_tab(i).VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET =  v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_ICMS =  v_tab(i).VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L =  v_tab(i).VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP =  v_tab(i).VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST =  v_tab(i).VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST =  v_tab(i).VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM =  v_tab(i).VL_BASE_ICMS_PART_REM,
VL_BASE_II =  v_tab(i).VL_BASE_II,
VL_BASE_INSS =  v_tab(i).VL_BASE_INSS,
VL_BASE_INSS_RET =  v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IPI =  v_tab(i).VL_BASE_IPI,
VL_BASE_IRRF =  v_tab(i).VL_BASE_IRRF,
VL_BASE_ISS =  v_tab(i).VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO =  v_tab(i).VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS =  v_tab(i).VL_BASE_PIS,
VL_BASE_PIS_RET =  v_tab(i).VL_BASE_PIS_RET,
VL_BASE_PIS_ST =  v_tab(i).VL_BASE_PIS_ST,
VL_BASE_SENAT =  v_tab(i).VL_BASE_SENAT,
VL_BASE_SEST =  v_tab(i).VL_BASE_SEST,
VL_BASE_STF =  v_tab(i).VL_BASE_STF,
VL_BASE_STF_FRONTEIRA =  v_tab(i).VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO =  v_tab(i).VL_BASE_STF_IDO,
VL_BASE_STF60 =  v_tab(i).VL_BASE_STF60,
VL_BASE_STT =  v_tab(i).VL_BASE_STT,
VL_BASE_SUFRAMA =  v_tab(i).VL_BASE_SUFRAMA,
VL_COFINS =  v_tab(i).VL_COFINS,
VL_COFINS_RET =  v_tab(i).VL_COFINS_RET,
VL_COFINS_ST =  v_tab(i).VL_COFINS_ST,
VL_CONTABIL =  v_tab(i).VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO =  v_tab(i).VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB =  v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB =  v_tab(i).VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO =  v_tab(i).VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB =  v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB =  v_tab(i).VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET =  v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG =  v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS =  v_tab(i).VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG =  v_tab(i).VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG =  v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP =  v_tab(i).VL_DESC_CP,
VL_DIFA =  v_tab(i).VL_DIFA,
VL_FATURADO =  v_tab(i).VL_FATURADO,
VL_FISCAL =  v_tab(i).VL_FISCAL,
VL_GILRAT =  v_tab(i).VL_GILRAT,
VL_ICMS =  v_tab(i).VL_ICMS,
VL_ICMS_DESC_L =  v_tab(i).VL_ICMS_DESC_L,
VL_ICMS_FCP =  v_tab(i).VL_ICMS_FCP,
VL_ICMS_FCPST =  v_tab(i).VL_ICMS_FCPST,
VL_ICMS_PART_DEST =  v_tab(i).VL_ICMS_PART_DEST,
VL_ICMS_PART_REM =  v_tab(i).VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP =  v_tab(i).VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC =  v_tab(i).VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP =  v_tab(i).VL_ICMS_ST_RECUP,
VL_II =  v_tab(i).VL_II,
VL_IMP_FCI =  v_tab(i).VL_IMP_FCI,
VL_IMPOSTO_COFINS =  v_tab(i).VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS =  v_tab(i).VL_IMPOSTO_PIS,
VL_INSS =  v_tab(i).VL_INSS,
VL_INSS_AE15_NAO_RET =  v_tab(i).VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET =  v_tab(i).VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET =  v_tab(i).VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET =  v_tab(i).VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET =  v_tab(i).VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET =  v_tab(i).VL_INSS_AE25_RET,
VL_INSS_NAO_RET =  v_tab(i).VL_INSS_NAO_RET,
VL_INSS_RET =  v_tab(i).VL_INSS_RET,
VL_IOF =  v_tab(i).VL_IOF,
VL_IPI =  v_tab(i).VL_IPI,
VL_IRRF =  v_tab(i).VL_IRRF,
VL_IRRF_NAO_RET =  v_tab(i).VL_IRRF_NAO_RET,
VL_ISENTO_ICMS =  v_tab(i).VL_ISENTO_ICMS,
VL_ISENTO_IPI =  v_tab(i).VL_ISENTO_IPI,
VL_ISS =  v_tab(i).VL_ISS,
VL_ISS_BITRIBUTADO =  v_tab(i).VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO =  v_tab(i).VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO =  v_tab(i).VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES =  v_tab(i).VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP =  v_tab(i).VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP =  v_tab(i).VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE =  v_tab(i).VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT =  v_tab(i).VL_OUTROS_ABAT,
VL_OUTROS_ICMS =  v_tab(i).VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS =  v_tab(i).VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI =  v_tab(i).VL_OUTROS_IPI,
VL_PIS =  v_tab(i).VL_PIS,
VL_PIS_RET =  v_tab(i).VL_PIS_RET,
VL_PIS_ST =  v_tab(i).VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO =  v_tab(i).VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF =  v_tab(i).VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF =  v_tab(i).VL_RATEIO_CT_STF,
VL_RATEIO_FRETE =  v_tab(i).VL_RATEIO_FRETE,
VL_RATEIO_ODA =  v_tab(i).VL_RATEIO_ODA,
VL_RATEIO_SEGURO =  v_tab(i).VL_RATEIO_SEGURO,
VL_RECUPERADO =  v_tab(i).VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA =  v_tab(i).VL_RETIDO_SUBEMPREITADA,
VL_SENAR =  v_tab(i).VL_SENAR,
VL_SENAT =  v_tab(i).VL_SENAT,
VL_SERVICO_AE15 =  v_tab(i).VL_SERVICO_AE15,
VL_SERVICO_AE20 =  v_tab(i).VL_SERVICO_AE20,
VL_SERVICO_AE25 =  v_tab(i).VL_SERVICO_AE25,
VL_SEST =  v_tab(i).VL_SEST,
VL_STF =  v_tab(i).VL_STF,
VL_STF_FRONTEIRA =  v_tab(i).VL_STF_FRONTEIRA,
VL_STF_IDO =  v_tab(i).VL_STF_IDO,
VL_ST_FRONTEIRA =  v_tab(i).VL_ST_FRONTEIRA,
VL_STF60 =  v_tab(i).VL_STF60,
VL_STT =  v_tab(i).VL_STT,
VL_SUFRAMA =  v_tab(i).VL_SUFRAMA,
VL_TAB_MAX =  v_tab(i).VL_TAB_MAX,
VL_TRANSPORTE =  v_tab(i).VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC =  v_tab(i).VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA =  v_tab(i).VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS =  v_tab(i).VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L =  v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI =  v_tab(i).VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF =  v_tab(i).VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT =  v_tab(i).VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA =  v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
XCON_ID =  v_tab(i).XCON_ID,
XINF_ID =  v_tab(i).XINF_ID
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and idf_num = v_tab(i).idf_num;
			  exception when others then
                     r_put_line('Erro ao alterar IDF DUP. - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir IDF - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
  end;
  else
     delete from hon_gttemp_idf
      where id   = v_tab(i).id;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_idf_fserv (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql            VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_idf%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_idf_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo in ('NFS','NFS-E')
        ;
     if v_tot > 0
     then
     begin
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      exception when others then 
	         null;
              end;
      begin
     INSERT INTO COR_IDF (ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID)
   VALUES (v_tab(i).ALIQ_ANTECIPACAO,
v_tab(i).ALIQ_ANTECIP_ICMS,
v_tab(i).ALIQ_COFINS,
v_tab(i).ALIQ_COFINS_RET,
v_tab(i).ALIQ_COFINS_ST,
v_tab(i).ALIQ_CSLL_RET,
v_tab(i).ALIQ_DIFA,
v_tab(i).ALIQ_DIFA_ICMS_PART,
v_tab(i).ALIQ_ICMS,
v_tab(i).ALIQ_ICMS_DESC_L,
v_tab(i).ALIQ_ICMS_FCP,
v_tab(i).ALIQ_ICMS_FCPST,
v_tab(i).ALIQ_ICMS_PART_DEST,
v_tab(i).ALIQ_ICMS_PART_REM,
v_tab(i).ALIQ_ICMS_STF60,
v_tab(i).ALIQ_II,
v_tab(i).ALIQ_INSS,
v_tab(i).ALIQ_INSS_AE15_RET,
v_tab(i).ALIQ_INSS_AE20_RET,
v_tab(i).ALIQ_INSS_AE25_RET,
v_tab(i).ALIQ_INSS_RET,
v_tab(i).ALIQ_IPI,
v_tab(i).ALIQ_ISS,
v_tab(i).ALIQ_ISS_BITRIBUTADO,
v_tab(i).ALIQ_PIS,
v_tab(i).ALIQ_PIS_RET,
v_tab(i).ALIQ_PIS_ST,
v_tab(i).ALIQ_SENAT,
v_tab(i).ALIQ_SEST,
v_tab(i).ALIQ_STF,
v_tab(i).ALIQ_STT,
v_tab(i).AM_CODIGO,
v_tab(i).CCA_CODIGO,
v_tab(i).CEST_CODIGO,
v_tab(i).CFOP_CODIGO,
v_tab(i).CHASSI_VEIC,
v_tab(i).CLI_CODIGO,
v_tab(i).CNPJ_FABRICANTE,
v_tab(i).CNPJ_PAR,
v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
v_tab(i).CNPJ_SUBEMPREITEIRO,
v_tab(i).COD_AREA,
v_tab(i).COD_BC_CREDITO,
v_tab(i).COD_BENEFICIO_FISCAL,
v_tab(i).COD_CCUS,
v_tab(i).COD_CONJ_KIT,
v_tab(i).COD_FISC_SERV_MUN,
v_tab(i).COD_GTIN,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).COD_IBGE,
v_site,
v_tab(i).CODIGO_ISS_MUNICIPIO,
v_tab(i).CODIGO_RETENCAO,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_INSS,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).COD_NVE,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPF_PAR,
v_tab(i).CSOSN_CODIGO,
v_tab(i).CTRL_DESCARGA,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_OBS_CALC_ICMS,
v_tab(i).CTRL_OBS_CALC_INSS,
v_tab(i).CTRL_OBS_CALC_IPI,
v_tab(i).CTRL_OBS_CALC_IRRF,
v_tab(i).CTRL_OBS_CALC_ISS,
v_tab(i).CTRL_OBS_CALC_STF,
v_tab(i).CTRL_OBS_CALC_STT,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_SETUP_ICMS,
v_tab(i).CTRL_OBS_SETUP_IPI,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_PRECO,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_TOLERANCIA_PED,
v_tab(i).CUSTO_ADICAO,
v_tab(i).CUSTO_REDUCAO,
v_tab(i).C4_SALDO_REF,
v_tab(i).DESCRICAO_ARMA,
v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
v_tab(i).DESONERACAO_CODIGO,
v_dof,
v_sequence,
v_tab(i).DT_DI,
v_tab(i).DT_FAB_MED,
v_tab(i).DT_FIN_SERV,
v_tab(i).DT_INI_SERV,
v_tab(i).DT_REF_CALC_IMP_IDF,
v_tab(i).DT_VAL,
v_tab(i).EMERC_CODIGO,
v_tab(i).ENQ_IPI_CODIGO,
v_tab(i).ENTSAI_UNI_CODIGO,
v_tab(i).ESTOQUE_UNI_CODIGO,
v_tab(i).FAT_BASE_CALCULO_STF,
v_tab(i).FAT_CONV_UNI_ESTOQUE,
v_tab(i).FAT_CONV_UNI_FISCAL,
v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
v_tab(i).FCI_NUMERO,
v_tab(i).FIN_CODIGO,
v_tab(i).FLAG_SUFRAMA,
v_tab(i).HON_COFINS_RET,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_CSLL_RET,
v_tab(i).HON_PIS_RET,
v_tab(i).IDF_NUM,
v_tab(i).IDF_NUM_PAI,
v_tab(i).IDF_TEXTO_COMPLEMENTAR,
v_tab(i).IE_PAR,
v_tab(i).IM_SUBCONTRATACAO,
v_tab(i).IND_ANTECIP_ICMS,
v_tab(i).IND_ARM,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_COFINS_ST,
v_tab(i).IND_COMPROVA_OPERACAO,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_ESCALA_RELEVANTE,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXIGIBILIDADE_ISS,
v_tab(i).IND_FCP_ST,
v_tab(i).IND_INCENTIVO_FISCAL_ISS,
v_tab(i).IND_INCIDENCIA_COFINS,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_COFINS_ST,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_ICMS,
v_tab(i).IND_INCIDENCIA_INSS,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IPI,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_ISS,
v_tab(i).IND_INCIDENCIA_PIS,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_INCIDENCIA_PIS_ST,
v_tab(i).IND_INCIDENCIA_SENAT,
v_tab(i).IND_INCIDENCIA_SEST,
v_tab(i).IND_INCIDENCIA_STF,
v_tab(i).IND_INCIDENCIA_STT,
v_tab(i).IND_IPI_BASE_ICMS,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LAN_FISCAL_ICMS,
v_tab(i).IND_LAN_FISCAL_IPI,
v_tab(i).IND_LAN_FISCAL_STF,
v_tab(i).IND_LAN_FISCAL_STT,
v_tab(i).IND_LAN_IMP,
v_tab(i).IND_MED,
v_tab(i).IND_MOVIMENTACAO_CIAP,
v_tab(i).IND_MOVIMENTACAO_CPC,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_MP_DO_BEM,
v_tab(i).IND_NAT_FRT_PISCOFINS,
v_tab(i).IND_OUTROS_ICMS,
v_tab(i).IND_OUTROS_IPI,
v_tab(i).IND_PAUTA_STF_MVA_MP,
v_tab(i).IND_PIS_ST,
v_tab(i).IND_SERV,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).IND_VL_FISC_VL_CONT,
v_tab(i).IND_VL_FISC_VL_FAT,
v_tab(i).IND_VL_ICMS_NO_PRECO,
v_tab(i).IND_VL_ICMS_VL_CONT,
v_tab(i).IND_VL_ICMS_VL_FAT,
v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
v_tab(i).IND_ZFM_ALC,
v_tab(i).LOTE_MED,
v_tab(i).MERC_CODIGO,
v_tab(i).MOD_BASE_ICMS_CODIGO,
v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
v_tab(i).MUN_CODIGO,
v_tab(i).NAT_REC_PISCOFINS,
v_tab(i).NAT_REC_PISCOFINS_DESCR,
v_tab(i).NBM_CODIGO,
v_tab(i).NBS_CODIGO,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_ARM,
v_tab(i).NUM_CANO,
v_tab(i).NUM_DI,
v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
v_tab(i).NUM_TANQUE,
v_tab(i).OM_CODIGO,
v_tab(i).ORD_IMPRESSAO,
v_tab(i).PARTICIPANTE_PFJ_CODIGO,
v_tab(i).PAUTA_FLUT_ICMS,
v_tab(i).PEDIDO_NUMERO,
v_tab(i).PEDIDO_NUMERO_ITEM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
v_tab(i).PERC_ICMS_PART_DEST,
v_tab(i).PERC_ICMS_PART_REM,
v_tab(i).PERC_IRRF,
v_tab(i).PERC_ISENTO_ICMS,
v_tab(i).PERC_ISENTO_IPI,
v_tab(i).PERC_OUTROS_ABAT,
v_tab(i).PERC_OUTROS_ICMS,
v_tab(i).PERC_OUTROS_IPI,
v_tab(i).PERC_PART_CI,
v_tab(i).PERC_TRIBUTAVEL_COFINS,
v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
v_tab(i).PERC_TRIBUTAVEL_ICMS,
v_tab(i).PERC_TRIBUTAVEL_INSS,
v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
v_tab(i).PERC_TRIBUTAVEL_IPI,
v_tab(i).PERC_TRIBUTAVEL_IRRF,
v_tab(i).PERC_TRIBUTAVEL_ISS,
v_tab(i).PERC_TRIBUTAVEL_PIS,
v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
v_tab(i).PERC_TRIBUTAVEL_SENAT,
v_tab(i).PERC_TRIBUTAVEL_SEST,
v_tab(i).PERC_TRIBUTAVEL_STF,
v_tab(i).PERC_TRIBUTAVEL_STT,
v_tab(i).PER_FISCAL,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PFJ_CODIGO_FORNECEDOR,
v_tab(i).PFJ_CODIGO_TERCEIRO,
v_tab(i).PRECO_NET_UNIT,
v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
v_tab(i).PRECO_TOTAL,
v_tab(i).PRECO_UNITARIO,
v_tab(i).PRES_CODIGO,
v_tab(i).QTD,
v_tab(i).QTD_BASE_COFINS,
v_tab(i).QTD_BASE_PIS,
v_tab(i).QTD_EMB,
v_tab(i).QTD_KIT,
v_tab(i).REVISAO,
v_tab(i).SELO_CODIGO,
v_tab(i).SELO_QTDE,
v_tab(i).SISS_CODIGO,
v_tab(i).STA_CODIGO,
v_tab(i).STC_CODIGO,
v_tab(i).STI_CODIGO,
v_tab(i).STM_CODIGO,
v_tab(i).STN_CODIGO,
v_tab(i).STP_CODIGO,
v_tab(i).SUBCLASSE_IDF,
v_tab(i).TERMINAL,
v_tab(i).TIPO_COMPLEMENTO,
v_tab(i).TIPO_OPER_VEIC,
v_tab(i).TIPO_PROD_MED,
v_tab(i).TIPO_RECEITA,
v_tab(i).TIPO_STF,
v_tab(i).UF_PAR,
v_tab(i).UNI_CODIGO_FISCAL,
v_tab(i).UNI_FISCAL_CODIGO,
v_tab(i).VL_ABAT_LEGAL_INSS_RET,
v_tab(i).VL_ABAT_LEGAL_IRRF,
v_tab(i).VL_ABAT_LEGAL_ISS,
v_tab(i).VL_ADICIONAL_RET_AE,
v_tab(i).VL_AJUSTE_PRECO_TOTAL,
v_tab(i).VL_ALIMENTACAO,
v_tab(i).VL_ALIQ_COFINS,
v_tab(i).VL_ALIQ_PIS,
v_tab(i).VL_ANTECIPACAO,
v_tab(i).VL_ANTECIP_ICMS,
v_tab(i).VL_BASE_COFINS,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_COFINS_ST,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_ICMS,
v_tab(i).VL_BASE_ICMS_DESC_L,
v_tab(i).VL_BASE_ICMS_FCP,
v_tab(i).VL_BASE_ICMS_FCPST,
v_tab(i).VL_BASE_ICMS_PART_DEST,
v_tab(i).VL_BASE_ICMS_PART_REM,
v_tab(i).VL_BASE_II,
v_tab(i).VL_BASE_INSS,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IPI,
v_tab(i).VL_BASE_IRRF,
v_tab(i).VL_BASE_ISS,
v_tab(i).VL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_BASE_PIS,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_PIS_ST,
v_tab(i).VL_BASE_SENAT,
v_tab(i).VL_BASE_SEST,
v_tab(i).VL_BASE_STF,
v_tab(i).VL_BASE_STF_FRONTEIRA,
v_tab(i).VL_BASE_STF_IDO,
v_tab(i).VL_BASE_STF60,
v_tab(i).VL_BASE_STT,
v_tab(i).VL_BASE_SUFRAMA,
v_tab(i).VL_COFINS,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CONTABIL,
v_tab(i).VL_CRED_COFINS_REC_EXPO,
v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
v_tab(i).VL_CRED_COFINS_REC_TRIB,
v_tab(i).VL_CRED_PIS_REC_EXPO,
v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
v_tab(i).VL_CRED_PIS_REC_TRIB,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_INSS,
v_tab(i).VL_DEDUCAO_IRRF_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESC_CP,
v_tab(i).VL_DIFA,
v_tab(i).VL_FATURADO,
v_tab(i).VL_FISCAL,
v_tab(i).VL_GILRAT,
v_tab(i).VL_ICMS,
v_tab(i).VL_ICMS_DESC_L,
v_tab(i).VL_ICMS_FCP,
v_tab(i).VL_ICMS_FCPST,
v_tab(i).VL_ICMS_PART_DEST,
v_tab(i).VL_ICMS_PART_REM,
v_tab(i).VL_ICMS_PROPRIO_RECUP,
v_tab(i).VL_ICMS_SIMPLES_NAC,
v_tab(i).VL_ICMS_ST_RECUP,
v_tab(i).VL_II,
v_tab(i).VL_IMP_FCI,
v_tab(i).VL_IMPOSTO_COFINS,
v_tab(i).VL_IMPOSTO_PIS,
v_tab(i).VL_INSS,
v_tab(i).VL_INSS_AE15_NAO_RET,
v_tab(i).VL_INSS_AE15_RET,
v_tab(i).VL_INSS_AE20_NAO_RET,
v_tab(i).VL_INSS_AE20_RET,
v_tab(i).VL_INSS_AE25_NAO_RET,
v_tab(i).VL_INSS_AE25_RET,
v_tab(i).VL_INSS_NAO_RET,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IOF,
v_tab(i).VL_IPI,
v_tab(i).VL_IRRF,
v_tab(i).VL_IRRF_NAO_RET,
v_tab(i).VL_ISENTO_ICMS,
v_tab(i).VL_ISENTO_IPI,
v_tab(i).VL_ISS,
v_tab(i).VL_ISS_BITRIBUTADO,
v_tab(i).VL_ISS_DESC_CONDICIONADO,
v_tab(i).VL_ISS_DESC_INCONDICIONADO,
v_tab(i).VL_ISS_OUTRAS_RETENCOES,
v_tab(i).VL_MATERIAS_EQUIP,
v_tab(i).VL_NAO_RETIDO_CP,
v_tab(i).VL_NAO_RETIDO_CP_AE,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_OUTROS_ICMS,
v_tab(i).VL_OUTROS_IMPOSTOS,
v_tab(i).VL_OUTROS_IPI,
v_tab(i).VL_PIS,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_RATEIO_AJUSTE_PRECO,
v_tab(i).VL_RATEIO_BASE_CT_STF,
v_tab(i).VL_RATEIO_CT_STF,
v_tab(i).VL_RATEIO_FRETE,
v_tab(i).VL_RATEIO_ODA,
v_tab(i).VL_RATEIO_SEGURO,
v_tab(i).VL_RECUPERADO,
v_tab(i).VL_RETIDO_SUBEMPREITADA,
v_tab(i).VL_SENAR,
v_tab(i).VL_SENAT,
v_tab(i).VL_SERVICO_AE15,
v_tab(i).VL_SERVICO_AE20,
v_tab(i).VL_SERVICO_AE25,
v_tab(i).VL_SEST,
v_tab(i).VL_STF,
v_tab(i).VL_STF_FRONTEIRA,
v_tab(i).VL_STF_IDO,
v_tab(i).VL_ST_FRONTEIRA,
v_tab(i).VL_STF60,
v_tab(i).VL_STT,
v_tab(i).VL_SUFRAMA,
v_tab(i).VL_TAB_MAX,
v_tab(i).VL_TRANSPORTE,
v_tab(i).VL_TRIBUTAVEL_ANTEC,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TRIBUTAVEL_ICMS,
v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
v_tab(i).VL_TRIBUTAVEL_IPI,
v_tab(i).VL_TRIBUTAVEL_STF,
v_tab(i).VL_TRIBUTAVEL_STT,
v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
v_tab(i).XCON_ID,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_idf
				    set
ALIQ_ANTECIPACAO =  v_tab(i).ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS =  v_tab(i).ALIQ_ANTECIP_ICMS,
ALIQ_COFINS =  v_tab(i).ALIQ_COFINS,
ALIQ_COFINS_RET =  v_tab(i).ALIQ_COFINS_RET,
ALIQ_COFINS_ST =  v_tab(i).ALIQ_COFINS_ST,
ALIQ_CSLL_RET =  v_tab(i).ALIQ_CSLL_RET,
ALIQ_DIFA =  v_tab(i).ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART =  v_tab(i).ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS =  v_tab(i).ALIQ_ICMS,
ALIQ_ICMS_DESC_L =  v_tab(i).ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP =  v_tab(i).ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST =  v_tab(i).ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST =  v_tab(i).ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM =  v_tab(i).ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60 =  v_tab(i).ALIQ_ICMS_STF60,
ALIQ_II =  v_tab(i).ALIQ_II,
ALIQ_INSS =  v_tab(i).ALIQ_INSS,
ALIQ_INSS_AE15_RET =  v_tab(i).ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET =  v_tab(i).ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET =  v_tab(i).ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET =  v_tab(i).ALIQ_INSS_RET,
ALIQ_IPI =  v_tab(i).ALIQ_IPI,
ALIQ_ISS =  v_tab(i).ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO =  v_tab(i).ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS =  v_tab(i).ALIQ_PIS,
ALIQ_PIS_RET =  v_tab(i).ALIQ_PIS_RET,
ALIQ_PIS_ST =  v_tab(i).ALIQ_PIS_ST,
ALIQ_SENAT =  v_tab(i).ALIQ_SENAT,
ALIQ_SEST =  v_tab(i).ALIQ_SEST,
ALIQ_STF =  v_tab(i).ALIQ_STF,
ALIQ_STT =  v_tab(i).ALIQ_STT,
AM_CODIGO =  v_tab(i).AM_CODIGO,
CCA_CODIGO =  v_tab(i).CCA_CODIGO,
CEST_CODIGO =  v_tab(i).CEST_CODIGO,
CFOP_CODIGO =  v_tab(i).CFOP_CODIGO,
CHASSI_VEIC =  v_tab(i).CHASSI_VEIC,
CLI_CODIGO =  v_tab(i).CLI_CODIGO,
CNPJ_FABRICANTE =  v_tab(i).CNPJ_FABRICANTE,
CNPJ_PAR =  v_tab(i).CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT =  v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO =  v_tab(i).CNPJ_SUBEMPREITEIRO,
COD_AREA =  v_tab(i).COD_AREA,
COD_BC_CREDITO =  v_tab(i).COD_BC_CREDITO,
COD_BENEFICIO_FISCAL =  v_tab(i).COD_BENEFICIO_FISCAL,
COD_CCUS =  v_tab(i).COD_CCUS,
COD_CONJ_KIT =  v_tab(i).COD_CONJ_KIT,
COD_FISC_SERV_MUN =  v_tab(i).COD_FISC_SERV_MUN,
COD_GTIN =  v_tab(i).COD_GTIN,
COD_IATA_FIM =  v_tab(i).COD_IATA_FIM,
COD_IATA_INI =  v_tab(i).COD_IATA_INI,
COD_IBGE =  v_tab(i).COD_IBGE,
CODIGO_ISS_MUNICIPIO =  v_tab(i).CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO =  v_tab(i).CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS =  v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL =  v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS =  v_tab(i).CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC =  v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS =  v_tab(i).CODIGO_RETENCAO_PIS,
COD_NVE =  v_tab(i).COD_NVE,
CONTA_CONTABIL =  v_tab(i).CONTA_CONTABIL,
CPF_PAR =  v_tab(i).CPF_PAR,
CSOSN_CODIGO =  v_tab(i).CSOSN_CODIGO,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
CTRL_ENTRADA_ST =  v_tab(i).CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS =  v_tab(i).CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS =  v_tab(i).CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI =  v_tab(i).CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF =  v_tab(i).CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS =  v_tab(i).CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF =  v_tab(i).CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT =  v_tab(i).CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ =  v_tab(i).CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS =  v_tab(i).CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI =  v_tab(i).CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO =  v_tab(i).CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO =  v_tab(i).CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL =  v_tab(i).CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST =  v_tab(i).CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED =  v_tab(i).CTRL_TOLERANCIA_PED,
CUSTO_ADICAO =  v_tab(i).CUSTO_ADICAO,
CUSTO_REDUCAO =  v_tab(i).CUSTO_REDUCAO,
C4_SALDO_REF =  v_tab(i).C4_SALDO_REF,
DESCRICAO_ARMA =  v_tab(i).DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO =  v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO =  v_tab(i).DESONERACAO_CODIGO,
DT_DI =  v_tab(i).DT_DI,
DT_FAB_MED =  v_tab(i).DT_FAB_MED,
DT_FIN_SERV =  v_tab(i).DT_FIN_SERV,
DT_INI_SERV =  v_tab(i).DT_INI_SERV,
DT_REF_CALC_IMP_IDF =  v_tab(i).DT_REF_CALC_IMP_IDF,
DT_VAL =  v_tab(i).DT_VAL,
EMERC_CODIGO =  v_tab(i).EMERC_CODIGO,
ENQ_IPI_CODIGO =  v_tab(i).ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO =  v_tab(i).ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO =  v_tab(i).ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF =  v_tab(i).FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE =  v_tab(i).FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL =  v_tab(i).FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO =  v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO =  v_tab(i).FCI_NUMERO,
FIN_CODIGO =  v_tab(i).FIN_CODIGO,
FLAG_SUFRAMA =  v_tab(i).FLAG_SUFRAMA,
HON_COFINS_RET =  v_tab(i).HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET =  v_tab(i).HON_CSLL_RET,
HON_PIS_RET =  v_tab(i).HON_PIS_RET,
IDF_NUM =  v_tab(i).IDF_NUM,
IDF_NUM_PAI =  v_tab(i).IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR =  v_tab(i).IDF_TEXTO_COMPLEMENTAR,
IE_PAR =  v_tab(i).IE_PAR,
IM_SUBCONTRATACAO =  v_tab(i).IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS =  v_tab(i).IND_ANTECIP_ICMS,
IND_ARM =  v_tab(i).IND_ARM,
IND_BLOQUEADO =  v_tab(i).IND_BLOQUEADO,
IND_COFINS_ST =  v_tab(i).IND_COFINS_ST,
IND_COMPROVA_OPERACAO =  v_tab(i).IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO =  v_tab(i).IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE =  v_tab(i).IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO =  v_tab(i).IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS =  v_tab(i).IND_EXIGIBILIDADE_ISS,
IND_FCP_ST =  v_tab(i).IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS =  v_tab(i).IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS =  v_tab(i).IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET =  v_tab(i).IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST =  v_tab(i).IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET =  v_tab(i).IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS =  v_tab(i).IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS =  v_tab(i).IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET =  v_tab(i).IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI =  v_tab(i).IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF =  v_tab(i).IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS =  v_tab(i).IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS =  v_tab(i).IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET =  v_tab(i).IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST =  v_tab(i).IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT =  v_tab(i).IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST =  v_tab(i).IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF =  v_tab(i).IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT =  v_tab(i).IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS =  v_tab(i).IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE =  v_tab(i).IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS =  v_tab(i).IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI =  v_tab(i).IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF =  v_tab(i).IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT =  v_tab(i).IND_LAN_FISCAL_STT,
IND_LAN_IMP =  v_tab(i).IND_LAN_IMP,
IND_MED =  v_tab(i).IND_MED,
IND_MOVIMENTACAO_CIAP =  v_tab(i).IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC =  v_tab(i).IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE =  v_tab(i).IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM =  v_tab(i).IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS =  v_tab(i).IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS =  v_tab(i).IND_OUTROS_ICMS,
IND_OUTROS_IPI =  v_tab(i).IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP =  v_tab(i).IND_PAUTA_STF_MVA_MP,
IND_PIS_ST =  v_tab(i).IND_PIS_ST,
IND_SERV =  v_tab(i).IND_SERV,
IND_VERIFICA_PEDIDO =  v_tab(i).IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT =  v_tab(i).IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT =  v_tab(i).IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO =  v_tab(i).IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT =  v_tab(i).IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT =  v_tab(i).IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO =  v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO =  v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC =  v_tab(i).IND_ZFM_ALC,
LOTE_MED =  v_tab(i).LOTE_MED,
MERC_CODIGO =  v_tab(i).MERC_CODIGO,
MOD_BASE_ICMS_CODIGO =  v_tab(i).MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO =  v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO =  v_tab(i).MUN_CODIGO,
NAT_REC_PISCOFINS =  v_tab(i).NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR =  v_tab(i).NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO =  v_tab(i).NBM_CODIGO,
NBS_CODIGO =  v_tab(i).NBS_CODIGO,
NOP_CODIGO =  v_tab(i).NOP_CODIGO,
NUM_ARM =  v_tab(i).NUM_ARM,
NUM_CANO =  v_tab(i).NUM_CANO,
NUM_DI =  v_tab(i).NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE =  v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE =  v_tab(i).NUM_TANQUE,
OM_CODIGO =  v_tab(i).OM_CODIGO,
ORD_IMPRESSAO =  v_tab(i).ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO =  v_tab(i).PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS =  v_tab(i).PAUTA_FLUT_ICMS,
PEDIDO_NUMERO =  v_tab(i).PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM =  v_tab(i).PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL =  v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST =  v_tab(i).PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM =  v_tab(i).PERC_ICMS_PART_REM,
PERC_IRRF =  v_tab(i).PERC_IRRF,
PERC_ISENTO_ICMS =  v_tab(i).PERC_ISENTO_ICMS,
PERC_ISENTO_IPI =  v_tab(i).PERC_ISENTO_IPI,
PERC_OUTROS_ABAT =  v_tab(i).PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS =  v_tab(i).PERC_OUTROS_ICMS,
PERC_OUTROS_IPI =  v_tab(i).PERC_OUTROS_IPI,
PERC_PART_CI =  v_tab(i).PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS =  v_tab(i).PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST =  v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS =  v_tab(i).PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS =  v_tab(i).PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET =  v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI =  v_tab(i).PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF =  v_tab(i).PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS =  v_tab(i).PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS =  v_tab(i).PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST =  v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT =  v_tab(i).PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST =  v_tab(i).PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF =  v_tab(i).PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT =  v_tab(i).PERC_TRIBUTAVEL_STT,
PER_FISCAL =  v_tab(i).PER_FISCAL,
PESO_BRUTO_KG =  v_tab(i).PESO_BRUTO_KG,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR =  v_tab(i).PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO =  v_tab(i).PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT =  v_tab(i).PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS =  v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL =  v_tab(i).PRECO_TOTAL,
PRECO_UNITARIO =  v_tab(i).PRECO_UNITARIO,
PRES_CODIGO =  v_tab(i).PRES_CODIGO,
QTD =  v_tab(i).QTD,
QTD_BASE_COFINS =  v_tab(i).QTD_BASE_COFINS,
QTD_BASE_PIS =  v_tab(i).QTD_BASE_PIS,
QTD_EMB =  v_tab(i).QTD_EMB,
QTD_KIT =  v_tab(i).QTD_KIT,
REVISAO =  v_tab(i).REVISAO,
SELO_CODIGO =  v_tab(i).SELO_CODIGO,
SELO_QTDE =  v_tab(i).SELO_QTDE,
SISS_CODIGO =  v_tab(i).SISS_CODIGO,
STA_CODIGO =  v_tab(i).STA_CODIGO,
STC_CODIGO =  v_tab(i).STC_CODIGO,
STI_CODIGO =  v_tab(i).STI_CODIGO,
STM_CODIGO =  v_tab(i).STM_CODIGO,
STN_CODIGO =  v_tab(i).STN_CODIGO,
STP_CODIGO =  v_tab(i).STP_CODIGO,
SUBCLASSE_IDF =  v_tab(i).SUBCLASSE_IDF,
TERMINAL =  v_tab(i).TERMINAL,
TIPO_COMPLEMENTO =  v_tab(i).TIPO_COMPLEMENTO,
TIPO_OPER_VEIC =  v_tab(i).TIPO_OPER_VEIC,
TIPO_PROD_MED =  v_tab(i).TIPO_PROD_MED,
TIPO_RECEITA =  v_tab(i).TIPO_RECEITA,
TIPO_STF =  v_tab(i).TIPO_STF,
UF_PAR =  v_tab(i).UF_PAR,
UNI_CODIGO_FISCAL =  v_tab(i).UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO =  v_tab(i).UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET =  v_tab(i).VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF =  v_tab(i).VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS =  v_tab(i).VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE =  v_tab(i).VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL =  v_tab(i).VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO =  v_tab(i).VL_ALIMENTACAO,
VL_ALIQ_COFINS =  v_tab(i).VL_ALIQ_COFINS,
VL_ALIQ_PIS =  v_tab(i).VL_ALIQ_PIS,
VL_ANTECIPACAO =  v_tab(i).VL_ANTECIPACAO,
VL_ANTECIP_ICMS =  v_tab(i).VL_ANTECIP_ICMS,
VL_BASE_COFINS =  v_tab(i).VL_BASE_COFINS,
VL_BASE_COFINS_RET =  v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST =  v_tab(i).VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET =  v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_ICMS =  v_tab(i).VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L =  v_tab(i).VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP =  v_tab(i).VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST =  v_tab(i).VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST =  v_tab(i).VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM =  v_tab(i).VL_BASE_ICMS_PART_REM,
VL_BASE_II =  v_tab(i).VL_BASE_II,
VL_BASE_INSS =  v_tab(i).VL_BASE_INSS,
VL_BASE_INSS_RET =  v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IPI =  v_tab(i).VL_BASE_IPI,
VL_BASE_IRRF =  v_tab(i).VL_BASE_IRRF,
VL_BASE_ISS =  v_tab(i).VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO =  v_tab(i).VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS =  v_tab(i).VL_BASE_PIS,
VL_BASE_PIS_RET =  v_tab(i).VL_BASE_PIS_RET,
VL_BASE_PIS_ST =  v_tab(i).VL_BASE_PIS_ST,
VL_BASE_SENAT =  v_tab(i).VL_BASE_SENAT,
VL_BASE_SEST =  v_tab(i).VL_BASE_SEST,
VL_BASE_STF =  v_tab(i).VL_BASE_STF,
VL_BASE_STF_FRONTEIRA =  v_tab(i).VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO =  v_tab(i).VL_BASE_STF_IDO,
VL_BASE_STF60 =  v_tab(i).VL_BASE_STF60,
VL_BASE_STT =  v_tab(i).VL_BASE_STT,
VL_BASE_SUFRAMA =  v_tab(i).VL_BASE_SUFRAMA,
VL_COFINS =  v_tab(i).VL_COFINS,
VL_COFINS_RET =  v_tab(i).VL_COFINS_RET,
VL_COFINS_ST =  v_tab(i).VL_COFINS_ST,
VL_CONTABIL =  v_tab(i).VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO =  v_tab(i).VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB =  v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB =  v_tab(i).VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO =  v_tab(i).VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB =  v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB =  v_tab(i).VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET =  v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG =  v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS =  v_tab(i).VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG =  v_tab(i).VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG =  v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP =  v_tab(i).VL_DESC_CP,
VL_DIFA =  v_tab(i).VL_DIFA,
VL_FATURADO =  v_tab(i).VL_FATURADO,
VL_FISCAL =  v_tab(i).VL_FISCAL,
VL_GILRAT =  v_tab(i).VL_GILRAT,
VL_ICMS =  v_tab(i).VL_ICMS,
VL_ICMS_DESC_L =  v_tab(i).VL_ICMS_DESC_L,
VL_ICMS_FCP =  v_tab(i).VL_ICMS_FCP,
VL_ICMS_FCPST =  v_tab(i).VL_ICMS_FCPST,
VL_ICMS_PART_DEST =  v_tab(i).VL_ICMS_PART_DEST,
VL_ICMS_PART_REM =  v_tab(i).VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP =  v_tab(i).VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC =  v_tab(i).VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP =  v_tab(i).VL_ICMS_ST_RECUP,
VL_II =  v_tab(i).VL_II,
VL_IMP_FCI =  v_tab(i).VL_IMP_FCI,
VL_IMPOSTO_COFINS =  v_tab(i).VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS =  v_tab(i).VL_IMPOSTO_PIS,
VL_INSS =  v_tab(i).VL_INSS,
VL_INSS_AE15_NAO_RET =  v_tab(i).VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET =  v_tab(i).VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET =  v_tab(i).VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET =  v_tab(i).VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET =  v_tab(i).VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET =  v_tab(i).VL_INSS_AE25_RET,
VL_INSS_NAO_RET =  v_tab(i).VL_INSS_NAO_RET,
VL_INSS_RET =  v_tab(i).VL_INSS_RET,
VL_IOF =  v_tab(i).VL_IOF,
VL_IPI =  v_tab(i).VL_IPI,
VL_IRRF =  v_tab(i).VL_IRRF,
VL_IRRF_NAO_RET =  v_tab(i).VL_IRRF_NAO_RET,
VL_ISENTO_ICMS =  v_tab(i).VL_ISENTO_ICMS,
VL_ISENTO_IPI =  v_tab(i).VL_ISENTO_IPI,
VL_ISS =  v_tab(i).VL_ISS,
VL_ISS_BITRIBUTADO =  v_tab(i).VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO =  v_tab(i).VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO =  v_tab(i).VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES =  v_tab(i).VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP =  v_tab(i).VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP =  v_tab(i).VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE =  v_tab(i).VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT =  v_tab(i).VL_OUTROS_ABAT,
VL_OUTROS_ICMS =  v_tab(i).VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS =  v_tab(i).VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI =  v_tab(i).VL_OUTROS_IPI,
VL_PIS =  v_tab(i).VL_PIS,
VL_PIS_RET =  v_tab(i).VL_PIS_RET,
VL_PIS_ST =  v_tab(i).VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO =  v_tab(i).VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF =  v_tab(i).VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF =  v_tab(i).VL_RATEIO_CT_STF,
VL_RATEIO_FRETE =  v_tab(i).VL_RATEIO_FRETE,
VL_RATEIO_ODA =  v_tab(i).VL_RATEIO_ODA,
VL_RATEIO_SEGURO =  v_tab(i).VL_RATEIO_SEGURO,
VL_RECUPERADO =  v_tab(i).VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA =  v_tab(i).VL_RETIDO_SUBEMPREITADA,
VL_SENAR =  v_tab(i).VL_SENAR,
VL_SENAT =  v_tab(i).VL_SENAT,
VL_SERVICO_AE15 =  v_tab(i).VL_SERVICO_AE15,
VL_SERVICO_AE20 =  v_tab(i).VL_SERVICO_AE20,
VL_SERVICO_AE25 =  v_tab(i).VL_SERVICO_AE25,
VL_SEST =  v_tab(i).VL_SEST,
VL_STF =  v_tab(i).VL_STF,
VL_STF_FRONTEIRA =  v_tab(i).VL_STF_FRONTEIRA,
VL_STF_IDO =  v_tab(i).VL_STF_IDO,
VL_ST_FRONTEIRA =  v_tab(i).VL_ST_FRONTEIRA,
VL_STF60 =  v_tab(i).VL_STF60,
VL_STT =  v_tab(i).VL_STT,
VL_SUFRAMA =  v_tab(i).VL_SUFRAMA,
VL_TAB_MAX =  v_tab(i).VL_TAB_MAX,
VL_TRANSPORTE =  v_tab(i).VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC =  v_tab(i).VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA =  v_tab(i).VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS =  v_tab(i).VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L =  v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI =  v_tab(i).VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF =  v_tab(i).VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT =  v_tab(i).VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA =  v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
XCON_ID =  v_tab(i).XCON_ID,
XINF_ID =  v_tab(i).XINF_ID
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and idf_num = v_tab(i).idf_num;
			  exception when others then
                     r_put_line('Erro ao alterar IDF DUP. - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir IDF - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
  end;
  else
     delete from hon_gttemp_idf
      where id   = v_tab(i).id;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_idf_fmerc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
--PRAGMA AUTONOMOUS_TRANSACTION;
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql            VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_idf%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_idf_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo not in ('NFS','NFS-E')
        ;
     if v_tot > 0
     then
     begin
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      exception when others then 
	         null;
              end;
      begin
     INSERT INTO COR_IDF (ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
CUSTO_ADICAO,
CUSTO_REDUCAO,
C4_SALDO_REF,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET,
HON_PIS_RET,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STF60,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMP_FCI,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_ST_FRONTEIRA,
VL_STF60,
VL_STT,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID)
   VALUES (v_tab(i).ALIQ_ANTECIPACAO,
v_tab(i).ALIQ_ANTECIP_ICMS,
v_tab(i).ALIQ_COFINS,
v_tab(i).ALIQ_COFINS_RET,
v_tab(i).ALIQ_COFINS_ST,
v_tab(i).ALIQ_CSLL_RET,
v_tab(i).ALIQ_DIFA,
v_tab(i).ALIQ_DIFA_ICMS_PART,
v_tab(i).ALIQ_ICMS,
v_tab(i).ALIQ_ICMS_DESC_L,
v_tab(i).ALIQ_ICMS_FCP,
v_tab(i).ALIQ_ICMS_FCPST,
v_tab(i).ALIQ_ICMS_PART_DEST,
v_tab(i).ALIQ_ICMS_PART_REM,
v_tab(i).ALIQ_ICMS_STF60,
v_tab(i).ALIQ_II,
v_tab(i).ALIQ_INSS,
v_tab(i).ALIQ_INSS_AE15_RET,
v_tab(i).ALIQ_INSS_AE20_RET,
v_tab(i).ALIQ_INSS_AE25_RET,
v_tab(i).ALIQ_INSS_RET,
v_tab(i).ALIQ_IPI,
v_tab(i).ALIQ_ISS,
v_tab(i).ALIQ_ISS_BITRIBUTADO,
v_tab(i).ALIQ_PIS,
v_tab(i).ALIQ_PIS_RET,
v_tab(i).ALIQ_PIS_ST,
v_tab(i).ALIQ_SENAT,
v_tab(i).ALIQ_SEST,
v_tab(i).ALIQ_STF,
v_tab(i).ALIQ_STT,
v_tab(i).AM_CODIGO,
v_tab(i).CCA_CODIGO,
v_tab(i).CEST_CODIGO,
v_tab(i).CFOP_CODIGO,
v_tab(i).CHASSI_VEIC,
v_tab(i).CLI_CODIGO,
v_tab(i).CNPJ_FABRICANTE,
v_tab(i).CNPJ_PAR,
v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
v_tab(i).CNPJ_SUBEMPREITEIRO,
v_tab(i).COD_AREA,
v_tab(i).COD_BC_CREDITO,
v_tab(i).COD_BENEFICIO_FISCAL,
v_tab(i).COD_CCUS,
v_tab(i).COD_CONJ_KIT,
v_tab(i).COD_FISC_SERV_MUN,
v_tab(i).COD_GTIN,
v_tab(i).COD_IATA_FIM,
v_tab(i).COD_IATA_INI,
v_tab(i).COD_IBGE,
v_site,
v_tab(i).CODIGO_ISS_MUNICIPIO,
v_tab(i).CODIGO_RETENCAO,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_INSS,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).COD_NVE,
v_tab(i).CONTA_CONTABIL,
v_tab(i).CPF_PAR,
v_tab(i).CSOSN_CODIGO,
v_tab(i).CTRL_DESCARGA,
v_tab(i).CTRL_ENTRADA_ST,
v_tab(i).CTRL_OBS_CALC_ICMS,
v_tab(i).CTRL_OBS_CALC_INSS,
v_tab(i).CTRL_OBS_CALC_IPI,
v_tab(i).CTRL_OBS_CALC_IRRF,
v_tab(i).CTRL_OBS_CALC_ISS,
v_tab(i).CTRL_OBS_CALC_STF,
v_tab(i).CTRL_OBS_CALC_STT,
v_tab(i).CTRL_OBS_ENQ,
v_tab(i).CTRL_OBS_SETUP_ICMS,
v_tab(i).CTRL_OBS_SETUP_IPI,
v_tab(i).CTRL_OBS_TOT_PESO,
v_tab(i).CTRL_OBS_TOT_PRECO,
v_tab(i).CTRL_OBS_VL_FISCAL,
v_tab(i).CTRL_SAIDA_ST,
v_tab(i).CTRL_TOLERANCIA_PED,
v_tab(i).CUSTO_ADICAO,
v_tab(i).CUSTO_REDUCAO,
v_tab(i).C4_SALDO_REF,
v_tab(i).DESCRICAO_ARMA,
v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
v_tab(i).DESONERACAO_CODIGO,
v_dof,
v_sequence,
v_tab(i).DT_DI,
v_tab(i).DT_FAB_MED,
v_tab(i).DT_FIN_SERV,
v_tab(i).DT_INI_SERV,
v_tab(i).DT_REF_CALC_IMP_IDF,
v_tab(i).DT_VAL,
v_tab(i).EMERC_CODIGO,
v_tab(i).ENQ_IPI_CODIGO,
v_tab(i).ENTSAI_UNI_CODIGO,
v_tab(i).ESTOQUE_UNI_CODIGO,
v_tab(i).FAT_BASE_CALCULO_STF,
v_tab(i).FAT_CONV_UNI_ESTOQUE,
v_tab(i).FAT_CONV_UNI_FISCAL,
v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
v_tab(i).FCI_NUMERO,
v_tab(i).FIN_CODIGO,
v_tab(i).FLAG_SUFRAMA,
v_tab(i).HON_COFINS_RET,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_CSLL_RET,
v_tab(i).HON_PIS_RET,
v_tab(i).IDF_NUM,
v_tab(i).IDF_NUM_PAI,
v_tab(i).IDF_TEXTO_COMPLEMENTAR,
v_tab(i).IE_PAR,
v_tab(i).IM_SUBCONTRATACAO,
v_tab(i).IND_ANTECIP_ICMS,
v_tab(i).IND_ARM,
v_tab(i).IND_BLOQUEADO,
v_tab(i).IND_COFINS_ST,
v_tab(i).IND_COMPROVA_OPERACAO,
v_tab(i).IND_CONTABILIZACAO,
v_tab(i).IND_ESCALA_RELEVANTE,
v_tab(i).IND_ESCRITURACAO,
v_tab(i).IND_EXIGIBILIDADE_ISS,
v_tab(i).IND_FCP_ST,
v_tab(i).IND_INCENTIVO_FISCAL_ISS,
v_tab(i).IND_INCIDENCIA_COFINS,
v_tab(i).IND_INCIDENCIA_COFINS_RET,
v_tab(i).IND_INCIDENCIA_COFINS_ST,
v_tab(i).IND_INCIDENCIA_CSLL_RET,
v_tab(i).IND_INCIDENCIA_ICMS,
v_tab(i).IND_INCIDENCIA_INSS,
v_tab(i).IND_INCIDENCIA_INSS_RET,
v_tab(i).IND_INCIDENCIA_IPI,
v_tab(i).IND_INCIDENCIA_IRRF,
v_tab(i).IND_INCIDENCIA_ISS,
v_tab(i).IND_INCIDENCIA_PIS,
v_tab(i).IND_INCIDENCIA_PIS_RET,
v_tab(i).IND_INCIDENCIA_PIS_ST,
v_tab(i).IND_INCIDENCIA_SENAT,
v_tab(i).IND_INCIDENCIA_SEST,
v_tab(i).IND_INCIDENCIA_STF,
v_tab(i).IND_INCIDENCIA_STT,
v_tab(i).IND_IPI_BASE_ICMS,
v_tab(i).IND_ISS_RETIDO_FONTE,
v_tab(i).IND_LAN_FISCAL_ICMS,
v_tab(i).IND_LAN_FISCAL_IPI,
v_tab(i).IND_LAN_FISCAL_STF,
v_tab(i).IND_LAN_FISCAL_STT,
v_tab(i).IND_LAN_IMP,
v_tab(i).IND_MED,
v_tab(i).IND_MOVIMENTACAO_CIAP,
v_tab(i).IND_MOVIMENTACAO_CPC,
v_tab(i).IND_MOVIMENTA_ESTOQUE,
v_tab(i).IND_MP_DO_BEM,
v_tab(i).IND_NAT_FRT_PISCOFINS,
v_tab(i).IND_OUTROS_ICMS,
v_tab(i).IND_OUTROS_IPI,
v_tab(i).IND_PAUTA_STF_MVA_MP,
v_tab(i).IND_PIS_ST,
v_tab(i).IND_SERV,
v_tab(i).IND_VERIFICA_PEDIDO,
v_tab(i).IND_VL_FISC_VL_CONT,
v_tab(i).IND_VL_FISC_VL_FAT,
v_tab(i).IND_VL_ICMS_NO_PRECO,
v_tab(i).IND_VL_ICMS_VL_CONT,
v_tab(i).IND_VL_ICMS_VL_FAT,
v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
v_tab(i).IND_ZFM_ALC,
v_tab(i).LOTE_MED,
v_tab(i).MERC_CODIGO,
v_tab(i).MOD_BASE_ICMS_CODIGO,
v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
v_tab(i).MUN_CODIGO,
v_tab(i).NAT_REC_PISCOFINS,
v_tab(i).NAT_REC_PISCOFINS_DESCR,
v_tab(i).NBM_CODIGO,
v_tab(i).NBS_CODIGO,
v_tab(i).NOP_CODIGO,
v_tab(i).NUM_ARM,
v_tab(i).NUM_CANO,
v_tab(i).NUM_DI,
v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
v_tab(i).NUM_TANQUE,
v_tab(i).OM_CODIGO,
v_tab(i).ORD_IMPRESSAO,
v_tab(i).PARTICIPANTE_PFJ_CODIGO,
v_tab(i).PAUTA_FLUT_ICMS,
v_tab(i).PEDIDO_NUMERO,
v_tab(i).PEDIDO_NUMERO_ITEM,
v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
v_tab(i).PERC_ICMS_PART_DEST,
v_tab(i).PERC_ICMS_PART_REM,
v_tab(i).PERC_IRRF,
v_tab(i).PERC_ISENTO_ICMS,
v_tab(i).PERC_ISENTO_IPI,
v_tab(i).PERC_OUTROS_ABAT,
v_tab(i).PERC_OUTROS_ICMS,
v_tab(i).PERC_OUTROS_IPI,
v_tab(i).PERC_PART_CI,
v_tab(i).PERC_TRIBUTAVEL_COFINS,
v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
v_tab(i).PERC_TRIBUTAVEL_ICMS,
v_tab(i).PERC_TRIBUTAVEL_INSS,
v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
v_tab(i).PERC_TRIBUTAVEL_IPI,
v_tab(i).PERC_TRIBUTAVEL_IRRF,
v_tab(i).PERC_TRIBUTAVEL_ISS,
v_tab(i).PERC_TRIBUTAVEL_PIS,
v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
v_tab(i).PERC_TRIBUTAVEL_SENAT,
v_tab(i).PERC_TRIBUTAVEL_SEST,
v_tab(i).PERC_TRIBUTAVEL_STF,
v_tab(i).PERC_TRIBUTAVEL_STT,
v_tab(i).PER_FISCAL,
v_tab(i).PESO_BRUTO_KG,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PFJ_CODIGO_FORNECEDOR,
v_tab(i).PFJ_CODIGO_TERCEIRO,
v_tab(i).PRECO_NET_UNIT,
v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
v_tab(i).PRECO_TOTAL,
v_tab(i).PRECO_UNITARIO,
v_tab(i).PRES_CODIGO,
v_tab(i).QTD,
v_tab(i).QTD_BASE_COFINS,
v_tab(i).QTD_BASE_PIS,
v_tab(i).QTD_EMB,
v_tab(i).QTD_KIT,
v_tab(i).REVISAO,
v_tab(i).SELO_CODIGO,
v_tab(i).SELO_QTDE,
v_tab(i).SISS_CODIGO,
v_tab(i).STA_CODIGO,
v_tab(i).STC_CODIGO,
v_tab(i).STI_CODIGO,
v_tab(i).STM_CODIGO,
v_tab(i).STN_CODIGO,
v_tab(i).STP_CODIGO,
v_tab(i).SUBCLASSE_IDF,
v_tab(i).TERMINAL,
v_tab(i).TIPO_COMPLEMENTO,
v_tab(i).TIPO_OPER_VEIC,
v_tab(i).TIPO_PROD_MED,
v_tab(i).TIPO_RECEITA,
v_tab(i).TIPO_STF,
v_tab(i).UF_PAR,
v_tab(i).UNI_CODIGO_FISCAL,
v_tab(i).UNI_FISCAL_CODIGO,
v_tab(i).VL_ABAT_LEGAL_INSS_RET,
v_tab(i).VL_ABAT_LEGAL_IRRF,
v_tab(i).VL_ABAT_LEGAL_ISS,
v_tab(i).VL_ADICIONAL_RET_AE,
v_tab(i).VL_AJUSTE_PRECO_TOTAL,
v_tab(i).VL_ALIMENTACAO,
v_tab(i).VL_ALIQ_COFINS,
v_tab(i).VL_ALIQ_PIS,
v_tab(i).VL_ANTECIPACAO,
v_tab(i).VL_ANTECIP_ICMS,
v_tab(i).VL_BASE_COFINS,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_COFINS_ST,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_ICMS,
v_tab(i).VL_BASE_ICMS_DESC_L,
v_tab(i).VL_BASE_ICMS_FCP,
v_tab(i).VL_BASE_ICMS_FCPST,
v_tab(i).VL_BASE_ICMS_PART_DEST,
v_tab(i).VL_BASE_ICMS_PART_REM,
v_tab(i).VL_BASE_II,
v_tab(i).VL_BASE_INSS,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IPI,
v_tab(i).VL_BASE_IRRF,
v_tab(i).VL_BASE_ISS,
v_tab(i).VL_BASE_ISS_BITRIBUTADO,
v_tab(i).VL_BASE_PIS,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_PIS_ST,
v_tab(i).VL_BASE_SENAT,
v_tab(i).VL_BASE_SEST,
v_tab(i).VL_BASE_STF,
v_tab(i).VL_BASE_STF_FRONTEIRA,
v_tab(i).VL_BASE_STF_IDO,
v_tab(i).VL_BASE_STF60,
v_tab(i).VL_BASE_STT,
v_tab(i).VL_BASE_SUFRAMA,
v_tab(i).VL_COFINS,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_COFINS_ST,
v_tab(i).VL_CONTABIL,
v_tab(i).VL_CRED_COFINS_REC_EXPO,
v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
v_tab(i).VL_CRED_COFINS_REC_TRIB,
v_tab(i).VL_CRED_PIS_REC_EXPO,
v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
v_tab(i).VL_CRED_PIS_REC_TRIB,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_INSS,
v_tab(i).VL_DEDUCAO_IRRF_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESC_CP,
v_tab(i).VL_DIFA,
v_tab(i).VL_FATURADO,
v_tab(i).VL_FISCAL,
v_tab(i).VL_GILRAT,
v_tab(i).VL_ICMS,
v_tab(i).VL_ICMS_DESC_L,
v_tab(i).VL_ICMS_FCP,
v_tab(i).VL_ICMS_FCPST,
v_tab(i).VL_ICMS_PART_DEST,
v_tab(i).VL_ICMS_PART_REM,
v_tab(i).VL_ICMS_PROPRIO_RECUP,
v_tab(i).VL_ICMS_SIMPLES_NAC,
v_tab(i).VL_ICMS_ST_RECUP,
v_tab(i).VL_II,
v_tab(i).VL_IMP_FCI,
v_tab(i).VL_IMPOSTO_COFINS,
v_tab(i).VL_IMPOSTO_PIS,
v_tab(i).VL_INSS,
v_tab(i).VL_INSS_AE15_NAO_RET,
v_tab(i).VL_INSS_AE15_RET,
v_tab(i).VL_INSS_AE20_NAO_RET,
v_tab(i).VL_INSS_AE20_RET,
v_tab(i).VL_INSS_AE25_NAO_RET,
v_tab(i).VL_INSS_AE25_RET,
v_tab(i).VL_INSS_NAO_RET,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IOF,
v_tab(i).VL_IPI,
v_tab(i).VL_IRRF,
v_tab(i).VL_IRRF_NAO_RET,
v_tab(i).VL_ISENTO_ICMS,
v_tab(i).VL_ISENTO_IPI,
v_tab(i).VL_ISS,
v_tab(i).VL_ISS_BITRIBUTADO,
v_tab(i).VL_ISS_DESC_CONDICIONADO,
v_tab(i).VL_ISS_DESC_INCONDICIONADO,
v_tab(i).VL_ISS_OUTRAS_RETENCOES,
v_tab(i).VL_MATERIAS_EQUIP,
v_tab(i).VL_NAO_RETIDO_CP,
v_tab(i).VL_NAO_RETIDO_CP_AE,
v_tab(i).VL_OUTROS_ABAT,
v_tab(i).VL_OUTROS_ICMS,
v_tab(i).VL_OUTROS_IMPOSTOS,
v_tab(i).VL_OUTROS_IPI,
v_tab(i).VL_PIS,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_PIS_ST,
v_tab(i).VL_RATEIO_AJUSTE_PRECO,
v_tab(i).VL_RATEIO_BASE_CT_STF,
v_tab(i).VL_RATEIO_CT_STF,
v_tab(i).VL_RATEIO_FRETE,
v_tab(i).VL_RATEIO_ODA,
v_tab(i).VL_RATEIO_SEGURO,
v_tab(i).VL_RECUPERADO,
v_tab(i).VL_RETIDO_SUBEMPREITADA,
v_tab(i).VL_SENAR,
v_tab(i).VL_SENAT,
v_tab(i).VL_SERVICO_AE15,
v_tab(i).VL_SERVICO_AE20,
v_tab(i).VL_SERVICO_AE25,
v_tab(i).VL_SEST,
v_tab(i).VL_STF,
v_tab(i).VL_STF_FRONTEIRA,
v_tab(i).VL_STF_IDO,
v_tab(i).VL_ST_FRONTEIRA,
v_tab(i).VL_STF60,
v_tab(i).VL_STT,
v_tab(i).VL_SUFRAMA,
v_tab(i).VL_TAB_MAX,
v_tab(i).VL_TRANSPORTE,
v_tab(i).VL_TRIBUTAVEL_ANTEC,
v_tab(i).VL_TRIBUTAVEL_DIFA,
v_tab(i).VL_TRIBUTAVEL_ICMS,
v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
v_tab(i).VL_TRIBUTAVEL_IPI,
v_tab(i).VL_TRIBUTAVEL_STF,
v_tab(i).VL_TRIBUTAVEL_STT,
v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
v_tab(i).XCON_ID,
v_tab(i).XINF_ID);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_idf
				    set
ALIQ_ANTECIPACAO =  v_tab(i).ALIQ_ANTECIPACAO,
ALIQ_ANTECIP_ICMS =  v_tab(i).ALIQ_ANTECIP_ICMS,
ALIQ_COFINS =  v_tab(i).ALIQ_COFINS,
ALIQ_COFINS_RET =  v_tab(i).ALIQ_COFINS_RET,
ALIQ_COFINS_ST =  v_tab(i).ALIQ_COFINS_ST,
ALIQ_CSLL_RET =  v_tab(i).ALIQ_CSLL_RET,
ALIQ_DIFA =  v_tab(i).ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART =  v_tab(i).ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS =  v_tab(i).ALIQ_ICMS,
ALIQ_ICMS_DESC_L =  v_tab(i).ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP =  v_tab(i).ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST =  v_tab(i).ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST =  v_tab(i).ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM =  v_tab(i).ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60 =  v_tab(i).ALIQ_ICMS_STF60,
ALIQ_II =  v_tab(i).ALIQ_II,
ALIQ_INSS =  v_tab(i).ALIQ_INSS,
ALIQ_INSS_AE15_RET =  v_tab(i).ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET =  v_tab(i).ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET =  v_tab(i).ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET =  v_tab(i).ALIQ_INSS_RET,
ALIQ_IPI =  v_tab(i).ALIQ_IPI,
ALIQ_ISS =  v_tab(i).ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO =  v_tab(i).ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS =  v_tab(i).ALIQ_PIS,
ALIQ_PIS_RET =  v_tab(i).ALIQ_PIS_RET,
ALIQ_PIS_ST =  v_tab(i).ALIQ_PIS_ST,
ALIQ_SENAT =  v_tab(i).ALIQ_SENAT,
ALIQ_SEST =  v_tab(i).ALIQ_SEST,
ALIQ_STF =  v_tab(i).ALIQ_STF,
ALIQ_STT =  v_tab(i).ALIQ_STT,
AM_CODIGO =  v_tab(i).AM_CODIGO,
CCA_CODIGO =  v_tab(i).CCA_CODIGO,
CEST_CODIGO =  v_tab(i).CEST_CODIGO,
CFOP_CODIGO =  v_tab(i).CFOP_CODIGO,
CHASSI_VEIC =  v_tab(i).CHASSI_VEIC,
CLI_CODIGO =  v_tab(i).CLI_CODIGO,
CNPJ_FABRICANTE =  v_tab(i).CNPJ_FABRICANTE,
CNPJ_PAR =  v_tab(i).CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT =  v_tab(i).CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO =  v_tab(i).CNPJ_SUBEMPREITEIRO,
COD_AREA =  v_tab(i).COD_AREA,
COD_BC_CREDITO =  v_tab(i).COD_BC_CREDITO,
COD_BENEFICIO_FISCAL =  v_tab(i).COD_BENEFICIO_FISCAL,
COD_CCUS =  v_tab(i).COD_CCUS,
COD_CONJ_KIT =  v_tab(i).COD_CONJ_KIT,
COD_FISC_SERV_MUN =  v_tab(i).COD_FISC_SERV_MUN,
COD_GTIN =  v_tab(i).COD_GTIN,
COD_IATA_FIM =  v_tab(i).COD_IATA_FIM,
COD_IATA_INI =  v_tab(i).COD_IATA_INI,
COD_IBGE =  v_tab(i).COD_IBGE,
CODIGO_ISS_MUNICIPIO =  v_tab(i).CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO =  v_tab(i).CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS =  v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL =  v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS =  v_tab(i).CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC =  v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS =  v_tab(i).CODIGO_RETENCAO_PIS,
COD_NVE =  v_tab(i).COD_NVE,
CONTA_CONTABIL =  v_tab(i).CONTA_CONTABIL,
CPF_PAR =  v_tab(i).CPF_PAR,
CSOSN_CODIGO =  v_tab(i).CSOSN_CODIGO,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
CTRL_ENTRADA_ST =  v_tab(i).CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS =  v_tab(i).CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS =  v_tab(i).CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI =  v_tab(i).CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF =  v_tab(i).CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS =  v_tab(i).CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF =  v_tab(i).CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT =  v_tab(i).CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ =  v_tab(i).CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS =  v_tab(i).CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI =  v_tab(i).CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO =  v_tab(i).CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO =  v_tab(i).CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL =  v_tab(i).CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST =  v_tab(i).CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED =  v_tab(i).CTRL_TOLERANCIA_PED,
CUSTO_ADICAO =  v_tab(i).CUSTO_ADICAO,
CUSTO_REDUCAO =  v_tab(i).CUSTO_REDUCAO,
C4_SALDO_REF =  v_tab(i).C4_SALDO_REF,
DESCRICAO_ARMA =  v_tab(i).DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO =  v_tab(i).DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO =  v_tab(i).DESONERACAO_CODIGO,
DT_DI =  v_tab(i).DT_DI,
DT_FAB_MED =  v_tab(i).DT_FAB_MED,
DT_FIN_SERV =  v_tab(i).DT_FIN_SERV,
DT_INI_SERV =  v_tab(i).DT_INI_SERV,
DT_REF_CALC_IMP_IDF =  v_tab(i).DT_REF_CALC_IMP_IDF,
DT_VAL =  v_tab(i).DT_VAL,
EMERC_CODIGO =  v_tab(i).EMERC_CODIGO,
ENQ_IPI_CODIGO =  v_tab(i).ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO =  v_tab(i).ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO =  v_tab(i).ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF =  v_tab(i).FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE =  v_tab(i).FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL =  v_tab(i).FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO =  v_tab(i).FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO =  v_tab(i).FCI_NUMERO,
FIN_CODIGO =  v_tab(i).FIN_CODIGO,
FLAG_SUFRAMA =  v_tab(i).FLAG_SUFRAMA,
HON_COFINS_RET =  v_tab(i).HON_COFINS_RET,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_CSLL_RET =  v_tab(i).HON_CSLL_RET,
HON_PIS_RET =  v_tab(i).HON_PIS_RET,
IDF_NUM =  v_tab(i).IDF_NUM,
IDF_NUM_PAI =  v_tab(i).IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR =  v_tab(i).IDF_TEXTO_COMPLEMENTAR,
IE_PAR =  v_tab(i).IE_PAR,
IM_SUBCONTRATACAO =  v_tab(i).IM_SUBCONTRATACAO,
IND_ANTECIP_ICMS =  v_tab(i).IND_ANTECIP_ICMS,
IND_ARM =  v_tab(i).IND_ARM,
IND_BLOQUEADO =  v_tab(i).IND_BLOQUEADO,
IND_COFINS_ST =  v_tab(i).IND_COFINS_ST,
IND_COMPROVA_OPERACAO =  v_tab(i).IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO =  v_tab(i).IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE =  v_tab(i).IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO =  v_tab(i).IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS =  v_tab(i).IND_EXIGIBILIDADE_ISS,
IND_FCP_ST =  v_tab(i).IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS =  v_tab(i).IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS =  v_tab(i).IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET =  v_tab(i).IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST =  v_tab(i).IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET =  v_tab(i).IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS =  v_tab(i).IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS =  v_tab(i).IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET =  v_tab(i).IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI =  v_tab(i).IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF =  v_tab(i).IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS =  v_tab(i).IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS =  v_tab(i).IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET =  v_tab(i).IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST =  v_tab(i).IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT =  v_tab(i).IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST =  v_tab(i).IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF =  v_tab(i).IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT =  v_tab(i).IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS =  v_tab(i).IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE =  v_tab(i).IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS =  v_tab(i).IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI =  v_tab(i).IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF =  v_tab(i).IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT =  v_tab(i).IND_LAN_FISCAL_STT,
IND_LAN_IMP =  v_tab(i).IND_LAN_IMP,
IND_MED =  v_tab(i).IND_MED,
IND_MOVIMENTACAO_CIAP =  v_tab(i).IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC =  v_tab(i).IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE =  v_tab(i).IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM =  v_tab(i).IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS =  v_tab(i).IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS =  v_tab(i).IND_OUTROS_ICMS,
IND_OUTROS_IPI =  v_tab(i).IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP =  v_tab(i).IND_PAUTA_STF_MVA_MP,
IND_PIS_ST =  v_tab(i).IND_PIS_ST,
IND_SERV =  v_tab(i).IND_SERV,
IND_VERIFICA_PEDIDO =  v_tab(i).IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT =  v_tab(i).IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT =  v_tab(i).IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO =  v_tab(i).IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT =  v_tab(i).IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT =  v_tab(i).IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO =  v_tab(i).IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO =  v_tab(i).IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC =  v_tab(i).IND_ZFM_ALC,
LOTE_MED =  v_tab(i).LOTE_MED,
MERC_CODIGO =  v_tab(i).MERC_CODIGO,
MOD_BASE_ICMS_CODIGO =  v_tab(i).MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO =  v_tab(i).MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO =  v_tab(i).MUN_CODIGO,
NAT_REC_PISCOFINS =  v_tab(i).NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR =  v_tab(i).NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO =  v_tab(i).NBM_CODIGO,
NBS_CODIGO =  v_tab(i).NBS_CODIGO,
NOP_CODIGO =  v_tab(i).NOP_CODIGO,
NUM_ARM =  v_tab(i).NUM_ARM,
NUM_CANO =  v_tab(i).NUM_CANO,
NUM_DI =  v_tab(i).NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE =  v_tab(i).NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE =  v_tab(i).NUM_TANQUE,
OM_CODIGO =  v_tab(i).OM_CODIGO,
ORD_IMPRESSAO =  v_tab(i).ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO =  v_tab(i).PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS =  v_tab(i).PAUTA_FLUT_ICMS,
PEDIDO_NUMERO =  v_tab(i).PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM =  v_tab(i).PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL =  v_tab(i).PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST =  v_tab(i).PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM =  v_tab(i).PERC_ICMS_PART_REM,
PERC_IRRF =  v_tab(i).PERC_IRRF,
PERC_ISENTO_ICMS =  v_tab(i).PERC_ISENTO_ICMS,
PERC_ISENTO_IPI =  v_tab(i).PERC_ISENTO_IPI,
PERC_OUTROS_ABAT =  v_tab(i).PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS =  v_tab(i).PERC_OUTROS_ICMS,
PERC_OUTROS_IPI =  v_tab(i).PERC_OUTROS_IPI,
PERC_PART_CI =  v_tab(i).PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS =  v_tab(i).PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST =  v_tab(i).PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS =  v_tab(i).PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS =  v_tab(i).PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET =  v_tab(i).PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI =  v_tab(i).PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF =  v_tab(i).PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS =  v_tab(i).PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS =  v_tab(i).PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST =  v_tab(i).PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT =  v_tab(i).PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST =  v_tab(i).PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF =  v_tab(i).PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT =  v_tab(i).PERC_TRIBUTAVEL_STT,
PER_FISCAL =  v_tab(i).PER_FISCAL,
PESO_BRUTO_KG =  v_tab(i).PESO_BRUTO_KG,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR =  v_tab(i).PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO =  v_tab(i).PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT =  v_tab(i).PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS =  v_tab(i).PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL =  v_tab(i).PRECO_TOTAL,
PRECO_UNITARIO =  v_tab(i).PRECO_UNITARIO,
PRES_CODIGO =  v_tab(i).PRES_CODIGO,
QTD =  v_tab(i).QTD,
QTD_BASE_COFINS =  v_tab(i).QTD_BASE_COFINS,
QTD_BASE_PIS =  v_tab(i).QTD_BASE_PIS,
QTD_EMB =  v_tab(i).QTD_EMB,
QTD_KIT =  v_tab(i).QTD_KIT,
REVISAO =  v_tab(i).REVISAO,
SELO_CODIGO =  v_tab(i).SELO_CODIGO,
SELO_QTDE =  v_tab(i).SELO_QTDE,
SISS_CODIGO =  v_tab(i).SISS_CODIGO,
STA_CODIGO =  v_tab(i).STA_CODIGO,
STC_CODIGO =  v_tab(i).STC_CODIGO,
STI_CODIGO =  v_tab(i).STI_CODIGO,
STM_CODIGO =  v_tab(i).STM_CODIGO,
STN_CODIGO =  v_tab(i).STN_CODIGO,
STP_CODIGO =  v_tab(i).STP_CODIGO,
SUBCLASSE_IDF =  v_tab(i).SUBCLASSE_IDF,
TERMINAL =  v_tab(i).TERMINAL,
TIPO_COMPLEMENTO =  v_tab(i).TIPO_COMPLEMENTO,
TIPO_OPER_VEIC =  v_tab(i).TIPO_OPER_VEIC,
TIPO_PROD_MED =  v_tab(i).TIPO_PROD_MED,
TIPO_RECEITA =  v_tab(i).TIPO_RECEITA,
TIPO_STF =  v_tab(i).TIPO_STF,
UF_PAR =  v_tab(i).UF_PAR,
UNI_CODIGO_FISCAL =  v_tab(i).UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO =  v_tab(i).UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET =  v_tab(i).VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF =  v_tab(i).VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS =  v_tab(i).VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE =  v_tab(i).VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL =  v_tab(i).VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO =  v_tab(i).VL_ALIMENTACAO,
VL_ALIQ_COFINS =  v_tab(i).VL_ALIQ_COFINS,
VL_ALIQ_PIS =  v_tab(i).VL_ALIQ_PIS,
VL_ANTECIPACAO =  v_tab(i).VL_ANTECIPACAO,
VL_ANTECIP_ICMS =  v_tab(i).VL_ANTECIP_ICMS,
VL_BASE_COFINS =  v_tab(i).VL_BASE_COFINS,
VL_BASE_COFINS_RET =  v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST =  v_tab(i).VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET =  v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_ICMS =  v_tab(i).VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L =  v_tab(i).VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP =  v_tab(i).VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST =  v_tab(i).VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST =  v_tab(i).VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM =  v_tab(i).VL_BASE_ICMS_PART_REM,
VL_BASE_II =  v_tab(i).VL_BASE_II,
VL_BASE_INSS =  v_tab(i).VL_BASE_INSS,
VL_BASE_INSS_RET =  v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IPI =  v_tab(i).VL_BASE_IPI,
VL_BASE_IRRF =  v_tab(i).VL_BASE_IRRF,
VL_BASE_ISS =  v_tab(i).VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO =  v_tab(i).VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS =  v_tab(i).VL_BASE_PIS,
VL_BASE_PIS_RET =  v_tab(i).VL_BASE_PIS_RET,
VL_BASE_PIS_ST =  v_tab(i).VL_BASE_PIS_ST,
VL_BASE_SENAT =  v_tab(i).VL_BASE_SENAT,
VL_BASE_SEST =  v_tab(i).VL_BASE_SEST,
VL_BASE_STF =  v_tab(i).VL_BASE_STF,
VL_BASE_STF_FRONTEIRA =  v_tab(i).VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO =  v_tab(i).VL_BASE_STF_IDO,
VL_BASE_STF60 =  v_tab(i).VL_BASE_STF60,
VL_BASE_STT =  v_tab(i).VL_BASE_STT,
VL_BASE_SUFRAMA =  v_tab(i).VL_BASE_SUFRAMA,
VL_COFINS =  v_tab(i).VL_COFINS,
VL_COFINS_RET =  v_tab(i).VL_COFINS_RET,
VL_COFINS_ST =  v_tab(i).VL_COFINS_ST,
VL_CONTABIL =  v_tab(i).VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO =  v_tab(i).VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB =  v_tab(i).VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB =  v_tab(i).VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO =  v_tab(i).VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB =  v_tab(i).VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB =  v_tab(i).VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET =  v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG =  v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS =  v_tab(i).VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG =  v_tab(i).VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG =  v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP =  v_tab(i).VL_DESC_CP,
VL_DIFA =  v_tab(i).VL_DIFA,
VL_FATURADO =  v_tab(i).VL_FATURADO,
VL_FISCAL =  v_tab(i).VL_FISCAL,
VL_GILRAT =  v_tab(i).VL_GILRAT,
VL_ICMS =  v_tab(i).VL_ICMS,
VL_ICMS_DESC_L =  v_tab(i).VL_ICMS_DESC_L,
VL_ICMS_FCP =  v_tab(i).VL_ICMS_FCP,
VL_ICMS_FCPST =  v_tab(i).VL_ICMS_FCPST,
VL_ICMS_PART_DEST =  v_tab(i).VL_ICMS_PART_DEST,
VL_ICMS_PART_REM =  v_tab(i).VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP =  v_tab(i).VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC =  v_tab(i).VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP =  v_tab(i).VL_ICMS_ST_RECUP,
VL_II =  v_tab(i).VL_II,
VL_IMP_FCI =  v_tab(i).VL_IMP_FCI,
VL_IMPOSTO_COFINS =  v_tab(i).VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS =  v_tab(i).VL_IMPOSTO_PIS,
VL_INSS =  v_tab(i).VL_INSS,
VL_INSS_AE15_NAO_RET =  v_tab(i).VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET =  v_tab(i).VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET =  v_tab(i).VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET =  v_tab(i).VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET =  v_tab(i).VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET =  v_tab(i).VL_INSS_AE25_RET,
VL_INSS_NAO_RET =  v_tab(i).VL_INSS_NAO_RET,
VL_INSS_RET =  v_tab(i).VL_INSS_RET,
VL_IOF =  v_tab(i).VL_IOF,
VL_IPI =  v_tab(i).VL_IPI,
VL_IRRF =  v_tab(i).VL_IRRF,
VL_IRRF_NAO_RET =  v_tab(i).VL_IRRF_NAO_RET,
VL_ISENTO_ICMS =  v_tab(i).VL_ISENTO_ICMS,
VL_ISENTO_IPI =  v_tab(i).VL_ISENTO_IPI,
VL_ISS =  v_tab(i).VL_ISS,
VL_ISS_BITRIBUTADO =  v_tab(i).VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO =  v_tab(i).VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO =  v_tab(i).VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES =  v_tab(i).VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP =  v_tab(i).VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP =  v_tab(i).VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE =  v_tab(i).VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT =  v_tab(i).VL_OUTROS_ABAT,
VL_OUTROS_ICMS =  v_tab(i).VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS =  v_tab(i).VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI =  v_tab(i).VL_OUTROS_IPI,
VL_PIS =  v_tab(i).VL_PIS,
VL_PIS_RET =  v_tab(i).VL_PIS_RET,
VL_PIS_ST =  v_tab(i).VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO =  v_tab(i).VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF =  v_tab(i).VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF =  v_tab(i).VL_RATEIO_CT_STF,
VL_RATEIO_FRETE =  v_tab(i).VL_RATEIO_FRETE,
VL_RATEIO_ODA =  v_tab(i).VL_RATEIO_ODA,
VL_RATEIO_SEGURO =  v_tab(i).VL_RATEIO_SEGURO,
VL_RECUPERADO =  v_tab(i).VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA =  v_tab(i).VL_RETIDO_SUBEMPREITADA,
VL_SENAR =  v_tab(i).VL_SENAR,
VL_SENAT =  v_tab(i).VL_SENAT,
VL_SERVICO_AE15 =  v_tab(i).VL_SERVICO_AE15,
VL_SERVICO_AE20 =  v_tab(i).VL_SERVICO_AE20,
VL_SERVICO_AE25 =  v_tab(i).VL_SERVICO_AE25,
VL_SEST =  v_tab(i).VL_SEST,
VL_STF =  v_tab(i).VL_STF,
VL_STF_FRONTEIRA =  v_tab(i).VL_STF_FRONTEIRA,
VL_STF_IDO =  v_tab(i).VL_STF_IDO,
VL_ST_FRONTEIRA =  v_tab(i).VL_ST_FRONTEIRA,
VL_STF60 =  v_tab(i).VL_STF60,
VL_STT =  v_tab(i).VL_STT,
VL_SUFRAMA =  v_tab(i).VL_SUFRAMA,
VL_TAB_MAX =  v_tab(i).VL_TAB_MAX,
VL_TRANSPORTE =  v_tab(i).VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC =  v_tab(i).VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA =  v_tab(i).VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS =  v_tab(i).VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L =  v_tab(i).VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI =  v_tab(i).VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF =  v_tab(i).VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT =  v_tab(i).VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA =  v_tab(i).VL_TRIBUTAVEL_SUFRAMA,
XCON_ID =  v_tab(i).XCON_ID,
XINF_ID =  v_tab(i).XINF_ID
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and idf_num = v_tab(i).idf_num;
			  exception when others then
                     r_put_line('Erro ao alterar IDF DUP. - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
			  end; 
            when others
                 then
                     r_put_line('Erro ao incluir IDF - ' || v_import || ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_IDF', 'E');  
                     commit;
                     end;
  end;
  else
     delete from hon_gttemp_idf
      where id   = v_tab(i).id;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_assoc_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof_assoc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_assoc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_ASSOCIADO (ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO)
   VALUES (v_tab(i).ANO_MES_EMISSAO_ASSOC,
v_site,
v_tab(i).CTRL_DESCARGA,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DESPACHO,
v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
v_tab(i).DH_EMISSAO_ASSOC,
v_tab(i).DOF_ASSOC_SEQUENCE,
v_sequence,
v_tab(i).EDOF_CODIGO_ASSOC,
v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_dof,
v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
v_tab(i).MDOF_CODIGO_ASSOC,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NUMERO_ASSOC,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_TOTAL,
v_tab(i).QTD_VOLUMES,
v_tab(i).SEQUENCIA,
v_tab(i).SERIE_SUBSERIE_ASSOC,
v_tab(i).TADOC_CODIGO,
v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
v_tab(i).TIPO_PRODUTO,
v_tab(i).VL_MERCADORIAS,
v_tab(i).VL_RATEIO,
v_tab(i).VL_TOTAL_DOF_ASSOC,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_dof_associado
				    set 
ANO_MES_EMISSAO_ASSOC =  v_tab(i).ANO_MES_EMISSAO_ASSOC,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
C4_IND_TEM_REAL =  v_tab(i).C4_IND_TEM_REAL,
DESPACHO =  v_tab(i).DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC =  v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC =  v_tab(i).DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE =  v_tab(i).DOF_ASSOC_SEQUENCE,
EDOF_CODIGO_ASSOC =  v_tab(i).EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC =  v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
IND_ENTRADA_SAIDA_ASSOC =  v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC =  v_tab(i).MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO =  v_tab(i).MUN_COD_DESTINO,
MUN_COD_ORIGEM =  v_tab(i).MUN_COD_ORIGEM,
NFE_LOCALIZADOR =  v_tab(i).NFE_LOCALIZADOR,
NUMERO_ASSOC =  v_tab(i).NUMERO_ASSOC,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PESO_TOTAL =  v_tab(i).PESO_TOTAL,
QTD_VOLUMES =  v_tab(i).QTD_VOLUMES,
SEQUENCIA =  v_tab(i).SEQUENCIA,
SERIE_SUBSERIE_ASSOC =  v_tab(i).SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO =  v_tab(i).TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC =  v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO =  v_tab(i).TIPO_PRODUTO,
VL_MERCADORIAS =  v_tab(i).VL_MERCADORIAS,
VL_RATEIO =  v_tab(i).VL_RATEIO,
VL_TOTAL_DOF_ASSOC =  v_tab(i).VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO =  v_tab(i).VL_TOTAL_STF_SUBSTITUIDO
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and dof_assoc_sequence = v_tab(i).dof_assoc_sequence;
			  exception when others then
                     r_put_line('Erro ao alterar DOF_ASSOC DUP. - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_ASSOC - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_assoc_fserv (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof_assoc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_assoc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo in ('NFS', 'NFS-E')
      ;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_ASSOCIADO (ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO)
   VALUES (v_tab(i).ANO_MES_EMISSAO_ASSOC,
v_site,
v_tab(i).CTRL_DESCARGA,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DESPACHO,
v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
v_tab(i).DH_EMISSAO_ASSOC,
v_tab(i).DOF_ASSOC_SEQUENCE,
v_sequence,
v_tab(i).EDOF_CODIGO_ASSOC,
v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_dof,
v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
v_tab(i).MDOF_CODIGO_ASSOC,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NUMERO_ASSOC,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_TOTAL,
v_tab(i).QTD_VOLUMES,
v_tab(i).SEQUENCIA,
v_tab(i).SERIE_SUBSERIE_ASSOC,
v_tab(i).TADOC_CODIGO,
v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
v_tab(i).TIPO_PRODUTO,
v_tab(i).VL_MERCADORIAS,
v_tab(i).VL_RATEIO,
v_tab(i).VL_TOTAL_DOF_ASSOC,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_dof_associado
				    set 
ANO_MES_EMISSAO_ASSOC =  v_tab(i).ANO_MES_EMISSAO_ASSOC,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
C4_IND_TEM_REAL =  v_tab(i).C4_IND_TEM_REAL,
DESPACHO =  v_tab(i).DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC =  v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC =  v_tab(i).DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE =  v_tab(i).DOF_ASSOC_SEQUENCE,
EDOF_CODIGO_ASSOC =  v_tab(i).EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC =  v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
IND_ENTRADA_SAIDA_ASSOC =  v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC =  v_tab(i).MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO =  v_tab(i).MUN_COD_DESTINO,
MUN_COD_ORIGEM =  v_tab(i).MUN_COD_ORIGEM,
NFE_LOCALIZADOR =  v_tab(i).NFE_LOCALIZADOR,
NUMERO_ASSOC =  v_tab(i).NUMERO_ASSOC,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PESO_TOTAL =  v_tab(i).PESO_TOTAL,
QTD_VOLUMES =  v_tab(i).QTD_VOLUMES,
SEQUENCIA =  v_tab(i).SEQUENCIA,
SERIE_SUBSERIE_ASSOC =  v_tab(i).SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO =  v_tab(i).TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC =  v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO =  v_tab(i).TIPO_PRODUTO,
VL_MERCADORIAS =  v_tab(i).VL_MERCADORIAS,
VL_RATEIO =  v_tab(i).VL_RATEIO,
VL_TOTAL_DOF_ASSOC =  v_tab(i).VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO =  v_tab(i).VL_TOTAL_STF_SUBSTITUIDO
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and dof_assoc_sequence = v_tab(i).dof_assoc_sequence;
			  exception when others then
                     r_put_line('Erro ao alterar DOF_ASSOC DUP. - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_ASSOC - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_assoc_fmerc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_dof_assoc%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_assoc_v;
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo not in ('NFS', 'NFS-E')
      ;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_ASSOCIADO (ANO_MES_EMISSAO_ASSOC,
CODIGO_DO_SITE,
CTRL_DESCARGA,
C4_IND_TEM_REAL,
DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE,
DOF_SEQUENCE,
EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM,
ID_DOF,
IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO,
MUN_COD_ORIGEM,
NFE_LOCALIZADOR,
NUMERO_ASSOC,
PESO_LIQUIDO_KG,
PESO_TOTAL,
QTD_VOLUMES,
SEQUENCIA,
SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO,
VL_MERCADORIAS,
VL_RATEIO,
VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO)
   VALUES (v_tab(i).ANO_MES_EMISSAO_ASSOC,
v_site,
v_tab(i).CTRL_DESCARGA,
v_tab(i).C4_IND_TEM_REAL,
v_tab(i).DESPACHO,
v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
v_tab(i).DH_EMISSAO_ASSOC,
v_tab(i).DOF_ASSOC_SEQUENCE,
v_sequence,
v_tab(i).EDOF_CODIGO_ASSOC,
v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_dof,
v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
v_tab(i).MDOF_CODIGO_ASSOC,
v_tab(i).MUN_COD_DESTINO,
v_tab(i).MUN_COD_ORIGEM,
v_tab(i).NFE_LOCALIZADOR,
v_tab(i).NUMERO_ASSOC,
v_tab(i).PESO_LIQUIDO_KG,
v_tab(i).PESO_TOTAL,
v_tab(i).QTD_VOLUMES,
v_tab(i).SEQUENCIA,
v_tab(i).SERIE_SUBSERIE_ASSOC,
v_tab(i).TADOC_CODIGO,
v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
v_tab(i).TIPO_PRODUTO,
v_tab(i).VL_MERCADORIAS,
v_tab(i).VL_RATEIO,
v_tab(i).VL_TOTAL_DOF_ASSOC,
v_tab(i).VL_TOTAL_STF_SUBSTITUIDO);
    exception when DUP_VAL_ON_INDEX 
	       then 
			  begin
			     update cor_dof_associado
				    set 
ANO_MES_EMISSAO_ASSOC =  v_tab(i).ANO_MES_EMISSAO_ASSOC,
CTRL_DESCARGA =  v_tab(i).CTRL_DESCARGA,
C4_IND_TEM_REAL =  v_tab(i).C4_IND_TEM_REAL,
DESPACHO =  v_tab(i).DESPACHO,
DESTINATARIO_PFJ_CODIGO_ASSOC =  v_tab(i).DESTINATARIO_PFJ_CODIGO_ASSOC,
DH_EMISSAO_ASSOC =  v_tab(i).DH_EMISSAO_ASSOC,
DOF_ASSOC_SEQUENCE =  v_tab(i).DOF_ASSOC_SEQUENCE,
EDOF_CODIGO_ASSOC =  v_tab(i).EDOF_CODIGO_ASSOC,
EMITENTE_PFJ_CODIGO_ASSOC =  v_tab(i).EMITENTE_PFJ_CODIGO_ASSOC,
HON_CONSOLIDA_ORIGEM =  v_tab(i).HON_CONSOLIDA_ORIGEM,
IND_ENTRADA_SAIDA_ASSOC =  v_tab(i).IND_ENTRADA_SAIDA_ASSOC,
MDOF_CODIGO_ASSOC =  v_tab(i).MDOF_CODIGO_ASSOC,
MUN_COD_DESTINO =  v_tab(i).MUN_COD_DESTINO,
MUN_COD_ORIGEM =  v_tab(i).MUN_COD_ORIGEM,
NFE_LOCALIZADOR =  v_tab(i).NFE_LOCALIZADOR,
NUMERO_ASSOC =  v_tab(i).NUMERO_ASSOC,
PESO_LIQUIDO_KG =  v_tab(i).PESO_LIQUIDO_KG,
PESO_TOTAL =  v_tab(i).PESO_TOTAL,
QTD_VOLUMES =  v_tab(i).QTD_VOLUMES,
SEQUENCIA =  v_tab(i).SEQUENCIA,
SERIE_SUBSERIE_ASSOC =  v_tab(i).SERIE_SUBSERIE_ASSOC,
TADOC_CODIGO =  v_tab(i).TADOC_CODIGO,
TIPO_DOC_TRANSP_ANT_ASSOC =  v_tab(i).TIPO_DOC_TRANSP_ANT_ASSOC,
TIPO_PRODUTO =  v_tab(i).TIPO_PRODUTO,
VL_MERCADORIAS =  v_tab(i).VL_MERCADORIAS,
VL_RATEIO =  v_tab(i).VL_RATEIO,
VL_TOTAL_DOF_ASSOC =  v_tab(i).VL_TOTAL_DOF_ASSOC,
VL_TOTAL_STF_SUBSTITUIDO =  v_tab(i).VL_TOTAL_STF_SUBSTITUIDO
				  where codigo_do_site = v_site
				    and dof_sequence = v_sequence
					and dof_assoc_sequence = v_tab(i).dof_assoc_sequence;
			  exception when others then
                     r_put_line('Erro ao alterar DOF_ASSOC DUP. - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
			  end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_ASSOC - ' || v_tab(i).dof_assoc_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_ASSOCIADO', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_parc_filtro (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_parcela%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_parc_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    v_site     := 0;
    v_sequence := 0;
    v_dof      := 0;
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_PARCELA (BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC)
   VALUES (v_tab(i).BLOQ_SEQ,
v_site,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_IRRF,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_sequence,
v_tab(i).DT_EFETIVO_PAGTO,
v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
v_tab(i).DT_VENCIMENTO_PARCELA,
v_tab(i).DUPL_SEQ,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_VENCTO_SAP,
v_tab(i).HON_VL_SAP,
v_tab(i).IND_ADIANTAMENTO,
v_tab(i).IND_SUSPEITA,
v_tab(i).MP_CODIGO,
v_tab(i).NUM_PARCELA,
v_tab(i).OBS_DESC_CONDICIONAL,
v_tab(i).PERC_DESC_CONDICIONAL,
v_tab(i).PERC_VALOR_NF,
v_tab(i).REVISAO,
v_tab(i).SEQ_CALCULO,
v_tab(i).SIGLA_MOEDA,
v_tab(i).VL_ABATIDO_RESIDUO_PCC,
v_tab(i).VL_BASE_ACUM_COFINS_RET,
v_tab(i).VL_BASE_ACUM_CSLL_RET,
v_tab(i).VL_BASE_ACUM_PCC,
v_tab(i).VL_BASE_ACUM_PIS_RET,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IRRF_RET,
v_tab(i).VL_BASE_ISS_RET,
v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_REDUZIDA_IRRF,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESCONTO_CONDICIONAL,
v_tab(i).VL_FISCAL,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IRRF_RET,
v_tab(i).VL_ISS_RET,
v_tab(i).VL_PARCELA,
v_tab(i).VL_PIS_COFINS_CSLL_RET,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_RESIDUO_PCC);
    exception when DUP_VAL_ON_INDEX 
	       then 
			   begin
			   update cor_dof_parcela
set BLOQ_SEQ = v_tab(i).BLOQ_SEQ,
CODIGO_RETENCAO_COFINS = v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL = v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF = v_tab(i).CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC = v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS = v_tab(i).CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
DT_EFETIVO_PAGTO = v_tab(i).DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL = v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA = v_tab(i).DT_VENCIMENTO_PARCELA,
DUPL_SEQ = v_tab(i).DUPL_SEQ,
HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP = v_tab(i).HON_VENCTO_SAP,
HON_VL_SAP = v_tab(i).HON_VL_SAP,
IND_ADIANTAMENTO = v_tab(i).IND_ADIANTAMENTO,
IND_SUSPEITA = v_tab(i).IND_SUSPEITA,
MP_CODIGO = v_tab(i).MP_CODIGO,
OBS_DESC_CONDICIONAL = v_tab(i).OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL = v_tab(i).PERC_DESC_CONDICIONAL,
PERC_VALOR_NF = v_tab(i).PERC_VALOR_NF,
REVISAO = v_tab(i).REVISAO,
SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC = v_tab(i).VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET = v_tab(i).VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET = v_tab(i).VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC = v_tab(i).VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET = v_tab(i).VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET = v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET = v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_INSS_RET = v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IRRF_RET = v_tab(i).VL_BASE_IRRF_RET,
VL_BASE_ISS_RET = v_tab(i).VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET = v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET = v_tab(i).VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF = v_tab(i).VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET = v_tab(i).VL_COFINS_RET,
VL_CSLL_RET = v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG = v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG = v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL = v_tab(i).VL_DESCONTO_CONDICIONAL,
VL_FISCAL = v_tab(i).VL_FISCAL,
VL_INSS_RET = v_tab(i).VL_INSS_RET,
VL_IRRF_RET = v_tab(i).VL_IRRF_RET,
VL_ISS_RET = v_tab(i).VL_ISS_RET,
VL_PARCELA = v_tab(i).VL_PARCELA,
VL_PIS_COFINS_CSLL_RET = v_tab(i).VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET = v_tab(i).VL_PIS_RET,
VL_RESIDUO_PCC = v_tab(i).VL_RESIDUO_PCC			   
where num_parcela = v_tab(i).num_parcela
  and dof_sequence = v_sequence
  and codigo_do_site =  v_site;
			   exception when others then
                     r_put_line('Erro ao alterar DOF_PARCELA DUP. - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem 
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_PARCELA - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_parc_fserv (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_parcela%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_parc_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    v_site     := 0;
    v_sequence := 0;
    v_dof      := 0;
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo in ('NFS','NFS-E')
      ;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_PARCELA (BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC)
   VALUES (v_tab(i).BLOQ_SEQ,
v_site,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_IRRF,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_sequence,
v_tab(i).DT_EFETIVO_PAGTO,
v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
v_tab(i).DT_VENCIMENTO_PARCELA,
v_tab(i).DUPL_SEQ,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_VENCTO_SAP,
v_tab(i).HON_VL_SAP,
v_tab(i).IND_ADIANTAMENTO,
v_tab(i).IND_SUSPEITA,
v_tab(i).MP_CODIGO,
v_tab(i).NUM_PARCELA,
v_tab(i).OBS_DESC_CONDICIONAL,
v_tab(i).PERC_DESC_CONDICIONAL,
v_tab(i).PERC_VALOR_NF,
v_tab(i).REVISAO,
v_tab(i).SEQ_CALCULO,
v_tab(i).SIGLA_MOEDA,
v_tab(i).VL_ABATIDO_RESIDUO_PCC,
v_tab(i).VL_BASE_ACUM_COFINS_RET,
v_tab(i).VL_BASE_ACUM_CSLL_RET,
v_tab(i).VL_BASE_ACUM_PCC,
v_tab(i).VL_BASE_ACUM_PIS_RET,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IRRF_RET,
v_tab(i).VL_BASE_ISS_RET,
v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_REDUZIDA_IRRF,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESCONTO_CONDICIONAL,
v_tab(i).VL_FISCAL,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IRRF_RET,
v_tab(i).VL_ISS_RET,
v_tab(i).VL_PARCELA,
v_tab(i).VL_PIS_COFINS_CSLL_RET,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_RESIDUO_PCC);
    exception when DUP_VAL_ON_INDEX 
	       then 
			   begin
			   update cor_dof_parcela
set BLOQ_SEQ = v_tab(i).BLOQ_SEQ,
CODIGO_RETENCAO_COFINS = v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL = v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF = v_tab(i).CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC = v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS = v_tab(i).CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
DT_EFETIVO_PAGTO = v_tab(i).DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL = v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA = v_tab(i).DT_VENCIMENTO_PARCELA,
DUPL_SEQ = v_tab(i).DUPL_SEQ,
HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP = v_tab(i).HON_VENCTO_SAP,
HON_VL_SAP = v_tab(i).HON_VL_SAP,
IND_ADIANTAMENTO = v_tab(i).IND_ADIANTAMENTO,
IND_SUSPEITA = v_tab(i).IND_SUSPEITA,
MP_CODIGO = v_tab(i).MP_CODIGO,
OBS_DESC_CONDICIONAL = v_tab(i).OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL = v_tab(i).PERC_DESC_CONDICIONAL,
PERC_VALOR_NF = v_tab(i).PERC_VALOR_NF,
REVISAO = v_tab(i).REVISAO,
SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC = v_tab(i).VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET = v_tab(i).VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET = v_tab(i).VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC = v_tab(i).VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET = v_tab(i).VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET = v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET = v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_INSS_RET = v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IRRF_RET = v_tab(i).VL_BASE_IRRF_RET,
VL_BASE_ISS_RET = v_tab(i).VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET = v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET = v_tab(i).VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF = v_tab(i).VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET = v_tab(i).VL_COFINS_RET,
VL_CSLL_RET = v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG = v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG = v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL = v_tab(i).VL_DESCONTO_CONDICIONAL,
VL_FISCAL = v_tab(i).VL_FISCAL,
VL_INSS_RET = v_tab(i).VL_INSS_RET,
VL_IRRF_RET = v_tab(i).VL_IRRF_RET,
VL_ISS_RET = v_tab(i).VL_ISS_RET,
VL_PARCELA = v_tab(i).VL_PARCELA,
VL_PIS_COFINS_CSLL_RET = v_tab(i).VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET = v_tab(i).VL_PIS_RET,
VL_RESIDUO_PCC = v_tab(i).VL_RESIDUO_PCC			   
where num_parcela = v_tab(i).num_parcela
  and dof_sequence = v_sequence
  and codigo_do_site =  v_site;
			   exception when others then
                     r_put_line('Erro ao alterar DOF_PARCELA DUP. - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem 
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_PARCELA - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE ins_tab_dof_parc_fmerc (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_site           cor_idf.codigo_do_site%TYPE;
  v_sequence       cor_idf.dof_sequence%TYPE;
  v_dof            cor_idf.dof_id%TYPE;
  v_import         cor_dof.dof_import_numero%TYPE;
  v_tot            integer;
  --
  v_sql    VARCHAR2(32767);
  TYPE typ_tab IS TABLE OF hon_gttemp_parcela%ROWTYPE;
  v_tab typ_tab;
CURSOR c IS
 SELECT
 BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC,
0 DOF_IMPORT_NUMERO
 FROM hon_consolida_dof_parc_v
  -- incluir filtro de origem na busca
 where hon_consolida_origem like g_origem; 
BEGIN
  OPEN c;
  LOOP
  FETCH c BULK COLLECT INTO v_tab LIMIT 10000;
  FOR i IN 1..v_tab.COUNT
  LOOP
    v_site     := 0;
    v_sequence := 0;
    v_dof      := 0;
     -- verifica qual origem e obtem dados do dof original
   if v_tab(i).hon_consolida_origem = 'MAO'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_mao
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'HAB'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_hab
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
   if v_tab(i).hon_consolida_origem = 'PECAS'
   then
        -- obtem informacoes do dof_import
        select dof_import_numero
          into v_import
          from cor_dof@lk_pecas
         where codigo_do_site = v_tab(i).codigo_do_site
         and dof_sequence   = v_tab(i).dof_sequence;
   end if;
     v_tot := 0;
     select count(1)
       into v_tot
       from cor_dof
      where dof_import_numero = v_import
        and edof_codigo not in ('NFS','NFS-E')
      ;
     if v_tot > 0
     then
        v_site     := 0;
        v_sequence := 0;
        v_dof      := 0;
     select codigo_do_site,
            dof_sequence,
            id
       into v_site,
            v_sequence,
            v_dof
       from cor_dof
      where dof_import_numero = v_import;
      begin
     INSERT INTO COR_DOF_PARCELA (BLOQ_SEQ,
CODIGO_DO_SITE,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO,
DOF_SEQUENCE,
DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA,
DUPL_SEQ,
HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP,
HON_VL_SAP,
IND_ADIANTAMENTO,
IND_SUSPEITA,
MP_CODIGO,
NUM_PARCELA,
OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL,
PERC_VALOR_NF,
REVISAO,
SEQ_CALCULO,
SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET,
VL_BASE_INSS_RET,
VL_BASE_IRRF_RET,
VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL,
VL_FISCAL,
VL_INSS_RET,
VL_IRRF_RET,
VL_ISS_RET,
VL_PARCELA,
VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET,
VL_RESIDUO_PCC)
   VALUES (v_tab(i).BLOQ_SEQ,
v_site,
v_tab(i).CODIGO_RETENCAO_COFINS,
v_tab(i).CODIGO_RETENCAO_CSLL,
v_tab(i).CODIGO_RETENCAO_IRRF,
v_tab(i).CODIGO_RETENCAO_PCC,
v_tab(i).CODIGO_RETENCAO_PIS,
v_tab(i).CTRL_ORIGEM_REGISTRO,
v_sequence,
v_tab(i).DT_EFETIVO_PAGTO,
v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
v_tab(i).DT_VENCIMENTO_PARCELA,
v_tab(i).DUPL_SEQ,
v_tab(i).HON_CONSOLIDA_ORIGEM,
v_tab(i).HON_VENCTO_SAP,
v_tab(i).HON_VL_SAP,
v_tab(i).IND_ADIANTAMENTO,
v_tab(i).IND_SUSPEITA,
v_tab(i).MP_CODIGO,
v_tab(i).NUM_PARCELA,
v_tab(i).OBS_DESC_CONDICIONAL,
v_tab(i).PERC_DESC_CONDICIONAL,
v_tab(i).PERC_VALOR_NF,
v_tab(i).REVISAO,
v_tab(i).SEQ_CALCULO,
v_tab(i).SIGLA_MOEDA,
v_tab(i).VL_ABATIDO_RESIDUO_PCC,
v_tab(i).VL_BASE_ACUM_COFINS_RET,
v_tab(i).VL_BASE_ACUM_CSLL_RET,
v_tab(i).VL_BASE_ACUM_PCC,
v_tab(i).VL_BASE_ACUM_PIS_RET,
v_tab(i).VL_BASE_COFINS_RET,
v_tab(i).VL_BASE_CSLL_RET,
v_tab(i).VL_BASE_INSS_RET,
v_tab(i).VL_BASE_IRRF_RET,
v_tab(i).VL_BASE_ISS_RET,
v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
v_tab(i).VL_BASE_PIS_RET,
v_tab(i).VL_BASE_REDUZIDA_IRRF,
v_tab(i).VL_COFINS_RET,
v_tab(i).VL_CSLL_RET,
v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
v_tab(i).VL_DEDUCAO_PENSAO_PRG,
v_tab(i).VL_DESCONTO_CONDICIONAL,
v_tab(i).VL_FISCAL,
v_tab(i).VL_INSS_RET,
v_tab(i).VL_IRRF_RET,
v_tab(i).VL_ISS_RET,
v_tab(i).VL_PARCELA,
v_tab(i).VL_PIS_COFINS_CSLL_RET,
v_tab(i).VL_PIS_RET,
v_tab(i).VL_RESIDUO_PCC);
    exception when DUP_VAL_ON_INDEX 
	       then 
			   begin
			   update cor_dof_parcela
set BLOQ_SEQ = v_tab(i).BLOQ_SEQ,
CODIGO_RETENCAO_COFINS = v_tab(i).CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL = v_tab(i).CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_IRRF = v_tab(i).CODIGO_RETENCAO_IRRF,
CODIGO_RETENCAO_PCC = v_tab(i).CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS = v_tab(i).CODIGO_RETENCAO_PIS,
CTRL_ORIGEM_REGISTRO = v_tab(i).CTRL_ORIGEM_REGISTRO,
DT_EFETIVO_PAGTO = v_tab(i).DT_EFETIVO_PAGTO,
DT_LIMITE_DESC_CONDICIONAL = v_tab(i).DT_LIMITE_DESC_CONDICIONAL,
DT_VENCIMENTO_PARCELA = v_tab(i).DT_VENCIMENTO_PARCELA,
DUPL_SEQ = v_tab(i).DUPL_SEQ,
HON_CONSOLIDA_ORIGEM = v_tab(i).HON_CONSOLIDA_ORIGEM,
HON_VENCTO_SAP = v_tab(i).HON_VENCTO_SAP,
HON_VL_SAP = v_tab(i).HON_VL_SAP,
IND_ADIANTAMENTO = v_tab(i).IND_ADIANTAMENTO,
IND_SUSPEITA = v_tab(i).IND_SUSPEITA,
MP_CODIGO = v_tab(i).MP_CODIGO,
OBS_DESC_CONDICIONAL = v_tab(i).OBS_DESC_CONDICIONAL,
PERC_DESC_CONDICIONAL = v_tab(i).PERC_DESC_CONDICIONAL,
PERC_VALOR_NF = v_tab(i).PERC_VALOR_NF,
REVISAO = v_tab(i).REVISAO,
SEQ_CALCULO = v_tab(i).SEQ_CALCULO,
SIGLA_MOEDA = v_tab(i).SIGLA_MOEDA,
VL_ABATIDO_RESIDUO_PCC = v_tab(i).VL_ABATIDO_RESIDUO_PCC,
VL_BASE_ACUM_COFINS_RET = v_tab(i).VL_BASE_ACUM_COFINS_RET,
VL_BASE_ACUM_CSLL_RET = v_tab(i).VL_BASE_ACUM_CSLL_RET,
VL_BASE_ACUM_PCC = v_tab(i).VL_BASE_ACUM_PCC,
VL_BASE_ACUM_PIS_RET = v_tab(i).VL_BASE_ACUM_PIS_RET,
VL_BASE_COFINS_RET = v_tab(i).VL_BASE_COFINS_RET,
VL_BASE_CSLL_RET = v_tab(i).VL_BASE_CSLL_RET,
VL_BASE_INSS_RET = v_tab(i).VL_BASE_INSS_RET,
VL_BASE_IRRF_RET = v_tab(i).VL_BASE_IRRF_RET,
VL_BASE_ISS_RET = v_tab(i).VL_BASE_ISS_RET,
VL_BASE_PIS_COFINS_CSLL_RET = v_tab(i).VL_BASE_PIS_COFINS_CSLL_RET,
VL_BASE_PIS_RET = v_tab(i).VL_BASE_PIS_RET,
VL_BASE_REDUZIDA_IRRF = v_tab(i).VL_BASE_REDUZIDA_IRRF,
VL_COFINS_RET = v_tab(i).VL_COFINS_RET,
VL_CSLL_RET = v_tab(i).VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG = v_tab(i).VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_PENSAO_PRG = v_tab(i).VL_DEDUCAO_PENSAO_PRG,
VL_DESCONTO_CONDICIONAL = v_tab(i).VL_DESCONTO_CONDICIONAL,
VL_FISCAL = v_tab(i).VL_FISCAL,
VL_INSS_RET = v_tab(i).VL_INSS_RET,
VL_IRRF_RET = v_tab(i).VL_IRRF_RET,
VL_ISS_RET = v_tab(i).VL_ISS_RET,
VL_PARCELA = v_tab(i).VL_PARCELA,
VL_PIS_COFINS_CSLL_RET = v_tab(i).VL_PIS_COFINS_CSLL_RET,
VL_PIS_RET = v_tab(i).VL_PIS_RET,
VL_RESIDUO_PCC = v_tab(i).VL_RESIDUO_PCC			   
where num_parcela = v_tab(i).num_parcela
  and dof_sequence = v_sequence
  and codigo_do_site =  v_site;
			   exception when others then
                     r_put_line('Erro ao alterar DOF_PARCELA DUP. - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem 
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
			   end;
            when others
                 then
                     r_put_line('Erro ao incluir DOF_PARCELA - ' || v_tab(i).dof_sequence|| ' - ' || v_tab(i).hon_consolida_origem
					 || ' - ' || 'Sequence DEST: ' || v_sequence);
                     r_put_line(SQLERRM);
                     r_put_line(rpad('=',120,'='));
                     begin
                     -- Atualizo registro a registro o controle de transferencia da tabela:
                     HON_CONSOLIDA_CTRL_TRANSF_PRC(v_tab(i).HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'E');  
                     commit;
                     end;
  end;
  end if;
  END LOOP;
  COMMIT;
  EXIT WHEN c%NOTFOUND;
  END LOOP;
  CLOSE c;
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
  IF c%ISOPEN THEN
  CLOSE c;
  END IF;
  RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE insere_tabela_def (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
)
IS
  v_sql    VARCHAR2(32767);
BEGIN

  v_sql :=        'DECLARE ';
  v_sql := v_sql||'  TYPE typ_tab IS TABLE OF '||p_view||'%ROWTYPE; ';
  v_sql := v_sql||'  v_tab typ_tab; ';
  v_sql := v_sql||'  CURSOR c IS ';
  v_sql := v_sql||'    SELECT '||p_select||' FROM '||p_view||' t'||p_where||' ; ';
  v_sql := v_sql||'BEGIN ';
  v_sql := v_sql||'  OPEN c; ';
  v_sql := v_sql||'  LOOP ';
  v_sql := v_sql||'    FETCH c BULK COLLECT INTO v_tab LIMIT '||p_lim||'; ';
  v_sql := v_sql||'    FOR i IN 1..v_tab.COUNT ';
  v_sql := v_sql||'    LOOP ';
  v_sql := v_sql||'      INSERT INTO '||p_tabela||' VALUES v_tab(i); ';
  v_sql := v_sql||'    END LOOP; ';
  v_sql := v_sql||'    COMMIT; ';
  v_sql := v_sql||'    EXIT WHEN c%NOTFOUND; ';
  v_sql := v_sql||'  END LOOP; ';
  v_sql := v_sql||'  CLOSE c; ';
  v_sql := v_sql||'  COMMIT; ';
  v_sql := v_sql||'EXCEPTION WHEN OTHERS THEN ';
  v_sql := v_sql||'  IF c%ISOPEN THEN ';
  v_sql := v_sql||'    CLOSE c; ';
  v_sql := v_sql||'  END IF; ';
  v_sql := v_sql||'  RAISE; ';
  v_sql := v_sql||'END; ';

  EXECUTE IMMEDIATE v_sql;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE copia_dof (
  p_processo    IN NUMBER
)
IS
  v_estab        VARCHAR2(4000);
  v_sql          VARCHAR2(4000);
BEGIN

-- Truncar tabelas temporarias
EXECUTE IMMEDIATE 'truncate table hon_gttemp_clas_merc';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_clas_pfj';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_dof';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_dof_ASSOC';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_idf';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_iss';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_loc_pfj';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_loc_vig';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_merc';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_parcela';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_pessoa';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_prest';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_pessoa_VIG';

--
  -- Inicializar variaveis
  g_data_comeco    := SYSDATE;
  g_processo := p_processo;
  g_limite   := 1000;

  -- Checo se as colunas estão cadastradas corretamente, gerando relatório de erro caso contrário.
  v_colunas_erro := NULL;
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PESSOA_VIGENCIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_LOCALIDADE_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_LOCALIDADE_VIGENCIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_CLASSIFICACAO_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_CLASSIFICACAO_MERCADORIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_MERCADORIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PRESTACAO');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_SERVICO_ISS');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_IDF');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF_ASSOCIADO');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF_PARCELA');

  -- Verifica se a inconsistência é por causa de alteração estrutural
  IF v_colunas_erro IS NOT NULL THEN
    gera_relatorio_erro_colunas(v_colunas_erro);

  ELSE
    -- DADOS DE DOCUMENTOS FISCAIS
    g_select := get_colunas('COR_DOF');

    -- Carrego dados do IDF
    g_select := get_colunas('COR_IDF');
    -- Não incluo dof_import_numero

    -- Carrego dados do DOF Associado
    g_select := get_colunas('COR_DOF_ASSOCIADO');
    -- Não incluo dof_import_numero
    -- Carrego dados do DOF Parcela
    g_select := get_colunas('COR_DOF_PARCELA');
    -- Não incluo dof_import_numero
    -- DADOS CADASTRAIS
    -- Carrego dados das PFJ
    g_select := get_colunas('COR_PESSOA');
    -- Carrego dados das PFJ Vigencia
    g_select := get_colunas('COR_PESSOA_VIGENCIA');
    -- Carrego dados das Localidades
    g_select := get_colunas('COR_LOCALIDADE_PESSOA');
    -- Carrego dados das Localidades Vigencia
    g_select := get_colunas('COR_LOCALIDADE_VIGENCIA');
    -- Carrego dados de Classificação Pessoa
    g_select := get_colunas('COR_CLASSIFICACAO_PESSOA');
    -- Carrego Dados de Mercadoria
    g_select := get_colunas('COR_MERCADORIA');
    -- Carrego Dados de Classificação de Mercadoria
    g_select := get_colunas('COR_CLASSIFICACAO_MERCADORIA');
    -- Carrego Dados de Prestação
    g_select := get_colunas('COR_PRESTACAO');
    -- Carrego Dados de Serviço ISS
    g_select := get_colunas('COR_SERVICO_ISS');
    gera_relatorio_erro;

	-- MARCO DADOS CADASTRAIS QUE NAO EXISTEM NO DESTINO PARA CONSOLIDACAO
	-- PFJ
	atualiza_pfj;
	atualiza_pfj_vig;
	atualiza_pfj_loc;
	atualiza_pfj_locvig;

	-- MERCADORIA
	atualiza_merc;

    -- DADOS CADASTRAIS - TABELAS DEFINITIVAS
    -- Inserção consolidada de dados cadastrais
    g_select := get_colunas('COR_PESSOA');
    begin
    insere_tabela_pfj( 'COR_PESSOA', g_select, 'hon_gttemp_pessoa', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_PESSOA_VIGENCIA');
    begin
    insere_tabela_pfj_vig( 'COR_PESSOA_VIGENCIA', g_select, 'hon_gttemp_pessoa_VIG', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_LOCALIDADE_PESSOA');
    begin
    insere_tabela_loc_pfj( 'COR_LOCALIDADE_PESSOA', g_select, 'hon_gttemp_loc_pfj', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_LOCALIDADE_VIGENCIA');
    begin
    insere_tabela_loc_vig( 'COR_LOCALIDADE_VIGENCIA', g_select, 'hon_gttemp_loc_vig', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_CLASSIFICACAO_PESSOA');
    begin
    insere_tabela_cla_pfj( 'COR_CLASSIFICACAO_PESSOA', g_select, 'hon_gttemp_clas_pfj', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_MERCADORIA');
    begin
    insere_tabela_merc( 'COR_MERCADORIA', g_select, 'hon_gttemp_merc', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_CLASSIFICACAO_MERCADORIA');
    begin
    insere_tabela_cla_merc( 'COR_CLASSIFICACAO_MERCADORIA', g_select, 'hon_gttemp_clas_merc', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_PRESTACAO');
    begin
    insere_tabela_prest( 'COR_PRESTACAO', g_select, 'hon_gttemp_prest', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_SERVICO_ISS');
    begin
    insere_tabela_iss( 'COR_SERVICO_ISS', g_select, 'hon_gttemp_iss', g_limite );
    commit;
    end;

    -- DADOS DE DOCUMENTOS FISCAIS - TABELAS DEFINITIVAS
    -- Levando os dados das views para tabelas definitivas - Documentos Fiscais
    g_select := get_colunas('COR_DOF');
    begin
    insere_tabela_dof( 'COR_DOF', g_select, 'hon_gttemp_dof', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_IDF');
    begin
    commit;
    insere_tabela_idf( 'COR_IDF', g_select, 'hon_gttemp_idf', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_ASSOCIADO');
    begin
    insere_tabela_dof_assoc( 'COR_DOF_ASSOCIADO', g_select, 'hon_gttemp_dof_ASSOC', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_PARCELA');
    begin
    insere_tabela_dof_parcela( 'COR_DOF_PARCELA', g_select, 'hon_gttemp_parcela', g_limite );
    commit;
    end;

    g_data_termino := SYSDATE;

    r_inclui_footer;
    COMMIT;
  END IF;

 EXCEPTION WHEN OTHERS THEN
  RAISE;
END; --copia_dof
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
PROCEDURE copia_dof_filtros (
  p_origem      IN VARCHAR2,
  p_tipo_docto  IN VARCHAR2,
  p_processo    IN NUMBER
)
IS
  v_estab        VARCHAR2(4000);
  v_sql          VARCHAR2(4000);
BEGIN

-- Truncar tabelas temporarias
EXECUTE IMMEDIATE 'truncate table hon_gttemp_clas_merc';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_clas_pfj';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_dof';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_dof_ASSOC';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_idf';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_iss';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_loc_pfj';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_loc_vig';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_merc';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_parcela';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_pessoa';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_prest';
EXECUTE IMMEDIATE 'truncate table hon_gttemp_pessoa_VIG';

--
  -- Inicializar variaveis
  g_data_comeco    := SYSDATE;
  g_processo       := p_processo;
  g_limite         := 1000;
  g_origem         := p_origem;
  g_tipo_docto     := p_tipo_docto;

  -- Checo se as colunas estão cadastradas corretamente, gerando relatório de erro caso contrário.
  v_colunas_erro := NULL;
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PESSOA_VIGENCIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_LOCALIDADE_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_LOCALIDADE_VIGENCIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_CLASSIFICACAO_PESSOA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_CLASSIFICACAO_MERCADORIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_MERCADORIA');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_PRESTACAO');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_SERVICO_ISS');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_IDF');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF_ASSOCIADO');
  v_colunas_erro := v_colunas_erro || checa_colunas('COR_DOF_PARCELA');

  -- Verifica se a inconsistência é por causa de alteração estrutural
  IF v_colunas_erro IS NOT NULL THEN
    gera_relatorio_erro_colunas(v_colunas_erro);

  ELSE
    -- DADOS DE DOCUMENTOS FISCAIS
    g_select := get_colunas('COR_DOF');

    -- Carrego dados do IDF
    g_select := get_colunas('COR_IDF');
    -- Não incluo dof_import_numero

    -- Carrego dados do DOF Associado
    g_select := get_colunas('COR_DOF_ASSOCIADO');
    -- Não incluo dof_import_numero
    -- Carrego dados do DOF Parcela
    g_select := get_colunas('COR_DOF_PARCELA');
    -- Não incluo dof_import_numero
    -- DADOS CADASTRAIS
    -- Carrego dados das PFJ
    g_select := get_colunas('COR_PESSOA');
    -- Carrego dados das PFJ Vigencia
    g_select := get_colunas('COR_PESSOA_VIGENCIA');
    -- Carrego dados das Localidades
    g_select := get_colunas('COR_LOCALIDADE_PESSOA');
    -- Carrego dados das Localidades Vigencia
    g_select := get_colunas('COR_LOCALIDADE_VIGENCIA');
    -- Carrego dados de Classificação Pessoa
    g_select := get_colunas('COR_CLASSIFICACAO_PESSOA');
    -- Carrego Dados de Mercadoria
    g_select := get_colunas('COR_MERCADORIA');
    -- Carrego Dados de Classificação de Mercadoria
    g_select := get_colunas('COR_CLASSIFICACAO_MERCADORIA');
    -- Carrego Dados de Prestação
    g_select := get_colunas('COR_PRESTACAO');
    -- Carrego Dados de Serviço ISS
    g_select := get_colunas('COR_SERVICO_ISS');
    gera_relatorio_erro_filtros;

	-- MARCO DADOS CADASTRAIS QUE NAO EXISTEM NO DESTINO PARA CONSOLIDACAO
	-- PFJ
	atualiza_pfj_filtro;
	atualiza_pfj_vig_filtro;
	atualiza_pfj_loc_filtro;
	atualiza_pfj_locvig_filtro;

	-- MERCADORIA
	if g_tipo_docto <> 'S' 
	then
	   atualiza_merc_filtro;
	end if;

    -- DADOS CADASTRAIS - TABELAS DEFINITIVAS
    -- Inserção consolidada de dados cadastrais
    g_select := get_colunas('COR_PESSOA');
    begin
    ins_tab_pfj_filtro( 'COR_PESSOA', g_select, 'hon_gttemp_pessoa', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_PESSOA_VIGENCIA');
    begin
    ins_tab_pfj_vig_filtro( 'COR_PESSOA_VIGENCIA', g_select, 'hon_gttemp_pessoa_VIG', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_LOCALIDADE_PESSOA');
    begin
    ins_tab_loc_pfj_filtro( 'COR_LOCALIDADE_PESSOA', g_select, 'hon_gttemp_loc_pfj', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_LOCALIDADE_VIGENCIA');
    begin
    ins_tab_loc_vig_filtro( 'COR_LOCALIDADE_VIGENCIA', g_select, 'hon_gttemp_loc_vig', g_limite);
    commit;
    end;

    g_select := get_colunas('COR_CLASSIFICACAO_PESSOA');
    begin
    ins_tab_cla_pfj_filtro( 'COR_CLASSIFICACAO_PESSOA', g_select, 'hon_gttemp_clas_pfj', g_limite );
    commit;
    end;

	if g_tipo_docto <> 'S' 
	then
       g_select := get_colunas('COR_MERCADORIA');
       begin
       ins_tab_merc_filtro( 'COR_MERCADORIA', g_select, 'hon_gttemp_merc', g_limite );
       commit;
       end;

       g_select := get_colunas('COR_CLASSIFICACAO_MERCADORIA');
       begin
       ins_tab_cla_merc_filtro( 'COR_CLASSIFICACAO_MERCADORIA', g_select, 'hon_gttemp_clas_merc', g_limite );
       commit;
       end;
    end if;

	if g_tipo_docto <> 'M' 
	then
       g_select := get_colunas('COR_PRESTACAO');
       begin
       ins_tab_prest_filtro( 'COR_PRESTACAO', g_select, 'hon_gttemp_prest', g_limite );
       commit;
       end;

       g_select := get_colunas('COR_SERVICO_ISS');
       begin
       ins_tab_iss_filtro( 'COR_SERVICO_ISS', g_select, 'hon_gttemp_iss', g_limite );
       commit;
       end;
    end if;

    -- DADOS DE DOCUMENTOS FISCAIS - TABELAS DEFINITIVAS
    -- Levando os dados das views para tabelas definitivas - Documentos Fiscais
	if g_tipo_docto = '%' 
	then
    g_select := get_colunas('COR_DOF');
    begin
    ins_tab_dof_filtro( 'COR_DOF', g_select, 'hon_gttemp_dof', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_IDF');
    begin
    commit;
    ins_tab_idf_filtro( 'COR_IDF', g_select, 'hon_gttemp_idf', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_ASSOCIADO');
    begin
    ins_tab_dof_assoc_filtro( 'COR_DOF_ASSOCIADO', g_select, 'hon_gttemp_dof_ASSOC', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_PARCELA');
    begin
    ins_tab_dof_parc_filtro( 'COR_DOF_PARCELA', g_select, 'hon_gttemp_parcela', g_limite );
    commit;
    end;
    end if;

	if g_tipo_docto = 'S' 
	then
    g_select := get_colunas('COR_DOF');
    begin
    ins_tab_dof_fserv( 'COR_DOF', g_select, 'hon_gttemp_dof', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_IDF');
    begin
    commit;
    ins_tab_idf_fserv( 'COR_IDF', g_select, 'hon_gttemp_idf', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_ASSOCIADO');
    begin
    ins_tab_dof_assoc_fserv( 'COR_DOF_ASSOCIADO', g_select, 'hon_gttemp_dof_ASSOC', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_PARCELA');
    begin
    ins_tab_dof_parc_fserv( 'COR_DOF_PARCELA', g_select, 'hon_gttemp_parcela', g_limite );
    commit;
    end;
    end if;

	if g_tipo_docto = 'M' 
	then
    g_select := get_colunas('COR_DOF');
    begin
    ins_tab_dof_fmerc( 'COR_DOF', g_select, 'hon_gttemp_dof', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_IDF');
    begin
    commit;
    ins_tab_idf_fmerc( 'COR_IDF', g_select, 'hon_gttemp_idf', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_ASSOCIADO');
    begin
    ins_tab_dof_assoc_fmerc( 'COR_DOF_ASSOCIADO', g_select, 'hon_gttemp_dof_ASSOC', g_limite );
    commit;
    end;

    g_select := get_colunas('COR_DOF_PARCELA');
    begin
    ins_tab_dof_parc_fmerc( 'COR_DOF_PARCELA', g_select, 'hon_gttemp_parcela', g_limite );
    commit;
    end;
    end if;

    g_data_termino := SYSDATE;

    r_inclui_footer;
    COMMIT;
  END IF;

 EXCEPTION WHEN OTHERS THEN
  RAISE;
END; --copia_dof_filtros
----------------------------------------------------------------------------------------------------
PROCEDURE r_inclui_header
IS
  v_header                VARCHAR2(4000);
  v_linha                 VARCHAR2(1000);
BEGIN
  v_linha  := syn_str.At_Say       ( ''     , TO_CHAR(SYSDATE,'DD/MM/YYYY')   , 1 );
  v_linha  := syn_str.At_Say_Center(v_linha , g_nome_relatorio                , g_posfinal );
  v_linha  := syn_str.At_say_Right (v_linha , 'Pagina: {PAG}'                 , g_posfinal );
  v_header := v_linha;

  v_linha  := RPAD('=',g_posfinal,'=');
  v_header := v_header || '/n' || v_linha;

  syn_processo.grava_cabecalho(g_processo, v_header);
END;
----------------------------------------------------------------------------------------------------
PROCEDURE r_inclui_header_filtros
IS
  v_header                VARCHAR2(4000);
  v_linha                 VARCHAR2(1000);
BEGIN
  v_linha  := syn_str.At_Say       ( ''     , TO_CHAR(SYSDATE,'DD/MM/YYYY')   , 1 );
  v_linha  := syn_str.At_Say_Center(v_linha , g_nome_relatorio                , g_posfinal );
  v_linha  := syn_str.At_say_Right (v_linha , 'Pagina: {PAG}'                 , g_posfinal );
  v_header := v_linha;

  v_linha  := RPAD('=',g_posfinal,'=');
  v_header := v_header || '/n' || v_linha;
  --
  v_linha  := 'Filtros: ' || g_origem || ' - ' || g_tipo_docto;
  v_header := v_header || '/n' || v_linha;
  v_linha  := RPAD('=',g_posfinal,'=');
  v_header := v_header || '/n' || v_linha;
  --

  syn_processo.grava_cabecalho(g_processo, v_header);
END;
----------------------------------------------------------------------------------------------------
PROCEDURE r_inclui_footer
IS
  v_linha                 VARCHAR2(1000);
  v_col01    CONSTANT     PLS_INTEGER :=  20;
  v_col02    CONSTANT     PLS_INTEGER :=  40;
BEGIN
  v_linha  := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);

  v_linha := syn_str.at_say( ''     , 'Horário de início:'                           , v_col01 );
  v_linha := syn_str.at_say( v_linha, TO_CHAR(g_data_comeco,'DD/MM/YYYY HH24:MI:SS') , v_col02 );
  r_put_line(v_linha);

  v_linha := syn_str.at_say( ''     , 'Horário de término:'                           , v_col01 );
  v_linha := syn_str.at_say( v_linha, TO_CHAR(g_data_termino,'DD/MM/YYYY HH24:MI:SS') , v_col02 );
  r_put_line(v_linha);
END;
----------------------------------------------------------------------------------------------------
PROCEDURE r_inclui_header_dof
IS
  v_linha                 VARCHAR2(1000);
BEGIN
  r_put_line(v_linha);

  v_linha  := syn_str.At_Say       ( '', 'Parâmetros utilizados:' , 10 );
  r_put_line(v_linha);

  v_linha  := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
END;
----------------------------------------------------------------------------------------------------
PROCEDURE r_put_line(
  p_linha  IN     VARCHAR2
)
IS
BEGIN
  -- Insere a linha no relatório e controla a paginacao
  syn_out.put_line(p_linha);
  IF g_cnt >= g_reg_pagina THEN
    syn_out.new_page;
    g_cnt := 0;
  END IF;
  g_cnt := g_cnt + 1;
END;
----------------------------------------------------------------------------------------------------
PROCEDURE gera_relatorio_dof
IS
  v_nome_transf           VARCHAR2(30)  := 'DOCUMENTOS FISCAIS';
  v_linha                 VARCHAR2(1000);
  v_sql                   VARCHAR2(32000);

  v_col01    CONSTANT     PLS_INTEGER :=              0;
  v_col02    CONSTANT     PLS_INTEGER :=  v_col01 +  20; --ESTABELECIMENTO
  v_col03    CONSTANT     PLS_INTEGER :=  v_col02 +  14; --DATA EMISSAO
  v_col04    CONSTANT     PLS_INTEGER :=  v_col03 +  20; --PARTICIPANTE
  v_col05    CONSTANT     PLS_INTEGER :=  v_col04 +  12; --NUMERO
  v_col06    CONSTANT     PLS_INTEGER :=  v_col05 +  10; --SERIE
  v_col07    CONSTANT     PLS_INTEGER :=  v_col06 +  25; --DOF IMPORT NUMERO
  v_posfinal CONSTANT     PLS_INTEGER :=            120; --DOF SEQUENCE

  c_dof      SYS_REFCURSOR;
  c_pfj      SYS_REFCURSOR;
  c_mrc      SYS_REFCURSOR;
  c_div_emp  SYS_REFCURSOR;

  v_informante_est_codigo cor_dof.informante_est_codigo%TYPE;
  v_data_emissao          cor_dof.dh_emissao%TYPE;
  v_participante          cor_dof.emitente_pfj_codigo%TYPE;
  v_numero                cor_dof.numero%TYPE;
  v_serie                 cor_dof.serie_subserie%TYPE;
  v_dof_import_numero     cor_dof.dof_import_numero%TYPE;
  v_dof_sequence          cor_dof.dof_sequence%TYPE;
  v_total                 NUMBER;

  v_pfj_codigo            cor_pessoa.pfj_codigo%TYPE;
  v_mnemonico             cor_pessoa.mnemonico%TYPE;
  v_cpf_cgc               cor_pessoa_vigencia.cpf_cgc%TYPE;
  v_dt_inicio             cor_pessoa_vigencia.dt_inicio%TYPE;

  v_merc_codigo           cor_mercadoria.merc_codigo%TYPE;
  v_nome                  cor_mercadoria.nome%TYPE;
  v_alterado_em           cor_mercadoria.alterado_em%TYPE;
  v_chave_origem          cor_mercadoria.chave_origem%TYPE;

  v_cnt_dof               PLS_INTEGER;
  v_cnt_idf               PLS_INTEGER;
--  v_cnt_idf               varchar2(500);
  v_cnt_dof_parc          PLS_INTEGER;
  v_cnt_dof_assoc         PLS_INTEGER;
  v_cnt_pes               PLS_INTEGER;
  v_cnt_pes_cls           PLS_INTEGER;
  v_cnt_loc               PLS_INTEGER;
  v_cnt_merc              PLS_INTEGER;
  v_cnt_merc_cls          PLS_INTEGER;
  v_cab                   PLS_INTEGER;

BEGIN
  g_posfinal := v_posfinal;
  -- Inicio variaveis globais para utilizacao
  g_nome_relatorio := 'Relatório de Processamento de Consolidação de Documentos Fiscais';
  g_cnt            := 0;
  g_reg_pagina     := 50;

  v_cab            := 0;

  syn_out.inicializa (g_processo, g_reg_pagina+2, g_posfinal);

  r_inclui_header;
  r_inclui_header_dof;

  r_put_line('REGISTROS - DOCUMENTOS FISCAIS CONSOLIDADOS');
  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
  OPEN c_dof FOR 'SELECT informante_est_codigo, dh_emissao,
                         DECODE(ind_entrada_saida,''E'',emitente_pfj_codigo,
                                                  ''S'',destinatario_pfj_codigo) participante,
                         numero, serie_subserie, dof_import_numero
                    FROM hon_gttemp_dof
                   ORDER BY 1,2';
  LOOP
    FETCH c_dof INTO v_informante_est_codigo,v_data_emissao,v_participante,v_numero,v_serie,v_dof_import_numero;
    EXIT WHEN c_dof%NOTFOUND;

    IF g_cnt = 0 OR v_cab = 0 THEN
      v_linha := syn_str.at_say( ''     , 'ESTABELECIMENTO'       , v_col01 );
      v_linha := syn_str.at_say( v_linha, 'DATA EMISSAO'          , v_col02 );
      v_linha := syn_str.at_say( v_linha, 'PARTICIPANTE'          , v_col03 );
      v_linha := syn_str.at_say( v_linha, 'NUMERO'                , v_col04 );
      v_linha := syn_str.at_say( v_linha, 'SERIE'                 , v_col05 );
      v_linha := syn_str.at_say( v_linha, 'DOF IMPORT NUMERO'     , v_col06 );
      r_put_line(v_linha);

      v_linha := syn_str.at_say( ''     , RPAD('-',v_col02-v_col01-1,'-')    , v_col01 );
      v_linha := syn_str.at_say( v_linha, RPAD('-',v_col03-v_col02-1,'-')    , v_col02 );
      v_linha := syn_str.at_say( v_linha, RPAD('-',v_col04-v_col03-1,'-')    , v_col03 );
      v_linha := syn_str.at_say( v_linha, RPAD('-',v_col05-v_col04-1,'-')    , v_col04 );
      v_linha := syn_str.at_say( v_linha, RPAD('-',v_col06-v_col05-1,'-')    , v_col05 );
      v_linha := syn_str.at_say( v_linha, RPAD('-',g_posfinal-v_col06-1,'-') , v_col06 );
      r_put_line(v_linha);

      v_cab := 1;
    END IF;

    v_linha := syn_str.at_say( ''     , v_informante_est_codigo , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_data_emissao          , v_col02 );
    v_linha := syn_str.at_say( v_linha, v_participante          , v_col03 );
    v_linha := syn_str.at_say( v_linha, v_numero                , v_col04 );
    v_linha := syn_str.at_say( v_linha, v_serie                 , v_col05 );
    v_linha := syn_str.at_say( v_linha, v_dof_import_numero     , v_col06 );
    r_put_line(v_linha);

  END LOOP;
  IF c_dof%ISOPEN THEN CLOSE c_dof; END IF;

    v_linha := RPAD('=',g_posfinal,'=');
    r_put_line(v_linha);
    r_put_line('REGISTROS - CADASTRO DE PESSOAS');
    v_linha := RPAD('=',g_posfinal,'=');
    r_put_line(v_linha);
    v_cab := 0;

    OPEN c_pfj FOR 'SELECT p.pfj_codigo, v.cpf_cgc, p.mnemonico, v.dt_inicio
                      FROM hon_gttemp_pessoa p, hon_gttemp_pessoa_VIG v
                     WHERE p.pfj_codigo = v.pfj_codigo
                     ORDER BY 1';
    LOOP
      FETCH c_pfj INTO v_pfj_codigo,v_cpf_cgc,v_mnemonico,v_dt_inicio;
      EXIT WHEN c_pfj%NOTFOUND;

      IF g_cnt = 0 OR v_cab = 0  THEN
        v_linha := syn_str.at_say( ''     , 'CÓD.PARTICIPANTE'       , v_col01 );
        v_linha := syn_str.at_say( v_linha, 'CNPJ'                   , v_col02 );
        v_linha := syn_str.at_say( v_linha, 'DESCRIÇÃO'              , v_col04 );
        v_linha := syn_str.at_say( v_linha, 'DT INICIO VIGENCIA'     , v_col07 );
        r_put_line(v_linha);

        v_linha := syn_str.at_say( ''     , RPAD('-',v_col02-v_col01-1,'-')    , v_col01 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',v_col04-v_col02-1,'-')    , v_col02 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',v_col07-v_col04-1,'-')    , v_col04 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',g_posfinal-v_col07-1,'-')    , v_col07 );
        r_put_line(v_linha);

        v_cab := 1;
      END IF;

      v_linha := syn_str.at_say( ''     , v_pfj_codigo     , v_col01 );
      v_linha := syn_str.at_say( v_linha, v_cpf_cgc        , v_col02 );
      v_linha := syn_str.at_say( v_linha, v_mnemonico      , v_col04 );
      v_linha := syn_str.at_say( v_linha, v_dt_inicio      , v_col07 );
      r_put_line(v_linha);

    END LOOP;
    IF c_pfj%ISOPEN THEN CLOSE c_pfj; END IF;

    v_linha := RPAD('=',g_posfinal,'=');
    r_put_line(v_linha);
    r_put_line('REGISTROS - CADASTRO DE MERCADORIAS');
    v_linha := RPAD('=',g_posfinal,'=');
    r_put_line(v_linha);
    v_cab := 0;

    OPEN c_mrc FOR 'SELECT merc_codigo, nome, alterado_em, chave_origem
                      FROM hon_gttemp_merc
                     ORDER BY 1';
    LOOP
      FETCH c_mrc INTO v_merc_codigo,v_nome,v_alterado_em,v_chave_origem;
      EXIT WHEN c_mrc%NOTFOUND;

      IF g_cnt = 0 OR v_cab = 0  THEN
        v_linha := syn_str.at_say( ''     , 'COD.MERCADORIA'    , v_col01 );
        v_linha := syn_str.at_say( v_linha, 'DESCRICAO'         , v_col03 );
        v_linha := syn_str.at_say( v_linha, 'ALTERADO EM'       , v_col05 );
        v_linha := syn_str.at_say( v_linha, 'CHAVE ORIGEM'      , v_col07 );
        r_put_line(v_linha);

        v_linha := syn_str.at_say( ''     , RPAD('-',v_col03-v_col01-1,'-')    , v_col01 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',v_col05-v_col03-1,'-')    , v_col03 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',v_col07-v_col05-1,'-')    , v_col05 );
        v_linha := syn_str.at_say( v_linha, RPAD('-',g_posfinal-v_col07-1,'-') , v_col07 );
        r_put_line(v_linha);

        v_cab := 1;
      END IF;

      v_linha := syn_str.at_say( ''     , v_merc_codigo    , v_col01 );
      v_linha := syn_str.at_say( v_linha, v_nome           , v_col03 );
      v_linha := syn_str.at_say( v_linha, v_alterado_em    , v_col05 );
      v_linha := syn_str.at_say( v_linha, v_chave_origem   , v_col07 );
      r_put_line(v_linha);

    END LOOP;
    IF c_mrc%ISOPEN THEN CLOSE c_mrc;
  END IF;

  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
  r_put_line('Resumo do processamento: REGISTROS TRANSFERIDOS');
  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);

  r_put_line('Documentos Fiscais');
  v_linha := RPAD('-',g_posfinal,'-');
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_dof ' INTO v_cnt_dof;
  v_linha := syn_str.at_say( ''     , '001 - Cabeçalho de documentos fiscais: ' , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_dof                                 , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_idf ' INTO v_cnt_idf;
--  EXECUTE IMMEDIATE 'SELECT dof_import_numero||hon_consolida_origem INTO :a FROM hon_gttemp_idf ' INTO v_cnt_idf;
  v_linha := syn_str.at_say( ''     , '002 - Itens de documentos fiscais: '     , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_idf                                 , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_parcela ' INTO v_cnt_dof_parc;
  v_linha := syn_str.at_say( ''     , '003 - Parcelas de pagamento: '           , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_dof_parc                            , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_dof_ASSOC ' INTO v_cnt_dof_assoc;
  v_linha := syn_str.at_say( ''     , '004 - Documentos Associados: '           , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_dof_assoc                           , v_col04 );
  r_put_line(v_linha);

  v_linha := RPAD('-',g_posfinal,'-');
  r_put_line(v_linha);
  r_put_line('Cadastros Associados');
  v_linha := RPAD('-',g_posfinal,'-');
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*)
                       INTO :a
                       FROM hon_gttemp_pessoa A, hon_gttemp_pessoa_VIG B
                      WHERE a.pfj_codigo = b.pfj_codigo' INTO v_cnt_pes;
  v_linha := syn_str.at_say( ''     , '005 - Cadastro de Pessoas: '       , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_pes                           , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*)
                       INTO :a
                       FROM hon_gttemp_loc_pfj A, hon_gttemp_loc_vig B
                      WHERE a.pfj_codigo = b.pfj_codigo
                        AND a.loc_codigo = b.loc_codigo ' INTO v_cnt_loc;
  v_linha := syn_str.at_say( ''     , '006 - Localidade: '       , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_loc                  , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_clas_pfj ' INTO v_cnt_pes_cls;
  v_linha := syn_str.at_say( ''     , '007 - Classe de Pessoas: '       , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_pes_cls                     , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_merc' INTO v_cnt_merc;
  v_linha := syn_str.at_say( ''     , '008 - Cadastro de Mercadorias: '       , v_col01 );
  v_linha := syn_str.at_say( v_linha, v_cnt_merc                              , v_col04 );
  r_put_line(v_linha);

  EXECUTE IMMEDIATE 'SELECT COUNT(*) INTO :a FROM hon_gttemp_clas_merc ' INTO v_cnt_merc_cls;
    v_linha := syn_str.at_say( ''     , '009 - Classe de Mercadorias: '       , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_merc_cls                        , v_col04 );
    r_put_line(v_linha);

  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
  r_put_line('POR EMPRESA');

  OPEN c_div_emp FOR 'SELECT informante_est_codigo, COUNT(*) total
                    FROM hon_gttemp_dof
                   GROUP BY informante_est_codigo
                   ORDER BY 1';
  LOOP
    FETCH c_div_emp INTO v_informante_est_codigo,v_total;
    EXIT WHEN c_div_emp%NOTFOUND;

    v_linha := RPAD('-',g_posfinal,'-');
    r_put_line(v_linha);
    v_linha := syn_str.at_say( ''     , v_informante_est_codigo , v_col01 );
    r_put_line(v_linha);
    v_linha := RPAD('-',g_posfinal,'-');
    r_put_line(v_linha);

    v_linha := syn_str.at_say( ''     , 'DOF: '                 , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_total                 , v_col02 );
    r_put_line(v_linha);

    v_cnt_idf := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_idf i
        WHERE i.dof_id IN ( SELECT id FROM hon_gttemp_dof WHERE informante_est_codigo = :a ) '
      INTO v_cnt_idf
      USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'IDF: '                 , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_idf               , v_col02 );
    r_put_line(v_linha);

    v_cnt_dof_parc := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_parcela p, hon_gttemp_dof d
        WHERE p.codigo_do_site = d.codigo_do_site
          AND p.dof_sequence = d.dof_sequence
          AND d.informante_est_codigo = :a '
      INTO v_cnt_dof_parc
      USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'PARCELA: '           , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_dof_parc        , v_col02 );
    r_put_line(v_linha);

    v_cnt_dof_assoc := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_dof_ASSOC a, hon_gttemp_dof d
        WHERE a.codigo_do_site = d.codigo_do_site
          AND a.dof_sequence = d.dof_sequence
          AND d.informante_est_codigo = :a '
      INTO v_cnt_dof_assoc
      USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'ASSOCIADO: '           , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_dof_assoc         , v_col02 );
    r_put_line(v_linha);

    v_sql := NULL;
    v_sql := v_sql || 'WITH var AS (SELECT :a FROM dual) ';
    v_sql := v_sql || '  SELECT pfj_codigo ';
    v_sql := v_sql || '    FROM hon_gttemp_pessoa p ';
    v_sql := v_sql || '   WHERE p.pfj_codigo IN ( ';
    v_sql := v_sql || '        SELECT distinct ferrov_subst_pfj_codigo     FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct cobranca_pfj_codigo         FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct destinatario_pfj_codigo     FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct emitente_pfj_codigo         FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct entrega_pfj_codigo          FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct remetente_pfj_codigo        FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct retirada_pfj_codigo         FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct transportador_pfj_codigo    FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct consig_pfj_codigo           FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct substituido_pfj_codigo      FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct pfj_codigo_redespachante    FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct tomador_outros_pfj_codigo   FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) ';
    v_sql := v_sql || '  UNION SELECT distinct destinatario_cte_pfj_codigo FROM  hon_gttemp_dof WHERE informante_est_codigo = (SELECT * FROM var) )';

  v_cnt_pes := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM ('||v_sql||') '
      INTO v_cnt_pes
      USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'PESSOA: '              , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_pes               , v_col02 );
    r_put_line(v_linha);

    v_cnt_loc := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_loc_pfj
        WHERE pfj_codigo IN ('||v_sql||') '
         INTO v_cnt_loc
        USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'LOCALIDADE: '          , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_loc               , v_col02 );
    r_put_line(v_linha);

    v_cnt_pes_cls := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_clas_pfj
        WHERE pfj_codigo IN ('||v_sql||') '
         INTO v_cnt_pes_cls
        USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'CLASS.PES.: '          , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_pes_cls           , v_col02 );
    r_put_line(v_linha);

    -- Mercadoria referente à empresa
    v_sql := NULL;
    v_sql := v_sql ||  'SELECT distinct merc_codigo ';
    v_sql := v_sql ||  '  FROM hon_gttemp_idf';
    v_sql := v_sql ||  ' WHERE dof_id IN ( SELECT id FROM hon_gttemp_dof WHERE informante_est_codigo = :a ) ';

    v_cnt_merc := 0;
    EXECUTE IMMEDIATE
      'SELECT COUNT(*)
         FROM hon_gttemp_merc
        WHERE merc_codigo IN ('||v_sql||') '
         INTO v_cnt_merc
        USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'MERCADORIA: '          , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_merc              , v_col02 );
    r_put_line(v_linha);

    v_cnt_merc_cls := 0;
    EXECUTE IMMEDIATE
        'SELECT COUNT(*)
           FROM hon_gttemp_clas_merc
          WHERE merc_codigo IN ('||v_sql||') '
          INTO v_cnt_merc_cls
         USING v_informante_est_codigo;

    v_linha := syn_str.at_say( ''     , 'CLASS.MERC.: '         , v_col01 );
    v_linha := syn_str.at_say( v_linha, v_cnt_merc_cls          , v_col02 );
    r_put_line(v_linha);

  END LOOP;
  IF c_div_emp%ISOPEN THEN CLOSE c_div_emp; END IF;

EXCEPTION
  WHEN OTHERS THEN
    IF c_dof%ISOPEN THEN CLOSE c_dof; END IF;
    IF c_pfj%ISOPEN THEN CLOSE c_pfj; END IF;
    IF c_mrc%ISOPEN THEN CLOSE c_mrc; END IF;
    IF c_div_emp%ISOPEN THEN CLOSE c_div_emp; END IF;
    RAISE;
END;

----------------------------------------------------------------------------------------------------
PROCEDURE gera_relatorio_erro
IS
  v_linha                 VARCHAR2(1000);
  v_posfinal     CONSTANT PLS_INTEGER   := 120;
BEGIN
  g_posfinal := v_posfinal;
  -- Inicio variaveis globais para utilizacao
  g_nome_relatorio := 'Relatório de Divergências de Consolidação de Documentos';
  g_cnt            := 0;
  g_reg_pagina     := 50;

  syn_out.inicializa (g_processo, g_reg_pagina+2, g_posfinal);

  r_inclui_header;

END;
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
PROCEDURE gera_relatorio_erro_filtros
IS
  v_linha                 VARCHAR2(1000);
  v_posfinal     CONSTANT PLS_INTEGER   := 120;
BEGIN
  g_posfinal := v_posfinal;
  -- Inicio variaveis globais para utilizacao
  g_nome_relatorio := 'Relatório de Divergências de Consolidação de Documentos - Filtros';
  g_cnt            := 0;
  g_reg_pagina     := 50;

  syn_out.inicializa (g_processo, g_reg_pagina+2, g_posfinal);

  r_inclui_header_filtros;

END;
----------------------------------------------------------------------------------------------------
PROCEDURE gera_relatorio_erro_colunas(
 p_colunas_erro  IN VARCHAR2
)
IS
  v_linha                 VARCHAR2(1000);
  v_posfinal     CONSTANT PLS_INTEGER   := 120;
BEGIN
  g_posfinal := v_posfinal;
  -- Inicio variaveis globais para utilizacao
  g_nome_relatorio := 'Relatório de Divergências de Consolidação de Documentos';
  g_cnt            := 0;
  g_reg_pagina     := 50;

  syn_out.inicializa (g_processo, g_reg_pagina+2, g_posfinal);

  r_inclui_header;

  r_put_line('Não foi possível realizar a transferência, existe inconsistência entre os bancos de dados!');
  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
  r_put_line(REPLACE(p_colunas_erro,'\n',CHR(13)||CHR(10) ));
  v_linha := RPAD('=',g_posfinal,'=');
  r_put_line(v_linha);
END;
----------------------------------------------------------------------------------------------------
BEGIN
  NULL;
END;