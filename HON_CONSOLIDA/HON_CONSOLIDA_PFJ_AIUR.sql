CREATE OR REPLACE TRIGGER HON_CONSOLIDA_PFJ_AIUR
AFTER INSERT  OR UPDATE
ON cor_pessoa
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink    VARCHAR2(30);
  v_schema    VARCHAR2(30);
  v_sql       VARCHAR2(32767);
  --
  v_count     integer;

PROCEDURE atualiza_origem_MAO is
begin
  v_count := 0;  
  select count(1)
    into v_count   
    from cor_pessoa@lk_mao
   where codigo_usual = :new.codigo_usual;       
  if v_count > 0                                
  then                                          
     cor_pessoa_cons_pkg.disable@lk_mao;
     commit;
     update cor_pessoa@lk_mao
        set hon_consolida_data   = sysdate,            
            hon_consolida_status = '1'                   
      where codigo_usual = :new.codigo_usual;        
     cor_pessoa_cons_pkg.enable@lk_mao; 
     commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'C');  
     commit;
     end;
  end if;                                        
  EXCEPTION WHEN OTHERS THEN                     
  rollback;
  cor_pessoa_cons_pkg.enable@lk_mao; 
  commit;
  RAISE;                                         
END;

PROCEDURE atualiza_origem_HAB is
begin
  v_count := 0;  
  select count(1)
    into v_count   
    from cor_pessoa@lk_hab
   where codigo_usual = :new.codigo_usual;       
  if v_count > 0                                
  then                                          
     cor_pessoa_cons_pkg.disable@lk_hab;
     commit;
     update cor_pessoa@lk_hab
        set hon_consolida_data   = sysdate,            
            hon_consolida_status = '1'                   
      where codigo_usual = :new.codigo_usual;        
     cor_pessoa_cons_pkg.enable@lk_hab; 
     commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'C');  
     commit;
     end;
  end if;                                        
  EXCEPTION WHEN OTHERS THEN                     
  rollback;
  cor_pessoa_cons_pkg.enable@lk_hab; 
  commit;
  RAISE;                                         
END;

PROCEDURE atualiza_origem_pecas is
begin
  v_count := 0;  
  select count(1)
    into v_count   
    from cor_pessoa@lk_pecas
   where codigo_usual = :new.codigo_usual;       
  if v_count > 0                                
  then                                          
     cor_pessoa_cons_pkg.disable@lk_pecas;
     commit;
     update cor_pessoa@lk_pecas
        set hon_consolida_data   = sysdate,            
            hon_consolida_status = '1'                   
      where codigo_usual = :new.codigo_usual;        
     cor_pessoa_cons_pkg.enable@lk_pecas; 
     commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'C');  
     commit;
     end;
  end if;                                        
  EXCEPTION WHEN OTHERS THEN                     
  rollback;
  cor_pessoa_cons_pkg.enable@lk_pecas; 
  commit;
  RAISE;                                         
END;

Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_MAO;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_HAB;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
--  HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA', 'C');  

End;

--ALTER TRIGGER HON_CONSOLIDA_PFJ_AIUR ENABLE;
