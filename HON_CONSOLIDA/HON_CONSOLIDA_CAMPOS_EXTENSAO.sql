----------------------------------------------------------------------------------------------------
-- SCRIPT: HON_CONSOLIDA_CAMPOS_EXTENSAO
--
-- Objetivo: Criar campos de extensão nas tabelas definitivas nas origens para indicar registros
--           já exportados e/ou status da exportação
--
-- Histórico:
--   27/03/2018 - MVS - Implementação
--
-- EXECUTAR NAS BASES DE ORIGEM: PECAS, HAB E FAB2W
--
----------------------------------------------------------------------------------------------------
-------------------------------------------------------------------
-- Origem PECAS - PECAS 
-------------------------------------------------------------------
-------------------------------------------------------------------
-- Origem HAB - HAB 
-------------------------------------------------------------------
-------------------------------------------------------------------
-- Origem FAB2W - MAO 
-------------------------------------------------------------------
SPOOL HON_CONSOLIDA_CAMPOS_EXTENSAO_ORIGEM.log

BEGIN
-- TABELAS CADASTRAIS
  -- COR_PESSOA                  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_PFJ_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264); 
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_PFJ_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_PFJ_I ON COR_PESSOA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
   -- COR_PESSOA_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA_VIGENCIA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA_VIGENCIA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD CONSTRAINT HON_CONSOL_STATUS_PFJVIG_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD CONSTRAINT HON_CONSOL_STATUS_PFJVIG_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_PFJVIG_I ON COR_PESSOA_VIGENCIA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_LOCALIDADE_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_PESSOA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_PESSOA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_LOCPFJ_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_LOCPFJ_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_LOCPFJ_I ON COR_LOCALIDADE_PESSOA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_LOCALIDADE_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_VIGENCIA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_VIGENCIA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD CONSTRAINT HON_CONSOL_STATUS_LOCVIG_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD CONSTRAINT HON_CONSOL_STATUS_LOCVIG_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_LOCVIG_I ON COR_LOCALIDADE_VIGENCIA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 

  -- COR_CLASSIFICACAO_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_PESSOA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_PESSOA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_CLAPFJ_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD CONSTRAINT HON_CONSOL_STATUS_CLAPFJ_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_CLAPFJ_I ON COR_CLASSIFICACAO_PESSOA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_MERCADORIA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_MERCADORIA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD CONSTRAINT HON_CONSOL_STATUS_MERC_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD CONSTRAINT HON_CONSOL_STATUS_MERC_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_MERC_I ON COR_MERCADORIA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_CLASSIFICACAO_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_MERCADORIA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_MERCADORIA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD CONSTRAINT HON_CONSOL_STATUS_CLAMERC_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD CONSTRAINT HON_CONSOL_STATUS_CLAMERC_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_CLAMERC_I ON COR_CLASSIFICACAO_MERCADORIA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_PRESTACAO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PRESTACAO.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PRESTACAO.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD CONSTRAINT HON_CONSOL_STATUS_PREST_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD CONSTRAINT HON_CONSOL_STATUS_PREST_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_PREST_I ON COR_PRESTACAO (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_SERVICO_ISS
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_SERVICO_ISS.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_SERVICO_ISS.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD CONSTRAINT HON_CONSOL_STATUS_SERV_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD CONSTRAINT HON_CONSOL_STATUS_SERV_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_SERV_I ON COR_SERVICO_ISS (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
-- DOCUMENTOS
  cor_dof_cons_pkg.DISABLE;
  cor_idf_cons_pkg.DISABLE;
  cor_dof_parcela_cons_pkg.DISABLE;
  commit;    

  -- COR_DOF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD CONSTRAINT HON_CONSOL_STATUS_DOF_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD CONSTRAINT HON_CONSOL_STATUS_DOF_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_DOF_I ON COR_DOF (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_IDF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_IDF.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_IDF.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD CONSTRAINT HON_CONSOL_STATUS_IDF_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD CONSTRAINT HON_CONSOL_STATUS_IDF_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_IDF_I ON COR_IDF (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_DOF_ASSOCIADO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_ASSOCIADO.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_ASSOCIADO.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD CONSTRAINT HON_CONSOL_STATUS_DOFASS_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD CONSTRAINT HON_CONSOL_STATUS_DOFASS_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_DOFASS_I ON COR_DOF_ASSOCIADO (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  -- COR_DOF_PARCELA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD (HON_CONSOLIDA_DATA DATE)',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_PARCELA.HON_CONSOLIDA_DATA IS ''Data de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD (HON_CONSOLIDA_STATUS VARCHAR2(1))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_PARCELA.HON_CONSOLIDA_STATUS IS ''Status de consolidação da informação.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD CONSTRAINT HON_CONSOL_STATUS_DOFPARC_CHK CHECK (HON_CONSOLIDA_STATUS IN (''0'', ''1''))' ,-2264);  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA MODIFY (HON_CONSOLIDA_STATUS DEFAULT ''0'')',-1430);
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD CONSTRAINT HON_CONSOL_STATUS_DOFPARC_DFLT CHECK (HON_CONSOLIDA_STATUS IS NOT NULL) ENABLE NOVALIDATE' ,-2264); 
  SYN_DYNAMIC.DDL('CREATE INDEX HON_CONSOLIDA_STATUS_DOFPARC_I ON COR_DOF_PARCELA (HON_CONSOLIDA_STATUS) NOLOGGING TABLESPACE &&TBSPC_INDEX',-955); 
  
  --
  cor_dof_cons_pkg.enable;
  cor_idf_cons_pkg.enable;
  cor_dof_parcela_cons_pkg.enable;
  commit;    
  
END;
/
------------------------------------------------
----- RECRIA API E RECARREGA REPOSITORIO -------
------------------------------------------------
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PRESTACAO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PRESTACAO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_SERVICO_ISS');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_SERVICO_ISS';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_IDF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_IDF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_ASSOCIADO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_ASSOCIADO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_PARCELA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_PARCELA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/

SPOOL OFF

----------------------------------------------------------------------------------------------------
-- SCRIPT: HON_CONSOLIDA_CAMPOS_EXTENSAO
--
-- Objetivo: Criar campo de extensão nas tabelas definitivas no destino para indicar origem dos registros
--           exportados 
--
-- Histórico:
--   02/04/2018 - MVS - Implementação
--
-- EXECUTAR NA BASE DE DESTINO: SAP4W 
--
----------------------------------------------------------------------------------------------------
SPOOL HON_CONSOLIDA_CAMPOS_EXTENSAO_DESTINO.log

BEGIN
-- TABELAS CADASTRAIS
  -- COR_PESSOA                  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_PFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_PESSOA_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA_VIGENCIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD CONSTRAINT HON_CONSOL_ORIGEM_PFJVIG_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_LOCALIDADE_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_LOCPFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_LOCALIDADE_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_VIGENCIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD CONSTRAINT HON_CONSOL_ORIGEM_LOCVIG_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  

  -- COR_CLASSIFICACAO_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_CLAPFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_MERCADORIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD CONSTRAINT HON_CONSOL_ORIGEM_MERC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_CLASSIFICACAO_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_MERCADORIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD CONSTRAINT HON_CONSOL_ORIGEM_CLAMERC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_PRESTACAO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PRESTACAO.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD CONSTRAINT HON_CONSOL_ORIGEM_PREST_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_SERVICO_ISS
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_SERVICO_ISS.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD CONSTRAINT HON_CONSOL_ORIGEM_SERV_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
-- DOCUMENTOS
  cor_dof_cons_pkg.DISABLE;
  cor_idf_cons_pkg.DISABLE;
  cor_dof_parcela_cons_pkg.DISABLE;
  commit;    

  -- COR_DOF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD CONSTRAINT HON_CONSOL_ORIGEM_DOF_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_IDF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_IDF.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD CONSTRAINT HON_CONSOL_ORIGEM_IDF_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_DOF_ASSOCIADO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_ASSOCIADO.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD CONSTRAINT HON_CONSOL_ORIGEM_DOFASS_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_DOF_PARCELA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_PARCELA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD CONSTRAINT HON_CONSOL_ORIGEM_DOFPARC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  --
  cor_dof_cons_pkg.enable;
  cor_idf_cons_pkg.enable;
  cor_dof_parcela_cons_pkg.enable;
  commit;    
  
END;
/
------------------------------------------------
----- RECRIA API E RECARREGA REPOSITORIO -------
------------------------------------------------
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PRESTACAO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PRESTACAO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_SERVICO_ISS');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_SERVICO_ISS';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_IDF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_IDF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_ASSOCIADO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_ASSOCIADO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_PARCELA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_PARCELA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/

SPOOL OFF