----------------------------------------------------------------------------------------------------
-- SCRIPT: HON_CONSOLIDA_CAMPOS_EXTENSAO
--
-- Objetivo: Criar campo de extensão nas tabelas definitivas no destino para indicar origem dos registros
--           exportados 
--
-- Histórico:
--   02/04/2018 - MVS - Implementação
--
-- EXECUTAR NA BASE DE DESTINO: SAP4W 
--
----------------------------------------------------------------------------------------------------
SPOOL HON_CONSOLIDA_CAMPOS_EXTENSAO_DESTINO.log

BEGIN
-- TABELAS CADASTRAIS
  -- COR_PESSOA                  
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_PFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_PESSOA_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PESSOA_VIGENCIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PESSOA_VIGENCIA ADD CONSTRAINT HON_CONSOL_ORIGEM_PFJVIG_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_LOCALIDADE_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_LOCPFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_LOCALIDADE_VIGENCIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_LOCALIDADE_VIGENCIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_LOCALIDADE_VIGENCIA ADD CONSTRAINT HON_CONSOL_ORIGEM_LOCVIG_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  

  -- COR_CLASSIFICACAO_PESSOA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_PESSOA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_PESSOA ADD CONSTRAINT HON_CONSOL_ORIGEM_CLAPFJ_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_MERCADORIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_MERCADORIA ADD CONSTRAINT HON_CONSOL_ORIGEM_MERC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_CLASSIFICACAO_MERCADORIA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_CLASSIFICACAO_MERCADORIA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_CLASSIFICACAO_MERCADORIA ADD CONSTRAINT HON_CONSOL_ORIGEM_CLAMERC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_PRESTACAO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_PRESTACAO.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_PRESTACAO ADD CONSTRAINT HON_CONSOL_ORIGEM_PREST_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_SERVICO_ISS
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_SERVICO_ISS.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_SERVICO_ISS ADD CONSTRAINT HON_CONSOL_ORIGEM_SERV_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
-- DOCUMENTOS
  cor_dof_cons_pkg.DISABLE;
  cor_idf_cons_pkg.DISABLE;
  cor_dof_parcela_cons_pkg.DISABLE;
  commit;    

  -- COR_DOF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF ADD CONSTRAINT HON_CONSOL_ORIGEM_DOF_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_IDF
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_IDF.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_IDF ADD CONSTRAINT HON_CONSOL_ORIGEM_IDF_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_DOF_ASSOCIADO
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_ASSOCIADO.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_ASSOCIADO ADD CONSTRAINT HON_CONSOL_ORIGEM_DOFASS_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  -- COR_DOF_PARCELA
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD (HON_CONSOLIDA_ORIGEM VARCHAR2(5))',-1430);
  SYN_DYNAMIC.DDL('COMMENT ON COLUMN COR_DOF_PARCELA.HON_CONSOLIDA_ORIGEM IS ''Origem da informação consolidada.''');
  SYN_DYNAMIC.DDL('ALTER TABLE COR_DOF_PARCELA ADD CONSTRAINT HON_CONSOL_ORIGEM_DOFPARC_CHK CHECK (HON_CONSOLIDA_ORIGEM IN (''MAO'', ''HAB'', ''PECAS''))' ,-2264);  
  
  --
  cor_dof_cons_pkg.enable;
  cor_idf_cons_pkg.enable;
  cor_dof_parcela_cons_pkg.enable;
  commit;    
  
END;
/
------------------------------------------------
----- RECRIA API E RECARREGA REPOSITORIO -------
------------------------------------------------
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PESSOA_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PESSOA_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_LOCALIDADE_VIGENCIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_LOCALIDADE_VIGENCIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_PESSOA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_PESSOA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_CLASSIFICACAO_MERCADORIA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_CLASSIFICACAO_MERCADORIA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_PRESTACAO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_PRESTACAO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_SERVICO_ISS');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_SERVICO_ISS';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_IDF');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_IDF';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_ASSOCIADO');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_ASSOCIADO';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/
BEGIN
    DIC_CARGA_REPOSITORIO ('COR_DOF_PARCELA');
END;
/
DECLARE
  API_TBL VARCHAR2(32);
BEGIN
  SELECT TABELA INTO API_TBL 
  FROM DIC_API_X_TABELA 
  WHERE TABELA = 'COR_DOF_PARCELA';

  SYN_RECRIA_API (API_TBL);
EXCEPTION WHEN NO_DATA_FOUND THEN
  NULL;
END;
/

SPOOL OFF