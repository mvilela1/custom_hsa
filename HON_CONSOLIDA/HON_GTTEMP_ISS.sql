--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HON_GTTEMP_ISS
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "FAB4W"."HON_GTTEMP_ISS" 
   (	"ALTERADO_EM" DATE, 
	"ALTERADO_POR" VARCHAR2(30 BYTE), 
	"CBO" VARCHAR2(10 CHAR), 
	"CHAVE_ORIGEM" VARCHAR2(255 BYTE), 
	"CLASSIF_CODIGO" NUMBER(9,0), 
	"CODRET_COP" VARCHAR2(6 BYTE), 
	"CODRET_FIS" VARCHAR2(6 BYTE), 
	"CODRET_JUR" VARCHAR2(6 BYTE), 
	"DESCRICAO" VARCHAR2(255 CHAR), 
	"DFLT_UNI_CODIGO" VARCHAR2(6 CHAR), 
	"HON_CONSOLIDA_ORIGEM" VARCHAR2(5 BYTE), 
	"IND_LC116" VARCHAR2(1 BYTE), 
	"IND_RESP_IRRF" VARCHAR2(1 CHAR), 
	"NBS_CODIGO" VARCHAR2(20 BYTE), 
	"NOME" VARCHAR2(80 CHAR), 
	"ORIGEM" VARCHAR2(30 BYTE), 
	"REVISAO" NUMBER(10,0), 
	"SISS_CODIGO" VARCHAR2(30 CHAR), 
	"SISS_CODIGO_LC116" VARCHAR2(30 BYTE), 
	"TIPO_SERVICO" VARCHAR2(1 CHAR)
   ) ON COMMIT PRESERVE ROWS ;
--------------------------------------------------------
--  DDL for Index HON_GTTEMP_ISS_I
--------------------------------------------------------

  CREATE INDEX "FAB4W"."HON_GTTEMP_ISS_I" ON "FAB4W"."HON_GTTEMP_ISS" ("SISS_CODIGO") ;
--------------------------------------------------------
--  Constraints for Table HON_GTTEMP_ISS
--------------------------------------------------------

  ALTER TABLE "FAB4W"."HON_GTTEMP_ISS" MODIFY ("IND_RESP_IRRF" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_ISS" MODIFY ("NOME" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_ISS" MODIFY ("SISS_CODIGO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_ISS" MODIFY ("TIPO_SERVICO" NOT NULL ENABLE);
