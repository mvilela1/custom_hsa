--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HON_GTTEMP_PREST
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "FAB4W"."HON_GTTEMP_PREST" 
   (	"CHAVE_ORIGEM" VARCHAR2(255 BYTE), 
	"CLASSIF_CODIGO" NUMBER(9,0), 
	"DESCRICAO" VARCHAR2(255 CHAR), 
	"HON_CONSOLIDA_ORIGEM" VARCHAR2(5 BYTE), 
	"NBS_CODIGO" VARCHAR2(20 BYTE), 
	"NOME" VARCHAR2(80 CHAR), 
	"ORIGEM" VARCHAR2(30 BYTE), 
	"PRES_CODIGO" VARCHAR2(30 CHAR), 
	"REVISAO" NUMBER(10,0)
   ) ON COMMIT PRESERVE ROWS ;
--------------------------------------------------------
--  DDL for Index HON_GTTEMP_PREST_I
--------------------------------------------------------

  CREATE INDEX "FAB4W"."HON_GTTEMP_PREST_I" ON "FAB4W"."HON_GTTEMP_PREST" ("PRES_CODIGO") ;
--------------------------------------------------------
--  Constraints for Table HON_GTTEMP_PREST
--------------------------------------------------------

  ALTER TABLE "FAB4W"."HON_GTTEMP_PREST" MODIFY ("NOME" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_PREST" MODIFY ("PRES_CODIGO" NOT NULL ENABLE);
