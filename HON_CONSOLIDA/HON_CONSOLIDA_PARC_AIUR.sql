CREATE OR REPLACE TRIGGER HON_CONSOLIDA_PARC_AIUR 
AFTER INSERT  OR UPDATE
ON cor_dof_parcela
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink         VARCHAR2(30);
  v_schema         VARCHAR2(30);
  v_sql            VARCHAR2(32767);
  --
  v_count          integer;                               
  v_estab          cor_dof.informante_est_codigo%TYPE;    
  v_dof_import     cor_dof.dof_import_numero%TYPE;        
  v_codigo_site    cor_dof.codigo_do_site%TYPE;           
  v_dof_sequence   cor_dof.dof_sequence%TYPE;             


PROCEDURE atualiza_origem_mao is
begin
   v_count := 0;             
   v_codigo_site := 1;       
   begin                     
      select informante_est_codigo,   
             dof_import_numero               
        into v_estab,                   
             v_dof_import                    
        from cor_dof                    
       where codigo_do_site = :new.codigo_do_site  
         and dof_sequence   = :new.dof_sequence;     
   exception when no_data_found then null;                                       
   end;                                        
   begin                                       
      select codigo_do_site,                      
             dof_sequence                                
        into v_codigo_site,                         
             v_dof_sequence                              
        from cor_dof@lk_mao 
       where informante_est_codigo = v_estab       
         and dof_import_numero     = v_dof_import;   
   exception when no_data_found then null;                                       
   end;                                        
   select count(1)                             
     into v_count                                
     from cor_dof_parcela@lk_mao  
    where codigo_do_site = v_codigo_site                 
      and dof_sequence   = v_dof_sequence                  
      and num_parcela    = :new.num_parcela;               
   if v_count > 0                                       
   then                                                 
      cor_dof_parcela_cons_pkg.disable@lk_mao; 
      update cor_dof_parcela@lk_mao 
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                      
       where codigo_do_site = v_codigo_site              
         and dof_sequence   = v_dof_sequence               
         and num_parcela    = :new.num_parcela;            
      cor_dof_parcela_cons_pkg.enable@lk_mao; 
      commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION                                            
WHEN OTHERS THEN                                     
rollback;
RAISE;                                               
END;

PROCEDURE atualiza_origem_hab is
begin
   v_count := 0;             
   v_codigo_site := 1;       
   begin                     
      select informante_est_codigo,   
             dof_import_numero               
        into v_estab,                   
             v_dof_import                    
        from cor_dof                    
       where codigo_do_site = :new.codigo_do_site  
         and dof_sequence   = :new.dof_sequence;     
   exception when no_data_found then null;                                       
   end;                                        
   begin                                       
      select codigo_do_site,                      
             dof_sequence                                
        into v_codigo_site,                         
             v_dof_sequence                              
        from cor_dof@lk_hab 
       where informante_est_codigo = v_estab       
         and dof_import_numero     = v_dof_import;   
   exception when no_data_found then null;                                       
   end;                                        
   select count(1)                             
     into v_count                                
     from cor_dof_parcela@lk_hab
    where codigo_do_site = v_codigo_site                 
      and dof_sequence   = v_dof_sequence                  
      and num_parcela    = :new.num_parcela;               
   if v_count > 0                                       
   then                                                 
      cor_dof_parcela_cons_pkg.disable@lk_hab; 
      update cor_dof_parcela@lk_hab 
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                      
       where codigo_do_site = v_codigo_site              
         and dof_sequence   = v_dof_sequence               
         and num_parcela    = :new.num_parcela;            
      cor_dof_parcela_cons_pkg.enable@lk_hab;  
	  commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION                                            
WHEN OTHERS THEN                                     
rollback;
RAISE;                                               
END;

PROCEDURE atualiza_origem_pecas is
begin
   v_count := 0;             
   v_codigo_site := 1;       
   begin                     
      select informante_est_codigo,   
             dof_import_numero               
        into v_estab,                   
             v_dof_import                    
        from cor_dof                    
       where codigo_do_site = :new.codigo_do_site  
         and dof_sequence   = :new.dof_sequence;     
   exception when no_data_found then null;                                       
   end;                                        
   begin                                       
      select codigo_do_site,                      
             dof_sequence                                
        into v_codigo_site,                         
             v_dof_sequence                              
        from cor_dof@lk_pecas 
       where informante_est_codigo = v_estab       
         and dof_import_numero     = v_dof_import;   
   exception when no_data_found then null;                                       
   end;                                        
   select count(1)                             
     into v_count                                
     from cor_dof_parcela@lk_pecas
    where codigo_do_site = v_codigo_site                 
      and dof_sequence   = v_dof_sequence                  
      and num_parcela    = :new.num_parcela;               
   if v_count > 0                                       
   then                                                 
      cor_dof_parcela_cons_pkg.disable@lk_pecas; 
      update cor_dof_parcela@lk_pecas 
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                      
       where codigo_do_site = v_codigo_site              
         and dof_sequence   = v_dof_sequence               
         and num_parcela    = :new.num_parcela;            
      cor_dof_parcela_cons_pkg.enable@lk_pecas;  
	  commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION                                            
WHEN OTHERS THEN                                     
rollback;
RAISE;                                               
END;

Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_mao;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_hab;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
  -- HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF_PARCELA', 'C');  

End;

--ALTER TRIGGER HON_CONSOLIDA_PARC_AIUR ENABLE;
