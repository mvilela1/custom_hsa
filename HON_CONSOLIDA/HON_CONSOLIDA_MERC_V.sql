--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View HON_CONSOLIDA_MERC_V
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FAB4W"."HON_CONSOLIDA_MERC_V" ("AGLU_CLASSE_STF_MVA_CODIGO", "AGLU_STF_PAUTA_CODIGO", "ALTERADO_EM", "ALTERADO_POR", "CEST_CODIGO", "CHAVE_ORIGEM", "CODIGO_COMBUSTIVEIS_SOLVENTES", "CODIGO_USUAL", "COD_ANT_ITEM", "COD_BARRA", "COD_COMB", "COD_GTIN", "COD_PROPRIO", "DEPART_CODIGO", "DESCRICAO", "DFLT_AM_CODIGO_ENTRADA", "DFLT_AM_CODIGO_SAIDA", "DFLT_NBM_CODIGO", "DFLT_OM_CODIGO", "DFLT_PRECO_UNITARIO_SAIDA", "DFLT_UNI_CODIGO_ENTRADA", "DFLT_UNI_CODIGO_ESTOQUE", "DFLT_UNI_CODIGO_SAIDA", "EXEMPLO_PARA_MANUAL", "FAMILIA_CODIGO", "GRUPO_CODIGO", "IND_SIMILAR_NACIONAL", "ITEM_COLETADO_ANP", "MAPA_FISIOGRAFICO", "MERCADOLOGICO", "MERC_CODIGO", "NOME", "ORIGEM", "REVISAO", "SELO_IPI_CODIGO", "SETOR_CODIGO", "SUBFAMILIA_CODIGO", "TESTE", "TESTE1", "TIPO_ITEM", "HON_CONSOLIDA_ORIGEM") AS 
  SELECT 
AGLU_CLASSE_STF_MVA_CODIGO,
AGLU_STF_PAUTA_CODIGO,
ALTERADO_EM,
ALTERADO_POR,
CEST_CODIGO,
CHAVE_ORIGEM,
CODIGO_COMBUSTIVEIS_SOLVENTES,
CODIGO_USUAL,
COD_ANT_ITEM,
COD_BARRA,
COD_COMB,
COD_GTIN,
COD_PROPRIO,
DEPART_CODIGO,
DESCRICAO,
DFLT_AM_CODIGO_ENTRADA,
DFLT_AM_CODIGO_SAIDA,
DFLT_NBM_CODIGO,
DFLT_OM_CODIGO,
DFLT_PRECO_UNITARIO_SAIDA,
DFLT_UNI_CODIGO_ENTRADA,
DFLT_UNI_CODIGO_ESTOQUE,
DFLT_UNI_CODIGO_SAIDA,
EXEMPLO_PARA_MANUAL,
FAMILIA_CODIGO,
GRUPO_CODIGO,
IND_SIMILAR_NACIONAL,
ITEM_COLETADO_ANP,
MAPA_FISIOGRAFICO,
MERCADOLOGICO,
MERC_CODIGO,
NOME,
ORIGEM,
REVISAO,
SELO_IPI_CODIGO,
SETOR_CODIGO,
SUBFAMILIA_CODIGO,
TESTE,
TESTE1,
TIPO_ITEM,
'HAB' HON_CONSOLIDA_ORIGEM
FROM COR_MERCADORIA@lk_hab
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
UNION ALL
SELECT 
AGLU_CLASSE_STF_MVA_CODIGO,
AGLU_STF_PAUTA_CODIGO,
ALTERADO_EM,
ALTERADO_POR,
CEST_CODIGO,
CHAVE_ORIGEM,
CODIGO_COMBUSTIVEIS_SOLVENTES,
CODIGO_USUAL,
COD_ANT_ITEM,
COD_BARRA,
COD_COMB,
COD_GTIN,
COD_PROPRIO,
DEPART_CODIGO,
DESCRICAO,
DFLT_AM_CODIGO_ENTRADA,
DFLT_AM_CODIGO_SAIDA,
DFLT_NBM_CODIGO,
DFLT_OM_CODIGO,
DFLT_PRECO_UNITARIO_SAIDA,
DFLT_UNI_CODIGO_ENTRADA,
DFLT_UNI_CODIGO_ESTOQUE,
DFLT_UNI_CODIGO_SAIDA,
EXEMPLO_PARA_MANUAL,
FAMILIA_CODIGO,
GRUPO_CODIGO,
IND_SIMILAR_NACIONAL,
ITEM_COLETADO_ANP,
MAPA_FISIOGRAFICO,
MERCADOLOGICO,
MERC_CODIGO,
NOME,
ORIGEM,
REVISAO,
SELO_IPI_CODIGO,
SETOR_CODIGO,
SUBFAMILIA_CODIGO,
TESTE,
TESTE1,
TIPO_ITEM,
'MAO' HON_CONSOLIDA_ORIGEM
FROM COR_MERCADORIA@lk_mao
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
UNION ALL
SELECT 
AGLU_CLASSE_STF_MVA_CODIGO,
AGLU_STF_PAUTA_CODIGO,
ALTERADO_EM,
ALTERADO_POR,
CEST_CODIGO,
CHAVE_ORIGEM,
CODIGO_COMBUSTIVEIS_SOLVENTES,
CODIGO_USUAL,
COD_ANT_ITEM,
COD_BARRA,
COD_COMB,
COD_GTIN,
COD_PROPRIO,
DEPART_CODIGO,
DESCRICAO,
DFLT_AM_CODIGO_ENTRADA,
DFLT_AM_CODIGO_SAIDA,
DFLT_NBM_CODIGO,
DFLT_OM_CODIGO,
DFLT_PRECO_UNITARIO_SAIDA,
DFLT_UNI_CODIGO_ENTRADA,
DFLT_UNI_CODIGO_ESTOQUE,
DFLT_UNI_CODIGO_SAIDA,
EXEMPLO_PARA_MANUAL,
FAMILIA_CODIGO,
GRUPO_CODIGO,
IND_SIMILAR_NACIONAL,
ITEM_COLETADO_ANP,
MAPA_FISIOGRAFICO,
MERCADOLOGICO,
MERC_CODIGO,
NOME,
ORIGEM,
REVISAO,
SELO_IPI_CODIGO,
SETOR_CODIGO,
SUBFAMILIA_CODIGO,
TESTE,
TESTE1,
TIPO_ITEM,
'PECAS' HON_CONSOLIDA_ORIGEM
FROM COR_MERCADORIA@lk_pecas
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
;
