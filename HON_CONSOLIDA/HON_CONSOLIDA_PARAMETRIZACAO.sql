----------------------------------------------------------------------------------------------------
-- SCRIPT: HON_CONSOLIDA_PARAMETRIZACAO
--
-- Objetivo: Criar parametros para execução da consolidação de documentos na base SAP4W
--           Este script deve ser executado somente na base SAP4W
--
-- Histórico:
--   27/03/2018 - MVS - Implementação
--
--
-- EXECUTAR NA BASE DE DESTINO: SAP4W
--
----------------------------------------------------------------------------------------------------
-------------------------------------------------------------------
-- Parametro "HON_DBLINK_HAB" 
-------------------------------------------------------------------
Declare
  mcount integer;
  i      integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_DBLINK_HAB';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_DBLINK_HAB'
     ,'HONDA: Nome do DBLink que será utilizado para buscar informações da base HAB.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do DBLink que será utilizado para buscar informações da base HAB.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_DBLINK_HAB';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_DBLINK_HAB';
  i := 1;
  for x in (select db_link, username from all_db_links) 
  loop
     Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
	 values ('HON_DBLINK_HAB'
             ,x.db_link
             ,'Nome do DBLink com Username: ' || x.username
             ,i);
     i := i + 1;  
     Commit;
  end loop;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_DBLINK_MAO" 
-------------------------------------------------------------------
Declare
  mcount integer;
  i      integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_DBLINK_MAO';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_DBLINK_MAO'
     ,'HONDA: Nome do DBLink que será utilizado para buscar informações da base MAO.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do DBLink que será utilizado para buscar informações da base MAO.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_DBLINK_MAO';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_DBLINK_MAO';
  i := 1;
  for x in (select db_link, username from all_db_links) 
  loop
     Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
	 values ('HON_DBLINK_MAO'
             ,x.db_link
             ,'Nome do DBLink com Username: ' || x.username
             ,i);
     i := i + 1;  
     Commit;
  end loop;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_DBLINK_PECAS" 
-------------------------------------------------------------------
Declare
  mcount integer;
  i      integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_DBLINK_PECAS';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_DBLINK_PECAS'
     ,'HONDA: Nome do DBLink que será utilizado para buscar informações da base PECAS.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do DBLink que será utilizado para buscar informações da base PECAS.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_DBLINK_PECAS';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_DBLINK_PECAS';
  i := 1;
  for x in (select db_link, username from all_db_links) 
  loop
     Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
	 values ('HON_DBLINK_PECAS'
             ,x.db_link
             ,'Nome do DBLink com Username: ' || x.username
             ,i);
     i := i + 1;  
     Commit;
  end loop;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_SCHEMA_HAB" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_SCHEMA_HAB';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_SCHEMA_HAB'
     ,'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base HAB.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base HAB.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_SCHEMA_HAB';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_SCHEMA_HAB';
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_HAB','FAB4W','Usuario de banco de dados da base FAB4W - HAB',1);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_HAB','FAB2W','Usuario de banco de dados da base FAB2W - MAO',2);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_HAB','PECAS','Usuario de banco de dados da base PECAS - PECAS',3);
  Commit;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_SCHEMA_MAO" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_SCHEMA_MAO';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_SCHEMA_MAO'
     ,'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base MAO.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base MAO.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_SCHEMA_MAO';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_SCHEMA_MAO';
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_MAO','FAB4W','Usuario de banco de dados da base FAB4W - HAB',1);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_MAO','FAB2W','Usuario de banco de dados da base FAB2W - MAO',2);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_MAO','PECAS','Usuario de banco de dados da base PECAS - PECAS',3);
  Commit;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_SCHEMA_PECAS" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_SCHEMA_PECAS';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_SCHEMA_PECAS'
     ,'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base PECAS.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Nome do usuário de banco de dados que será utilizado para buscar informações da base PECAS.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_SCHEMA_PECAS';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_SCHEMA_PECAS';
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_PECAS','FAB4W','Usuario de banco de dados da base FAB4W - HAB',1);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_PECAS','FAB2W','Usuario de banco de dados da base FAB2W - MAO',2);
  Insert into syn_lista_valores(lista_tipo,lista_valor,descricao,sequencia) 
  values ('HON_SCHEMA_PECAS','PECAS','Usuario de banco de dados da base PECAS - PECAS',3);
  Commit;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_ESTAB_HAB" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_ESTAB_HAB';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_ESTAB_HAB'
     ,'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base HAB.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base HAB.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_ESTAB_HAB';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_ESTAB_HAB';
  Commit;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_ESTAB_MAO" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_ESTAB_MAO';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_ESTAB_MAO'
     ,'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base MAO.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base MAO.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_ESTAB_MAO';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_ESTAB_MAO';
  Commit;
End;
/

-------------------------------------------------------------------
-- Parametro "HON_ESTAB_PECAS" 
-------------------------------------------------------------------
Declare
  mcount integer;
Begin
  Select count(*) into mcount from syn_parametros where par_codigo = 'HON_ESTAB_PECAS';
  If mcount = 0 Then
     Insert Into syn_parametros(par_codigo,definicao,valor,valor_default,tipo,ativo,nivel_estabelecimento) values (
      'HON_ESTAB_PECAS'
     ,'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base PECAS.'
     ,null
     ,null
     ,'VARCHAR2'
     ,'S'
     ,'N');
  Else
     Update syn_parametros set 
      definicao = 'HONDA: Estabelecimentos utilizados na consolidação de dados de outras bases. Referente à base PECAS.'
     ,valor_default = null
     ,tipo = 'VARCHAR2'
     ,ativo = 'S'
     ,nivel_estabelecimento = 'N'
     where par_codigo = 'HON_ESTAB_PECAS';
  End If;
  Delete from syn_lista_valores where lista_tipo = 'HON_ESTAB_PECAS';
  Commit;
End;
/
