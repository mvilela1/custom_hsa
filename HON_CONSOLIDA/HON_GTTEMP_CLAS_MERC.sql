--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HON_GTTEMP_CLAS_MERC
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" 
   (	"AGLU_CLASSE_STF_MVA_CODIGO" VARCHAR2(10 BYTE), 
	"CMERC_CODIGO" VARCHAR2(6 CHAR), 
	"DT_FIM" DATE, 
	"DT_INICIO" DATE, 
	"ELEMENTO" VARCHAR2(60 CHAR), 
	"HON_CONSOLIDA_ORIGEM" VARCHAR2(5 BYTE), 
	"ID" NUMBER(15,0), 
	"IND_AGLUTINADOR" VARCHAR2(1 BYTE), 
	"MERC_CODIGO" VARCHAR2(60 BYTE), 
	"UF_CODIGO" VARCHAR2(2 CHAR)
   ) ON COMMIT PRESERVE ROWS ;
--------------------------------------------------------
--  DDL for Index HON_GTTEMP_CLAS_MERC_I
--------------------------------------------------------

  CREATE INDEX "FAB4W"."HON_GTTEMP_CLAS_MERC_I" ON "FAB4W"."HON_GTTEMP_CLAS_MERC" ("MERC_CODIGO", "UF_CODIGO", "CMERC_CODIGO", "ELEMENTO", "DT_INICIO") ;
--------------------------------------------------------
--  Constraints for Table HON_GTTEMP_CLAS_MERC
--------------------------------------------------------

  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" MODIFY ("CMERC_CODIGO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" MODIFY ("DT_INICIO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" MODIFY ("ELEMENTO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_MERC" MODIFY ("MERC_CODIGO" NOT NULL ENABLE);
