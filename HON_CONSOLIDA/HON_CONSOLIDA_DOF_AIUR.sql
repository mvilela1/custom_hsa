CREATE OR REPLACE TRIGGER HON_CONSOLIDA_DOF_AIUR
AFTER INSERT  OR UPDATE
ON cor_dof
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink    VARCHAR2(30);
  v_schema    VARCHAR2(30);
  v_sql       VARCHAR2(32767);
  --
  v_count     integer;

PROCEDURE atualiza_origem_mao is
begin
  v_count := 0;   
begin
  select count(1)   
    into v_count      
    from cor_dof@lk_mao 
   where informante_est_codigo = :new.informante_est_codigo 
     and dof_import_numero     = :new.dof_import_numero;      
exception when OTHERS THEN null; 
end;
  if v_count > 0                                           
  then                                                     
     cor_dof_cons_pkg.disable@lk_mao; 
     commit;
     update cor_dof@lk_mao             
        set hon_consolida_data   = sysdate,                 
            hon_consolida_status = '1'                       
          where informante_est_codigo = :new.informante_est_codigo 
            and dof_import_numero     = :new.dof_import_numero;    
     cor_dof_cons_pkg.enable@lk_mao; 
     commit;	
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'C');  
     commit;
     end;
  end if;                                                
EXCEPTION WHEN OTHERS THEN                             
  rollback;
  RAISE;                                                 
END;

PROCEDURE atualiza_origem_hab is
begin
  v_count := 0;     
  select count(1)   
    into v_count      
    from cor_dof@lk_hab
   where informante_est_codigo = :new.informante_est_codigo 
     and dof_import_numero     = :new.dof_import_numero;      
  if v_count > 0                                           
  then                                                     
     cor_dof_cons_pkg.disable@lk_hab;   
     commit;
     update cor_dof@lk_hab             
        set hon_consolida_data   = sysdate,                 
            hon_consolida_status = '1'                       
          where informante_est_codigo = :new.informante_est_codigo 
            and dof_import_numero     = :new.dof_import_numero;    
     cor_dof_cons_pkg.enable@lk_hab;            
	 commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'C');  
     commit;
     end;
  end if;                                                
EXCEPTION WHEN OTHERS THEN                             
  rollback;
  RAISE;                                                 
END;

PROCEDURE atualiza_origem_pecas is
begin
  v_count := 0;     
  select count(1)   
    into v_count      
    from cor_dof@lk_pecas
   where informante_est_codigo = :new.informante_est_codigo 
     and dof_import_numero     = :new.dof_import_numero;      
  if v_count > 0                                           
  then                                                     
     cor_dof_cons_pkg.disable@lk_pecas; 
     commit;
     update cor_dof@lk_pecas             
        set hon_consolida_data   = sysdate,                 
            hon_consolida_status = '1'                       
          where informante_est_codigo = :new.informante_est_codigo 
            and dof_import_numero     = :new.dof_import_numero;    
     cor_dof_cons_pkg.enable@lk_pecas;            
	 commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'C');  
     commit;
     end;
  end if;                                                
EXCEPTION WHEN OTHERS THEN                             
  rollback;
  RAISE;                                                 
END;

Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_mao;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_hab;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
  -- HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_DOF', 'C');  

End;

--ALTER TRIGGER HON_CONSOLIDA_DOF_AIUR ENABLE;
