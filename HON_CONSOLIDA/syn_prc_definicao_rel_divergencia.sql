--
-- Importando Definicao 'UTI\ITF\PRC\PRC\HON_CONSOLIDA_REL' ...
--
DECLARE
  mid syn_prc_definicao.id%type;
  mprcdef_prcdef_id syn_prc_definicao.prcdef_id%type := null;
  mprcdef_frm_id syn_prc_definicao.frm_id%type := null;
BEGIN
  Select id into mprcdef_prcdef_id from syn_prc_definicao where codigo = 'UTI\ITF\PRC\PRC\';
Begin
--************************************************************************
--* ATEN??O -- Se voc? alterou o c?digo da funcionalidade ou processo    *
--* no select abaixo voc? deve procurar pelo c?digo antigo para que o    *
--* script possa ser executado mais de uma vez e atualize a defini??o    *
--* ao inv?s de criar uma nova!!!                                        *
--************************************************************************
  Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO';
  Update SYN_PRC_DEFINICAO set
  CODIGO              = 'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO'
  ,NOME                = 'Relatorio de Divergencia de Consolidacao - Filtros'
  ,TITULO              = 'Relatorio de Divergencia de Consolidacao - Filtros'
  ,FUN_TIPO            = 'F_FUNC'
  ,PRCDEF_ID           = mprcdef_prcdef_id
  ,NIVEL               = 5
  ,EXECUTAVEL_NOME     = ''
  ,ROTINA_DE_GERACAO   = 'HON_CONSOLIDA_REL.copia_dof_filtros'
  ,CTRL_TIPO_PROCESSO  = 'R'
  ,ORDEM               = 3
  ,CALC_ORDEM          = 138
  ,ICONE_NUMERO        = 0
  ,NOME_DO_FONTE       = 'Courier New'
  ,TAMANHO_DO_FONTE    = 3
  ,FONTE_NEGRITO       = 'N'
  ,IMPRESSORA          = ''
  ,ORIENTACAO          = 1
  ,FRM_ID              = mprcdef_frm_id
  ,IND_DEF_USUARIO     = 'S'
  ,IND_DESATIVADO      = 'N'
  Where ID = mid;
  Commit;
Exception When NO_DATA_FOUND Then
   Begin
--************************************************************************
--* ATEN??O -- Se voc? alterou o c?digo da funcionalidade ou processo    *
--* no select abaixo voc? deve procurar pelo novo c?digo para que o      *
--* script possa ser executado mais de uma vez e atualize a defini??o    *
--* ao inv?s de criar uma nova!!!                                        *
--************************************************************************
       Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO';
       Update SYN_PRC_DEFINICAO set
           CODIGO              = 'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO'
           ,NOME                = 'Relatorio de Divergencia de Consolidacao - Filtros'
           ,TITULO              = 'Relatorio de Divergencia de Consolidacao - Filtros'
           ,FUN_TIPO            = 'F_FUNC'
           ,PRCDEF_ID           = mprcdef_prcdef_id
           ,NIVEL               = 5
           ,EXECUTAVEL_NOME     = ''
           ,ROTINA_DE_GERACAO   = 'HON_CONSOLIDA_REL.copia_dof_filtros'
           ,CTRL_TIPO_PROCESSO  = 'R'
           ,ORDEM               = 3
           ,CALC_ORDEM          = 138
           ,ICONE_NUMERO        = 0
           ,NOME_DO_FONTE       = 'Courier New'
           ,TAMANHO_DO_FONTE    = 3
           ,FONTE_NEGRITO       = 'N'
           ,IMPRESSORA          = ''
           ,ORIENTACAO          = 1
           ,FRM_ID              = mprcdef_frm_id
           ,IND_DEF_USUARIO     = 'S'
           ,IND_DESATIVADO      = 'N'
       Where ID = mid;
       Commit;
   Exception When NO_DATA_FOUND Then
       Select syn_seq_geral.nextval into mid from dual;
       Insert into SYN_PRC_DEFINICAO(
           ID
           ,CODIGO
           ,NOME
           ,TITULO
           ,FUN_TIPO
           ,PRCDEF_ID
           ,NIVEL
           ,EXECUTAVEL_NOME
           ,ROTINA_DE_GERACAO
           ,CTRL_TIPO_PROCESSO
           ,ORDEM
           ,CALC_ORDEM
           ,ICONE_NUMERO
           ,NOME_DO_FONTE
           ,TAMANHO_DO_FONTE
           ,FONTE_NEGRITO
           ,IMPRESSORA
           ,ORIENTACAO
           ,FRM_ID
           ,IND_DEF_USUARIO
           ,IND_DESATIVADO
           ) Values (
           mid
           ,'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO'
           ,'Relatorio de Divergencia de Consolidacao - Filtros'
           ,'Relatorio de Divergencia de Consolidacao - Filtros'
           ,'F_FUNC'
           ,mprcdef_prcdef_id
           ,5
           ,''
           ,'HON_CONSOLIDA_REL.copia_dof_filtros'
           ,'R'
           ,3
           ,138
           ,0
           ,'Courier New'
           ,3
           ,'N'
           ,''
           ,1
           ,mprcdef_frm_id
           ,'S'
           ,'N'
           );
       Commit;
   End;
End;
DELETE SYN_PRC_PARAMETRO WHERE PRCDEF_ID = mid;
Commit;
-- Importando Definicao de Parametros de 'UTI\ITF\PRC\HONCONS\HON_DIV_CONSOLIDA_DOF_FILTRO' ...
-- Importando Parametro 0 'p_origem' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_origem'
,'Origem'
,0
,'N'
,'TEXT'
,''
,'%'
,'select ''%'', ''TODOS'' from dual union
select ''HAB'', ''FAB4W'' from dual union
select ''MAO'', ''FAB2W'' from dual union
select ''PECAS'', ''PECAS'' from dual'
,''
);
-- Importando Parametro 1 'p_tipo_docto' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_tipo_docto'
,'Tipo de Documento Fiscal'
,1
,'N'
,'TEXT'
,''
,'%'
,'select ''%'', ''TODOS'' from dual union
select ''S'', ''Servico'' from dual union
select ''M'', ''Mercadoria'' from dual'
,''
);
Commit;
END;
/
 
BEGIN
  SYN_PRC_DEFINICAO_PKG.ATUALIZA; 
  SYN_PRC_DEFINICAO_PKG.VALIDA; 
END; 
/ 

