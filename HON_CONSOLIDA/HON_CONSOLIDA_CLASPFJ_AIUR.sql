CREATE OR REPLACE TRIGGER HON_CONSOLIDA_CLASPFJ_AIUR
AFTER INSERT  OR UPDATE
ON cor_classificacao_pessoa
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink    VARCHAR2(30);
  v_schema    VARCHAR2(30);
  v_sql       VARCHAR2(32767);
  --
  v_count     integer;

PROCEDURE atualiza_origem_MAO is
begin
  v_count := 0;              
  select count(1)            
    into v_count               
    from cor_classificacao_pessoa@lk_mao
   where elemento   = :new.elemento                            
     and cp_codigo  = :new.cp_codigo                             
     and pfj_codigo = :new.pfj_codigo                            
     and dt_inicio  = :new.dt_inicio;                            
  if v_count > 0                                              
  then                                                        
     update cor_classificacao_pessoa@lk_mao 
        set hon_consolida_data   = sysdate,                       
            hon_consolida_status = '1'                         
          where elemento   = :new.elemento                     
            and cp_codigo  = :new.cp_codigo                      
            and pfj_codigo = :new.pfj_codigo                     
            and dt_inicio  = :new.dt_inicio;
     commit;	
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'C');
     commit;
     end;
  end if;                                              
EXCEPTION WHEN OTHERS THEN                           
  rollback;
  RAISE;                                               
END;

PROCEDURE atualiza_origem_HAB is
begin
  v_count := 0;              
  select count(1)            
    into v_count               
    from cor_classificacao_pessoa@lk_hab
   where elemento   = :new.elemento                            
     and cp_codigo  = :new.cp_codigo                             
     and pfj_codigo = :new.pfj_codigo                            
     and dt_inicio  = :new.dt_inicio;                            
  if v_count > 0                                              
  then                                                        
     update cor_classificacao_pessoa@lk_hab 
        set hon_consolida_data   = sysdate,                       
            hon_consolida_status = '1'                         
          where elemento   = :new.elemento                     
            and cp_codigo  = :new.cp_codigo                      
            and pfj_codigo = :new.pfj_codigo                     
            and dt_inicio  = :new.dt_inicio;                     
	 commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'C');
     commit;
     end;
  end if;                                              
EXCEPTION WHEN OTHERS THEN                           
  rollback;
  RAISE;                                               
END;

PROCEDURE atualiza_origem_pecas is
begin
  v_count := 0;              
  select count(1)            
    into v_count               
    from cor_classificacao_pessoa@lk_pecas
   where elemento   = :new.elemento                            
     and cp_codigo  = :new.cp_codigo                             
     and pfj_codigo = :new.pfj_codigo                            
     and dt_inicio  = :new.dt_inicio;                            
  if v_count > 0                                              
  then                                                        
     update cor_classificacao_pessoa@lk_pecas
        set hon_consolida_data   = sysdate,                       
            hon_consolida_status = '1'                         
          where elemento   = :new.elemento                     
            and cp_codigo  = :new.cp_codigo                      
            and pfj_codigo = :new.pfj_codigo                     
            and dt_inicio  = :new.dt_inicio;                     
     commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'C');
     commit;
     end;
  end if;                                              
EXCEPTION WHEN OTHERS THEN                           
  rollback;
  RAISE;                                               
END;

Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_mao;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_hab;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
  -- HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_CLASSIFICACAO_PESSOA', 'C');

End;

--ALTER TRIGGER HON_CONSOLIDA_CLASPFJ_AIUR ENABLE;
