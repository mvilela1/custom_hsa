CREATE OR REPLACE TRIGGER HON_CONSOLIDA_CLASMERC_BUR 
BEFORE UPDATE
ON cor_classificacao_mercadoria
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  --PRAGMA AUTONOMOUS_TRANSACTION;
  --
  v_count     integer;
  v_dblink    varchar2(200);
Begin
   v_dblink := null;
   begin
      select SYS_CONTEXT ('USERENV', 'DBLINK_INFO') 
      -- Quando for executado update na base SAP4W esta variavel estar� preenchida  
        into v_dblink
        FROM DUAL;
   end;
   -- Se registro jah consolidado permite que seja re-consolidado 
   if :old.hon_consolida_status = '1' 
   -- Update realizado na base de origem - N�o deve ser executado via dblink
   -- MVS - 18/06/2018
   and (v_dblink is null)
   then
      :new.hon_consolida_status := '0';
      -- Registro consolidado alterado - MVS - 15/06/18
      --:new.hon_consolida_status := '2';
   end if;
   -- 
End;

--ALTER TRIGGER HON_CONSOLIDA_CLASMERC_BUR ENABLE;
