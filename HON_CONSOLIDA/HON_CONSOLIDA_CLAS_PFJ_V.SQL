--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View HON_CONSOLIDA_CLAS_PFJ_V
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FAB4W"."HON_CONSOLIDA_CLAS_PFJ_V" ("CP_CODIGO", "DT_FIM", "DT_INICIO", "ELEMENTO", "PFJ_CODIGO", "HON_CONSOLIDA_ORIGEM") AS 
  SELECT 
CP_CODIGO,
DT_FIM,
DT_INICIO,
ELEMENTO,
PFJ_CODIGO,
'HAB' HON_CONSOLIDA_ORIGEM
FROM COR_CLASSIFICACAO_PESSOA@lk_hab
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
UNION ALL
SELECT 
CP_CODIGO,
DT_FIM,
DT_INICIO,
ELEMENTO,
PFJ_CODIGO,
'MAO' HON_CONSOLIDA_ORIGEM
FROM COR_CLASSIFICACAO_PESSOA@lk_mao
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
UNION ALL
SELECT 
CP_CODIGO,
DT_FIM,
DT_INICIO,
ELEMENTO,
PFJ_CODIGO,
'PECAS' HON_CONSOLIDA_ORIGEM
FROM COR_CLASSIFICACAO_PESSOA@lk_pecas
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
;
