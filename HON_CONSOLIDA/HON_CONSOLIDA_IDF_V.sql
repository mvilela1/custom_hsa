--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View HON_CONSOLIDA_IDF_V
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "FAB4W"."HON_CONSOLIDA_IDF_V" ("ALIQ_ANTECIPACAO", "ALIQ_ANTECIP_ICMS", "ALIQ_COFINS", "ALIQ_COFINS_RET", "ALIQ_COFINS_ST", "ALIQ_CSLL_RET", "ALIQ_DIFA", "ALIQ_DIFA_ICMS_PART", "ALIQ_ICMS", "ALIQ_ICMS_DESC_L", "ALIQ_ICMS_FCP", "ALIQ_ICMS_FCPST", "ALIQ_ICMS_PART_DEST", "ALIQ_ICMS_PART_REM", "ALIQ_ICMS_STF60", "ALIQ_II", "ALIQ_INSS", "ALIQ_INSS_AE15_RET", "ALIQ_INSS_AE20_RET", "ALIQ_INSS_AE25_RET", "ALIQ_INSS_RET", "ALIQ_IPI", "ALIQ_ISS", "ALIQ_ISS_BITRIBUTADO", "ALIQ_PIS", "ALIQ_PIS_RET", "ALIQ_PIS_ST", "ALIQ_SENAT", "ALIQ_SEST", "ALIQ_STF", "ALIQ_STT", "AM_CODIGO", "C4_SALDO_REF", "CCA_CODIGO", "CEST_CODIGO", "CFOP_CODIGO", "CHASSI_VEIC", "CLI_CODIGO", "CNPJ_FABRICANTE", "CNPJ_PAR", "CNPJ_PROD_DIFERENTE_EMIT", "CNPJ_SUBEMPREITEIRO", "CODIGO_DO_SITE", "CODIGO_ISS_MUNICIPIO", "CODIGO_RETENCAO", "CODIGO_RETENCAO_COFINS", "CODIGO_RETENCAO_CSLL", "CODIGO_RETENCAO_INSS", "CODIGO_RETENCAO_PCC", "CODIGO_RETENCAO_PIS", "COD_AREA", "COD_BC_CREDITO", "COD_BENEFICIO_FISCAL", "COD_CCUS", "COD_CONJ_KIT", "COD_FISC_SERV_MUN", "COD_GTIN", "COD_IATA_FIM", "COD_IATA_INI", "COD_IBGE", "COD_NVE", "CONTA_CONTABIL", "CPF_PAR", "CSOSN_CODIGO", "CTRL_DESCARGA", "CTRL_ENTRADA_ST", "CTRL_OBS_CALC_ICMS", "CTRL_OBS_CALC_INSS", "CTRL_OBS_CALC_IPI", "CTRL_OBS_CALC_IRRF", "CTRL_OBS_CALC_ISS", "CTRL_OBS_CALC_STF", "CTRL_OBS_CALC_STT", "CTRL_OBS_ENQ", "CTRL_OBS_SETUP_ICMS", "CTRL_OBS_SETUP_IPI", "CTRL_OBS_TOT_PESO", "CTRL_OBS_TOT_PRECO", "CTRL_OBS_VL_FISCAL", "CTRL_SAIDA_ST", "CTRL_TOLERANCIA_PED", "CUSTO_ADICAO", "CUSTO_REDUCAO", "DESCRICAO_ARMA", "DESCRICAO_COMPLEMENTAR_SERVICO", "DESONERACAO_CODIGO", "DOF_ID", "DOF_SEQUENCE", "DT_DI", "DT_FAB_MED", "DT_FIN_SERV", "DT_INI_SERV", "DT_REF_CALC_IMP_IDF", "DT_VAL", "EMERC_CODIGO", "ENQ_IPI_CODIGO", "ENTSAI_UNI_CODIGO", "ESTOQUE_UNI_CODIGO", "FAT_BASE_CALCULO_STF", "FAT_CONV_UNI_ESTOQUE", "FAT_CONV_UNI_FISCAL", "FAT_INCLUSAO_ICMS_PRECO", "FCI_NUMERO", "FIN_CODIGO", "FLAG_SUFRAMA", "HON_COFINS_RET", "HON_CSLL_RET", "HON_PIS_RET", "ID", "IDF_NUM", "IDF_NUM_PAI", "IDF_TEXTO_COMPLEMENTAR", "IE_PAR", "IM_SUBCONTRATACAO", "IND_ANTECIP_ICMS", "IND_ARM", "IND_BLOQUEADO", "IND_COFINS_ST", "IND_COMPROVA_OPERACAO", "IND_CONTABILIZACAO", "IND_ESCALA_RELEVANTE", "IND_ESCRITURACAO", "IND_EXIGIBILIDADE_ISS", "IND_FCP_ST", "IND_INCENTIVO_FISCAL_ISS", "IND_INCIDENCIA_COFINS", "IND_INCIDENCIA_COFINS_RET", "IND_INCIDENCIA_COFINS_ST", "IND_INCIDENCIA_CSLL_RET", "IND_INCIDENCIA_ICMS", "IND_INCIDENCIA_INSS", "IND_INCIDENCIA_INSS_RET", "IND_INCIDENCIA_IPI", "IND_INCIDENCIA_IRRF", "IND_INCIDENCIA_ISS", "IND_INCIDENCIA_PIS", "IND_INCIDENCIA_PIS_RET", "IND_INCIDENCIA_PIS_ST", "IND_INCIDENCIA_SENAT", "IND_INCIDENCIA_SEST", "IND_INCIDENCIA_STF", "IND_INCIDENCIA_STT", "IND_IPI_BASE_ICMS", "IND_ISS_RETIDO_FONTE", "IND_LAN_FISCAL_ICMS", "IND_LAN_FISCAL_IPI", "IND_LAN_FISCAL_STF", "IND_LAN_FISCAL_STT", "IND_LAN_IMP", "IND_MED", "IND_MOVIMENTACAO_CIAP", "IND_MOVIMENTACAO_CPC", "IND_MOVIMENTA_ESTOQUE", "IND_MP_DO_BEM", "IND_NAT_FRT_PISCOFINS", "IND_OUTROS_ICMS", "IND_OUTROS_IPI", "IND_PAUTA_STF_MVA_MP", "IND_PIS_ST", "IND_SERV", "IND_VERIFICA_PEDIDO", "IND_VL_FISC_VL_CONT", "IND_VL_FISC_VL_FAT", "IND_VL_ICMS_NO_PRECO", "IND_VL_ICMS_VL_CONT", "IND_VL_ICMS_VL_FAT", "IND_VL_PIS_COFINS_NO_PRECO", "IND_VL_TRIB_RET_NO_PRECO", "IND_ZFM_ALC", "LOTE_MED", "MERC_CODIGO", "MOD_BASE_ICMS_CODIGO", "MOD_BASE_ICMS_ST_CODIGO", "MUN_CODIGO", "NAT_REC_PISCOFINS", "NAT_REC_PISCOFINS_DESCR", "NBM_CODIGO", "NBS_CODIGO", "NOP_CODIGO", "NUM_ARM", "NUM_CANO", "NUM_DI", "NUM_PROC_SUSP_EXIGIBILIDADE", "NUM_TANQUE", "OM_CODIGO", "ORD_IMPRESSAO", "PARTICIPANTE_PFJ_CODIGO", "PAUTA_FLUT_ICMS", "PEDIDO_NUMERO", "PEDIDO_NUMERO_ITEM", "PERC_AJUSTE_PRECO_TOTAL", "PERC_ICMS_PART_DEST", "PERC_ICMS_PART_REM", "PERC_IRRF", "PERC_ISENTO_ICMS", "PERC_ISENTO_IPI", "PERC_OUTROS_ABAT", "PERC_OUTROS_ICMS", "PERC_OUTROS_IPI", "PERC_PART_CI", "PERC_TRIBUTAVEL_COFINS", "PERC_TRIBUTAVEL_COFINS_ST", "PERC_TRIBUTAVEL_ICMS", "PERC_TRIBUTAVEL_INSS", "PERC_TRIBUTAVEL_INSS_RET", "PERC_TRIBUTAVEL_IPI", "PERC_TRIBUTAVEL_IRRF", "PERC_TRIBUTAVEL_ISS", "PERC_TRIBUTAVEL_PIS", "PERC_TRIBUTAVEL_PIS_ST", "PERC_TRIBUTAVEL_SENAT", "PERC_TRIBUTAVEL_SEST", "PERC_TRIBUTAVEL_STF", "PERC_TRIBUTAVEL_STT", "PER_FISCAL", "PESO_BRUTO_KG", "PESO_LIQUIDO_KG", "PFJ_CODIGO_FORNECEDOR", "PFJ_CODIGO_TERCEIRO", "PRECO_NET_UNIT", "PRECO_NET_UNIT_PIS_COFINS", "PRECO_TOTAL", "PRECO_UNITARIO", "PRES_CODIGO", "QTD", "QTD_BASE_COFINS", "QTD_BASE_PIS", "QTD_EMB", "QTD_KIT", "REVISAO", "SELO_CODIGO", "SELO_QTDE", "SISS_CODIGO", "STA_CODIGO", "STC_CODIGO", "STI_CODIGO", "STM_CODIGO", "STN_CODIGO", "STP_CODIGO", "SUBCLASSE_IDF", "TERMINAL", "TIPO_COMPLEMENTO", "TIPO_OPER_VEIC", "TIPO_PROD_MED", "TIPO_RECEITA", "TIPO_STF", "UF_PAR", "UNI_CODIGO_FISCAL", "UNI_FISCAL_CODIGO", "VL_ABAT_LEGAL_INSS_RET", "VL_ABAT_LEGAL_IRRF", "VL_ABAT_LEGAL_ISS", "VL_ADICIONAL_RET_AE", "VL_AJUSTE_PRECO_TOTAL", "VL_ALIMENTACAO", "VL_ALIQ_COFINS", "VL_ALIQ_PIS", "VL_ANTECIPACAO", "VL_ANTECIP_ICMS", "VL_BASE_COFINS", "VL_BASE_COFINS_RET", "VL_BASE_COFINS_ST", "VL_BASE_CSLL_RET", "VL_BASE_ICMS", "VL_BASE_ICMS_DESC_L", "VL_BASE_ICMS_FCP", "VL_BASE_ICMS_FCPST", "VL_BASE_ICMS_PART_DEST", "VL_BASE_ICMS_PART_REM", "VL_BASE_II", "VL_BASE_INSS", "VL_BASE_INSS_RET", "VL_BASE_IPI", "VL_BASE_IRRF", "VL_BASE_ISS", "VL_BASE_ISS_BITRIBUTADO", "VL_BASE_PIS", "VL_BASE_PIS_RET", "VL_BASE_PIS_ST", "VL_BASE_SENAT", "VL_BASE_SEST", "VL_BASE_STF", "VL_BASE_STF60", "VL_BASE_STF_FRONTEIRA", "VL_BASE_STF_IDO", "VL_BASE_STT", "VL_BASE_SUFRAMA", "VL_COFINS", "VL_COFINS_RET", "VL_COFINS_ST", "VL_CONTABIL", "VL_CRED_COFINS_REC_EXPO", "VL_CRED_COFINS_REC_NAO_TRIB", "VL_CRED_COFINS_REC_TRIB", "VL_CRED_PIS_REC_EXPO", "VL_CRED_PIS_REC_NAO_TRIB", "VL_CRED_PIS_REC_TRIB", "VL_CSLL_RET", "VL_DEDUCAO_DEPENDENTE_PRG", "VL_DEDUCAO_INSS", "VL_DEDUCAO_IRRF_PRG", "VL_DEDUCAO_PENSAO_PRG", "VL_DESC_CP", "VL_DIFA", "VL_FATURADO", "VL_FISCAL", "VL_GILRAT", "VL_ICMS", "VL_ICMS_DESC_L", "VL_ICMS_FCP", "VL_ICMS_FCPST", "VL_ICMS_PART_DEST", "VL_ICMS_PART_REM", "VL_ICMS_PROPRIO_RECUP", "VL_ICMS_SIMPLES_NAC", "VL_ICMS_ST_RECUP", "VL_II", "VL_IMPOSTO_COFINS", "VL_IMPOSTO_PIS", "VL_IMP_FCI", "VL_INSS", "VL_INSS_AE15_NAO_RET", "VL_INSS_AE15_RET", "VL_INSS_AE20_NAO_RET", "VL_INSS_AE20_RET", "VL_INSS_AE25_NAO_RET", "VL_INSS_AE25_RET", "VL_INSS_NAO_RET", "VL_INSS_RET", "VL_IOF", "VL_IPI", "VL_IRRF", "VL_IRRF_NAO_RET", "VL_ISENTO_ICMS", "VL_ISENTO_IPI", "VL_ISS", "VL_ISS_BITRIBUTADO", "VL_ISS_DESC_CONDICIONADO", "VL_ISS_DESC_INCONDICIONADO", "VL_ISS_OUTRAS_RETENCOES", "VL_MATERIAS_EQUIP", "VL_NAO_RETIDO_CP", "VL_NAO_RETIDO_CP_AE", "VL_OUTROS_ABAT", "VL_OUTROS_ICMS", "VL_OUTROS_IMPOSTOS", "VL_OUTROS_IPI", "VL_PIS", "VL_PIS_RET", "VL_PIS_ST", "VL_RATEIO_AJUSTE_PRECO", "VL_RATEIO_BASE_CT_STF", "VL_RATEIO_CT_STF", "VL_RATEIO_FRETE", "VL_RATEIO_ODA", "VL_RATEIO_SEGURO", "VL_RECUPERADO", "VL_RETIDO_SUBEMPREITADA", "VL_SENAR", "VL_SENAT", "VL_SERVICO_AE15", "VL_SERVICO_AE20", "VL_SERVICO_AE25", "VL_SEST", "VL_STF", "VL_STF60", "VL_STF_FRONTEIRA", "VL_STF_IDO", "VL_STT", "VL_ST_FRONTEIRA", "VL_SUFRAMA", "VL_TAB_MAX", "VL_TRANSPORTE", "VL_TRIBUTAVEL_ANTEC", "VL_TRIBUTAVEL_DIFA", "VL_TRIBUTAVEL_ICMS", "VL_TRIBUTAVEL_ICMS_DESC_L", "VL_TRIBUTAVEL_IPI", "VL_TRIBUTAVEL_STF", "VL_TRIBUTAVEL_STT", "VL_TRIBUTAVEL_SUFRAMA", "XCON_ID", "XINF_ID", "HON_CONSOLIDA_ORIGEM") AS 
  SELECT
ALIQ_ANTECIPACAO,
nvl(ALIQ_ANTECIP_ICMS,0) ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
C4_SALDO_REF,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
nvl(CUSTO_ADICAO,0) CUSTO_ADICAO,
nvl(CUSTO_REDUCAO,0) CUSTO_REDUCAO,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
nvl(IND_ANTECIP_ICMS,'N') IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
nvl(VL_ANTECIP_ICMS,0) VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF60,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_IMP_FCI,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF60,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_STT,
VL_ST_FRONTEIRA,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
'HAB' HON_CONSOLIDA_ORIGEM
FROM COR_IDF@lk_hab
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
UNION ALL
SELECT
ALIQ_ANTECIPACAO,
nvl(ALIQ_ANTECIP_ICMS,0) ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
C4_SALDO_REF,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
nvl(CUSTO_ADICAO,0) CUSTO_ADICAO,
nvl(CUSTO_REDUCAO,0) CUSTO_REDUCAO,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
nvl(IND_ANTECIP_ICMS,'N') IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
nvl(VL_ANTECIP_ICMS,0) VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF60,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_IMP_FCI,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF60,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_STT,
VL_ST_FRONTEIRA,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
'MAO' HON_CONSOLIDA_ORIGEM
FROM COR_IDF@lk_mao
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
--and 2 = 1
UNION ALL
SELECT
ALIQ_ANTECIPACAO,
nvl(ALIQ_ANTECIP_ICMS,0) ALIQ_ANTECIP_ICMS,
ALIQ_COFINS,
ALIQ_COFINS_RET,
ALIQ_COFINS_ST,
ALIQ_CSLL_RET,
ALIQ_DIFA,
ALIQ_DIFA_ICMS_PART,
ALIQ_ICMS,
ALIQ_ICMS_DESC_L,
ALIQ_ICMS_FCP,
ALIQ_ICMS_FCPST,
ALIQ_ICMS_PART_DEST,
ALIQ_ICMS_PART_REM,
ALIQ_ICMS_STF60,
ALIQ_II,
ALIQ_INSS,
ALIQ_INSS_AE15_RET,
ALIQ_INSS_AE20_RET,
ALIQ_INSS_AE25_RET,
ALIQ_INSS_RET,
ALIQ_IPI,
ALIQ_ISS,
ALIQ_ISS_BITRIBUTADO,
ALIQ_PIS,
ALIQ_PIS_RET,
ALIQ_PIS_ST,
ALIQ_SENAT,
ALIQ_SEST,
ALIQ_STF,
ALIQ_STT,
AM_CODIGO,
C4_SALDO_REF,
CCA_CODIGO,
CEST_CODIGO,
CFOP_CODIGO,
CHASSI_VEIC,
CLI_CODIGO,
CNPJ_FABRICANTE,
CNPJ_PAR,
CNPJ_PROD_DIFERENTE_EMIT,
CNPJ_SUBEMPREITEIRO,
CODIGO_DO_SITE,
CODIGO_ISS_MUNICIPIO,
CODIGO_RETENCAO,
CODIGO_RETENCAO_COFINS,
CODIGO_RETENCAO_CSLL,
CODIGO_RETENCAO_INSS,
CODIGO_RETENCAO_PCC,
CODIGO_RETENCAO_PIS,
COD_AREA,
COD_BC_CREDITO,
COD_BENEFICIO_FISCAL,
COD_CCUS,
COD_CONJ_KIT,
COD_FISC_SERV_MUN,
COD_GTIN,
COD_IATA_FIM,
COD_IATA_INI,
COD_IBGE,
COD_NVE,
CONTA_CONTABIL,
CPF_PAR,
CSOSN_CODIGO,
CTRL_DESCARGA,
CTRL_ENTRADA_ST,
CTRL_OBS_CALC_ICMS,
CTRL_OBS_CALC_INSS,
CTRL_OBS_CALC_IPI,
CTRL_OBS_CALC_IRRF,
CTRL_OBS_CALC_ISS,
CTRL_OBS_CALC_STF,
CTRL_OBS_CALC_STT,
CTRL_OBS_ENQ,
CTRL_OBS_SETUP_ICMS,
CTRL_OBS_SETUP_IPI,
CTRL_OBS_TOT_PESO,
CTRL_OBS_TOT_PRECO,
CTRL_OBS_VL_FISCAL,
CTRL_SAIDA_ST,
CTRL_TOLERANCIA_PED,
nvl(CUSTO_ADICAO,0) CUSTO_ADICAO,
nvl(CUSTO_REDUCAO,0) CUSTO_REDUCAO,
DESCRICAO_ARMA,
DESCRICAO_COMPLEMENTAR_SERVICO,
DESONERACAO_CODIGO,
DOF_ID,
DOF_SEQUENCE,
DT_DI,
DT_FAB_MED,
DT_FIN_SERV,
DT_INI_SERV,
DT_REF_CALC_IMP_IDF,
DT_VAL,
EMERC_CODIGO,
ENQ_IPI_CODIGO,
ENTSAI_UNI_CODIGO,
ESTOQUE_UNI_CODIGO,
FAT_BASE_CALCULO_STF,
FAT_CONV_UNI_ESTOQUE,
FAT_CONV_UNI_FISCAL,
FAT_INCLUSAO_ICMS_PRECO,
FCI_NUMERO,
FIN_CODIGO,
FLAG_SUFRAMA,
HON_COFINS_RET,
HON_CSLL_RET,
HON_PIS_RET,
ID,
IDF_NUM,
IDF_NUM_PAI,
IDF_TEXTO_COMPLEMENTAR,
IE_PAR,
IM_SUBCONTRATACAO,
nvl(IND_ANTECIP_ICMS,'N') IND_ANTECIP_ICMS,
IND_ARM,
IND_BLOQUEADO,
IND_COFINS_ST,
IND_COMPROVA_OPERACAO,
IND_CONTABILIZACAO,
IND_ESCALA_RELEVANTE,
IND_ESCRITURACAO,
IND_EXIGIBILIDADE_ISS,
IND_FCP_ST,
IND_INCENTIVO_FISCAL_ISS,
IND_INCIDENCIA_COFINS,
IND_INCIDENCIA_COFINS_RET,
IND_INCIDENCIA_COFINS_ST,
IND_INCIDENCIA_CSLL_RET,
IND_INCIDENCIA_ICMS,
IND_INCIDENCIA_INSS,
IND_INCIDENCIA_INSS_RET,
IND_INCIDENCIA_IPI,
IND_INCIDENCIA_IRRF,
IND_INCIDENCIA_ISS,
IND_INCIDENCIA_PIS,
IND_INCIDENCIA_PIS_RET,
IND_INCIDENCIA_PIS_ST,
IND_INCIDENCIA_SENAT,
IND_INCIDENCIA_SEST,
IND_INCIDENCIA_STF,
IND_INCIDENCIA_STT,
IND_IPI_BASE_ICMS,
IND_ISS_RETIDO_FONTE,
IND_LAN_FISCAL_ICMS,
IND_LAN_FISCAL_IPI,
IND_LAN_FISCAL_STF,
IND_LAN_FISCAL_STT,
IND_LAN_IMP,
IND_MED,
IND_MOVIMENTACAO_CIAP,
IND_MOVIMENTACAO_CPC,
IND_MOVIMENTA_ESTOQUE,
IND_MP_DO_BEM,
IND_NAT_FRT_PISCOFINS,
IND_OUTROS_ICMS,
IND_OUTROS_IPI,
IND_PAUTA_STF_MVA_MP,
IND_PIS_ST,
IND_SERV,
IND_VERIFICA_PEDIDO,
IND_VL_FISC_VL_CONT,
IND_VL_FISC_VL_FAT,
IND_VL_ICMS_NO_PRECO,
IND_VL_ICMS_VL_CONT,
IND_VL_ICMS_VL_FAT,
IND_VL_PIS_COFINS_NO_PRECO,
IND_VL_TRIB_RET_NO_PRECO,
IND_ZFM_ALC,
LOTE_MED,
MERC_CODIGO,
MOD_BASE_ICMS_CODIGO,
MOD_BASE_ICMS_ST_CODIGO,
MUN_CODIGO,
NAT_REC_PISCOFINS,
NAT_REC_PISCOFINS_DESCR,
NBM_CODIGO,
NBS_CODIGO,
NOP_CODIGO,
NUM_ARM,
NUM_CANO,
NUM_DI,
NUM_PROC_SUSP_EXIGIBILIDADE,
NUM_TANQUE,
OM_CODIGO,
ORD_IMPRESSAO,
PARTICIPANTE_PFJ_CODIGO,
PAUTA_FLUT_ICMS,
PEDIDO_NUMERO,
PEDIDO_NUMERO_ITEM,
PERC_AJUSTE_PRECO_TOTAL,
PERC_ICMS_PART_DEST,
PERC_ICMS_PART_REM,
PERC_IRRF,
PERC_ISENTO_ICMS,
PERC_ISENTO_IPI,
PERC_OUTROS_ABAT,
PERC_OUTROS_ICMS,
PERC_OUTROS_IPI,
PERC_PART_CI,
PERC_TRIBUTAVEL_COFINS,
PERC_TRIBUTAVEL_COFINS_ST,
PERC_TRIBUTAVEL_ICMS,
PERC_TRIBUTAVEL_INSS,
PERC_TRIBUTAVEL_INSS_RET,
PERC_TRIBUTAVEL_IPI,
PERC_TRIBUTAVEL_IRRF,
PERC_TRIBUTAVEL_ISS,
PERC_TRIBUTAVEL_PIS,
PERC_TRIBUTAVEL_PIS_ST,
PERC_TRIBUTAVEL_SENAT,
PERC_TRIBUTAVEL_SEST,
PERC_TRIBUTAVEL_STF,
PERC_TRIBUTAVEL_STT,
PER_FISCAL,
PESO_BRUTO_KG,
PESO_LIQUIDO_KG,
PFJ_CODIGO_FORNECEDOR,
PFJ_CODIGO_TERCEIRO,
PRECO_NET_UNIT,
PRECO_NET_UNIT_PIS_COFINS,
PRECO_TOTAL,
PRECO_UNITARIO,
PRES_CODIGO,
QTD,
QTD_BASE_COFINS,
QTD_BASE_PIS,
QTD_EMB,
QTD_KIT,
REVISAO,
SELO_CODIGO,
SELO_QTDE,
SISS_CODIGO,
STA_CODIGO,
STC_CODIGO,
STI_CODIGO,
STM_CODIGO,
STN_CODIGO,
STP_CODIGO,
SUBCLASSE_IDF,
TERMINAL,
TIPO_COMPLEMENTO,
TIPO_OPER_VEIC,
TIPO_PROD_MED,
TIPO_RECEITA,
TIPO_STF,
UF_PAR,
UNI_CODIGO_FISCAL,
UNI_FISCAL_CODIGO,
VL_ABAT_LEGAL_INSS_RET,
VL_ABAT_LEGAL_IRRF,
VL_ABAT_LEGAL_ISS,
VL_ADICIONAL_RET_AE,
VL_AJUSTE_PRECO_TOTAL,
VL_ALIMENTACAO,
VL_ALIQ_COFINS,
VL_ALIQ_PIS,
VL_ANTECIPACAO,
nvl(VL_ANTECIP_ICMS,0) VL_ANTECIP_ICMS,
VL_BASE_COFINS,
VL_BASE_COFINS_RET,
VL_BASE_COFINS_ST,
VL_BASE_CSLL_RET,
VL_BASE_ICMS,
VL_BASE_ICMS_DESC_L,
VL_BASE_ICMS_FCP,
VL_BASE_ICMS_FCPST,
VL_BASE_ICMS_PART_DEST,
VL_BASE_ICMS_PART_REM,
VL_BASE_II,
VL_BASE_INSS,
VL_BASE_INSS_RET,
VL_BASE_IPI,
VL_BASE_IRRF,
VL_BASE_ISS,
VL_BASE_ISS_BITRIBUTADO,
VL_BASE_PIS,
VL_BASE_PIS_RET,
VL_BASE_PIS_ST,
VL_BASE_SENAT,
VL_BASE_SEST,
VL_BASE_STF,
VL_BASE_STF60,
VL_BASE_STF_FRONTEIRA,
VL_BASE_STF_IDO,
VL_BASE_STT,
VL_BASE_SUFRAMA,
VL_COFINS,
VL_COFINS_RET,
VL_COFINS_ST,
VL_CONTABIL,
VL_CRED_COFINS_REC_EXPO,
VL_CRED_COFINS_REC_NAO_TRIB,
VL_CRED_COFINS_REC_TRIB,
VL_CRED_PIS_REC_EXPO,
VL_CRED_PIS_REC_NAO_TRIB,
VL_CRED_PIS_REC_TRIB,
VL_CSLL_RET,
VL_DEDUCAO_DEPENDENTE_PRG,
VL_DEDUCAO_INSS,
VL_DEDUCAO_IRRF_PRG,
VL_DEDUCAO_PENSAO_PRG,
VL_DESC_CP,
VL_DIFA,
VL_FATURADO,
VL_FISCAL,
VL_GILRAT,
VL_ICMS,
VL_ICMS_DESC_L,
VL_ICMS_FCP,
VL_ICMS_FCPST,
VL_ICMS_PART_DEST,
VL_ICMS_PART_REM,
VL_ICMS_PROPRIO_RECUP,
VL_ICMS_SIMPLES_NAC,
VL_ICMS_ST_RECUP,
VL_II,
VL_IMPOSTO_COFINS,
VL_IMPOSTO_PIS,
VL_IMP_FCI,
VL_INSS,
VL_INSS_AE15_NAO_RET,
VL_INSS_AE15_RET,
VL_INSS_AE20_NAO_RET,
VL_INSS_AE20_RET,
VL_INSS_AE25_NAO_RET,
VL_INSS_AE25_RET,
VL_INSS_NAO_RET,
VL_INSS_RET,
VL_IOF,
VL_IPI,
VL_IRRF,
VL_IRRF_NAO_RET,
VL_ISENTO_ICMS,
VL_ISENTO_IPI,
VL_ISS,
VL_ISS_BITRIBUTADO,
VL_ISS_DESC_CONDICIONADO,
VL_ISS_DESC_INCONDICIONADO,
VL_ISS_OUTRAS_RETENCOES,
VL_MATERIAS_EQUIP,
VL_NAO_RETIDO_CP,
VL_NAO_RETIDO_CP_AE,
VL_OUTROS_ABAT,
VL_OUTROS_ICMS,
VL_OUTROS_IMPOSTOS,
VL_OUTROS_IPI,
VL_PIS,
VL_PIS_RET,
VL_PIS_ST,
VL_RATEIO_AJUSTE_PRECO,
VL_RATEIO_BASE_CT_STF,
VL_RATEIO_CT_STF,
VL_RATEIO_FRETE,
VL_RATEIO_ODA,
VL_RATEIO_SEGURO,
VL_RECUPERADO,
VL_RETIDO_SUBEMPREITADA,
VL_SENAR,
VL_SENAT,
VL_SERVICO_AE15,
VL_SERVICO_AE20,
VL_SERVICO_AE25,
VL_SEST,
VL_STF,
VL_STF60,
VL_STF_FRONTEIRA,
VL_STF_IDO,
VL_STT,
VL_ST_FRONTEIRA,
VL_SUFRAMA,
VL_TAB_MAX,
VL_TRANSPORTE,
VL_TRIBUTAVEL_ANTEC,
VL_TRIBUTAVEL_DIFA,
VL_TRIBUTAVEL_ICMS,
VL_TRIBUTAVEL_ICMS_DESC_L,
VL_TRIBUTAVEL_IPI,
VL_TRIBUTAVEL_STF,
VL_TRIBUTAVEL_STT,
VL_TRIBUTAVEL_SUFRAMA,
XCON_ID,
XINF_ID,
'PECAS' HON_CONSOLIDA_ORIGEM
FROM COR_IDF@lk_pecas
WHERE HON_CONSOLIDA_STATUS = '0'
-- teste mvs 04/05/18
and 2 = 1
;
