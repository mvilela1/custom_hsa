  CREATE OR REPLACE PACKAGE HON_CONSOLIDA_REL
----------------------------------------------------------------------------------------------------
-- PACKAGE: HON_CONSOLIDA_REL
--
-- Objetivo: Esse package tem por finalidade a verifica��o de inconsist�ncias na consolida��o de dados
--           das 4 unidades de neg�cio
--
-- Hist�rico:
--   24/04/2018 - MVS - Implementa��o
--
----------------------------------------------------------------------------------------------------
IS
  -- Crio um tipo para facilitar o controle das tabelas utilizadas e posterior exclus�o das mesmas.
  TYPE typ_tabela_tmp IS TABLE OF VARCHAR2(50) INDEX BY PLS_INTEGER;
  l_tabela_tmp typ_tabela_tmp;

  -- Refer�ncia dos ID's utilizados na vari�vel de tabela:
  -- PROCESSO DOF
  --  1 - COR_DOF
  --  2 - COR_IDF
  --  3 - COR_DOF_ASSOCIADO
  --  4 - COR_DOF_PARCELA
  -- 11 - COR_PESSOA
  -- 12 - COR_PESSOA_VIGENCIA
  -- 13 - COR_LOCALIDADE_PESSOA
  -- 14 - COR_LOCALIDADE_VIGENCIA
  -- 15 - COR_CLASSIFICACAO_PESSOA
  -- 16 - COR_MERCADORIA
  -- 17 - COR_CLASSIFICACAO_MERCADORIA
  --    - COR_PRESTACAO
  --    - COR_SERVICO_ISS

  -- Vari�veis globais utilizados nos processos
  g_tempo           NUMBER;
  g_select          VARCHAR2(32767);
  g_limite          PLS_INTEGER := 1000;
  g_processo        NUMBER;
  g_nome_relatorio  VARCHAR2(200);
  g_reg_pagina      PLS_INTEGER := 50;
  g_cnt             PLS_INTEGER;
  g_data_comeco     DATE;
  g_data_termino    DATE;
  g_posfinal        PLS_INTEGER;
  g_base_dest       VARCHAR2(50);
  --
  g_dblink          VARCHAR2(30);
  g_schema          VARCHAR2(30);
  g_sql             VARCHAR2(32767);
  v_colunas_erro    VARCHAR2(32000);
  g_origem          VARCHAR2(10);
  g_tipo_docto      VARCHAR2(10);

  -- Efetua o c�lculo do tempo autom�tico, em caso de debug facilita saber o ponto que demora mais tempo
  FUNCTION calcula_tempo ( p_tempo IN OUT NUMBER ) RETURN NUMBER;

-- Efetua o drop de uma tabela
  PROCEDURE drop_tabela ( p_tabela IN VARCHAR2 );

  -- Cria��o padr�o de uma tabela tempor�ria como c�pia de outra
  PROCEDURE cria_tabela (
  p_tabela_temp     IN VARCHAR2,
  p_tabela          IN VARCHAR2,
  p_select          IN VARCHAR2
  );

  PROCEDURE insere_tabela_def (
  p_tabela       IN VARCHAR2,
  p_select       IN out VARCHAR2,
  p_view         IN VARCHAR2,
  p_lim          IN NUMBER   DEFAULT 100,
  p_where        IN VARCHAR2 DEFAULT NULL
);
  -- Processo de transfer�ncia de documentos fiscais
  PROCEDURE copia_dof (
    p_processo    IN NUMBER
  );

 PROCEDURE copia_dof_filtros (
  p_origem      IN VARCHAR2,
  p_tipo_docto  IN VARCHAR2,
  p_processo    IN NUMBER
  );

  -- Inclui header padr�o para os relat�rios
  PROCEDURE r_inclui_header;
  -- Inclui footer padr�o para os relat�rios
  PROCEDURE r_inclui_footer;
  -- Inclui header para o relat�rio de Documentos Fiscais
  PROCEDURE r_inclui_header_dof;
  -- Insere uma linha
  PROCEDURE r_put_line(
    p_linha  IN     VARCHAR2
  );
  -- Gera o relat�rio dos registros transferidos de DOF
  PROCEDURE gera_relatorio_dof;
  -- Gera relat�rio se houver inconsist�ncias entre as bases
  PROCEDURE gera_relatorio_erro;
--    (
--    p_colunas_erro  IN VARCHAR2
--  );

  PROCEDURE gera_relatorio_erro_colunas(
    p_colunas_erro  IN VARCHAR2);

  PROCEDURE gera_relatorio_erro_filtros;

  PROCEDURE r_inclui_header_filtros;

END;

/
