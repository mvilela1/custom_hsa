--------------------------------------------------------
--  Arquivo criado - Segunda-feira-Maio-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HON_GTTEMP_CLAS_PFJ
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE "FAB4W"."HON_GTTEMP_CLAS_PFJ" 
   (	"CP_CODIGO" VARCHAR2(6 CHAR), 
	"DT_FIM" DATE, 
	"DT_INICIO" DATE, 
	"ELEMENTO" VARCHAR2(60 CHAR), 
	"HON_CONSOLIDA_ORIGEM" VARCHAR2(5 BYTE), 
	"PFJ_CODIGO" VARCHAR2(20 CHAR)
   ) ON COMMIT PRESERVE ROWS ;
--------------------------------------------------------
--  DDL for Index HON_GTTEMP_CLAS_PFJ_I
--------------------------------------------------------

  CREATE INDEX "FAB4W"."HON_GTTEMP_CLAS_PFJ_I" ON "FAB4W"."HON_GTTEMP_CLAS_PFJ" ("PFJ_CODIGO", "DT_INICIO", "CP_CODIGO", "ELEMENTO") ;
--------------------------------------------------------
--  Constraints for Table HON_GTTEMP_CLAS_PFJ
--------------------------------------------------------

  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_PFJ" MODIFY ("CP_CODIGO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_PFJ" MODIFY ("DT_INICIO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_PFJ" MODIFY ("ELEMENTO" NOT NULL ENABLE);
  ALTER TABLE "FAB4W"."HON_GTTEMP_CLAS_PFJ" MODIFY ("PFJ_CODIGO" NOT NULL ENABLE);
