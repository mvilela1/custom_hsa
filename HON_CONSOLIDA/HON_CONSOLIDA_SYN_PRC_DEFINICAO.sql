--
-- Importando Definicao 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF' ...
--
--
DECLARE
  mid syn_prc_definicao.id%type;
  mprcdef_prcdef_id syn_prc_definicao.prcdef_id%type := null;
  mprcdef_frm_id syn_prc_definicao.frm_id%type := null;
BEGIN
  Select id into mprcdef_prcdef_id from syn_prc_definicao where codigo = 'UTI\ITF\PRC\HONCONS';
Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo c�digo antigo para que o    *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
  Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS';
  Update SYN_PRC_DEFINICAO set
   CODIGO              = 'UTI\ITF\PRC\HONCONS'
  ,NOME                = 'Consolida��o de Documentos'
  ,TITULO              = 'Consolida��o de Documentos'
  ,FUN_TIPO            = 'F_REF'
  ,PRCDEF_ID           = mprcdef_prcdef_id
  ,NIVEL               = 4
  ,EXECUTAVEL_NOME     = ''
  ,ROTINA_DE_GERACAO   = ''
  ,CTRL_TIPO_PROCESSO  = ''
  ,ORDEM               = 11
  ,CALC_ORDEM          = 131
  ,CALC_PATH           = '01.01.02.04.03.01.04.11.'
  ,ICONE_NUMERO        = 0
  ,NOME_DO_FONTE       = ''
  ,TAMANHO_DO_FONTE    = 10
  ,FONTE_NEGRITO       = 'N'
  ,IMPRESSORA          = ''
  ,ORIENTACAO          = 1
  ,FRM_ID              = mprcdef_frm_id
  ,IND_DEF_USUARIO     = 'S'
  ,IND_DESATIVADO      = 'N'
  ,HTML_MOD_ID         = ''
  Where ID = mid;
  Commit;
Exception When NO_DATA_FOUND Then
   Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo novo c�digo para que o      *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
       Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS';
       Update SYN_PRC_DEFINICAO set
            CODIGO              = 'UTI\ITF\PRC\HONCONS'
           ,NOME                = 'Consolida��o de Documentos'
           ,TITULO              = 'Consolida��o de Documentos'
           ,FUN_TIPO            = 'F_REF'
           ,PRCDEF_ID           = mprcdef_prcdef_id
           ,NIVEL               = 4
           ,EXECUTAVEL_NOME     = ''
           ,ROTINA_DE_GERACAO   = ''
           ,CTRL_TIPO_PROCESSO  = ''
           ,ORDEM               = 11
           ,CALC_ORDEM          = 131
		   ,CALC_PATH           = '01.01.02.04.03.01.04.11.'
           ,ICONE_NUMERO        = 0
           ,NOME_DO_FONTE       = ''
           ,TAMANHO_DO_FONTE    = 10
           ,FONTE_NEGRITO       = 'N'
           ,IMPRESSORA          = ''
           ,ORIENTACAO          = 1
           ,FRM_ID              = mprcdef_frm_id
           ,IND_DEF_USUARIO     = 'S'
           ,IND_DESATIVADO      = 'N'
           ,HTML_MOD_ID         = ''
       Where ID = mid;
       Commit;
   Exception When NO_DATA_FOUND Then
       Select syn_seq_geral.nextval into mid from dual;
       Insert into SYN_PRC_DEFINICAO(
           ID
           ,CODIGO
           ,NOME
           ,TITULO
           ,FUN_TIPO
           ,PRCDEF_ID
           ,NIVEL
           ,EXECUTAVEL_NOME
           ,ROTINA_DE_GERACAO
           ,CTRL_TIPO_PROCESSO
           ,ORDEM
           ,CALC_ORDEM
		   ,CALC_PATH
           ,ICONE_NUMERO
           ,NOME_DO_FONTE
           ,TAMANHO_DO_FONTE
           ,FONTE_NEGRITO
           ,IMPRESSORA
           ,ORIENTACAO
           ,FRM_ID
           ,IND_DEF_USUARIO
           ,IND_DESATIVADO
           ,HTML_MOD_ID
           ) Values (
           mid
           ,'UTI\ITF\PRC\HONCONS'
           ,'Consolida��o de Documentos'
           ,'Consolida��o de Documentos'
           ,'F_REF'
           ,mprcdef_prcdef_id
           ,4
           ,''
           ,''
           ,''
           ,11
           ,131
		   ,'01.01.02.04.03.01.04.11.'
           ,0
           ,''
           ,10
           ,'N'
           ,''
           ,1
           ,mprcdef_frm_id
           ,'S'
           ,'N'
           ,''
           );
       Commit;
   End;
End;
---
  Select id into mprcdef_prcdef_id from syn_prc_definicao where codigo = 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF';
Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo c�digo antigo para que o    *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
  Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF';
  Update SYN_PRC_DEFINICAO set
   CODIGO              = 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF'
  ,NOME                = 'Consolida Documentos Fiscais'
  ,TITULO              = 'Consolida Documentos Fiscais'
  ,FUN_TIPO            = 'F_FUNC'
  ,PRCDEF_ID           = mprcdef_prcdef_id
  ,NIVEL               = 5
  ,EXECUTAVEL_NOME     = ''
  ,ROTINA_DE_GERACAO   = 'HON_CONSOLIDA_DOF'
  ,CTRL_TIPO_PROCESSO  = ''
  ,ORDEM               = 1
  ,CALC_ORDEM          = 132
  ,CALC_PATH           = '01.01.02.04.03.01.04.11.05.01.'
  ,ICONE_NUMERO        = 0
  ,NOME_DO_FONTE       = ''
  ,TAMANHO_DO_FONTE    = 10
  ,FONTE_NEGRITO       = 'N'
  ,IMPRESSORA          = ''
  ,ORIENTACAO          = 1
  ,FRM_ID              = mprcdef_frm_id
  ,IND_DEF_USUARIO     = 'S'
  ,IND_DESATIVADO      = 'N'
  ,HTML_MOD_ID         = ''
  Where ID = mid;
  Commit;
Exception When NO_DATA_FOUND Then
   Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo novo c�digo para que o      *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
       Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF';
       Update SYN_PRC_DEFINICAO set
            CODIGO              = 'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF'
           ,NOME                = 'Consolida Documentos Fiscais'
           ,TITULO              = 'Consolida Documentos Fiscais'
           ,FUN_TIPO            = 'F_FUNC'
           ,PRCDEF_ID           = mprcdef_prcdef_id
           ,NIVEL               = 5
           ,EXECUTAVEL_NOME     = ''
           ,ROTINA_DE_GERACAO   = 'HON_CONSOLIDA_DOF.transfere_dof'
           ,CTRL_TIPO_PROCESSO  = ''
           ,ORDEM               = 1
           ,CALC_ORDEM          = 132
		   ,CALC_PATH           = '01.01.02.04.03.01.04.11.05.01.'
           ,ICONE_NUMERO        = 0
           ,NOME_DO_FONTE       = ''
           ,TAMANHO_DO_FONTE    = 10
           ,FONTE_NEGRITO       = 'N'
           ,IMPRESSORA          = ''
           ,ORIENTACAO          = 1
           ,FRM_ID              = mprcdef_frm_id
           ,IND_DEF_USUARIO     = 'S'
           ,IND_DESATIVADO      = 'N'
           ,HTML_MOD_ID         = ''
       Where ID = mid;
       Commit;
   Exception When NO_DATA_FOUND Then
       Select syn_seq_geral.nextval into mid from dual;
       Insert into SYN_PRC_DEFINICAO(
           ID
           ,CODIGO
           ,NOME
           ,TITULO
           ,FUN_TIPO
           ,PRCDEF_ID
           ,NIVEL
           ,EXECUTAVEL_NOME
           ,ROTINA_DE_GERACAO
           ,CTRL_TIPO_PROCESSO
           ,ORDEM
           ,CALC_ORDEM
		   ,CALC_PATH
           ,ICONE_NUMERO
           ,NOME_DO_FONTE
           ,TAMANHO_DO_FONTE
           ,FONTE_NEGRITO
           ,IMPRESSORA
           ,ORIENTACAO
           ,FRM_ID
           ,IND_DEF_USUARIO
           ,IND_DESATIVADO
           ,HTML_MOD_ID
           ) Values (
           mid
           ,'UTI\ITF\PRC\HONCONS\HON_CONSOLIDA_DOF'
           ,'Consolida Documentos Fiscais'
           ,'Consolida Documentos Fiscais'
           ,'F_FUNC'
           ,mprcdef_prcdef_id
           ,5
           ,''
           ,'HON_CONSOLIDA_DOF.transfere_dof'
           ,''
           ,1
           ,132
		   ,'01.01.02.04.03.01.04.11.05.01.'
           ,0
           ,''
           ,10
           ,'N'
           ,''
           ,1
           ,mprcdef_frm_id
           ,'S'
           ,'N'
           ,''
           );
       Commit;
   End;
End;
---
SYN_PRC_DEFINICAO_PKG.ATUALIZA; 
SYN_PRC_DEFINICAO_PKG.VALIDA; 
END;
/
