--------------------------------------------------------
--  Arquivo criado - Ter�a-feira-Junho-19-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Trigger HON_CONSOLIDA_SISS_AIUR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "FAB4W"."HON_CONSOLIDA_SISS_AIUR" 
AFTER INSERT  OR UPDATE
ON cor_servico_iss
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink    VARCHAR2(30);
  v_schema    VARCHAR2(30);
  v_sql       VARCHAR2(32767);
  --
  v_count     integer;

PROCEDURE atualiza_origem_mao is
begin
   v_count := 0;     
   select count(1)   
     into v_count      
     from cor_servico_iss@lk_mao
    where siss_codigo = :new.siss_codigo;              
   if v_count > 0                                     
   then                                               
      update cor_servico_iss@lk_mao 
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                         
       where siss_codigo = :new.siss_codigo;
      commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION WHEN OTHERS THEN                           
rollback;
RAISE;                                               
END;

PROCEDURE atualiza_origem_hab is
begin
   v_count := 0;     
   select count(1)   
     into v_count      
     from cor_servico_iss@lk_hab
    where siss_codigo = :new.siss_codigo;              
   if v_count > 0                                     
   then                                               
      update cor_servico_iss@lk_hab
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                         
       where siss_codigo = :new.siss_codigo;                
      commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION WHEN OTHERS THEN                           
rollback;
RAISE;                                               
END;

PROCEDURE atualiza_origem_pecas is
begin
   v_count := 0;     
   select count(1)   
     into v_count      
     from cor_servico_iss@lk_pecas
    where siss_codigo = :new.siss_codigo;              
   if v_count > 0                                     
   then                                               
      update cor_servico_iss@lk_pecas 
         set hon_consolida_data   = sysdate,                  
             hon_consolida_status = '1'                         
       where siss_codigo = :new.siss_codigo;                
      commit;
      begin
      -- Atualizo registro a registro o controle de transferencia da tabela:
      HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'C');  
      commit;
      end;
   end if;                                              
EXCEPTION WHEN OTHERS THEN                           
rollback;
RAISE;                                               
END;


Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_mao;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_hab;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
  -- HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_SERVICO_ISS', 'C');  

End;
/
ALTER TRIGGER "FAB4W"."HON_CONSOLIDA_SISS_AIUR" ENABLE;
