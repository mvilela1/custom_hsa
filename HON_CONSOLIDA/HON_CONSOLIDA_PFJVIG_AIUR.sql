CREATE OR REPLACE TRIGGER HON_CONSOLIDA_PFJVIG_AIUR
AFTER INSERT  OR UPDATE
ON cor_pessoa_vigencia
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare
  PRAGMA AUTONOMOUS_TRANSACTION;
  v_dblink       VARCHAR2(30);
  v_schema       VARCHAR2(30);
  v_sql          VARCHAR2(32767);
  v_pfj_codigo   cor_pessoa_vigencia.pfj_codigo%TYPE;
  v_dt_inicio    cor_pessoa_vigencia.dt_inicio%TYPE;
  --
  v_count        integer;

PROCEDURE atualiza_origem_MAO is
begin
  v_count := 0;
  select count(1)
    into v_count   
    from cor_pessoa_vigencia@lk_mao
   where pfj_codigo = :new.pfj_codigo                     
     and dt_inicio  = :new.dt_inicio;
  if v_count > 0                     
  then                               
     update cor_pessoa_vigencia@lk_mao
        set hon_consolida_data   = sysdate,                    
            hon_consolida_status = '1'                        
      where pfj_codigo = :new.pfj_codigo                  
        and dt_inicio  = :new.dt_inicio;
     commit;	
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'C');  
     commit;
     end;
  end if;                                     
EXCEPTION WHEN OTHERS THEN                  
  rollback;
  RAISE;                                      
END;

PROCEDURE atualiza_origem_HAB is
begin
  v_count := 0;
  select count(1)
    into v_count   
    from cor_pessoa_vigencia@lk_HAB
   where pfj_codigo = :new.pfj_codigo                     
     and dt_inicio  = :new.dt_inicio;
  if v_count > 0                     
  then                               
     update cor_pessoa_vigencia@lk_HAB
        set hon_consolida_data   = sysdate,                    
            hon_consolida_status = '1'                        
      where pfj_codigo = :new.pfj_codigo                  
        and dt_inicio  = :new.dt_inicio;         
	 commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'C');  
     commit;
     end;
  end if;                                     
EXCEPTION WHEN OTHERS THEN                  
  rollback;
  RAISE;                                      
END;

PROCEDURE atualiza_origem_PECAS is
begin
  v_count := 0;
  select count(1)
    into v_count   
    from cor_pessoa_vigencia@lk_pecas
   where pfj_codigo = :new.pfj_codigo                     
     and dt_inicio  = :new.dt_inicio;
  if v_count > 0                     
  then                               
     update cor_pessoa_vigencia@lk_pecas
        set hon_consolida_data   = sysdate,                    
            hon_consolida_status = '1'                        
      where pfj_codigo = :new.pfj_codigo                  
        and dt_inicio  = :new.dt_inicio;         
	 commit;
     begin
     -- Atualizo registro a registro o controle de transferencia da tabela:
     HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'C');  
     commit;
     end;
  end if;                                     
EXCEPTION WHEN OTHERS THEN                  
  rollback;
  RAISE;                                      
END;


Begin
  -- Origem MAO
  if :new.HON_CONSOLIDA_ORIGEM = 'MAO'
  then
     atualiza_origem_MAO;
  end if;

  -- Origem HAB
  if :new.HON_CONSOLIDA_ORIGEM = 'HAB'
  then
     atualiza_origem_HAB;
  end if;

  -- Origem PECAS
  if :new.HON_CONSOLIDA_ORIGEM = 'PECAS'
  then
     atualiza_origem_pecas;
  end if;
  -- Atualizo registro a registro o controle de transferencia da tabela:
  -- HON_CONSOLIDA_CTRL_TRANSF_PRC(:new.HON_CONSOLIDA_ORIGEM, 'COR_PESSOA_VIGENCIA', 'C');  

End;

--ALTER TRIGGER HON_CONSOLIDA_PFJVIG_AIUR ENABLE;
