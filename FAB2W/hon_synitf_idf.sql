 
--**********************************************************************************************
-- SYNCHRO SISTEMAS DE INFORMACAO
-- 
-- Script da Interface...: HON_SYNITF_IDF
-- Data de criacao.......: 12/07/2018 13:36:11
-- 
-- Descricao.............: Este script contem uma copia da INTERFACE selecionada.
-- 
-- Etapas................: 1) Apaga INTERFACE--> VISAO--> REPOSITORIO caso existirem
--                            OBS:Pode ocorrer do REPOSITORIO estar relacionada com
--                                mais de uma VISAO, porem o tratamento para delecao
--                                do REPOSITORIO � tratado no scrit.
--                         2) Insere REPOSITORIO--> VISAO--> INTERFACE 
-- **********
-- ATENCAO!!!
-- **********
-- IMPORTANTE: Antes de rodar este script, verifique se a INTERFACE em questao foi customizada.
--**********************************************************************************************
----------------------
-- INICIO DEPENDENCIAS 
----------------------
 BEGIN 
 SYN_DYNAMIC.DDL('ALTER TABLE SYN_INTERFACE ADD (VERSAO_OPEN VARCHAR2(10))', -01430);
 EXCEPTION WHEN OTHERS THEN
 BEGIN
 SYN_DYNAMIC.DDL('ALTER TABLE SYN_INTERFACE MODIFY (VERSAO_OPEN VARCHAR2(10))');
 END;
-------------------
-- FIM DEPENDENCIAS 
-------------------
 END;
/
--------------------
-- INICIO DECLARE 1 
--------------------
DECLARE
    mREP_ID                  dic_repositorio.id%TYPE;
    mREP_REP_ID              dic_repositorio.id%TYPE;
    mREP_ID_CURSOR           dic_repositorio.id%TYPE;
    mREP_ID_RLC              dic_repositorio.id%TYPE;
    mREP_ID_JUNCAO           dic_repositorio.id%TYPE;
    mVIS_ID                  dic_visao.id%TYPE;
    mDOM_ID                  dic_dominio.id%TYPE;
    mREPCLA_ID_W             dic_rep_clausula.id%TYPE:=NULL;
    mREPCLA_ID_G             dic_rep_clausula.id%TYPE:=NULL;
    mREPCLA_ID_O             dic_rep_clausula.id%TYPE:=NULL;
    mREPCLA_ID_H             dic_rep_clausula.id%TYPE:=NULL;
    mREPCLA_ID_GERAL         dic_rep_clausula.id%TYPE:=NULL;
    mVSQL_ID                 dic_variavel_plsql.id%TYPE:=NULL;
    mRLC_ID                  dic_relacionamento.id%TYPE:=NULL;
    mRLCFT_ID                dic_rlc_from_to.id%TYPE:=NULL;
    mREPJUN_ID               dic_rep_juncao.id%TYPE:=NULL;
    mREPJUN_REPJUN_ID        dic_rep_juncao.id%TYPE:=NULL;
    mDAD_ID                  dic_dado.id%TYPE;
    mITF_ID                  syn_interface.id%TYPE;
    mNUM_REG_COMMIT          syn_interface.num_reg_commit%TYPE;
    mIND_DESATIVADO          syn_interface.ind_desativado%TYPE;
    E                        VARCHAR2(1):=chr(13); -- ENTER
    T                        VARCHAR2(1):=chr(10); -- TAB
    mRetaux                  dic_rep_clausula.id%TYPE;
    mNumero_backup_no_dia    INTEGER:=0;
    mresp                    VARCHAR2(1):='S';
    
------------------
-- INICIO BLOCO 1 
------------------
BEGIN
    IF mresp!='S' THEN
        RETURN;
    END IF;
    ----------------------------------------------------------------------
    -- IMPORTACAO DO REPOSITORIO, VISAO E INTERFACE                     --
    ----------------------------------------------------------------------
    BEGIN -- Pego o NUM_REG_COMMIT e IND_DESATIVADO da interface alterada pelo usuario
        SELECT ID, NUM_REG_COMMIT, IND_DESATIVADO 
          INTO mITF_ID, mNUM_REG_COMMIT, mIND_DESATIVADO 
          FROM SYN_INTERFACE WHERE CODIGO ='HON_SYNITF_IDF';
        --************************************************************************
        --Preciso verificar se ja existe um backup no dia atual para uma mesma ITF
        --Caso exista, nao faco backup
        SELECT COUNT(*) INTO mNumero_backup_no_dia FROM SYN_ITF_HISTORICO WHERE CODIGO_ITF='HON_SYNITF_IDF' AND DT_ATUALIZACAO= Trunc(SYSDATE);
        IF mNumero_backup_no_dia = 0 THEN
            --Caso a ITF ja exista faco o Backup da ITF antes de apaga-la
            SYN_GERA_SCRIPT_INTERFACE( 'HON_SYNITF_IDF', NULL, 'S' );
            COMMIT;
        END IF;
        --************************************************************************
    EXCEPTION
         WHEN NO_DATA_FOUND THEN
              mNUM_REG_COMMIT := 1;
              mIND_DESATIVADO := 'N';
              SELECT SYN_SEQ_INTERFACE.NEXTVAL INTO mITF_ID FROM DUAL;
         WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR(-20001, 'Nao foi possivel fazer o backup da Interface.');
    END;
    
    ------------------------------
    -- APAGA ITF --> VISAO --> REP
    ------------------------------
    Syn_Gera_Script_pkg.Delete_Interface('HON_SYNITF_IDF');
    
    ------------------------
    -- 1) DIC_REPOSITORIO --
    ------------------------
    DIC_REPOSITORIO_PKG.DISABLE;
    BEGIN
    -- Inserindo SYNITF_IDF na DIC_REPOSITORIO
    INSERT INTO DIC_REPOSITORIO (
     ID
     ,REP_TYPE
     ,NOME
     ,ALIAS
     ,IND_DEF_USUARIO
     ,IND_CRIACAO
     ,IND_EXTENSAO
     ,REP_ID
     ,STORAGE_CRIACAO
     ,SCHEMA_CRIACAO
     ,EXPRESSAO_NOME
     ,COMENTARIO
     ,IND_INVALIDO
     ,IND_DESATIVADO
     ,SQL_OPTIMIZER_HINTS
    ) VALUES (
    DIC_SEQ_GERAL.NEXTVAL
    ,'T'
    ,'SYNITF_IDF'
    ,'SYNITF_IDF'
    ,'S'
    ,'S'
    ,'S'
    , NULL
    ,''
    ,''
    ,''
    ,''
    ,'N'
    ,'N'
    ,'');
    EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
    DIC_REPOSITORIO_PKG.ENABLE;
    
    -- Pego o ID do novo repositorio inserido acima
    mREP_ID  := Syn_Gera_Script_pkg.Pego_ID_Repositorio ( 'SYNITF_IDF' );
        -------------------
        -- 1.1) DIC_DADO --
        -------------------
        
        ---------------------------
        -- 1.2) DIC_REP_CLAUSULA --
        ---------------------------
        
        ------------------
        -- 2) DIC_VISAO --
        ------------------
        -- Inserindo na DIC_VISAO.VIS_HON_SYNITF_IDF
        dic_visao_pkg.Disable;
        BEGIN
        INSERT INTO DIC_VISAO ( ID ,TITULO ,REP_ID ,IND_DISTINCT ,DESCRICAO ,SQL_OPTIMIZER_HINTS ,SQL_FROM ,SQL_WHERE ,SQL_GROUP_BY ,SQL_ORDER_BY ,SQL_HAVING ,IND_INVALIDO ,IND_DESATIVADO ,REPTAB_ID ,REPCV_ID ,REPCLA_ID_WHERE ,REPCLA_ID_GROUP_BY ,REPCLA_ID_ORDER_BY ,REPCLA_ID_HAVING ) VALUES (DIC_SEQ_GERAL.NEXTVAL,'VIS_HON_SYNITF_IDF', mREP_ID ,'N','(COPIA) - Visao do repositorio de Itens de Documentos Fiscais SYNITF_IDF',''
        ,'SYNITF_IDF'
        ,''
        ,''
        ,''
        ,''
        ,'N','N', NULL, NULL, mREPCLA_ID_W, mREPCLA_ID_G, mREPCLA_ID_O, mREPCLA_ID_H);
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        dic_visao_pkg.Enable;
        
        -- Pego o ID da nova visao inserida acima
        mVIS_ID := Syn_Gera_Script_pkg.Pego_Vis_Id ( mREP_ID, 'VIS_HON_SYNITF_IDF' );
        -----------------------------
        -- 2.1) DIC_VIS_INFORMACAO --
        -----------------------------
        
        ------------------------
        -- 2.2) SYN_INTERFACE --
        ------------------------
        BEGIN
        INSERT INTO SYN_INTERFACE ( ID ,CODIGO ,TITULO ,VIS_ID ,CTRL_COMMIT ,IND_INVALIDO ,IND_DESATIVADO ,IND_DEF_USUARIO ,NUM_REG_COMMIT ,VERSAO_OPEN) VALUES ( mITF_ID,'HON_SYNITF_IDF','DOF - Itens de Documentos Fiscais - (COPIA)', mVIS_ID ,'R','N', mIND_DESATIVADO ,'S', mNUM_REG_COMMIT ,'1.0');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
 
EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
------------------
-- FIM DO BLOCO 1 
------------------
END;
/
        
--------------------
-- INICIO DECLARE 2 
--------------------
DECLARE
    mREP_ID                  dic_repositorio.id%TYPE;
    mVIS_ID                  dic_visao.id%TYPE;
    mREP_ID_CURSOR           dic_repositorio.id%TYPE;
------------------
-- INICIO BLOCO 2 
------------------
BEGIN
 
        mREP_ID  := Syn_Gera_Script_pkg.Pego_ID_Repositorio ( 'SYNITF_IDF' );
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','48','AM_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','AM_CODIGO','Codigo da aplicacao da mercadoria.','Codigo da aplicacao da mercadoria','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','49','CFOP_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','8','','CFOP_CODIGO','Codigo fiscal de operacao e prestacao.','CFOP do item','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','101','CHASSI_VEIC','N','N','S','N','S','N','S','N','N','VARCHAR2','50','','CHASSI_VEIC','Chassi do veiculo.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','91','CODIGO_RETENCAO','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO','Codigo de retencao do IRRF.','Codigo de Retencao IRRF','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','102','CONTA_CONTABIL','N','N','S','N','S','N','S','N','N','VARCHAR2','100','','CONTA_CONTABIL','Codigo da conta contabil.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','75','CTRL_INSTRUCAO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','CTRL_INSTRUCAO','Controle de Instrucao (I)nclusao, (A)lteracao, (E)xclusao ou (M)ix inclui ou altera.','Instrucao de importacao para a Open Interface (I=inclusao; A=alteracao; E=exclusao; M=alteracao ou inclusao)','''M''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','77','DH_CRITICA','N','N','S','N','S','N','S','N','N','DATE','','','DH_CRITICA','(Uso exclusivo da Synchro) Data da ultima critica.','Data da critica da ultima importacao da Open Interface','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','4','DOF_IMPORT_NUMERO','N','N','S','N','S','N','S','N','N','VARCHAR2','40','','DOF_IMPORT_NUMERO','Codigo (ID) do DOF no sistema / tabela origem.','ID do DOF no sistema de origem','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','90','EMERC_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','EMERC_CODIGO','Codigo da estrutura da mercadoria.','C�digo da Estrutura de mercadoria','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','51','ENTSAI_UNI_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','ENTSAI_UNI_CODIGO','Codigo da unidade de medida do documento.','Codigo da unidade de medida do item constante no DOF','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','52','FIN_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','4','','FIN_CODIGO','Codigo da finalidade de aquisicao cadastrado.','Codigo da finalidade de aquisicao','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','53','IDF_TEXTO_COMPLEMENTAR','N','N','S','N','S','N','S','N','N','VARCHAR2','1000','','IDF_TEXTO_COMPLEMENTAR','Texto complementar','Texto complementar para o item','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','98','IND_VL_ICMS_NO_PRECO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_VL_ICMS_NO_PRECO','Indica se o valor do ICMS esta embutido no preco unitario ou no preco total.','Indica se o Valor do ICMS esta embutido ou nao no Preco do item','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','1','INFORMANTE_EST_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','INFORMANTE_EST_CODIGO','Codigo do estabelecimento informante do DOF.','Estabelecimento informante do DOF','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','3','INFORMANTE_PFJ_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','INFORMANTE_PFJ_ORIGEM','Nome do sistema ou tabela origem do estabelecimento informante do DOF.','PFJ informante - Sistema/Tabela de origem','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','55','MERC_CHAVE_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','MERC_CHAVE_ORIGEM','Codigo da mercadoria no sistema / tabela origem.','Mercadoria - ID no sistema de origem','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','54','MERC_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','60','','MERC_CODIGO','Codigo da Mercadoria no item','Codigo da mercadoria','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','56','MERC_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','MERC_ORIGEM','Nome do sistema ou tabela origem da mercadoria.','Mercadoria - Sistema/tabela de origem','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','57','MUN_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','60','','MUN_CODIGO','Codigo do Municipio.','Codigo do municipio produtor','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','58','NBM_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','NBM_CODIGO','Codigo NCM da mercadoria (Nomenclatura Comum do Mercosul).','Codigo da NCM','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','59','NOP_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','NOP_CODIGO','Codigo da natureza de operacao (Modulo Fiscal).','Natureza de Operacao (NOP) do item','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','60','NUM_DI','N','N','S','N','S','N','S','N','N','VARCHAR2','12','','NUM_DI','Numero da DI (declaracao de importacao).','Numero da DI (declaracao de importacao)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','62','OBS_FISCAL_IPI','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_IPI','Mensagens de IPI associadas ao DOF.','Observacao fiscal quanto ao IPI','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','63','OBS_FISCAL_IRRF','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_IRRF','Mensagens de IRRF associadas ao DOF.','Observacao fiscal quanto ao IRRF','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','64','OBS_FISCAL_ISS','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_ISS','Mensagens de ISS associadas ao DOF.','Observacao fiscal quanto ao ISS','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','65','OBS_FISCAL_STF','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_STF','Mensagens de STF (substituicao tributaria para frente) associadas ao DOF.','Observacao fiscal quanto a STF','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','8','ALIQ_II','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_II','Aliquota do II (Imposto de Importacao).','Aliquota de II (imposto de importacao)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','81','ALIQ_INSS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_INSS','Aliquota do INSS (Empresa).','Al�quota INSS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','93','ALIQ_INSS_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_INSS_RET','Aliquota para calculo da retencao do INSS.','Aliquota de INSS retido','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','222','VL_NAO_RETIDO_CP','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_NAO_RETIDO_CP','Valor das contribuicoes previdenciarias nao retido ou depositada em juizo em decorrencia da decisao judicial  (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','223','CNPJ_SUBEMPREITEIRO','N','N','S','N','S','N','S','N','N','VARCHAR2','14','','CNPJ_SUBEMPREITEIRO','CNPJ do Subempreiteiro (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','224','VL_MATERIAS_EQUIP','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_MATERIAS_EQUIP','Valor dos materiais e equipamentos fornecidos, desde que previstos em contrato (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','224','VL_GILRAT','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_GILRAT','Valor da contribuicao destinada ao financiamento dos beneficios concedidos em razao do grau de incidencia da incapacidade laborativa decorrente dos riscos ambientais do trabalho, incidente sobre a aquisicao de producao rural de produtor rural pessoa fisica (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','225','VL_SENAR','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_SENAR','Valor da contribuicao destinada ao SENAR, incidente sobre a aquisicao de producao rural de produtor rural pessoa fisica/segurado especial (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','226','IND_MP_DO_BEM','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_MP_DO_BEM','Indicador do item sujeito a MP do Bem.','','''S''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','66','OBS_FISCAL_STT','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_STT','Mensagens de STT (substituicao tributaria para tras) associadas ao DOF.','Observacao fiscal quanto a STT','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','68','OM_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','OM_CODIGO','Codigo da origem da mercadoria (tabela oficial).','Origem da mercadoria (tabela oficial)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','10','ALIQ_ISS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ISS','Aliquota do ISS.','Aliquota de ISS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','11','ALIQ_STF','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_STF','Aliquota de ICMS de substituicao tributaria para frente (STF).','Aliquota de STF (Substituicao Tributaria para Frente)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','73','STC_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','STC_CODIGO','Codigo situacao tributaria  do documento fiscal (DOF).','Situacao tributaria do ICMS (tabela oficial)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','74','STP_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','STP_CODIGO','Codigo situacao tributaria  do IPI (tabela oficial).','Situacao tributaria do IPI (tabela oficial)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','17','SUBCLASSE_IDF','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','SUBCLASSE_IDF','Tipo de item.','Tipo de item (M=mercadoria; P=prestacao; S=servico; O=outros)','''M''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','79','TIPO_COMPLEMENTO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','TIPO_COMPLEMENTO','Indica qual tipo de complemento do item.','Tipo de complemento, para DOF de complemento (P=preco; I=imposto; PI=preco+imposto)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','100','TIPO_OPER_VEIC','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','TIPO_OPER_VEIC','Indicador do tipo de venda com veiculo (0-Venda p concessionaria, 1-Faturamento direto, 2-Venda direta)','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','228','PERC_TRIBUTAVEL_STF','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_TRIBUTAVEL_STF','Percentual da Base de Calculo do STF (substituicao tributaria para frente) classificado como Tributavel','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','227','VL_ICMS_SIMPLES_NAC','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS_SIMPLES_NAC','Valor de credito do ICMS na aquisicao de mercadorias adquiridas de ME ou EPP Optante pelo Simples Nacional para industrializacao ou comercializacao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','6','ALIQ_DIFA','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_DIFA','Aliquota do DIFA.','Aliquota do diferencial (direfencial de aliquotas)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','7','ALIQ_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS','Aliquota do ICMS.','Aliquota de ICMS do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','211','VL_ABAT_LEGAL_ISS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ABAT_LEGAL_ISS','Valor de abatimento legal da base do imposto de ISS Retido.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','212','VL_ABAT_LEGAL_INSS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ABAT_LEGAL_INSS_RET','Valor de abatimento legal da base do imposto de INSS Retido','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','213','VL_ABAT_LEGAL_IRRF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ABAT_LEGAL_IRRF','Valor de abatimento legal da base do imposto de IRRF Retido.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','214','DESONERACAO_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','DESONERACAO_CODIGO','Codigo do motivo da desoneracao do ICMS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','215','IND_EXIGIBILIDADE_ISS','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','IND_EXIGIBILIDADE_ISS','Indicador da exigibilidade do ISS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','216','IND_INCENTIVO_FISCAL_ISS','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_INCENTIVO_FISCAL_ISS','Indicador de incentivo fiscal.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','217','DT_REF_CALC_IMP_IDF','N','N','S','N','S','N','S','N','N','DATE','','','DT_REF_CALC_IMP_IDF','Data de referencia de calculo para os itens dos documentos fiscais.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','218','VL_SERVICO_AE15','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_SERVICO_AE15','Valor dos Servicos prestados por segurados em condicoes especiais, cuja atividade permita concessao de aposentadoria especial apos 15 anos de contribuicao (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','219','VL_SERVICO_AE20','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_SERVICO_AE20','Valor dos Servicos prestados por segurados em condicoes especiais, cuja atividade permita concessao de aposentadoria especial apos 20 anos de contribuicao (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','220','VL_SERVICO_AE25','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_SERVICO_AE25','Valor dos Servicos prestados por segurados em condicoes especiais, cuja atividade permita concessao de aposentadoria especial apos 25 anos de contribuicao (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','221','VL_ADICIONAL_RET_AE','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ADICIONAL_RET_AE','Adicional de retencao relativo aos servicos prestados em condicoes especiais e insalubres cuja atividade permita concessao de aposentadoria especial (e-Social)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','9','ALIQ_IPI','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_IPI','Aliquota de IPI.','Aliquota de IPI','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','12','ALIQ_STT','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_STT','Aliquota de ICMS de substituicao tributaria para tras (STT).','Aliquota de STT (Substituicao Tributaria para Tras)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','92','CODIGO_ISS_MUNICIPIO','N','N','S','N','S','N','S','N','N','VARCHAR2','60','','CODIGO_ISS_MUNICIPIO','Codigo do fiscal do servico no municipio onde o ISS sera recolhido.','Codigo do servico no municipio de ISS','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','18','VL_AJUSTE_PRECO_TOTAL','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_AJUSTE_PRECO_TOTAL','Valor do ajuste (+/-) do Preco Total do item, ou seja o ajuste dados no item.','Valor do ajuste (+/-) do preco total do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','84','VL_ALIQ_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','VL_ALIQ_COFINS','Aliquota da COFINS (Contribuicao para Financiamento da Seguridade Social).','Aliquota COFINS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','85','VL_ALIQ_PIS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','VL_ALIQ_PIS','Aliquota do PIS/PASEP.','Aliquota PIS/PASEP','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','87','VL_BASE_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_COFINS','Valor base de calculo da COFINS (Contribuicao para Financiamento da Seguridade Social).','Valor Base COFINS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','19','VL_BASE_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS','Valor da base de calculo do ICMS','Valor da base plena de ICMS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','95','VL_BASE_INSS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_INSS_RET','Valor da base de caculo do INSS para retencao do INSS.','Valor base INSS retido','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','20','VL_BASE_IPI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_IPI','Valor base para calculo de IPI.','Valor da base plena de IPI','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','22','VL_BASE_ISS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ISS','Valor da base de calculo do ISS.','Valor da base de ISS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','86','VL_BASE_PIS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_PIS','Valor base para calculo do PIS/PASEP.','Valor Base PIS/PASEP','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','23','VL_BASE_STF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_STF','Valor base para calculo do ICMS por substituicao tributaria para frente.','Valor da base plena de STF','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','25','VL_CONTABIL','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CONTABIL','Informar o valor contabil do item (normalmente eh o valor fiscal + rateio frete + rateio seguro + rateio oda + valor ipi + valor stf + valor stt + valor ii).','Valor contabil do item (fiscal + rateio frete + rateio seguro + rateio oda + ipi + stf + stt + ii)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','26','VL_DIFA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_DIFA','Valor do  base tributavel do diferencial de aliquotas.','Valor do diferencial de aliquota','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','27','VL_FATURADO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_FATURADO','Informar o valor faturado do item, normalmente e igual ao valor contabil do item.','Valor faturado do item (fiscal + rateio frete + rateio seguro + rateio oda + ipi + stt + stf - irrf + ii)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','29','VL_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS','Valor do ICMS. ','Valor do ICMS do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','30','VL_II','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_II','Valor do Imposto de Importacao.','Valor do II do item (imposto de importacao)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','89','VL_IMPOSTO_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IMPOSTO_COFINS','Valor do COFINS (Contribuicao para Financiamento da Seguridade Social).','Valor de Imposto COFINS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','88','VL_IMPOSTO_PIS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IMPOSTO_PIS','Valor do PIS/PASEP.','Valor de Imposto PIS/PASEP','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','83','VL_INSS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS','Valor do INSS retido.','Valor do INSS para o item da NF','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','99','VL_IOF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IOF','Valor do Imposto sobre Operacoes de Financeiras.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','31','VL_IPI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IPI','Valor do IPI. ','Valor do IPI do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','33','VL_ISENTO_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ISENTO_ICMS','Valor da coluna isentos de ICMS.','Valor da "coluna isentos" de ICMS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','34','VL_ISENTO_IPI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ISENTO_IPI','Valor da coluna isentos de IPI.','Valor da "coluna isentos" de IPI','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','97','VL_OUTROS_ABAT','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_OUTROS_ABAT','Valor de outros abatimentos.','Valor de outros abatimentos','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','36','VL_OUTROS_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_OUTROS_ICMS','Valor de outros de ICMS (Convenio ICMS).','Valor da "coluna outros" de ICMS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','78','CTRL_CRITICA','N','N','S','N','S','N','S','N','N','NUMBER','','','CTRL_CRITICA','(Uso exclusivo da Synchro) Quantidade de vezes que o registro ja foi criticado.','Numero de vezes que o registro foi criticado pela Open Interface','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','37','VL_OUTROS_IPI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_OUTROS_IPI','Valor de outros de IPI (Convenio ICMS).','Valor da "coluna outros" de IPI','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','38','VL_RATEIO_AJUSTE_PRECO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_AJUSTE_PRECO','Valor do rateio do ajuste (+/-) do DOF para o item.','Valor do rateio do ajuste (+/-) do DOF para o item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','41','VL_RATEIO_SEGURO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_SEGURO','Informar o valor do rateio do seguro do DOF para o item.','Valor do rateio do seguro do DOF para o item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','42','VL_STF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_STF','Valor do ICMS de substituicao tributaria para frente (STF).','Valor da STF','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','43','VL_STT','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_STT','Valor do ICMS de substituicao tributaria para tras (STT).','Valor da STT','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','80','VL_TRIBUTAVEL_DIFA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_DIFA','Somatorio dos valores tributaveis de base tributavel do diferencial de aliquota ICMS dos itens.','Valor tributavel do diferencial de aliquota do item','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','45','VL_TRIBUTAVEL_IPI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_IPI','Valor tributavel de IPI.','Valor tributavel de IPI (usado para calculo do imposto)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','46','VL_TRIBUTAVEL_STF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_STF','Valor tributavel de STF.','Valor tributavel de STF (usado para calculo do imposto)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','47','VL_TRIBUTAVEL_STT','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_STT','Valor tributavel de STT.','Valor tributavel de STT (usado para calculo do imposto)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','39','VL_RATEIO_FRETE','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_FRETE','Informar o valor do rateio do frete do DOF para o item.','Valor do rateio do frete do DOF para o item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','103','DH_INCLUSAO','N','N','S','N','S','N','S','N','N','DATE','','','DH_INCLUSAO','(Uso exclusivo da Synchro) Data de inclusao do registro na tabela de Open Interface.','','sysdate','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','13','ORD_IMPRESSAO','N','N','S','N','S','N','S','N','N','NUMBER','8','','ORD_IMPRESSAO','Ordem de impressao do item no formulario.','Ordem de impressao dos itens no formulario','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','94','PERC_OUTROS_ABAT','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_OUTROS_ABAT','Percentual de outros abatimentos.','Percentual de outros abatimentos','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','69','PESO_BRUTO_KG','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PESO_BRUTO_KG','Peso bruto da mercadoria.','Peso bruto da mercadoria','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','70','PESO_LIQUIDO_KG','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PESO_LIQUIDO_KG','Peso liquido da mercadoria.','Peso liquido da mercadoria','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','14','PRECO_TOTAL','N','N','S','N','S','N','S','N','N','NUMBER','19','2','PRECO_TOTAL','Preco total do item.','Preco total do item (quantidade X preco unitario)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','15','PRECO_UNITARIO','N','N','S','N','S','N','S','N','N','NUMBER','','','PRECO_UNITARIO','Preco unitario do item.','Preco unitario do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','71','PRES_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','PRES_CODIGO','Codigo da prestacao de servico (para prestacoes de servico sujeitas ao ICMS).','Codigo da prestacao (servico sujeito a ICMS)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','16','QTD','N','N','S','N','S','N','S','N','N','NUMBER','','','QTD','Quantidade do item da nota fiscal.','Quantidado do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','72','SISS_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','SISS_CODIGO','Codigo do servico tributado por ISS.','Codigo do servico (sujeito a ISS)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','82','VL_BASE_INSS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_INSS','Valor da base de caculo do INSS.','Valor da base de c�lculo de INSS','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','21','VL_BASE_IRRF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_IRRF','Valor base para calculo do IRRF.','Valor da base de IRRF','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','24','VL_BASE_STT','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_STT','Valor base para calculo do ICMS por substituicao tributaria para tras.','Valor da base plena de STT','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','28','VL_FISCAL','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_FISCAL','Informar o valor fiscal do item (normalmente eh o preco total + ajuste preco total + rateio ajuste preco).','Valor fiscal do item em moeda nacional (preco total + ajuste preco total + rateio ajuste preco)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','96','VL_INSS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_RET','Valor do INSS retido','Valor total INSS retido.','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','32','VL_IRRF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IRRF','Valor total do IRRF.','Valor do IRRF do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','35','VL_ISS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ISS','Valor do ISS.','Valor do ISS do item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','40','VL_RATEIO_ODA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_ODA','Informar o valor do rateio de ODA (Outras Despesas Acessorias) do DOF para o item.','Valor do rateio de outras despesas acess�rias do DOF para o item','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','44','VL_TRIBUTAVEL_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_ICMS','Valor tributavel de ICMS.','Valor tributavel de ICMS (usado para o calculo do imposto)','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','50','DT_DI','N','N','S','N','S','N','S','N','N','DATE','','','DT_DI','Data da declaracao de importacao (DI).','Data da DI (declaracao de importacao)','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','5','IDF_NUM','N','N','S','N','S','N','S','N','N','NUMBER','6','','IDF_NUM','Numero sequencial do item.','Numero sequencial do item','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','2','INFORMANTE_PFJ_CHAVE_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','INFORMANTE_PFJ_CHAVE_ORIGEM','Codigo do estabelecimento informante do DOF no sistema / tabela origem.','PFJ informante - ID no sistema de origem','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','76','MSG_CRITICA','N','N','S','N','S','N','S','N','N','VARCHAR2','2000','','MSG_CRITICA','(Uso exclusivo da Synchro) Mensagem de Critica.','Mensagem de critica de importacao da Open Interface','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','61','OBS_FISCAL_ICMS','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_ICMS','Mensagens de ICMS associadas ao DOF.','Observacao fiscal quanto ao ICMS','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','67','OBS_FISCAL_OPERACIONAL','N','N','S','N','S','N','S','N','N','VARCHAR2','4000','','OBS_FISCAL_OPERACIONAL','Mensagem operacional associada ao DOF.','Observacao fiscal Operacional','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','104','STA_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','STA_CODIGO','Codigo da Situacao Tributaria do PIS (tabela oficial).','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','105','STN_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','STN_CODIGO','Codigo da Situacao Tributaria do COFINS (tabela oficial).','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','106','ALIQ_PIS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_PIS','Valor da aliquota do PIS de pauta.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','107','QTD_BASE_PIS','N','N','S','N','S','N','S','N','N','NUMBER','15','3','QTD_BASE_PIS','Valor da base do PIS de pauta.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','108','VL_PIS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_PIS','Valor do imposto PIS.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','109','ALIQ_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_COFINS','Valor da aliquota do COFINS de pauta.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','110','QTD_BASE_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','15','3','QTD_BASE_COFINS','Valor da base da COFINS de pauta.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','111','VL_COFINS','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_COFINS','Valor do imposto COFINS.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','112','NUM_TANQUE','N','N','S','N','S','N','S','N','N','VARCHAR2','50','','NUM_TANQUE','Tanque onde o combustivel foi armazenado.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','113','DT_FAB_MED','N','N','S','N','S','N','S','N','N','DATE','','','DT_FAB_MED','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','114','TIPO_PROD_MED','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','TIPO_PROD_MED','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','115','PARTICIPANTE_PFJ_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','PARTICIPANTE_PFJ_CODIGO','Codigo do participante (do emitente do documento relativa a ultima entrada sujeita a ST).','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','116','CLI_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','5','','CLI_CODIGO','Codigo da classe de enquadramento do IPI.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','141','VL_DEDUCAO_PENSAO_PRG','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_DEDUCAO_PENSAO_PRG','Valor da deducao da pensao alimenticia/judicial para o IRRF.','','''0''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','142','VL_DEDUCAO_DEPENDENTE_PRG','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_DEDUCAO_DEPENDENTE_PRG','Valor da deducao do(s) dependente(s) para o IRRF.','','''0''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','109','VL_ST_FRONTEIRA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','','','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','138','NUM_CANO','N','N','S','N','S','N','S','N','N','VARCHAR2','50','','NUM_CANO','Numeracao de serie de fabricacao do cano.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','139','UF_PAR','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','UF_PAR','Sigla da unidade federativa do participante para venda de veiculos, comunicacoes e telecomunicacoes.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','140','VL_TAB_MAX','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TAB_MAX','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','143','PERC_IRRF','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_IRRF','Aliquota do IRRF (Imposto de Renda Retido na Fonte).','','''0''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','117','SISS_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','SISS_ORIGEM','Nome do sistema / tabela origem do registro do servico.','','''IMPORTADO''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','118','SISS_CHAVE_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','SISS_CHAVE_ORIGEM','Codigo do servico (ISS) no sistema / tabela origem.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','119','PRES_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','PRES_ORIGEM','Nome do Sistema ou Tabela origem da Prestacao de Servico (ICMS).','','''IMPORTADO''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','120','PRES_CHAVE_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','PRES_CHAVE_ORIGEM','Codigo da Prestacao de Servico no Sistema / Tabela origem.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','121','TIPO_STF','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','TIPO_STF','(Nao Utilizar) Coluna nao utilizada na open interface - desconsidera-la.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','122','VL_BASE_STF_FRONTEIRA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_STF_FRONTEIRA','(Nao Utilizar) Coluna nao utilizada na open interface - desconsidera-la.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','123','VL_BASE_STF_IDO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_STF_IDO','(Nao Utilizar) Coluna nao utilizada na open interface - desconsidera-la.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','124','VL_STF_FRONTEIRA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_STF_FRONTEIRA','(Nao Utilizar) Coluna nao utilizada na open interface - desconsidera-la.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','125','VL_STF_IDO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_STF_IDO','(Nao Utilizar) Coluna nao utilizada na open interface - desconsidera-la.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','126','STI_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','STI_CODIGO','Codigo da situacao tributaria para ISS.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','127','CNPJ_PAR','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','CNPJ_PAR','CNPJ do participante para venda de veiculos, comunicacoes e telecomunicacoes.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','128','COD_IATA_FIM','N','N','S','N','S','N','S','N','N','VARCHAR2','3','','COD_IATA_FIM','Codigo IATA da cidade final do voo no conhecimento aereo.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','129','COD_IATA_INI','N','N','S','N','S','N','S','N','N','VARCHAR2','3','','COD_IATA_INI','Codigo IATA da cidade de inicio do voo no conhecimento aereo.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','130','CPF_PAR','N','N','S','N','S','N','S','N','N','VARCHAR2','19','','CPF_PAR','CPF do participante para venda de veiculos, comunicacoes e telecomunicacoes.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','131','DT_VAL','N','N','S','N','S','N','S','N','N','DATE','','','DT_VAL','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','132','IE_PAR','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','IE_PAR','Inscricao Estadual do participante.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','133','IM_SUBCONTRATACAO','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','IM_SUBCONTRATACAO','Inscricao municipal do subcontratado.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','134','IND_ARM','N','N','S','N','S','N','S','N','N','NUMBER','1','','IND_ARM','Indicador do tipo de arma de fogo.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','135','IND_MED','N','N','S','N','S','N','S','N','N','NUMBER','1','','IND_MED','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','136','LOTE_MED','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','LOTE_MED','N�O UTILIZAR.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','137','NUM_ARM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','NUM_ARM','Numeracao de serie de fabricacao da arma.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','196','COD_CCUS','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','COD_CCUS','Codigo do centro de custos','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','198','IND_NAT_FRT_PISCOFINS','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_NAT_FRT_PISCOFINS','Indicador da Natureza do Frete Contratado, referente a: 0 - Opera��es de vendas, com �nus suportado pelo estabelecimento vendedor; 1 - Opera��es de vendas, com �nus suportado pelo adquirente; 2 - Opera��es de compras (bens para revenda, mat�riasprima e outros produtos, geradores de cr�dito); 3 - Opera��es de compras (bens para revenda, mat�riasprima e outros produtos, n�o geradores de cr�dito); 4 - Transfer�ncia de produtos acabados entre estabelecimentos da pessoa jur�dica; 5 - Transfer�ncia de produtos e','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','199','NAT_REC_PISCOFINS','N','N','S','N','S','N','S','N','N','VARCHAR2','3','','NAT_REC_PISCOFINS','Codigo da Natureza da Receita correspondente a Situacao Tributaria do PIS e do COFINS Isentas, nao alcancadas pela incidencia da contribuicao, sujeitas a','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','200','NAT_REC_PISCOFINS_DESCR','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','NAT_REC_PISCOFINS_DESCR','Descricao Complementar do Codigo da Natureza da Receita do PIS e do COFINS','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','197','PFJ_CODIGO_FORNECEDOR','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','PFJ_CODIGO_FORNECEDOR','Codigo da PFJ fornecedor do servico de turismo, se DOF de saida, ou codigo da PFJ cliente do tipo orgao governamental, se DOF de entrada.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','189','VL_BASE_II','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_II','Valor da Base do II.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','195','VL_CRED_COFINS_REC_EXPO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_COFINS_REC_EXPO','Valor de Credito de COFINS vinculado a Receitas de Exportacao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','194','VL_CRED_COFINS_REC_NAO_TRIB','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_COFINS_REC_NAO_TRIB','Valor de Credito de COFINS vinculado a Receitas Nao Tributadas no Mercado Interno.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','193','VL_CRED_COFINS_REC_TRIB','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_COFINS_REC_TRIB','Valor de Credito de COFINS vinculado a Receitas Tributadas no Mercado Interno.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','192','VL_CRED_PIS_REC_EXPO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_PIS_REC_EXPO','Valor de Credito de PIS vinculado a Receitas de Exportacao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','191','VL_CRED_PIS_REC_NAO_TRIB','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_PIS_REC_NAO_TRIB','Valor de Credito de PIS vinculado a Receitas Nao Tributadas no Mercado Interno.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','190','VL_CRED_PIS_REC_TRIB','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CRED_PIS_REC_TRIB','Valor de Credito de PIS vinculado a Receitas Tributadas no Mercado Interno.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','144','ALIQ_PIS_ST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_PIS_ST','Aliquota para o calculo do PIS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','145','VL_BASE_PIS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_PIS_ST','Valor do PIS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','146','VL_IMPOSTO_PIS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IMPOSTO_PIS_ST','','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','147','ALIQ_COFINS_ST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_COFINS_ST','Aliquota para o calculo do COFINS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','148','VL_BASE_COFINS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_COFINS_ST','Valor de base de c�lculo para o COFINS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','149','VL_IMPOSTO_COFINS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IMPOSTO_COFINS_ST','','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','156','COD_AREA','N','N','S','N','S','N','S','N','N','VARCHAR2','10','','COD_AREA','C�digo de �rea do terminal faturado.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','159','DT_FIN_SERV','N','N','S','N','S','N','S','N','N','DATE','','','DT_FIN_SERV','Data em que se encerrou a presta��o do servi�o.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','157','DT_INI_SERV','N','N','S','N','S','N','S','N','N','DATE','','','DT_INI_SERV','Data em que se iniciou a presta��o do servi�o.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','158','IND_SERV','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_SERV','Indicador do tipo de servi�o prestado (0,1,2,3,4,9)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','155','PER_FISCAL','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','PER_FISCAL','Per�odo fiscal da presta��o do servi�o (MMAAAA)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','160','TERMINAL','N','N','S','N','S','N','S','N','N','VARCHAR2','50','','TERMINAL','Identifica��o do terminal faturado.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','161','UNI_FISCAL_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','UNI_FISCAL_CODIGO','Codigo da Unidade de Medida Fiscal do item.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','150','CCA_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CCA_CODIGO','Codigo da Classificacao do Item de Consumo','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','151','TIPO_RECEITA','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','TIPO_RECEITA','Codigo da Receita de Consumo','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','152','PFJ_CODIGO_TERCEIRO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','PFJ_CODIGO_TERCEIRO','Codigo da Pessoa Receptora da Receita de Consumo','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','153','PFJ_TERCEIRO_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','PFJ_TERCEIRO_ORIGEM','Nome do sistema ou tabela origem do estabelecimento de Terceiros do DOF.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','154','PFJ_TERCEIRO_CHAVE_ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','255','','PFJ_TERCEIRO_CHAVE_ORIGEM','Codigo do estabelecimento de Terceiros do DOF no sistema / tabela origem.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','177','ALIQ_COFINS_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_COFINS_RET','Aliquota COFINS retida.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','180','ALIQ_CSLL_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_CSLL_RET','Aliquota para calculo de retencao da CSLL (Contribuicao Social Sobre o Lucro Liquido).','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','183','ALIQ_PIS_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_PIS_RET','Aliquota PIS/PASEP retido.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','186','IND_ISS_RETIDO_FONTE','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_ISS_RETIDO_FONTE','Indicador de ISS retido na fonte. Pode ser deduzido a partir da Classe da pessoa destinataria.','','''N''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','174','IND_MOVIMENTA_ESTOQUE','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_MOVIMENTA_ESTOQUE','Indicador de Movimentacao de Estoque','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','187','IND_VL_TRIB_RET_NO_PRECO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_VL_TRIB_RET_NO_PRECO','Indicador de Tributos Retidos contidos no Preco.','','''N''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','188','PERC_TRIBUTAVEL_INSS_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_TRIBUTAVEL_INSS_RET','Percentual tributavel do INSS Retido.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','173','STM_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','STM_CODIGO','Situacao tributaria do ISSQN para a NFe versao 2.0. A codificacao para atender a Nota Tecnica 5 (que criou essa coluna) difere do codigo STI criado para o Ato Cotepe 70.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','178','VL_BASE_COFINS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_COFINS_RET','Valor Base para Calculo de retencao da COFINS (Contribuicao para Financiamento da Seguridade Social).','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','181','VL_BASE_CSLL_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_CSLL_RET','Valor Base para Calculo de retencao da CSLL (Contribuicao Social Sobre o Lucro Liquido).','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','184','VL_BASE_PIS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_PIS_RET','Valor Base para Calculo de retencao do PIS/PASEP.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','179','VL_COFINS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_COFINS_RET','Valor de retencao da COFINS (Contribuicao para Financiamento da Seguridade Social).','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','176','VL_COFINS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_COFINS_ST','Valor de base de c�lculo para o COFINS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','182','VL_CSLL_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_CSLL_RET','Valor de Reten��o da CSLL (Contribuicao Social Sobre o Lucro Liquido).','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','185','VL_PIS_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_PIS_RET','Valor de retencao do PIS/PASEP.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','175','VL_PIS_ST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_PIS_ST','Valor de base de c�lculo para o PIS-ST','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','168','ALIQ_ANTECIP_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','8','2','ALIQ_ANTECIP_ICMS','Aliquota de antecipacao de ICMS.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','164','ALIQ_ICMS_DESC_L','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS_DESC_L','Al�quota do desconto legal de ICMS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','170','CUSTO_ADICAO','N','N','S','N','S','N','S','N','N','NUMBER','12','2','CUSTO_ADICAO','Representa valor de adicao de custo.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','171','CUSTO_REDUCAO','N','N','S','N','S','N','S','N','N','NUMBER','12','2','CUSTO_REDUCAO','Representa valor de reducao de custo.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','167','IND_ANTECIP_ICMS','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_ANTECIP_ICMS','Indicador de antecipacao de ICMS.','','''N''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','172','IND_VL_PIS_COFINS_NO_PRECO','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_VL_PIS_COFINS_NO_PRECO','Indica se o o valor do PIS e COFINS estao embutidos no pre�o unitario informado.','','''S''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','166','IND_ZFM_ALC','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_ZFM_ALC','Indicador de calculo de Zona Franca de Manaus.','','''N''','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','169','VL_ANTECIP_ICMS','N','N','S','N','S','N','S','N','N','NUMBER','12','2','VL_ANTECIP_ICMS','Valor de antecipacao de ICMS.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','162','VL_BASE_ICMS_DESC_L','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS_DESC_L','Base do ICMS dado como desconto legal.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','165','VL_ICMS_DESC_L','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS_DESC_L','Valor do desconto legal de ICMS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','163','VL_TRIBUTAVEL_ICMS_DESC_L','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRIBUTAVEL_ICMS_DESC_L','Base tributavel do ICMS dado como desconto legal.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','208','COD_BC_CREDITO','N','N','S','N','S','N','S','N','N','VARCHAR2','2','','COD_BC_CREDITO','Codigo Base de Calculo de Credito','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','205','COD_FISC_SERV_MUN','N','N','S','N','S','N','S','N','N','VARCHAR2','15','','COD_FISC_SERV_MUN','Codigo fiscal de servico no municipio utilizado pelas obrigacoes municipais e Nfse','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','204','CSOSN_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','3','','CSOSN_CODIGO','O Cod. Sit. da Operacao no Simples Nacional sera usado na NFe exclusivamente quando o Cod. Regime Tributario for igual a "1", e substituira os codigos da Tabela B - Tributacao pelo ICMS do Anexo Cod. Situacao Tributaria do Convenio s/nr de 15/12/1970.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','202','FCI_NUMERO','N','N','S','N','S','N','S','N','N','VARCHAR2','36','','FCI_NUMERO','Numero da FCI.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','210','IND_CIAP','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_CIAP','Indica se o IDF e de Movimentacao de CIAP (S/N)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','209','IND_CPC','N','N','S','N','S','N','S','N','N','VARCHAR2','1','','IND_CPC','Indica se o IDF e de Movimentacao de Cr�dito de PIS/COFINS (S/N)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','207','NBS_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','20','','NBS_CODIGO','Codigo da Nomenclatura Brasileira de Servicos (para item do tipo servico ou prestacao)','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','203','PERC_PART_CI','N','N','S','N','S','N','S','N','N','NUMBER','8','5','PERC_PART_CI','Percentual do Conteudo de Importacao do bem ou mercadoria.','','0','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','201','VL_IMP_FCI','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IMP_FCI','Valor da parcela importada do exterior.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','206','DESCRICAO_COMPLEMENTAR_SERVICO','N','N','S','N','S','N','S','N','N','VARCHAR2','2000','','DESCRICAO_COMPLEMENTAR_SERVICO','Utilizado na emissao de NFe, onde a descricao do servico pode ter ate 1000 posicoes.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','241','VL_RATEIO_CT_STF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_CT_STF','Valor do ICMS Rateado do Conhecimento de Transporte para calculo de STF Fronteira','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','229','CEST_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','9','','CEST_CODIGO','Codigo Especificador da Substituicao Tributaria - CEST, que identifica a mercadoria sujeita aos regimes de substituicao tributaria e de antecipacao do recolhimento do imposto.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','235','VL_BASE_ICMS_PART_DEST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS_PART_DEST','Valor da base tributavel do ICMS partilhado na UF do Destinatario.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','236','ALIQ_ICMS_PART_DEST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS_PART_DEST','Aliquota do ICMS partilhado na UF do Destinatario.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','237','VL_ICMS_PART_DEST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','VL_ICMS_PART_DEST','Valor do ICMS partilhado na UF do Destinatario.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','238','ALIQ_ICMS_FCP','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS_FCP','Aliquota do Fundo de Combate a Pobreza (FCP).','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','240','VL_BASE_ICMS_FCP','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS_FCP','Valor da base tributavel do ICMS relativo ao Fundo de Combate a Pobreza (FCP) da UF de destino.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','242','VL_RATEIO_BASE_CT_STF','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RATEIO_BASE_CT_STF','Valor da Base do ICMS Rateada do Conhecimento de Transporte para calculo de STF Fronteira','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','231','VL_BASE_ICMS_PART_REM','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS_PART_REM','Valor da base tributavel do ICMS partilhado na UF do Remetente.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','243','ALIQ_DIFA_ICMS_PART','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_DIFA_ICMS_PART','Diferencial de Aliquota do ICMS partilhado. A difereca entre a aliquota destino e aliquota interestadual da operacao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','244','ENQ_IPI_CODIGO','N','N','S','N','S','N','S','N','N','VARCHAR2','5','','ENQ_IPI_CODIGO','Codigo do Enquadramento Legal do IPI','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','232','ALIQ_ICMS_PART_REM','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS_PART_REM','Aliquota do ICMS partilhado na UF do Remetente.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','233','VL_ICMS_PART_REM','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS_PART_REM','Valor do ICMS partilhado na UF do Remetente.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','234','PERC_ICMS_PART_DEST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_ICMS_PART_DEST','Percentual provisorio do ICMS partilhado na UF do Destinatario.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','239','VL_ICMS_FCP','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS_FCP','Valor do ICMS relativo ao Fundo de Combate a Pobreza (FCP) da UF de destino.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','230','PERC_ICMS_PART_REM','N','N','S','N','S','N','S','N','N','NUMBER','15','4','PERC_ICMS_PART_REM','Percentual provisorio do ICMS partilhado na UF do Remetente.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','255','ALIQ_INSS_AE15_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_INSS_AE15_RET','Aliquota de INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 15 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','258','ALIQ_INSS_AE20_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_INSS_AE20_RET','Aliquota de INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 20 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','261','ALIQ_INSS_AE25_RET','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_INSS_AE25_RET','Aliquota de INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 25 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','248','CODIGO_RETENCAO_COFINS','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO_COFINS','Codigo de Retencao COFINS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','249','CODIGO_RETENCAO_CSLL','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO_CSLL','Codigo de Retencao CSLL (Contribuicao Social sobre o Lucro Liquido).','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','250','CODIGO_RETENCAO_INSS','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO_INSS','Codigo de Retencao INSS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','251','CODIGO_RETENCAO_PCC','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO_PCC','Codigo de Retencao PCC.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','252','CODIGO_RETENCAO_PIS','N','N','S','N','S','N','S','N','N','VARCHAR2','6','','CODIGO_RETENCAO_PIS','Codigo de Retencao PIS.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','245','ID_PROCESSO','N','N','S','N','S','N','S','N','N','NUMBER','','','ID_PROCESSO','','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','247','VL_ALIMENTACAO','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ALIMENTACAO','Valor gasto com alimentacao. Devera ser deduzido do valor bruto para apuracao da base de calculo da contribuicao previdenciaria.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','257','VL_INSS_AE15_NAO_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE15_NAO_RET','Valor do INSS Nao Retido cuja atividade permita concessao de Aposentadoria especial apos 15 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','256','VL_INSS_AE15_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE15_RET','Valor do INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 15 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','260','VL_INSS_AE20_NAO_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE20_NAO_RET','Valor do INSS Nao Retido cuja atividade permita concessao de Aposentadoria especial apos 20 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','259','VL_INSS_AE20_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE20_RET','Valor do INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 20 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','263','VL_INSS_AE25_NAO_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE25_NAO_RET','Valor do INSS Nao Retido cuja atividade permita concessao de Aposentadoria especial apos 25 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','262','VL_INSS_AE25_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_AE25_RET','Valor do INSS Retido cuja atividade permita concessao de Aposentadoria especial apos 25 anos de contribuicao.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','254','VL_INSS_NAO_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_INSS_NAO_RET','Valor de INSS Nao Retido.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','253','VL_IRRF_NAO_RET','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_IRRF_NAO_RET','Valor do IRRF Nao Retido.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','247','VL_NAO_RETIDO_CP_AE','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_NAO_RETIDO_CP_AE','Valor total do Adicional de AE nao retido.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','246','VL_RETIDO_SUBEMPREITADA','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_RETIDO_SUBEMPREITADA','Valor da retencao relativo aos servicos subempreitados.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','245','HASH','N','N','S','N','S','N','S','N','N','VARCHAR2','100','','HASH','Campo usado pela integracao Eagle-DFE','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','248','VL_TRANSPORTE','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_TRANSPORTE','Valor gasto com transporte. Devera ser deduzido do valor bruto para apuracao da base de calculo da contribuicao previdenciaria.','','NULL','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','267','ALIQ_ICMS_FCPST','N','N','S','N','S','N','S','N','N','NUMBER','15','4','ALIQ_ICMS_FCPST','Aliquota do Fundo de Combate a Pobreza (FCP) da UF de origem retido por Substituicao Tributaria.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','266','ORIGEM','N','N','S','N','S','N','S','N','N','VARCHAR2','30','','ORIGEM','Nome do Sistema de Origem do registro','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','269','VL_BASE_ICMS_FCPST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_BASE_ICMS_FCPST','Valor da base tributavel do ICMS relativo ao Fundo de Combate a Pobreza (FCP) da UF de origem retido por Substituicao Tributaria.','','','','','','N','N', mREP_ID);
        Syn_Gera_Script_Pkg.Insert_Dic_Dado ('C','268','VL_ICMS_FCPST','N','N','S','N','S','N','S','N','N','NUMBER','19','2','VL_ICMS_FCPST','Valor do ICMS relativo ao Fundo de Combate a Pobreza (FCP) da UF de origem retido por Substituicao Tributaria.','','','','','','N','N', mREP_ID);
 
EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
------------------
-- FIM DO BLOCO 2 
------------------
END;
/
        
--------------------
-- INICIO DECLARE 3
--------------------
DECLARE
    mREP_ID                  dic_repositorio.id%TYPE;
    mVIS_ID                  dic_visao.id%TYPE;
    mREP_ID_CURSOR           dic_repositorio.id%TYPE;
------------------
-- INICIO BLOCO 3
------------------
BEGIN
 
        mREP_ID  := Syn_Gera_Script_pkg.Pego_ID_Repositorio ( 'SYNITF_IDF' );
        mVIS_ID := Syn_Gera_Script_pkg.Pego_Vis_Id ( mREP_ID, 'VIS_HON_SYNITF_IDF' );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('1','NBM_CODIGO','N','NBM_CODIGO','SYNITF_IDF.NBM_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('2','DOF_IMPORT_NUMERO','N','DOF_IMPORT_NUMERO','SYNITF_IDF.DOF_IMPORT_NUMERO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('3','IDF_NUM','N','IDF_NUM','SYNITF_IDF.IDF_NUM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('5','SUBCLASSE_IDF','N','SUBCLASSE_IDF','SYNITF_IDF.SUBCLASSE_IDF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('6','ALIQ_STF','N','ALIQ_STF','SYNITF_IDF.ALIQ_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('7','ALIQ_STT','N','ALIQ_STT','SYNITF_IDF.ALIQ_STT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('7','VL_BASE_IRRF','N','VL_BASE_IRRF','SYNITF_IDF.VL_BASE_IRRF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('8','AM_CODIGO','N','AM_CODIGO','SYNITF_IDF.AM_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('8','VL_IRRF','N','VL_IRRF','SYNITF_IDF.VL_IRRF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('9','CFOP_CODIGO','N','CFOP_CODIGO','SYNITF_IDF.CFOP_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('13','CTRL_INSTRUCAO','N','CTRL_INSTRUCAO','SYNITF_IDF.CTRL_INSTRUCAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('16','DT_DI','N','DT_DI','SYNITF_IDF.DT_DI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('17','ENTSAI_UNI_CODIGO','N','ENTSAI_UNI_CODIGO','SYNITF_IDF.ENTSAI_UNI_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('23','FIN_CODIGO','N','FIN_CODIGO','SYNITF_IDF.FIN_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('25','IDF_TEXTO_COMPLEMENTAR','N','IDF_TEXTO_COMPLEMENTAR','SYNITF_IDF.IDF_TEXTO_COMPLEMENTAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('46','MERC_CHAVE_ORIGEM','N','MERC_CHAVE_ORIGEM','SYNITF_IDF.MERC_CHAVE_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('48','MERC_ORIGEM','N','MERC_ORIGEM','SYNITF_IDF.MERC_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('50','MUN_CODIGO','N','MUN_CODIGO','SYNITF_IDF.MUN_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('52','NOP_CODIGO','N','NOP_CODIGO','SYNITF_IDF.NOP_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('53','NUM_DI','N','NUM_DI','SYNITF_IDF.NUM_DI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('60','OM_CODIGO','N','OM_CODIGO','SYNITF_IDF.OM_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('61','ORD_IMPRESSAO','N','ORD_IMPRESSAO','SYNITF_IDF.ORD_IMPRESSAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('72','PESO_BRUTO_KG','N','PESO_BRUTO_KG','SYNITF_IDF.PESO_BRUTO_KG','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('73','PESO_LIQUIDO_KG','N','PESO_LIQUIDO_KG','SYNITF_IDF.PESO_LIQUIDO_KG','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('74','PRECO_TOTAL','N','PRECO_TOTAL','SYNITF_IDF.PRECO_TOTAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('75','PRECO_UNITARIO','N','PRECO_UNITARIO','SYNITF_IDF.PRECO_UNITARIO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('76','PRES_CODIGO','N','PRES_CODIGO','SYNITF_IDF.PRES_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('77','QTD','N','QTD','SYNITF_IDF.QTD','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('78','SISS_CODIGO','N','SISS_CODIGO','SYNITF_IDF.SISS_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('79','STC_CODIGO','N','STC_CODIGO','SYNITF_IDF.STC_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('80','STP_CODIGO','N','STP_CODIGO','SYNITF_IDF.STP_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('82','TIPO_COMPLEMENTO','N','TIPO_COMPLEMENTO','SYNITF_IDF.TIPO_COMPLEMENTO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('86','VL_AJUSTE_PRECO_TOTAL','N','VL_AJUSTE_PRECO_TOTAL','SYNITF_IDF.VL_AJUSTE_PRECO_TOTAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('87','VL_BASE_ICMS','N','VL_BASE_ICMS','SYNITF_IDF.VL_BASE_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('88','VL_BASE_INSS','N','VL_BASE_INSS','SYNITF_IDF.VL_BASE_INSS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('89','VL_BASE_IPI','N','VL_BASE_IPI','SYNITF_IDF.VL_BASE_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('91','VL_BASE_ISS','N','VL_BASE_ISS','SYNITF_IDF.VL_BASE_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('92','VL_BASE_STF','N','VL_BASE_STF','SYNITF_IDF.VL_BASE_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('93','VL_BASE_STT','N','VL_BASE_STT','SYNITF_IDF.VL_BASE_STT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('94','VL_CONTABIL','N','VL_CONTABIL','SYNITF_IDF.VL_CONTABIL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('95','VL_DIFA','N','VL_DIFA','SYNITF_IDF.VL_DIFA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('96','VL_FATURADO','N','VL_FATURADO','SYNITF_IDF.VL_FATURADO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('97','VL_FISCAL','N','VL_FISCAL','SYNITF_IDF.VL_FISCAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('98','VL_ICMS','N','VL_ICMS','SYNITF_IDF.VL_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('99','VL_II','N','VL_II','SYNITF_IDF.VL_II','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('100','VL_INSS','N','VL_INSS','SYNITF_IDF.VL_INSS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('101','VL_IPI','N','VL_IPI','SYNITF_IDF.VL_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('103','VL_ISENTO_ICMS','N','VL_ISENTO_ICMS','SYNITF_IDF.VL_ISENTO_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('104','VL_ISENTO_IPI','N','VL_ISENTO_IPI','SYNITF_IDF.VL_ISENTO_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('105','VL_ISS','N','VL_ISS','SYNITF_IDF.VL_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('106','VL_OUTROS_ICMS','N','VL_OUTROS_ICMS','SYNITF_IDF.VL_OUTROS_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('107','VL_OUTROS_IPI','N','VL_OUTROS_IPI','SYNITF_IDF.VL_OUTROS_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('108','VL_RATEIO_AJUSTE_PRECO','N','VL_RATEIO_AJUSTE_PRECO','SYNITF_IDF.VL_RATEIO_AJUSTE_PRECO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('109','VL_RATEIO_FRETE','N','VL_RATEIO_FRETE','SYNITF_IDF.VL_RATEIO_FRETE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('110','VL_RATEIO_ODA','N','VL_RATEIO_ODA','SYNITF_IDF.VL_RATEIO_ODA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('111','VL_RATEIO_SEGURO','N','VL_RATEIO_SEGURO','SYNITF_IDF.VL_RATEIO_SEGURO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('112','VL_STF','N','VL_STF','SYNITF_IDF.VL_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('113','VL_STT','N','VL_STT','SYNITF_IDF.VL_STT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('114','VL_TRIBUTAVEL_DIFA','N','VL_TRIBUTAVEL_DIFA','SYNITF_IDF.VL_TRIBUTAVEL_DIFA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('115','VL_TRIBUTAVEL_ICMS','N','VL_TRIBUTAVEL_ICMS','SYNITF_IDF.VL_TRIBUTAVEL_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('116','VL_TRIBUTAVEL_IPI','N','VL_TRIBUTAVEL_IPI','SYNITF_IDF.VL_TRIBUTAVEL_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('117','VL_TRIBUTAVEL_STF','N','VL_TRIBUTAVEL_STF','SYNITF_IDF.VL_TRIBUTAVEL_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('118','VL_TRIBUTAVEL_STT','N','VL_TRIBUTAVEL_STT','SYNITF_IDF.VL_TRIBUTAVEL_STT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('119','CODIGO_DO_SITE','N','','1','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('122','WROWID','N','','SYNITF_IDF.ROWID','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('124','INFORMANTE_EST_CODIGO','N','INFORMANTE_EST_CODIGO','SYNITF_IDF.INFORMANTE_EST_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('130','ALIQ_DIFA','N','ALIQ_DIFA','SYNITF_IDF.ALIQ_DIFA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('131','ALIQ_ICMS','N','ALIQ_ICMS','SYNITF_IDF.ALIQ_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('132','ALIQ_INSS','N','ALIQ_INSS','SYNITF_IDF.ALIQ_INSS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('135','ALIQ_IPI','N','ALIQ_IPI','SYNITF_IDF.ALIQ_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('135','ALIQ_II','N','ALIQ_II','SYNITF_IDF.ALIQ_II','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('140','ALIQ_ISS','N','ALIQ_ISS','SYNITF_IDF.ALIQ_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('141','DOF_ID','N','','1111111111111111111','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('142','DOF_SEQUENCE','N','','111111111111111','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('147','VL_ALIQ_COFINS','N','VL_ALIQ_COFINS','SYNITF_IDF.VL_ALIQ_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('148','VL_ALIQ_PIS','N','VL_ALIQ_PIS','SYNITF_IDF.VL_ALIQ_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('149','VL_BASE_COFINS','N','VL_BASE_COFINS','SYNITF_IDF.VL_BASE_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('150','VL_BASE_PIS','N','VL_BASE_PIS','SYNITF_IDF.VL_BASE_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('153','VL_IMPOSTO_COFINS','N','VL_IMPOSTO_COFINS','SYNITF_IDF.VL_IMPOSTO_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('154','VL_IMPOSTO_PIS','N','VL_IMPOSTO_PIS','SYNITF_IDF.VL_IMPOSTO_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('169','MSG_CRITICA','N','MSG_CRITICA','SYNITF_IDF.MSG_CRITICA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('170','CTRL_CRITICA','N','CTRL_CRITICA','SYNITF_IDF.CTRL_CRITICA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('171','DH_CRITICA','N','DH_CRITICA','SYNITF_IDF.DH_CRITICA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('172','OBS_FISCAL_ICMS','N','OBS_FISCAL_ICMS','SYNITF_IDF.OBS_FISCAL_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('173','OBS_FISCAL_IPI','N','OBS_FISCAL_IPI','SYNITF_IDF.OBS_FISCAL_IPI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('174','OBS_FISCAL_IRRF','N','OBS_FISCAL_IRRF','SYNITF_IDF.OBS_FISCAL_IRRF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('175','OBS_FISCAL_ISS','N','OBS_FISCAL_ISS','SYNITF_IDF.OBS_FISCAL_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('176','OBS_FISCAL_STF','N','OBS_FISCAL_STF','SYNITF_IDF.OBS_FISCAL_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('177','OBS_FISCAL_STT','N','OBS_FISCAL_STT','SYNITF_IDF.OBS_FISCAL_STT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('178','OBS_FISCAL_OPERACIONAL','N','OBS_FISCAL_OPERACIONAL','SYNITF_IDF.OBS_FISCAL_OPERACIONAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('179','INFORMANTE_PFJ_CHAVE_ORIGEM','N','INFORMANTE_PFJ_CHAVE_ORIGEM','SYNITF_IDF.INFORMANTE_PFJ_CHAVE_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('180','INFORMANTE_PFJ_ORIGEM','N','INFORMANTE_PFJ_ORIGEM','SYNITF_IDF.INFORMANTE_PFJ_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('181','MERC_CODIGO','N','MERC_CODIGO','SYNITF_IDF.MERC_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('182','EMERC_CODIGO','N','EMERC_CODIGO','SYNITF_IDF.EMERC_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('183','ID','N','','123456789012345678901','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('194','CODIGO_ISS_MUNICIPIO','N','CODIGO_ISS_MUNICIPIO','SYNITF_IDF.CODIGO_ISS_MUNICIPIO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('195','ALIQ_INSS_RET','N','ALIQ_INSS_RET','SYNITF_IDF.ALIQ_INSS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('201','PERC_OUTROS_ABAT','N','PERC_OUTROS_ABAT','SYNITF_IDF.PERC_OUTROS_ABAT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('202','VL_BASE_INSS_RET','N','VL_BASE_INSS_RET','SYNITF_IDF.VL_BASE_INSS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('203','VL_INSS_RET','N','VL_INSS_RET','SYNITF_IDF.VL_INSS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('204','VL_OUTROS_ABAT','N','VL_OUTROS_ABAT','SYNITF_IDF.VL_OUTROS_ABAT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('205','CODIGO_RETENCAO','N','CODIGO_RETENCAO','SYNITF_IDF.CODIGO_RETENCAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('206','IND_VL_ICMS_NO_PRECO','N','','NVL ( SYNITF_IDF.IND_VL_ICMS_NO_PRECO, ''S'') ','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('207','CHASSI_VEIC','N','CHASSI_VEIC','SYNITF_IDF.CHASSI_VEIC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('208','TIPO_OPER_VEIC','N','TIPO_OPER_VEIC','SYNITF_IDF.TIPO_OPER_VEIC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('209','VL_IOF','N','VL_IOF','SYNITF_IDF.VL_IOF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('210','CONTA_CONTABIL','N','CONTA_CONTABIL','SYNITF_IDF.CONTA_CONTABIL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('211','DH_INCLUSAO','N','DH_INCLUSAO','SYNITF_IDF.DH_INCLUSAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('213','VL_BASE_STF_FRONTEIRA','N','VL_BASE_STF_FRONTEIRA','SYNITF_IDF.VL_BASE_STF_FRONTEIRA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('214','VL_BASE_STF_IDO','N','VL_BASE_STF_IDO','SYNITF_IDF.VL_BASE_STF_IDO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('215','VL_STF_FRONTEIRA','N','VL_STF_FRONTEIRA','SYNITF_IDF.VL_STF_FRONTEIRA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('216','VL_STF_IDO','N','VL_STF_IDO','SYNITF_IDF.VL_STF_IDO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('217','STA_CODIGO','N','STA_CODIGO','SYNITF_IDF.STA_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('218','ALIQ_PIS','N','ALIQ_PIS','SYNITF_IDF.ALIQ_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('219','QTD_BASE_PIS','N','QTD_BASE_PIS','SYNITF_IDF.QTD_BASE_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('220','VL_PIS','N','VL_PIS','SYNITF_IDF.VL_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('221','ALIQ_COFINS','N','ALIQ_COFINS','SYNITF_IDF.ALIQ_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('222','QTD_BASE_COFINS','N','QTD_BASE_COFINS','SYNITF_IDF.QTD_BASE_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('223','VL_COFINS','N','VL_COFINS','SYNITF_IDF.VL_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('224','NUM_TANQUE','N','NUM_TANQUE','SYNITF_IDF.NUM_TANQUE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('225','DT_FAB_MED','N','DT_FAB_MED','SYNITF_IDF.DT_FAB_MED','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('226','TIPO_PROD_MED','N','TIPO_PROD_MED','SYNITF_IDF.TIPO_PROD_MED','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('227','PARTICIPANTE_PFJ_CODIGO','N','PARTICIPANTE_PFJ_CODIGO','SYNITF_IDF.PARTICIPANTE_PFJ_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('228','CLI_CODIGO','N','CLI_CODIGO','SYNITF_IDF.CLI_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('229','STN_CODIGO','N','STN_CODIGO','SYNITF_IDF.STN_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('234','TIPO_STF','N','','SYNITF_IDF.TIPO_STF','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('235','SISS_CHAVE_ORIGEM','N','','DECODE(SYNITF_IDF.SISS_CHAVE_ORIGEM, NULL, NULL, RTRIM ( LTRIM ( SYNITF_IDF.SISS_CHAVE_ORIGEM)))','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('236','SISS_ORIGEM','N','','DECODE (SYNITF_IDF.SISS_ORIGEM, NULL, NULL, RTRIM ( LTRIM ( SYNITF_IDF.SISS_ORIGEM)))','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('237','PRES_CHAVE_ORIGEM','N','','DECODE( SYNITF_IDF.PRES_CHAVE_ORIGEM, NULL, NULL, RTRIM ( LTRIM ( SYNITF_IDF.PRES_CHAVE_ORIGEM)))','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('238','PRES_ORIGEM','N','','DECODE( SYNITF_IDF.PRES_ORIGEM, NULL, NULL, RTRIM ( LTRIM ( SYNITF_IDF.PRES_ORIGEM)))','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('239','STI_CODIGO','N','STI_CODIGO','SYNITF_IDF.STI_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('240','CNPJ_PAR','N','CNPJ_PAR','SYNITF_IDF.CNPJ_PAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('241','COD_IATA_FIM','N','COD_IATA_FIM','SYNITF_IDF.COD_IATA_FIM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('242','COD_IATA_INI','N','COD_IATA_INI','SYNITF_IDF.COD_IATA_INI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('243','CPF_PAR','N','CPF_PAR','SYNITF_IDF.CPF_PAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('244','DT_VAL','N','DT_VAL','SYNITF_IDF.DT_VAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('245','IE_PAR','N','IE_PAR','SYNITF_IDF.IE_PAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('246','IM_SUBCONTRATACAO','N','IM_SUBCONTRATACAO','SYNITF_IDF.IM_SUBCONTRATACAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('247','IND_ARM','N','IND_ARM','SYNITF_IDF.IND_ARM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('248','IND_MED','N','IND_MED','SYNITF_IDF.IND_MED','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('249','LOTE_MED','N','LOTE_MED','SYNITF_IDF.LOTE_MED','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('250','NUM_ARM','N','NUM_ARM','SYNITF_IDF.NUM_ARM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('251','NUM_CANO','N','NUM_CANO','SYNITF_IDF.NUM_CANO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('252','UF_PAR','N','UF_PAR','SYNITF_IDF.UF_PAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('253','VL_TAB_MAX','N','VL_TAB_MAX','SYNITF_IDF.VL_TAB_MAX','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('254','VL_DEDUCAO_DEPENDENTE_PRG','N','VL_DEDUCAO_DEPENDENTE_PRG','SYNITF_IDF.VL_DEDUCAO_DEPENDENTE_PRG','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('255','VL_DEDUCAO_PENSAO_PRG','N','VL_DEDUCAO_PENSAO_PRG','SYNITF_IDF.VL_DEDUCAO_PENSAO_PRG','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('256','PERC_IRRF','N','PERC_IRRF','SYNITF_IDF.PERC_IRRF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('257','CCA_CODIGO','N','CCA_CODIGO','SYNITF_IDF.CCA_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('258','TIPO_RECEITA','N','TIPO_RECEITA','SYNITF_IDF.TIPO_RECEITA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('259','PFJ_CODIGO_TERCEIRO','N','PFJ_CODIGO_TERCEIRO','SYNITF_IDF.PFJ_CODIGO_TERCEIRO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('260','PFJ_TERCEIRO_CHAVE_ORIGEM','N','PFJ_TERCEIRO_CHAVE_ORIGEM','SYNITF_IDF.PFJ_TERCEIRO_CHAVE_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('261','PFJ_TERCEIRO_ORIGEM','N','PFJ_TERCEIRO_ORIGEM','SYNITF_IDF.PFJ_TERCEIRO_ORIGEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('262','UNI_FISCAL_CODIGO','N','UNI_FISCAL_CODIGO','SYNITF_IDF.UNI_FISCAL_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('263','IND_SERV','N','IND_SERV','SYNITF_IDF.IND_SERV','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('264','DT_INI_SERV','N','DT_INI_SERV','SYNITF_IDF.DT_INI_SERV','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('265','DT_FIN_SERV','N','DT_FIN_SERV','SYNITF_IDF.DT_FIN_SERV','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('266','PER_FISCAL','N','PER_FISCAL','SYNITF_IDF.PER_FISCAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('267','COD_AREA','N','COD_AREA','SYNITF_IDF.COD_AREA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('268','TERMINAL','N','TERMINAL','SYNITF_IDF.TERMINAL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('269','ALIQ_ANTECIP_ICMS','N','ALIQ_ANTECIP_ICMS','SYNITF_IDF.ALIQ_ANTECIP_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('270','ALIQ_ICMS_DESC_L','N','ALIQ_ICMS_DESC_L','SYNITF_IDF.ALIQ_ICMS_DESC_L','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('271','CUSTO_ADICAO','N','CUSTO_ADICAO','SYNITF_IDF.CUSTO_ADICAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('272','CUSTO_REDUCAO','N','CUSTO_REDUCAO','SYNITF_IDF.CUSTO_REDUCAO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('273','IND_ANTECIP_ICMS','N','IND_ANTECIP_ICMS','SYNITF_IDF.IND_ANTECIP_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('274','IND_VL_PIS_COFINS_NO_PRECO','N','IND_VL_PIS_COFINS_NO_PRECO','SYNITF_IDF.IND_VL_PIS_COFINS_NO_PRECO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('275','IND_ZFM_ALC','N','IND_ZFM_ALC','SYNITF_IDF.IND_ZFM_ALC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('276','VL_ANTECIP_ICMS','N','VL_ANTECIP_ICMS','SYNITF_IDF.VL_ANTECIP_ICMS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('277','VL_BASE_ICMS_DESC_L','N','VL_BASE_ICMS_DESC_L','SYNITF_IDF.VL_BASE_ICMS_DESC_L','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('278','VL_ICMS_DESC_L','N','VL_ICMS_DESC_L','SYNITF_IDF.VL_ICMS_DESC_L','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('279','VL_TRIBUTAVEL_ICMS_DESC_L','N','VL_TRIBUTAVEL_ICMS_DESC_L','SYNITF_IDF.VL_TRIBUTAVEL_ICMS_DESC_L','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('280','STM_CODIGO','N','STM_CODIGO','SYNITF_IDF.STM_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('281','ALIQ_COFINS_ST','N','ALIQ_COFINS_ST','SYNITF_IDF.ALIQ_COFINS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('282','ALIQ_PIS_ST','N','ALIQ_PIS_ST','SYNITF_IDF.ALIQ_PIS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('283','IND_MOVIMENTA_ESTOQUE','N','IND_MOVIMENTA_ESTOQUE','SYNITF_IDF.IND_MOVIMENTA_ESTOQUE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('284','VL_BASE_COFINS_ST','N','VL_BASE_COFINS_ST','SYNITF_IDF.VL_BASE_COFINS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('285','VL_BASE_PIS_ST','N','VL_BASE_PIS_ST','SYNITF_IDF.VL_BASE_PIS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('286','VL_COFINS_ST','N','VL_COFINS_ST','SYNITF_IDF.VL_COFINS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('287','VL_PIS_ST','N','VL_PIS_ST','SYNITF_IDF.VL_PIS_ST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('288','ALIQ_COFINS_RET','N','ALIQ_COFINS_RET','SYNITF_IDF.ALIQ_COFINS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('289','ALIQ_CSLL_RET','N','ALIQ_CSLL_RET','SYNITF_IDF.ALIQ_CSLL_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('290','ALIQ_PIS_RET','N','ALIQ_PIS_RET','SYNITF_IDF.ALIQ_PIS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('295','IND_ISS_RETIDO_FONTE','N','IND_ISS_RETIDO_FONTE','SYNITF_IDF.IND_ISS_RETIDO_FONTE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('296','IND_VL_TRIB_RET_NO_PRECO','N','IND_VL_TRIB_RET_NO_PRECO','SYNITF_IDF.IND_VL_TRIB_RET_NO_PRECO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('297','PERC_TRIBUTAVEL_INSS_RET','N','PERC_TRIBUTAVEL_INSS_RET','SYNITF_IDF.PERC_TRIBUTAVEL_INSS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('298','VL_BASE_COFINS_RET','N','VL_BASE_COFINS_RET','SYNITF_IDF.VL_BASE_COFINS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('299','VL_BASE_CSLL_RET','N','VL_BASE_CSLL_RET','SYNITF_IDF.VL_BASE_CSLL_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('300','VL_BASE_PIS_RET','N','VL_BASE_PIS_RET','SYNITF_IDF.VL_BASE_PIS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('301','VL_COFINS_RET','N','VL_COFINS_RET','SYNITF_IDF.VL_COFINS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('302','VL_CSLL_RET','N','VL_CSLL_RET','SYNITF_IDF.VL_CSLL_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('303','VL_PIS_RET','N','VL_PIS_RET','SYNITF_IDF.VL_PIS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('304','VL_BASE_II','N','VL_BASE_II','SYNITF_IDF.VL_BASE_II','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('305','VL_CRED_COFINS_REC_EXPO','N','VL_CRED_COFINS_REC_EXPO','SYNITF_IDF.VL_CRED_COFINS_REC_EXPO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('306','VL_CRED_COFINS_REC_NAO_TRIB','N','VL_CRED_COFINS_REC_NAO_TRIB','SYNITF_IDF.VL_CRED_COFINS_REC_NAO_TRIB','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('307','VL_CRED_COFINS_REC_TRIB','N','VL_CRED_COFINS_REC_TRIB','SYNITF_IDF.VL_CRED_COFINS_REC_TRIB','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('308','VL_CRED_PIS_REC_EXPO','N','VL_CRED_PIS_REC_EXPO','SYNITF_IDF.VL_CRED_PIS_REC_EXPO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('309','VL_CRED_PIS_REC_NAO_TRIB','N','VL_CRED_PIS_REC_NAO_TRIB','SYNITF_IDF.VL_CRED_PIS_REC_NAO_TRIB','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('310','VL_CRED_PIS_REC_TRIB','N','VL_CRED_PIS_REC_TRIB','SYNITF_IDF.VL_CRED_PIS_REC_TRIB','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('311','COD_CCUS','N','COD_CCUS','SYNITF_IDF.COD_CCUS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('312','PFJ_CODIGO_FORNECEDOR','N','PFJ_CODIGO_FORNECEDOR','SYNITF_IDF.PFJ_CODIGO_FORNECEDOR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('313','IND_NAT_FRT_PISCOFINS','N','IND_NAT_FRT_PISCOFINS','SYNITF_IDF.IND_NAT_FRT_PISCOFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('314','NAT_REC_PISCOFINS','N','NAT_REC_PISCOFINS','SYNITF_IDF.NAT_REC_PISCOFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('315','NAT_REC_PISCOFINS_DESCR','N','NAT_REC_PISCOFINS_DESCR','SYNITF_IDF.NAT_REC_PISCOFINS_DESCR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('316','IND_CIAP','N','IND_CIAP','SYNITF_IDF.IND_CIAP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('317','IND_CPC','N','IND_CPC','SYNITF_IDF.IND_CPC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('318','COD_FISC_SERV_MUN','N','COD_FISC_SERV_MUN','SYNITF_IDF.COD_FISC_SERV_MUN','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('319','COD_BC_CREDITO','N','COD_BC_CREDITO','SYNITF_IDF.COD_BC_CREDITO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('320','CSOSN_CODIGO','N','CSOSN_CODIGO','SYNITF_IDF.CSOSN_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('321','NBS_CODIGO','N','NBS_CODIGO','SYNITF_IDF.NBS_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('322','PERC_PART_CI','N','PERC_PART_CI','SYNITF_IDF.PERC_PART_CI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('323','VL_IMP_FCI','N','VL_IMP_FCI','SYNITF_IDF.VL_IMP_FCI','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('324','FCI_NUMERO','N','FCI_NUMERO','SYNITF_IDF.FCI_NUMERO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('325','DESCRICAO_COMPLEMENTAR_SERVICO','N','DESCRICAO_COMPLEMENTAR_SERVICO','SYNITF_IDF.DESCRICAO_COMPLEMENTAR_SERVICO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('326','VL_ABAT_LEGAL_INSS_RET','N','VL_ABAT_LEGAL_INSS_RET','SYNITF_IDF.VL_ABAT_LEGAL_INSS_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('327','VL_ABAT_LEGAL_IRRF','N','VL_ABAT_LEGAL_IRRF','SYNITF_IDF.VL_ABAT_LEGAL_IRRF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('328','VL_ABAT_LEGAL_ISS','N','VL_ABAT_LEGAL_ISS','SYNITF_IDF.VL_ABAT_LEGAL_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('329','DT_REF_CALC_IMP_IDF','N','DT_REF_CALC_IMP_IDF','SYNITF_IDF.DT_REF_CALC_IMP_IDF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('330','DESONERACAO_CODIGO','N','DESONERACAO_CODIGO','SYNITF_IDF.DESONERACAO_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('331','IND_EXIGIBILIDADE_ISS','N','IND_EXIGIBILIDADE_ISS','SYNITF_IDF.IND_EXIGIBILIDADE_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('332','IND_INCENTIVO_FISCAL_ISS','N','IND_INCENTIVO_FISCAL_ISS','SYNITF_IDF.IND_INCENTIVO_FISCAL_ISS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('333','CNPJ_SUBEMPREITEIRO','N','CNPJ_SUBEMPREITEIRO','SYNITF_IDF.CNPJ_SUBEMPREITEIRO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('334','VL_ADICIONAL_RET_AE','N','VL_ADICIONAL_RET_AE','SYNITF_IDF.VL_ADICIONAL_RET_AE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('335','VL_GILRAT','N','VL_GILRAT','SYNITF_IDF.VL_GILRAT','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('336','VL_MATERIAS_EQUIP','N','VL_MATERIAS_EQUIP','SYNITF_IDF.VL_MATERIAS_EQUIP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('337','VL_NAO_RETIDO_CP','N','VL_NAO_RETIDO_CP','SYNITF_IDF.VL_NAO_RETIDO_CP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('338','VL_SENAR','N','VL_SENAR','SYNITF_IDF.VL_SENAR','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('339','VL_SERVICO_AE15','N','VL_SERVICO_AE15','SYNITF_IDF.VL_SERVICO_AE15','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('340','VL_SERVICO_AE20','N','VL_SERVICO_AE20','SYNITF_IDF.VL_SERVICO_AE20','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('341','VL_SERVICO_AE25','N','VL_SERVICO_AE25','SYNITF_IDF.VL_SERVICO_AE25','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('342','IND_MP_DO_BEM','N','IND_MP_DO_BEM','SYNITF_IDF.IND_MP_DO_BEM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('343','VL_ICMS_SIMPLES_NAC','N','VL_ICMS_SIMPLES_NAC','SYNITF_IDF.VL_ICMS_SIMPLES_NAC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('344','PERC_TRIBUTAVEL_STF','N','PERC_TRIBUTAVEL_STF','SYNITF_IDF.PERC_TRIBUTAVEL_STF','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('347','CEST_CODIGO','N','CEST_CODIGO','SYNITF_IDF.CEST_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('348','PERC_ICMS_PART_REM','N','PERC_ICMS_PART_REM','SYNITF_IDF.PERC_ICMS_PART_REM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('349','VL_BASE_ICMS_PART_REM','N','VL_BASE_ICMS_PART_REM','SYNITF_IDF.VL_BASE_ICMS_PART_REM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('350','ALIQ_ICMS_PART_REM','N','ALIQ_ICMS_PART_REM','SYNITF_IDF.ALIQ_ICMS_PART_REM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('351','VL_ICMS_PART_REM','N','VL_ICMS_PART_REM','SYNITF_IDF.VL_ICMS_PART_REM','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('352','PERC_ICMS_PART_DEST','N','PERC_ICMS_PART_DEST','SYNITF_IDF.PERC_ICMS_PART_DEST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('353','VL_BASE_ICMS_PART_DEST','N','VL_BASE_ICMS_PART_DEST','SYNITF_IDF.VL_BASE_ICMS_PART_DEST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('354','ALIQ_ICMS_PART_DEST','N','ALIQ_ICMS_PART_DEST','SYNITF_IDF.ALIQ_ICMS_PART_DEST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('355','VL_ICMS_PART_DEST','N','VL_ICMS_PART_DEST','SYNITF_IDF.VL_ICMS_PART_DEST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('356','ALIQ_ICMS_FCP','N','ALIQ_ICMS_FCP','SYNITF_IDF.ALIQ_ICMS_FCP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('357','VL_BASE_ICMS_FCP','N','VL_BASE_ICMS_FCP','SYNITF_IDF.VL_BASE_ICMS_FCP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('358','VL_ICMS_FCP','N','VL_ICMS_FCP','SYNITF_IDF.VL_ICMS_FCP','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('359','ALIQ_DIFA_ICMS_PART','N','ALIQ_DIFA_ICMS_PART','SYNITF_IDF.ALIQ_DIFA_ICMS_PART','N','N','','COR_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('360','ENQ_IPI_CODIGO','N','ENQ_IPI_CODIGO','SYNITF_IDF.ENQ_IPI_CODIGO','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('361','HASH','N','HASH','SYNITF_IDF.HASH','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('362','VL_RATEIO_BASE_CT_STF','N','','NVL(SYNITF_IDF.VL_RATEIO_BASE_CT_STF, 0)','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('363','VL_RATEIO_CT_STF','N','','NVL(SYNITF_IDF.VL_RATEIO_CT_STF, 0)','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('364','CTRL_SITUACAO_DOF','N','','NULL','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('365','IND_CONTABILIZACAO','N','','''S''','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('368','VL_RETIDO_SUBEMPREITADA','N','VL_RETIDO_SUBEMPREITADA','SYNITF_IDF.VL_RETIDO_SUBEMPREITADA','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('369','VL_NAO_RETIDO_CP_AE','N','VL_NAO_RETIDO_CP_AE','SYNITF_IDF.VL_NAO_RETIDO_CP_AE','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('370','CODIGO_RETENCAO_COFINS','N','CODIGO_RETENCAO_COFINS','SYNITF_IDF.CODIGO_RETENCAO_COFINS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('371','CODIGO_RETENCAO_CSLL','N','CODIGO_RETENCAO_CSLL','SYNITF_IDF.CODIGO_RETENCAO_CSLL','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('372','CODIGO_RETENCAO_PIS','N','CODIGO_RETENCAO_PIS','SYNITF_IDF.CODIGO_RETENCAO_PIS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('373','CODIGO_RETENCAO_INSS','N','CODIGO_RETENCAO_INSS','SYNITF_IDF.CODIGO_RETENCAO_INSS','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('374','CODIGO_RETENCAO_PCC','N','CODIGO_RETENCAO_PCC','SYNITF_IDF.CODIGO_RETENCAO_PCC','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('375','VL_IRRF_NAO_RET','N','VL_IRRF_NAO_RET','SYNITF_IDF.VL_IRRF_NAO_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('376','VL_INSS_NAO_RET','N','VL_INSS_NAO_RET','SYNITF_IDF.VL_INSS_NAO_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('377','ALIQ_INSS_AE15_RET','N','ALIQ_INSS_AE15_RET','SYNITF_IDF.ALIQ_INSS_AE15_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('378','ALIQ_INSS_AE20_RET','N','ALIQ_INSS_AE20_RET','SYNITF_IDF.ALIQ_INSS_AE20_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('379','ALIQ_INSS_AE25_RET','N','ALIQ_INSS_AE25_RET','SYNITF_IDF.ALIQ_INSS_AE25_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('380','VL_INSS_AE15_NAO_RET','N','VL_INSS_AE15_NAO_RET','SYNITF_IDF.VL_INSS_AE15_NAO_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('381','VL_INSS_AE15_RET','N','VL_INSS_AE15_RET','SYNITF_IDF.VL_INSS_AE15_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('382','VL_INSS_AE20_NAO_RET','N','VL_INSS_AE20_NAO_RET','SYNITF_IDF.VL_INSS_AE20_NAO_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('383','VL_INSS_AE20_RET','N','VL_INSS_AE20_RET','SYNITF_IDF.VL_INSS_AE20_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('384','VL_INSS_AE25_NAO_RET','N','VL_INSS_AE25_NAO_RET','SYNITF_IDF.VL_INSS_AE25_NAO_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('385','VL_INSS_AE25_RET','N','VL_INSS_AE25_RET','SYNITF_IDF.VL_INSS_AE25_RET','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('386','VL_BASE_ICMS_FCPST','N','VL_BASE_ICMS_FCPST','SYNITF_IDF.VL_BASE_ICMS_FCPST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('387','VL_ICMS_FCPST','N','VL_ICMS_FCPST','SYNITF_IDF.VL_ICMS_FCPST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('388','ALIQ_ICMS_FCPST','N','ALIQ_ICMS_FCPST','SYNITF_IDF.ALIQ_ICMS_FCPST','N','N','','SYNITF_IDF',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('389','IND_INCIDENCIA_ICMS','N','','NULL','N','N','','',mREP_ID ,mVIS_ID );
        Syn_Gera_Script_pkg.Insert_Dic_Vis ('390','IND_ESCRITURACAO','N','','NULL','N','N','','',mREP_ID ,mVIS_ID );
 
EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
------------------
-- FIM DO BLOCO 3
------------------
END;
/
        
--------------------
-- INICIO DECLARE 4
--------------------
DECLARE
    mDAD_ID                  dic_dado.id%TYPE;
    mITF_ID                  syn_interface.id%TYPE;
    mITF_REP_DESTINO         syn_itf_repositorio.rep_id%TYPE;
    mDAD_ID_CTRL_CARGA       dic_dado.id%TYPE;
    mDAD_ID_CTRL_INSTR       dic_dado.id%TYPE;
    mVISINF_ID_ORIG_INSTR    dic_vis_informacao.id%TYPE;
    mITFREP_ID_SEQ           syn_itf_repositorio.id%TYPE;
    mVISINF_ID               dic_vis_informacao.id%TYPE;
    E                        VARCHAR2(1):=chr(13); -- ENTER
    T                        VARCHAR2(1):=chr(10); -- TAB
    mresp                    VARCHAR2(1):='S';
    
------------------
-- INICIO BLOCO 4
------------------
BEGIN
    IF mresp!='S' THEN
        RETURN;
    END IF;
    
     -- Pego o ID da interface
        mITF_ID := Syn_Gera_Script_pkg.Pego_ID_Interface ( 'HON_SYNITF_IDF' );
    
        ----------------------------
        -- 4 ) SYN_ITF_REPOSITORIO --
        ----------------------------
        mITF_REP_DESTINO:= Syn_Gera_Script_pkg.Pego_RepId_Destino( 'COR_IDF' ); -- Repositorio destino da ITF
        mDAD_ID_CTRL_CARGA:= NULL; --dad_id_ctrl_carga
        mDAD_ID_CTRL_INSTR:= NULL; --dad_id_ctrl_instr
        mVISINF_ID_ORIG_INSTR:= Syn_Gera_Script_pkg.Pego_VISINF_ID('VIS_HON_SYNITF_IDF', 'CTRL_INSTRUCAO'); --visinf_id_orig_instr
        BEGIN
            SELECT SYN_SEQ_INTERFACE.NEXTVAL INTO mITFREP_ID_SEQ FROM dual;
        END;
        BEGIN
        INSERT INTO SYN_ITF_REPOSITORIO (
         ID
         ,ITF_ID
         ,REP_ID
         ,ORDEM
         ,DAD_ID_CTRL_CARGA
         ,DAD_ID_CTRL_INSTR
         ,DEST_INSTR_INCLUSAO
         ,DEST_INSTR_ALTERACAO
         ,DEST_INSTR_EXCLUSAO
         ,DEST_INSTR_AUTO_INC_ALT
         ,DEST_CARGA_COMPLETA
         ,DEST_CARGA_INCOMPLETA
         ,API_PACKAGE
         ,ORIG_INSTR_INCLUSAO
         ,ORIG_INSTR_ALTERACAO
         ,ORIG_INSTR_EXCLUSAO
         ,ORIG_INSTR_AUTO_INC_ALT
         ,VISINF_ID_ORIG_INSTR
         ,IND_INVALIDO
         ,IND_DESATIVADO
         ,IND_INS_UPD
         ) VALUES (
         mITFREP_ID_SEQ
        , mITF_ID 
        , mITF_REP_DESTINO 
        ,'1'
        , mDAD_ID_CTRL_CARGA 
        , mDAD_ID_CTRL_INSTR 
        ,''
        ,''
        ,''
        ,''
        ,''
        ,''
        ,'CORAPI_IDF'
        ,'I'
        ,'A'
        ,'E'
        ,'M'
        , mVISINF_ID_ORIG_INSTR 
        ,'N'
        ,'N'
        ,'I');
        
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        ----------------------------
        --4.1) SYN_ITFREP_LAYOUT --
        ----------------------------
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'MUN_CODIGO','VIS_HON_SYNITF_IDF','MUN_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NOP_CODIGO','VIS_HON_SYNITF_IDF','NOP_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NUM_DI','VIS_HON_SYNITF_IDF','NUM_DI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'OM_CODIGO','VIS_HON_SYNITF_IDF','OM_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ORD_IMPRESSAO','VIS_HON_SYNITF_IDF','ORD_IMPRESSAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PESO_BRUTO_KG','VIS_HON_SYNITF_IDF','PESO_BRUTO_KG');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PESO_LIQUIDO_KG','VIS_HON_SYNITF_IDF','PESO_LIQUIDO_KG');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PRECO_TOTAL','VIS_HON_SYNITF_IDF','PRECO_TOTAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PRECO_UNITARIO','VIS_HON_SYNITF_IDF','PRECO_UNITARIO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PRES_CODIGO','VIS_HON_SYNITF_IDF','PRES_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'QTD','VIS_HON_SYNITF_IDF','QTD');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'SISS_CODIGO','VIS_HON_SYNITF_IDF','SISS_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STC_CODIGO','VIS_HON_SYNITF_IDF','STC_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STP_CODIGO','VIS_HON_SYNITF_IDF','STP_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'TIPO_COMPLEMENTO','VIS_HON_SYNITF_IDF','TIPO_COMPLEMENTO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_AJUSTE_PRECO_TOTAL','VIS_HON_SYNITF_IDF','VL_AJUSTE_PRECO_TOTAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS','VIS_HON_SYNITF_IDF','VL_BASE_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_INSS','VIS_HON_SYNITF_IDF','VL_BASE_INSS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_IPI','VIS_HON_SYNITF_IDF','VL_BASE_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ISS','VIS_HON_SYNITF_IDF','VL_BASE_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_STF','VIS_HON_SYNITF_IDF','VL_BASE_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_STT','VIS_HON_SYNITF_IDF','VL_BASE_STT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CONTABIL','VIS_HON_SYNITF_IDF','VL_CONTABIL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_DIFA','VIS_HON_SYNITF_IDF','VL_DIFA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_FATURADO','VIS_HON_SYNITF_IDF','VL_FATURADO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_FISCAL','VIS_HON_SYNITF_IDF','VL_FISCAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS','VIS_HON_SYNITF_IDF','VL_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_II','VIS_HON_SYNITF_IDF','VL_II');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS','VIS_HON_SYNITF_IDF','VL_INSS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IPI','VIS_HON_SYNITF_IDF','VL_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ISENTO_ICMS','VIS_HON_SYNITF_IDF','VL_ISENTO_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ISENTO_IPI','VIS_HON_SYNITF_IDF','VL_ISENTO_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ISS','VIS_HON_SYNITF_IDF','VL_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_OUTROS_ICMS','VIS_HON_SYNITF_IDF','VL_OUTROS_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_OUTROS_IPI','VIS_HON_SYNITF_IDF','VL_OUTROS_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_AJUSTE_PRECO','VIS_HON_SYNITF_IDF','VL_RATEIO_AJUSTE_PRECO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_FRETE','VIS_HON_SYNITF_IDF','VL_RATEIO_FRETE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_ODA','VIS_HON_SYNITF_IDF','VL_RATEIO_ODA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_SEGURO','VIS_HON_SYNITF_IDF','VL_RATEIO_SEGURO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_STF','VIS_HON_SYNITF_IDF','VL_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_STT','VIS_HON_SYNITF_IDF','VL_STT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_DIFA','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_DIFA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_ICMS','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_IPI','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_STF','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_STT','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_STT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_DO_SITE','VIS_HON_SYNITF_IDF','CODIGO_DO_SITE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','WROWID');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','INFORMANTE_EST_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_DIFA','VIS_HON_SYNITF_IDF','ALIQ_DIFA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS','VIS_HON_SYNITF_IDF','ALIQ_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_INSS','VIS_HON_SYNITF_IDF','ALIQ_INSS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_IPI','VIS_HON_SYNITF_IDF','ALIQ_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_II','VIS_HON_SYNITF_IDF','ALIQ_II');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ISS','VIS_HON_SYNITF_IDF','ALIQ_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DOF_ID','VIS_HON_SYNITF_IDF','DOF_ID');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'0','0','','N','N','N',mITF_REP_DESTINO,'DOF_SEQUENCE','VIS_HON_SYNITF_IDF','DOF_SEQUENCE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ALIQ_COFINS','VIS_HON_SYNITF_IDF','VL_ALIQ_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ALIQ_PIS','VIS_HON_SYNITF_IDF','VL_ALIQ_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_COFINS','VIS_HON_SYNITF_IDF','VL_BASE_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_PIS','VIS_HON_SYNITF_IDF','VL_BASE_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IMPOSTO_COFINS','VIS_HON_SYNITF_IDF','VL_IMPOSTO_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IMPOSTO_PIS','VIS_HON_SYNITF_IDF','VL_IMPOSTO_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','MSG_CRITICA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','CTRL_CRITICA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','DH_CRITICA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_IPI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_IRRF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_STT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','OBS_FISCAL_OPERACIONAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','INFORMANTE_PFJ_CHAVE_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','INFORMANTE_PFJ_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'MERC_CODIGO','VIS_HON_SYNITF_IDF','MERC_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'EMERC_CODIGO','VIS_HON_SYNITF_IDF','EMERC_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ID','VIS_HON_SYNITF_IDF','ID');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_ISS_MUNICIPIO','VIS_HON_SYNITF_IDF','CODIGO_ISS_MUNICIPIO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_INSS_RET','VIS_HON_SYNITF_IDF','ALIQ_INSS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_OUTROS_ABAT','VIS_HON_SYNITF_IDF','PERC_OUTROS_ABAT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_INSS_RET','VIS_HON_SYNITF_IDF','VL_BASE_INSS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_RET','VIS_HON_SYNITF_IDF','VL_INSS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_OUTROS_ABAT','VIS_HON_SYNITF_IDF','VL_OUTROS_ABAT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_VL_ICMS_NO_PRECO','VIS_HON_SYNITF_IDF','IND_VL_ICMS_NO_PRECO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CHASSI_VEIC','VIS_HON_SYNITF_IDF','CHASSI_VEIC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'TIPO_OPER_VEIC','VIS_HON_SYNITF_IDF','TIPO_OPER_VEIC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IOF','VIS_HON_SYNITF_IDF','VL_IOF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CONTA_CONTABIL','VIS_HON_SYNITF_IDF','CONTA_CONTABIL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'0','0','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','DH_INCLUSAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_STF_FRONTEIRA','VIS_HON_SYNITF_IDF','VL_BASE_STF_FRONTEIRA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_STF_IDO','VIS_HON_SYNITF_IDF','VL_BASE_STF_IDO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_STF_FRONTEIRA','VIS_HON_SYNITF_IDF','VL_STF_FRONTEIRA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_STF_IDO','VIS_HON_SYNITF_IDF','VL_STF_IDO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STA_CODIGO','VIS_HON_SYNITF_IDF','STA_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_PIS','VIS_HON_SYNITF_IDF','ALIQ_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'QTD_BASE_PIS','VIS_HON_SYNITF_IDF','QTD_BASE_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_PIS','VIS_HON_SYNITF_IDF','VL_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_COFINS','VIS_HON_SYNITF_IDF','ALIQ_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'QTD_BASE_COFINS','VIS_HON_SYNITF_IDF','QTD_BASE_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_COFINS','VIS_HON_SYNITF_IDF','VL_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NUM_TANQUE','VIS_HON_SYNITF_IDF','NUM_TANQUE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PARTICIPANTE_PFJ_CODIGO','VIS_HON_SYNITF_IDF','PARTICIPANTE_PFJ_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CLI_CODIGO','VIS_HON_SYNITF_IDF','CLI_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STN_CODIGO','VIS_HON_SYNITF_IDF','STN_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'TIPO_STF','VIS_HON_SYNITF_IDF','TIPO_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','SISS_CHAVE_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','SISS_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','PRES_CHAVE_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','PRES_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STI_CODIGO','VIS_HON_SYNITF_IDF','STI_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CNPJ_PAR','VIS_HON_SYNITF_IDF','CNPJ_PAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_IATA_FIM','VIS_HON_SYNITF_IDF','COD_IATA_FIM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_IATA_INI','VIS_HON_SYNITF_IDF','COD_IATA_INI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CPF_PAR','VIS_HON_SYNITF_IDF','CPF_PAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IE_PAR','VIS_HON_SYNITF_IDF','IE_PAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IM_SUBCONTRATACAO','VIS_HON_SYNITF_IDF','IM_SUBCONTRATACAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_ARM','VIS_HON_SYNITF_IDF','IND_ARM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NUM_ARM','VIS_HON_SYNITF_IDF','NUM_ARM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NUM_CANO','VIS_HON_SYNITF_IDF','NUM_CANO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'UF_PAR','VIS_HON_SYNITF_IDF','UF_PAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_DEDUCAO_DEPENDENTE_PRG','VIS_HON_SYNITF_IDF','VL_DEDUCAO_DEPENDENTE_PRG');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NBM_CODIGO','VIS_HON_SYNITF_IDF','NBM_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','DOF_IMPORT_NUMERO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IDF_NUM','VIS_HON_SYNITF_IDF','IDF_NUM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'SUBCLASSE_IDF','VIS_HON_SYNITF_IDF','SUBCLASSE_IDF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_STF','VIS_HON_SYNITF_IDF','ALIQ_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_STT','VIS_HON_SYNITF_IDF','ALIQ_STT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_IRRF','VIS_HON_SYNITF_IDF','VL_BASE_IRRF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IRRF','VIS_HON_SYNITF_IDF','VL_IRRF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'AM_CODIGO','VIS_HON_SYNITF_IDF','AM_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CFOP_CODIGO','VIS_HON_SYNITF_IDF','CFOP_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','CTRL_INSTRUCAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DT_DI','VIS_HON_SYNITF_IDF','DT_DI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ENTSAI_UNI_CODIGO','VIS_HON_SYNITF_IDF','ENTSAI_UNI_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'FIN_CODIGO','VIS_HON_SYNITF_IDF','FIN_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IDF_TEXTO_COMPLEMENTAR','VIS_HON_SYNITF_IDF','IDF_TEXTO_COMPLEMENTAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','MERC_CHAVE_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_DEDUCAO_PENSAO_PRG','VIS_HON_SYNITF_IDF','VL_DEDUCAO_PENSAO_PRG');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_IRRF','VIS_HON_SYNITF_IDF','PERC_IRRF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','MERC_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CCA_CODIGO','VIS_HON_SYNITF_IDF','CCA_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'TIPO_RECEITA','VIS_HON_SYNITF_IDF','TIPO_RECEITA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PFJ_CODIGO_TERCEIRO','VIS_HON_SYNITF_IDF','PFJ_CODIGO_TERCEIRO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','PFJ_TERCEIRO_CHAVE_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','PFJ_TERCEIRO_ORIGEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'UNI_FISCAL_CODIGO','VIS_HON_SYNITF_IDF','UNI_FISCAL_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_SERV','VIS_HON_SYNITF_IDF','IND_SERV');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DT_INI_SERV','VIS_HON_SYNITF_IDF','DT_INI_SERV');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DT_FIN_SERV','VIS_HON_SYNITF_IDF','DT_FIN_SERV');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PER_FISCAL','VIS_HON_SYNITF_IDF','PER_FISCAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_AREA','VIS_HON_SYNITF_IDF','COD_AREA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'TERMINAL','VIS_HON_SYNITF_IDF','TERMINAL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ANTECIP_ICMS','VIS_HON_SYNITF_IDF','ALIQ_ANTECIP_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS_DESC_L','VIS_HON_SYNITF_IDF','ALIQ_ICMS_DESC_L');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CUSTO_ADICAO','VIS_HON_SYNITF_IDF','CUSTO_ADICAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CUSTO_REDUCAO','VIS_HON_SYNITF_IDF','CUSTO_REDUCAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_ANTECIP_ICMS','VIS_HON_SYNITF_IDF','IND_ANTECIP_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_VL_PIS_COFINS_NO_PRECO','VIS_HON_SYNITF_IDF','IND_VL_PIS_COFINS_NO_PRECO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_ZFM_ALC','VIS_HON_SYNITF_IDF','IND_ZFM_ALC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ANTECIP_ICMS','VIS_HON_SYNITF_IDF','VL_ANTECIP_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS_DESC_L','VIS_HON_SYNITF_IDF','VL_BASE_ICMS_DESC_L');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_DESC_L','VIS_HON_SYNITF_IDF','VL_ICMS_DESC_L');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_TRIBUTAVEL_ICMS_DESC_L','VIS_HON_SYNITF_IDF','VL_TRIBUTAVEL_ICMS_DESC_L');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'STM_CODIGO','VIS_HON_SYNITF_IDF','STM_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_COFINS_ST','VIS_HON_SYNITF_IDF','ALIQ_COFINS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_PIS_ST','VIS_HON_SYNITF_IDF','ALIQ_PIS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_MOVIMENTA_ESTOQUE','VIS_HON_SYNITF_IDF','IND_MOVIMENTA_ESTOQUE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_COFINS_ST','VIS_HON_SYNITF_IDF','VL_BASE_COFINS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_PIS_ST','VIS_HON_SYNITF_IDF','VL_BASE_PIS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_COFINS_ST','VIS_HON_SYNITF_IDF','VL_COFINS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_PIS_ST','VIS_HON_SYNITF_IDF','VL_PIS_ST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_COFINS_RET','VIS_HON_SYNITF_IDF','ALIQ_COFINS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_CSLL_RET','VIS_HON_SYNITF_IDF','ALIQ_CSLL_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_PIS_RET','VIS_HON_SYNITF_IDF','ALIQ_PIS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_ISS_RETIDO_FONTE','VIS_HON_SYNITF_IDF','IND_ISS_RETIDO_FONTE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_VL_TRIB_RET_NO_PRECO','VIS_HON_SYNITF_IDF','IND_VL_TRIB_RET_NO_PRECO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_TRIBUTAVEL_INSS_RET','VIS_HON_SYNITF_IDF','PERC_TRIBUTAVEL_INSS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_COFINS_RET','VIS_HON_SYNITF_IDF','VL_BASE_COFINS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_CSLL_RET','VIS_HON_SYNITF_IDF','VL_BASE_CSLL_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_PIS_RET','VIS_HON_SYNITF_IDF','VL_BASE_PIS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_COFINS_RET','VIS_HON_SYNITF_IDF','VL_COFINS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CSLL_RET','VIS_HON_SYNITF_IDF','VL_CSLL_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_PIS_RET','VIS_HON_SYNITF_IDF','VL_PIS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_II','VIS_HON_SYNITF_IDF','VL_BASE_II');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_COFINS_REC_EXPO','VIS_HON_SYNITF_IDF','VL_CRED_COFINS_REC_EXPO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_COFINS_REC_NAO_TRIB','VIS_HON_SYNITF_IDF','VL_CRED_COFINS_REC_NAO_TRIB');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_COFINS_REC_TRIB','VIS_HON_SYNITF_IDF','VL_CRED_COFINS_REC_TRIB');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_PIS_REC_EXPO','VIS_HON_SYNITF_IDF','VL_CRED_PIS_REC_EXPO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_PIS_REC_NAO_TRIB','VIS_HON_SYNITF_IDF','VL_CRED_PIS_REC_NAO_TRIB');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_CRED_PIS_REC_TRIB','VIS_HON_SYNITF_IDF','VL_CRED_PIS_REC_TRIB');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_CCUS','VIS_HON_SYNITF_IDF','COD_CCUS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PFJ_CODIGO_FORNECEDOR','VIS_HON_SYNITF_IDF','PFJ_CODIGO_FORNECEDOR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_NAT_FRT_PISCOFINS','VIS_HON_SYNITF_IDF','IND_NAT_FRT_PISCOFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NAT_REC_PISCOFINS','VIS_HON_SYNITF_IDF','NAT_REC_PISCOFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NAT_REC_PISCOFINS_DESCR','VIS_HON_SYNITF_IDF','NAT_REC_PISCOFINS_DESCR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_MOVIMENTACAO_CIAP','VIS_HON_SYNITF_IDF','IND_CIAP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_MOVIMENTACAO_CPC','VIS_HON_SYNITF_IDF','IND_CPC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_FISC_SERV_MUN','VIS_HON_SYNITF_IDF','COD_FISC_SERV_MUN');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'COD_BC_CREDITO','VIS_HON_SYNITF_IDF','COD_BC_CREDITO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CSOSN_CODIGO','VIS_HON_SYNITF_IDF','CSOSN_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'NBS_CODIGO','VIS_HON_SYNITF_IDF','NBS_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_PART_CI','VIS_HON_SYNITF_IDF','PERC_PART_CI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IMP_FCI','VIS_HON_SYNITF_IDF','VL_IMP_FCI');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'FCI_NUMERO','VIS_HON_SYNITF_IDF','FCI_NUMERO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DESCRICAO_COMPLEMENTAR_SERVICO','VIS_HON_SYNITF_IDF','DESCRICAO_COMPLEMENTAR_SERVICO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ABAT_LEGAL_INSS_RET','VIS_HON_SYNITF_IDF','VL_ABAT_LEGAL_INSS_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ABAT_LEGAL_IRRF','VIS_HON_SYNITF_IDF','VL_ABAT_LEGAL_IRRF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ABAT_LEGAL_ISS','VIS_HON_SYNITF_IDF','VL_ABAT_LEGAL_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DT_REF_CALC_IMP_IDF','VIS_HON_SYNITF_IDF','DT_REF_CALC_IMP_IDF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'DESONERACAO_CODIGO','VIS_HON_SYNITF_IDF','DESONERACAO_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_EXIGIBILIDADE_ISS','VIS_HON_SYNITF_IDF','IND_EXIGIBILIDADE_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_INCENTIVO_FISCAL_ISS','VIS_HON_SYNITF_IDF','IND_INCENTIVO_FISCAL_ISS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CNPJ_SUBEMPREITEIRO','VIS_HON_SYNITF_IDF','CNPJ_SUBEMPREITEIRO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_INCIDENCIA_ICMS','VIS_HON_SYNITF_IDF','IND_INCIDENCIA_ICMS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS_FCPST','VIS_HON_SYNITF_IDF','VL_BASE_ICMS_FCPST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_FCPST','VIS_HON_SYNITF_IDF','VL_ICMS_FCPST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS_FCPST','VIS_HON_SYNITF_IDF','ALIQ_ICMS_FCPST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_ESCRITURACAO','VIS_HON_SYNITF_IDF','IND_ESCRITURACAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ADICIONAL_RET_AE','VIS_HON_SYNITF_IDF','VL_ADICIONAL_RET_AE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_GILRAT','VIS_HON_SYNITF_IDF','VL_GILRAT');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_MATERIAS_EQUIP','VIS_HON_SYNITF_IDF','VL_MATERIAS_EQUIP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_NAO_RETIDO_CP','VIS_HON_SYNITF_IDF','VL_NAO_RETIDO_CP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_SENAR','VIS_HON_SYNITF_IDF','VL_SENAR');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_SERVICO_AE15','VIS_HON_SYNITF_IDF','VL_SERVICO_AE15');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_SERVICO_AE20','VIS_HON_SYNITF_IDF','VL_SERVICO_AE20');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_SERVICO_AE25','VIS_HON_SYNITF_IDF','VL_SERVICO_AE25');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_MP_DO_BEM','VIS_HON_SYNITF_IDF','IND_MP_DO_BEM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_SIMPLES_NAC','VIS_HON_SYNITF_IDF','VL_ICMS_SIMPLES_NAC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_TRIBUTAVEL_STF','VIS_HON_SYNITF_IDF','PERC_TRIBUTAVEL_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CEST_CODIGO','VIS_HON_SYNITF_IDF','CEST_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_ICMS_PART_REM','VIS_HON_SYNITF_IDF','PERC_ICMS_PART_REM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS_PART_REM','VIS_HON_SYNITF_IDF','VL_BASE_ICMS_PART_REM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS_PART_REM','VIS_HON_SYNITF_IDF','ALIQ_ICMS_PART_REM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_PART_REM','VIS_HON_SYNITF_IDF','VL_ICMS_PART_REM');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'PERC_ICMS_PART_DEST','VIS_HON_SYNITF_IDF','PERC_ICMS_PART_DEST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS_PART_DEST','VIS_HON_SYNITF_IDF','VL_BASE_ICMS_PART_DEST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS_PART_DEST','VIS_HON_SYNITF_IDF','ALIQ_ICMS_PART_DEST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_PART_DEST','VIS_HON_SYNITF_IDF','VL_ICMS_PART_DEST');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_ICMS_FCP','VIS_HON_SYNITF_IDF','ALIQ_ICMS_FCP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_BASE_ICMS_FCP','VIS_HON_SYNITF_IDF','VL_BASE_ICMS_FCP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_ICMS_FCP','VIS_HON_SYNITF_IDF','VL_ICMS_FCP');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_DIFA_ICMS_PART','VIS_HON_SYNITF_IDF','ALIQ_DIFA_ICMS_PART');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ENQ_IPI_CODIGO','VIS_HON_SYNITF_IDF','ENQ_IPI_CODIGO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','HASH');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_BASE_CT_STF','VIS_HON_SYNITF_IDF','VL_RATEIO_BASE_CT_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RATEIO_CT_STF','VIS_HON_SYNITF_IDF','VL_RATEIO_CT_STF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'','VIS_HON_SYNITF_IDF','CTRL_SITUACAO_DOF');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'IND_CONTABILIZACAO','VIS_HON_SYNITF_IDF','IND_CONTABILIZACAO');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_RETIDO_SUBEMPREITADA','VIS_HON_SYNITF_IDF','VL_RETIDO_SUBEMPREITADA');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_NAO_RETIDO_CP_AE','VIS_HON_SYNITF_IDF','VL_NAO_RETIDO_CP_AE');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO_COFINS','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO_COFINS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO_CSLL','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO_CSLL');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO_PIS','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO_PIS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO_INSS','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO_INSS');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'CODIGO_RETENCAO_PCC','VIS_HON_SYNITF_IDF','CODIGO_RETENCAO_PCC');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_IRRF_NAO_RET','VIS_HON_SYNITF_IDF','VL_IRRF_NAO_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_NAO_RET','VIS_HON_SYNITF_IDF','VL_INSS_NAO_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_INSS_AE15_RET','VIS_HON_SYNITF_IDF','ALIQ_INSS_AE15_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_INSS_AE20_RET','VIS_HON_SYNITF_IDF','ALIQ_INSS_AE20_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'ALIQ_INSS_AE25_RET','VIS_HON_SYNITF_IDF','ALIQ_INSS_AE25_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE15_NAO_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE15_NAO_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE15_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE15_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE20_NAO_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE20_NAO_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE20_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE20_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE25_NAO_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE25_NAO_RET');
        
        Syn_Gera_Script_pkg.Insert_Dic_ItfRep (mITFREP_ID_SEQ,'','','','N','N','N',mITF_REP_DESTINO,'VL_INSS_AE25_RET','VIS_HON_SYNITF_IDF','VL_INSS_AE25_RET');
        
        
        -------------------------------
        --4.2) SYN_ITFREP_VALIDACAO --
        -------------------------------
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'A','I','E'
        ,'-- Acha dof_sequence'||E||''||T||'BEGIN '||E||''||T||'select codigo_do_site,dof_sequence,id,ctrl_situacao_dof into mreg.codigo_do_site,mreg.dof_sequence,mreg.dof_id,mreg.ctrl_situacao_dof from cor_dof where informante_est_codigo=mreg.informante_est_codigo and dof_import_numero= mreg.dof_import_numero;EXCEPTION when no_data'
        ||'_found then '||E||''||T||'raise_application_error(-20001,''N�o encontrado DOF para o estabelecimento '''''' ||mreg.informante_est_codigo||'''''' e DOF_IMPORT_NUMERO ''''''||mreg.dof_import_numero||'''''''');END;'||E||''||T||'-- Acha o ID do IDF'||E||''||T||'mreg.id:=cor_retorna_id_idf(mreg.codigo_do_site,mreg.dof_sequence,mreg.idf_num'
        ||');'||E||''||T||'IF mreg.id=-1 THEN mreg.id:=null;END IF;'||E||''||T||' '||E||''||T||'IF mreg.fin_codigo IS NULL THEN--Deduzo pela aplicacao'||E||''||T||'mreg.fin_codigo:=syn_itf_validacoes.deduz_fin_codigo(mreg.am_codigo);'||E||''||T||'END IF;'||E||''||T||''||E||''||T||'-- begin vda Ocor 1270805'||E||''||T||'syn_itf_validacoes.synitf_critica_item(mreg.subclasse_idf,mreg.merc_codigo,mreg.merc_o'
        ||'rigem,mreg.merc_chave_origem,mreg.pres_codigo,mreg.pres_origem,mreg.pres_chave_origem,mreg.siss_codigo,mreg.siss_origem,mreg.siss_chave_origem);'||E||''||T||'--end vda Ocor 1270805'||E||''||T||''||E||''||T||'-- begin vda Ocor 1260994'||E||''||T||'if(mreg.cfop_codigo is not null)then'||E||''||T||'    syn_itf_validacoes.valida_cfop(mreg.cfop_codigo);'||E||''||T||'end if;'||E||''
        ||''||T||'if mreg.nop_codigo is not null then'||E||''||T||'    syn_itf_validacoes.valida_nop(mreg.nop_codigo);'||E||''||T||'/*cdc ocorr 1683856 06/05/09 else mreg.nop_codigo:=syn_itf_validacoes.retorna_nop_valida(mreg.cfop_codigo,mreg.informante_est_codigo,mreg.dof_import_numero, mreg.subclasse_idf);*/'||E||''||T||''||E||''||T||'-- BEGIN JSJ Ocor2365226 0'
        ||'5/10/10'||E||''||T||'ELSE'||E||''||T||' IF (cor_le_inicializacao(NULL, ''DEDUZ_NOP_INTERFACE_IDF'', ''N'') = ''N'') THEN'||E||''||T||'      raise_application_error(-20001, ''Campo NOP_CODIGO e obrigatorio!'');'||E||''||T||' END IF;'||E||''||T||'end if;'||E||''||T||'-- END JSJ Ocor2365226 05/10/10'||E||''||T||'-- end vda Ocor 1260994'||E||''||T||''||E||''||T||'--Begin custom - CMG 09/02/2009'||E||''||T||'if mreg.siss_'
        ||'codigo is null and mreg.merc_codigo is null and mreg.pres_codigo is null then'||E||''||T||'   raise_application_error(-20001,'' E obrigatorio o preenchimento do de c�digo do item (servi�os, mercadoria ou presta��o) ''  );'||E||''||T||'end if;'||E||''||T||''||E||''||T||'IF mreg.subclasse_idf not in (''M'',''P'',''S'',''O'') THEN'||E||''||T||'raise_application'
        ||'_error(-20001,'' SubClasse IDF deve ser M,P,S ou O ''  );'||E||''||T||'end if;'||E||''||T||'--End custom - CMG 09/02/2009'||E||''||T||''||E||''||T||'-- TBT AD.3050835 11/11/2016'||E||''||T||'IF mreg.ctrl_situacao_dof = ''I'' THEN'||E||''||T||'cor_idf_cons_pkg.disable;'||E||''||T||'mreg.ID := COR_DOF_IDF_ID(mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num);'||E||''||T||'--END IF;'||E||''||T||''||E||''||T||'      S'
        ||'ELECT especie.ind_escrituracao'||E||''||T||'        INTO mreg.ind_escrituracao'||E||''||T||'        FROM cor_dof dof'||E||''||T||'       INNER JOIN cor_especie_dof especie'||E||''||T||'          ON dof.edof_codigo = especie.edof_codigo'||E||''||T||'       WHERE dof.codigo_do_site = mreg.codigo_do_site'||E||''||T||'         AND dof.dof_sequence = mreg.dof_sequence;'||E||''||T||'    '
        ||''||E||''||T||'    '||E||''||T||'      IF mreg.ind_escrituracao = ''S'' THEN'||E||''||T||'        IF mreg.nop_codigo IS NOT NULL THEN'||E||''||T||'          SELECT ind_escrituracao'||E||''||T||'            INTO mreg.ind_escrituracao'||E||''||T||'            FROM cor_natureza_de_operacao'||E||''||T||'           WHERE nop_codigo = mreg.nop_codigo;'||E||''||T||'        ELSE'||E||''||T||'          SELECT nop.i'
        ||'nd_escrituracao'||E||''||T||'            INTO mreg.ind_escrituracao'||E||''||T||'            FROM cor_dof dof'||E||''||T||'           INNER JOIN cor_natureza_de_operacao nop'||E||''||T||'              ON dof.nop_codigo = nop.nop_codigo'||E||''||T||'           WHERE dof.codigo_do_site = mreg.codigo_do_site'||E||''||T||'             AND dof.dof_sequence = mreg.dof_sequenc'
        ||'e;'||E||''||T||'        END IF;'||E||''||T||'      END IF;'||E||''||T||'mreg.ind_incidencia_icms:=mreg.ind_escrituracao;'||E||''||T||'    END IF;'||E||''||T||''
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'A','E','R'
        ,'mdata.v.CODIGO_DO_SITE := mreg.CODIGO_DO_SITE;'||E||''||T||'mdata.v.DOF_SEQUENCE   := mreg.DOF_SEQUENCE;'||E||''||T||'mdata.v.IDF_NUM        := mreg.IDF_NUM;'||E||''||T||''
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'D','C','R'
        ,'cor_idf_cons_pkg.enable;'||E||''||T||''||E||''||T||'UPDATE SYNITF_IDF SET MSG_CRITICA = merror, CTRL_CRITICA = CTRL_CRITICA + 1, DH_CRITICA = SYSDATE WHERE ROWID = mreg.WROWID;'||E||''||T||''||E||''||T||'--Begin custom CMG 02/09/2009'||E||''||T||'UPDATE HON_ID_DOF SET FLAG_IDF = NULL WHERE DOF_SEQUENCE = MREG.DOF_SEQUENCE; COMMIT;'||E||''||T||'--End custom CMG 02/09/200'
        ||'9'
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'A','I','P'
        ,'-- Verifico se o PFJ_CODIGO / MERC_CODIGO  deve ser deduzido'||E||''||T||''||E||''||T||'syn_itf.v_deduz_pfj_codigo   := syn_itf.deduz_pfj_codigo ;'||E||''||T||'syn_itf.v_deduz_merc_codigo  := syn_itf.deduz_merc_codigo ;'||E||''||T||'syn_itf.v_deduz_pres_codigo  := syn_itf.deduz_pres_codigo ;'||E||''||T||'syn_itf.v_deduz_siss_codigo  := syn_itf.deduz_siss_cod'
        ||'igo ;'||E||''||T||''||E||''||T||'-- TBT AD.2899232 09/10/2012 (Begin) - Verifica se ir� acionar o carimbador da DIRF.'||E||''||T||'if (upper(COR_LE_INICIALIZACAO(null,''CARIMBADOR_DIRF'',null)) = ''N'') Then'||E||''||T||'  SRF_CARIMBADOR_DIRF_PKG.disable;'||E||''||T||'end if;'||E||''||T||'-- TBT AD.2899232 09/10/2012 (End)'||E||''||T||''
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'D','G','R'
        ,'-- Vamos Gravar as Mensagens do IDF'||E||''||T||'if(mreg.ctrl_instrucao <> ''E'') then'||E||''||T||'IF mreg.obs_fiscal_icms IS NOT NULL THEN'||E||''||T||'    syn_itf_validacoes.mensagens_idf ( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_icms,  ''ICMS'' );'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_ipi IS NOT NULL THEN'||E||''||T||'   '
        ||' syn_itf_validacoes.mensagens_idf ( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_ipi,  ''IPI'' );'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_irrf IS NOT NULL THEN'||E||''||T||'    syn_itf_validacoes.mensagens_idf ( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_irrf,  ''IRRF'' '
        ||');'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_iss IS NOT NULL THEN'||E||''||T||'    syn_itf_validacoes.mensagens_idf ( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_iss,  ''ISS'' );'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_stt IS NOT NULL THEN'||E||''||T||'    syn_itf_validacoes.mensagens_idf (  mreg.codigo_do_site, mreg.dof'
        ||'_sequence, mreg.idf_num, mreg.obs_fiscal_stt,  ''STT'' );'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_stf IS NOT NULL THEN'||E||''||T||'    syn_itf_validacoes.mensagens_idf( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_stf,  ''STF'' );'||E||''||T||'END IF;'||E||''||T||'IF mreg.obs_fiscal_operacional IS NOT NULL THEN'||E||''||T||'    syn'
        ||'_itf_validacoes.mensagens_idf ( mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, mreg.obs_fiscal_operacional,  ''OPERACIONAL'' );'||E||''||T||'END IF;'||E||''||T||'end if;'||E||''||T||''||E||''||T||'-- TBT AD.3050835 11/11/2016'||E||''||T||'cor_idf_cons_pkg.enable;'||E||''||T||''||E||''||T||'--Begin Custom CMG - 09/02/2009'||E||''||T||'UPDATE HON_ID_DOF SET FLAG_IDF = ''S'' WHERE DOF_SEQU'
        ||'ENCE = MREG.DOF_SEQUENCE;COMMIT;'||E||''||T||'--End Custom CMG - 09/02/2009'||E||''||T||''||E||''||T||'DELETE FROM  SYNITF_IDF  WHERE  ROWID = mreg.WROWID;'
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'D','I','R'
        ,'-- apm gravar na tabela de track p/ enviar ao RST'||E||''||T||'RST_GRAVA_DOF_TRACK(mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, ''entrada'', mprocesso);'||E||''||T||'-- fim apm'
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'A','G','R'
        ,'IF mreg.subclasse_idf <> ''S'' THEN'||E||''||T||'  mreg.idf_texto_complementar := SUBSTR(mreg.idf_texto_complementar, 1, 1000);'||E||''||T||'END IF;'||E||''||T||''||E||''||T||'IF syn_itf.v_deduz_pfj_codigo=''S''THEN mreg.informante_est_codigo:=cor_retorna_pfj_codigo(mreg.informante_pfj_chave_origem,mreg.informante_pfj_origem);'||E||''||T||'mreg.pfj_codigo_te'
        ||'rceiro:=cor_retorna_pfj_codigo(mreg.pfj_terceiro_chave_origem, mreg.pfj_terceiro_origem);'||E||''||T||'END IF;'||E||''||T||'IF syn_itf.v_deduz_merc_codigo=''S''THEN mreg.merc_codigo:=cor_retorna_merc_codigo(mreg.merc_chave_origem,mreg.merc_origem);END IF;'||E||''||T||''||E||''||T||'--BEGIN CUSTOM ACE'||E||''||T||'IF mreg.SUBCLASSE_IDF NOT IN (''M'',''P'') TH'
        ||'EN mreg.fin_codigo:=null;mreg.nbm_codigo:=null;mreg.peso_bruto_kg:=null;mreg.peso_liquido_kg:=null;mreg.merc_codigo:=null;END IF;'||E||''||T||'IF mreg.SUBCLASSE_IDF = ''S'' AND mreg.informante_est_codigo IN (''05001'',''10001'',''15001'',''05098'',''05096'') THEN'||E||''||T||'mreg.fin_codigo:=null;mreg.AM_codigo:=null;mreg'
        ||'.stp_codigo:=null;mreg.nbm_codigo:=null;mreg.peso_bruto_kg:=null;mreg.peso_liquido_kg:=null;mreg.merc_codigo:=null;END IF;--END CUSTOM ACE'||E||''||T||''||E||''||T||'/*cdc ocorr 1635022 15/04/2009'||E||''||T||'IF mreg.SUBCLASSE_IDF<>''M''THEN'||E||''||T||'mreg.fin_codigo:=null; mreg.am_codigo:=null; mreg.nbm_codigo:=null; mreg.merc_codigo:=null; '
        ||'mreg.om_codigo:=null; mreg.peso_bruto_kg:=null;'||E||''||T||'mreg.peso_liquido_kg:=null;'||E||''||T||'--cdc 1548300 15/12/2008 - comentado a pedido do RMP'||E||''||T||'--mreg.stc_codigo:=null;mreg.stp_codigo:=null;'||E||''||T||'--mreg.entsai_uni_codigo:=null;'||E||''||T||'END IF;'||E||''||T||'cdc fim */'||E||''||T||'--comentado a pedido Augusto 25/03/04 apm'||E||''||T||'--IF mreg.subclasse_idf'
        ||' is null THEN raise_application_error(-20330,''SUBCLASSE_IDF nao pode ser Nula'');END IF;'||E||''||T||''||E||''||T||'syn_itf_validacoes.consiste_cod_retencao(mreg.codigo_retencao);'||E||''||T||'if nvl(mreg.ind_vl_icms_no_preco,''X'')not in(''S'',''N'')then'||E||''||T||'raise_application_error(-20444,''ind_vl_icms_no_preco deve ser "S" ou "N".'');'
        ||''||E||''||T||'end if;'||E||''||T||'-- begin vda Ocor 1265729'||E||''||T||'begin if(mreg.uf_par is not null)then select uf_codigo into mreg.uf_par from cor_unidade_federativa where uf_codigo=mreg.uf_par;'||E||''||T||'end if;exception when no_data_found then raise_application_error(-20001,''UF_PAR inv�lido!'');end;'||E||''||T||'-- end vda'||E||''||T||''||E||''||T||'-- jrg 04/10/05: d'
        ||'eduzir cfop se for nulo'||E||''||T||'if mreg.cfop_codigo is null then'||E||''||T||'   if syn_itf_validacoes.deduz_parametro(''COR_VALIDA_NOP_X_CFOP'') = ''N'' then'||E||''||T||'      raise_application_error(-20330, ''CFOP_CODIGO nao pode ser Nulo'');'||E||''||T||'   else mreg.cfop_codigo := syn_itf_validacoes.retorna_cfop_valido_import(mreg.nop_c'
        ||'odigo, mreg.informante_est_codigo, mreg.dof_import_numero);'||E||''||T||'      if mreg.cfop_codigo is null then raise_application_error(-20331, ''CFOP_CODIGO nao encontrado na tabela COR_NOP_X_CFOP''); end if;end if; end if;'||E||''||T||''||E||''||T||'-- JSJ Ocor2410793'||E||''||T||'-- JSJ Ocor1703922'||E||''||T||'IF (mreg.om_codigo is null) THEN'||E||''||T||' --JSJ Oco'
        ||'r2481781'||E||''||T||' IF mreg.merc_codigo IS NOT NULL THEN'||E||''||T||'  BEGIN'||E||''||T||'   SELECT dflt_om_codigo INTO mreg.om_codigo FROM cor_mercadoria WHERE merc_codigo = mreg.merc_codigo;'||E||''||T||'  EXCEPTION WHEN no_data_found THEN raise_application_error(-20001,''Mercadoria (''|| mreg.merc_codigo ||'') nao cadastrada'');'||E||''||T||'  END;'||E||''||T||' '
        ||'END IF;'||E||''||T||' IF (trim(mreg.om_codigo) is null) THEN'||E||''||T||'  mreg.om_codigo := 0;'||E||''||T||' END IF;'||E||''||T||'END IF;'||E||''||T||'--TBT AD.2891999 21/08/2012 (Begin)'||E||''||T||'IF mreg.subclasse_idf not in (''P'',''S'') THEN mreg.nbs_codigo:=null; END IF;'||E||''||T||'--TBT AD.2891999 21/08/2012 (End)'
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'D','I','P'
        ,'declare'||E||''||T||'mitem number;'||E||''||T||'mcount number;'||E||''||T||'begin'||E||''||T||''||E||''||T||'-- LHB - Incluido um teste para ver se o estabelecimento usa o RST'||E||''||T||'-- Ocorrencia:1455984 - Data: 27/08/2008'||E||''||T||'select count(*) '||E||''||T||'into   mcount'||E||''||T||'from cor_dof where '||E||''||T||'codigo_do_site = mreg.codigo_do_site'||E||''||T||'and dof_sequence = mreg.dof_sequence'||E||''||T||'and destina'
        ||'tario_pfj_codigo in ('||E||''||T||'SELECT est_codigo'||E||''||T||'FROM cor_estabelecimento'||E||''||T||'where est_codigo = destinatario_pfj_codigo'||E||''||T||'      and'||E||''||T||'      ind_controla_st = ''S'''||E||''||T||');'||E||''||T||''||E||''||T||'IF mcount > 0 THEN'||E||''||T||''||E||''||T||'-- apm RST'||E||''||T||'-- envia p/ cst dados de notas de entrada que estao sendo incluidas no sistema'||E||''||T||' rst_envia_dados(''entrada'
        ||''', mprocesso,null,null,null, mitem);'||E||''||T||''||E||''||T||'END IF;'||E||''||T||'-- LHB - FIM'||E||''||T||''||E||''||T||'end;'||E||''||T||''||E||''||T||'-- TBT AD.2899232 09/10/2012 (Begin) - Aciona o carimbador da DIRF.'||E||''||T||'SRF_CARIMBADOR_DIRF_PKG.enable;'||E||''||T||'-- TBT AD.2899232 09/10/2012 (End)'
        ,'','N');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        BEGIN
        INSERT INTO SYN_ITFREP_VALIDACAO ( ID ,ITFREP_ID ,QUANDO ,EVENTO ,PARA ,CODIGO_PLSQL ,CONDICAO_EXECUCAO ,IND_DESATIVADO ) VALUES (SYN_SEQ_INTERFACE.NEXTVAL, mITFREP_ID_SEQ,'D','A','R'
        ,'-- apm gravar na tabela de track p/ enviar ao RST'||E||''||T||'--RST_GRAVA_DOF_TRACK(mreg.codigo_do_site, mreg.dof_sequence, mreg.idf_num, ''correcao'', mprocesso);'||E||''||T||'-- fim apm'
        ,'','S');
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  NULL; END;
        
        
        
        COMMIT;
 
        --Testa a Interface
        BEGIN
           syn_itf.testa(mitf_id);
        EXCEPTION WHEN OTHERS THEN
           NULL;
        END;
 
 
EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    RAISE;
------------------
-- FIM DO BLOCO 4
------------------
END;
/
 
