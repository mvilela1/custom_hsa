--------------------------------------------------------
--  Arquivo criado - S�bado-Julho-07-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body SAP_DOF_UTIL
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PECAS"."SAP_DOF_UTIL" IS
  -- ---------   --------  ----------------------------------------------
  -- MODIFICATION HISTORY
  -- Person      Date      Comments
  -- ---------   --------  ----------------------------------------------
  -- cdc        20180503   PrettyPrinter
  -- ---------   --------  ----------------------------------------------

  sufixo_estab       varchar2(2) := null;
  sufixo_estab_parid varchar2(2) := null;

  function get_dof_info(pdof        in varchar2,
                        pinformante varchar2,
                        pmodulo     varchar2,
                        pITF        BOOLEAN := FALSE,
                        pDOCNUM     varchar2 := NULL,
                        pITMNUM     varchar2 := NULL,
                        pPROG       varchar2 := NULL) return dof_values is
    mdof_data       dof_values;
    empty_dof_data  dof_values;
    mpfj_data       dof_pfj;
    empty_pfj_data  dof_pfj;
    mCOR_data       CORAPI_DOF.data;
    mCOR_clear      CORAPI_DOF.data;
    mIN_data        INAPI_DOF.data;
    mIN_clear       INAPI_DOF.data;
    mdof_itf        dof_itf;
    midf_itf        idf_itf;
    msql            VARCHAR2(1000);
    mcur            PLS_INTEGER;
    mrows_processed PLS_INTEGER;
    mcount          PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
    mId             number;
    mId_IDF         number;
    mDOF_TOT        COR_DOF.VL_TOTAL_CONTABIL%type;
    mDOCREF         varchar2(10);
    mPFJ_emitente   cor_dof.emitente_pfj_codigo%type;
    mteste          in_dof.id%type;
    mxml_id         NUMBER;
  
    mDT_REFERENCIA    cor_dof.dh_emissao%type;
    mUF_REFERENCIA    cor_unidade_federativa.uf_codigo%type;
    mPFJ_destinatario cor_dof.destinatario_pfj_codigo%type;
    mPFJ_remetente    cor_dof.remetente_pfj_codigo%type;
  
  begin
    If not pITF Then
      Begin
        If pmodulo = 'IN' Then
          Select id into mId from in_dof where chave_origem = pdof;
          INAPI_DOF.sel(mIN_data);
          mdof_data.DOF_ID := mIN_data.v.ID;
        
        Elsif pmodulo = 'COR' Then
          Begin
            Select codigo_do_site, dof_sequence
              into mCOR_data.v.codigo_do_site, mCOR_data.v.dof_sequence
              from cor_dof
             where informante_est_codigo = pinformante
               and dof_import_numero = pdof;
          Exception
            when no_data_found then
              Select codigo_do_site, dof_sequence
                into mCOR_data.v.codigo_do_site, mCOR_data.v.dof_sequence
                from cor_dof
               where dof_import_numero = pdof;
          End;
        
          CORAPI_DOF.sel(mCOR_data);
          mdof_data.EMITENTE_PFJ_CODIGO     := mCOR_data.v.EMITENTE_PFJ_CODIGO;
          mdof_data.SERIE_SUBSERIE          := mCOR_data.v.SERIE_SUBSERIE;
          mdof_data.NUMERO                  := mCOR_data.v.NUMERO;
          mdof_data.EDOF_CODIGO             := mCOR_data.v.EDOF_CODIGO;
          mdof_data.DH_EMISSAO              := mCOR_data.v.DH_EMISSAO;
          mdof_data.DOF_IMPORT_NUMERO       := mCOR_data.v.DOF_IMPORT_NUMERO;
          mdof_data.VL_TOTAL_CONTABIL       := mCOR_data.v.VL_TOTAL_CONTABIL;
          mdof_data.DOF_ID                  := mCOR_data.v.ID;
          mdof_data.DOF_SEQUENCE            := mCOR_data.v.DOF_SEQUENCE;
          mdof_data.CODIGO_DO_SITE          := mCOR_data.v.CODIGO_DO_SITE;
          mdof_data.NOP_CODIGO              := mCOR_data.v.NOP_CODIGO;
          mdof_data.CFOP_CODIGO             := mCOR_data.v.CFOP_CODIGO;
          mdof_data.IND_ENTRADA_SAIDA       := mCOR_data.v.IND_ENTRADA_SAIDA;
          mdof_data.DESTINATARIO_PFJ_CODIGO := mCOR_data.v.DESTINATARIO_PFJ_CODIGO;
          mdof_data.CTRL_SITUACAO_DOF       := mCOR_data.v.CTRL_SITUACAO_DOF;
          mdof_data.VL_TOTAL_BASE_STF       := mCOR_data.v.VL_TOTAL_BASE_STF;
        
        End if;
      Exception
        when no_data_found Then
          raise_application_error(-20001,
                                  'Dof ' || pdof || ' nao encontrado em ' ||
                                  pmodulo || '_DOF');
      End;
      return mdof_data;
    Else
      msql := 'SELECT /*+ index(DOF sap_itf_dof_' || pmodulo ||
              '_docnum_i )*/';
      msql := msql || ' DOF.id DOF_ID,';
      msql := msql || ' IDF.id IDF_ID,';
      msql := msql ||
              ' TO_NUMBER(DOFSUM.NFTOT,''999999999999999D99'') DOF_TOT,';
      msql := msql || ' DOF.DOCREF DOCREF,';
      msql := msql || ' DOF.XML_ID DOF_XML_ID';
      msql := msql || ' FROM';
      msql := msql || ' SAP_ITF_DOF_' || pmodulo || ' DOF';
      msql := msql || ' ,SAP_ITF_IDF_' || pmodulo || ' IDF';
      msql := msql || ' ,SAP_ITF_TOTAL_DOF_' || pmodulo || ' DOFSUM';
      msql := msql || ' WHERE';
      msql := msql || ' DOF.docnum = ''' || pDOCNUM || '''';
      msql := msql || ' AND DOF.CPROG = ''' || pPROG || '''';
      msql := msql || ' AND IDF.YJ1BNFDOC_ID(+) = DOF.ID';
    
      If ltrim(pITMNUM, 0) is not null Then
        msql := msql || ' AND IDF.itmnum = ''' || pITMNUM || '''';
      End if;
    
      msql := msql || ' AND DOF.ID = DOFSUM.YJ1BNFDOC_ID';
    
      mcur := dbms_sql.open_cursor;
    
      dbms_sql.parse(mcur, msql, dbms_sql.v7);
      dbms_sql.define_column(mcur, 1, mid);
      dbms_sql.define_column(mcur, 2, mid_IDF);
      dbms_sql.define_column(mcur, 3, mDOF_TOT);
      dbms_sql.define_column_char(mcur, 4, mDOCREF, 10);
      dbms_sql.define_column(mcur, 5, mxml_id);
      mrows_processed := dbms_sql.execute(mcur);
      mfetch_rows     := dbms_sql.fetch_rows(mcur);
      dbms_sql.column_value(mcur, 1, mid);
      dbms_sql.column_value(mcur, 2, mid_IDF);
      dbms_sql.column_value(mcur, 3, mDOF_TOT);
      dbms_sql.column_value_char(mcur, 4, mDOCREF);
      dbms_sql.column_value(mcur, 5, mxml_id);
    
      If mid is not null Then
        mpfj_data     := GET_DOF_PFJ(mID, pmodulo);
        mPFJ_EMITENTE := mpfj_data.EMITENTE_PFJ_CODIGO;
      
        mPFJ_REMETENTE    := mpfj_data.REMETENTE_PFJ_CODIGO;
        mPFJ_DESTINATARIO := mpfj_data.DESTINATARIO_PFJ_CODIGO;
      
        mdof_data.ITF_ID := mid;
        mdof_data.xml_id := mxml_id;
        mdof_itf.id      := mid;
        midf_itf.id      := mid_IDF;
      
        sel_itf_dof(mdof_itf, pmodulo);
        sel_itf_idf(midf_itf, pmodulo);
      
        mdof_data.SERIE_SUBSERIE := nvl(mdof_itf.SERIESSUB,
                                        sap_itf_xml_util.sap_get_par_associacao('SAP_SERIES_DEFAULT_CATEGORIA_' ||
                                                                                pmodulo,
                                                                                mdof_itf.NFTYPE));
        mdof_data.NUMERO         := mdof_itf.NFNUM;
        mdof_data.EDOF_CODIGO    := sap_itf_xml_util.sap_get_par_associacao('SAP_ESPECIE_NF',
                                                                            ltrim(rtrim(mdof_itf.NFTYPE)));
      
        mdof_data.DH_EMISSAO             := mdof_itf.DOCDAT;
        mdof_data.DOF_IMPORT_NUMERO      := mDOCREF;
        mdof_data.EMITENTE_PFJ_CODIGO    := MPFJ_EMITENTE;
        mdof_data.VL_TOTAL_CONTABIL      := mDOF_TOT;
        mdof_data.IDF_TEXTO_COMPLEMENTAR := midf_itf.MAKTX;
      
        If ltrim(mdof_data.NUMERO, 0) is null and pmodulo <> 'CIAP' Then
          mdof_data := empty_dof_data;
        End if;
      
        mdof_data.NOP_CODIGO := ltrim(rtrim(midf_itf.CFOP));
      
        If mdof_itf.DIRECT = '1' Then
          mDT_REFERENCIA := mdof_itf.PSTDAT;
          mUF_REFERENCIA := cor_uf_pessoa(mPFJ_DESTINATARIO,
                                          null,
                                          null,
                                          'N');
        Elsif mdof_itf.DIRECT = '2' Then
          mDT_REFERENCIA := mdof_itf.DOCDAT;
          mUF_REFERENCIA := cor_uf_pessoa(mPFJ_REMETENTE, null, null, 'N');
        End if;
      
        mdof_data.CFOP_CODIGO := cor_calc_cfop(ltrim(rtrim(midf_itf.CFOP)),
                                               COR_CALC_EIX_UF(cor_uf_pessoa(mPFJ_REMETENTE,
                                                                             null,
                                                                             mDT_REFERENCIA,
                                                                             'N'),
                                                               cor_uf_pessoa(mPFJ_DESTINATARIO,
                                                                             null,
                                                                             mDT_REFERENCIA,
                                                                             'N')),
                                               mUF_REFERENCIA,
                                               mdof_itf.PSTDAT);
      
        mdof_data.IDF_TEXTO_COMPLEMENTAR := midf_itf.MAKTX;
      
        DEL_REG_DOF(mid, mid_IDF, mdof_itf.XML_ID, pmodulo, pProg, pDOCNUM);
      
      End if;
      dbms_sql.close_cursor(mcur);
      return mdof_data;
    End if;
  exception
    when too_many_rows then
      syn_dynamic.close_cursor(mcur);
    
      raise_application_error(-20001,
                              'Chave origem :''' || pdof ||
                              ''' , duplicada em ' || pmodulo || '_DOF .');
    when others Then
      syn_dynamic.close_cursor(mcur);
    
      return empty_dof_data;
  end get_dof_info;

  function get_dof_info(pdof in varchar2, pinformante varchar2)
    return dof_in_values is
    mIN_data       INAPI_DOF.data;
    mIN_clear      INAPI_DOF.data;
    mdof_in_data   dof_in_values;
    empty_dof_data dof_in_values;
    mId            number;
  Begin
    Select id into mid from in_dof where chave_origem = pdof;
  
    mIN_data.v.id := mid;
    INAPI_DOF.sel(mIN_data);
  
    mdof_in_data.IEST_ID          := mIN_Data.v.IEST_ID;
    mdof_in_data.E_S              := mIN_Data.v.E_S;
    mdof_in_data.SERIE_SUBSERIE   := mIN_Data.v.SERIE_SUBSERIE;
    mdof_in_data.ANO_MES_EMISSAO  := mIN_Data.v.ANO_MES_EMISSAO;
    mdof_in_data.NUMERO           := mIN_Data.v.NUMERO;
    mdof_in_data.IESP_ID          := mIN_Data.v.IESP_ID;
    mdof_in_data.IPFJ_ID_EMITENTE := mIN_Data.v.IPFJ_ID_EMITENTE;
    mdof_in_data.DT_EMISSAO       := mIN_Data.v.DT_EMISSAO;
  
    return mdof_in_data;
  exception
    when no_data_found then
      raise_application_error(-20001,
                              'Dof ' || pdof || ' nao encontrado em IN_DOF');
    when too_many_rows then
      raise_application_error(-20001,
                              'Chave origem :''' || pdof ||
                              ''' , duplicada em IN_DOF .');
    when others Then
      return empty_dof_data;
  End;

  function GET_DOF_PFJ(pID_DOF number, pmodulo varchar2) return dof_pfj is
  
    memitido      varchar2(5) := 'N';
    mfuncao       varchar2(10) := null;
    mPARID        varchar2(200);
    mPARID_alt    varchar2(200);
    mdof_itf      dof_itf;
    mdof_pfj_data dof_pfj;
    mITFCOR_data  dof_itf;
    mIN_data      CORAPI_DOF.data;
    mCOR_data     CORAPI_DOF.data;
    mfunpar_itf   funpar_itf;
    empty_mfunpar funpar_itf;
  
    mOTC varchar2(2000) := null;
  
    mEMPRESA_FILIAL     varchar2(255) := null;
    msufixo_estab       varchar2(2) := null;
    msufixo_estab_parid varchar2(2) := null;
  
    function get_parceiro(pID_DOF number,
                          pfuncao varchar2,
                          pmodulo sap_itf_appl_synchro.appl_synchro%type,
                          potc    VARCHAR2) return funpar_itf is
      msql            VARCHAR2(1000);
      mcur            PLS_INTEGER;
      mrows_processed PLS_INTEGER;
      mcount          PLS_INTEGER;
      mfetch_rows     PLS_INTEGER;
      mfunpar         funpar_itf;
      mfunpar_clear   funpar_itf;
    Begin
      msql := 'SELECT';
      msql := msql || ' funpar.parvw,';
      IF nvl(sap_itf_xml_util.get_par_val('SAP_ITF_SUPRIME_ZEROS_PFJ'), 'S') = 'S' THEN
        msql := msql || ' ltrim ( funpar.parid, 0 ) parid,';
      ELSE
        msql := msql || ' funpar.parid parid,';
      END IF;
    
      msql := msql || ' partyp';
      msql := msql || ' FROM';
      msql := msql || ' SAP_ITF_FUN_PARCEIRO_' || pmodulo || ' FUNPAR';
      msql := msql || ' WHERE';
      msql := msql || ' FUNPAR.yj1bnfdoc_id = :p1';
      msql := msql || ' and FUNPAR.parvw = :p2';
    
      If potc = 'X' then
        msql := msql || ' and FUNPAR.xcpdk is null';
      End if;
    
      mcur := dbms_sql.open_cursor;
      dbms_sql.parse(mcur, msql, dbms_sql.v7);
    
      dbms_sql.bind_variable(mcur, 'p1', pID_DOF);
      dbms_sql.bind_variable(mcur, 'p2', pfuncao);
    
      dbms_sql.define_column_char(mcur, 1, mfunpar.parvw, 2);
      dbms_sql.define_column_char(mcur, 2, mfunpar.parid, 30);
      dbms_sql.define_column_char(mcur, 3, mfunpar.partyp, 1);
      mrows_processed := dbms_sql.execute(mcur);
      mfetch_rows     := dbms_sql.fetch_rows(mcur);
      dbms_sql.column_value_char(mcur, 1, mfunpar.parvw);
      dbms_sql.column_value_char(mcur, 2, mfunpar.parid);
      dbms_sql.column_value_char(mcur, 3, mfunpar.partyp);
      dbms_sql.close_cursor(mcur);
    
      mfunpar.parid := ltrim(rtrim(mfunpar.parid));
    
      return mfunpar;
    
    Exception
      when others Then
        syn_dynamic.close_cursor(mcur);
      
        raise;
    End;
  
    function get_otc return cor_pessoa.pfj_codigo%type is
      mrefkey         cor_pessoa.pfj_codigo%type;
      msql            VARCHAR2(1000);
      mcur            PLS_INTEGER;
      mrows_processed PLS_INTEGER;
      mcount          PLS_INTEGER;
      mfetch_rows     PLS_INTEGER;
    begin
    
      msql := 'SELECT';
      msql := msql || ' refkey';
      msql := msql || ' FROM';
      msql := msql || '     SAP_ITF_IDF_' || pmodulo;
      msql := msql || ' WHERE';
      msql := msql || '     yj1bnfdoc_id = ' || pID_DOF || '';
      msql := msql || '     and rownum = 1';
      msql := msql || ' ORDER by itmnum';
    
      mcur := dbms_sql.open_cursor;
    
      dbms_sql.parse(mcur, msql, dbms_sql.v7);
      dbms_sql.define_column_char(mcur, 1, mrefkey, 200);
      mrows_processed := dbms_sql.execute(mcur);
      mfetch_rows     := dbms_sql.fetch_rows(mcur);
      dbms_sql.column_value_char(mcur, 1, mrefkey);
      dbms_sql.close_cursor(mcur);
    
      If ltrim(rtrim(mrefkey)) is null Then
        mrefkey := mdof_itf.docnum;
      End if;
    
      If sap_itf_xml_util.get_par_val('SAP_ITF_SUPRIME_ZEROS_PFJ') = 'S' then
        mrefkey := ltrim(mrefkey, 0);
      End if;
    
      return ltrim(rtrim(mrefkey));
    exception
      when no_data_found then
        syn_dynamic.close_cursor(mcur);
      
        raise_application_error(-20001,
                                'Deducao de OTC incorreta - GET_DOF_PFJ');
      when others then
        syn_dynamic.close_cursor(mcur);
        raise;
    end;
  
  BEGIN
  
    set_sufixo_estab('E');
    set_sufixo_estab('PARID', 'E');
  
    mdof_itf.id := pID_DOF;
    sel_itf_dof(mdof_itf, pmodulo);
    memitido := sap_itf_xml_util.sap_get_par_associacao('SAP_ESPECIE_NF_CLASSIFICACAO_' ||
                                                        pmodulo,
                                                        mdof_itf.NFTYPE);
  
    if sap_itf_xml_util.get_par_val('SAP_ITF_SUPRIME_ZEROS_PFJ') = 'S' then
      mdof_itf.PARID := ltrim(mdof_itf.PARID, 0);
    End if;
  
    If mdof_itf.PARXCPDK = 'X' Then
      mPARID := get_otc;
      mPARID := 'O' || mPARID;
    Elsif mdof_itf.PARTYP = 'B' Then
      mPARID     := rtrim(ltrim(mdof_itf.PARID));
      mPARID_alt := mPARID;
    
      mPARID := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                pmodulo,
                                                                ltrim(mdof_itf.PARID,
                                                                      0)),
                        sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                pmodulo,
                                                                ltrim(mdof_itf.PARID,
                                                                      0) || 'E')),
                    ltrim(mdof_itf.PARID, 0) || 'E');
    
      -- BEGIN CUSTOM - 30/12/2016 - AMB - Reposicionamentp de custom
      -- BEGIN CUSTOM - 16/05/2015 - GDD - Reposicionado custom
      -- BEGIN CUSTOM - 02/12/2009 - ACS - ANTONIO CARLOS SCARASSATI
      -- N�o eliminar os zeros � esquerda do parid quando estabelecimento, mesmo tratamento realizado para estabelecimento
      --    mPARID
      --       :=  nvl(  nvl ( sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_'||pmodulo, ltrim (mdof_itf.PARID,0) )
      --                     , sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_'||pmodulo, ltrim (mdof_itf.PARID,0) || 'E' )
      --                     )
      --           , ltrim (mdof_itf.PARID,0) || 'E');
      mPARID := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' || pmodulo, mdof_itf.PARID),
                        sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' || pmodulo, mdof_itf.PARID || 'E')),
                        mdof_itf.PARID || 'E');
      -- END CUSTOM - 02/12/2009 - ACS - ANTONIO CARLOS SCARASSATI
      -- END CUSTOM - 16/05/2015 - GDD - Reposicionado custom
      -- END CUSTOM - 30/12/2016 - AMB - Reposicionamentp de custom

           -- END   ATL 26/02/2003 - verifica EQUIVALENCIA de ESTABELECIMENTOS
          -- <<<<< CBT 10/02/09 ocor 1590871 nao esta respeitando a equivalencia SAP_ESTAB_COR
	
      If mPARID is not null Then
        set_sufixo_estab('PARID', null);
      End if;
    
      IF mPARID IS NULL AND instr(mPARID_alt, '_') <= 4 AND
         LENGTH(mPARID_alt) <= 7 THEN
        mPARID     := mPARID_alt;
        mPARID_alt := replace(mPARID_alt, '_', '');
        IF mPARID_alt <> mPARID THEN
          mPARID := mPARID_alt;
        END IF;
        set_sufixo_estab('PARID', 'E');
      END IF;
    End if;
  
    mdof_itf.PARID          := rtrim(ltrim(mdof_itf.PARID));
    mdof_itf.EMPRESA_FILIAL := rtrim(ltrim(mdof_itf.EMPRESA_FILIAL));
  
    mEMPRESA_FILIAL := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                       pmodulo,
                                                                       mdof_itf.EMPRESA_FILIAL),
                               sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                       pmodulo,
                                                                       mdof_itf.EMPRESA_FILIAL || 'E')),
                           ltrim(mdof_itf.EMPRESA_FILIAL, 0) || 'E');
    If mEMPRESA_FILIAL is not null Then
      set_sufixo_estab(null);
    End if;
  
    msufixo_estab := get_sufixo_estab;
    If mdof_itf.PARTYP = 'B' Then
      msufixo_estab_parid := get_sufixo_estab('PARID');
    End if;
  
    mEMPRESA_FILIAL := nvl(mEMPRESA_FILIAL, mdof_itf.EMPRESA_FILIAL) ||
                       msufixo_estab;
    mPARID          := nvl(mPARID, mdof_itf.PARID) || msufixo_estab_parid;
  
    If memitido = 'E' Then
      mdof_pfj_data.EMITENTE_PFJ_CODIGO := mEMPRESA_FILIAL;
    Else
    
      If mdof_itf.DIRECT = '1' Then
        If mdof_itf.PARTYP = 'V' Then
          mdof_pfj_data.EMITENTE_PFJ_CODIGO := mPARID || 'F';
        Elsif mdof_itf.PARTYP = 'C' Then
          mdof_pfj_data.EMITENTE_PFJ_CODIGO := mPARID || 'C';
        Elsif mdof_itf.PARTYP = 'B' Then
          mdof_pfj_data.EMITENTE_PFJ_CODIGO := mPARID;
        End if;
      Elsif mdof_itf.DIRECT = '2' Then
        mdof_pfj_data.EMITENTE_PFJ_CODIGO := mEMPRESA_FILIAL;
      End if;
    End if;
  
    If mdof_itf.DIRECT = '1' Then
      mdof_pfj_data.DESTINATARIO_PFJ_CODIGO := mEMPRESA_FILIAL;
    Elsif mdof_itf.DIRECT = '2' Then
      If mdof_itf.PARTYP = 'V' Then
        mdof_pfj_data.DESTINATARIO_PFJ_CODIGO := mPARID || 'F';
      Elsif mdof_itf.PARTYP = 'C' Then
        mdof_pfj_data.DESTINATARIO_PFJ_CODIGO := mPARID || 'C';
      Elsif mdof_itf.PARTYP = 'B' Then
        mdof_pfj_data.DESTINATARIO_PFJ_CODIGO := mPARID;
      End if;
    End if;
  
    mfunpar_itf := empty_mfunpar;
    mfuncao     := sap_itf_xml_util.get_sap_code('SAP_PFJ_TIPO_' || pmodulo,
                                                 'TR');
    mfunpar_itf := get_parceiro(pID_DOF,
                                mfuncao,
                                pmodulo,
                                mdof_itf.PARXCPDK);
  
    If mfunpar_itf.parid is not null Then
      mfunpar_itf.parid := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid,
                                                                           'S'),
                                   sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid || 'E',
                                                                           'S')),
                               ltrim(mfunpar_itf.parid, 0) || 'E');
    
      mdof_pfj_data.TRANSPORTADOR_PFJ_CODIGO := mfunpar_itf.parid || 'F';
    
    Else
      mdof_pfj_data.TRANSPORTADOR_PFJ_CODIGO := null;
    End if;
  
    mfunpar_itf := empty_mfunpar;
    mfuncao     := sap_itf_xml_util.get_sap_code('SAP_PFJ_TIPO_' || pmodulo,
                                                 'RM');
    mfunpar_itf := get_parceiro(pID_DOF,
                                mfuncao,
                                pmodulo,
                                mdof_itf.PARXCPDK);
  
    If mfunpar_itf.parid is not null Then
      mfunpar_itf.parid := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid,
                                                                           'S'),
                                   sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid || 'E',
                                                                           'S')),
                               ltrim(mfunpar_itf.parid, 0) || 'E');
      If mfunpar_itf.PARTYP = 'V' Then
        mdof_pfj_data.REMETENTE_PFJ_CODIGO := mfunpar_itf.PARID || 'F';
      Elsif mfunpar_itf.PARTYP = 'C' Then
        mdof_pfj_data.REMETENTE_PFJ_CODIGO := mfunpar_itf.PARID || 'C';
      Elsif mfunpar_itf.PARTYP = 'B' Then
        mdof_pfj_data.REMETENTE_PFJ_CODIGO := mPARID;
      End if;
    Else
      If mdof_itf.DIRECT = '1' Then
        If mdof_itf.PARTYP = 'V' Then
          mdof_pfj_data.REMETENTE_PFJ_CODIGO := mPARID || 'F';
        Elsif mdof_itf.PARTYP = 'C' Then
          mdof_pfj_data.REMETENTE_PFJ_CODIGO := mPARID || 'C';
        Elsif mdof_itf.PARTYP = 'B' Then
          mdof_pfj_data.REMETENTE_PFJ_CODIGO := mPARID;
        End if;
      Elsif mdof_itf.DIRECT = '2' Then
        mdof_pfj_data.REMETENTE_PFJ_CODIGO := mEMPRESA_FILIAL;
      End if;
    End if;
  
    mfunpar_itf := empty_mfunpar;
    mfuncao     := sap_itf_xml_util.get_sap_code('SAP_PFJ_TIPO_' || pmodulo,
                                                 'ET');
    mfunpar_itf := get_parceiro(pID_DOF,
                                mfuncao,
                                pmodulo,
                                mdof_itf.PARXCPDK);
  
    If mfunpar_itf.parid is not null Then
      mfunpar_itf.parid := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid,
                                                                           'S'),
                                   sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid || 'E',
                                                                           'S')),
                               ltrim(mfunpar_itf.parid, 0) || 'E');
    
      If mfunpar_itf.PARTYP = 'V' Then
        mdof_pfj_data.ENTREGA_PFJ_CODIGO := mfunpar_itf.PARID || 'F';
      Elsif mfunpar_itf.PARTYP = 'C' Then
        mdof_pfj_data.ENTREGA_PFJ_CODIGO := mfunpar_itf.PARID || 'C';
      Elsif mfunpar_itf.PARTYP = 'B' Then
      
        mdof_pfj_data.ENTREGA_PFJ_CODIGO := mfunpar_itf.PARID ||
                                            msufixo_estab;
      End if;
    Else
      If mdof_itf.DIRECT = '1' Then
        mdof_pfj_data.ENTREGA_PFJ_CODIGO := mEMPRESA_FILIAL;
      Elsif mdof_itf.DIRECT = '2' Then
        If mdof_itf.PARTYP = 'V' Then
          mdof_pfj_data.ENTREGA_PFJ_CODIGO := mPARID || 'F';
        Elsif mdof_itf.PARTYP = 'C' Then
          mdof_pfj_data.ENTREGA_PFJ_CODIGO := mPARID || 'C';
        Elsif mdof_itf.PARTYP = 'B' Then
          mdof_pfj_data.ENTREGA_PFJ_CODIGO := mPARID;
        End if;
      End if;
    End if;
  
    mfunpar_itf := empty_mfunpar;
    mfuncao     := sap_itf_xml_util.get_sap_code('SAP_PFJ_TIPO_' || pmodulo,
                                                 'RT');
    mfunpar_itf := get_parceiro(pID_DOF,
                                mfuncao,
                                pmodulo,
                                mdof_itf.PARXCPDK);
  
    If mfunpar_itf.parid is not null Then
    
      mfunpar_itf.parid := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid,
                                                                           'S'),
                                   sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid || 'E',
                                                                           'S')),
                               ltrim(mfunpar_itf.parid, 0) || 'E');
    
      If mfunpar_itf.PARTYP = 'V' Then
        mdof_pfj_data.RETIRADA_PFJ_CODIGO := mfunpar_itf.PARID || 'F';
      Elsif mfunpar_itf.PARTYP = 'C' Then
        mdof_pfj_data.RETIRADA_PFJ_CODIGO := mfunpar_itf.PARID || 'C';
      Elsif mfunpar_itf.PARTYP = 'B' Then
        mdof_pfj_data.RETIRADA_PFJ_CODIGO := mfunpar_itf.PARID ||
                                             msufixo_estab;
      End if;
    Else
      If mdof_itf.DIRECT = '1' Then
        If mdof_itf.PARTYP = 'V' Then
          mdof_pfj_data.RETIRADA_PFJ_CODIGO := mPARID || 'F';
        Elsif mdof_itf.PARTYP = 'C' Then
          mdof_pfj_data.RETIRADA_PFJ_CODIGO := mPARID || 'C';
        Elsif mdof_itf.PARTYP = 'B' Then
          mdof_pfj_data.RETIRADA_PFJ_CODIGO := mPARID;
        End if;
      Elsif mdof_itf.DIRECT = '2' Then
        mdof_pfj_data.RETIRADA_PFJ_CODIGO := mEMPRESA_FILIAL;
      End if;
    End if;
  
    mfunpar_itf := empty_mfunpar;
    mfuncao     := sap_itf_xml_util.get_sap_code('SAP_PFJ_TIPO_' || pmodulo,
                                                 'PG');
    mfunpar_itf := get_parceiro(pID_DOF,
                                mfuncao,
                                pmodulo,
                                mdof_itf.PARXCPDK);
  
    If mfunpar_itf.parid is not null Then
      mfunpar_itf.parid := nvl(nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid,
                                                                           'S'),
                                   sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_' ||
                                                                           pmodulo,
                                                                           mfunpar_itf.parid || 'E',
                                                                           'S')),
                               ltrim(mfunpar_itf.parid, 0) || 'E');
    
      If mfunpar_itf.PARTYP = 'V' Then
        mdof_pfj_data.COBRANCA_PFJ_CODIGO := mfunpar_itf.PARID || 'F';
      Elsif mfunpar_itf.PARTYP = 'C' Then
        mdof_pfj_data.COBRANCA_PFJ_CODIGO := mfunpar_itf.PARID || 'C';
      Elsif mfunpar_itf.PARTYP = 'B' Then
        mdof_pfj_data.COBRANCA_PFJ_CODIGO := mfunpar_itf.PARID ||
                                             msufixo_estab;
      End if;
    
    Else
    
      If mdof_itf.DIRECT = '1' Then
        If mdof_itf.PARTYP = 'V' Then
          mdof_pfj_data.COBRANCA_PFJ_CODIGO := mPARID || 'F';
        Elsif mdof_itf.PARTYP = 'C' Then
          mdof_pfj_data.COBRANCA_PFJ_CODIGO := mPARID || 'C';
        Elsif mdof_itf.PARTYP = 'B' Then
          mdof_pfj_data.COBRANCA_PFJ_CODIGO := mPARID;
        End if;
      Elsif mdof_itf.DIRECT = '2' Then
        mdof_pfj_data.COBRANCA_PFJ_CODIGO := mEMPRESA_FILIAL;
      End if;
    End if;
  
    return mdof_pfj_data;
  
  END GET_DOF_PFJ;

  function GET_IDF_ID(pmodulo         varchar2,
                      pCodigo_do_Site cor_dof.codigo_do_site%type := NULL,
                      pDof_Sequence   cor_dof.dof_sequence%type := NULL,
                      pIdf_Num        cor_idf.idf_num%type := NULL)
    return COR_IDF.ID%type is
    mreturn COR_IDF.ID%type;
  Begin
    If pModulo = 'COR' Then
      mreturn := COR_DOF_IDF_ID(pCodigo_do_Site, pDof_Sequence, pIdf_Num);
    Elsif pModulo = 'IN' Then
      Select in_seq_iidf.nextval into mreturn from dual;
    End if;
    return mreturn;
  exception
    when others then
      raise;
  end GET_IDF_ID;

  function get_idf_impostos(pIDF in number, pmodulo varchar2)
    return CORAPI_IDF.data is
    mCOR_data CORAPI_IDF.data;
    mIN_data  INAPI_IDF.data;
  
    mCOR_clear CORAPI_IDF.data;
  
    msql            varchar2(1000);
    mcur            PLS_INTEGER;
    mrows_processed PLS_INTEGER;
    mcount          PLS_INTEGER;
    type tp_impostos is record(
      taxtyp          varchar2(20),
      base_plena      number(19, 2),
      rate            number(7, 4),
      taxval          number(19, 2),
      excbas          number(19, 2),
      base_tributavel number(19, 2),
      othbas          number(19, 2),
      qtd_base        number(19, 2));
    mimpostos      tp_impostos;
    mtipo          varchar2(20);
    v_tipo_imposto varchar2(20);
    v_tipo_stf     cor_idf.tipo_stf%type;
    old_v_tipo_stf cor_idf.tipo_stf%type := 'N';
  
  Begin
      -- ***********************************************************
    -- Begin Custom CBT 09/06/16 Tratamento Impostos DIFA NT 2015-03
    -- Quando existir nos impostos do item do tipo "ICMS", um imposto cujo taxtyp = "ZIDO"
    -- dever�o ser desconsiderados todos os demais impostos associados ao tipo ICMS
    begin
      mtipo := null;
      begin
        select taxtyp
          into mtipo
          from sap_itf_impostos_idf_cor
         where yj1bnflin_id = pidf
           and sap_itf_xml_util.sap_get_par_associacao('SAP_TIPO_IMPOSTO_COR',
                                                       taxtyp) = 'ICMS'
           and taxtyp = 'ZIDO'
           and rownum <= 1;
      exception
        when no_data_found then
          mtipo := null;
        when others then
          raise;
      end;

      if mtipo is not null then

        delete from sap_itf_impostos_idf_cor
         where yj1bnflin_id = pidf
           and sap_itf_xml_util.sap_get_par_associacao('SAP_TIPO_IMPOSTO_COR',
                                                       taxtyp) = 'ICMS'
           and taxtyp <> 'ZIDO';
      end if;

    exception
      when others then
        raise;
    end;

    -- End Custom CBT 09/06/16 Tratamento Impostos DIFA NT 2015-03
    -- ***********************************************************
  
    msql := 'SELECT';
    msql := msql ||
            '   SAP_ITF_XML_UTIL.sap_get_par_associacao (''SAP_TIPO_IMPOSTO_' ||
            pmodulo || ''', taxtyp) taxtyp';
    msql := msql ||
            ', max(nvl(base,0))+ max(nvl(othbas,0)) + max(nvl(excbas,0)) base_plena';
    msql := msql || ' , max(rate) rate';
    msql := msql || ' , SUM(taxval) taxval';
    msql := msql || ' , max(nvl(excbas,0)) excbas';
    msql := msql || ' , max(nvl(base,0)) base_tributavel';
    msql := msql || ' , max(nvl(othbas,0)) othbas';
    msql := msql || ' , max(nvl(qtd_base,0)) qtd_base';
    msql := msql || ' FROM SAP_ITF_IMPOSTOS_IDF_' || pmodulo;
    msql := msql || ' where YJ1BNFLIN_ID = ' || pIDF;
    msql := msql ||
            ' group by SAP_ITF_XML_UTIL.sap_get_par_associacao (''SAP_TIPO_IMPOSTO_' ||
            pmodulo || ''', taxtyp)';
    msql := msql || ' ORDER BY 1';
  
    mcur := dbms_sql.open_cursor;
  
    dbms_sql.parse(mcur, msql, dbms_sql.v7);
    dbms_sql.define_column_char(mcur, 1, mimpostos.taxtyp, 20);
    dbms_sql.define_column(mcur, 2, mimpostos.base_plena);
    dbms_sql.define_column(mcur, 3, mimpostos.rate);
    dbms_sql.define_column(mcur, 4, mimpostos.taxval);
    dbms_sql.define_column(mcur, 5, mimpostos.excbas);
    dbms_sql.define_column(mcur, 6, mimpostos.base_tributavel);
    dbms_sql.define_column(mcur, 7, mimpostos.othbas);
    dbms_sql.define_column(mcur, 8, mimpostos.qtd_base);
  
    mrows_processed := dbms_sql.execute(mcur);
  
    LOOP
      EXIT WHEN dbms_sql.fetch_rows(mcur) <= 0;
      dbms_sql.column_value_char(mcur, 1, mtipo);
      mtipo := rtrim(mtipo);
    
      v_tipo_stf := '';
    
      If mtipo is null then
        IF PMODULO = 'COR' THEN
          BEGIN
            select taxtyp
              into v_tipo_imposto
              from sap_itf_impostos_idf_cor
             where yj1bnflin_id = pidf
               and sap_itf_xml_util.sap_get_par_associacao('SAP_TIPO_IMPOSTO_COR',
                                                           taxtyp) is null
               and rownum = 1;
            raise_application_error(-20222,
                                    'Imposto ' || v_tipo_imposto ||
                                    ' nao parametrizado para o item.');
          
          EXCEPTION
            when others then
              raise;
              null;
          END;
        END IF;
        IF PMODULO = 'IN' THEN
          BEGIN
            select taxtyp
              into v_tipo_imposto
              from sap_itf_impostos_idf_in
             where yj1bnflin_id = pidf
               and sap_itf_xml_util.sap_get_par_associacao('SAP_TIPO_IMPOSTO_IN',
                                                           taxtyp) is null
               and rownum = 1;
            raise_application_error(-20223,
                                    'Imposto ' || v_tipo_imposto ||
                                    ' nao parametrizado para o item.');
          
          EXCEPTION
            when others then
              raise;
              null;
          END;
        END IF;
      End If;
    
      If pmodulo = 'IN' Then
        If mtipo = 'ICMS' Then
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ICMS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ICMS);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_ICMS);
          dbms_sql.column_value(mcur, 5, mCOR_data.v.VL_ISENTO_ICMS);
          dbms_sql.column_value(mcur, 7, mCOR_data.v.VL_OUTROS_ICMS);
        Elsif mtipo = 'IPI' Then
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_IPI);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_IPI);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_IPI);
          dbms_sql.column_value(mcur, 5, mCOR_data.v.VL_ISENTO_IPI);
          dbms_sql.column_value(mcur, 7, mCOR_data.v.VL_OUTROS_IPI);
        Elsif mtipo = 'STT' Then
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_STT);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STT);
        Elsif mtipo = 'STF' Then
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_STF);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STF);
        Elsif mtipo = 'DIFA' Then
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_DIFA);
        Elsif mtipo = 'ISS' Then
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ISS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ISS);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_ISS);
        Elsif mtipo = 'II' Then
          dbms_sql.column_value(mcur, 3, mCOR_data.v.VL_II);
        End if;
      
      Elsif pmodulo = 'COR' Then
        If mtipo = 'ICMS' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_ICMS);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_ICMS);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ICMS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ICMS);
          dbms_sql.column_value(mcur, 5, mCOR_data.v.VL_ISENTO_ICMS);
          dbms_sql.column_value(mcur, 7, mCOR_data.v.VL_OUTROS_ICMS);
        Elsif mtipo = 'ICMS_ZF' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_ICMS_DESC_L);
          dbms_sql.column_value(mcur,
                                6,
                                mCOR_data.v.VL_TRIBUTAVEL_ICMS_DESC_L);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ICMS_DESC_L);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ICMS_DESC_L);
        Elsif mtipo = 'IPI' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_IPI);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_IPI);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_IPI);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_IPI);
          dbms_sql.column_value(mcur, 5, mCOR_data.v.VL_ISENTO_IPI);
          dbms_sql.column_value(mcur, 7, mCOR_data.v.VL_OUTROS_IPI);
        Elsif mtipo = 'STT' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_STT);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STT);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_STT);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_STT);
          mCOR_data.v.VL_STT := abs(mCOR_data.v.VL_STT);
        Elsif mtipo = 'STF' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_STF);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STF);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_STF);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_STF);
          mCOR_data.v.TIPO_STF := 'N';
          v_tipo_stf           := 'N';
        Elsif mtipo = 'STF_SUBSTITUTO' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_STF);
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STF);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_STF);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_STF);
          mCOR_data.v.TIPO_STF := 'T';
          v_tipo_stf           := 'T';
        Elsif mtipo = 'STF_FRONTEIRA' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STF);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_STF);
          dbms_sql.column_value(mcur, 2, mCOR_data.v.vl_base_stf_fronteira);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_stf_fronteira);
          mCOR_data.v.TIPO_STF := 'F';
          v_tipo_stf           := 'F';
        Elsif mtipo = 'STF_SUBSTITUIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_STF);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_STF);
          dbms_sql.column_value(mcur, 2, mCOR_data.v.vl_base_stf_ido);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_stf_ido);
          mCOR_data.v.TIPO_STF := 'I';
          v_tipo_stf           := 'I';
        Elsif mtipo = 'ICMS_PART_REM' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_icms_part_rem);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_icms_part_rem);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_icms_part_rem);
        Elsif mtipo = 'ICMS_PART_DEST' Then
          dbms_sql.column_value(mcur,
                                6,
                                mCOR_data.v.vl_base_icms_part_dest);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_icms_part_dest);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_icms_part_dest);
        Elsif mtipo = 'ICMS_FCP' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_icms_fcp);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_icms_fcp);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_icms_fcp);
        Elsif mtipo = 'ICMS_FCP_ST' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_ICMS_FCPST);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ICMS_FCPST);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ICMS_FCPST);
        Elsif mtipo = 'DIFA' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_TRIBUTAVEL_DIFA);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_DIFA);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_DIFA);
        Elsif mtipo = 'ISS' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_ISS);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ISS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ISS);
          mCOR_data.v.ind_iss_retido_fonte := 'N';
        Elsif mtipo = 'ISS_RETIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_ISS);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_ISS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_ISS);
          mCOR_data.v.ind_iss_retido_fonte := 'S';
        Elsif mtipo = 'IRRF' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_IRRF);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.PERC_IRRF);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_IRRF);
        Elsif mtipo = 'PIS' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_PIS);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.VL_ALIQ_PIS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_IMPOSTO_PIS);
          dbms_sql.column_value(mcur, 8, mCOR_data.v.QTD_BASE_PIS);
        Elsif mtipo = 'COFINS' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_COFINS);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.VL_ALIQ_COFINS);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_IMPOSTO_COFINS);
          dbms_sql.column_value(mcur, 8, mCOR_data.v.QTD_BASE_COFINS);
        Elsif mtipo = 'II' Then
          dbms_sql.column_value(mcur, 2, mCOR_data.v.VL_BASE_II);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_II);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_II);
        Elsif mtipo = 'SEST' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_sest);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_sest);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_sest);
        Elsif mtipo = 'SENAT' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_senat);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_senat);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_senat);
        Elsif mtipo = 'INSS' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_inss);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_inss);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_inss);
        Elsif mtipo = 'INSS_RETIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_inss_ret);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_inss_ret);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_inss_ret);
        Elsif mtipo = 'PIS_RETIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_pis_ret);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_pis_ret);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_pis_ret);
        Elsif mtipo = 'COFINS_RETIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_cofins_ret);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_cofins_ret);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_cofins_ret);
        Elsif mtipo = 'CSLL_RETIDO' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.vl_base_csll_ret);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.aliq_csll_ret);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.vl_csll_ret);
        Elsif mtipo = 'PIS_ST' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_PIS_ST);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_PIS_ST);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_PIS_ST);
        Elsif mtipo = 'COFINS_ST' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_BASE_COFINS_ST);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_COFINS_ST);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_COFINS_ST);
        Elsif mtipo = 'INSS_RETIDO_AE_15' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_SERVICO_AE15);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_INSS_AE15_RET);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_INSS_AE15_RET);
        Elsif mtipo = 'INSS_RETIDO_AE_20' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_SERVICO_AE20);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_INSS_AE20_RET);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_INSS_AE20_RET);
        Elsif mtipo = 'INSS_RETIDO_AE_25' Then
          dbms_sql.column_value(mcur, 6, mCOR_data.v.VL_SERVICO_AE25);
          dbms_sql.column_value(mcur, 3, mCOR_data.v.ALIQ_INSS_AE25_RET);
          dbms_sql.column_value(mcur, 4, mCOR_data.v.VL_INSS_AE25_RET);
        End if;
      End if;
    
      if v_tipo_stf in ('I', 'T', 'F') then
        if old_v_tipo_stf = 'N' Then
          old_v_tipo_stf := v_tipo_stf;
        else
          if old_v_tipo_stf <> v_tipo_stf Then
            raise_application_error(-20223,
                                    'Erro na parametrizacao do Imposto ' ||
                                    mtipo ||
                                    '. Não é permitido mais de um tipo de Imposto STF para o mesmo item.');
          end if;
        end if;
      else
        old_v_tipo_stf := 'N';
      end if;
    
    END LOOP;
    dbms_sql.close_cursor(mcur);
    return mCOR_data;
  exception
    when others then
      syn_dynamic.close_cursor(mcur);
      raise;
  end get_idf_impostos;

  Procedure DEL_REG_DOF(pID_DOF IN number,
                        pID_IDF IN number,
                        pXML_ID IN sap_itf_idoc_xml.id%type,
                        pModulo IN varchar2,
                        pProg   IN varchar2 := null,
                        pDOCNUM IN varchar2 := '0') IS
    msql            VARCHAR2(1000);
    mcur            INTEGER;
    mrows_processed PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
    mcount          PLS_INTEGER;
  
  BEGIN
  
    If ltrim(pProg) is Null or pProg = 'YSDMMPDOF' Then
    
      execute immediate 'DELETE FROM SAP_ITF_IDF_' || pModulo ||
                        ' WHERE ID = :p1'
        using pID_IDF;
    
      msql := 'SELECT';
      msql := msql || ' COUNT(id)';
      msql := msql || ' FROM';
      msql := msql || ' SAP_ITF_IDF_' || pmodulo;
      msql := msql || ' WHERE yj1bnfdoc_id = :p1';
    
      mcur := dbms_sql.open_cursor;
      dbms_sql.parse(mcur, msql, dbms_sql.v7);
    
      dbms_sql.bind_variable(mcur, 'p1', pID_DOF);
    
      dbms_sql.define_column(mcur, 1, mcount);
      mrows_processed := dbms_sql.execute(mcur);
      mfetch_rows     := dbms_sql.fetch_rows(mcur);
      dbms_sql.column_value(mcur, 1, mcount);
    
      IF mcount = 0 THEN
        SAP_ITF_XML_UTIL.apaga_registros_itf('SAP_ITF_IDF_' || pModulo,
                                             pID_IDF,
                                             pXML_ID,
                                             'YJ1BNFDOC_ID',
                                             'SAP_ITF_DOF_' || pModulo,
                                             pID_DOF);
      
        Begin
          Delete SAP_ITF_DOF_COR where DOCNUM = pDOCNUM;
        Exception
          when others then
            raise_application_error(-20001,
                                    'Erro na exclusão dos DOFs da SAP_ITF_DOF_COR - ' ||
                                    sqlerrm);
        End;
      
      END IF;
      dbms_sql.close_cursor(mcur);
    
    Else
    
      msql := 'SELECT';
      msql := msql || ' COUNT(id)';
      msql := msql || ' FROM';
      msql := msql || ' SAP_ITF_MOV_ESTOQUE_COR';
      msql := msql || ' WHERE docnum = :p1';
    
      mcur := dbms_sql.open_cursor;
      dbms_sql.parse(mcur, msql, dbms_sql.v7);
    
      dbms_sql.bind_variable(mcur, 'p1', pDOCNUM);
    
      dbms_sql.define_column(mcur, 1, mcount);
      mrows_processed := dbms_sql.execute(mcur);
      mfetch_rows     := dbms_sql.fetch_rows(mcur);
      dbms_sql.column_value(mcur, 1, mcount);
    
      IF mcount = 1 THEN
        SAP_ITF_XML_UTIL.apaga_registros_itf('SAP_ITF_DOF_' || pModulo,
                                             pID_DOF,
                                             pXML_ID);
      END IF;
      dbms_sql.close_cursor(mcur);
    End if;
  
  exception
    when no_data_found Then
      syn_dynamic.close_cursor(mcur);
    
      raise_application_error(-20001, 'DEL_REG_DOF' || sqlerrm);
    when others Then
      syn_dynamic.close_cursor(mcur);
    
      raise;
    
  END;

  Procedure SET_DOF_CANCEL(pDOF        number,
                           pInformante varchar2,
                           pModulo     varchar2,
                           pDocRef     varchar2,
                           pSituacao   varchar2) is
  
    mIN_data    INAPI_DOF.data;
    mIN_IDOFASS INAPI_IDOF_DOFASS.data;
    mCOR_data   CORAPI_DOF.data;
    mCOR_DOFASS CORAPI_DOF_ASSOCIADO.data;
    mdof_data   dof_values;
  
    mitf_id         number := null;
    msql            varchar2(2000) := null;
    mcur            INTEGER;
    mrows_processed PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
    mmessage        varchar2(2000) := null;
  
    Procedure delete_dofs(pModulo varchar2,
                          pDOF    varchar2,
                          pDocref varchar2,
                          mE_S    varchar2) is
    
      cursor c_cor_dofass(pcodigo_do_site COR_DOF.CODIGO_DO_SITE%type,
                          pdof_sequence   COR_DOF.DOF_SEQUENCE%type) is
        Select dof_assoc_sequence
          from cor_dof_associado
         where codigo_do_site = pcodigo_do_site
           and dof_sequence = pdof_sequence;
      m_cor_dofass c_cor_dofass%rowtype;
    
      cursor c_in_dofass(pIDOF_ID IN_DOF.ID%type) is
        Select id from in_idof_dofass where idof_id = pIDOF_ID;
      m_in_dofass c_in_dofass%rowtype;
    
    Begin
      If mE_S = 'E' Then
      
        If pmodulo = 'COR' and mCOR_data.v.INFORMANTE_EST_CODIGO <>
           mCOR_data.v.EMITENTE_PFJ_CODIGO Then
        
          open c_cor_dofass(mCOR_data.v.CODIGO_DO_SITE,
                            mCOR_data.v.DOF_SEQUENCE);
          loop
            fetch c_cor_dofass
              into m_cor_dofass;
            exit when c_cor_dofass%notfound;
          
            BEGIN
              mCOR_DOFASS                      := null;
              mCOR_DOFASS.v.CODIGO_DO_SITE     := mCOR_data.v.CODIGO_DO_SITE;
              mCOR_DOFASS.v.DOF_SEQUENCE       := mCOR_data.v.DOF_SEQUENCE;
              mCOR_DOFASS.v.DOF_ASSOC_SEQUENCE := m_cor_dofass.DOF_ASSOC_SEQUENCE;
              CORAPI_DOF_ASSOCIADO.del(mCOR_DOFASS);
            
            EXCEPTION
              WHEN CORAPI_DOF_ASSOCIADO.invalid_ref THEN
                raise_application_error(-20110,
                                        'Erro na exclus?o da linha da COR_DOF_ASSOCIADO de DOF_ASSOC_SEQUENCE =  ' ||
                                        mCOR_DOFASS.v.DOF_ASSOC_SEQUENCE);
              WHEN CORAPI_DOF_ASSOCIADO.element_locked THEN
                raise_application_error(-20120,
                                        'Erro na exclus?o da linha da COR_DOF_ASSOCIADO de DOF_ASSOC_SEQUENCE =  ' ||
                                        mCOR_DOFASS.v.DOF_ASSOC_SEQUENCE);
              WHEN CORAPI_DOF_ASSOCIADO.operation_failed THEN
                raise_application_error(-20130,
                                        'Erro na exclus?o da linha da COR_DOF_ASSOCIADO de DOF_ASSOC_SEQUENCE =  ' ||
                                        mCOR_DOFASS.v.DOF_ASSOC_SEQUENCE);
            END;
          
          end loop;
          close c_cor_dofass;
          BEGIN
            --cdc - 2370559 - 26/10/10
            --"ORA-04091: table SYNCHRO.COR_DOF is mutating, trigger/function may not see it"
            DELETE FROM COR_IDF
             WHERE CODIGO_DO_SITE = mCOR_data.v.CODIGO_DO_SITE
               AND DOF_SEQUENCE = mCOR_data.v.DOF_SEQUENCE;
            --cdc - 3021089 - 27/10/15
            --"delete installments too
            DELETE FROM COR_DOF_PARCELA
             WHERE CODIGO_DO_SITE = mCOR_data.v.CODIGO_DO_SITE
               AND DOF_SEQUENCE = mCOR_data.v.DOF_SEQUENCE;
          
            CORAPI_DOF.del(mCOR_data);
          
          EXCEPTION
            WHEN CORAPI_DOF.invalid_ref THEN
              raise_application_error(-20110,
                                      'Erro na exclus?o da linha da COR_DOF de ID =  ' ||
                                      mCOR_data.v.ID);
            WHEN CORAPI_DOF.element_locked THEN
              raise_application_error(-20120,
                                      'Erro na exclus?o da linha da COR_DOF de ID =  ' ||
                                      mCOR_data.v.ID);
            WHEN CORAPI_DOF.operation_failed THEN
              raise_application_error(-20130,
                                      'Erro na exclus?o da linha da COR_DOF de ID =  ' ||
                                      mCOR_data.v.ID);
          END;
        
        Elsif pmodulo = 'IN' and get_est_pfj(mIN_data.v.IEST_ID, 'IPFJ_ID') <>
              mIN_data.v.IPFJ_ID_EMITENTE Then
        
          open c_in_dofass(mIN_data.v.ID);
          loop
            fetch c_in_dofass
              into m_in_dofass;
            exit when c_in_dofass%notfound;
          
            BEGIN
              mIN_IDOFASS.v.ID := m_in_dofass.ID;
              INAPI_IDOF_DOFASS.del(mIN_IDOFASS);
            
            EXCEPTION
              WHEN INAPI_IDOF_DOFASS.invalid_ref THEN
                raise_application_error(-20110,
                                        'Erro na exclus?o da linha da in_idof_dofass de ID =  ' ||
                                        mIN_IDOFASS.v.ID);
              WHEN INAPI_IDOF_DOFASS.element_locked THEN
                raise_application_error(-20120,
                                        'Erro na exclus?o da linha da in_idof_dofass de ID =  ' ||
                                        mIN_IDOFASS.v.ID);
              WHEN INAPI_IDOF_DOFASS.operation_failed THEN
                raise_application_error(-20130,
                                        'Erro na exclus?o da linha da in_idof_dofass de ID =  ' ||
                                        mIN_IDOFASS.v.ID);
            END;
          
          end loop;
          close c_in_dofass;
        
          BEGIN
            INAPI_DOF.del(mIN_data);
          EXCEPTION
            WHEN INAPI_DOF.invalid_ref THEN
              raise_application_error(-20210,
                                      'Erro na exclus?o da Linha da in_idof de ID =  ' ||
                                      mIN_data.v.ID);
            WHEN INAPI_DOF.element_locked THEN
              raise_application_error(-20220,
                                      'Erro na exclus?o da Linha da in_idof  de ID =  ' ||
                                      mIN_data.v.ID);
            WHEN INAPI_DOF.operation_failed THEN
              raise_application_error(-20230,
                                      'Erro na exclus?o da Linha da in_idof  de ID =  ' ||
                                      mIN_data.v.ID);
          END;
        
        End if;
      End if;
    Exception
      when no_data_found Then
        If c_in_dofass%isopen Then
          close c_in_dofass;
        End if;
        If c_cor_dofass%isopen Then
          close c_cor_dofass;
        End if;
      when others Then
        If c_in_dofass%isopen Then
          close c_in_dofass;
        End if;
        If c_cor_dofass%isopen Then
          close c_cor_dofass;
        End if;
        raise;
    End;
  
    Procedure delete_itf(pModulo varchar2,
                         pDOF    number,
                         pDocref varchar2,
                         mE_S    varchar2) is
      mITF_DOF dof_itf;
    
    Begin
      If pmodulo = 'COR' Then
      
        NULL;
      
      Else
        mITF_DOF.id := pDof;
      
      End if;
    Exception
      when others Then
        syn_dynamic.close_cursor(mcur);
      
    End;
  
  BEGIN
    If pmodulo = 'COR' Then
    
      COR_IDF_CONS_PKG.disable;
      mdof_data                  := get_dof_info(pDocref,
                                                 pInformante,
                                                 pModulo);
      mCOR_data.v.DOF_SEQUENCE   := mdof_data.DOF_SEQUENCE;
      mCOR_data.v.CODIGO_DO_SITE := mdof_data.CODIGO_DO_SITE;
    
      If nvl(mdof_data.CTRL_SITUACAO_DOF, 'N') <> 'I' and
         nvl(mdof_data.CTRL_SITUACAO_DOF, 'N') <> 'D' then
        mCOR_data.i.CTRL_SITUACAO_DOF := true;
        mCOR_data.v.CTRL_SITUACAO_DOF := pSituacao;
      End if;
    
      BEGIN
        CORAPI_DOF.upd(mCOR_data);
      EXCEPTION
        WHEN CORAPI_DOF.invalid_ref THEN
          mmessage := 'dof_import_numero a cancelar nao existe em COR_DOF =  ' ||
                      pDocref;
        WHEN CORAPI_DOF.element_locked THEN
          raise_application_error(-20120,
                                  'Erro no update da linha da COR_DOF de ID =  ' ||
                                  mCOR_data.v.ID);
        WHEN CORAPI_DOF.operation_failed THEN
          raise_application_error(-20130,
                                  'Erro no update da linha da COR_DOF de ID =  ' ||
                                  mCOR_data.v.ID);
      END;
    
      If mmessage is null Then
        delete_dofs(pModulo, pDOF, pDocref, mCOR_data.v.IND_ENTRADA_SAIDA);
        delete_itf(pModulo, pDOF, pDocref, mCOR_data.v.IND_ENTRADA_SAIDA);
      End if;
    Elsif pmodulo = 'IN' Then
      mdof_data                      := get_dof_info(pDocref,
                                                     pInformante,
                                                     pModulo);
      mIN_data.v.ID                  := mdof_data.DOF_ID;
      mIN_data.i.IND_SITUACAO_CANCEL := true;
      mIN_data.v.IND_SITUACAO_CANCEL := 'S';
    
      BEGIN
        INAPI_DOF.upd(mIN_data);
      EXCEPTION
        WHEN INAPI_DOF.invalid_ref THEN
          raise_application_error(-20210,
                                  'Erro no update da Linha da in_idof  ID =  ' ||
                                  mIN_data.v.ID);
        WHEN INAPI_DOF.element_locked THEN
          raise_application_error(-20220,
                                  'Erro no update da Linha da in_idof  ID =  ' ||
                                  mIN_data.v.ID);
        WHEN INAPI_DOF.operation_failed THEN
          raise_application_error(-20230,
                                  'Erro no update da Linha da in_idof  ID =  ' ||
                                  mIN_data.v.ID);
      END;
      delete_dofs(pModulo, pDOF, pDocref, mIN_data.v.E_S);
      delete_itf(pModulo, pDOF, pDocref, mIN_data.v.E_S);
    End if;
  
    If pmodulo = 'COR' Then
      COR_IDF_CONS_PKG.enable;
    End if;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      null;
    WHEN OTHERS THEN
      If pmodulo = 'COR' Then
        COR_IDF_CONS_PKG.enable;
      End if;
      raise_application_error(-20001,
                              'Procedure SET_DOF_CANCEL : ' || SQLERRM);
  END SET_DOF_CANCEL;

  procedure set_dof_values(pdof number, pID_IDF NUMBER, pmodulo varchar2) is
    mCOR_data        CORAPI_DOF.data;
    mCOR_empty       CORAPI_DOF.data;
    mIN_data         INAPI_DOF.data;
    mreturn          INAPI_IDF.data;
    mreturnCOR       CORAPI_IDF.data;
    mreturnCOR_empty CORAPI_IDF.data;
  
    merror      varchar2(2000);
    merror_code integer;
  
    function first_item(pID_DOF   in_dof.ID%type,
                        pNUM_ITEM COR_IDF.IDF_NUM%type) return boolean is
      mNUM_ITEM COR_IDF.IDF_NUM%type;
    Begin
      Select idf_num
        into mNUM_ITEM
        from cor_idf
       where dof_id = pID_DOF
         and idf_num < pNUM_ITEM;
      return false;
    exception
      when no_data_found Then
        return true;
      when too_many_rows Then
        return false;
    end;
  begin
  
    If pmodulo = 'IN' Then
      mIN_data.v.id := pdof;
      INAPI_DOF.sel(mIN_data);
      mreturn.v.ID := pID_IDF;
      Begin
        INAPI_IDF.sel(mreturn);
      Exception
        when INAPI_IDF.invalid_ref Then
          RAISE_application_error(-20001,
                                  'set_dof_values : INAPI_IDF  invalid_ref');
      End;
      mIN_data.i.VL_TOTAL_PRODUTOS   := TRUE;
      mIN_data.i.VL_TOTAL_DOCUMENTO  := TRUE;
      mIN_data.i.VL_DESCONTO         := TRUE;
      mIN_data.i.VL_TOTAL_IPI        := TRUE;
      mIN_data.i.VL_TOTAL_SERVICO    := TRUE;
      mIN_data.i.VL_DESCONTO_SERVICO := TRUE;
      mIN_data.i.VL_BASE_IRRF        := TRUE;
      mIN_data.i.VL_IRRF             := TRUE;
      If first_item(mIN_data.v.ID, mreturn.v.NUM_ITEM) Then
        mIN_data.v.VL_TOTAL_PRODUTOS   := 0;
        mIN_data.v.VL_TOTAL_DOCUMENTO  := 0;
        mIN_data.v.VL_DESCONTO         := 0;
        mIN_data.v.VL_TOTAL_IPI        := 0;
        mIN_data.v.VL_TOTAL_SERVICO    := 0;
        mIN_data.v.VL_DESCONTO_SERVICO := 0;
      End if;
    
      mIN_data.v.VL_TOTAL_PRODUTOS   := mIN_data.v.VL_TOTAL_PRODUTOS +
                                        mreturn.v.VL_PRECO_TOTAL_ITEM;
      mIN_data.v.VL_TOTAL_DOCUMENTO  := mIN_data.v.VL_TOTAL_DOCUMENTO +
                                        NVL(mreturn.v.VL_PRECO_TOTAL_ITEM,
                                            0) +
                                        NVL(mreturn.v.VL_SERVICO, 0) +
                                        NVL(mreturn.v.VL_DESCONTO, 0) +
                                        NVL(mreturn.v.VL_DESCONTO_SERVICO,
                                            0) +
                                        NVL(mreturn.v.VL_SEGURO, 0) +
                                        NVL(mreturn.v.VL_FRETE, 0) +
                                        NVL(mreturn.v.VL_IPI, 0) +
                                        NVL(mreturn.v.VL_OUTRAS_DESPESAS, 0) +
                                        NVL(mreturn.v.VL_ICMS_SUBST, 0);
      mIN_data.v.VL_DESCONTO         := mIN_data.v.VL_DESCONTO +
                                        mreturn.v.VL_DESCONTO;
      mIN_data.v.VL_TOTAL_IPI        := mIN_data.v.VL_TOTAL_IPI +
                                        mreturn.v.VL_IPI;
      mIN_data.v.VL_TOTAL_SERVICO    := mIN_data.v.VL_TOTAL_SERVICO +
                                        mreturn.v.VL_SERVICO;
      mIN_data.v.VL_DESCONTO_SERVICO := mIN_data.v.VL_DESCONTO_SERVICO +
                                        mreturn.v.VL_DESCONTO_SERVICO;
      If cor_le_inicializacao(null, 'DT_INICIO_IN86', SYSDATE) <
         mIN_data.v.DT_ENTRADA_SAIDA Then
        null;
      End if;
      INAPI_DOF.upd(mIN_data);
    Elsif pmodulo = 'COR' Then
    
      mCOR_data  := mCOR_empty;
      mreturnCOR := mreturnCOR_empty;
    
      Select codigo_do_site, dof_sequence
        into mCOR_data.v.codigo_do_site, mCOR_data.v.dof_sequence
        from cor_dof
       where id = pdof;
      CORAPI_DOF.sel(mCOR_data);
      mreturnCOR.v.codigo_do_site := mCOR_data.v.codigo_do_site;
      mreturnCOR.v.dof_sequence   := mCOR_data.v.dof_sequence;
      Select idf_num
        into mreturnCOR.v.idf_num
        from cor_idf
       where id = pID_IDF;
      Begin
        CORAPI_IDF.sel(mreturnCOR);
      Exception
        when CORAPI_IDF.invalid_ref Then
          RAISE_application_error(-20001,
                                  'set_dof_values : CORAPI_IDF  invalid_ref');
      End;
      mCOR_data.i.PRECO_TOTAL_M                := TRUE;
      mCOR_data.i.PRECO_TOTAL_S                := TRUE;
      mCOR_data.i.PRECO_TOTAL_P                := TRUE;
      mCOR_data.i.PRECO_TOTAL_O                := TRUE;
      mCOR_data.i.VL_FISCAL_M                  := TRUE;
      mCOR_data.i.VL_FISCAL_S                  := TRUE;
      mCOR_data.i.VL_FISCAL_P                  := TRUE;
      mCOR_data.i.VL_FISCAL_O                  := TRUE;
      mCOR_data.i.VL_AJUSTE_PRECO_TOTAL_M      := TRUE;
      mCOR_data.i.VL_AJUSTE_PRECO_TOTAL_S      := TRUE;
      mCOR_data.i.VL_AJUSTE_PRECO_TOTAL_P      := TRUE;
      mCOR_data.i.VL_TOTAL_CONTABIL            := TRUE;
      mCOR_data.i.VL_TOTAL_FATURADO            := TRUE;
      mCOR_data.i.VL_TOTAL_ICMS                := TRUE;
      mCOR_data.i.VL_TOTAL_IPI                 := TRUE;
      mCOR_data.i.VL_TOTAL_ISS                 := TRUE;
      mCOR_data.i.VL_TOTAL_IRRF                := TRUE;
      mCOR_data.i.VL_TOTAL_STT                 := TRUE;
      mCOR_data.i.VL_TOTAL_STF                 := TRUE;
      mCOR_data.i.VL_TOTAL_ICMS_DIFA           := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS           := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_IPI            := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_IRRF           := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_STT            := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_STF            := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_STF_FRONTEIRA  := TRUE;
      mCOR_data.i.VL_TOTAL_STF_FRONTEIRA       := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_STF_IDO        := TRUE;
      mCOR_data.i.VL_TOTAL_STF_IDO             := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS_DIFA      := TRUE;
      mCOR_data.i.VL_TOTAL_PIS_PASEP           := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_PIS_PASEP      := TRUE;
      mCOR_data.i.VL_TOTAL_COFINS              := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_COFINS         := TRUE;
      mCOR_data.i.VL_TOTAL_INSS_RET            := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_INSS_RET       := TRUE;
      mCOR_data.i.VL_TOTAL_PIS_RET             := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_PIS_RET        := TRUE;
      mCOR_data.i.VL_TOTAL_COFINS_RET          := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_COFINS_RET     := TRUE;
      mCOR_data.i.VL_TOTAL_CSLL_RET            := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_CSLL_RET       := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_INSS           := TRUE;
      mCOR_data.i.VL_TOTAL_INSS                := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ISS            := TRUE;
      mCOR_data.i.IND_ISS_RETIDO_FONTE         := TRUE;
      mCOR_data.i.VL_TOTAL_OUTROS_ICMS         := TRUE;
      mCOR_data.i.VL_TOTAL_ISENTO_ICMS         := TRUE;
      mCOR_data.i.VL_PIS_ST                    := TRUE;
      mCOR_data.i.VL_COFINS_ST                 := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS_PART_REM  := TRUE;
      mCOR_data.i.VL_TOTAL_ICMS_PART_REM       := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS_PART_DEST := TRUE;
      mCOR_data.i.VL_TOTAL_ICMS_PART_DEST      := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS_FCP       := TRUE;
      mCOR_data.i.VL_TOTAL_FCP                 := TRUE;
      mCOR_data.i.VL_TOTAL_BASE_ICMS_FCP_ST    := TRUE;
      mCOR_data.i.VL_TOTAL_FCP_ST              := TRUE;
    
      mCOR_data.i.VL_ABAT_NT := TRUE;
    
      If first_item(mCOR_data.v.ID, mreturnCOR.v.IDF_NUM) Then
        mCOR_data.v.PRECO_TOTAL_M                := 0;
        mCOR_data.v.PRECO_TOTAL_S                := 0;
        mCOR_data.v.PRECO_TOTAL_P                := 0;
        mCOR_data.v.PRECO_TOTAL_O                := 0;
        mCOR_data.v.VL_FISCAL_M                  := 0;
        mCOR_data.v.VL_FISCAL_S                  := 0;
        mCOR_data.v.VL_FISCAL_P                  := 0;
        mCOR_data.v.VL_FISCAL_O                  := 0;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_M      := 0;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_S      := 0;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_P      := 0;
        mCOR_data.v.VL_TOTAL_CONTABIL            := 0;
        mCOR_data.v.VL_TOTAL_FATURADO            := 0;
        mCOR_data.v.VL_TOTAL_ICMS                := 0;
        mCOR_data.v.VL_TOTAL_IPI                 := 0;
        mCOR_data.v.VL_TOTAL_ISS                 := 0;
        mCOR_data.v.VL_TOTAL_IRRF                := 0;
        mCOR_data.v.VL_TOTAL_STT                 := 0;
        mCOR_data.v.VL_TOTAL_STF                 := 0;
        mCOR_data.v.VL_TOTAL_ICMS_DIFA           := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS           := 0;
        mCOR_data.v.VL_TOTAL_BASE_IPI            := 0;
        mCOR_data.v.VL_TOTAL_BASE_IRRF           := 0;
        mCOR_data.v.VL_TOTAL_BASE_STT            := 0;
        mCOR_data.v.VL_TOTAL_BASE_STF            := 0;
        mCOR_data.v.VL_TOTAL_BASE_STF_FRONTEIRA  := 0;
        mCOR_data.v.VL_TOTAL_STF_FRONTEIRA       := 0;
        mCOR_data.v.VL_TOTAL_BASE_STF_IDO        := 0;
        mCOR_data.v.VL_TOTAL_STF_IDO             := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS_DIFA      := 0;
        mCOR_data.v.VL_TOTAL_PIS_PASEP           := 0;
        mCOR_data.v.VL_TOTAL_BASE_PIS_PASEP      := 0;
        mCOR_data.v.VL_TOTAL_COFINS              := 0;
        mCOR_data.v.VL_TOTAL_BASE_COFINS         := 0;
        mCOR_data.v.VL_TOTAL_PIS_RET             := 0;
        mCOR_data.v.VL_TOTAL_BASE_PIS_RET        := 0;
        mCOR_data.v.VL_TOTAL_COFINS_RET          := 0;
        mCOR_data.v.VL_TOTAL_BASE_COFINS_RET     := 0;
        mCOR_data.v.VL_TOTAL_CSLL_RET            := 0;
        mCOR_data.v.VL_TOTAL_BASE_CSLL_RET       := 0;
        mCOR_data.v.VL_TOTAL_BASE_INSS           := 0;
        mCOR_data.v.VL_TOTAL_INSS                := 0;
        mCOR_data.v.VL_TOTAL_BASE_ISS            := 0;
        mCOR_data.v.VL_TOTAL_BASE_INSS_RET       := 0;
        mCOR_data.v.VL_TOTAL_INSS_RET            := 0;
        mCOR_data.v.VL_TOTAL_BASE_PIS_RET        := 0;
        mCOR_data.v.VL_TOTAL_PIS_RET             := 0;
        mCOR_data.v.VL_TOTAL_BASE_COFINS_RET     := 0;
        mCOR_data.v.VL_TOTAL_COFINS_RET          := 0;
        mCOR_data.v.VL_TOTAL_BASE_CSLL_RET       := 0;
        mCOR_data.v.VL_TOTAL_CSLL_RET            := 0;
        mCOR_data.v.IND_ISS_RETIDO_FONTE         := 'N';
        mCOR_data.v.VL_TOTAL_OUTROS_ICMS         := 0;
        mCOR_data.v.VL_TOTAL_ISENTO_ICMS         := 0;
        mCOR_data.v.VL_PIS_ST                    := 0;
        mCOR_data.v.VL_COFINS_ST                 := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_REM  := 0;
        mCOR_data.v.VL_TOTAL_ICMS_PART_REM       := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_DEST := 0;
        mCOR_data.v.VL_TOTAL_ICMS_PART_DEST      := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP       := 0;
        mCOR_data.v.VL_TOTAL_FCP                 := 0;
        mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP_ST    := 0;
        mCOR_data.v.VL_TOTAL_FCP_ST              := 0;
        mCOR_data.v.VL_ABAT_NT                   := 0;
      End if;
    
      If mreturnCOR.v.SUBCLASSE_IDF = 'M' Then
        mCOR_data.v.PRECO_TOTAL_M           := mCOR_data.v.PRECO_TOTAL_M +
                                               mreturnCOR.v.PRECO_TOTAL;
        mCOR_data.v.VL_FISCAL_M             := mCOR_data.v.VL_FISCAL_M +
                                               mreturnCOR.v.VL_FISCAL;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_M := mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_M +
                                               mreturnCOR.v.VL_AJUSTE_PRECO_TOTAL;
      Elsif mreturnCOR.v.SUBCLASSE_IDF = 'S' Then
        mCOR_data.v.PRECO_TOTAL_S           := mCOR_data.v.PRECO_TOTAL_S +
                                               mreturnCOR.v.PRECO_TOTAL;
        mCOR_data.v.VL_FISCAL_S             := mCOR_data.v.VL_FISCAL_S +
                                               mreturnCOR.v.VL_FISCAL;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_S := mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_S +
                                               mreturnCOR.v.VL_AJUSTE_PRECO_TOTAL;
      Elsif mreturnCOR.v.SUBCLASSE_IDF = 'P' Then
        mCOR_data.v.PRECO_TOTAL_P           := mCOR_data.v.PRECO_TOTAL_P +
                                               mreturnCOR.v.PRECO_TOTAL;
        mCOR_data.v.VL_FISCAL_P             := mCOR_data.v.VL_FISCAL_P +
                                               mreturnCOR.v.VL_FISCAL;
        mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_P := mCOR_data.v.VL_AJUSTE_PRECO_TOTAL_P +
                                               mreturnCOR.v.VL_AJUSTE_PRECO_TOTAL;
      Elsif mreturnCOR.v.SUBCLASSE_IDF = 'O' Then
        mCOR_data.v.PRECO_TOTAL_O := mCOR_data.v.PRECO_TOTAL_O +
                                     mreturnCOR.v.PRECO_TOTAL;
        mCOR_data.v.VL_FISCAL_O   := mCOR_data.v.VL_FISCAL_O +
                                     mreturnCOR.v.VL_FISCAL;
      End if;
    
      mCOR_data.v.VL_TOTAL_CONTABIL := mCOR_data.v.VL_TOTAL_CONTABIL +
                                       NVL(mreturnCOR.v.VL_CONTABIL, 0);
      mCOR_data.v.VL_TOTAL_FATURADO := mCOR_data.v.VL_TOTAL_CONTABIL;
    
      mCOR_data.v.VL_TOTAL_ICMS               := mCOR_data.v.VL_TOTAL_ICMS +
                                                 mreturnCOR.v.VL_ICMS;
      mCOR_data.v.VL_TOTAL_IPI                := mCOR_data.v.VL_TOTAL_IPI +
                                                 mreturnCOR.v.VL_IPI;
      mCOR_data.v.VL_TOTAL_IRRF               := mCOR_data.v.VL_TOTAL_IRRF +
                                                 mreturnCOR.v.VL_IRRF;
      mCOR_data.v.VL_TOTAL_BASE_IRRF          := mCOR_data.v.VL_TOTAL_BASE_IRRF +
                                                 mreturnCOR.v.VL_BASE_IRRF;
      mCOR_data.v.VL_TOTAL_STT                := mCOR_data.v.VL_TOTAL_STT +
                                                 mreturnCOR.v.VL_STT;
      mCOR_data.v.VL_TOTAL_STF                := mCOR_data.v.VL_TOTAL_STF +
                                                 mreturnCOR.v.VL_STF;
      mCOR_data.v.VL_TOTAL_ISS                := mCOR_data.v.VL_TOTAL_ISS +
                                                 mreturnCOR.v.VL_ISS;
      mCOR_data.v.VL_TOTAL_ICMS_DIFA          := mCOR_data.v.VL_TOTAL_ICMS_DIFA +
                                                 mreturnCOR.v.VL_DIFA;
      mCOR_data.v.VL_OUTRAS_DESPESAS          := mCOR_data.v.VL_OUTRAS_DESPESAS +
                                                 mreturnCOR.v.VL_RATEIO_ODA;
      mCOR_data.v.VL_TOTAL_BASE_ICMS          := mCOR_data.v.VL_TOTAL_BASE_ICMS +
                                                 mreturnCOR.v.VL_TRIBUTAVEL_ICMS;
      mCOR_data.v.VL_TOTAL_BASE_IPI           := mCOR_data.v.VL_TOTAL_BASE_IPI +
                                                 mreturnCOR.v.VL_TRIBUTAVEL_IPI;
      mCOR_data.v.VL_TOTAL_BASE_STT           := mCOR_data.v.VL_TOTAL_BASE_STT +
                                                 mreturnCOR.v.VL_TRIBUTAVEL_STT;
      mCOR_data.v.VL_TOTAL_BASE_STF           := mCOR_data.v.VL_TOTAL_BASE_STF +
                                                 mreturnCOR.v.VL_TRIBUTAVEL_STF;
      mCOR_data.v.VL_TOTAL_BASE_STF_FRONTEIRA := mCOR_data.v.VL_TOTAL_BASE_STF_FRONTEIRA +
                                                 mreturnCOR.v.VL_TRIBUTAVEL_STF;
      mCOR_data.v.VL_TOTAL_STF_FRONTEIRA      := mCOR_data.v.VL_TOTAL_STF_FRONTEIRA +
                                                 mreturnCOR.v.vl_stf_fronteira;
    
      mCOR_data.v.VL_TOTAL_BASE_STF_IDO := mCOR_data.v.VL_TOTAL_BASE_STF_IDO +
                                           mreturnCOR.v.VL_TRIBUTAVEL_STF;
      mCOR_data.v.VL_TOTAL_STF_IDO      := mCOR_data.v.VL_TOTAL_STF_IDO +
                                           mreturnCOR.v.vl_stf_ido;
    
      mCOR_data.v.VL_TOTAL_BASE_ISS        := mCOR_data.v.VL_TOTAL_BASE_ISS +
                                              mreturnCOR.v.VL_BASE_ISS;
      mCOR_data.v.VL_TOTAL_BASE_ICMS_DIFA  := mCOR_data.v.VL_TOTAL_BASE_ICMS_DIFA +
                                              mreturnCOR.v.VL_TRIBUTAVEL_DIFA;
      mCOR_data.v.VL_TOTAL_PIS_PASEP       := mCOR_data.v.VL_TOTAL_PIS_PASEP +
                                              mreturnCOR.v.VL_IMPOSTO_PIS;
      mCOR_data.v.VL_TOTAL_BASE_PIS_PASEP  := mCOR_data.v.VL_TOTAL_BASE_PIS_PASEP +
                                              mreturnCOR.v.VL_BASE_PIS;
      mCOR_data.v.VL_TOTAL_COFINS          := mCOR_data.v.VL_TOTAL_COFINS +
                                              mreturnCOR.v.VL_IMPOSTO_COFINS;
      mCOR_data.v.VL_TOTAL_BASE_COFINS     := mCOR_data.v.VL_TOTAL_BASE_COFINS +
                                              mreturnCOR.v.VL_BASE_COFINS;
      mCOR_data.v.VL_TOTAL_INSS_RET        := mCOR_data.v.VL_TOTAL_INSS_RET +
                                              mreturnCOR.v.VL_INSS_RET;
      mCOR_data.v.VL_TOTAL_BASE_INSS_RET   := mCOR_data.v.VL_TOTAL_BASE_INSS_RET +
                                              mreturnCOR.v.VL_BASE_INSS_RET;
      mCOR_data.v.VL_TOTAL_PIS_RET         := mCOR_data.v.VL_TOTAL_PIS_RET +
                                              mreturnCOR.v.VL_PIS_RET;
      mCOR_data.v.VL_TOTAL_BASE_PIS_RET    := mCOR_data.v.VL_TOTAL_BASE_PIS_RET +
                                              mreturnCOR.v.VL_BASE_PIS_RET;
      mCOR_data.v.VL_TOTAL_COFINS_RET      := mCOR_data.v.VL_TOTAL_COFINS_RET +
                                              mreturnCOR.v.VL_COFINS_RET;
      mCOR_data.v.VL_TOTAL_BASE_COFINS_RET := mCOR_data.v.VL_TOTAL_BASE_COFINS_RET +
                                              mreturnCOR.v.VL_BASE_COFINS_RET;
      mCOR_data.v.VL_TOTAL_CSLL_RET        := mCOR_data.v.VL_TOTAL_CSLL_RET +
                                              mreturnCOR.v.VL_CSLL_RET;
      mCOR_data.v.VL_TOTAL_BASE_CSLL_RET   := mCOR_data.v.VL_TOTAL_BASE_CSLL_RET +
                                              mreturnCOR.v.VL_BASE_CSLL_RET;
      mCOR_data.v.VL_TOTAL_INSS            := mCOR_data.v.VL_TOTAL_INSS +
                                              mreturnCOR.v.VL_INSS;
      mCOR_data.v.VL_TOTAL_BASE_INSS       := mCOR_data.v.VL_TOTAL_BASE_INSS +
                                              mreturnCOR.v.VL_BASE_INSS;
    
      mCOR_data.v.VL_PIS_ST    := mCOR_data.v.VL_PIS_ST +
                                  mreturnCOR.v.VL_PIS_ST;
      mCOR_data.v.VL_COFINS_ST := mCOR_data.v.VL_COFINS_ST +
                                  mreturnCOR.v.VL_COFINS_ST;
    
      if mreturnCOR.v.IND_ISS_RETIDO_FONTE = 'S' then
        mCOR_data.v.IND_ISS_RETIDO_FONTE := mreturnCOR.v.IND_ISS_RETIDO_FONTE;
      end if;
    
      mCOR_data.v.VL_TOTAL_OUTROS_ICMS   := mCOR_data.v.VL_TOTAL_OUTROS_ICMS +
                                            mreturnCOR.v.VL_OUTROS_ICMS;
      mCOR_data.v.VL_TOTAL_ISENTO_ICMS   := mCOR_data.v.VL_TOTAL_ISENTO_ICMS +
                                            mreturnCOR.v.VL_ISENTO_ICMS;
      mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP := mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP +
                                            nvl(mreturnCOR.v.VL_BASE_ICMS_FCP,
                                                0);
      mCOR_data.v.VL_TOTAL_FCP           := mCOR_data.v.VL_TOTAL_FCP +
                                            nvl(mreturnCOR.v.VL_ICMS_FCP, 0);
    
      mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP_ST := mCOR_data.v.VL_TOTAL_BASE_ICMS_FCP_ST +
                                               nvl(mreturnCOR.v.VL_BASE_ICMS_FCPST,
                                                   0);
      mCOR_data.v.VL_TOTAL_FCP_ST           := mCOR_data.v.VL_TOTAL_FCP_ST +
                                               nvl(mreturnCOR.v.VL_ICMS_FCPST,
                                                   0);
    
      mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_REM  := mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_REM +
                                                  nvl(mreturnCOR.v.VL_BASE_ICMS_PART_REM,
                                                      0);
      mCOR_data.v.VL_TOTAL_ICMS_PART_REM       := mCOR_data.v.VL_TOTAL_ICMS_PART_REM +
                                                  nvl(mreturnCOR.v.VL_ICMS_PART_REM,
                                                      0);
      mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_DEST := mCOR_data.v.VL_TOTAL_BASE_ICMS_PART_DEST +
                                                  nvl(mreturnCOR.v.VL_BASE_ICMS_PART_DEST,
                                                      0);
      mCOR_data.v.VL_TOTAL_ICMS_PART_DEST      := mCOR_data.v.VL_TOTAL_ICMS_PART_DEST +
                                                  nvl(mreturnCOR.v.VL_ICMS_PART_DEST,
                                                      0);
      mCOR_data.v.VL_ABAT_NT                   := mCOR_data.v.VL_ABAT_NT +
                                                  nvl(mreturnCOR.v.VL_ICMS_DESC_L,
                                                      0);
    
      CORAPI_DOF.upd(mCOR_data);
    
      commit;
    
    End if;
  
  exception
    when others then
      merror      := SQLERRM;
      merror_code := SQLCODE;
      raise_application_error(-20001,
                              'set_dof_values ' || ' COR_DOF.ID : ' || pdof ||
                              ' Item : ' || mreturnCOR.v.idf_num ||
                              ' Critica : ' || merror);
    
  end set_dof_values;

  function GET_SUBCLASSE_IDF(pCODIGO     in varchar2,
                             pNop_Codigo in cor_idf.nop_codigo%type)
    return cor_idf.subclasse_idf%type is
    msubclasse cor_idf.subclasse_idf%type := null;
    mcount     integer := 0;
    mmps_nop   COR_NATUREZA_DE_OPERACAO.IND_MERC_PRES_SERV%TYPE;
  
  Begin
    Begin
      Select ind_merc_pres_serv
        Into mmps_nop
        From cor_natureza_de_operacao
       Where nop_codigo = pnop_codigo;
    Exception
      When no_data_found then
        Return null;
    End;
  
    Return mmps_nop;
  end GET_SUBCLASSE_IDF;

  Procedure sel_itf_dof(p_dof_itf in out dof_itf,
                        pModulo   sap_itf_appl_synchro.appl_synchro%type) IS
    msql            VARCHAR2(10000);
    mcur            PLS_INTEGER;
    mrows_processed PLS_INTEGER;
    mcount          PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
  BEGIN
  
    msql := 'SELECT';
    msql := msql || ' ID,';
    msql := msql || ' XML_ID,';
    msql := msql || ' DH_IDOC,';
    msql := msql || ' NUM_IDOC,';
    msql := msql || ' USNAM,';
    msql := msql || ' CPROG,';
    msql := msql || ' DIRECT,';
    msql := msql || ' NFTYPE,';
    msql := msql || ' MODEL,';
    msql := msql || ' SERIESSUB,';
    msql := msql || ' NFNUM,';
    msql := msql || ' DOCNUM,';
    msql := msql || ' DOCDAT,';
    msql := msql || ' PSTDAT,';
    msql := msql || ' CFOPITM,';
    msql := msql || ' SITUACAO,';
    msql := msql || ' IND_CANCEL,';
    msql := msql || ' EMPRESA_FILIAL,';
    msql := msql || ' INCO1,';
    msql := msql || ' TRATY,';
    msql := msql || ' TRAID,';
    msql := msql || ' BRGEW,';
    msql := msql || ' NTGEW,';
    msql := msql || ' ZTERM,';
    msql := msql || ' PARVW,';
    msql := msql || ' PARTYP,';
    msql := msql || ' PARID,';
    msql := msql || ' DOCREF,';
    msql := msql || ' PARXCPDK,';
    msql := msql || ' ALIQ_IRRF,';
    msql := msql || ' OBSERVAT,';
    msql := msql || ' WITHA,';
    msql := msql || ' EXCBAS,';
    msql := msql || ' SHPUNT,';
    msql := msql || ' ANZPK,';
    msql := msql || ' PESO_BRUTO_TOTAL,';
    msql := msql || ' DH_INCLUSAO,';
    msql := msql || ' DH_CRITICA,';
    msql := msql || ' CTRL_CRITICA,';
    msql := msql || ' MSG_CRITICA';
  
    msql := msql || ' FROM';
    msql := msql || '     SAP_ITF_DOF_' || pmodulo;
    msql := msql || ' WHERE';
    msql := msql || '     id = ' || p_dof_itf.ID;
  
    mcur := dbms_sql.open_cursor;
    dbms_sql.parse(mcur, msql, dbms_sql.v7);
  
    dbms_sql.define_column(mcur, 1, p_dof_itf.ID);
    dbms_sql.define_column(mcur, 2, p_dof_itf.XML_ID);
    dbms_sql.define_column(mcur, 3, p_dof_itf.DH_IDOC);
    dbms_sql.define_column_char(mcur, 4, p_dof_itf.NUM_IDOC, 30);
    dbms_sql.define_column_char(mcur, 5, p_dof_itf.USNAM, 12);
    dbms_sql.define_column_char(mcur, 6, p_dof_itf.CPROG, 40);
    dbms_sql.define_column_char(mcur, 7, p_dof_itf.DIRECT, 1);
    dbms_sql.define_column_char(mcur, 8, p_dof_itf.NFTYPE, 20);
    dbms_sql.define_column_char(mcur, 9, p_dof_itf.MODEL, 2);
    dbms_sql.define_column_char(mcur, 10, p_dof_itf.SERIESSUB, 6);
    dbms_sql.define_column_char(mcur, 11, p_dof_itf.NFNUM, 6);
    dbms_sql.define_column_char(mcur, 12, p_dof_itf.DOCNUM, 10);
    dbms_sql.define_column(mcur, 13, p_dof_itf.DOCDAT);
    dbms_sql.define_column(mcur, 14, p_dof_itf.PSTDAT);
    dbms_sql.define_column_char(mcur, 15, p_dof_itf.CFOPITM, 16);
    dbms_sql.define_column_char(mcur, 16, p_dof_itf.SITUACAO, 1);
    dbms_sql.define_column_char(mcur, 17, p_dof_itf.IND_CANCEL, 1);
    dbms_sql.define_column_char(mcur, 18, p_dof_itf.EMPRESA_FILIAL, 30);
    dbms_sql.define_column_char(mcur, 19, p_dof_itf.INCO1, 3);
    dbms_sql.define_column_char(mcur, 20, p_dof_itf.TRATY, 4);
    dbms_sql.define_column_char(mcur, 21, p_dof_itf.TRAID, 20);
    dbms_sql.define_column(mcur, 22, p_dof_itf.BRGEW);
    dbms_sql.define_column(mcur, 23, p_dof_itf.NTGEW);
    dbms_sql.define_column_char(mcur, 24, p_dof_itf.ZTERM, 4);
    dbms_sql.define_column_char(mcur, 25, p_dof_itf.PARVW, 2);
    dbms_sql.define_column_char(mcur, 26, p_dof_itf.PARTYP, 1);
    dbms_sql.define_column_char(mcur, 27, p_dof_itf.PARID, 30);
    dbms_sql.define_column_char(mcur, 28, p_dof_itf.DOCREF, 10);
    dbms_sql.define_column_char(mcur, 29, p_dof_itf.PARXCPDK, 1);
    dbms_sql.define_column_char(mcur, 30, p_dof_itf.ALIQ_IRRF, 13);
    dbms_sql.define_column_char(mcur, 31, p_dof_itf.OBSERVAT, 50);
    dbms_sql.define_column_char(mcur, 32, p_dof_itf.WITHA, 17);
    dbms_sql.define_column_char(mcur, 33, p_dof_itf.EXCBAS, 21);
    dbms_sql.define_column_char(mcur, 34, p_dof_itf.SHPUNT, 3);
    dbms_sql.define_column_char(mcur, 35, p_dof_itf.ANZPK, 5);
    dbms_sql.define_column(mcur, 36, p_dof_itf.PESO_BRUTO_TOTAL);
    dbms_sql.define_column(mcur, 37, p_dof_itf.DH_INCLUSAO);
    dbms_sql.define_column(mcur, 38, p_dof_itf.DH_CRITICA);
    dbms_sql.define_column(mcur, 39, p_dof_itf.CTRL_CRITICA);
    dbms_sql.define_column_char(mcur, 40, p_dof_itf.MSG_CRITICA, 2000);
  
    mrows_processed := dbms_sql.execute(mcur);
    mfetch_rows     := dbms_sql.fetch_rows(mcur);
    dbms_sql.column_value(mcur, 1, p_dof_itf.ID);
    dbms_sql.column_value(mcur, 2, p_dof_itf.XML_ID);
    dbms_sql.column_value(mcur, 3, p_dof_itf.DH_IDOC);
    dbms_sql.column_value_char(mcur, 4, p_dof_itf.NUM_IDOC);
    dbms_sql.column_value_char(mcur, 5, p_dof_itf.USNAM);
    dbms_sql.column_value_char(mcur, 6, p_dof_itf.CPROG);
    dbms_sql.column_value_char(mcur, 7, p_dof_itf.DIRECT);
    dbms_sql.column_value_char(mcur, 8, p_dof_itf.NFTYPE);
    dbms_sql.column_value_char(mcur, 9, p_dof_itf.MODEL);
    dbms_sql.column_value_char(mcur, 10, p_dof_itf.SERIESSUB);
    dbms_sql.column_value_char(mcur, 11, p_dof_itf.NFNUM);
    dbms_sql.column_value_char(mcur, 12, p_dof_itf.DOCNUM);
    dbms_sql.column_value(mcur, 13, p_dof_itf.DOCDAT);
    dbms_sql.column_value(mcur, 14, p_dof_itf.PSTDAT);
    dbms_sql.column_value_char(mcur, 15, p_dof_itf.CFOPITM);
    dbms_sql.column_value_char(mcur, 16, p_dof_itf.SITUACAO);
    dbms_sql.column_value_char(mcur, 17, p_dof_itf.IND_CANCEL);
    dbms_sql.column_value_char(mcur, 18, p_dof_itf.EMPRESA_FILIAL);
    dbms_sql.column_value_char(mcur, 19, p_dof_itf.INCO1);
    dbms_sql.column_value_char(mcur, 20, p_dof_itf.TRATY);
    dbms_sql.column_value_char(mcur, 21, p_dof_itf.TRAID);
    dbms_sql.column_value(mcur, 22, p_dof_itf.BRGEW);
    dbms_sql.column_value(mcur, 23, p_dof_itf.NTGEW);
    dbms_sql.column_value_char(mcur, 24, p_dof_itf.ZTERM);
    dbms_sql.column_value_char(mcur, 25, p_dof_itf.PARVW);
    dbms_sql.column_value_char(mcur, 26, p_dof_itf.PARTYP);
    dbms_sql.column_value_char(mcur, 27, p_dof_itf.PARID);
    dbms_sql.column_value_char(mcur, 28, p_dof_itf.DOCREF);
    dbms_sql.column_value_char(mcur, 29, p_dof_itf.PARXCPDK);
    dbms_sql.column_value_char(mcur, 30, p_dof_itf.ALIQ_IRRF);
    dbms_sql.column_value_char(mcur, 31, p_dof_itf.OBSERVAT);
    dbms_sql.column_value_char(mcur, 32, p_dof_itf.WITHA);
    dbms_sql.column_value_char(mcur, 33, p_dof_itf.EXCBAS);
    dbms_sql.column_value_char(mcur, 34, p_dof_itf.SHPUNT);
    dbms_sql.column_value_char(mcur, 35, p_dof_itf.ANZPK);
    dbms_sql.column_value(mcur, 36, p_dof_itf.PESO_BRUTO_TOTAL);
    dbms_sql.column_value(mcur, 37, p_dof_itf.DH_INCLUSAO);
    dbms_sql.column_value(mcur, 38, p_dof_itf.DH_CRITICA);
    dbms_sql.column_value(mcur, 39, p_dof_itf.CTRL_CRITICA);
    dbms_sql.column_value_char(mcur, 40, p_dof_itf.MSG_CRITICA);
  
    p_dof_itf.parid := rtrim(ltrim(p_dof_itf.parid));
  
    p_dof_itf.NFTYPE := rtrim(ltrim(p_dof_itf.NFTYPE));
  
    dbms_sql.close_cursor(mcur);
  exception
    when others Then
      raise_application_error(-20001, 'SEL_ITF_DOF' || sqlerrm);
  END;

  procedure sel_itf_idf(p_idf_itf in out idf_itf,
                        pModulo   sap_itf_appl_synchro.appl_synchro%type) IS
    msql            VARCHAR2(10000);
    mcur            PLS_INTEGER;
    mrows_processed PLS_INTEGER;
    mcount          PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
  BEGIN
  
    msql := 'SELECT';
    msql := msql || ' ID,';
    msql := msql || ' YJ1BNFDOC_ID,';
    msql := msql || ' ITMNUM,';
    msql := msql || ' CFOP,';
    msql := msql || ' NBM,';
    msql := msql || ' MATNR,';
    msql := msql || ' MAKTX,';
    msql := msql || ' MATORG,';
    msql := msql || ' TAXSIT,';
    msql := msql || ' TAXSI2,';
    msql := msql || ' UNI_NF,';
    msql := msql || ' MEINS,';
    msql := msql || ' MENGE,';
    msql := msql || ' QSATZ,';
    msql := msql || ' NETWR,';
    msql := msql || ' NETPR,';
    msql := msql || ' NETDIS,';
    msql := msql || ' J_1AQSSHH,';
    msql := msql || ' VL_CONTABIL,';
    msql := msql || ' VL_FATURADO,';
    msql := msql || ' VL_FISCAL,';
    msql := msql || ' J_1AQBSHH,';
    msql := msql || ' NETFRE,';
    msql := msql || ' NETOTH,';
    msql := msql || ' NETINS,';
    msql := msql || ' MATUSE,';
    msql := msql || ' rtrim(ltrim(REFKEY)) REFKEY,';
    msql := msql || ' DH_INCLUSAO,';
    msql := msql || ' DH_CRITICA,';
    msql := msql || ' CTRL_CRITICA,';
    msql := msql || ' MSG_CRITICA';
  
    msql := msql || ' FROM';
    msql := msql || '     SAP_ITF_IDF_' || pmodulo;
    msql := msql || ' WHERE';
    msql := msql || '     id = ' || p_idf_itf.ID;
    mcur := dbms_sql.open_cursor;
    dbms_sql.parse(mcur, msql, dbms_sql.v7);
  
    dbms_sql.define_column(mcur, 1, p_idf_itf.ID);
    dbms_sql.define_column(mcur, 2, p_idf_itf.YJ1BNFDOC_ID);
    dbms_sql.define_column_char(mcur, 3, p_idf_itf.ITMNUM, 6);
    dbms_sql.define_column_char(mcur, 4, p_idf_itf.CFOP, 10);
    dbms_sql.define_column_char(mcur, 5, p_idf_itf.NBM, 16);
    dbms_sql.define_column_char(mcur, 6, p_idf_itf.MATNR, 18);
    dbms_sql.define_column_char(mcur, 7, p_idf_itf.MAKTX, 40);
    dbms_sql.define_column_char(mcur, 8, p_idf_itf.MATORG, 1);
    dbms_sql.define_column_char(mcur, 9, p_idf_itf.TAXSIT, 2);
    dbms_sql.define_column_char(mcur, 10, p_idf_itf.TAXSI2, 5);
    dbms_sql.define_column_char(mcur, 11, p_idf_itf.UNI_NF, 3);
    dbms_sql.define_column_char(mcur, 12, p_idf_itf.MEINS, 3);
    dbms_sql.define_column(mcur, 13, p_idf_itf.MENGE);
    dbms_sql.define_column_char(mcur, 14, p_idf_itf.QSATZ, 7);
    dbms_sql.define_column(mcur, 15, p_idf_itf.NETWR);
    dbms_sql.define_column(mcur, 16, p_idf_itf.NETPR);
    dbms_sql.define_column(mcur, 17, p_idf_itf.NETDIS);
    dbms_sql.define_column_char(mcur, 18, p_idf_itf.J_1AQSSHH, 17);
    dbms_sql.define_column(mcur, 19, p_idf_itf.VL_CONTABIL);
    dbms_sql.define_column(mcur, 20, p_idf_itf.VL_FATURADO);
    dbms_sql.define_column(mcur, 21, p_idf_itf.VL_FISCAL);
    dbms_sql.define_column_char(mcur, 22, p_idf_itf.J_1AQBSHH, 17);
    dbms_sql.define_column(mcur, 23, p_idf_itf.NETFRE);
    dbms_sql.define_column(mcur, 24, p_idf_itf.NETOTH);
    dbms_sql.define_column(mcur, 25, p_idf_itf.NETINS);
    dbms_sql.define_column_char(mcur, 26, p_idf_itf.MATUSE, 1);
    dbms_sql.define_column_char(mcur, 27, p_idf_itf.REFKEY, 35);
    dbms_sql.define_column(mcur, 28, p_idf_itf.DH_INCLUSAO);
    dbms_sql.define_column(mcur, 29, p_idf_itf.DH_CRITICA);
    dbms_sql.define_column(mcur, 30, p_idf_itf.CTRL_CRITICA);
    dbms_sql.define_column_char(mcur, 31, p_idf_itf.MSG_CRITICA, 2000);
  
    mrows_processed := dbms_sql.execute(mcur);
    mfetch_rows     := dbms_sql.fetch_rows(mcur);
  
    dbms_sql.column_value(mcur, 1, p_idf_itf.ID);
    dbms_sql.column_value(mcur, 2, p_idf_itf.YJ1BNFDOC_ID);
    dbms_sql.column_value_char(mcur, 3, p_idf_itf.ITMNUM);
    dbms_sql.column_value_char(mcur, 4, p_idf_itf.CFOP);
    dbms_sql.column_value_char(mcur, 5, p_idf_itf.NBM);
    dbms_sql.column_value_char(mcur, 6, p_idf_itf.MATNR);
    dbms_sql.column_value_char(mcur, 7, p_idf_itf.MAKTX);
    dbms_sql.column_value_char(mcur, 8, p_idf_itf.MATORG);
    dbms_sql.column_value_char(mcur, 9, p_idf_itf.TAXSIT);
    dbms_sql.column_value_char(mcur, 10, p_idf_itf.TAXSI2);
    dbms_sql.column_value_char(mcur, 11, p_idf_itf.UNI_NF);
    dbms_sql.column_value_char(mcur, 12, p_idf_itf.MEINS);
    dbms_sql.column_value(mcur, 13, p_idf_itf.MENGE);
    dbms_sql.column_value_char(mcur, 14, p_idf_itf.QSATZ);
    dbms_sql.column_value(mcur, 15, p_idf_itf.NETWR);
    dbms_sql.column_value(mcur, 16, p_idf_itf.NETPR);
    dbms_sql.column_value(mcur, 17, p_idf_itf.NETDIS);
    dbms_sql.column_value_char(mcur, 18, p_idf_itf.J_1AQSSHH);
    dbms_sql.column_value(mcur, 19, p_idf_itf.VL_CONTABIL);
    dbms_sql.column_value(mcur, 20, p_idf_itf.VL_FATURADO);
    dbms_sql.column_value(mcur, 21, p_idf_itf.VL_FISCAL);
    dbms_sql.column_value_char(mcur, 22, p_idf_itf.J_1AQBSHH);
    dbms_sql.column_value(mcur, 23, p_idf_itf.NETFRE);
    dbms_sql.column_value(mcur, 24, p_idf_itf.NETOTH);
    dbms_sql.column_value(mcur, 25, p_idf_itf.NETINS);
    dbms_sql.column_value_char(mcur, 26, p_idf_itf.MATUSE);
    dbms_sql.column_value_char(mcur, 27, p_idf_itf.REFKEY);
    dbms_sql.column_value(mcur, 28, p_idf_itf.DH_INCLUSAO);
    dbms_sql.column_value(mcur, 29, p_idf_itf.DH_CRITICA);
    dbms_sql.column_value(mcur, 30, p_idf_itf.CTRL_CRITICA);
    dbms_sql.column_value_char(mcur, 31, p_idf_itf.MSG_CRITICA);
  
    dbms_sql.close_cursor(mcur);
  exception
    when others Then
      syn_dynamic.close_cursor(mcur);
    
      raise_application_error(-20001, 'SEL_ITF_IDF' || sqlerrm);
  END;

  FUNCTION get_est_pfj(pIEST_ID in_almoxarifado.codigo%type,
                       pRetorno varchar2) return in_pfj.id%type is
    mRETURN in_pfj.id%type;
  
  Begin
    SELECT decode(pRetorno, 'IPFJ_ID', EST.IPFJ_ID, null)
      INTO mRETURN
      FROM IN_ESTABELECIMENTO EST
     WHERE est.id = pIEST_id;
    return mRETURN;
  Exception
    when no_data_found Then
      return - 1;
    when too_many_rows Then
      raise_application_error(-20001,
                              'Cadastro de Estabelecimento SAP duplicado : ' ||
                              pIEST_ID);
  End;

  function get_docref(p_yj1bnflin_id number, p_modulo varchar2)
    return varchar2 is
  
    msql            VARCHAR2(1000);
    mcur            PLS_INTEGER;
    mrows_processed PLS_INTEGER;
    mcount          PLS_INTEGER;
    mfetch_rows     PLS_INTEGER;
    mdocref_values  docref_values;
    empty_values    docref_values;
  
  Begin
  
    mdocref_values := empty_values;
  
    msql := ' select ';
    msql := msql || ' docnum  , ';
    msql := msql || ' itmnum    ';
    msql := msql || ' from sap_itf_idf_assoc_' || p_modulo;
    msql := msql || ' where yj1bnflin_id = :p1';
  
    mcur := dbms_sql.open_cursor;
    dbms_sql.parse(mcur, msql, dbms_sql.v7);
  
    dbms_sql.bind_variable(mcur, 'p1', p_yj1bnflin_id);
    dbms_sql.parse(mcur, msql, dbms_sql.v7);
  
    dbms_sql.define_column_char(mcur, 1, mdocref_values.docnum, 10);
    dbms_sql.define_column_char(mcur, 2, mdocref_values.itmnum, 6);
  
    mrows_processed := dbms_sql.execute(mcur);
    mfetch_rows     := dbms_sql.fetch_rows(mcur);
    dbms_sql.column_value_char(mcur, 1, mdocref_values.docnum);
    dbms_sql.column_value_char(mcur, 2, mdocref_values.itmnum);
  
    dbms_sql.close_cursor(mcur);
    return ltrim(mdocref_values.docnum);
  
  Exception
    when Others Then
      syn_dynamic.close_cursor(mcur);
    
      return null;
  End;

  function internal_get_sufixo_estab return varchar2 is
  Begin
    return sufixo_estab;
  end;

  procedure set_sufixo_estab(pSufixo varchar2) is
  Begin
    sufixo_estab := pSufixo;
  end;

  function get_sufixo_estab(pind_parid varchar2 default null) return varchar2 is
  Begin
    if pind_parid is null then
      return internal_get_sufixo_estab;
    else
      return sufixo_estab_parid;
    end if;
  end;

  procedure set_sufixo_estab(pind_parid varchar2, pSufixo varchar2) is
  Begin
    sufixo_estab_parid := pSufixo;
  end;

END;

/
