create or replace package HON_REINF_GAP_EXPORT is

  -- Author  : 1BY1
  -- Created : 11/05/2018 13:27:54
  -- Purpose : Permitir a exportacao de registros para tratamento de gaps do REINF

  -- Procedure para exportacao dos dados no formato das tabelas SYNITF
  -- Parametros: p_est_codigo    - Estabelecimento a processar (aceita '%')
  --             p_dt_inicio     - Data inicial de processamento
  --             p_dt_fim        - Data final de processamento
  --             p_siss_codigo   - Codigo de servi?o a processar (aceita '%')
  --             p_subclasse_idf - Tipo de subclasse de idf a processar (aceita '%')
  --             p_tipo_synitf   - Tipo de interface a gerar.
  --                               Aceita os tipos: SYNITF_PROCESSO
  --                                                SYNITF_DOF_PROCESSO
  --                                                SYNITF_IDF
  --                                                SYNITF_IDF_CONSTRUCAO_CIVIL
  --
  procedure exporta_synitf (p_est_codigo    varchar2
                           ,p_dt_inicio     date
                           ,p_dt_fim        date
                           ,p_siss_codigo   varchar2
                           ,p_subclasse_idf varchar2
                           ,p_tipo_synitf   varchar2
                           ,p_proc_id       number);
  --
  -- Procedure para geracao do relatorio de conferencia
  -- Parametros: p_est_codigo    - Estabelecimento a processar (aceita '%')
  --             p_dt_inicio     - Data inicial de processamento
  --             p_dt_fim        - Data final de processamento
  --             p_siss_codigo   - Codigo de servi?o a processar (aceita '%')
  --             p_subclasse_idf - Tipo de subclasse de idf a processar (aceita '%')
  --
  procedure relatorio_conferencia (p_est_codigo    varchar2
                                  ,p_dt_inicio     date
                                  ,p_dt_fim        date
                                  ,p_siss_codigo   varchar2
                                  ,p_subclasse_idf varchar2
                                  ,p_proc_id       number);
  --
end HON_REINF_GAP_EXPORT;
/
create or replace package body HON_REINF_GAP_EXPORT is

  procedure exporta_synitf (p_est_codigo    varchar2
                           ,p_dt_inicio     date
                           ,p_dt_fim        date
                           ,p_siss_codigo   varchar2
                           ,p_subclasse_idf varchar2
                           ,p_tipo_synitf   varchar2
                           ,p_proc_id       number) is
    --
    procedure imp(p_texto varchar2) is
    begin
      syn_out.put_line(p_texto);
    end imp;
    --
    procedure gera_synitf_processo is
      m_linha varchar2(2000);
    begin
      -- Imprime cabecalho
      imp ('SYNITF_PROCESSO');
      imp ('CONTRIBUINTE_PFJ_CODIGO;CONTRIB_PFJ_COD_ORIGEM;CONTRIB_PFJ_COD_CHAVE_ORIGEM;'
         ||'TP_PROCESSO;IND_ORIGEM_PROCESSO;NUMERO;SUSPENSAO_CODIGO;IND_SUSPENSAO;DT_ABERTURA;'
         ||'DT_ENCERRAMENTO;DT_DECISAO_SENTENCA_DESPACHO;IND_DEPOSITO_MONTANTE;'
         ||'VARA_MUNICIPIO_CODIGO;VARA_CODIGO;IND_AUTORIA;NATUREZA_RENDIMENTO;QTD_MESES;'
         ||'IND_ORIGEM_RECURSOS;ORIGEM_RECURSOS_PFJ_CODIGO;ORIGEM_REC_PFJ_COD_ORIGEM;'
         ||'ORIGEM_REC_COD_CHAVE_ORIGEM;VL_CUSTOS_JUDICIAIS;VARA_UF_CODIGO;CTRL_INSTRUCAO;'
         ||'DH_CRITICA;CTRL_CRITICA;MSG_CRITICA;DH_INCLUSAO;TRIBUTO;PERCENTUAL');
      --
      -- Loop para impressao dos registros
      for m_prc in (select p.contribuinte_pfj_codigo        contribuinte_pfj_codigo
                         , null                             contrib_pfj_cod_origem
                         , null                             contrib_pfj_cod_chave_origem
                         , p.tp_processo                    tp_processo
                         , p.ind_origem_processo            ind_origem_processo
                         , p.numero                         numero
                         , pt.suspensao_codigo              suspensao_codigo
                         , pt.ind_suspensao                 ind_suspensao
                         , p.dt_abertura                    dt_abertura
                         , p.dt_encerramento                dt_encerramento
                         , pt.dt_decisao_sentenca_despacho  dt_decisao_sentenca_despacho
                         , pt.ind_deposito_montante         ind_deposito_montante
						             , p.vara_municipio_codigo          vara_municipio_codigo
                         , p.vara_codigo                    vara_codigo
                         , p.ind_autoria                    ind_autoria
                         , pt.natureza_rendimento           natureza_rendimento
                         , pt.qtd_meses                     qtd_meses
                         , p.ind_origem_recursos            ind_origem_recursos
                         , p.origem_recursos_pfj_codigo     origem_recursos_pfj_codigo
                         , null                             origem_rec_pfj_cod_origem
                         , null                             origem_rec_cod_chave_origem
                         , p.vl_custos_judiciais            vl_custos_judiciais
                         , p.vara_uf_codigo                 vara_uf_codigo
                         , 'M'                              ctrl_instrucao
                         , null                             dh_critica
                         , null                             ctrl_critica
                         , null                             msg_critica
                         , null                             dh_inclusao
                         , null                             tributo
                         , null                             percentual
                      from cor_processo         p
					            join cor_processo_tributo pt
					              on p.id = pt.processo_id
                      join cor_dof_processo dp
                        on p.numero = dp.num_proc)
      loop
        m_linha := replace(m_prc.contribuinte_pfj_codigo      ,';',' ')
           ||';'|| replace(m_prc.contrib_pfj_cod_origem       ,';',' ')
           ||';'|| replace(m_prc.contrib_pfj_cod_chave_origem ,';',' ')
           ||';'|| replace(m_prc.tp_processo                  ,';',' ')
           ||';'|| replace(m_prc.ind_origem_processo          ,';',' ')
           ||';'|| replace(m_prc.numero                       ,';',' ')
           ||';'|| replace(m_prc.suspensao_codigo             ,';',' ')
           ||';'|| replace(m_prc.ind_suspensao                ,';',' ')
           ||';'|| replace(m_prc.dt_abertura                  ,';',' ')
           ||';'|| replace(m_prc.dt_encerramento              ,';',' ')
           ||';'|| replace(m_prc.dt_decisao_sentenca_despacho ,';',' ')
           ||';'|| replace(m_prc.ind_deposito_montante        ,';',' ')
           ||';'|| replace(m_prc.vara_municipio_codigo        ,';',' ')
           ||';'|| replace(m_prc.vara_codigo                  ,';',' ')
           ||';'|| replace(m_prc.ind_autoria                  ,';',' ')
           ||';'|| replace(m_prc.natureza_rendimento          ,';',' ')
           ||';'|| replace(m_prc.qtd_meses                    ,';',' ')
           ||';'|| replace(m_prc.ind_origem_recursos          ,';',' ')
           ||';'|| replace(m_prc.origem_recursos_pfj_codigo   ,';',' ')
           ||';'|| replace(m_prc.origem_rec_pfj_cod_origem    ,';',' ')
           ||';'|| replace(m_prc.origem_rec_cod_chave_origem  ,';',' ')
           ||';'|| replace(m_prc.vl_custos_judiciais          ,';',' ')
           ||';'|| replace(m_prc.vara_uf_codigo               ,';',' ')
           ||';'|| replace(m_prc.ctrl_instrucao               ,';',' ')
           ||';'|| replace(m_prc.dh_critica                   ,';',' ')
           ||';'|| replace(m_prc.ctrl_critica                 ,';',' ')
           ||';'|| replace(m_prc.msg_critica                  ,';',' ')
           ||';'|| replace(m_prc.dh_inclusao                  ,';',' ')
           ||';'|| replace(m_prc.tributo                      ,';',' ')
           ||';'|| replace(m_prc.percentual                   ,';',' ');
        --
        imp(m_linha);
        --
      end loop;
    end gera_synitf_processo;
    --
    procedure gera_synitf_dof_processo is
      m_linha varchar2(2000);
    begin
      -- Imprime cabecalho
      imp ('SYNITF_DOF_PROCESSO');
      imp ('INFORMANTE_EST_CODIGO;DOF_IMPORT_NUMERO;INFORMANTE_PFJ_CHAVE_ORIGEM;'
         ||'INFORMANTE_PFJ_ORIGEM;NUM_PROC;IND_PROC;CTRL_CRITICA;CTRL_INSTRUCAO;DH_CRITICA;'
         ||'MSG_CRITICA;DH_INCLUSAO;CHAVE_ORIGEM;ORIGEM;HASH;SUSPENSAO_CODIGO;IND_SUSPENSAO');
      --
      -- Loop para impressao dos registros
      for m_dpr in (select distinct 
                           d.informante_est_codigo informante_est_codigo
                         , d.dof_import_numero     dof_import_numero
                         , null                    informante_pfj_chave_origem
                         , null                    informante_pfj_origem
                         , dp.num_proc             num_proc
                         , dp.ind_proc             ind_proc
                         , null                    ctrl_critica
                         , 'M'                     ctrl_instrucao
                         , null                    dh_critica
                         , null                    msg_critica
                         , null                    dh_inclusao
                         , dp.chave_origem         chave_origem
                         , dp.origem               origem
                         , null                    hash
                         , dp.suspensao_codigo     suspensao_codigo
                         , pt.ind_suspensao        ind_suspensao
                      from cor_dof          d
                      join cor_idf          i
                        on d.codigo_do_site = i.codigo_do_site
                       and d.dof_sequence   = i.dof_sequence
                      left join cor_dof_processo     dp
                        on d.codigo_do_site = dp.codigo_do_site
                       and d.dof_sequence   = dp.dof_sequence
                      left join cor_processo_tributo pt
                        on pt.id = dp.processo_tributo_id
                     where d.informante_est_codigo like p_est_codigo
                       and d.dh_emissao between p_dt_inicio
                                            and p_dt_fim
                       and i.siss_codigo like p_siss_codigo
                       and i.subclasse_idf like p_subclasse_idf
                     order by 1,2)
      loop
        m_linha := m_dpr.informante_est_codigo
           ||';'|| m_dpr.dof_import_numero
           ||';'|| m_dpr.informante_pfj_chave_origem
           ||';'|| m_dpr.informante_pfj_origem
           ||';'|| m_dpr.num_proc
           ||';'|| m_dpr.ind_proc
           ||';'|| m_dpr.ctrl_critica
           ||';'|| m_dpr.ctrl_instrucao
           ||';'|| m_dpr.dh_critica
           ||';'|| m_dpr.msg_critica
           ||';'|| m_dpr.dh_inclusao
           ||';'|| m_dpr.chave_origem
           ||';'|| m_dpr.origem
           ||';'|| m_dpr.hash
           ||';'|| m_dpr.suspensao_codigo
           ||';'|| m_dpr.ind_suspensao;
        --
        imp(m_linha);
        --
      end loop;
    end gera_synitf_dof_processo;
    --
    procedure gera_synitf_idf is
      m_linha varchar2(32000);
    begin
      -- Imprime cabecalho
      imp ('SYNITF_IDF');
         
         
      imp ('INFORMANTE_EST_CODIGO;INFORMANTE_PFJ_CHAVE_ORIGEM;INFORMANTE_PFJ_ORIGEM;DOF_IMPORT_NUMERO;'
         ||'IDF_NUM;ALIQ_DIFA;ALIQ_ICMS;ALIQ_II;ALIQ_IPI;ALIQ_ISS;ALIQ_STF;ALIQ_STT;ORD_IMPRESSAO;'
         ||'PRECO_TOTAL;PRECO_UNITARIO;QTD;SUBCLASSE_IDF;VL_AJUSTE_PRECO_TOTAL;VL_BASE_ICMS;VL_BASE_IPI;'
         ||'VL_BASE_IRRF;VL_BASE_ISS;VL_BASE_STF;VL_BASE_STT;VL_CONTABIL;VL_DIFA;VL_FATURADO;VL_FISCAL;'
         ||'VL_ICMS;VL_II;VL_IPI;VL_IRRF;VL_ISENTO_ICMS;VL_ISENTO_IPI;VL_ISS;VL_OUTROS_ICMS;VL_OUTROS_IPI;'
         ||'VL_RATEIO_AJUSTE_PRECO;VL_RATEIO_FRETE;VL_RATEIO_ODA;VL_RATEIO_SEGURO;VL_STF;VL_STT;'
         ||'VL_TRIBUTAVEL_ICMS;VL_TRIBUTAVEL_IPI;VL_TRIBUTAVEL_STF;VL_TRIBUTAVEL_STT;AM_CODIGO;CFOP_CODIGO;'
         ||'DT_DI;ENTSAI_UNI_CODIGO;FIN_CODIGO;IDF_TEXTO_COMPLEMENTAR;MERC_CODIGO;MERC_CHAVE_ORIGEM;'
         ||'MERC_ORIGEM;MUN_CODIGO;NBM_CODIGO;NOP_CODIGO;NUM_DI;OBS_FISCAL_ICMS;OBS_FISCAL_IPI;'
         ||'OBS_FISCAL_IRRF;OBS_FISCAL_ISS;OBS_FISCAL_STF;OBS_FISCAL_STT;OBS_FISCAL_OPERACIONAL;OM_CODIGO;'
         ||'PESO_BRUTO_KG;PESO_LIQUIDO_KG;PRES_CODIGO;SISS_CODIGO;STC_CODIGO;STP_CODIGO;CTRL_INSTRUCAO;'
         ||'MSG_CRITICA;DH_CRITICA;CTRL_CRITICA;TIPO_COMPLEMENTO;VL_TRIBUTAVEL_DIFA;ALIQ_INSS;VL_BASE_INSS;'
         ||'VL_INSS;VL_ALIQ_COFINS;VL_ALIQ_PIS;VL_BASE_PIS;VL_BASE_COFINS;VL_IMPOSTO_PIS;VL_IMPOSTO_COFINS;'
         ||'EMERC_CODIGO;CODIGO_RETENCAO;CODIGO_ISS_MUNICIPIO;ALIQ_INSS_RET;PERC_OUTROS_ABAT;'
         ||'VL_BASE_INSS_RET;VL_INSS_RET;VL_OUTROS_ABAT;IND_VL_ICMS_NO_PRECO;VL_IOF;TIPO_OPER_VEIC;'
         ||'CHASSI_VEIC;CONTA_CONTABIL;DH_INCLUSAO;STA_CODIGO;STN_CODIGO;ALIQ_PIS;QTD_BASE_PIS;VL_PIS;'
         ||'ALIQ_COFINS;QTD_BASE_COFINS;VL_COFINS;NUM_TANQUE;DT_FAB_MED;TIPO_PROD_MED;'
         ||'PARTICIPANTE_PFJ_CODIGO;CLI_CODIGO;SISS_ORIGEM;SISS_CHAVE_ORIGEM;PRES_ORIGEM;PRES_CHAVE_ORIGEM;'
         ||'TIPO_STF;VL_BASE_STF_FRONTEIRA;VL_BASE_STF_IDO;VL_STF_FRONTEIRA;VL_STF_IDO;STI_CODIGO;CNPJ_PAR;'
         ||'COD_IATA_FIM;COD_IATA_INI;CPF_PAR;DT_VAL;IE_PAR;IM_SUBCONTRATACAO;IND_ARM;IND_MED;LOTE_MED;'
         ||'NUM_ARM;NUM_CANO;UF_PAR;VL_TAB_MAX;VL_DEDUCAO_PENSAO_PRG;VL_DEDUCAO_DEPENDENTE_PRG;PERC_IRRF;'
         ||'ALIQ_PIS_ST;VL_BASE_PIS_ST;VL_IMPOSTO_PIS_ST;ALIQ_COFINS_ST;VL_BASE_COFINS_ST;'
         ||'VL_IMPOSTO_COFINS_ST;CCA_CODIGO;TIPO_RECEITA;PFJ_CODIGO_TERCEIRO;PFJ_TERCEIRO_ORIGEM;'
         ||'PFJ_TERCEIRO_CHAVE_ORIGEM;UNI_FISCAL_CODIGO;IND_SERV;DT_INI_SERV;DT_FIN_SERV;PER_FISCAL;'
         ||'COD_AREA;TERMINAL;VL_BASE_ICMS_DESC_L;VL_TRIBUTAVEL_ICMS_DESC_L;ALIQ_ICMS_DESC_L;'
         ||'VL_ICMS_DESC_L;IND_ZFM_ALC;IND_ANTECIP_ICMS;ALIQ_ANTECIP_ICMS;VL_ANTECIP_ICMS;CUSTO_ADICAO'
         ||';CUSTO_REDUCAO;IND_VL_PIS_COFINS_NO_PRECO;IND_VL_TRIB_RET_NO_PRECO;VL_PIS_ST;'
         ||'IND_ISS_RETIDO_FONTE;ALIQ_CSLL_RET;VL_BASE_CSLL_RET;ALIQ_COFINS_RET;VL_BASE_COFINS_RET;'
         ||'VL_BASE_PIS_RET;VL_CSLL_RET;ALIQ_PIS_RET;STM_CODIGO;VL_COFINS_ST;VL_PIS_RET;VL_COFINS_RET;'
         ||'IND_MOVIMENTA_ESTOQUE;PERC_TRIBUTAVEL_INSS_RET;VL_BASE_II;VL_CRED_PIS_REC_TRIB;'
         ||'VL_CRED_PIS_REC_NAO_TRIB;VL_CRED_PIS_REC_EXPO;VL_CRED_COFINS_REC_TRIB;'
         ||'VL_CRED_COFINS_REC_NAO_TRIB;VL_CRED_COFINS_REC_EXPO;COD_CCUS;PFJ_CODIGO_FORNECEDOR;'
         ||'IND_NAT_FRT_PISCOFINS;NAT_REC_PISCOFINS;NAT_REC_PISCOFINS_DESCR;IND_CPC;IND_CIAP;'
         ||'VL_IMP_FCI;FCI_NUMERO;PERC_PART_CI;NBS_CODIGO;COD_FISC_SERV_MUN;COD_BC_CREDITO;CSOSN_CODIGO;'
         ||'DESCRICAO_COMPLEMENTAR_SERVICO;VL_ABAT_LEGAL_ISS;VL_ABAT_LEGAL_INSS_RET;VL_ABAT_LEGAL_IRRF;'
         ||'DESONERACAO_CODIGO;IND_EXIGIBILIDADE_ISS;IND_INCENTIVO_FISCAL_ISS;DT_REF_CALC_IMP_IDF;'
         ||'VL_SERVICO_AE15;VL_SERVICO_AE20;VL_SERVICO_AE25;VL_ADICIONAL_RET_AE;VL_NAO_RETIDO_CP;'
         ||'CNPJ_SUBEMPREITEIRO;VL_GILRAT;VL_SENAR;IND_MP_DO_BEM;VL_ICMS_SIMPLES_NAC;'
         ||'PERC_TRIBUTAVEL_STF;CEST_CODIGO;ENQ_IPI_CODIGO;PERC_ICMS_PART_REM;VL_BASE_ICMS_PART_REM;'
         ||'ALIQ_ICMS_PART_REM;VL_ICMS_PART_REM;PERC_ICMS_PART_DEST;VL_BASE_ICMS_PART_DEST;'
         ||'ALIQ_ICMS_PART_DEST;VL_ICMS_PART_DEST;ALIQ_ICMS_FCP;ALIQ_DIFA_ICMS_PART;VL_ICMS_FCP;'
         ||'VL_RATEIO_BASE_CT_STF;VL_BASE_ICMS_FCP;VL_RATEIO_CT_STF;HASH;VL_RETIDO_SUBEMPREITADA;'
         ||'VL_NAO_RETIDO_CP_AE;CODIGO_RETENCAO_COFINS;CODIGO_RETENCAO_CSLL;CODIGO_RETENCAO_INSS;'
         ||'CODIGO_RETENCAO_PCC;CODIGO_RETENCAO_PIS;VL_IRRF_NAO_RET;VL_INSS_NAO_RET;'
         ||'ALIQ_INSS_AE15_RET;VL_INSS_AE15_RET;VL_INSS_AE15_NAO_RET;ALIQ_INSS_AE20_RET;'
         ||'VL_INSS_AE20_RET;VL_INSS_AE20_NAO_RET;ALIQ_INSS_AE25_RET;VL_INSS_AE25_RET;'
         ||'VL_INSS_AE25_NAO_RET;VL_MATERIAS_EQUIP');
      --
      -- Loop para impressao dos registros
      for m_idf in (select d.informante_est_codigo             informante_est_codigo
                         , null                                informante_pfj_chave_origem
                         , null                                informante_pfj_origem
                         , d.dof_import_numero                 dof_import_numero
                         , i.idf_num                           idf_num
                         , i.aliq_difa                         aliq_difa
                         , i.aliq_icms                         aliq_icms
                         , i.aliq_ii                           aliq_ii
                         , i.aliq_ipi                          aliq_ipi
                         , i.aliq_iss                          aliq_iss
                         , i.aliq_stf                          aliq_stf
                         , i.aliq_stt                          aliq_stt
                         , i.ord_impressao                     ord_impressao
                         , i.preco_total                       preco_total
                         , i.preco_unitario                    preco_unitario
                         , i.qtd                               qtd
                         , i.subclasse_idf                     subclasse_idf
                         , i.vl_ajuste_preco_total             vl_ajuste_preco_total
                         , i.vl_base_icms                      vl_base_icms
                         , i.vl_base_ipi                       vl_base_ipi
                         , i.vl_base_irrf                      vl_base_irrf
                         , i.vl_base_iss                       vl_base_iss
                         , i.vl_base_stf                       vl_base_stf
                         , i.vl_base_stt                       vl_base_stt
                         , i.vl_contabil                       vl_contabil
                         , i.vl_difa                           vl_difa
                         , i.vl_faturado                       vl_faturado
                         , i.vl_fiscal                         vl_fiscal
                         , i.vl_icms                           vl_icms
                         , i.vl_ii                             vl_ii
                         , i.vl_ipi                            vl_ipi
                         , i.vl_irrf                           vl_irrf
                         , i.vl_isento_icms                    vl_isento_icms
                         , i.vl_isento_ipi                     vl_isento_ipi
                         , i.vl_iss                            vl_iss
                         , i.vl_outros_icms                    vl_outros_icms
                         , i.vl_outros_ipi                     vl_outros_ipi
                         , i.vl_rateio_ajuste_preco            vl_rateio_ajuste_preco
                         , i.vl_rateio_frete                   vl_rateio_frete
                         , i.vl_rateio_oda                     vl_rateio_oda
                         , i.vl_rateio_seguro                  vl_rateio_seguro
                         , i.vl_stf                            vl_stf
                         , i.vl_stt                            vl_stt
                         , i.vl_tributavel_icms                vl_tributavel_icms
                         , i.vl_tributavel_ipi                 vl_tributavel_ipi
                         , i.vl_tributavel_stf                 vl_tributavel_stf
                         , i.vl_tributavel_stt                 vl_tributavel_stt
                         , i.am_codigo                         am_codigo
                         , i.cfop_codigo                       cfop_codigo
                         , i.dt_di                             dt_di
                         , i.entsai_uni_codigo                 entsai_uni_codigo
                         , i.fin_codigo                        fin_codigo
                         , i.idf_texto_complementar            idf_texto_complementar
                         , i.merc_codigo                       merc_codigo
                         , null                                merc_chave_origem
                         , null                                merc_origem
                         , i.mun_codigo                        mun_codigo
                         , i.nbm_codigo                        nbm_codigo
                         , i.nop_codigo                        nop_codigo
                         , i.num_di                            num_di
                         , null                                obs_fiscal_icms
                         , null                                obs_fiscal_ipi
                         , null                                obs_fiscal_irrf
                         , null                                obs_fiscal_iss
                         , null                                obs_fiscal_stf
                         , null                                obs_fiscal_stt
                         , null                                obs_fiscal_operacional
                         , i.om_codigo                         om_codigo
                         , i.peso_bruto_kg                     peso_bruto_kg
                         , i.peso_liquido_kg                   peso_liquido_kg
                         , i.pres_codigo                       pres_codigo
                         , i.siss_codigo                       siss_codigo
                         , i.stc_codigo                        stc_codigo
                         , i.stp_codigo                        stp_codigo
                         , 'M'                                 ctrl_instrucao
                         , null                                msg_critica
                         , null                                dh_critica
                         , null                                ctrl_critica
                         , i.tipo_complemento                  tipo_complemento
                         , i.vl_tributavel_difa                vl_tributavel_difa
                         , i.aliq_inss                         aliq_inss
                         , i.vl_base_inss                      vl_base_inss
                         , i.vl_inss                           vl_inss
                         , i.vl_aliq_cofins                    vl_aliq_cofins
                         , i.vl_aliq_pis                       vl_aliq_pis
                         , i.vl_base_pis                       vl_base_pis
                         , i.vl_base_cofins                    vl_base_cofins
                         , i.vl_imposto_pis                    vl_imposto_pis
                         , i.vl_imposto_cofins                 vl_imposto_cofins
                         , i.emerc_codigo                      emerc_codigo
                         , i.codigo_retencao                   codigo_retencao
                         , i.codigo_iss_municipio              codigo_iss_municipio
                         , i.aliq_inss_ret                     aliq_inss_ret
                         , i.perc_outros_abat                  perc_outros_abat
                         , i.vl_base_inss_ret                  vl_base_inss_ret
                         , i.vl_inss_ret                       vl_inss_ret
                         , i.vl_outros_abat                    vl_outros_abat
                         , i.ind_vl_icms_no_preco              ind_vl_icms_no_preco
                         , i.vl_iof                            vl_iof
                         , i.tipo_oper_veic                    tipo_oper_veic
                         , i.chassi_veic                       chassi_veic
                         , i.conta_contabil                    conta_contabil
                         , null                                dh_inclusao
                         , i.sta_codigo                        sta_codigo
                         , i.stn_codigo                        stn_codigo
                         , i.aliq_pis                          aliq_pis
                         , i.qtd_base_pis                      qtd_base_pis
                         , i.vl_pis                            vl_pis
                         , i.aliq_cofins                       aliq_cofins
                         , i.qtd_base_cofins                   qtd_base_cofins
                         , i.vl_cofins                         vl_cofins
                         , i.num_tanque                        num_tanque
                         , i.dt_fab_med                        dt_fab_med
                         , i.tipo_prod_med                     tipo_prod_med
                         , i.participante_pfj_codigo           participante_pfj_codigo
                         , i.cli_codigo                        cli_codigo
                         , null                                siss_origem
                         , null                                siss_chave_origem
                         , null                                pres_origem
                         , null                                pres_chave_origem
                         , i.tipo_stf                          tipo_stf
                         , i.vl_base_stf_fronteira             vl_base_stf_fronteira
                         , i.vl_base_stf_ido                   vl_base_stf_ido
                         , i.vl_stf_fronteira                  vl_stf_fronteira
                         , i.vl_stf_ido                        vl_stf_ido
                         , i.sti_codigo                        sti_codigo
                         , i.cnpj_par                          cnpj_par
                         , i.cod_iata_fim                      cod_iata_fim
                         , i.cod_iata_ini                      cod_iata_ini
                         , i.cpf_par                           cpf_par
                         , i.dt_val                            dt_val
                         , i.ie_par                            ie_par
                         , i.im_subcontratacao                 im_subcontratacao
                         , i.ind_arm                           ind_arm
                         , i.ind_med                           ind_med
                         , i.lote_med                          lote_med
                         , i.num_arm                           num_arm
                         , i.num_cano                          num_cano
                         , i.uf_par                            uf_par
                         , i.vl_tab_max                        vl_tab_max
                         , i.vl_deducao_pensao_prg             vl_deducao_pensao_prg
                         , i.vl_deducao_dependente_prg         vl_deducao_dependente_prg
                         , i.perc_irrf                         perc_irrf
                         , i.cca_codigo                        cca_codigo
                         , i.tipo_receita                      tipo_receita
                         , i.pfj_codigo_terceiro               pfj_codigo_terceiro
                         , null                                pfj_terceiro_origem
                         , null                                pfj_terceiro_chave_origem
                         , i.uni_fiscal_codigo                 uni_fiscal_codigo
                         , i.ind_serv                          ind_serv
                         , i.dt_ini_serv                       dt_ini_serv
                         , i.dt_fin_serv                       dt_fin_serv
                         , i.per_fiscal                        per_fiscal
                         , i.cod_area                          cod_area
                         , i.terminal                          terminal
                         , i.vl_base_icms_desc_l               vl_base_icms_desc_l
                         , i.vl_tributavel_icms_desc_l         vl_tributavel_icms_desc_l
                         , i.aliq_icms_desc_l                  aliq_icms_desc_l
                         , i.vl_icms_desc_l                    vl_icms_desc_l
                         , i.ind_zfm_alc                       ind_zfm_alc
                         , i.ind_antecip_icms                  ind_antecip_icms
                         , i.aliq_antecip_icms                 aliq_antecip_icms
                         , i.vl_antecip_icms                   vl_antecip_icms
                         , i.custo_adicao                      custo_adicao
                         , i.custo_reducao                     custo_reducao
                         , i.ind_vl_pis_cofins_no_preco        ind_vl_pis_cofins_no_preco
                         , i.stm_codigo                        stm_codigo
                         , i.ind_movimenta_estoque             ind_movimenta_estoque
                         , i.aliq_pis_st                       aliq_pis_st
                         , i.vl_base_pis_st                    vl_base_pis_st
                         , i.vl_pis_st                         vl_pis_st
                         , i.aliq_cofins_st                    aliq_cofins_st
                         , i.vl_base_cofins_st                 vl_base_cofins_st
                         , i.vl_cofins_st                      vl_cofins_st
                         , i.aliq_cofins_ret                   aliq_cofins_ret
                         , i.vl_base_cofins_ret                vl_base_cofins_ret
                         , i.vl_cofins_ret                     vl_cofins_ret
                         , i.aliq_csll_ret                     aliq_csll_ret
                         , i.vl_base_csll_ret                  vl_base_csll_ret
                         , i.vl_csll_ret                       vl_csll_ret
                         , i.aliq_pis_ret                      aliq_pis_ret
                         , i.vl_base_pis_ret                   vl_base_pis_ret
                         , i.vl_pis_ret                        vl_pis_ret
                         , i.ind_iss_retido_fonte              ind_iss_retido_fonte
                         , i.ind_vl_trib_ret_no_preco          ind_vl_trib_ret_no_preco
                         , i.perc_tributavel_inss_ret          perc_tributavel_inss_ret
                         , i.vl_base_ii                        vl_base_ii
                         , i.vl_cred_pis_rec_trib              vl_cred_pis_rec_trib
                         , i.vl_cred_pis_rec_nao_trib          vl_cred_pis_rec_nao_trib
                         , i.vl_cred_pis_rec_expo              vl_cred_pis_rec_expo
                         , i.vl_cred_cofins_rec_trib           vl_cred_cofins_rec_trib
                         , i.vl_cred_cofins_rec_nao_trib       vl_cred_cofins_rec_nao_trib
                         , i.vl_cred_cofins_rec_expo           vl_cred_cofins_rec_expo
                         , i.cod_ccus                          cod_ccus
                         , i.pfj_codigo_fornecedor             pfj_codigo_fornecedor
                         , i.ind_nat_frt_piscofins             ind_nat_frt_piscofins
                         , i.ind_movimentacao_cpc              ind_cpc
                         , i.ind_movimentacao_ciap             ind_ciap
                         , i.nat_rec_piscofins                 nat_rec_piscofins
                         , i.nat_rec_piscofins_descr           nat_rec_piscofins_descr
                         , i.cod_fisc_serv_mun                 cod_fisc_serv_mun
                         , i.csosn_codigo                      csosn_codigo
                         , i.cod_bc_credito                    cod_bc_credito
                         , i.nbs_codigo                        nbs_codigo
                         , i.perc_part_ci                      perc_part_ci
                         , i.vl_imp_fci                        vl_imp_fci
                         , i.fci_numero                        fci_numero
                         , i.descricao_complementar_servico    descricao_complementar_servico
                         , i.vl_abat_legal_iss                 vl_abat_legal_iss
                         , i.vl_abat_legal_inss_ret            vl_abat_legal_inss_ret
                         , i.vl_abat_legal_irrf                vl_abat_legal_irrf
                         , i.desoneracao_codigo                desoneracao_codigo
                         , i.ind_exigibilidade_iss             ind_exigibilidade_iss
                         , i.ind_incentivo_fiscal_iss          ind_incentivo_fiscal_iss
                         , i.dt_ref_calc_imp_idf               dt_ref_calc_imp_idf
                         , i.vl_servico_ae15                   vl_servico_ae15
                         , i.vl_servico_ae20                   vl_servico_ae20
                         , i.vl_servico_ae25                   vl_servico_ae25
                         , i.vl_adicional_ret_ae               vl_adicional_ret_ae
                         , i.vl_nao_retido_cp                  vl_nao_retido_cp
                         , i.cnpj_subempreiteiro               cnpj_subempreiteiro
                         , i.vl_gilrat                         vl_gilrat
                         , i.vl_senar                          vl_senar
                         , i.ind_mp_do_bem                     ind_mp_do_bem
                         , i.vl_icms_simples_nac               vl_icms_simples_nac
                         , i.perc_tributavel_stf               perc_tributavel_stf
                         , i.vl_rateio_base_ct_stf             vl_rateio_base_ct_stf
                         , i.vl_rateio_ct_stf                  vl_rateio_ct_stf
                         , i.cest_codigo                       cest_codigo
                         , i.perc_icms_part_rem                perc_icms_part_rem
                         , i.vl_base_icms_part_rem             vl_base_icms_part_rem
                         , i.aliq_icms_part_rem                aliq_icms_part_rem
                         , i.vl_icms_part_rem                  vl_icms_part_rem
                         , i.perc_icms_part_dest               perc_icms_part_dest
                         , i.vl_base_icms_part_dest            vl_base_icms_part_dest
                         , i.aliq_icms_part_dest               aliq_icms_part_dest
                         , i.vl_icms_part_dest                 vl_icms_part_dest
                         , i.aliq_icms_fcp                     aliq_icms_fcp
                         , i.vl_icms_fcp                       vl_icms_fcp
                         , i.vl_base_icms_fcp                  vl_base_icms_fcp
                         , i.aliq_difa_icms_part               aliq_difa_icms_part
                         , i.enq_ipi_codigo                    enq_ipi_codigo
                         , null                                hash
                         , i.vl_retido_subempreitada           vl_retido_subempreitada
                         , i.vl_nao_retido_cp_ae               vl_nao_retido_cp_ae
                         , i.codigo_retencao_cofins            codigo_retencao_cofins
                         , i.codigo_retencao_csll              codigo_retencao_csll
                         , i.codigo_retencao_inss              codigo_retencao_inss
                         , i.codigo_retencao_pcc               codigo_retencao_pcc
                         , i.codigo_retencao_pis               codigo_retencao_pis
                         , i.vl_irrf_nao_ret                   vl_irrf_nao_ret
                         , i.vl_inss_nao_ret                   vl_inss_nao_ret
                         , i.aliq_inss_ae15_ret                aliq_inss_ae15_ret
                         , i.vl_inss_ae15_ret                  vl_inss_ae15_ret
                         , i.vl_inss_ae15_nao_ret              vl_inss_ae15_nao_ret
                         , i.aliq_inss_ae20_ret                aliq_inss_ae20_ret
                         , i.vl_inss_ae20_ret                  vl_inss_ae20_ret
                         , i.vl_inss_ae20_nao_ret              vl_inss_ae20_nao_ret
                         , i.aliq_inss_ae25_ret                aliq_inss_ae25_ret
                         , i.vl_inss_ae25_ret                  vl_inss_ae25_ret
                         , i.vl_inss_ae25_nao_ret              vl_inss_ae25_nao_ret
                         , i.vl_materias_equip                 vl_materias_equip
                         , i.vl_pis_st                         vl_imposto_pis_st
                         , i.vl_cofins_st                      vl_imposto_cofins_st
                      from cor_idf i
                      join cor_dof d
                        on d.codigo_do_site = i.codigo_do_site
                       and d.dof_sequence   = i.dof_sequence
                     where d.informante_est_codigo like p_est_codigo
                       and d.dh_emissao between p_dt_inicio
                                            and p_dt_fim
                       and i.siss_codigo like p_siss_codigo
                       and i.subclasse_idf like p_subclasse_idf)
      loop
        m_linha := m_idf.INFORMANTE_EST_CODIGO
           ||';'|| m_idf.INFORMANTE_PFJ_CHAVE_ORIGEM
           ||';'|| m_idf.INFORMANTE_PFJ_ORIGEM
           ||';'|| m_idf.DOF_IMPORT_NUMERO
           ||';'|| m_idf.IDF_NUM
           ||';'|| m_idf.ALIQ_DIFA
           ||';'|| m_idf.ALIQ_ICMS
           ||';'|| m_idf.ALIQ_II
           ||';'|| m_idf.ALIQ_IPI
           ||';'|| m_idf.ALIQ_ISS
           ||';'|| m_idf.ALIQ_STF
           ||';'|| m_idf.ALIQ_STT
           ||';'|| m_idf.ORD_IMPRESSAO
           ||';'|| m_idf.PRECO_TOTAL
           ||';'|| m_idf.PRECO_UNITARIO
           ||';'|| m_idf.QTD
           ||';'|| m_idf.SUBCLASSE_IDF
           ||';'|| m_idf.VL_AJUSTE_PRECO_TOTAL
           ||';'|| m_idf.VL_BASE_ICMS
           ||';'|| m_idf.VL_BASE_IPI
           ||';'|| m_idf.VL_BASE_IRRF
           ||';'|| m_idf.VL_BASE_ISS
           ||';'|| m_idf.VL_BASE_STF
           ||';'|| m_idf.VL_BASE_STT
           ||';'|| m_idf.VL_CONTABIL
           ||';'|| m_idf.VL_DIFA
           ||';'|| m_idf.VL_FATURADO
           ||';'|| m_idf.VL_FISCAL
           ||';'|| m_idf.VL_ICMS
           ||';'|| m_idf.VL_II
           ||';'|| m_idf.VL_IPI
           ||';'|| m_idf.VL_IRRF
           ||';'|| m_idf.VL_ISENTO_ICMS
           ||';'|| m_idf.VL_ISENTO_IPI
           ||';'|| m_idf.VL_ISS
           ||';'|| m_idf.VL_OUTROS_ICMS
           ||';'|| m_idf.VL_OUTROS_IPI
           ||';'|| m_idf.VL_RATEIO_AJUSTE_PRECO
           ||';'|| m_idf.VL_RATEIO_FRETE
           ||';'|| m_idf.VL_RATEIO_ODA
           ||';'|| m_idf.VL_RATEIO_SEGURO
           ||';'|| m_idf.VL_STF
           ||';'|| m_idf.VL_STT
           ||';'|| m_idf.VL_TRIBUTAVEL_ICMS
           ||';'|| m_idf.VL_TRIBUTAVEL_IPI
           ||';'|| m_idf.VL_TRIBUTAVEL_STF
           ||';'|| m_idf.VL_TRIBUTAVEL_STT
           ||';'|| m_idf.AM_CODIGO
           ||';'|| m_idf.CFOP_CODIGO
           ||';'|| m_idf.DT_DI
           ||';'|| m_idf.ENTSAI_UNI_CODIGO
           ||';'|| m_idf.FIN_CODIGO
           ||';'|| m_idf.IDF_TEXTO_COMPLEMENTAR
           ||';'|| m_idf.MERC_CODIGO
           ||';'|| m_idf.MERC_CHAVE_ORIGEM
           ||';'|| m_idf.MERC_ORIGEM
           ||';'|| m_idf.MUN_CODIGO
           ||';'|| m_idf.NBM_CODIGO
           ||';'|| m_idf.NOP_CODIGO
           ||';'|| m_idf.NUM_DI
           ||';'|| m_idf.OBS_FISCAL_ICMS
           ||';'|| m_idf.OBS_FISCAL_IPI
           ||';'|| m_idf.OBS_FISCAL_IRRF
           ||';'|| m_idf.OBS_FISCAL_ISS
           ||';'|| m_idf.OBS_FISCAL_STF
           ||';'|| m_idf.OBS_FISCAL_STT
           ||';'|| m_idf.OBS_FISCAL_OPERACIONAL
           ||';'|| m_idf.OM_CODIGO
           ||';'|| m_idf.PESO_BRUTO_KG
           ||';'|| m_idf.PESO_LIQUIDO_KG
           ||';'|| m_idf.PRES_CODIGO
           ||';'|| m_idf.SISS_CODIGO
           ||';'|| m_idf.STC_CODIGO
           ||';'|| m_idf.STP_CODIGO
           ||';'|| m_idf.CTRL_INSTRUCAO
           ||';'|| m_idf.MSG_CRITICA
           ||';'|| m_idf.DH_CRITICA
           ||';'|| m_idf.CTRL_CRITICA
           ||';'|| m_idf.TIPO_COMPLEMENTO
           ||';'|| m_idf.VL_TRIBUTAVEL_DIFA
           ||';'|| m_idf.ALIQ_INSS
           ||';'|| m_idf.VL_BASE_INSS
           ||';'|| m_idf.VL_INSS
           ||';'|| m_idf.VL_ALIQ_COFINS
           ||';'|| m_idf.VL_ALIQ_PIS
           ||';'|| m_idf.VL_BASE_PIS
           ||';'|| m_idf.VL_BASE_COFINS
           ||';'|| m_idf.VL_IMPOSTO_PIS
           ||';'|| m_idf.VL_IMPOSTO_COFINS
           ||';'|| m_idf.EMERC_CODIGO
           ||';'|| m_idf.CODIGO_RETENCAO
           ||';'|| m_idf.CODIGO_ISS_MUNICIPIO
           ||';'|| m_idf.ALIQ_INSS_RET
           ||';'|| m_idf.PERC_OUTROS_ABAT
           ||';'|| m_idf.VL_BASE_INSS_RET
           ||';'|| m_idf.VL_INSS_RET
           ||';'|| m_idf.VL_OUTROS_ABAT
           ||';'|| m_idf.IND_VL_ICMS_NO_PRECO
           ||';'|| m_idf.VL_IOF
           ||';'|| m_idf.TIPO_OPER_VEIC
           ||';'|| m_idf.CHASSI_VEIC
           ||';'|| m_idf.CONTA_CONTABIL
           ||';'|| m_idf.DH_INCLUSAO
           ||';'|| m_idf.STA_CODIGO
           ||';'|| m_idf.STN_CODIGO
           ||';'|| m_idf.ALIQ_PIS
           ||';'|| m_idf.QTD_BASE_PIS
           ||';'|| m_idf.VL_PIS
           ||';'|| m_idf.ALIQ_COFINS
           ||';'|| m_idf.QTD_BASE_COFINS
           ||';'|| m_idf.VL_COFINS
           ||';'|| m_idf.NUM_TANQUE
           ||';'|| m_idf.DT_FAB_MED
           ||';'|| m_idf.TIPO_PROD_MED
           ||';'|| m_idf.PARTICIPANTE_PFJ_CODIGO
           ||';'|| m_idf.CLI_CODIGO
           ||';'|| m_idf.SISS_ORIGEM
           ||';'|| m_idf.SISS_CHAVE_ORIGEM
           ||';'|| m_idf.PRES_ORIGEM
           ||';'|| m_idf.PRES_CHAVE_ORIGEM
           ||';'|| m_idf.TIPO_STF
           ||';'|| m_idf.VL_BASE_STF_FRONTEIRA
           ||';'|| m_idf.VL_BASE_STF_IDO
           ||';'|| m_idf.VL_STF_FRONTEIRA
           ||';'|| m_idf.VL_STF_IDO
           ||';'|| m_idf.STI_CODIGO
           ||';'|| m_idf.CNPJ_PAR
           ||';'|| m_idf.COD_IATA_FIM
           ||';'|| m_idf.COD_IATA_INI
           ||';'|| m_idf.CPF_PAR
           ||';'|| m_idf.DT_VAL
           ||';'|| m_idf.IE_PAR
           ||';'|| m_idf.IM_SUBCONTRATACAO
           ||';'|| m_idf.IND_ARM
           ||';'|| m_idf.IND_MED
           ||';'|| m_idf.LOTE_MED
           ||';'|| m_idf.NUM_ARM
           ||';'|| m_idf.NUM_CANO
           ||';'|| m_idf.UF_PAR
           ||';'|| m_idf.VL_TAB_MAX
           ||';'|| m_idf.VL_DEDUCAO_PENSAO_PRG
           ||';'|| m_idf.VL_DEDUCAO_DEPENDENTE_PRG
           ||';'|| m_idf.PERC_IRRF
           ||';'|| m_idf.ALIQ_PIS_ST
           ||';'|| m_idf.VL_BASE_PIS_ST
           ||';'|| m_idf.VL_IMPOSTO_PIS_ST
           ||';'|| m_idf.ALIQ_COFINS_ST
           ||';'|| m_idf.VL_BASE_COFINS_ST
           ||';'|| m_idf.VL_IMPOSTO_COFINS_ST
           ||';'|| m_idf.CCA_CODIGO
           ||';'|| m_idf.TIPO_RECEITA
           ||';'|| m_idf.PFJ_CODIGO_TERCEIRO
           ||';'|| m_idf.PFJ_TERCEIRO_ORIGEM
           ||';'|| m_idf.PFJ_TERCEIRO_CHAVE_ORIGEM
           ||';'|| m_idf.UNI_FISCAL_CODIGO
           ||';'|| m_idf.IND_SERV
           ||';'|| m_idf.DT_INI_SERV
           ||';'|| m_idf.DT_FIN_SERV
           ||';'|| m_idf.PER_FISCAL
           ||';'|| m_idf.COD_AREA
           ||';'|| m_idf.TERMINAL
           ||';'|| m_idf.VL_BASE_ICMS_DESC_L
           ||';'|| m_idf.VL_TRIBUTAVEL_ICMS_DESC_L
           ||';'|| m_idf.ALIQ_ICMS_DESC_L
           ||';'|| m_idf.VL_ICMS_DESC_L
           ||';'|| m_idf.IND_ZFM_ALC
           ||';'|| m_idf.IND_ANTECIP_ICMS
           ||';'|| m_idf.ALIQ_ANTECIP_ICMS
           ||';'|| m_idf.VL_ANTECIP_ICMS
           ||';'|| m_idf.CUSTO_ADICAO
           ||';'|| m_idf.CUSTO_REDUCAO
           ||';'|| m_idf.IND_VL_PIS_COFINS_NO_PRECO
           ||';'|| m_idf.IND_VL_TRIB_RET_NO_PRECO
           ||';'|| m_idf.VL_PIS_ST
           ||';'|| m_idf.IND_ISS_RETIDO_FONTE
           ||';'|| m_idf.ALIQ_CSLL_RET
           ||';'|| m_idf.VL_BASE_CSLL_RET
           ||';'|| m_idf.ALIQ_COFINS_RET
           ||';'|| m_idf.VL_BASE_COFINS_RET
           ||';'|| m_idf.VL_BASE_PIS_RET
           ||';'|| m_idf.VL_CSLL_RET
           ||';'|| m_idf.ALIQ_PIS_RET
           ||';'|| m_idf.STM_CODIGO
           ||';'|| m_idf.VL_COFINS_ST
           ||';'|| m_idf.VL_PIS_RET
           ||';'|| m_idf.VL_COFINS_RET
           ||';'|| m_idf.IND_MOVIMENTA_ESTOQUE
           ||';'|| m_idf.PERC_TRIBUTAVEL_INSS_RET
           ||';'|| m_idf.VL_BASE_II
           ||';'|| m_idf.VL_CRED_PIS_REC_TRIB
           ||';'|| m_idf.VL_CRED_PIS_REC_NAO_TRIB
           ||';'|| m_idf.VL_CRED_PIS_REC_EXPO
           ||';'|| m_idf.VL_CRED_COFINS_REC_TRIB
           ||';'|| m_idf.VL_CRED_COFINS_REC_NAO_TRIB
           ||';'|| m_idf.VL_CRED_COFINS_REC_EXPO
           ||';'|| m_idf.COD_CCUS
           ||';'|| m_idf.PFJ_CODIGO_FORNECEDOR
           ||';'|| m_idf.IND_NAT_FRT_PISCOFINS
           ||';'|| m_idf.NAT_REC_PISCOFINS
           ||';'|| m_idf.NAT_REC_PISCOFINS_DESCR
           ||';'|| m_idf.IND_CPC
           ||';'|| m_idf.IND_CIAP
           ||';'|| m_idf.VL_IMP_FCI
           ||';'|| m_idf.FCI_NUMERO
           ||';'|| m_idf.PERC_PART_CI
           ||';'|| m_idf.NBS_CODIGO
           ||';'|| m_idf.COD_FISC_SERV_MUN
           ||';'|| m_idf.COD_BC_CREDITO
           ||';'|| m_idf.CSOSN_CODIGO
           ||';'|| m_idf.DESCRICAO_COMPLEMENTAR_SERVICO
           ||';'|| m_idf.VL_ABAT_LEGAL_ISS
           ||';'|| m_idf.VL_ABAT_LEGAL_INSS_RET
           ||';'|| m_idf.VL_ABAT_LEGAL_IRRF
           ||';'|| m_idf.DESONERACAO_CODIGO
           ||';'|| m_idf.IND_EXIGIBILIDADE_ISS
           ||';'|| m_idf.IND_INCENTIVO_FISCAL_ISS
           ||';'|| m_idf.DT_REF_CALC_IMP_IDF
           ||';'|| m_idf.VL_SERVICO_AE15
           ||';'|| m_idf.VL_SERVICO_AE20
           ||';'|| m_idf.VL_SERVICO_AE25
           ||';'|| m_idf.VL_ADICIONAL_RET_AE
           ||';'|| m_idf.VL_NAO_RETIDO_CP
           ||';'|| m_idf.CNPJ_SUBEMPREITEIRO
           ||';'|| m_idf.VL_GILRAT
           ||';'|| m_idf.VL_SENAR
           ||';'|| m_idf.IND_MP_DO_BEM
           ||';'|| m_idf.VL_ICMS_SIMPLES_NAC
           ||';'|| m_idf.PERC_TRIBUTAVEL_STF
           ||';'|| m_idf.CEST_CODIGO
           ||';'|| m_idf.ENQ_IPI_CODIGO
           ||';'|| m_idf.PERC_ICMS_PART_REM
           ||';'|| m_idf.VL_BASE_ICMS_PART_REM
           ||';'|| m_idf.ALIQ_ICMS_PART_REM
           ||';'|| m_idf.VL_ICMS_PART_REM
           ||';'|| m_idf.PERC_ICMS_PART_DEST
           ||';'|| m_idf.VL_BASE_ICMS_PART_DEST
           ||';'|| m_idf.ALIQ_ICMS_PART_DEST
           ||';'|| m_idf.VL_ICMS_PART_DEST
           ||';'|| m_idf.ALIQ_ICMS_FCP
           ||';'|| m_idf.ALIQ_DIFA_ICMS_PART
           ||';'|| m_idf.VL_ICMS_FCP
           ||';'|| m_idf.VL_RATEIO_BASE_CT_STF
           ||';'|| m_idf.VL_BASE_ICMS_FCP
           ||';'|| m_idf.VL_RATEIO_CT_STF
           ||';'|| m_idf.HASH
           ||';'|| m_idf.VL_RETIDO_SUBEMPREITADA
           ||';'|| m_idf.VL_NAO_RETIDO_CP_AE
           ||';'|| m_idf.CODIGO_RETENCAO_COFINS
           ||';'|| m_idf.CODIGO_RETENCAO_CSLL
           ||';'|| m_idf.CODIGO_RETENCAO_INSS
           ||';'|| m_idf.CODIGO_RETENCAO_PCC
           ||';'|| m_idf.CODIGO_RETENCAO_PIS
           ||';'|| m_idf.VL_IRRF_NAO_RET
           ||';'|| m_idf.VL_INSS_NAO_RET
           ||';'|| m_idf.ALIQ_INSS_AE15_RET
           ||';'|| m_idf.VL_INSS_AE15_RET
           ||';'|| m_idf.VL_INSS_AE15_NAO_RET
           ||';'|| m_idf.ALIQ_INSS_AE20_RET
           ||';'|| m_idf.VL_INSS_AE20_RET
           ||';'|| m_idf.VL_INSS_AE20_NAO_RET
           ||';'|| m_idf.ALIQ_INSS_AE25_RET
           ||';'|| m_idf.VL_INSS_AE25_RET
           ||';'|| m_idf.VL_INSS_AE25_NAO_RET
           ||';'|| m_idf.VL_MATERIAS_EQUIP;
		    --
        imp(m_linha);
        --
      end loop;
    end gera_synitf_idf;
    --
    procedure gera_synitf_idf_constr_civil is
      m_linha varchar2(2000);
    begin
      -- Imprime cabecalho
      imp ('SYNITF_IDF_CONSTRUCAO_CIVIL');
      imp ('DOF_IMPORT_NUMERO;INFORMANTE_EST_CODIGO;INFORMANTE_PFJ_CHAVE_ORIGEM;'
         ||'INFORMANTE_PFJ_ORIGEM;IDF_NUM;IDENTIFICACAO_OBRA;DT_INICIO_OBRA;'
         ||'CNPJ_CPF_PROP_OBRA;NOME_PROP_OBRA;ENDERECO;NUMERO;COMPLEMENTO;CEP;BAIRRO;'
         ||'MUN_CODIGO;UF_CODIGO;PAIS_CODIGO;MSG_CRITICA;DH_CRITICA;CTRL_CRITICA;'
         ||'DH_INCLUSAO;CTRL_INSTRUCAO;CNO_CODIGO;IND_OBRA');
      --
      -- Loop para impressao dos registros
      for m_icc in (select d.dof_import_numero            dof_import_numero
                         , d.informante_est_codigo        informante_est_codigo
                         , null                           informante_pfj_chave_origem
                         , null                           informante_pfj_origem
                         , c.idf_num                      idf_num
                         , c.identificacao_obra           identificacao_obra
                         , c.dt_inicio_obra               dt_inicio_obra
                         , c.cnpj_cpf_prop_obra           cnpj_cpf_prop_obra
                         , c.nome_prop_obra               nome_prop_obra
                         , c.endereco                     endereco
                         , c.numero                       numero
                         , c.complemento                  complemento
                         , c.cep                          cep
                         , c.bairro                       bairro
                         , c.mun_codigo                   mun_codigo
                         , c.uf_codigo                    uf_codigo
                         , c.pais_codigo                  pais_codigo
                         , null                           msg_critica
                         , null                           dh_critica
                         , null                           ctrl_critica
                         , null                           dh_inclusao
                         , 'M'                            ctrl_instrucao
                         , c.cno_codigo                   cno_codigo
                         , c.ind_obra                     ind_obra
                      from cor_dof          d
                      join cor_idf          i
                        on d.codigo_do_site = i.codigo_do_site
                       and d.dof_sequence   = i.dof_sequence
                      left join cor_idf_construcao_civil c 
                        on d.codigo_do_site = c.codigo_do_site
                       and d.dof_sequence   = c.dof_sequence
                     where d.informante_est_codigo like p_est_codigo
                       and d.dh_emissao between p_dt_inicio
                                            and p_dt_fim
                       and i.siss_codigo like p_siss_codigo
                       and i.subclasse_idf like p_subclasse_idf
                     order by 1,2)
      loop
        m_linha := m_icc.dof_import_numero
           ||';'|| m_icc.informante_est_codigo
           ||';'|| m_icc.informante_pfj_chave_origem
           ||';'|| m_icc.informante_pfj_origem
           ||';'|| m_icc.idf_num
           ||';'|| m_icc.identificacao_obra
           ||';'|| m_icc.dt_inicio_obra
           ||';'|| m_icc.cnpj_cpf_prop_obra
           ||';'|| m_icc.nome_prop_obra
           ||';'|| m_icc.endereco
           ||';'|| m_icc.numero
           ||';'|| m_icc.complemento
           ||';'|| m_icc.cep
           ||';'|| m_icc.bairro
           ||';'|| m_icc.mun_codigo
           ||';'|| m_icc.uf_codigo
           ||';'|| m_icc.pais_codigo
           ||';'|| m_icc.msg_critica
           ||';'|| m_icc.dh_critica
           ||';'|| m_icc.ctrl_critica
           ||';'|| m_icc.dh_inclusao
           ||';'|| m_icc.ctrl_instrucao
           ||';'|| m_icc.cno_codigo
           ||';'|| m_icc.ind_obra;
        --
        imp(m_linha);
        --
      end loop;

    end gera_synitf_idf_constr_civil;
    --
  begin
    syn_out.inicializa(p_proc_id);
    --
    -- Checar conteudo do campo p_tipo_synitf
    case p_tipo_synitf
      when 'SYNITF_PROCESSO'             then gera_synitf_processo;
      when 'SYNITF_DOF_PROCESSO'         then gera_synitf_dof_processo;
      when 'SYNITF_IDF'                  then gera_synitf_idf;
      when 'SYNITF_IDF_CONSTRUCAO_CIVIL' then gera_synitf_idf_constr_civil;
      else
        raise_application_error(-20202,'O parametro "Tipo de Interface" est? preenchido com um valor invalido');
    end case;
    --
  end exporta_synitf;
  --
  procedure relatorio_conferencia (p_est_codigo    varchar2
                                  ,p_dt_inicio     date
                                  ,p_dt_fim        date
                                  ,p_siss_codigo   varchar2
                                  ,p_subclasse_idf varchar2
                                  ,p_proc_id       number) is
    --
    cColFinal constant integer := 446; 
    --
    m_header     varchar2(10000);
    m_linha      varchar2(1000);
    m_est_nome   cor_pessoa.mnemonico%type;
    m_tipo_nome  varchar2(20);
    m_serv_nome  cor_servico_iss.nome%type;
    m_pfj_codigo cor_pessoa.pfj_codigo%type;
    m_pfj_nome   cor_pessoa.mnemonico%type;
    --
    procedure imp(p_texto varchar2) is
    begin
      syn_out.put_line(p_texto);
    end imp;
    --
  begin
    -- Checa estabelecimento
    begin
      select p.mnemonico
        into m_est_nome
        from cor_pessoa p
       where p.pfj_codigo = p_est_codigo
         and exists (select 1
                       from cor_estab_usuario e
                      where e.est_codigo = p.pfj_codigo);
    exception when no_data_found then
      raise_application_error(-20202,'O estabelecimento ' || p_est_codigo || ' nao existe ou nao esta autorizado para este usuario');
    end;
    --
    -- Carrega o servico
    if p_siss_codigo = '%' then
      m_serv_nome := 'Todos';
    else
      begin
        select s.nome
          into m_serv_nome
          from cor_servico_iss s
         where s.siss_codigo = p_siss_codigo;
      exception when no_data_found then
        raise_application_error(-20202,'O servico ' || p_siss_codigo || ' nao esta cadastrado');
      end;
    end if;
    --
    -- Checa tipo
    case p_subclasse_idf
      when '%' then m_tipo_nome := '% - Todos';
      when 'S' then m_tipo_nome := 'S - Servicos';
      when 'M' then m_tipo_nome := 'M - Mercadorias';
      when 'P' then m_tipo_nome := 'P - Prestacoes';
      when 'O' then m_tipo_nome := 'O - Outros';
      else raise_application_error(-20202,'Tipo Invalido');
    end case;
    --
    syn_out.inicializa(p_proc_id,100,cColFinal);
    --
    -- Gera cabecalho
    m_header := ' +' || rpad('-',cColFinal-3,'-') || '+';
    --
    m_header := m_header || '/n' || syn_str.At_Say_Right(' |','|',cColFinal);
    --
    m_linha := syn_str.At_Say_Center(' |','Relatorio de Conferencia de dados do REINF',cColFinal);
    m_linha := syn_str.At_Say_Right(m_linha,'Gerado em: ' || to_char(sysdate,'dd/mm/yyyy hh24:mi') || ' |',cColFinal);
    m_header := m_header || '/n' || m_linha;
    --
    m_header := m_header || '/n' || syn_str.At_Say_Right(' |','|',cColFinal);
    --
    m_linha := ' | Estabelecimento : ' || p_est_codigo || ' - ' || m_est_nome;
    m_linha := syn_str.At_Say_Right(m_linha,'Periodo : ' || to_char(p_dt_inicio,'dd/mm/yyyy') || ' a ' || to_char(p_dt_fim,'dd/mm/yyyy') || ' |',cColFinal);
    m_header := m_header || '/n' || m_linha;
    --
    m_linha := ' | Servico         : ' || p_siss_codigo || ' - ' || m_serv_nome;
    m_linha := syn_str.At_Say_Right(m_linha,'Tipo de item : ' || m_tipo_nome || ' |',cColFinal);
    m_header := m_header || '/n' || m_linha;
    --
    m_header := m_header || '/n' || syn_str.At_Say_Right(' |','|',cColFinal);
    --
    m_header := m_header || '/n' ||' +' || rpad('-',cColFinal-3,'-') || '+';
    --
    m_header := m_header || '/n' || syn_str.At_Say_Right(' |','|',cColFinal);
    --
    m_linha := ' |     NUMERO |                        DOF_IMPORT_NUMERO | DH_EMISSAO | DT_FATO_GER'
            --   |  999999999 | 1234567890123456789012345678901234567890 | DD/MM/YYYY |  DD/MM/YYYY
            || ' |  EMIT (E) / DEST (S) |                                     NOME EMIT (E) / DEST (S)'
            --   | 12345678901234567890 | 123456789012345678901234567890123456789012345678901234567890
            || ' |  CLASSIF_COD |                    SISS_CODIGO |          VL_INSS |   VL_INSS_RETIDO'
            --   |    999999999 | 123456789012345678901234567890 |   999.999.990,00 |   999.999.990,00
            || ' | VL_NAO_RET_CP_AE |    VL_NAO_RET_CP |   VL_RET_SUBEMPR'
            --   |   999.999.990,00 |   999.999.990,00 |   999.999.990,00
            || ' |     VL_SERV_AE15 |     VL_SERV_AE20 |     VL_SERV_AE25 |   VL_ADIC_RET_AE'
            --   |   999.999.990,00 |   999.999.990,00 |   999.999.990,00 |   999.999.990,00 
            || ' | IND_OBRA |    NUM_PROCESSO | TIPO_PROC |  SUSPENS_COD |';
            --   |        9 | 999999999999999 |         A | 999999999999 |
            --   
    m_header := m_header || '/n' || m_linha;
    --
    m_header := m_header || '/n' || syn_str.At_Say_Right(' |','|',cColFinal);
    --
    --m_header := m_header || '/n' ||' +' || rpad('-',cColFinal-3,'-') || '+';
    --
    syn_processo.Grava_Cabecalho(p_proc_id, m_header);
    --
    for m_rel in (select d.numero                         numero_nf
                       , trunc(d.dh_emissao)              dh_emissao
                       , trunc(d.dt_fato_gerador_imposto) dt_fato_gerador_imposto 
                       , d.dof_import_numero              dof_import_numero
                       , d.ind_entrada_saida              ind_entrada_saida
                       , emi.pfj_codigo                   cod_emitente
                       , emi.mnemonico                    nome_emitente
                       , rem.pfj_codigo                   cod_remetente
                       , rem.mnemonico                    nome_remetente
                       , dest.pfj_codigo                  cod_destinatario
                       , dest.mnemonico                   nome_destinatario
                       , dp.num_proc                      numero_processo
                       , p.tp_processo                    tp_processo
                       , i.siss_codigo                    siss_codigo
                       , icc.ind_obra                     ind_obra
                       , s.classif_codigo                 classif_codigo
                       , pt.suspensao_codigo              suspensao_codigo
                       , sum(i.vl_inss)                   vl_inss
                       , sum(i.vl_inss_ret)               vl_inss_ret
                       , sum(i.vl_nao_retido_cp_ae)       vl_nao_retido_cp_ae
                       , sum(i.vl_nao_retido_cp)          vl_nao_retido_cp
                       , sum(i.vl_retido_subempreitada)   vl_retido_subempreitada
                       , sum(i.vl_servico_ae15)           vl_servico_ae15
                       , sum(i.vl_servico_ae20)           vl_servico_ae20
                       , sum(i.vl_servico_ae25)           vl_servico_ae25
                       , sum(i.vl_adicional_ret_ae)       vl_adicional_ret_ae
                    from cor_dof d
                    join cor_idf i
                      on i.codigo_do_site = d.codigo_do_site
                     and i.dof_sequence   = d.dof_sequence
                    join cor_pessoa emi
                      on emi.pfj_codigo = d.emitente_pfj_codigo  
                    join cor_pessoa rem
                      on rem.pfj_codigo = d.remetente_pfj_codigo 
                    join cor_pessoa dest
                      on dest.pfj_codigo = d.destinatario_pfj_codigo 
                    left join cor_dof_processo dp
                      on dp.codigo_do_site = d.codigo_do_site
                     and dp.dof_sequence   = d.dof_sequence
                    left join cor_idf_construcao_civil icc
                      on icc.codigo_do_site = i.codigo_do_site
                     and icc.dof_sequence   = i.dof_sequence
                     and icc.idf_num        = i.idf_num
                    left join cor_servico_iss s
                      on s.siss_codigo = i.siss_codigo
                    left join cor_processo p
                      on p.numero = dp.num_proc
                    left join cor_processo_tributo pt
                      on pt.processo_id = p.id
                   where d.informante_est_codigo like p_est_codigo
                     and d.dh_emissao between p_dt_inicio
                                          and p_dt_fim
                     and i.siss_codigo like p_siss_codigo
                     and i.subclasse_idf like p_subclasse_idf
                   group by d.numero                       
                       , trunc(d.dh_emissao)            
                       , trunc(d.dt_fato_gerador_imposto) 
                       , d.dof_import_numero            
                       , d.ind_entrada_saida            
                       , emi.pfj_codigo                 
                       , emi.mnemonico                  
                       , rem.pfj_codigo                 
                       , rem.mnemonico                  
                       , dest.pfj_codigo                
                       , dest.mnemonico                 
                       , dp.num_proc                    
                       , p.tp_processo                  
                       , i.siss_codigo                  
                       , icc.ind_obra                   
                       , s.classif_codigo               
                       , pt.suspensao_codigo
                   order by 1,2,3)
    loop
      --
      if m_rel.ind_entrada_saida = 'E' then
        m_pfj_codigo := m_rel.cod_emitente;
        m_pfj_nome   := m_rel.nome_emitente;
      else
        m_pfj_codigo := m_rel.cod_destinatario;
        m_pfj_nome   := m_rel.nome_destinatario;
      end if;
      --
      m_linha := ' | ' || lpad(m_rel.numero_nf,10,' ')
              || ' | ' || lpad(m_rel.dof_import_numero,40,' ')
              || ' | ' || to_char(m_rel.dh_emissao,'DD/MM/YYYY')
              || ' | ' || ' ' || to_char(m_rel.dt_fato_gerador_imposto,'DD/MM/YYYY')
              || ' | ' || lpad(m_pfj_codigo,20,' ')
              || ' | ' || lpad(m_pfj_nome,60,' ')
              || ' | ' || lpad(nvl(to_char(m_rel.classif_codigo),' '),12,' ')
              || ' | ' || lpad(nvl(m_rel.siss_codigo,' '),30,' ')
              || ' | ' || to_char(nvl(m_rel.vl_inss,0)                ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_inss_ret,0)            ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_nao_retido_cp_ae,0)    ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_nao_retido_cp,0)       ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_retido_subempreitada,0),'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_servico_ae15,0)        ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_servico_ae20,0)        ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_servico_ae25,0)        ,'9999G999G990D00')
              || ' | ' || to_char(nvl(m_rel.vl_adicional_ret_ae,0)    ,'9999G999G990D00')
              || ' | ' || lpad(nvl(m_rel.ind_obra,' '),8,' ')
              || ' | ' || lpad(nvl(to_char(m_rel.numero_processo),' '),15,' ')
              || ' | ' || lpad(nvl(m_rel.tp_processo,' '),9,' ')
              || ' | ' || lpad(nvl(to_char(m_rel.suspensao_codigo) ,' '),12,' ')
              || ' |';
      --
      syn_out.put_line(m_linha);
      --
    end loop;
    --
    syn_out.put_line(syn_str.At_Say_Right(' |','|',cColFinal));
    syn_out.put_line(' +' || rpad('-',cColFinal-3,'-') || '+');
    --
  end relatorio_conferencia;
  --
end HON_REINF_GAP_EXPORT;
/
