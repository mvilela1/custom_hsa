﻿--
-- Importando Definicao 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA' ...
--
DECLARE
  mid syn_prc_definicao.id%type;
  mprcdef_prcdef_id syn_prc_definicao.prcdef_id%type := null;
  mprcdef_frm_id syn_prc_definicao.frm_id%type := null;
BEGIN
  Select id into mprcdef_prcdef_id from syn_prc_definicao where codigo = 'UTI\ITF\PRC\PRC\';
Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo c�digo antigo para que o    *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
  Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA';
  Update SYN_PRC_DEFINICAO set
  CODIGO              = 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA'
  ,NOME                = 'Relatorio de conferencia de dados do REINF'
  ,TITULO              = 'Relatorio de conferencia de dados do REINF'
  ,FUN_TIPO            = 'F_FUNC'
  ,PRCDEF_ID           = mprcdef_prcdef_id
  ,NIVEL               = 5
  ,EXECUTAVEL_NOME     = ''
  ,ROTINA_DE_GERACAO   = 'HON_REINF_GAP_EXPORT.RELATORIO_CONFERENCIA'
  ,CTRL_TIPO_PROCESSO  = 'R'
  ,ORDEM               = 11
  ,CALC_ORDEM          = 89
  ,ICONE_NUMERO        = 0
  ,NOME_DO_FONTE       = 'Courier New'
  ,TAMANHO_DO_FONTE    = 3
  ,FONTE_NEGRITO       = 'N'
  ,IMPRESSORA          = ''
  ,ORIENTACAO          = 1
  ,FRM_ID              = mprcdef_frm_id
  ,IND_DEF_USUARIO     = 'S'
  ,IND_DESATIVADO      = 'N'
  Where ID = mid;
  Commit;
Exception When NO_DATA_FOUND Then
   Begin
--************************************************************************
--* ATEN��O -- Se voc� alterou o c�digo da funcionalidade ou processo    *
--* no select abaixo voc� deve procurar pelo novo c�digo para que o      *
--* script possa ser executado mais de uma vez e atualize a defini��o    *
--* ao inv�s de criar uma nova!!!                                        *
--************************************************************************
       Select ID into mid from SYN_PRC_DEFINICAO where CODIGO = 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA';
       Update SYN_PRC_DEFINICAO set
           CODIGO              = 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA'
           ,NOME                = 'Relatorio de conferencia de dados do REINF'
           ,TITULO              = 'Relatorio de conferencia de dados do REINF'
           ,FUN_TIPO            = 'F_FUNC'
           ,PRCDEF_ID           = mprcdef_prcdef_id
           ,NIVEL               = 5
           ,EXECUTAVEL_NOME     = ''
           ,ROTINA_DE_GERACAO   = 'HON_REINF_GAP_EXPORT.RELATORIO_CONFERENCIA'
           ,CTRL_TIPO_PROCESSO  = 'R'
           ,ORDEM               = 11
           ,CALC_ORDEM          = 89
           ,ICONE_NUMERO        = 0
           ,NOME_DO_FONTE       = 'Courier New'
           ,TAMANHO_DO_FONTE    = 3
           ,FONTE_NEGRITO       = 'N'
           ,IMPRESSORA          = ''
           ,ORIENTACAO          = 1
           ,FRM_ID              = mprcdef_frm_id
           ,IND_DEF_USUARIO     = 'S'
           ,IND_DESATIVADO      = 'N'
       Where ID = mid;
       Commit;
   Exception When NO_DATA_FOUND Then
       Select syn_seq_geral.nextval into mid from dual;
       Insert into SYN_PRC_DEFINICAO(
           ID
           ,CODIGO
           ,NOME
           ,TITULO
           ,FUN_TIPO
           ,PRCDEF_ID
           ,NIVEL
           ,EXECUTAVEL_NOME
           ,ROTINA_DE_GERACAO
           ,CTRL_TIPO_PROCESSO
           ,ORDEM
           ,CALC_ORDEM
           ,ICONE_NUMERO
           ,NOME_DO_FONTE
           ,TAMANHO_DO_FONTE
           ,FONTE_NEGRITO
           ,IMPRESSORA
           ,ORIENTACAO
           ,FRM_ID
           ,IND_DEF_USUARIO
           ,IND_DESATIVADO
           ) Values (
           mid
           ,'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA'
           ,'Relatorio de conferencia de dados do REINF'
           ,'Relatorio de conferencia de dados do REINF'
           ,'F_FUNC'
           ,mprcdef_prcdef_id
           ,5
           ,''
           ,'HON_REINF_GAP_EXPORT.RELATORIO_CONFERENCIA'
           ,'R'
           ,11
           ,89
           ,0
           ,'Courier New'
           ,3
           ,'N'
           ,''
           ,1
           ,mprcdef_frm_id
           ,'S'
           ,'N'
           );
       Commit;
   End;
End;
DELETE SYN_PRC_PARAMETRO WHERE PRCDEF_ID = mid;
Commit;
-- Importando Definicao de Parametros de 'UTI\ITF\PRC\PRC\HON_REINF_GAP_EXPORT_RELATORIO_CONFERENCIA' ...
-- Importando Parametro 0 'p_est_codigo' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_est_codigo'
,'Estabelecimento'
,0
,'N'
,'TEXT'
,''
,''
,'select p.pfj_codigo
     , p.mnemonico
  from cor_pessoa        p
  join cor_estab_usuario e
    on p.pfj_codigo = e.est_codigo
 order by 1   
'
,''
);
-- Importando Parametro 1 'p_dt_inicio' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_dt_inicio'
,'Data Inicial'
,1
,'N'
,'DATE'
,''
,''
,''
,''
);
-- Importando Parametro 2 'p_dt_fim' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_dt_fim'
,'Data Final'
,2
,'N'
,'DATE'
,''
,''
,''
,''
);
-- Importando Parametro 3 'p_siss_codigo' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_siss_codigo'
,'Codigo de servico'
,3
,'N'
,'TEXT'
,''
,'%'
,'select ''%'', ''Todos'' from dual union
select s.siss_codigo
     , s.nome 
  from cor_servico_iss s
 order by 1
'
,''
);
-- Importando Parametro 4 'p_subclasse_idf' ...
Insert INTO SYN_PRC_PARAMETRO(
 ID
,PRCDEF_ID
,PARAMETRO
,TITULO
,ORDEM
,OPCIONAL
,TIPO
,VALIDACAO
,VALOR_DEFAULT
,LISTA_DE_VALORES
,TEXTO_DE_AJUDA
) Values (
 syn_seq_geral.nextval
,mid
,'p_subclasse_idf'
,'Tipo de Item'
,4
,'N'
,'TEXT'
,''
,'%'
,'select ''%'', ''Todos'' from dual union
select ''S'', ''Servico'' from dual union
select ''P'', ''Prestacao'' from dual union
select ''M'', ''Mercadoria'' from dual union
select ''O'', ''Outros'' from dual
'
,''
);
Commit;
END;
/
 
BEGIN
  SYN_PRC_DEFINICAO_PKG.ATUALIZA; 
  SYN_PRC_DEFINICAO_PKG.VALIDA; 
END; 
/ 
