  CREATE OR REPLACE PACKAGE BODY "FAB4W"."HON_SAP_COR_DOF_ITF" IS
  
   PROCEDURE acha_vl_abat_nt ( p_id_dof in SAP_ITF_DOF_COR.ID%type
                              ,p_vl_vl_abat_nt in out cor_dof.vl_abat_nt%type) is
							  
     cursor creg is
     select idf.id id_idf
     from sap_itf_dof_cor dof, sap_itf_idf_cor idf
     where dof.id = p_id_dof and dof.id = idf.yj1bnfdoc_id (+);

     mreg creg%rowtype;
     v_vl_vl_abat_nt cor_dof.vl_abat_nt%type;

     BEGIN
       p_vl_vl_abat_nt := 0; -- Inicializo a vari�vel
       open creg;
       loop
         fetch creg 
		 into mreg;
         exit when creg%notfound;
		 
           Begin
             select nvl ( sum ( taxval), 0 )
             into v_vl_vl_abat_nt
             from sap_itf_impostos_idf_cor
             where yj1bnflin_id = mreg.id_idf
                   and taxtyp = 'ICZF' -- Somente para este imposto
             group by yj1bnflin_id;

           Exception when no_data_found then
             v_vl_vl_abat_nt := 0;
                     when others then
                       raise_application_error ( -20716, 
					   'Falha na Leitura do Valor de abatimento n�o tributado . ' );
           End;

           p_vl_vl_abat_nt := p_vl_vl_abat_nt + abs(v_vl_vl_abat_nt);

         end loop;
         close creg;
     EXCEPTION WHEN OTHERS THEN
       raise;

END; -- procedure acha_vl_abat_nt
------------------------------------------------------------------------------------------------------------------------------------
--Processa a Interface via JOB
------------------------------------------------------------------------------------------------------------------------------------
   procedure process_dof_job(mprocesso number, qtdJob integer) is
     jobno NUMBER;
     what varchar2(32000);

     begin
       update sap_itf_dof_cor set job = mod(rownum,qtdJob);
       commit;

       for J in (  select count(*) qtd, 
	   dof.nfnum,
	   dof.seriessub,
	   dof.direct,
	   dof.docdat,
	   dof.empresa_filial
                   from sap_itf_dof_cor dof
                   group by dof.nfnum, 
				   dof.seriessub, 
				   dof.direct, 
				   dof.docdat, 
				   dof.empresa_filial
                 )
       loop
        if j.qtd > 1 Then
          Update sap_itf_dof_cor set job = 0
          Where nfnum = j.nfnum
                and seriessub = j.seriessub
                and direct = j.direct
                and docdat = j.docdat
                and empresa_filial = j.empresa_filial;
           commit;
        end if;
       end loop;

       syn_out.put_line('');
       syn_out.put_line('ATEN��O - A Interface ser� executada por paralelismo devido ao par�metro SAP_PARALELISMO_ITF_DOF igual a: ' || qtdJob);
       syn_out.put_line('Ser�o criados ' || qtdJob || ' Job(s) para a execu��o da interface.');
       syn_out.put_line('');

       for i in 1..qtdJob loop
         what := 'begin sap_executa_itf_dof_job(' || i || ',''' || nvl(SYN_USU_CTRL.get_usu_codigo,'INDEFINIDO') || '''); end;';
         DBMS_JOB.submit(jobno,what);
         syn_out.put_line('Criado Job ' || i || ' id = ' || jobno);
       end loop;

       syn_out.put_line('');
       syn_out.put_line('Os Job(s) foram configurados. Aguarde o processamento!');
       syn_out.put_line('');
       syn_out.put_line('Os Relat�rios ser�o gravados no Gerenciador de Processos: Relat�rios e Processos > Processos > Integra��o SAP R3 > Corporativo > Interface - Executar');

end;
------------------------------------------------------------------------------------------------------------------------------------
--Processa a Interface normalmente
------------------------------------------------------------------------------------------------------------------------------------
   PROCEDURE process_dof( p_prc_id in integer,
                          p_xml_id in number,
                          pregs_importados in out integer,
                          pregs_rejeitados in out integer,
                          -- SGS - 02/06/11 - Ocor. 2662452 - Parametro de registros a processar
                          p_critica        in varchar2,
                          -- SGS - 02/06/11 - Ocor. 2662452
                          -- cdc Filtro de Estabelecimento
                          p_filtro_estab  cor_dof.informante_est_codigo%type,
                          p_num_job       sap_itf_dof_cor.job%type default 0
     ) IS

     mprocesso                integer;
     mregs_importados         integer;
     mregs_rejeitados         integer;
     mtipo_complemento        varchar2(2);
     mId                      sap_itf_dof_cor.id%type;
     mcritica                 varchar2(20) := null;
     mold_docnum              varchar2(20) := null;
     mDOF_ID                  cor_idf.dof_id%type := null;
     mDOF_SEQUENCE            cor_dof.dof_sequence%type := null;
     mtipo_complemento_aux    varchar2(2);
     v_ult_dof_gravado        varchar2(20) := null;
     v_deduz_mun_codigo_iss   varchar2(1)  := null ;
     v_suprime_zeros          varchar2(1)  := null ;
     v_nop                    COR_NATUREZA_DE_OPERACAO.NOP_CODIGO%type;
     v_critica_i              number;
     v_critica_f              number;
     v_qtd_iss_retido         number;
     v_carimbador_dirf        varchar2(1)  := null;
     v_sap_utiliza_lote_med   varchar2(1)  := null;
     v_sap_especie_nf_class   varchar2(20)  := null;
     v_nao_carregar           boolean := false;
     mcontador                integer := 0;
     v_old_INFORMANTE         cor_dof.informante_est_codigo%type   := null;
     v_INFORMANTE_EST_CODIGO  cor_dof.informante_est_codigo%type   := null;
     v_codigo_especie         cor_dof.serie_subserie%type          := null;
     v_old_cod_especie        cor_dof.serie_subserie%type          := null;
     v_edof_codigo            cor_dof.edof_codigo%type             := null;
     v_old_edof_codigo        cor_dof.edof_codigo%type             := null;
     v_ind_resp_frete         sap_itf_dof_cor.INCO1%type           := null;
     v_old_ind_resp_frete     sap_itf_dof_cor.INCO1%type           := null;

     v_DESTINATARIO_PFJ_CODIGO       cor_pessoa.pfj_codigo%type    := null;
     v_EMITENTE_PFJ_CODIGO           cor_pessoa.pfj_codigo%type    := null;
     v_REMETENTE_PFJ_CODIGO          cor_pessoa.pfj_codigo%type    := null;
     v_TRANSPORTADOR_PFJ_CODIGO      cor_pessoa.pfj_codigo%type    := null;
     v_ENTREGA_PFJ_CODIGO            cor_pessoa.pfj_codigo%type    := null;
     v_RETIRADA_PFJ_CODIGO           cor_pessoa.pfj_codigo%type    := null;
     v_cobranca_pfj_codigo           cor_pessoa.pfj_codigo%type    := null;
     v_substituido_pfj_codigo        cor_pessoa.pfj_codigo%type    := null;

     v_DESTINATARIO_LOC_CODIGO       cor_localidade_vigencia.loc_codigo%type  := null;
     v_EMITENTE_loc_CODIGO           cor_localidade_vigencia.loc_codigo%type  := null;
     v_REMETENTE_loc_CODIGO          cor_localidade_vigencia.loc_codigo%type  := null;
     v_TRANSPORTADOR_loc_CODIGO      cor_localidade_vigencia.loc_codigo%type  := null;
     v_ENTREGA_loc_CODIGO            cor_localidade_vigencia.loc_codigo%type  := null;
     v_RETIRADA_loc_CODIGO           cor_localidade_vigencia.loc_codigo%type  := null;
     v_cobranca_loc_codigo           cor_localidade_vigencia.loc_codigo%type  := null;
     pTipoMsg                        varchar2(100) := '';
     muf_codigo_destino              cor_dof.uf_codigo_destino%type           := null;
     muf_codigo_origem               cor_dof.uf_codigo_origem%type            := null;
     muf_documento                   cor_dof.uf_codigo_origem%type            := null;
     mind_eix                        cor_dof.ind_eix%type                     := null;
     mRegimeEspecialRS          integer;            
    
    type t_rec is record(               
         CODIGO_DO_SITE cor_dof.CODIGO_DO_SITE%type,
         DOF_SEQUENCE cor_dof.DOF_SEQUENCE%type
    );
  
    type t_rec_array is table of t_rec index by pls_integer;
    t_dof_criticados          t_rec_array;
    v_idx            					number := 1;

     cursor c_indicador_titulo is
     select  ind_tit, desc_titulo, count(*)
     from sap_itf_dof_cor
     where ind_tit is not null
     group by ind_tit, desc_titulo -- Verificar se � esta a informa��o da Descri��o d t�tulo
     ;

     r_indicador_titulo c_indicador_titulo%rowtype;

     CURSOR cdof(v_critica_i number,v_critica_f number)
     IS
     SELECT SAP_ITF_DOF_IDF_COR.SERIESSUB SERIE_SUBSERIE
          , SAP_ITF_DOF_IDF_COR.EMPRESA_FILIAL   INFORMANTE_EST_CODIGO   -- Codigo original do estabelecimento
         , NULL EDOF_CODIGO
         , NULL IND_RESP_FRETE
         , SAP_ITF_DOF_IDF_COR.INCO1 INCO1
        , to_char ( SAP_ITF_DOF_IDF_COR.DOCDAT, 'YYYYMM' ) ANO_MES_EMISSAO
        , SAP_ITF_DOF_IDF_COR.TRATY VT_CODIGO
		, 'SAP_J_1BNFDOC' ORIGEM
        , 0 VL_DESCONTO
        , 0 VL_DESCONTO_SERVICO
        , sap_dof_util.get_subclasse_idf (SAP_ITF_IDF_COR.MATNR, SAP_ITF_IDF_COR.CFOP) SUBCLASSE_IDF
        , SAP_ITF_DOF_IDF_COR.DOCNUM ||  SAP_ITF_IDF_COR.ITMNUM CHAVE_ORIGEM_IDF
        , decode ( sap_dof_util.get_subclasse_idf (SAP_ITF_IDF_COR.MATNR,SAP_ITF_IDF_COR.CFOP),  
		           'M', 
				   decode(sap_itf_xml_util.get_par_val ('SAP_SPLIT_VALUATION'),
				          'S',
				          SAP_ITF_IDF_COR.matnr||SAP_ITF_IDF_COR.bwtar,
				          SAP_ITF_IDF_COR.matnr) , 
				   null ) MERC_CODIGO
        , SAP_ITF_IDF_COR.MATNR   MATNR
        , SAP_ITF_IDF_COR.MAKTX IDF_TEXTO_COMPLEMENTAR
        , SAP_ITF_IDF_COR.TAXSIT STC_CODIGO
		, DECODE(LENGTH(SAP_ITF_IDF_COR.TAXSI2),2, SAP_ITF_IDF_COR.TAXSI2, SUBSTR(SAP_ITF_IDF_COR.TAXSI2,4,2)) STP_CODIGO
        , SAP_ITF_IDF_COR.UNI_NF ENTSAI_UNI_CODIGO
        , COR_DOF_IDF_ID (null, null, null) DOF_ID
        , SAP_ITF_IDF_COR.NBM NBM_CODIGO
        , 'SAP_J1BNFLIN' ORIGEM_IDF
        ,SAP_ITF_IDF_COR.VL_FISCAL * SAP_ITF_IDF_COR.MENGE PRECO_TOTAL_ITEM
        , nvl( SAP_ITF_IDF_COR.NFNETT, sap_itf_idf_cor.netwr ) PRECO_TOTAL_CONTABIL
        , SAP_ITF_DOF_IDF_COR.ZTERM CPAG_CODIGO
        , SAP_ITF_DOF_IDF_COR.SHPUNT ESPECIE_VOLUMES
        , SAP_ITF_DOF_IDF_COR.PARID DESTINATARIO_PFJ_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID EMITENTE_PFJ_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID REMETENTE_PFJ_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID TRANSPORTADOR_PFJ_CODIGO
        , sap_itf_dof_idf_cor.parid cobranca_pfj_codigo
        , sap_itf_dof_idf_cor.parid cobranca_loc_codigo
        , sap_itf_dof_idf_cor.parid substituido_pfj_codigo
        , '00000000000000000' ALIQ_ICMS
        , '00000000000000000' ALIQ_DIFA
        , '00000000000000000' ALIQ_IPI
        , '00000000000000000' ALIQ_ISS
        , '00000000000000000' ALIQ_STT
        , '00000000000000000' ALIQ_STF
        , '00000000000000000' PERC_IRRF
        , '00000000000000000' VL_BASE_ICMS
        , '00000000000000000' VL_BASE_IPI
        , '00000000000000000' VL_BASE_IRRF
        , '00000000000000000' VL_BASE_ISS
        , '00000000000000000' VL_BASE_REDUCAO_IPI
        , '00000000000000000' VL_BASE_STT
        , '00000000000000000' VL_BASE_STF
        , '00000000000000000' vl_base_stf_fronteira
        , '00000000000000000' vl_stf_fronteira
        , '00000000000000000' vl_base_stf_ido
        , '00000000000000000' vl_stf_ido
        , 'N'                 tipo_stf
        , '00000000000000000' perc_icms_part_rem
        , '00000000000000000' vl_base_icms_part_rem
        , '00000000000000000' aliq_icms_part_rem
        , '00000000000000000' vl_icms_part_rem
        , '00000000000000000' perc_icms_part_dest
        , '00000000000000000' vl_base_icms_part_dest
        , '00000000000000000' aliq_icms_part_dest
        , '00000000000000000' vl_icms_part_dest
        , '00000000000000000' aliq_icms_fcp
        , '00000000000000000' vl_icms_fcp
        , '00000000000000000' vl_base_icms_fcp
		,'00000000000000000' VL_SERVICO_AE15
        ,'00000000000000000' ALIQ_INSS_AE15_RET
        ,'00000000000000000' VL_INSS_AE15_RET
        ,'00000000000000000' VL_SERVICO_AE20
        ,'00000000000000000' ALIQ_INSS_AE20_RET
        ,'00000000000000000' VL_INSS_AE20_RET
        ,'00000000000000000' VL_SERVICO_AE25
        ,'00000000000000000' ALIQ_INSS_AE25_RET
        ,'00000000000000000' VL_INSS_AE25_RET
        , SAP_ITF_IDF_COR.cest CEST_CODIGO
        , SAP_ITF_IDF_COR.cenq ENQ_IPI_CODIGO
        , '00000000000000000' aliq_icms_part_dest_tmp
        , '00000000000000000' VL_TRIBUTAVEL_ICMS
        , '00000000000000000' VL_TRIBUTAVEL_IPI
        , '00000000000000000' VL_TRIBUTAVEL_DIFA
        , '00000000000000000' VL_TRIBUTAVEL_STT
        , '00000000000000000' VL_TRIBUTAVEL_STF
        , '00000000000000000' VL_DIFA
        , '00000000000000000' VL_ICMS
        , '00000000000000000' VL_IPI
        , '00000000000000000' VL_IRRF
        , '00000000000000000' VL_ISS
        , '00000000000000000' VL_STT
        , '00000000000000000' VL_STF
        , '00000000000000000' VL_RED_BASE_ICMS
        , '00000000000000000' VL_ISENTO_ICMS
        , '00000000000000000' VL_ISENTO_IPI
        , '00000000000000000' VL_OUTROS_ICMS
        , '00000000000000000' VL_OUTROS_IPI
        , '00000000000000000' VL_ALIQ_PIS
        , '00000000000000000' VL_ALIQ_COFINS
        , '00000000000000000' ALIQ_PIS_RET
        , '00000000000000000' ALIQ_COFINS_RET
        , '00000000000000000' ALIQ_CSLL_RET
        , '00000000000000000' VL_BASE_PIS
        , '00000000000000000' VL_BASE_COFINS
        , '00000000000000000' VL_BASE_PIS_RET
        , '00000000000000000' VL_BASE_COFINS_RET
        , '00000000000000000' VL_BASE_CSLL_RET
        , '00000000000000000' VL_IMPOSTO_PIS
        , '00000000000000000' VL_IMPOSTO_COFINS
        , '00000000000000000' VL_PIS_RET
        , '00000000000000000' VL_COFINS_RET
        , '00000000000000000' VL_CSLL_RET
        , '00000000000000000' VL_BASE_INSS_RET
        , '00000000000000000' VL_INSS_RET
        , '00000000000000000' ALIQ_INSS_RET
	    , '00000000000000000' ALIQ_ICMS_FCPST
        , '00000000000000000' VL_BASE_ICMS_FCPST
        , '00000000000000000' VL_ICMS_FCPST
        , SAP_ITF_TOTAL_DOF_COR.NFTOT VL_TOTAL_CONTABIL
        , SAP_ITF_TOTAL_DOF_COR.NFTOT VL_TOTAL_FATURADO
        , '0000000000000000000000' VL_CONTABIL
        , '0000000000000000000000' VL_FATURADO
        , SAP_ITF_IDF_COR.NETWR + SAP_ITF_IDF_COR.NETDIS VL_FISCAL
        , null am_codigo
        , sap_itf_idf_cor.matuse matuse
        , 'M' CTRL_INSTRUCAO
        , decode ( SAP_ITF_DOF_IDF_COR.DIRECT, '1', 'E', '2', 'S' , null) IND_ENTRADA_SAIDA
        , SAP_ITF_IDF_COR.CFOP NOP_CODIGO_DOF
        , decode ( SAP_ITF_DOF_IDF_COR.IND_CANCEL, 'X', 'S', 'N' ) CTRL_SITUACAO_DOF
        , COR_DOF_IDF_ID (null, null, null) IDF_ID
        , ltrim ( SAP_ITF_IDF_COR.MAKTX ) DESCRICAO_IDF
        , SAP_DEDUZ_CFOP_NOP  (SAP_ITF_IDF_COR.CFOP,  null,'CFOP') CFOP_CODIGO
        , SAP_ITF_IDF_COR.CFOP NOP_CODIGO_IDF
        , SAP_ITF_IDF_COR.CFOP CFOP_IDF
        , decode ( SAP_ITF_DOF_IDF_COR.DIRECT, '1', SAP_ITF_DOF_IDF_COR.PSTDAT, SAP_ITF_DOF_IDF_COR.DOCDAT ) DT_REFERENCIA
        , cor_uf_pessoa ( null, null, null, 'N'  ) UF_REFERENCIA
        , SAP_ITF_TOTAL_DOF_COR.NFTOT VALOR_TOTAL_DOFASS
        , SAP_ITF_DOF_IDF_COR.WITHA VL_BASE_IRRF_DOF
        , SAP_ITF_DOF_IDF_COR.ALIQ_IRRF ALIQ_IRRF_DOF
        , SAP_ITF_DOF_IDF_COR.OBSERVAT OBSERVAT  -- CBT 04/05/05 Mensagem ICMS Ocor 731937
        , SAP_ITF_IDF_COR.ITMNUM IDF_NUM
        , SAP_ITF_DOF_IDF_COR.DOCDAT DH_EMISSAO
        , SAP_ITF_DOF_IDF_COR.DOCNUM DOF_IMPORT_NUMERO
        , decode ( replace(SAP_ITF_DOF_IDF_COR.DOCREF,'0',''), null, SAP_ITF_IDF_COR.DOCREF, SAP_ITF_DOF_IDF_COR.DOCREF ) DOCREF
        , SAP_ITF_DOF_IDF_COR.ID ID_DOF
        , SAP_ITF_DOF_IDF_COR.NFTYPE CODIGO_ESPECIE
        , SAP_ITF_DOF_IDF_COR.NTGEW PESO_LIQUIDO
        , SAP_ITF_DOF_IDF_COR.PSTDAT DT_FATO_GERADOR_IMPOSTO
        , SAP_ITF_DOF_IDF_COR.XML_ID XML_ID
        , SAP_ITF_IDF_COR.ID ID_IDF
        , SAP_ITF_IDF_COR.NETFRE VL_RATEIO_FRETE
        , SAP_ITF_IDF_COR.NETINS VL_RATEIO_SEGURO
        , SAP_ITF_IDF_COR.NETOTH VL_RATEIO_ODA
        , SAP_ITF_IDF_COR.vl_fiscal PRECO_UNITARIO
        , SAP_ITF_IDF_COR.REFKEY CHAVE_DO_PARCEIRO_CD
        , SAP_ITF_IDF_COR.MENGE QTD
        , decode ( sap_dof_util.get_subclasse_idf (SAP_ITF_IDF_COR.MATNR,SAP_ITF_IDF_COR.CFOP),  'P', SAP_ITF_IDF_COR.MATNR , null ) PRES_CODIGO
        , decode ( sap_dof_util.get_subclasse_idf (SAP_ITF_IDF_COR.MATNR,SAP_ITF_IDF_COR.CFOP),  'S', SAP_ITF_IDF_COR.MATNR , null ) SISS_CODIGO
        , SAP_ITF_IDF_COR.MEINS ESTOQUE_UNI_CODIGO
        , SAP_ITF_DOF_IDF_COR.ANZPK QTD_VOLUMES
        , SAP_ITF_DOF_IDF_COR.BRGEW PESO_BRUTO
        , SAP_ITF_IDF_COR.NETDIS VL_AJUSTE_PRECO_TOTAL
        , decode ( SAP_ITF_IDF_COR.NFNETT, NULL, SAP_ITF_IDF_COR.NETDIS, 0 ) VL_AJUSTE_SOMA
        , decode ( SAP_ITF_IDF_COR.NFNETT, null, 'S', 'N' ) AJUSTA_VL_CONTABIL
        , SAP_ITF_TOTAL_DOF_COR.NFINS VL_SEGURO
        , SAP_ITF_TOTAL_DOF_COR.NFOTH VL_OUTRAS_DESPESAS
        , SAP_ITF_TOTAL_DOF_COR.NFFRE VL_FRETE
        , SAP_ITF_DOF_IDF_COR.PARXCPDK PARXCPDK
        , COR_LE_CODIGO_DO_SITE CODIGO_DO_SITE
        , COR_PK_SEQ_DOF ( null, null ) DOF_SEQUENCE
        , SAP_ITF_IDF_COR.MATNR CODIGO_IDF
        , SAP_ITF_DOF_IDF_COR.PARID ENTREGA_PFJ_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID RETIRADA_PFJ_CODIGO
        , NULL EDOF_CODIGO_ASSOC
        , SAP_ITF_DOF_IDF_COR.SERIESSUB SERIE_SUBSERIE_ASSOC
        , SAP_ITF_DOF_IDF_COR.DOCDAT DH_EMISSAO_ASSOC
        , SAP_ITF_DOF_IDF_COR.PARID EMITENTE_PFJ_CODIGO_ASSOC
        , SAP_ITF_DOF_IDF_COR.NFNUM NUMERO
        , SAP_ITF_DOF_IDF_COR.NFNUM NUMERO_ASSOC
        , to_char ( SAP_ITF_DOF_IDF_COR.DOCDAT, 'YYYYMM' ) ANO_MES_EMISSAO_ASSOC
        , '00000000000000000' VL_TOTAL_DOF_ASSOC
          , 111111111111111 DOF_ASSOC_SEQUENCE
        , 'C' CTRL_CONTEUDO
        , 'SAP' SIS_CODIGO
        , SAP_ITF_DOF_IDF_COR.PESO_BRUTO_TOTAL PESO_REEMBALAGEM_KG
        , SAP_ITF_DOF_IDF_COR.PARID DESTINATARIO_LOC_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID EMITENTE_LOC_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID REMETENTE_LOC_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID TRANSPORTADOR_LOC_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID ENTREGA_LOC_CODIGO
        , SAP_ITF_DOF_IDF_COR.PARID RETIRADA_LOC_CODIGO
        , 'E' MODO_EMISSAO
        , SAP_ITF_IDF_COR.MATORG OM_CODIGO
        , COR_MUNICIPIO_PESSOA(null, null, null) mun_codigo_iss
        , TXJCD TXJCD
        , null MUN_PRES_SERVICO
        , SAP_ITF_DOF_IDF_COR.EMPRESA_FILIAL EMPRESA_FILIAL
        , 'M' c_COR_DOF
        , 'M' c_COR_DOF_ASSOCIADO
        , 'M' c_COR_IDF
        , 'M' c_COR_DOF_VOLUME_CARGA
        , 'M' C_COR_LOTE_MED
        , 'M' C_COR_IDF_CONS_CIVIL
        , 'M' C_COR_DOF_PROCESSO
        , 'N' Tipo
        , nvl(SAP_ITF_DOF_IDF_COR.IND_CANCEL, '*') IND_CANCEL
        , SAP_ITF_IDF_COR.BRGEW PESO_BRUTO_ITEM
        , SAP_ITF_IDF_COR.NTGEW PESO_LIQUIDO_ITEM
        , 'I' CTRL_ORIGEM_REGISTRO -- CBT 15/08/06 Ocor 1055662
        , SAP_ITF_DOF_IDF_COR.ID ID -- RCT 26/06/07 Ocor 1203847
        , sap_itf_dof_idf_cor.traid  num_placa_veiculo
        , sap_itf_dof_idf_cor.tipo_pgto  tipo_pgto
        , '00000000000000000' VL_ABAT_NT
        , '00000000000000000' VL_BASE_INSS
        , '00000000000000000' ALIQ_INSS
        , '00000000000000000' VL_INSS
        , sap_itf_idf_cor.hsdat dt_fab_med
        , sap_itf_dof_idf_cor.ind_iss_retido_fonte
        , sap_itf_dof_idf_cor.peso_total peso_total
        , sap_itf_dof_idf_cor.destinatario_pfj_assoc destinatario_pfj_codigo_assoc
        , sap_itf_idf_cor.predu dt_di
        , substr ( sap_itf_idf_cor.prefn,1,12) num_di
        , sap_itf_idf_cor.chassis_num chassi_veic
        , sap_itf_idf_cor.codigo_retencao
        , sap_itf_idf_cor.im_subcontratacao
        , sap_itf_idf_cor.charg  -- Lote do medicamento
        , sap_itf_dof_idf_cor.ind_tit
        , sap_itf_dof_idf_cor.desc_titulo
        , sap_itf_dof_idf_cor.num_titulo
        , sap_itf_dof_idf_cor.qtd_prestacoes
        , sap_itf_dof_idf_cor.vl_tit
        , '00000000000000000' ALIQ_II
        , '00000000000000000' VL_II
        , '00000000000000000' VL_BASE_II
        , ltrim (sap_itf_idf_cor.conta_contabil , '0' ) conta_contabil
        , nvl(sap_itf_dof_idf_cor.CNPJ_COL,sap_itf_dof_idf_cor.CPF_COL) CNPJ_COL
        , sap_itf_dof_idf_cor.IE_COL IE_COL
        , sap_itf_dof_idf_cor.COD_MUN_COL COD_MUN_COL
        , nvl(sap_itf_dof_idf_cor.CPF_ENTG,sap_itf_dof_idf_cor.CNPJ_ENTG) CNPJ_ENTG
        , sap_itf_dof_idf_cor.IE_ENTG IE_ENTG
        , '00000000000000000' QTD_BASE_PIS -- Quantidade base para Pis e cofins
        , '00000000000000000' QTD_BASE_COFINS -- Quantidade base para Pis e cofins
        , SAP_ITF_IDF_COR.IND_MOVIMENTA_ESTOQUE IND_MOVIMENTA_ESTOQUE-- Indicador de movimenta��o de estoque
        , decode (ltrim(SAP_ITF_IDF_COR.TAXSI4, '0'), null , null, SAP_ITF_IDF_COR.TAXSI4 ) STA_CODIGO
        , decode (ltrim(SAP_ITF_IDF_COR.TAXSI5, '0'), null , null, SAP_ITF_IDF_COR.TAXSI5 ) STN_CODIGO
        , SAP_ITF_DOF_IDF_COR.NFE_LOCALIZADOR NFE_LOCALIZADOR
        , SAP_ITF_DOF_IDF_COR.NFE_LOCALIZADOR NFE_LOCALIZADOR_assoc
		, SAP_ITF_DOF_IDF_COR.SITUACAO
        , SAP_ITF_DOF_IDF_COR.PAIS_COL
        , SAP_ITF_DOF_IDF_COR.UF_COL
        , SAP_ITF_DOF_IDF_COR.NOME_MUN_COL
        , SAP_ITF_DOF_IDF_COR.PAIS_ENTG
        , SAP_ITF_DOF_IDF_COR.UF_ENTG
        , SAP_ITF_DOF_IDF_COR.NOME_MUN_ENTG
        , SAP_ITF_DOF_IDF_COR.COD_MUN_ENTG
        , '00000000000000000' VL_BASE_SEST
        , '00000000000000000' VL_BASE_SENAT
        , '00000000000000000' VL_SEST
        , '00000000000000000' VL_SENAT
        , '00000000000000000' ALIQ_SEST
        , '00000000000000000' ALIQ_SENAT
        ,'E' IND_ENTRADA_SAIDA_ASSOC
        , 'N' IND_INCIDENCIA_PIS_ST
        , '00000000000000000' VL_BASE_PIS_ST
        , '00000000000000000' ALIQ_PIS_ST
        , '00000000000000000' VL_PIS_ST
        , 'N' IND_INCIDENCIA_COFINS_ST
        , '00000000000000000' VL_BASE_COFINS_ST
        , '00000000000000000' ALIQ_COFINS_ST
        , '00000000000000000' VL_COFINS_ST
        , SAP_ITF_IDF_LOTE_MED_COR.LOTE_MED         LOTE_MED
        , SAP_ITF_IDF_LOTE_MED_COR.DT_FAB_MED       DT_FAB_lote_MED
        , SAP_ITF_IDF_LOTE_MED_COR.DT_VAL           DT_VAL
        , SAP_ITF_IDF_LOTE_MED_COR.IND_MED          IND_MED
        , SAP_ITF_IDF_LOTE_MED_COR.TIPO_PROD_MED    TIPO_PROD_MED
        , SAP_ITF_IDF_LOTE_MED_COR.VL_TAB_MAX       VL_TAB_MAX
        , SAP_ITF_IDF_LOTE_MED_COR.QTD              QTD_lote_med
        , sap_itf_dof_idf_cor.DT_EXE_SERV           DT_EXE_SERV
        , sap_itf_idf_cor.COD_CCUS                  COD_CCUS
        , sap_itf_idf_cor.NAT_REC_PISCOFINS         NAT_REC_PISCOFINS
        , sap_itf_idf_cor.NAT_REC_PISCOFINS_DESCR   NAT_REC_PISCOFINS_DESCR
        , sap_itf_idf_cor.VL_CRED_PIS_REC_TRIB          VL_CRED_PIS_REC_TRIB
        , sap_itf_idf_cor.VL_CRED_PIS_REC_NAO_TRIB      VL_CRED_PIS_REC_NAO_TRIB
        , sap_itf_idf_cor.VL_CRED_PIS_REC_EXPO          VL_CRED_PIS_REC_EXPO
        , sap_itf_idf_cor.VL_CRED_COFINS_REC_TRIB       VL_CRED_COFINS_REC_TRIB
        , sap_itf_idf_cor.VL_CRED_COFINS_REC_NAO_TRIB   VL_CRED_COFINS_REC_NAO_TRIB
        , sap_itf_idf_cor.VL_CRED_COFINS_REC_EXPO       VL_CRED_COFINS_REC_EXPO
        , '00000000000000000' VL_BASE_ICMS_DESC_L
        , '00000000000000000' VL_TRIBUTAVEL_ICMS_DESC_L
        , '00000000000000000' ALIQ_ICMS_DESC_L
        , '00000000000000000' VL_ICMS_DESC_L
        , SAP_ITF_IDF_COR.IND_NAT_FRT_PISCOFINS
        , '00000000000000000' VL_DEDUCAO_DEPENDENTE_PRG
        , '00000000000000000' VL_DEDUCAO_PENSAO_PRG
        , NVL(SAP_ITF_IDF_COR.PERC_PART_CI,0) PERC_PART_CI
        , CHECOD
        , PREFNO
        , SAP_ITF_IDF_COR.NFCI
        , 'OPC' TADOC_CODIGO
        , '' TIPO_CTE
        ,SAP_ITF_DOF_IDF_COR.VEIC_ID_COMP
        ,SAP_ITF_DOF_IDF_COR.UF_VEIC_COMP
        ,SAP_ITF_DOF_IDF_COR.VEIC_ID_COMP_2
        ,SAP_ITF_DOF_IDF_COR.UF_VEIC_COMP_2
        ,SAP_ITF_DOF_IDF_COR.PLACA_VEICULO_UF_CODIGO
        ,lpad(sap_itf_idf_cor.motdesicms,2,0) Desoneracao_Codigo
        ,sap_itf_idf_cor.vicmsdeson
		,sap_itf_xml_util.get_par_val('SAP_NOP_CODIGO') NOP_CODIGO_INUTIL
        ,SAP_ITF_DOF_IDF_COR.CTE_STRT_LCT
        ,SAP_ITF_DOF_IDF_COR.CTE_END_LCT
        ,'00000000000000000' MUN_COD_ORIGEM
        ,'00000000000000000' MUN_COD_DESTINO
        ,SAP_ITF_DOF_IDF_COR.pstdat pstdat
        ,SAP_ITF_DOF_IDF_COR.docdat docdat
        ,decode(SAP_ITF_DOF_IDF_COR.DIRECT,
                    '1',
                    SAP_ITF_DOF_IDF_COR.PSTDAT,
                    SAP_ITF_DOF_IDF_COR.DOCDAT) DT_REFER_EXT
        ,SAP_ITF_IDF_CONS_CIVIL_COR.CNPJ_CPF_PROP_OBRA                 CNPJ_CPF_PROP_OBRA
        ,SAP_ITF_IDF_CONS_CIVIL_COR.IND_OBRA                           IND_OBRA
        ,SAP_ITF_IDF_CONS_CIVIL_COR.CNO_CODIGO                         CNO_CODIGO
        ,SAP_ITF_IDF_CONS_CIVIL_COR.IDF_NUM                            IDF_NUM_CONS_CIVIL
        ,SAP_ITF_IDF_CONS_CIVIL_COR.DOF_IMPORT_NUMERO                  DOF_IMPORT_CONS_CIVIL
        ,SAP_ITF_IDF_CONS_CIVIL_COR.INFORMANTE_EST_CODIGO              INFORMANTE_CONS_CIVIL
        ,SAP_ITF_DOF_PROCESSO_COR.NUM_PROC                             NUM_PROC
        ,SAP_ITF_DOF_PROCESSO_COR.IND_PROC                             IND_PROC
        ,SAP_ITF_DOF_PROCESSO_COR.SUSPENSAO_CODIGO                     SUSPENSAO_CODIGO             
        FROM SAP_ITF_DOF_COR                    SAP_ITF_DOF_IDF_COR,
             SAP_ITF_TOTAL_DOF_COR              SAP_ITF_TOTAL_DOF_COR,
             SAP_ITF_IDF_COR                    SAP_ITF_IDF_COR,
             SAP_ITF_IDF_LOTE_MED_COR           SAP_ITF_IDF_LOTE_MED_COR,
             SAP_ITF_IDF_CONS_CIVIL_COR         SAP_ITF_IDF_CONS_CIVIL_COR,
             SAP_ITF_DOF_PROCESSO_COR           SAP_ITF_DOF_PROCESSO_COR

       WHERE SAP_ITF_DOF_IDF_COR.ID = SAP_ITF_TOTAL_DOF_COR.YJ1BNFDOC_ID(+)
         and SAP_ITF_DOF_IDF_COR.ID = SAP_ITF_IDF_COR.YJ1BNFDOC_ID(+)
         and SAP_ITF_DOF_IDF_COR.ID = SAP_ITF_DOF_PROCESSO_COR.YJ1BNFDOC_ID(+)
         and SAP_ITF_DOF_IDF_COR.xml_id =
             NVL(p_xml_id, SAP_ITF_DOF_IDF_COR.XML_ID)
         and nvl(SAP_ITF_DOF_IDF_COR.CPROG, '*') not in
             ('YSDMMPMSEG', 'YGRPSALKL', 'YMMPANLA')
         and sap_itf_idf_cor.id = SAP_ITF_IDF_LOTE_MED_COR.yj1bnflin_id(+)
         AND sap_itf_idf_cor.id = SAP_ITF_IDF_CONS_CIVIL_COR.yj1bnflin_id(+)
         and SAP_ITF_DOF_IDF_COR.ctrl_critica >= v_critica_i
         and SAP_ITF_DOF_IDF_COR.ctrl_critica <= v_critica_f
         and nvl(sap_itf_dof_idf_cor.est_codigo, '%') like p_filtro_estab
         and SAP_ITF_DOF_IDF_COR.job = p_num_job
      
       ORDER BY SAP_ITF_DOF_IDF_COR.xml_id,
                SAP_ITF_DOF_IDF_COR.DOCNUM,
                SAP_ITF_IDF_COR.ITMNUM;

	mdof cdof%ROWTYPE;

    PROCEDURE put_line( str VARCHAR2 ) is
      BEGIN
        IF mprocesso IS NOT NULL THEN
          syn_out.put_line(str);
        END IF;
      END;

    PROCEDURE INC_COR_DOF is
      BEGIN
        INSERT INTO COR_DOF (
        INFORMANTE_EST_CODIGO
        ,IND_ENTRADA_SAIDA
        ,CTRL_SITUACAO_DOF
        ,SERIE_SUBSERIE
        ,ANO_MES_EMISSAO
        ,NUMERO
        ,DH_EMISSAO
        ,DT_FATO_GERADOR_IMPOSTO
        ,VL_SEGURO
        ,PESO_BRUTO_KG
        ,PESO_LIQUIDO_KG
        ,VL_FRETE
        ,VT_CODIGO
        ,EDOF_CODIGO
        ,REMETENTE_PFJ_CODIGO
        ,EMITENTE_PFJ_CODIGO
        ,DESTINATARIO_PFJ_CODIGO
        ,TRANSPORTADOR_PFJ_CODIGO
        ,ENTREGA_PFJ_CODIGO
        ,RETIRADA_PFJ_CODIGO
        ,DESTINATARIO_LOC_CODIGO
        ,EMITENTE_LOC_CODIGO
        ,REMETENTE_LOC_CODIGO
        ,TRANSPORTADOR_LOC_CODIGO
        ,ENTREGA_LOC_CODIGO
        ,RETIRADA_LOC_CODIGO
        ,DOF_IMPORT_NUMERO
        ,SIS_CODIGO
        ,VL_OUTRAS_DESPESAS
        ,CPAG_CODIGO
        ,CTRL_CONTEUDO
        ,VL_TOTAL_CONTABIL
        ,VL_TOTAL_FATURADO
        ,MODO_EMISSAO
        ,MUN_CODIGO_ISS
        ,TIPO
        , codigo_do_site
        , dof_sequence
        , ctrl_origem_registro
        ,cobranca_pfj_codigo
        ,cobranca_loc_codigo
        ,num_placa_veiculo
        ,substituido_pfj_codigo
        ,tipo_pgto
        ,num_titulo
        ,ind_iss_retido_fonte
        , ind_tit
        , desc_titulo
        , qtd_prestacoes
        , CNPJ_COL
        , IE_COL
        , COD_MUN_COL
        , CNPJ_ENTG
        , IE_ENTG
        , NFE_LOCALIZADOR
        , COD_MUN_ENTG
        , ind_resp_frete
        , IND_USA_IF_CALC_IMP
        , DT_EXE_SERV
        , MUN_PRES_SERVICO
        , TIPO_CTE
        , VEIC_ID_COMP
        , UF_VEIC_COMP
        , VEIC_ID_COMP_2
        ,UF_VEIC_COMP_2
        ,PLACA_VEICULO_UF_CODIGO
        ,CONTA_CONTABIL
        ,vl_abat_nt
         MUN_COD_ORIGEM,
         MUN_COD_DESTINO,
         DT_REFER_EXT
        ) VALUES (
         mdof.INFORMANTE_EST_CODIGO
        ,mdof.IND_ENTRADA_SAIDA
        ,mdof.CTRL_SITUACAO_DOF
        ,mdof.SERIE_SUBSERIE
        ,mdof.ANO_MES_EMISSAO
        ,mdof.NUMERO
        ,mdof.DH_EMISSAO
        ,mdof.DT_FATO_GERADOR_IMPOSTO
        ,mdof.VL_SEGURO
        ,mdof.PESO_BRUTO
        ,mdof.PESO_LIQUIDO
        ,mdof.VL_FRETE
        ,mdof.VT_CODIGO
        ,mdof.EDOF_CODIGO
        ,mdof.REMETENTE_PFJ_CODIGO
        ,mdof.EMITENTE_PFJ_CODIGO
        ,mdof.DESTINATARIO_PFJ_CODIGO
        ,mdof.TRANSPORTADOR_PFJ_CODIGO
        ,mdof.ENTREGA_PFJ_CODIGO
        ,mdof.RETIRADA_PFJ_CODIGO
        ,mdof.DESTINATARIO_LOC_CODIGO
        ,mdof.EMITENTE_LOC_CODIGO
        ,mdof.REMETENTE_LOC_CODIGO
        ,mdof.TRANSPORTADOR_LOC_CODIGO
        ,mdof.ENTREGA_LOC_CODIGO
        ,mdof.RETIRADA_LOC_CODIGO
        ,mdof.DOF_IMPORT_NUMERO
        ,mdof.SIS_CODIGO
        ,mdof.VL_OUTRAS_DESPESAS
        ,mdof.CPAG_CODIGO
        ,mdof.CTRL_CONTEUDO
        ,mdof.VL_TOTAL_CONTABIL
        ,mdof.VL_TOTAL_FATURADO
        ,mdof.MODO_EMISSAO
        ,mdof.MUN_CODIGO_ISS
        ,mdof.Tipo
        ,mdof.codigo_do_site
        ,mdof.dof_sequence
        ,mdof.ctrl_origem_registro
        ,mdof.cobranca_pfj_codigo
        ,mdof.cobranca_loc_codigo
        ,mdof.num_placa_veiculo
        ,mdof.substituido_pfj_codigo
        ,mdof.tipo_pgto
        ,mdof.num_titulo
        ,mdof.ind_iss_retido_fonte
        , mdof.ind_tit
        , mdof.desc_titulo
        , mdof.qtd_prestacoes
        , mdof.CNPJ_COL
        , mdof.IE_COL
        , mdof.COD_MUN_COL
        , mdof.CNPJ_ENTG
        , mdof.IE_ENTG
        , mdof.NFE_LOCALIZADOR
        , mdof.COD_MUN_ENTG
        , mdof.ind_resp_frete
        , 'N'
        , mdof.DT_EXE_SERV
        , mdof.MUN_PRES_SERVICO
        , mdof.TIPO_CTE
        , mdof.VEIC_ID_COMP
        , mdof.UF_VEIC_COMP
        , mdof.VEIC_ID_COMP_2
        , mdof.UF_VEIC_COMP_2
        , mdof.PLACA_VEICULO_UF_CODIGO
        , mdof.CONTA_CONTABIL
        ,mdof.vl_abat_nt
        ,mdof.MUN_COD_ORIGEM
        ,mdof.MUN_COD_DESTINO
        ,mdof.DT_REFER_EXT
         );
      END;

    PROCEDURE ALT_COR_DOF IS
      BEGIN
        UPDATE  COR_DOF SET
           IND_ENTRADA_SAIDA = mdof.IND_ENTRADA_SAIDA
          ,SERIE_SUBSERIE    = mdof.SERIE_SUBSERIE
          ,ANO_MES_EMISSAO   = mdof.ANO_MES_EMISSAO
          ,NUMERO = mdof.NUMERO
          ,EDOF_CODIGO = mdof.EDOF_CODIGO
          ,EMITENTE_PFJ_CODIGO = mdof.EMITENTE_PFJ_CODIGO
          ,CTRL_SITUACAO_DOF   = mdof.CTRL_SITUACAO_DOF
          ,DH_EMISSAO = mdof.DH_EMISSAO
          ,DT_FATO_GERADOR_IMPOSTO = mdof.DT_FATO_GERADOR_IMPOSTO
          ,VL_SEGURO = mdof.VL_SEGURO
          ,PESO_BRUTO_KG   = mdof.PESO_BRUTO
          ,PESO_LIQUIDO_KG = mdof.PESO_LIQUIDO
          ,VL_FRETE  = mdof.VL_FRETE
          ,VT_CODIGO = mdof.VT_CODIGO
          ,REMETENTE_PFJ_CODIGO = mdof.REMETENTE_PFJ_CODIGO
          ,DESTINATARIO_PFJ_CODIGO  = mdof.DESTINATARIO_PFJ_CODIGO
          ,TRANSPORTADOR_PFJ_CODIGO = mdof.TRANSPORTADOR_PFJ_CODIGO
          ,ENTREGA_PFJ_CODIGO  = mdof.ENTREGA_PFJ_CODIGO
          ,RETIRADA_PFJ_CODIGO = mdof.RETIRADA_PFJ_CODIGO
          ,DESTINATARIO_LOC_CODIGO  = mdof.DESTINATARIO_LOC_CODIGO
          ,EMITENTE_LOC_CODIGO      = mdof.EMITENTE_LOC_CODIGO
          ,REMETENTE_LOC_CODIGO     = mdof.REMETENTE_LOC_CODIGO
          ,TRANSPORTADOR_LOC_CODIGO = mdof.TRANSPORTADOR_LOC_CODIGO
          ,ENTREGA_LOC_CODIGO       = mdof.ENTREGA_LOC_CODIGO
          ,RETIRADA_LOC_CODIGO      = mdof.RETIRADA_LOC_CODIGO
          ,DOF_IMPORT_NUMERO  = mdof.DOF_IMPORT_NUMERO
          ,SIS_CODIGO   = mdof.SIS_CODIGO
          ,VL_OUTRAS_DESPESAS = mdof.VL_OUTRAS_DESPESAS
          ,CPAG_CODIGO  = mdof.CPAG_CODIGO
          ,VL_TOTAL_CONTABIL = mdof.VL_TOTAL_CONTABIL
          ,VL_TOTAL_FATURADO = mdof.VL_TOTAL_FATURADO
          ,MODO_EMISSAO = mdof.MODO_EMISSAO
          ,MUN_CODIGO_ISS = mdof.MUN_CODIGO_ISS
          ,TIPO = mdof.Tipo
          ,cobranca_pfj_codigo = mdof.cobranca_pfj_codigo
          ,cobranca_loc_codigo = mdof.cobranca_loc_codigo
          ,num_placa_veiculo   = mdof.num_placa_veiculo
          ,substituido_pfj_codigo = mdof.substituido_pfj_codigo
          ,tipo_pgto = mdof.tipo_pgto
          ,num_titulo = mdof.num_titulo
          ,ind_iss_retido_fonte = mdof.ind_iss_retido_fonte
          , ind_tit = mdof.ind_tit
          , desc_titulo = mdof.desc_titulo
          , qtd_prestacoes = mdof.qtd_prestacoes
          , cfop_codigo = null
          , CNPJ_COL = mdof.CNPJ_COL
          , IE_COL = mdof.IE_COL
          , COD_MUN_COL = mdof.COD_MUN_COL
          , CNPJ_ENTG = mdof.CNPJ_ENTG
          , IE_ENTG = mdof.IE_ENTG
          , NFE_LOCALIZADOR = mdof.NFE_LOCALIZADOR
          , COD_MUN_ENTG = mdof.COD_MUN_ENTG
          , ind_resp_frete = mdof.ind_resp_frete
          , IND_USA_IF_CALC_IMP = 'N'
          , DT_EXE_SERV = mdof.DT_EXE_SERV
          , MUN_PRES_SERVICO = mdof.MUN_PRES_SERVICO
          , TIPO_CTE = mdof.TIPO_CTE
          , VEIC_ID_COMP = mdof.VEIC_ID_COMP
          , UF_VEIC_COMP = mdof.UF_VEIC_COMP
          , VEIC_ID_COMP_2 = mdof.VEIC_ID_COMP_2
          ,UF_VEIC_COMP_2 = mdof.UF_VEIC_COMP_2
          ,PLACA_VEICULO_UF_CODIGO = mdof.PLACA_VEICULO_UF_CODIGO
          , CONTA_CONTABIL = mdof.CONTA_CONTABIL
          ,vl_abat_nt = mdof.vl_abat_nt
          ,DT_REFER_EXT             = mdof.DT_REFER_EXT
          ,MUN_COD_ORIGEM           = mdof.MUN_COD_ORIGEM
          ,MUN_COD_DESTINO          = mdof.MUN_COD_DESTINO         
          WHERE
             INFORMANTE_EST_CODIGO = mdof.INFORMANTE_EST_CODIGO
             AND DOF_IMPORT_NUMERO = mdof.DOF_IMPORT_NUMERO
           ;
         END;

    PROCEDURE EXC_COR_DOF IS
      BEGIN
        DELETE  FROM COR_DOF
        WHERE INFORMANTE_EST_CODIGO = mdof.INFORMANTE_EST_CODIGO
        AND DOF_IMPORT_NUMERO = mdof.DOF_IMPORT_NUMERO
        ;
      END;

    PROCEDURE INC_COR_DOF_INUTILIZADA IS
      mdata CORAPI_DOF.data;
    BEGIN
      mdata.v.CODIGO_DO_SITE           := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_SEQUENCE             := mdof.DOF_SEQUENCE;
      mdata.v.CTRL_CONTEUDO            := mdof.CTRL_CONTEUDO;
      mdata.v.CTRL_SITUACAO_DOF        := mdof.CTRL_SITUACAO_DOF;
      mdata.v.DOF_IMPORT_NUMERO        := mdof.DOF_IMPORT_NUMERO;
      mdata.v.EDOF_CODIGO              := mdof.EDOF_CODIGO;
      mdata.v.ID                       := mdof.DOF_ID;
      mdata.v.IND_ENTRADA_SAIDA        := mdof.IND_ENTRADA_SAIDA;
      mdata.v.INFORMANTE_EST_CODIGO    := mdof.INFORMANTE_EST_CODIGO;
      mdata.v.MODO_EMISSAO             := mdof.MODO_EMISSAO;
      mdata.v.SERIE_SUBSERIE           := mdof.SERIE_SUBSERIE;
      mdata.v.SIS_CODIGO               := mdof.SIS_CODIGO;
      mdata.v.COBRANCA_LOC_CODIGO      := mdof.COBRANCA_LOC_CODIGO;
      mdata.v.COBRANCA_PFJ_CODIGO      := mdof.COBRANCA_PFJ_CODIGO;
      mdata.v.DESTINATARIO_LOC_CODIGO  := mdof.DESTINATARIO_LOC_CODIGO;
      mdata.v.DESTINATARIO_PFJ_CODIGO  := mdof.DESTINATARIO_PFJ_CODIGO;
      mdata.v.DH_EMISSAO               := mdof.DH_EMISSAO;
      mdata.v.DT_FATO_GERADOR_IMPOSTO  := mdof.DH_EMISSAO;
      mdata.v.EMITENTE_LOC_CODIGO      := mdof.EMITENTE_LOC_CODIGO;
      mdata.v.EMITENTE_PFJ_CODIGO      := mdof.EMITENTE_PFJ_CODIGO;
      mdata.v.ENTREGA_LOC_CODIGO       := mdof.ENTREGA_LOC_CODIGO;
      mdata.v.ENTREGA_PFJ_CODIGO       := mdof.ENTREGA_PFJ_CODIGO;
      mdata.v.MDOF_CODIGO              := '55';
      mdata.v.NOP_CODIGO               := mdof.NOP_CODIGO_INUTIL;
      mdata.v.NUMERO                   := mdof.NUMERO;
      mdata.v.REMETENTE_LOC_CODIGO     := mdof.REMETENTE_LOC_CODIGO;
      mdata.v.REMETENTE_PFJ_CODIGO     := mdof.REMETENTE_PFJ_CODIGO;
      mdata.v.RETIRADA_LOC_CODIGO      := mdof.RETIRADA_LOC_CODIGO;
      mdata.v.RETIRADA_PFJ_CODIGO      := mdof.RETIRADA_PFJ_CODIGO;
      mdata.v.TRANSPORTADOR_LOC_CODIGO := mdof.TRANSPORTADOR_LOC_CODIGO;
      mdata.v.TRANSPORTADOR_PFJ_CODIGO := mdof.TRANSPORTADOR_PFJ_CODIGO;
      CORAPI_DOF.ins(mdata);
    END;
  
    PROCEDURE ALT_COR_DOF_INUTILIZADA IS
      mdata CORAPI_DOF.data;
    BEGIN
      mdata.v.CODIGO_DO_SITE           := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_SEQUENCE             := mdof.DOF_SEQUENCE;
      mdata.v.CTRL_CONTEUDO            := mdof.CTRL_CONTEUDO;
      mdata.v.CTRL_SITUACAO_DOF        := mdof.CTRL_SITUACAO_DOF;
      mdata.v.DOF_IMPORT_NUMERO        := mdof.DOF_IMPORT_NUMERO;
      mdata.v.EDOF_CODIGO              := mdof.EDOF_CODIGO;
      mdata.v.ID                       := mdof.DOF_ID;
      mdata.v.IND_ENTRADA_SAIDA        := mdof.IND_ENTRADA_SAIDA;
      mdata.v.INFORMANTE_EST_CODIGO    := mdof.INFORMANTE_EST_CODIGO;
      mdata.v.MODO_EMISSAO             := mdof.MODO_EMISSAO;
      mdata.v.SERIE_SUBSERIE           := mdof.SERIE_SUBSERIE;
      mdata.v.SIS_CODIGO               := mdof.SIS_CODIGO;
      mdata.v.COBRANCA_LOC_CODIGO      := mdof.COBRANCA_LOC_CODIGO;
      mdata.v.COBRANCA_PFJ_CODIGO      := mdof.COBRANCA_PFJ_CODIGO;
      mdata.v.DESTINATARIO_LOC_CODIGO  := mdof.DESTINATARIO_LOC_CODIGO;
      mdata.v.DESTINATARIO_PFJ_CODIGO  := mdof.DESTINATARIO_PFJ_CODIGO;
      mdata.v.DH_EMISSAO               := mdof.DH_EMISSAO;
      mdata.v.DT_FATO_GERADOR_IMPOSTO  := mdof.DH_EMISSAO;
      mdata.v.EMITENTE_LOC_CODIGO      := mdof.EMITENTE_LOC_CODIGO;
      mdata.v.EMITENTE_PFJ_CODIGO      := mdof.EMITENTE_PFJ_CODIGO;
      mdata.v.ENTREGA_LOC_CODIGO       := mdof.ENTREGA_LOC_CODIGO;
      mdata.v.ENTREGA_PFJ_CODIGO       := mdof.ENTREGA_PFJ_CODIGO;
      mdata.v.MDOF_CODIGO              := '55';
      mdata.v.NOP_CODIGO               := mdof.NOP_CODIGO_INUTIL;
      mdata.v.NUMERO                   := mdof.NUMERO;
      mdata.v.REMETENTE_LOC_CODIGO     := mdof.REMETENTE_LOC_CODIGO;
      mdata.v.REMETENTE_PFJ_CODIGO     := mdof.REMETENTE_PFJ_CODIGO;
      mdata.v.RETIRADA_LOC_CODIGO      := mdof.RETIRADA_LOC_CODIGO;
      mdata.v.RETIRADA_PFJ_CODIGO      := mdof.RETIRADA_PFJ_CODIGO;
      mdata.v.TRANSPORTADOR_LOC_CODIGO := mdof.TRANSPORTADOR_LOC_CODIGO;
      mdata.v.TRANSPORTADOR_PFJ_CODIGO := mdof.TRANSPORTADOR_PFJ_CODIGO;
      mdata.i.CODIGO_DO_SITE           := TRUE;
      mdata.i.DOF_SEQUENCE             := TRUE;
      mdata.i.CTRL_CONTEUDO            := TRUE;
      mdata.i.CTRL_SITUACAO_DOF        := TRUE;
      mdata.i.DOF_IMPORT_NUMERO        := TRUE;
      mdata.i.EDOF_CODIGO              := TRUE;
      mdata.i.ID                       := TRUE;
      mdata.i.IND_ENTRADA_SAIDA        := TRUE;
      mdata.i.INFORMANTE_EST_CODIGO    := TRUE;
      mdata.i.MODO_EMISSAO             := TRUE;
      mdata.i.SERIE_SUBSERIE           := TRUE;
      mdata.i.SIS_CODIGO               := TRUE;
      mdata.i.COBRANCA_LOC_CODIGO      := TRUE;
      mdata.i.COBRANCA_PFJ_CODIGO      := TRUE;
      mdata.i.DESTINATARIO_LOC_CODIGO  := TRUE;
      mdata.i.DESTINATARIO_PFJ_CODIGO  := TRUE;
      mdata.i.DH_EMISSAO               := TRUE;
      mdata.i.DT_FATO_GERADOR_IMPOSTO  := TRUE;
      mdata.i.EMITENTE_LOC_CODIGO      := TRUE;
      mdata.i.EMITENTE_PFJ_CODIGO      := TRUE;
      mdata.i.ENTREGA_LOC_CODIGO       := TRUE;
      mdata.i.ENTREGA_PFJ_CODIGO       := TRUE;
      mdata.i.MDOF_CODIGO              := TRUE;
      mdata.i.NOP_CODIGO               := TRUE;
      mdata.i.NUMERO                   := TRUE;
      mdata.i.REMETENTE_LOC_CODIGO     := TRUE;
      mdata.i.REMETENTE_PFJ_CODIGO     := TRUE;
      mdata.i.RETIRADA_LOC_CODIGO      := TRUE;
      mdata.i.RETIRADA_PFJ_CODIGO      := TRUE;
      mdata.i.TRANSPORTADOR_LOC_CODIGO := TRUE;
      mdata.i.TRANSPORTADOR_PFJ_CODIGO := TRUE;
      CORAPI_DOF.upd(mdata);
    END;
  
    PROCEDURE EXC_COR_DOF_INUTILIZADA IS
      mdata CORAPI_DOF.data;
    BEGIN
    
      syn_itf_validacoes.exclui_dependencias_dof(mdof.codigo_do_site,
                                                 mdof.dof_sequence,
                                                 mdof.id);
    
      mdata.v.CODIGO_DO_SITE := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_SEQUENCE   := mdof.DOF_SEQUENCE;
      CORAPI_DOF.del(mdata);
    
    END;
	  
    PROCEDURE INC_COR_DOF_ASSOCIADO IS
      BEGIN
        INSERT INTO COR_DOF_ASSOCIADO (
        CODIGO_DO_SITE
       ,DOF_SEQUENCE
       ,DOF_ASSOC_SEQUENCE
       ,ANO_MES_EMISSAO_ASSOC
       ,DH_EMISSAO_ASSOC
       ,SERIE_SUBSERIE_ASSOC
       ,NUMERO_ASSOC
       ,EMITENTE_PFJ_CODIGO_ASSOC
       ,EDOF_CODIGO_ASSOC
       ,VL_TOTAL_DOF_ASSOC
       ,TADOC_CODIGO
       ,DESTINATARIO_PFJ_CODIGO_ASSOC
       ,IND_ENTRADA_SAIDA_ASSOC /*cdc  1790854 - 15/09/09 - Inclus�o do campo IND_ENTRADA_SAIDA_ASSOC*/
       ,id_dof
       ,nfe_localizador
       ) VALUES (
        mdof.CODIGO_DO_SITE
       ,mdof.DOF_SEQUENCE
       ,mdof.DOF_ASSOC_SEQUENCE
       ,mdof.ANO_MES_EMISSAO_ASSOC
       ,mdof.DH_EMISSAO_ASSOC
       ,mdof.SERIE_SUBSERIE_ASSOC
       ,mdof.NUMERO_ASSOC
       ,mdof.EMITENTE_PFJ_CODIGO_ASSOC
       ,mdof.EDOF_CODIGO_ASSOC
       ,mdof.VL_TOTAL_DOF_ASSOC
       ,mdof.TADOC_CODIGO
       ,mdof.DESTINATARIO_PFJ_CODIGO_ASSOC
       ,mdof.IND_ENTRADA_SAIDA_ASSOC /*cdc  1790854 - 15/09/09 - Inclus�o do campo IND_ENTRADA_SAIDA_ASSOC*/
       ,mdof.id_dof
       ,mdof.nfe_localizador_assoc
       );
      END;

    PROCEDURE ALT_COR_DOF_ASSOCIADO IS
    BEGIN
      UPDATE COR_DOF_ASSOCIADO
         SET DH_EMISSAO_ASSOC              = mdof.DH_EMISSAO_ASSOC,
             VL_TOTAL_DOF_ASSOC            = mdof.VL_TOTAL_DOF_ASSOC,
             EMITENTE_PFJ_CODIGO_ASSOC     = mdof.EMITENTE_PFJ_CODIGO_ASSOC,
             EDOF_CODIGO_ASSOC             = mdof.EDOF_CODIGO_ASSOC,
             NUMERO_ASSOC                  = mdof.NUMERO_ASSOC,
             SERIE_SUBSERIE_ASSOC          = mdof.SERIE_SUBSERIE_ASSOC,
             ANO_MES_EMISSAO_ASSOC         = mdof.ANO_MES_EMISSAO_ASSOC,
             DESTINATARIO_PFJ_CODIGO_ASSOC = MDOF.DESTINATARIO_PFJ_CODIGO_ASSOC,
             IND_ENTRADA_SAIDA_ASSOC       = mdof.IND_ENTRADA_SAIDA_ASSOC,
             ID_DOF                        = mdof.id_dof,
             nfe_localizador               = mdof.nfe_localizador_assoc
      
       WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
         AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
         AND DOF_ASSOC_SEQUENCE = mdof.DOF_ASSOC_SEQUENCE;
    END;

    PROCEDURE EXC_COR_DOF_ASSOCIADO is
      BEGIN
        DELETE  FROM COR_DOF_ASSOCIADO
        WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
        AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
        AND DOF_ASSOC_SEQUENCE = mdof.DOF_ASSOC_SEQUENCE
        ;
      END;

    PROCEDURE INC_COR_DOF_VOLUME_CARGA IS
      BEGIN
        INSERT INTO COR_DOF_VOLUME_CARGA (
        CODIGO_DO_SITE
        ,DOF_SEQUENCE
        ,ESPECIE_VOLUMES
        ,QTD_VOLUMES
        ,PESO_REEMBALAGEM_KG
        )VALUES (
         mdof.CODIGO_DO_SITE
        ,mdof.DOF_SEQUENCE
        ,mdof.ESPECIE_VOLUMES
        ,mdof.QTD_VOLUMES
        ,mdof.PESO_REEMBALAGEM_KG
        );
      END;

    PROCEDURE ALT_COR_DOF_VOLUME_CARGA IS
      BEGIN
        UPDATE COR_DOF_VOLUME_CARGA SET
        QTD_VOLUMES = mdof.QTD_VOLUMES
        ,PESO_REEMBALAGEM_KG  = mdof.PESO_REEMBALAGEM_KG
        WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
        AND DOF_SEQUENCE  = mdof.DOF_SEQUENCE
        AND ESPECIE_VOLUMES = mdof.ESPECIE_VOLUMES
        ;
      END;

    PROCEDURE EXC_COR_DOF_VOLUME_CARGA IS
      BEGIN
        DELETE FROM COR_DOF_VOLUME_CARGA
        WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
        AND DOF_SEQUENCE  = mdof.DOF_SEQUENCE
        AND ESPECIE_VOLUMES = mdof.ESPECIE_VOLUMES
        ;
      END;

    PROCEDURE INC_COR_DOF_PROCESSO IS
    BEGIN
      INSERT INTO COR_DOF_PROCESSO
        (CODIGO_DO_SITE,
         DOF_SEQUENCE,
         NUM_PROC,
         IND_PROC,
         SUSPENSAO_CODIGO
         )
      VALUES
        (mdof.CODIGO_DO_SITE,
         mdof.DOF_SEQUENCE,
         mdof.NUM_PROC,
         mdof.IND_PROC,
         mdof.SUSPENSAO_CODIGO);
    END;
  
    PROCEDURE ALT_COR_DOF_PROCESSO IS
    BEGIN
      UPDATE COR_DOF_PROCESSO
         SET IND_PROC         = mdof.IND_PROC             
      WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
         AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
         AND NUM_PROC = mdof.NUM_PROC
         AND SUSPENSAO_CODIGO = mdof.SUSPENSAO_CODIGO;
    END;
  
    PROCEDURE EXC_COR_DOF_PROCESSO IS
    BEGIN
      DELETE FROM COR_DOF_PROCESSO
       WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
         AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
         AND NUM_PROC = mdof.NUM_PROC
         AND SUSPENSAO_CODIGO = mdof.SUSPENSAO_CODIGO;
    END;
  
    PROCEDURE INC_COR_IDF IS
      BEGIN
        cor_idf_cons_pkg.enable;
        INSERT INTO COR_IDF (
         ID
        ,IDF_NUM
        ,SUBCLASSE_IDF
        ,AM_CODIGO
        ,OM_CODIGO
        ,MERC_CODIGO
        ,SISS_CODIGO
        ,PRES_CODIGO
        ,QTD
        ,ENTSAI_UNI_CODIGO
        ,ESTOQUE_UNI_CODIGO
        ,PRECO_UNITARIO
        ,VL_AJUSTE_PRECO_TOTAL
        ,PRECO_TOTAL
        ,VL_RATEIO_FRETE
        ,VL_RATEIO_SEGURO
        ,VL_RATEIO_ODA
        ,CFOP_CODIGO
        ,STP_CODIGO
        ,STC_CODIGO
        ,NBM_CODIGO
        ,IDF_TEXTO_COMPLEMENTAR
        ,ALIQ_ICMS
        ,ALIQ_DIFA
        ,ALIQ_IPI
        ,ALIQ_ISS
        ,ALIQ_STT
        ,ALIQ_STF
        ,PERC_IRRF
        ,VL_BASE_ICMS
        ,VL_BASE_IPI
        ,VL_BASE_IRRF
        ,VL_BASE_ISS
        ,VL_BASE_STT
        ,VL_BASE_STF
        ,VL_TRIBUTAVEL_ICMS
        ,VL_TRIBUTAVEL_IPI
        ,VL_TRIBUTAVEL_DIFA
        ,VL_TRIBUTAVEL_STT
        ,VL_TRIBUTAVEL_STF
        ,VL_DIFA
        ,VL_ICMS
        ,VL_IPI
        ,VL_IRRF
        ,VL_ISS
        ,VL_STT
        ,VL_STF
        ,ORD_IMPRESSAO
        ,CODIGO_DO_SITE
        ,DOF_SEQUENCE
        ,DOF_ID
        ,NOP_CODIGO
        ,VL_CONTABIL
        ,VL_FATURADO
        ,VL_FISCAL
        ,VL_RATEIO_AJUSTE_PRECO
        ,TIPO_COMPLEMENTO
        ,VL_ISENTO_ICMS
        ,VL_ISENTO_IPI
        ,VL_OUTROS_ICMS
        ,VL_OUTROS_IPI
        ,VL_ALIQ_PIS
        ,VL_ALIQ_COFINS
        ,ALIQ_PIS_RET
        ,ALIQ_COFINS_RET
        ,ALIQ_CSLL_RET
        ,VL_BASE_PIS
        ,VL_BASE_COFINS
        ,VL_BASE_PIS_RET
        ,VL_BASE_COFINS_RET
        ,VL_BASE_CSLL_RET
        ,VL_IMPOSTO_PIS
        ,VL_IMPOSTO_COFINS
        ,VL_PIS_RET
        ,VL_COFINS_RET
        ,VL_CSLL_RET
        ,PESO_BRUTO_KG -- CBT 22/09/04 Ocor738800
        ,peso_liquido_kg
        ,dt_di
        ,num_di
        ,chassi_veic
        ,codigo_retencao
        ,im_subcontratacao
        ,ALIQ_II
        ,VL_II
        ,VL_BASE_II
        , conta_contabil
        , QTD_BASE_PIS
        , QTD_BASE_COFINS
        , IND_MOVIMENTA_ESTOQUE
        ,STA_CODIGO
        ,STN_CODIGO
        , VL_BASE_SEST
        , VL_BASE_SENAT
        , ALIQ_SEST
        , ALIQ_SENAT
        , VL_SEST
        , VL_SENAT
        , VL_INSS
        , VL_BASE_INSS
        , ALIQ_INSS
        , VL_INSS_RET
        , VL_BASE_INSS_RET
        , ALIQ_INSS_RET
        , IND_ISS_RETIDO_FONTE
        , IND_INCIDENCIA_PIS_ST
        , VL_BASE_PIS_ST
        , ALIQ_PIS_ST
        , VL_PIS_ST
        , IND_INCIDENCIA_COFINS_ST
        , VL_BASE_COFINS_ST
        , ALIQ_COFINS_ST
        , VL_COFINS_ST
        , COD_CCUS
        , NAT_REC_PISCOFINS
        , NAT_REC_PISCOFINS_DESCR
        , VL_CRED_PIS_REC_TRIB
        , VL_CRED_PIS_REC_NAO_TRIB
        , VL_CRED_PIS_REC_EXPO
        , VL_CRED_COFINS_REC_TRIB
        , VL_CRED_COFINS_REC_NAO_TRIB
        , VL_CRED_COFINS_REC_EXPO
        , VL_BASE_ICMS_DESC_L
        , VL_TRIBUTAVEL_ICMS_DESC_L
        , ALIQ_ICMS_DESC_L
        , VL_ICMS_DESC_L
        , IND_NAT_FRT_PISCOFINS
        , VL_DEDUCAO_DEPENDENTE_PRG
        , VL_DEDUCAO_PENSAO_PRG
        , PERC_PART_CI
        , FCI_NUMERO
        ,vl_base_stf_fronteira
        ,vl_stf_fronteira
        ,vl_base_stf_ido
        ,vl_stf_ido
        ,tipo_stf
        ,perc_icms_part_rem
        ,vl_base_icms_part_rem
        ,aliq_icms_part_rem
        ,vl_icms_part_rem
        ,perc_icms_part_dest
        ,vl_base_icms_part_dest
        ,aliq_icms_part_dest
        ,vl_icms_part_dest
        ,aliq_icms_fcp
        ,vl_icms_fcp
        ,vl_base_icms_fcp
        ,cest_codigo
        ,ENQ_IPI_CODIGO
        ,Aliq_Difa_Icms_Part
        ,Desoneracao_Codigo
        ,VL_SERVICO_AE15
        ,ALIQ_INSS_AE15_RET
        ,VL_INSS_AE15_RET
        ,VL_SERVICO_AE20
        ,ALIQ_INSS_AE20_RET
        ,VL_INSS_AE20_RET
        ,VL_SERVICO_AE25
        ,ALIQ_INSS_AE25_RET
        ,VL_INSS_AE25_RET        
        ,ALIQ_ICMS_FCPST 
        ,VL_BASE_ICMS_FCPST
        ,VL_ICMS_FCPST
        ) VALUES (
         mdof.IDF_ID
        ,mdof.IDF_NUM
        ,mdof.SUBCLASSE_IDF
        ,mdof.AM_CODIGO
        ,mdof.OM_CODIGO
        ,mdof.MERC_CODIGO
        ,mdof.SISS_CODIGO
        ,mdof.PRES_CODIGO
        ,mdof.QTD
        ,mdof.ENTSAI_UNI_CODIGO
        ,mdof.ESTOQUE_UNI_CODIGO
        ,mdof.PRECO_UNITARIO
        ,mdof.VL_AJUSTE_PRECO_TOTAL
        ,mdof.PRECO_TOTAL_ITEM
        ,mdof.VL_RATEIO_FRETE
        ,mdof.VL_RATEIO_SEGURO
        ,mdof.VL_RATEIO_ODA
        ,mdof.CFOP_CODIGO
        ,mdof.STP_CODIGO
        ,mdof.STC_CODIGO
        ,mdof.NBM_CODIGO
        ,mdof.IDF_TEXTO_COMPLEMENTAR
        ,mdof.ALIQ_ICMS
        ,mdof.ALIQ_DIFA
        ,mdof.ALIQ_IPI
        ,mdof.ALIQ_ISS
        ,mdof.ALIQ_STT
        ,mdof.ALIQ_STF
        ,mdof.PERC_IRRF
        ,mdof.VL_BASE_ICMS
        ,mdof.VL_BASE_IPI
        ,mdof.VL_BASE_IRRF
        ,mdof.VL_BASE_ISS
        ,mdof.VL_BASE_STT
        ,mdof.VL_BASE_STF
        ,mdof.VL_TRIBUTAVEL_ICMS
        ,mdof.VL_TRIBUTAVEL_IPI
        ,mdof.VL_TRIBUTAVEL_DIFA
        ,mdof.VL_TRIBUTAVEL_STT
        ,mdof.VL_TRIBUTAVEL_STF
        ,mdof.VL_DIFA
        ,mdof.VL_ICMS
        ,mdof.VL_IPI
        ,mdof.VL_IRRF
        ,mdof.VL_ISS
        ,mdof.VL_STT
        ,mdof.VL_STF
        ,mdof.IDF_NUM
        ,mdof.CODIGO_DO_SITE
        ,mdof.DOF_SEQUENCE
        ,mdof.DOF_ID
        ,mdof.NOP_CODIGO_IDF
        ,mdof.VL_CONTABIL
        ,mdof.VL_FATURADO
        ,mdof.VL_FISCAL
        ,0
        ,mtipo_complemento
        ,mdof.VL_ISENTO_ICMS
        ,mdof.VL_ISENTO_IPI
        ,mdof.VL_OUTROS_ICMS
        ,mdof.VL_OUTROS_IPI
        ,mdof.VL_ALIQ_PIS
        ,mdof.VL_ALIQ_COFINS
        ,mdof.ALIQ_PIS_RET
        ,mdof.ALIQ_COFINS_RET
        ,mdof.ALIQ_CSLL_RET
        ,mdof.VL_BASE_PIS
        ,mdof.VL_BASE_COFINS
        ,mdof.VL_BASE_PIS_RET
        ,mdof.VL_BASE_COFINS_RET
        ,mdof.VL_BASE_CSLL_RET
        ,mdof.VL_IMPOSTO_PIS
        ,mdof.VL_IMPOSTO_COFINS
        ,mdof.VL_PIS_RET
        ,mdof.VL_COFINS_RET
        ,mdof.VL_CSLL_RET
        ,mdof.PESO_BRUTO_ITEM -- CBT 22/09/04 Ocor738800
        ,mdof.PESO_LIQUIDO_ITEM
        ,mdof.dt_di
        ,mdof.num_di
        ,mdof.chassi_veic
        ,mdof.codigo_retencao
        ,mdof.im_subcontratacao
        ,mdof.ALIQ_II
        ,mdof.VL_II
        ,mdof.VL_BASE_II
        ,mdof.conta_contabil
        ,mdof.QTD_BASE_PIS
        ,mdof.QTD_BASE_COFINS
        ,mdof.IND_MOVIMENTA_ESTOQUE
        ,mdof.STA_CODIGO
        ,mdof.STN_CODIGO
        , mdof.VL_BASE_SEST
        , mdof.VL_BASE_SENAT
        , mdof.ALIQ_SEST
        , mdof.ALIQ_SENAT
        , mdof.VL_SEST
        , mdof.VL_SENAT
        , mdof.VL_INSS
        , mdof.VL_BASE_INSS
        , mdof.ALIQ_INSS
        , mdof.VL_INSS_RET
        , mdof.VL_BASE_INSS_RET
        , mdof.ALIQ_INSS_RET
        , mdof.IND_ISS_RETIDO_FONTE
        , mdof.IND_INCIDENCIA_PIS_ST
        , mdof.VL_BASE_PIS_ST
        , mdof.ALIQ_PIS_ST
        , mdof.VL_PIS_ST
        , mdof.IND_INCIDENCIA_COFINS_ST
        , mdof.VL_BASE_COFINS_ST
        , mdof.ALIQ_COFINS_ST
        , mdof.VL_COFINS_ST
        , mdof.COD_CCUS
        , mdof.NAT_REC_PISCOFINS
        , mdof.NAT_REC_PISCOFINS_DESCR
        ,mdof.VL_CRED_PIS_REC_TRIB
        ,mdof.VL_CRED_PIS_REC_NAO_TRIB
        ,mdof.VL_CRED_PIS_REC_EXPO
        ,mdof.VL_CRED_COFINS_REC_TRIB
        ,mdof.VL_CRED_COFINS_REC_NAO_TRIB
        ,mdof.VL_CRED_COFINS_REC_EXPO
        ,mdof.VL_BASE_ICMS_DESC_L
        ,mdof.VL_TRIBUTAVEL_ICMS_DESC_L
        ,mdof.ALIQ_ICMS_DESC_L
        ,mdof.VL_ICMS_DESC_L
        ,mdof.IND_NAT_FRT_PISCOFINS
        ,mdof.VL_DEDUCAO_DEPENDENTE_PRG
        ,mdof.VL_DEDUCAO_PENSAO_PRG
        ,mdof.PERC_PART_CI
        ,mdof.NFCI
        ,mdof.vl_base_stf_fronteira
        ,mdof.vl_stf_fronteira
        ,mdof.vl_base_stf_ido
        ,mdof.vl_stf_ido
        ,mdof.tipo_stf
        ,mdof.perc_icms_part_rem
        ,mdof.vl_base_icms_part_rem
        ,mdof.aliq_icms_part_rem
        ,mdof.vl_icms_part_rem
        ,mdof.perc_icms_part_dest
        ,mdof.vl_base_icms_part_dest
        ,mdof.aliq_icms_part_dest
        ,mdof.vl_icms_part_dest
        ,mdof.aliq_icms_fcp
        ,mdof.vl_icms_fcp
        ,mdof.vl_base_icms_fcp
        ,mdof.cest_codigo
        ,mdof.ENQ_IPI_CODIGO
        ,mdof.aliq_icms_part_dest_tmp - mdof.aliq_icms
        ,mdof.Desoneracao_Codigo
        ,mdof.VL_SERVICO_AE15
        ,mdof.ALIQ_INSS_AE15_RET
        ,mdof.VL_INSS_AE15_RET
        ,mdof.VL_SERVICO_AE20
        ,mdof.ALIQ_INSS_AE20_RET
        ,mdof.VL_INSS_AE20_RET
        ,mdof.VL_SERVICO_AE25
        ,mdof.ALIQ_INSS_AE25_RET
        ,mdof.VL_INSS_AE25_RET        
        ,mdof.ALIQ_ICMS_FCPST 
        ,mdof.VL_BASE_ICMS_FCPST
        ,mdof.VL_ICMS_FCPST         
        );
      END;

    PROCEDURE ALT_COR_IDF IS
      BEGIN
        UPDATE  COR_IDF SET
         SUBCLASSE_IDF = mdof.SUBCLASSE_IDF
        ,AM_CODIGO = mdof.AM_CODIGO
        ,OM_CODIGO = mdof.OM_CODIGO
        ,MERC_CODIGO = mdof.MERC_CODIGO
        ,PRES_CODIGO = mdof.PRES_CODIGO
        ,SISS_CODIGO = mdof.SISS_CODIGO
        ,QTD = mdof.QTD
        ,ENTSAI_UNI_CODIGO  = mdof.ENTSAI_UNI_CODIGO
        ,ESTOQUE_UNI_CODIGO = mdof.ESTOQUE_UNI_CODIGO
        ,PRECO_UNITARIO     = mdof.PRECO_UNITARIO
        ,VL_AJUSTE_PRECO_TOTAL = mdof.VL_AJUSTE_PRECO_TOTAL
        ,PRECO_TOTAL = mdof.PRECO_TOTAL_ITEM
        ,VL_RATEIO_FRETE  = mdof.VL_RATEIO_FRETE
        ,VL_RATEIO_SEGURO = mdof.VL_RATEIO_SEGURO
        ,VL_RATEIO_ODA    = mdof.VL_RATEIO_ODA
        ,CFOP_CODIGO = mdof.CFOP_CODIGO
        ,STP_CODIGO = mdof.STP_CODIGO
        ,STC_CODIGO = mdof.STC_CODIGO
        ,NBM_CODIGO = mdof.NBM_CODIGO
        ,IDF_TEXTO_COMPLEMENTAR = mdof.IDF_TEXTO_COMPLEMENTAR
        ,ALIQ_DIFA      = mdof.ALIQ_DIFA
        ,ALIQ_ICMS      = mdof.ALIQ_ICMS
        ,ALIQ_IPI       = mdof.ALIQ_IPI
        ,ALIQ_ISS       = mdof.ALIQ_ISS
        ,ALIQ_STT       = mdof.ALIQ_STT
        ,ALIQ_STF       = mdof.ALIQ_STF
        ,PERC_IRRF      = mdof.PERC_IRRF
        ,VL_BASE_ICMS   = mdof.VL_BASE_ICMS
        ,VL_BASE_IPI    = mdof.VL_BASE_IPI
        ,VL_BASE_IRRF   = mdof.VL_BASE_IRRF
        ,VL_BASE_ISS    = mdof.VL_BASE_ISS
        ,VL_BASE_STT    = mdof.VL_BASE_STT
        ,VL_BASE_STF    = mdof.VL_BASE_STF
        ,VL_TRIBUTAVEL_ICMS = mdof.VL_TRIBUTAVEL_ICMS
        ,VL_TRIBUTAVEL_IPI  = mdof.VL_TRIBUTAVEL_IPI
        ,VL_TRIBUTAVEL_DIFA = mdof.VL_TRIBUTAVEL_DIFA
        ,VL_TRIBUTAVEL_STT  = mdof.VL_TRIBUTAVEL_STT
        ,VL_TRIBUTAVEL_STF  = mdof.VL_TRIBUTAVEL_STF
        ,VL_DIFA        = mdof.VL_DIFA
        ,VL_ICMS        = mdof.VL_ICMS
        ,VL_IPI         = mdof.VL_IPI
        ,VL_IRRF        = mdof.VL_IRRF
        ,VL_ISS         = mdof.VL_ISS
        ,VL_STT         = mdof.VL_STT
        ,VL_STF         = mdof.VL_STF
        ,ORD_IMPRESSAO  = mdof.IDF_NUM
        ,NOP_CODIGO     = mdof.NOP_CODIGO_IDF
        ,VL_CONTABIL    = mdof.VL_CONTABIL
        ,VL_FATURADO    = mdof.VL_FATURADO
        ,VL_FISCAL      = mdof.VL_FISCAL
        ,VL_RATEIO_AJUSTE_PRECO = 0
        ,TIPO_COMPLEMENTO = mtipo_complemento
        ,VL_ISENTO_ICMS = mdof.VL_ISENTO_ICMS
        ,VL_ISENTO_IPI  = mdof.VL_ISENTO_IPI
        ,VL_OUTROS_ICMS = mdof.VL_OUTROS_ICMS
        ,VL_OUTROS_IPI  = mdof.VL_OUTROS_IPI
        ,VL_ALIQ_PIS      =    mdof.VL_ALIQ_PIS
        ,VL_ALIQ_COFINS      =    mdof.VL_ALIQ_COFINS
        ,ALIQ_PIS_RET      =    mdof.ALIQ_PIS_RET
        ,ALIQ_COFINS_RET    =    mdof.ALIQ_COFINS_RET
        ,ALIQ_CSLL_RET      =    mdof.ALIQ_CSLL_RET
        ,VL_BASE_PIS      =    mdof.VL_BASE_PIS
        ,VL_BASE_COFINS      =    mdof.VL_BASE_COFINS
        ,VL_BASE_PIS_RET    =    mdof.VL_BASE_PIS_RET
        ,VL_BASE_COFINS_RET  =    mdof.VL_BASE_COFINS_RET
        ,VL_BASE_CSLL_RET  =    mdof.VL_BASE_CSLL_RET
        ,VL_IMPOSTO_PIS      =    mdof.VL_IMPOSTO_PIS
        ,VL_IMPOSTO_COFINS  =    mdof.VL_IMPOSTO_COFINS
        ,VL_PIS_RET          =    mdof.VL_PIS_RET
        ,VL_COFINS_RET      =    mdof.VL_COFINS_RET
        ,VL_CSLL_RET        =    mdof.VL_CSLL_RET
        ,PESO_BRUTO_KG      =    mdof.PESO_BRUTO_ITEM -- CBT 22/09/04 Ocor738800
        ,PESO_LIQUIDO_KG    =    mdof.PESO_LIQUIDO_ITEM
        ,dt_fab_med         =    mdof.dt_fab_med
        ,dt_di              =    mdof.dt_di
        ,num_di             =    mdof.num_di
        ,chassi_veic        =    mdof.chassi_veic
        ,codigo_retencao    =    mdof.codigo_retencao
        ,im_subcontratacao  =    mdof.im_subcontratacao
        ,lote_med           =    mdof.charg
        ,aliq_ii            =    mdof.aliq_ii
        ,vl_ii              =    mdof.vl_ii
        ,vl_base_ii         =    mdof.vl_base_ii
        , conta_contabil    =    mdof.conta_contabil
        ,QTD_BASE_PIS = mdof.QTD_BASE_PIS
        ,QTD_BASE_COFINS = mdof.QTD_BASE_COFINS
        ,IND_MOVIMENTA_ESTOQUE = mdof.IND_MOVIMENTA_ESTOQUE
        ,STA_CODIGO = mdof.STA_CODIGO
        ,STN_CODIGO = mdof.STN_CODIGO
        , VL_BASE_SEST = mdof.VL_BASE_SEST
        , VL_BASE_SENAT = mdof.VL_BASE_SENAT
        , ALIQ_SEST = mdof.ALIQ_SEST
        , ALIQ_SENAT = mdof.ALIQ_SENAT
        , VL_SEST = mdof.VL_SEST
        , VL_SENAT = mdof.VL_SENAT
        , VL_INSS = mdof.VL_INSS
        , VL_BASE_INSS = mdof.VL_BASE_INSS
        , ALIQ_INSS = mdof.ALIQ_INSS
        , VL_INSS_RET = mdof.VL_INSS_RET
        , VL_BASE_INSS_RET = mdof.VL_BASE_INSS_RET
        , ALIQ_INSS_RET = mdof.ALIQ_INSS_RET
        , IND_ISS_RETIDO_FONTE = mdof.IND_ISS_RETIDO_FONTE
        , IND_INCIDENCIA_PIS_ST = mdof.IND_INCIDENCIA_PIS_ST
        , VL_BASE_PIS_ST = mdof.VL_BASE_PIS_ST
        , ALIQ_PIS_ST = mdof.ALIQ_PIS_ST
        , VL_PIS_ST = mdof.VL_PIS_ST
        , IND_INCIDENCIA_COFINS_ST = mdof.IND_INCIDENCIA_COFINS_ST
        , VL_BASE_COFINS_ST = mdof.VL_BASE_COFINS_ST
        , ALIQ_COFINS_ST = mdof.ALIQ_COFINS_ST
        , VL_COFINS_ST = mdof.VL_COFINS_ST
        , COD_CCUS = mdof.COD_CCUS
        , NAT_REC_PISCOFINS = mdof.NAT_REC_PISCOFINS
        , NAT_REC_PISCOFINS_DESCR = mdof.NAT_REC_PISCOFINS_DESCR
        , VL_CRED_PIS_REC_TRIB = mdof.VL_CRED_PIS_REC_TRIB
        , VL_CRED_PIS_REC_NAO_TRIB = mdof.VL_CRED_PIS_REC_NAO_TRIB
        , VL_CRED_PIS_REC_EXPO = mdof.VL_CRED_PIS_REC_EXPO
        , VL_CRED_COFINS_REC_TRIB = mdof.VL_CRED_COFINS_REC_TRIB
        , VL_CRED_COFINS_REC_NAO_TRIB = mdof.VL_CRED_COFINS_REC_NAO_TRIB
        , VL_CRED_COFINS_REC_EXPO =  mdof.VL_CRED_COFINS_REC_EXPO
        , VL_BASE_ICMS_DESC_L = mdof.VL_BASE_ICMS_DESC_L
        , VL_TRIBUTAVEL_ICMS_DESC_L = mdof.VL_TRIBUTAVEL_ICMS_DESC_L
        , ALIQ_ICMS_DESC_L = mdof.ALIQ_ICMS_DESC_L
        , VL_ICMS_DESC_L = mdof.VL_ICMS_DESC_L
        , IND_NAT_FRT_PISCOFINS = mdof.IND_NAT_FRT_PISCOFINS
        , VL_DEDUCAO_DEPENDENTE_PRG = mdof.VL_DEDUCAO_DEPENDENTE_PRG
        , VL_DEDUCAO_PENSAO_PRG = mdof.VL_DEDUCAO_PENSAO_PRG
        , PERC_PART_CI = mdof.PERC_PART_CI
        , FCI_NUMERO = mdof.NFCI
        ,VL_BASE_STF_FRONTEIRA = MDOF.VL_BASE_STF_FRONTEIRA
        ,VL_STF_FRONTEIRA = MDOF.VL_STF_FRONTEIRA
        ,VL_BASE_STF_IDO  = MDOF.VL_BASE_STF_IDO
        ,VL_STF_IDO = MDOF.VL_STF_IDO
        ,TIPO_STF = MDOF.TIPO_STF
        ,perc_icms_part_rem = mdof.perc_icms_part_rem
        ,vl_base_icms_part_rem = mdof.vl_base_icms_part_rem
        ,aliq_icms_part_rem = mdof.aliq_icms_part_rem
        ,vl_icms_part_rem = mdof.vl_icms_part_rem
        ,perc_icms_part_dest = mdof.perc_icms_part_dest
        ,vl_base_icms_part_dest = mdof.vl_base_icms_part_dest
        ,aliq_icms_part_dest = mdof.aliq_icms_part_dest
        ,vl_icms_part_dest = mdof.vl_icms_part_dest
        ,aliq_icms_fcp = mdof.aliq_icms_fcp
        ,vl_icms_fcp = mdof.vl_icms_fcp
        ,vl_base_icms_fcp = mdof.vl_base_icms_fcp
        ,cest_codigo = mdof.cest_codigo
        ,ENQ_IPI_CODIGO = mdof.ENQ_IPI_CODIGO
        ,aliq_difa_icms_part = mdof.aliq_icms_part_dest - mdof.aliq_icms
        ,Desoneracao_Codigo = mdof.Desoneracao_Codigo
        ,ALIQ_ICMS_FCPST             = mdof.ALIQ_ICMS_FCPST
        ,VL_BASE_ICMS_FCPST          = mdof.VL_BASE_ICMS_FCPST
        ,VL_ICMS_FCPST               = mdof.VL_ICMS_FCPST             
         WHERE
         ID = mdof.IDF_ID
         ;
    END;

    PROCEDURE EXC_COR_IDF IS
      BEGIN
        DELETE  FROM COR_IDF
        WHERE
        ID = mdof.IDF_ID
        ;
      END;

    PROCEDURE INC_COR_IDF_INUTILIZADA is
      mdata CORAPI_IDF.data;
    BEGIN
      mdata.v.CODIGO_DO_SITE := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_ID         := mdof.DOF_ID;
      mdata.v.DOF_SEQUENCE   := mdof.DOF_SEQUENCE;
      mdata.v.IDF_NUM        := mdof.IDF_NUM;
      mdata.v.NOP_CODIGO     := mdof.NOP_CODIGO_INUTIL;
      CORAPI_IDF.ins(mdata);
    END;
  
    PROCEDURE ALT_COR_IDF_INUTILIZADA is
      mdata CORAPI_IDF.data;
    BEGIN
      mdata.v.CODIGO_DO_SITE := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_ID         := mdof.DOF_ID;
      mdata.v.DOF_SEQUENCE   := mdof.DOF_SEQUENCE;
      mdata.v.IDF_NUM        := mdof.IDF_NUM;
      mdata.i.CODIGO_DO_SITE := TRUE;
      mdata.i.DOF_ID         := TRUE;
      mdata.i.DOF_SEQUENCE   := TRUE;
      mdata.i.IDF_NUM        := TRUE;
      CORAPI_IDF.upd(mdata);
    
    END;
  
    PROCEDURE EXC_COR_IDF_INUTILIZADA is
      mdata CORAPI_IDF.data;
    BEGIN
      mdata.v.CODIGO_DO_SITE := mdof.CODIGO_DO_SITE;
      mdata.v.DOF_SEQUENCE   := mdof.DOF_SEQUENCE;
      mdata.v.IDF_NUM        := mdof.IDF_NUM;
      CORAPI_IDF.del(mdata);
    END;
	  
    PROCEDURE inc_COR_LOTE_MED IS
      BEGIN
        INSERT INTO COR_IDF_LOTE_MED (
        CODIGO_DO_SITE
        ,DOF_SEQUENCE
        ,IDF_NUM
        ,LOTE_MED
        ,DT_FAB_MED
        ,DT_VAL
        ,IND_MED
        ,TIPO_PROD_MED
        ,VL_TAB_MAX
        ,QTD
        ) VALUES (
        mdof.CODIGO_DO_SITE
        ,mdof.DOF_SEQUENCE
        ,mdof.IDF_NUM
        ,mdof.LOTE_MED
        ,mdof.DT_FAB_LOTE_MED
        ,mdof.DT_VAL
        ,mdof.IND_MED
        ,mdof.TIPO_PROD_MED
        ,mdof.VL_TAB_MAX
        ,mdof.QTD_LOTE_MED
        );
      END;

    PROCEDURE alt_COR_LOTE_MED IS
      BEGIN
        UPDATE COR_IDF_LOTE_MED SET
         LOTE_MED = mdof.LOTE_MED
        ,DT_FAB_MED = mdof.DT_FAB_lote_MED
        ,DT_VAL = mdof.DT_VAL
        ,IND_MED = mdof.IND_MED
        ,TIPO_PROD_MED = mdof.TIPO_PROD_MED
        ,VL_TAB_MAX = mdof.VL_TAB_MAX
        ,QTD = mdof.QTD_lote_med
        WHERE
        CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
        AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
        AND IDF_NUM = mdof.IDF_NUM
        ;
      END;

    PROCEDURE exc_COR_LOTE_MED IS
      BEGIN
        DELETE COR_IDF_LOTE_MED
        WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
        AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
        AND IDF_NUM = mdof.IDF_NUM
        ;
      END; -- cdc 2547549 fim
	  
    PROCEDURE inc_COR_IDF_CONS_CIVIL IS
    BEGIN
      INSERT INTO COR_IDF_CONSTRUCAO_CIVIL
        (CODIGO_DO_SITE,
         DOF_SEQUENCE,
         IDF_NUM,         
         CNPJ_CPF_PROP_OBRA,
         IND_OBRA,
         CNO_CODIGO
         )
      VALUES
        (mdof.CODIGO_DO_SITE,
         mdof.DOF_SEQUENCE,
         mdof.IDF_NUM,
         mdof.CNPJ_CPF_PROP_OBRA,
         mdof.IND_OBRA,
         mdof.CNO_CODIGO
	       );	
    END;
  
    PROCEDURE alt_COR_IDF_CONS_CIVIL IS
    BEGIN
      UPDATE COR_IDF_CONSTRUCAO_CIVIL
         SET CNPJ_CPF_PROP_OBRA = mdof.CNPJ_CPF_PROP_OBRA,
             IND_OBRA           = mdof.IND_OBRA,
             CNO_CODIGO         = mdof.CNO_CODIGO
       WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
         AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
         AND IDF_NUM = mdof.IDF_NUM;
    END;
  
    PROCEDURE exc_COR_IDF_CONS_CIVIL IS
    BEGIN
      DELETE COR_IDF_CONSTRUCAO_CIVIL
       WHERE CODIGO_DO_SITE = mdof.CODIGO_DO_SITE
         AND DOF_SEQUENCE = mdof.DOF_SEQUENCE
         AND IDF_NUM = mdof.IDF_NUM;
    END;

    PROCEDURE grv_COR_DOF IS

      minstrucao varchar2(10);
      v_msg_id     cor_dof_msg.msg_id%type;

      Begin
        minstrucao := mdof.c_COR_DOF;
        mdof.DOF_SEQUENCE :=  COR_PK_SEQ_DOF ( mdof.INFORMANTE_EST_CODIGO, mdof.DOF_IMPORT_NUMERO );

        mDOF_SEQUENCE := mdof.DOF_SEQUENCE ;

        mId := mdof.ID;
      if mdof.ctrl_situacao_dof = 'I' Then
        mdof.EMITENTE_PFJ_CODIGO      := mdof.INFORMANTE_EST_CODIGO;
        mdof.REMETENTE_PFJ_CODIGO     := mdof.INFORMANTE_EST_CODIGO;
        mdof.DESTINATARIO_PFJ_CODIGO  := mdof.INFORMANTE_EST_CODIGO;
        mdof.RETIRADA_PFJ_CODIGO      := mdof.INFORMANTE_EST_CODIGO;
        mdof.ENTREGA_PFJ_CODIGO       := mdof.INFORMANTE_EST_CODIGO;
        mdof.TRANSPORTADOR_PFJ_CODIGO := mdof.INFORMANTE_EST_CODIGO;
        mdof.COBRANCA_PFJ_CODIGO      := mdof.INFORMANTE_EST_CODIGO;
      
        Begin
          mdof.EMITENTE_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.EMITENTE_PFJ_CODIGO,
                                                                 mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ EMITENTE  : ' || SQLERRM);
        end;
      
        Begin
          mdof.REMETENTE_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.REMETENTE_PFJ_CODIGO,
                                                                  mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ REMETENTE  : ' || SQLERRM);
        end;
      
        Begin
          mdof.DESTINATARIO_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.DESTINATARIO_PFJ_CODIGO,
                                                                     mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001,
                                    'PFJ DESTINATARIO: ' || SQLERRM);
        end;
      
        Begin
          mdof.RETIRADA_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.RETIRADA_PFJ_CODIGO,
                                                                 mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ RETIRADA: ' || SQLERRM);
        end;
      
        begin
          mdof.ENTREGA_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.ENTREGA_PFJ_CODIGO,
                                                                mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ ENTREGA: ' || SQLERRM);
        end;
      
        begin
          mdof.TRANSPORTADOR_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.TRANSPORTADOR_PFJ_CODIGO,
                                                                      mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001,
                                    'PFJ TRANSPORTADOR  : ' || SQLERRM);
        end;
      
        begin
          mdof.COBRANCA_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.COBRANCA_PFJ_CODIGO,
                                                                 mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ COBRANCA  : ' || SQLERRM);
        end;
      
        begin
          mdof.EMITENTE_LOC_CODIGO := sap_cor_pfj.GET_LOC_CODIGO(mdof.EMITENTE_PFJ_CODIGO,
                                                                 mdof.DH_EMISSAO);
        exception
          when others then
            RAISE_APPLICATION_ERROR(-20001, 'PFJ EMITENTE  : ' || SQLERRM);
        end;
      
        if (mdof.NOP_CODIGO_INUTIL is null) Then
          raise_application_error(-20001,
                                  'O preenchimento do par�metro SAP_NOP_CODIGO � obrigat�rio.');
        end if;
      
        mdof.modo_emissao      := 'E';
        mdof.ctrl_conteudo     := 'C';
        mdof.ind_entrada_saida := 'S';
      
        if minstrucao = 'I' then
          inc_COR_DOF_INUTILIZADA;
        elsif minstrucao = 'A' then
          alt_COR_DOF_INUTILIZADA;
          if SQL%NOTFOUND then
            raise_application_error(-20001,
                                    'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_DOF');
          end if;
        elsif minstrucao = 'E' then
          exc_COR_DOF_INUTILIZADA;
          if SQL%NOTFOUND then
            raise_application_error(-20001,
                                    'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_DOF');
          end if;
        elsif minstrucao = 'M' then
          begin
            inc_COR_DOF_INUTILIZADA;
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
              alt_COR_DOF_INUTILIZADA;
          end;
        else
          put_line('Instrucao Invalida ' || minstrucao || '.');
        end if;
      
      else

       if minstrucao = 'I' then
         inc_COR_DOF;
       elsif minstrucao = 'A' then
         alt_COR_DOF;
         if SQL%NOTFOUND then
           raise_application_error(-20001,'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_DOF');
         end if;
       elsif minstrucao = 'E' then
         exc_COR_DOF;
         if SQL%NOTFOUND then
           raise_application_error(-20001,'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_DOF');
         end if;
       elsif  minstrucao = 'M' then
         begin
           inc_COR_DOF;
         EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
           alt_COR_DOF;
         end;
       else
         put_line('Instrucao Invalida ' || minstrucao || '.');
       end if;

      end if;
	   
        mdof.DOF_ID := COR_DOF_IDF_ID ( mdof.CODIGO_DO_SITE, mdof.DOF_SEQUENCE );
        mDOF_ID := mdof.DOF_ID ;

        For msg_dof IN (Select msg_id into v_msg_id
                        from cor_dof_msg
                        where dof_sequence = mdof.dof_sequence
                        and codigo_do_site = mdof.codigo_do_site)
        Loop
          Begin
            delete cor_dof_msg
            where dof_sequence = mdof.dof_sequence
            and codigo_do_site = mdof.codigo_do_site;

            delete cor_mensagem
            where id = v_msg_id;
          End;
        End loop;
        IF mdof.SUBCLASSE_IDF = 'S' THEN
          pTipoMsg := 'ISS';
        ELSIF mdof.SUBCLASSE_IDF = 'M' OR mdof.SUBCLASSE_IDF = 'P' THEN
          pTipoMsg := 'ICMS';
        END IF;

        IF mdof.observat IS NOT NULL THEN
          cor_msg.del_msg_dof ( mdof.codigo_do_site, mdof.dof_sequence, pTipoMsg);
          begin
            cor_msg.ins_msg_dof ( mdof.codigo_do_site, mdof.dof_sequence,  mdof.observat, pTipoMsg);
          exception when others then
            raise_application_error ( -20510, '  Erro na gravacao da mensagem ' || pTipoMsg || ' ==> '||  mdof.observat );
          end;
        END IF;

        IF mdof.SUBCLASSE_IDF = 'S' THEN
          pTipoMsg := 'ISS';
        ELSE
          pTipoMsg := 'OPERACIONAL';
        END IF;

        DECLARE
        v_mens_operacional    varchar2( 1200 );

        CURSOR c_le_mensagens_dof ( p_id number )  IS
             select * from sap_itf_dof_msg_cor
             where yj1bnfdoc_id  = p_id
             order by id
             ;
        BEGIN
        FOR mensagem_dof_rec IN c_le_mensagens_dof ( mdof.id_dof )
        LOOP
          begin
              cor_msg.ins_msg_dof (  mdof.codigo_do_site, mdof.dof_sequence, mensagem_dof_rec.message, pTipoMsg   );
          exception
          when others then
              null;
          end;
        End loop;
        End;
       If mdof.PREFNO is not null and mdof.CHECOD is not null then
        mdof.PREFNO := replace(substr(lpad(mdof.PREFNO, 20, 'W'), 6, 15),
                               'W',
                               '');
        Begin
          Insert into COR_DOF_NFE (ID, NUMERO_NFE, CODIGO_VERIFICACAO_NFE, Dt_Emissao_Nfe,Situacao_Nfe,Serie_Nfe)
          values (mDOF_ID, mdof.PREFNO, mdof.CHECOD,mdof.dh_emissao,'T',mdof.serie_subserie);

        Exception when DUP_VAL_ON_INDEX then
          Begin
            Update cor_dof_nfe
             set CODIGO_VERIFICACAO_NFE = mdof.CHECOD
                 , Dt_emissao_Nfe = mdof.dh_emissao
                 , Situacao_Nfe = 'T'
                 , Serie_Nfe = mdof.serie_subserie
             where id = mDOF_ID and NUMERO_NFE = mdof.PREFNO;

          Exception when others then
            raise_application_error(-20001,'Erro ao incluir COR_DOF_NFE - ' || sqlerrm);
          End;
        End;
        End if;
    END;

    PROCEDURE grv_COR_DOF_PROCESSO is
      minstrucao varchar2(10);
    
    Begin
      minstrucao := mdof.c_COR_DOF_PROCESSO;
    
        if minstrucao = 'I' then
          inc_COR_DOF_PROCESSO;
        elsif minstrucao = 'A' then
          alt_COR_DOF_PROCESSO;
          if SQL%NOTFOUND then
            raise_application_error(-20001,
                                    'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_DOF_PROCESSO');
          end if;
        elsif minstrucao = 'E' then
          exc_COR_DOF_PROCESSO;
          if SQL%NOTFOUND then
             NULL;  
          end if;
        elsif minstrucao = 'M' then
          begin
            inc_COR_DOF_PROCESSO;
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
              alt_COR_DOF_PROCESSO;
          end;
        else
          put_line('Instrucao Invalida ' || minstrucao || '.');
        end if;
    END;
	
    PROCEDURE grv_COR_DOF_VOLUME_CARGA is
      minstrucao varchar2(10);

      Begin
        minstrucao := mdof.c_COR_DOF_VOLUME_CARGA;

        If mdof.ESPECIE_VOLUMES is not null and mdof.PESO_REEMBALAGEM_KG is not null Then
          if minstrucao = 'I' then
            inc_COR_DOF_VOLUME_CARGA;
          elsif  minstrucao = 'A' then
            alt_COR_DOF_VOLUME_CARGA;
            if SQL%NOTFOUND then
              raise_application_error(-20001,'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_DOF_VOLUME_CARGA');
            end if;
          elsif  minstrucao = 'E' then
            exc_COR_DOF_VOLUME_CARGA;
            if SQL%NOTFOUND then
              raise_application_error(-20001,'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_DOF_VOLUME_CARGA');
            end if;
          elsif  minstrucao = 'M' then
            begin
              inc_COR_DOF_VOLUME_CARGA;
            EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
              alt_COR_DOF_VOLUME_CARGA;
            end;
          else
            put_line('Instrucao Invalida ' || minstrucao || '.');
          end if;
        End if;
      END;

    PROCEDURE grv_COR_DOF_ASSOCIADO is
    minstrucao varchar2(10);
    begin
      minstrucao := mdof.c_COR_DOF_ASSOCIADO;
      If ltrim(mdof.DOCREF,0) is null Then
        mdof.DOCREF := sap_dof_util.get_docref (mdof.id_idf, 'COR');
      End if;

      If ltrim(mdof.DOCREF,0) is not null Then
        Declare
        mDOFASS   SAP_DOF_UTIL.dof_values;

        Begin
          mDOFASS := sap_dof_util.get_dof_info ( mdof.DOCREF, mdof.INFORMANTE_EST_CODIGO, 'COR' );
          mdof.DH_EMISSAO_ASSOC          := mDOFASS.DH_EMISSAO ;
          mdof.EDOF_CODIGO_ASSOC         := mDOFASS.EDOF_CODIGO ;
          mdof.EMITENTE_PFJ_CODIGO_ASSOC := mDOFASS.EMITENTE_PFJ_CODIGO ;
          mdof.NUMERO_ASSOC              := mDOFASS.NUMERO ;
          mdof.SERIE_SUBSERIE_ASSOC      := mDOFASS.SERIE_SUBSERIE ;
          mdof.ANO_MES_EMISSAO_ASSOC     := to_char ( mDOFASS.DH_EMISSAO, 'YYYYMM' ) ;
          mdof.VL_TOTAL_DOF_ASSOC        := mDOFASS.VL_TOTAL_CONTABIL ;
          mdof.DOF_ASSOC_SEQUENCE        := mDOFASS.DOF_SEQUENCE ;
          mdof.DESTINATARIO_PFJ_CODIGO_ASSOC := mDOFASS.DESTINATARIO_PFJ_CODIGO;
          mdof.IND_ENTRADA_SAIDA_ASSOC        := mDOFASS.IND_ENTRADA_SAIDA;
          mdof.id_dof                     := mDOFASS.DOF_ID;
        End;
        If mdof.DOF_ASSOC_SEQUENCE is null Then
          raise_application_error (-20001, 'O DOF associado (docnum) :'||mdof.DOCREF
		  ||' nao foi encontrado em COR_DOF. '||chr(13)|| 'Este DOF original deve ser importado antes do DOF que o referencia');
        End if;

        Begin
          Select 'OPC' into mdof.tadoc_codigo
          From cor_natureza_de_operacao NOP
          Where nop.nop_codigo = mdof.nop_codigo_dof
                and nop.ind_comprova_operacao = 'S';
          Exception when no_data_found Then
            mdof.tadoc_codigo := null;
        End;

        BEGIN
          SELECT NFE_LOCALIZADOR
            INTO mdof.NFE_LOCALIZADOR_assoc
            FROM COR_DOF
           WHERE DOF_IMPORT_NUMERO = mdof.DOCREF;
        EXCEPTION
          WHEN OTHERS THEN
            mdof.NFE_LOCALIZADOR_assoc := null;
        END;
		
        if  minstrucao = 'I' then
          inc_COR_DOF_ASSOCIADO;
        elsif  minstrucao = 'A' then
          alt_COR_DOF_ASSOCIADO;
          if SQL%NOTFOUND then
            raise_application_error(-20001,'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_DOF_ASSOCIADO');
          end if;
         elsif  minstrucao = 'E' then
          exc_COR_DOF_ASSOCIADO;
          if SQL%NOTFOUND then
            raise_application_error(-20001,'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_DOF_ASSOCIADO');
          end if;
         elsif  minstrucao = 'M' then
          begin
          inc_COR_DOF_ASSOCIADO;
          EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
            alt_COR_DOF_ASSOCIADO;
          end;
         else
          put_line('Instrucao Invalida ' || minstrucao || '.');
         end if;
      End if;
    END;

    PROCEDURE grv_COR_IDF is
      minstrucao varchar2(10);

      Begin
         minstrucao := mdof.c_COR_IDF;
         mdof.IDF_ID:= sap_dof_util.get_idf_id ( 'COR', mdof.CODIGO_DO_SITE, mdof.DOF_SEQUENCE, mdof.IDF_NUM );

         mdof.AM_CODIGO      := sap_itf_xml_util.sap_get_par_associacao ('SAP_APLIC_MERCADORIA_COR', mdof.MATUSE );

        mdof.aliq_icms_part_dest_tmp := mdof.aliq_icms_part_dest;

        if mdof.vl_base_icms_part_rem <> 0 and
           mdof.aliq_icms_part_rem <> 0 and
           mdof.vl_icms_part_rem <> 0 and
           mdof.vl_base_icms_part_dest <> 0 and
           mdof.aliq_icms_part_dest <> 0 and
           mdof.vl_icms_part_dest <> 0 Then

            CASE
               WHEN to_char(mdof.dh_emissao,'YYYY') = '2015' Then
                    mdof.PERC_ICMS_PART_DEST := 20;
                    mdof.PERC_ICMS_PART_REM :=  80;
               WHEN to_char(mdof.dh_emissao,'YYYY') = '2016' Then
                    mdof.PERC_ICMS_PART_DEST := 40;
                    mdof.PERC_ICMS_PART_REM :=  60;
               WHEN to_char(mdof.dh_emissao,'YYYY') = '2017' Then
                    mdof.PERC_ICMS_PART_DEST := 60;
                    mdof.PERC_ICMS_PART_REM :=  40;
               WHEN to_char(mdof.dh_emissao,'YYYY') = '2018' Then
                    mdof.PERC_ICMS_PART_DEST := 80;
                    mdof.PERC_ICMS_PART_REM :=  20;
               WHEN to_char(mdof.dh_emissao,'YYYY') = '2019' Then
                    mdof.PERC_ICMS_PART_DEST := 100;
                    mdof.PERC_ICMS_PART_REM :=  0;
               ELSE
                    mdof.PERC_ICMS_PART_DEST := 0;
                    mdof.PERC_ICMS_PART_REM :=  0;
            END CASE;
            else
              mdof.PERC_ICMS_PART_DEST := 0;
              mdof.PERC_ICMS_PART_REM :=  0;
              mdof.aliq_icms_part_dest_tmp := mdof.aliq_icms;
            end if;

      if mdof.ctrl_situacao_dof = 'I' Then
      
        if minstrucao = 'I' then
          inc_COR_IDF_INUTILIZADA;
        elsif minstrucao = 'A' then
          alt_COR_IDF_INUTILIZADA;
          if SQL%NOTFOUND then
            raise_application_error(-20001,
                                    'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_IDF');
          end if;
        elsif minstrucao = 'E' then
          exc_COR_IDF_INUTILIZADA;
          if SQL%NOTFOUND then
            raise_application_error(-20001,
                                    'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_IDF');
          end if;
        elsif minstrucao = 'M' then
          begin
            inc_COR_IDF_INUTILIZADA;
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
              alt_COR_IDF_INUTILIZADA;
          end;
        else
          put_line('Instrucao Invalida ' || minstrucao || '.');
        end if;
      
      else
         if minstrucao = 'I' then
           inc_COR_IDF;
         elsif  minstrucao = 'A' then
           alt_COR_IDF;
           if SQL%NOTFOUND then
             raise_application_error(-20001,'Nao e possivel alterar este registro,pois o mesmo nao existente na tabela COR_IDF');
           end if;
         elsif  minstrucao = 'E' then
           exc_COR_IDF;
           if SQL%NOTFOUND then
             raise_application_error(-20001,'Nao e possivel deletar este registro,pois o mesmo nao existente na tabela COR_IDF');
           end if;
         elsif  minstrucao = 'M' then
           begin
             inc_COR_IDF;
             EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
               alt_COR_IDF;
           end;
         else
           put_line('Instrucao Invalida ' || minstrucao || '.');
         end if;
      end if;

         IF mdof.SUBCLASSE_IDF = 'S' THEN
           pTipoMsg := 'ISS';
         ELSIF mdof.SUBCLASSE_IDF = 'M' OR mdof.SUBCLASSE_IDF = 'P' THEN
           pTipoMsg := 'ICMS';
         END IF;

         DECLARE
          v_mens_operacional    varchar2( 1200 );

          CURSOR c_le_mensagens_dof ( p_id number )  IS
               select * from sap_itf_dof_msg_cor
               where yj1bnfdoc_id  = p_id
               order by id
               ;
          BEGIN
          FOR mensagem_dof_rec IN c_le_mensagens_dof ( mdof.id_dof )
            LOOP
              begin
                  cor_msg.ins_msg_IDF (  mdof.codigo_do_site, mdof.dof_sequence, mdof.idf_num ,mensagem_dof_rec.message, pTipoMsg  );
              exception
              when others then
                  raise_application_error ( -20520, '  Erro na gravacao da v_mens_operacional ==> '||  v_mens_operacional || '  ' || sqlcode || '  ' || sqlerrm );
              end;
            End loop;
          End;
    END;

    PROCEDURE grv_COR_LOTE_MED IS
      minstrucao varchar2(10);

      Begin
        minstrucao := mdof.C_COR_LOTE_MED;
        If minstrucao = 'I' then
          inc_COR_LOTE_MED;
        Elsif  minstrucao = 'A' then
          alt_COR_LOTE_MED;
          if SQL%NOTFOUND then
            raise_application_error(-20001,'Nao e possivel alterar este registro, pois o mesmo nao existente na tabela COR_IDF_LOTE_MED');
          end if;
        Elsif  minstrucao = 'E' then
          exc_COR_LOTE_MED;
          if SQL%NOTFOUND then
            raise_application_error(-20001,'Nao e possivel deletar este registro, pois o mesmo nao existente na tabela COR_IDF_LOTE_MED');
          end if;
        Elsif  minstrucao = 'M' then
          Begin
            inc_COR_LOTE_MED;
          EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
            alt_COR_LOTE_MED;
          end;
        Else
          put_line('Instrucao Invalida ' || minstrucao || '.');
        End if;
      End;
	  
   PROCEDURE grv_COR_IDF_CONS_CIVIL IS
      minstrucao varchar2(10);
    
    Begin
      minstrucao := mdof.C_COR_IDF_CONS_CIVIL;
      If minstrucao = 'I' then
        inc_COR_IDF_CONS_CIVIL;
      Elsif minstrucao = 'A' then
        alt_COR_LOTE_MED;
        if SQL%NOTFOUND then
          raise_application_error(-20001,
                                  'Nao e possivel alterar este registro, pois o mesmo nao existente na tabela COR_IDF_CONSTRUCAO_CIVIL');
        end if;
      Elsif minstrucao = 'E' then
        exc_COR_IDF_CONS_CIVIL;
        if SQL%NOTFOUND then          
          NULL;
        end if;
      Elsif minstrucao = 'M' then
        Begin
          inc_COR_IDF_CONS_CIVIL;
        EXCEPTION
          WHEN DUP_VAL_ON_INDEX THEN
            alt_COR_IDF_CONS_CIVIL;
        end;
      Else
        put_line('Instrucao Invalida ' || minstrucao || '.');
      End if;
    End;
	  
    procedure Calcula_Totais(pDofImportNumero cor_dof.dof_import_numero%type) is
      begin
        cor_dof_cons_pkg.disable;
        for mIdfsOldDof in (select dof.id dof_id, idf.id idf_id
                            from cor_dof dof, cor_idf idf
                            where dof.id = idf.dof_id
                            and dof.dof_import_numero = pDofImportNumero
                            order by idf.idf_num
                            )
        loop
          begin
            sap_dof_util.set_dof_values ( mIdfsOldDof.DOF_ID, mIdfsOldDof.IDF_ID, 'COR' );
          exception when others then
            cor_dof_cons_pkg.enable;
            raise;
          end;
        end loop;
        cor_dof_cons_pkg.enable;
      end Calcula_Totais;

    procedure loadParameters is
      begin
        v_deduz_mun_codigo_iss          := upper ( COR_LE_INICIALIZACAO  ( null,   'DEDUZ_MUNICIPIO_ISS',  null ) );
        v_suprime_zeros                 := sap_itf_xml_util.get_par_val ( 'SAP_ITF_SUPRIME_ZEROS_PFJ' );
        v_carimbador_dirf               := upper ( COR_LE_INICIALIZACAO  ( null,   'CARIMBADOR_DIRF',  null ) );
        v_sap_utiliza_lote_med          := sap_itf_xml_util.get_par_val ( 'SAP_UTILIZA_LOTE_MED');
        sap_fis_util.v_split_valuation  := sap_itf_xml_util.get_par_val ('SAP_SPLIT_VALUATION');
        v_deduz_idf_num                := sap_itf_xml_util.get_par_val('SAP_DEDUZ_DOF_IDF_NUM');
	   end;

    procedure limpa_duplicado_itf is
      cursor creg is
      select a.docnum docnum, MAX(A.NUM_IDOC) num_idoc, count(*)
      from   sap_itf_dof_cor a
      group  by a.docnum
      having count(*) > 1
      order  by count(*) desc
      ;
      mreg creg%rowtype;

      Begin
        open creg;
        loop
          fetch creg
          into mreg;
          exit when creg%notfound;

          delete from sap_itf_dof_cor
          where  docnum = mreg.docnum
          and    NUM_IDOC <> mreg.NUM_IDOC;
          commit;

        end loop;
        close creg;

        delete sap_itf_impostos_idf_cor where taxtyp is null;
        commit;

        if sap_itf_xml_util.get_par_val_integer ('SAP_PARALELISMO_ITF_DOF')  <= 1 Then
          update sap_itf_dof_cor set job = 0;
          commit;
        end if;
      End;

    FUNCTION grava_registro RETURN BOOLEAN IS
      merror varchar2(2000);
      merror_code integer;
      mUF    varchar2(2);
      mIBGE  varchar2(10);
      v_mun_codigo_iss   cor_municipio.mun_codigo%type;
      mCODIGO_RETENCAO   varchar2(6);
      mTAXTYP     SAP_ITF_IMPOSTOS_IDF_COR.TAXTYP%type;
      v_ind_eix   cor_dof.ind_eix%type;

      Begin
        mId := mdof.ID;

        If v_ult_dof_gravado is not NULL  and v_ult_dof_gravado  <> mdof.DOF_IMPORT_NUMERO  Then
          Begin
            Calcula_Totais( v_ult_dof_gravado);
          Exception When Others Then
            mcritica := null;
          End;
        End If;
        If nvl(mold_docnum, '*') <> mdof.DOF_IMPORT_NUMERO Then
          mcritica := null;
        End if;

        If NVL(mcritica,'*') <> 'ERROR' Then
          if v_SUPRIME_ZEROS = 'S' then
            mdof.INFORMANTE_EST_CODIGO := ltrim(mdof.INFORMANTE_EST_CODIGO,0);
          end if;

          if nvl(v_old_INFORMANTE,'*') <> mdof.INFORMANTE_EST_CODIGO Then
            v_old_INFORMANTE := mdof.INFORMANTE_EST_CODIGO;
            mdof.INFORMANTE_EST_CODIGO  := nvl  ( sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_COR', mdof.INFORMANTE_EST_CODIGO ),
			sap_itf_xml_util.sap_get_par_associacao('SAP_ESTAB_COR', mdof.INFORMANTE_EST_CODIGO || 'E' ));
            v_INFORMANTE_EST_CODIGO := mdof.INFORMANTE_EST_CODIGO;
          else
            mdof.INFORMANTE_EST_CODIGO := v_INFORMANTE_EST_CODIGO;
          end if;

          If sap_itf_xml_util.sap_get_par_associacao ( 'SAP_ESPECIE_NF_CLASSIFICACAO_COR', mdof.CODIGO_ESPECIE) = 'C' Then
            If mdof.SITUACAO = 'I' or mdof.SITUACAO = 'D' then
              mdof.CTRL_SITUACAO_DOF := mdof.SITUACAO;
            Else
              mdof.CTRL_SITUACAO_DOF := 'S';
            End if;
            sap_dof_util.set_dof_cancel ( mdof.DOF_IMPORT_NUMERO, mdof.INFORMANTE_EST_CODIGO, 'COR', mdof.DOCREF,mdof.CTRL_SITUACAO_DOF );
          Else
            Begin -- ATL - 15/01/2004 - nao processa os itens restantes de DOF's quando houver critica na gravacao do DOF
              IF nvl(v_ult_dof_gravado,'*') <> mdof.DOF_IMPORT_NUMERO Then
                mdof.serie_subserie :=  nvl ( mdof.serie_subserie, sap_itf_xml_util.sap_get_par_associacao ('SAP_SERIES_DEFAULT_CATEGORIA_COR' , 
				mdof.codigo_especie  ) );  --serie_subserie
                v_codigo_especie := mdof.serie_subserie;
              else
                mdof.serie_subserie := nvl ( mdof.serie_subserie, v_codigo_especie);
              end if;

            IF nvl(v_ult_dof_gravado, '*') <> mdof.DOF_IMPORT_NUMERO Then
              mdof.edof_codigo := sap_itf_xml_util.sap_get_par_associacao('SAP_ESPECIE_NF',
                                                                          mdof.codigo_especie); --edof_codigo
              v_edof_codigo    := mdof.edof_codigo;
            
              mdof.ctrl_conteudo := nvl(sap_itf_xml_util.sap_get_par_associacao('SAP_NFSE_EMISSAO_COR',
                                                                                mdof.codigo_especie),
                                        'C'); --ctrl_conteudo
              v_ctrl_conteudo    := mdof.ctrl_conteudo;
            else
              mdof.edof_codigo   := v_edof_codigo;
              mdof.ctrl_conteudo := v_ctrl_conteudo;
            end if;

              IF nvl(v_ult_dof_gravado,'*') <> mdof.DOF_IMPORT_NUMERO Then
                mdof.ind_resp_frete :=  sap_itf_xml_util.sap_get_par_associacao ( 'SAP_RESP_FRETE_COR', mdof.inco1 );  -- ind_resp_frete
                v_ind_resp_frete := mdof.ind_resp_frete;
              else
                mdof.ind_resp_frete := v_ind_resp_frete;
              end if;
                IF v_ind_resp_frete = 9 then
                  mdof.ind_resp_frete := null;
                end if;
              If mdof.SERIE_SUBSERIE is null Then
                raise_application_error (-20001, 'Serie Subserie da NF em branco. Revisar Parametro da Integracao SAP' );
              Elsif mdof.EDOF_CODIGO is null Then
                raise_application_error (-20001, 'Verificar Equivalencia de Especie de NF :'|| mdof.CODIGO_ESPECIE);
              Elsif mdof.INFORMANTE_EST_CODIGO is null Then
                raise_application_error (-20001, 'Estabelecimento esta nulo ou n�o esta parametrizado em SAP_ESTAB_COR');
              Elsif ltrim ( mdof.NUMERO, 0 ) is null Then
                raise_application_error (-20001, 'Numero da NF esta nulo');
              End if;

              mdof.substituido_pfj_codigo := null;

              IF nvl(v_ult_dof_gravado,'*') <> mdof.DOF_IMPORT_NUMERO Then
                Declare
                mreturn0 SAP_DOF_UTIL.dof_pfj;
                mloc     COR_PESSOA_VIGENTE_EM.data;
                mloc_empty COR_PESSOA_VIGENTE_EM.data;
                Begin
                   mreturn0 := sap_dof_util.get_dof_pfj ( mdof.ID_DOF, 'COR' );
                   mdof.DESTINATARIO_PFJ_CODIGO   := mreturn0.DESTINATARIO_PFJ_CODIGO;
                   mdof.EMITENTE_PFJ_CODIGO       := mreturn0.EMITENTE_PFJ_CODIGO;
                   mdof.REMETENTE_PFJ_CODIGO      := mreturn0.REMETENTE_PFJ_CODIGO;
                   mdof.TRANSPORTADOR_PFJ_CODIGO  := mreturn0.TRANSPORTADOR_PFJ_CODIGO;
                   mdof.ENTREGA_PFJ_CODIGO        := mreturn0.ENTREGA_PFJ_CODIGO;
                   mdof.RETIRADA_PFJ_CODIGO       := mreturn0.RETIRADA_PFJ_CODIGO;
                   mdof.cobranca_pfj_codigo       := mreturn0.cobranca_pfj_codigo;
                   mdof.substituido_pfj_codigo    := mreturn0.substituido_pfj_codigo;
                   If mdof.DESTINATARIO_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.DESTINATARIO_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.DESTINATARIO_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ DESTINATARIO :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.DESTINATARIO_LOC_CODIGO := null;
                   End if;
                   If mdof.EMITENTE_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.EMITENTE_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.EMITENTE_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ EMITENTE  :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.EMITENTE_LOC_CODIGO := null;
                   End if;
                   If mdof.REMETENTE_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.REMETENTE_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.REMETENTE_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                        mdof.MUN_CODIGO_ISS         := mloc.locvig.MUN_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ REMETENTE :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.REMETENTE_LOC_CODIGO := null;
                   End if;
                   If mdof.TRANSPORTADOR_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.TRANSPORTADOR_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.TRANSPORTADOR_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ TRANSPORTADOR  :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.TRANSPORTADOR_LOC_CODIGO := null;
                   End if;
                   If mdof.ENTREGA_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.ENTREGA_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.ENTREGA_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ ENTREGA  :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.ENTREGA_LOC_CODIGO := null;
                   End if;
                   If mdof.RETIRADA_PFJ_CODIGO is not null Then
                     mloc.PFJ_CODIGO    := mdof.RETIRADA_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.RETIRADA_LOC_CODIGO   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ RETIRADA :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.RETIRADA_LOC_CODIGO := null;
                   End if;

                   If mdof.cobranca_pfj_codigo is not null Then
                     mloc.PFJ_CODIGO    := mdof.cobranca_pfj_codigo;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );
                     mdof.cobranca_loc_codigo   := mloc.locvig.LOC_CODIGO;
                     If mloc.locvig.invalid_ref = TRUE Then
                         RAISE_APPLICATION_ERROR(-20001, 'PFJ Cobranca  :'||mloc.locvig.error);
                     End if;
                     mloc := mloc_empty;
                   Else
                     mdof.cobranca_loc_codigo := null;
                   End if;
                 if mdof.destinatario_pfj_codigo_assoc is not null then
                    if mdof.ind_entrada_saida = 'E' then
                       mdof.destinatario_pfj_codigo_assoc := mdof.destinatario_pfj_codigo_assoc || 'F';
                    else
                       mdof.destinatario_pfj_codigo_assoc := mdof.destinatario_pfj_codigo_assoc || 'C';
                    end if;
                 end if;

                 IF mdof.subclasse_idf = 'S' THEN   -- Somente notas de Servicos
                   IF v_deduz_mun_codigo_iss = 'N' THEN -- Remetente da NF
                     mloc.PFJ_CODIGO    := mdof.REMETENTE_PFJ_CODIGO;
                     mloc.DT_REFERENCIA := mdof.DT_REFERENCIA;
                     COR_PESSOA_VIGENTE_EM.sel ( mloc );

                     If mloc.locvig.invalid_ref = TRUE Then
                       RAISE_APPLICATION_ERROR(-20112, 'Localidade da pessoa REMETENTE ' || mdof.REMETENTE_PFJ_CODIGO ||
					   ' nao localizada :'||mloc.locvig.error);
                     End if;

                     mdof.MUN_CODIGO_ISS         := mloc.locvig.MUN_CODIGO;
                     mloc := mloc_empty;
                   ELSIF v_deduz_mun_codigo_iss = 'S' THEN -- Regras do Expert
                     mdof.MUN_CODIGO_ISS := eTax_deduz_municipio_iss( null                       -- pmun_pres_servico
                                                                   ,mdof.siss_codigo             -- psiss_codigo
                                                                   ,mdof.remetente_pfj_codigo    -- premetente_pfj_codigo
                                                                   ,mdof.destinatario_pfj_codigo -- pdestinatario_pfj_codigo
                                                                   ,mdof.dt_referencia           -- pdt_base
                                                                   ,null                         -- ptipo_recolhimento
                                                                   ,null                         -- pind_nacional_estrangeira
                                                                   ,mdof.ind_entrada_saida       -- pind_entrada_saida
                                                                   ,mdof.remetente_loc_codigo    -- premetente_loc_codigo
                                                                   ,mdof.destinatario_loc_codigo -- pdestinatario_loc_codigo
                                                                   );
                     if mdof.MUN_CODIGO_ISS is null then
                       RAISE_APPLICATION_ERROR(-20115, 'Codigo do Municipio de ISS do Expert nao deduzido');
                     end if;
                   Else
                     RAISE_APPLICATION_ERROR(-20114, 'MUN_CODIGO_ISS nao deduzido ' );
                   End If; -- Somente para notas de Servicoes
                 END IF; -- END CBT 05/11/2007 Ocor 1220724 -- Deduz municipio para apuracao do ISS,conforme parametro 'DEDUZ_MUNICIPIO_ISS'

                 v_DESTINATARIO_PFJ_CODIGO         := mdof.DESTINATARIO_PFJ_CODIGO;
                 v_EMITENTE_PFJ_CODIGO             := mdof.EMITENTE_PFJ_CODIGO;
                 v_REMETENTE_PFJ_CODIGO            := mdof.REMETENTE_PFJ_CODIGO;
                 v_TRANSPORTADOR_PFJ_CODIGO        := mdof.TRANSPORTADOR_PFJ_CODIGO;
                 v_ENTREGA_PFJ_CODIGO              := mdof.ENTREGA_PFJ_CODIGO;
                 v_RETIRADA_PFJ_CODIGO             := mdof.RETIRADA_PFJ_CODIGO;
                 v_cobranca_pfj_codigo             := mdof.cobranca_pfj_codigo;
                 v_substituido_pfj_codigo          := mdof.substituido_pfj_codigo;

                 v_DESTINATARIO_LOC_CODIGO         := mdof.DESTINATARIO_LOC_CODIGO;
                 v_EMITENTE_loc_CODIGO             := mdof.EMITENTE_loc_CODIGO;
                 v_REMETENTE_loc_CODIGO            := mdof.REMETENTE_loc_CODIGO;
                 v_TRANSPORTADOR_loc_CODIGO        := mdof.TRANSPORTADOR_loc_CODIGO;
                 v_ENTREGA_loc_CODIGO              := mdof.ENTREGA_loc_CODIGO;
                 v_RETIRADA_loc_CODIGO             := mdof.RETIRADA_loc_CODIGO;
                 v_cobranca_loc_codigo             := mdof.cobranca_loc_codigo;
                End;
              else
                mdof.DESTINATARIO_PFJ_CODIGO   := v_DESTINATARIO_PFJ_CODIGO;
                mdof.EMITENTE_PFJ_CODIGO       := v_EMITENTE_PFJ_CODIGO;
                mdof.REMETENTE_PFJ_CODIGO      := v_REMETENTE_PFJ_CODIGO;
                mdof.TRANSPORTADOR_PFJ_CODIGO  := v_TRANSPORTADOR_PFJ_CODIGO;
                mdof.ENTREGA_PFJ_CODIGO        := v_ENTREGA_PFJ_CODIGO;
                mdof.RETIRADA_PFJ_CODIGO       := v_RETIRADA_PFJ_CODIGO;
                mdof.cobranca_pfj_codigo       := v_cobranca_pfj_codigo;
                mdof.substituido_pfj_codigo    := v_substituido_pfj_codigo;

                mdof.DESTINATARIO_LOC_CODIGO   := v_DESTINATARIO_LOC_CODIGO;
                mdof.EMITENTE_loc_CODIGO       := v_EMITENTE_loc_CODIGO;
                mdof.REMETENTE_loc_CODIGO      := v_REMETENTE_loc_CODIGO;
                mdof.TRANSPORTADOR_loc_CODIGO  := v_TRANSPORTADOR_loc_CODIGO;
                mdof.ENTREGA_loc_CODIGO        := v_ENTREGA_loc_CODIGO;
                mdof.RETIRADA_loc_CODIGO       := v_RETIRADA_loc_CODIGO;
                mdof.cobranca_loc_codigo       := v_cobranca_loc_codigo;
              end if;

              mtipo_complemento_aux := NULL ;

              v_sap_especie_nf_class := sap_itf_xml_util.sap_get_par_associacao ( 'SAP_ESPECIE_NF_CLASSIFICACAO_COR', mdof.CODIGO_ESPECIE);

              if v_sap_especie_nf_class = 'PIE' Then
                mdof.Tipo:= 'C';
                mtipo_complemento_aux := 'PI';

                if mdof.ind_entrada_saida = 'E' Then
                   mdof.remetente_pfj_codigo := mdof.remetente_pfj_codigo;
                   mdof.remetente_loc_codigo := mdof.remetente_loc_codigo;

                   mdof.emitente_pfj_codigo := mdof.destinatario_pfj_codigo;
                   mdof.emitente_loc_codigo := mdof.destinatario_loc_codigo;
                end if;

              end if;
              mdof.IND_CANCEL := '*';

              If mdof.CTRL_SITUACAO_DOF = 'S' and mdof.IND_ENTRADA_SAIDA = 'E' Then
                If mdof.INFORMANTE_EST_CODIGO <> mdof.EMITENTE_PFJ_CODIGO Then
                  mdof.IND_CANCEL := 'E' ;
                End if;
              End if;

              If mdof.SITUACAO = 'I' or mdof.SITUACAO = 'D' then
                mdof.CTRL_SITUACAO_DOF := mdof.SITUACAO;
              End if;

              if mdof.COD_MUN_COL is null then
                Begin
                  Select to_number(ibge_codigo) into mdof.COD_MUN_COL
                  From cor_municipio
                  Where nome = upper(substr(mdof.NOME_MUN_col,1,60))
                  and uf_codigo   = upper(substr(mdof.UF_col,1,2))
                  and pais_codigo = upper(substr(mdof.PAIS_col,1,6));
                Exception
                  When no_data_found Then
                       mdof.COD_MUN_COL := null;
                  When others Then
                       raise_application_error(-20001,'N�o foi poss�vel deduzir o IBGE_CODIGO do municipio de Coleta');
                End;
              end if;

              if mdof.COD_MUN_ENTG is null then
                Begin
                  Select to_number(ibge_codigo) into mdof.COD_MUN_ENTG
                  From cor_municipio
                  Where nome = upper(substr(mdof.NOME_MUN_ENTG,1,60))
                  and uf_codigo   = upper(substr(mdof.UF_ENTG,1,2))
                  and pais_codigo = upper(substr(mdof.PAIS_ENTG,1,6));
                Exception
                  When no_data_found Then
                       mdof.COD_MUN_ENTG := null;
                  When others Then
                       raise_application_error(-20001,'N�o foi poss�vel deduzir o IBGE_CODIGO do municipio de Entrega');
                End;
              end if;

              If mdof.IND_CANCEL = '*' Then
                if v_sap_especie_nf_class = 'P' Then
                  mdof.Tipo:= 'C';
                  mtipo_complemento_aux := 'P';
                end if;
                if v_sap_especie_nf_class = 'PI' Then
                  mdof.Tipo:= 'C';
                  mtipo_complemento_aux := 'PI';
                end if;
                if v_sap_especie_nf_class = 'I' Then
                  mdof.Tipo:= 'C';
                  mtipo_complemento_aux := 'I';
                end if;

                If substr(mdof.edof_codigo,1,2) = 'CT' then
                   If v_sap_especie_nf_class = 'P' or v_sap_especie_nf_class = 'I' or v_sap_especie_nf_class = 'PI' then
                     mdof.tipo_cte := 1;
                   Else
                     mdof.tipo_cte := 0;
                   End if;
                End if;

                mdof.ind_tit := sap_itf_xml_util.sap_get_par_associacao ( 'SAP_CODIGO_TIPO_TITULO', mdof.ind_tit );

                if mdof.mun_codigo_iss is null Then
                  Begin
                    Select substr(TAX_LOC,1,2) , substr(TAX_LOC,4,10),TAXTYP
                    into mUF, mIBGE, mtaxtyp
                    from SAP_ITF_IMPOSTOS_IDF_COR
                    Where yj1bnflin_id = mdof.id_idf
                    and TAX_LOC is not null
                    and rownum <= 1;
                  Exception when no_data_found then
                      null;
                  End;

                  IF mUF is not null or mIBGE is not null then
                    Begin
                      Select mun_codigo into v_mun_codigo_iss
                      from COR_MUNICIPIO
                      Where uf_codigo = mUF and ibge_codigo = mIBGE
                      and rownum <= 1;
                      mdof.mun_codigo_iss := v_mun_codigo_iss;
                    Exception when no_data_found then
                      raise_application_error(-20001,'N�o foi possivel deduzir o C�digo de Municipio de ISS para UF = ' || mUF || ' e IBGE = ' || mIBGE);
                    End;
                  End if;
                end if;

                If sap_itf_xml_util.sap_get_par_associacao ('SAP_TIPO_IMPOSTO_COR', mtaxtyp) = 'ISS_RETIDO' or  
				sap_itf_xml_util.sap_get_par_associacao ('SAP_TIPO_IMPOSTO_COR', mtaxtyp) = 'ISS'  then
                  mdof.MUN_PRES_SERVICO := v_mun_codigo_iss;
                End if;

              if mdof.MUN_CODIGO_ISS is null then
                Begin
                  Select MUN_CODIGO
                    into mdof.MUN_CODIGO_ISS
                    from cor_localidade_vigencia
                   where pfj_codigo = mdof.DESTINATARIO_PFJ_CODIGO
                     and rownum = 1
                     and dt_fim is null;
                Exception
                  When no_data_found Then
                    mdof.MUN_CODIGO_ISS := null;
                end;
              
                If mdof.MUN_CODIGO_ISS is not null Then
                  mdof.MUN_PRES_SERVICO := mdof.MUN_CODIGO_ISS;
                Else
                  RAISE_APPLICATION_ERROR(-20111,
                                          'Localidade da pessoa DESTINATARIA ' ||
                                          mdof.DESTINATARIO_PFJ_CODIGO ||
                                          ' nao localizada :' || sqlerrm);
                End if;
              End if;

                v_ind_eix := COR_CALC_EIX(mdof.remetente_pfj_codigo,mdof.remetente_loc_codigo, mdof.destinatario_pfj_codigo, mdof.destinatario_loc_codigo, mdof.dt_fato_gerador_imposto);

                If mdof.MUN_PRES_SERVICO is null and mdof.TXJCD is not null and v_ind_eix <> 'X' then
                  Begin
                    Select mun_codigo into v_mun_codigo_iss
                    from COR_MUNICIPIO
                    Where uf_codigo = substr(mdof.TXJCD,1,2) and ibge_codigo = substr(mdof.TXJCD,4,10)
                    and rownum <= 1;
                    mdof.MUN_PRES_SERVICO := v_mun_codigo_iss;
                  Exception 
                  when no_data_found then
                       Begin
                          Select mun_codigo
                            into v_mun_codigo_iss
                            from COR_MUNICIPIO
                           Where ibge_codigo = substr(mdof.TXJCD, 4, 10)
                             and rownum <= 1;
                          mdof.MUN_PRES_SERVICO := v_mun_codigo_iss;
                       Exception
                          when no_data_found then
                            raise_application_error(-20001,
                                            'N�o foi possivel deduzir o Municipio de Presta��o do servi�o para UF = ' ||
                                            substr(mdof.TXJCD, 1, 2) ||
                                            ' e IBGE = ' ||
                                            substr(mdof.TXJCD, 4, 10));
                       End;
                End;
                End if;

                Begin
                  Select CODIGO_RETENCAO
                  into mCODIGO_RETENCAO
                  from SAP_ITF_IMPOSTOS_IDF_COR
                  Where yj1bnflin_id = mdof.id_idf
                      and CODIGO_RETENCAO is not null
                      and sap_itf_xml_util.sap_get_par_associacao ('SAP_TIPO_IMPOSTO_COR', TAXTYP) = 'IRRF'
                      and rownum <= 1;
                  mdof.codigo_retencao := mCODIGO_RETENCAO;
                Exception when no_data_found then
                   mCODIGO_RETENCAO:= null;
                End;

                if mdof.ind_entrada_saida = 'E' and mdof.situacao = 'X' Then
                  v_nao_carregar := true;
                  Begin
                    UPDATE sap_itf_dof_cor
                    SET dh_critica = SYSDATE,
                    ctrl_critica = ctrl_critica + 1,
                    msg_critica = 'ERROR - NFE NAO DEVE SER IMPORTADA'
                    WHERE id = mID;
                  Exception When others then
                      null;
                  end;
                else
                  if (v_carimbador_dirf = 'N') Then
                    SRF_CARIMBADOR_DIRF_PKG.disable;
                  end if;

                  IF nvl(v_ult_dof_gravado,'*') <> mdof.DOF_IMPORT_NUMERO Then
                    v_qtd_iss_retido := 0;

                    SELECT COUNT(*) into v_qtd_iss_retido
                    FROM DUAL
                    WHERE 'ISS_RETIDO' IN
                     (SELECT DISTINCT
                          SAP_ITF_XML_UTIL.SAP_GET_PAR_ASSOCIACAO('SAP_TIPO_IMPOSTO_COR',TAXTYP) TAXTYP
                      FROM SAP_ITF_IMPOSTOS_IDF_COR
                      WHERE YJ1BNFLIN_ID IN
                        (SELECT ID
                         FROM SAP_ITF_IDF_COR
                         WHERE YJ1BNFDOC_ID = mdof.id));

                    if v_qtd_iss_retido > 0 then
                      mdof.ind_iss_retido_fonte := 'S';
                    else
                      mdof.ind_iss_retido_fonte := 'N';
                    end if;

                    mdof.mun_pres_servico := mdof.MUN_CODIGO_ISS;

                   acha_vl_abat_nt(mdof.ID_DOF, mdof.vl_abat_nt);

                  Begin
                    Select to_number(substr(mdof.CTE_STRT_LCT, 4, 10))
                      into mdof.MUN_COD_ORIGEM
                      from dual;
                  Exception
                    when others then
                      mdof.MUN_COD_ORIGEM := null;
                  end;
                
                  Begin
                    Select to_number(substr(mdof.CTE_END_LCT, 4, 10))
                      into mdof.MUN_COD_DESTINO
                      from dual;
                  Exception
                    When others then
                      mdof.MUN_COD_DESTINO := null;
                  end;
                                    
                  if to_char(mdof.pstdat,'MMYYYY') <> to_char(mdof.docdat,'MMYYYY') then
                     if mdof.ctrl_situacao_dof = 'S' then
                       mdof.ctrl_situacao_dof := 'X';
                     else
                       mdof.ctrl_situacao_dof := 'E';  
                     end if;                    
                  else
                     mdof.DT_REFER_EXT := null;                      
                  end if;
                  
                  if mdof.nfe_localizador is not null then
                     
                     mRegimeEspecialRS := 0;
                     
                     select instr(translate(mdof.nfe_localizador, 'XAaEeIiOoUuCc????-.,\/() |><"][}{=+*^$%#@!~`:;? ', 'XAaEeIiOoUuCcAaOo'),'87958674000181') 
                     into mRegimeEspecialRS
                     from dual;
                     
                     if mRegimeEspecialRS > 0 Then
                       mdof.ctrl_situacao_dof := 'B';
                     end if;
                     
                  end if;
				   
                   grv_COR_DOF;
                  END IF;
                end if;

                v_ult_dof_gravado  := mdof.DOF_IMPORT_NUMERO;

              End if;

            Exception when others then
              mcritica := 'ERROR';
              raise;
            End;
            Begin
              If mdof.IND_CANCEL = '*' Then
                mdof.DOF_ID := mDOF_ID ;
                mdof.DOF_SEQUENCE := mDOF_SEQUENCE ;
                mtipo_complemento:= mtipo_complemento_aux ;

                If mdof.nop_codigo_idf is null then
                  mdof.nop_codigo_idf := sap_itf_xml_util.sap_get_par_associacao ( 'SAP_DEDUZ_NOP_COR', mdof.CODIGO_ESPECIE || ' - ' || v_ind_eix);

                  if not mdof.nop_codigo_idf is null then
                     Begin
                       Select uf_codigo_destino, uf_codigo_origem, ind_eix
                         into muf_codigo_destino, muf_codigo_origem, mind_eix
                       from cor_dof
                       where dof_import_numero = mdof.dof_import_numero
                             and informante_Est_codigo = mdof.informante_Est_codigo;

                       if mdof.ind_entrada_saida =  'E' then
                          muf_documento := muf_codigo_destino;
                       else
                          muf_documento := muf_codigo_origem;
                       end if;

                       mdof.CFOP_CODIGO := cor_calc_cfop(mdof.nop_codigo_idf, mind_eix, muf_documento, mdof.dt_fato_gerador_imposto);

                     Exception when no_data_found then
                       mdof.CFOP_CODIGO := SAP_DEDUZ_CFOP_NOP (mdof.nop_codigo_idf,  null,'CFOP');
                     end;

                     mdof.CFOP_IDF := mdof.nop_codigo_idf;
                     mdof.SUBCLASSE_IDF := 'S';
                     mdof.SISS_CODIGO := mdof.matnr;
                  end if;
                 End if;
                 If mdof.ID_IDF is null Then
                   raise_application_error (-20001, 'O registro de DOF na ITF nao possui itens relacionados');
                 Elsif mdof.CODIGO_IDF is null  AND mdof.DESCRICAO_IDF is null Then
                   raise_application_error (-20001, 'Codigo e descricao do Item da NF estao nulos');
                 Elsif mdof.CFOP_IDF is null Then
                   raise_application_error (-20001, 'NOP codigo esta nula. Verificar na tabela SAP o CFOP dos itens da NF');
                 Elsif mdof.MERC_CODIGO is null
                       and mdof.SISS_CODIGO is null
                       and mdof.PRES_CODIGO is null
                       and ltrim(mdof.CODIGO_IDF, '0') is not null Then
                   raise_application_error (-20001, 'Codigo do material nao deduzido. Verificar no cadastro se existe a mercadoria, prestacao ou servico com o codigo '||mdof.CODIGO_IDF);
                 End if;

                 If mdof.SUBCLASSE_IDF <> 'M' Then
                   mdof.NBM_CODIGO := null;
                 End if;

                 Declare
                 mreturn1   CORAPI_IDF.data;
                 Begin
                   mreturn1 := sap_dof_util.get_idf_impostos ( mdof.ID_IDF , 'COR' );
                   mdof.ALIQ_ICMS              := NVL( MRETURN1.v.ALIQ_ICMS, 0 );
                   mdof.ALIQ_DIFA              := NVL( MRETURN1.v.ALIQ_DIFA, 0 );
                   mdof.ALIQ_IPI               := NVL( MRETURN1.v.ALIQ_IPI, 0 );
                   mdof.ALIQ_ISS               := NVL( MRETURN1.v.ALIQ_ISS, 0 );
                   mdof.ALIQ_STT               := NVL( MRETURN1.v.ALIQ_STT, 0 );
                   mdof.ALIQ_STF               := NVL( MRETURN1.v.ALIQ_STF, 0 );
                   mdof.PERC_IRRF              := NVL( MRETURN1.v.PERC_IRRF, 0 );
                   mdof.VL_BASE_ICMS           := NVL( MRETURN1.v.VL_BASE_ICMS, 0 );
                   mdof.VL_BASE_IPI            := NVL( MRETURN1.v.VL_BASE_IPI, 0 );
                   mdof.VL_BASE_IRRF           := NVL( MRETURN1.v.VL_BASE_IRRF, 0 );
                   mdof.VL_BASE_ISS            := NVL( MRETURN1.v.VL_BASE_ISS, 0 );
                   mdof.VL_BASE_STT            := NVL( MRETURN1.v.VL_BASE_STT, 0 );
                   mdof.VL_BASE_STF            := NVL( MRETURN1.v.VL_BASE_STF, 0 );
                   mdof.VL_TRIBUTAVEL_ICMS     := NVL( MRETURN1.v.VL_TRIBUTAVEL_ICMS, 0 );
                   mdof.VL_TRIBUTAVEL_IPI      := NVL( MRETURN1.v.VL_TRIBUTAVEL_IPI, 0 );
                   mdof.VL_TRIBUTAVEL_DIFA     := NVL( MRETURN1.v.VL_TRIBUTAVEL_DIFA, 0 );
                   mdof.VL_TRIBUTAVEL_STT      := NVL( MRETURN1.v.VL_TRIBUTAVEL_STT, 0 );
                   mdof.VL_TRIBUTAVEL_STF      := NVL( MRETURN1.v.VL_TRIBUTAVEL_STF, 0 );
                   mdof.VL_DIFA                := NVL( MRETURN1.v.VL_DIFA, 0 );
                   mdof.VL_ICMS                := NVL( MRETURN1.v.VL_ICMS, 0 );
                   mdof.VL_IPI                 := NVL( MRETURN1.v.VL_IPI, 0 );
                   mdof.VL_IRRF                := NVL( MRETURN1.v.VL_IRRF, 0 );
                   mdof.VL_ISS                 := NVL( MRETURN1.v.VL_ISS, 0 );
                   mdof.VL_STT                 := NVL( MRETURN1.v.VL_STT, 0 );
                   mdof.VL_STF                 := NVL( MRETURN1.v.VL_STF, 0 );
                   mdof.VL_ISENTO_ICMS         := NVL( MRETURN1.v.VL_ISENTO_ICMS, 0 );
                   mdof.VL_ISENTO_IPI          := NVL( MRETURN1.v.VL_ISENTO_IPI, 0 );
                   mdof.VL_OUTROS_ICMS         := NVL( MRETURN1.v.VL_OUTROS_ICMS, 0 );
                   mdof.VL_OUTROS_IPI          := NVL( MRETURN1.v.VL_OUTROS_IPI, 0 );
                   mdof.VL_ALIQ_PIS            := NVL( MRETURN1.v.VL_ALIQ_PIS, 0 );
                   mdof.VL_ALIQ_COFINS         := NVL( MRETURN1.v.VL_ALIQ_COFINS, 0 );
                   mdof.ALIQ_PIS_RET           := NVL( MRETURN1.v.ALIQ_PIS_RET, 0 );
                   mdof.ALIQ_COFINS_RET        := NVL( MRETURN1.v.ALIQ_COFINS_RET, 0 );
                   mdof.ALIQ_CSLL_RET          := NVL( MRETURN1.v.ALIQ_CSLL_RET, 0 );
                   mdof.VL_BASE_PIS            := NVL( MRETURN1.v.VL_BASE_PIS, 0 );
                   mdof.VL_BASE_COFINS         := NVL( MRETURN1.v.VL_BASE_COFINS, 0 );
                   mdof.VL_BASE_PIS_RET        := NVL( MRETURN1.v.VL_BASE_PIS_RET, 0 );
                   mdof.VL_BASE_COFINS_RET     := NVL( MRETURN1.v.VL_BASE_COFINS_RET, 0 );
                   mdof.VL_BASE_CSLL_RET       := NVL( MRETURN1.v.VL_BASE_CSLL_RET, 0 );
                   mdof.VL_IMPOSTO_PIS         := NVL( MRETURN1.v.VL_IMPOSTO_PIS, 0 );
                   mdof.VL_IMPOSTO_COFINS      := NVL( MRETURN1.v.VL_IMPOSTO_COFINS, 0 );
                   mdof.VL_II                  := NVL( MRETURN1.v.VL_II, 0 );
                   mdof.VL_BASE_II             := NVL( MRETURN1.v.VL_BASE_II, 0 );
                   mdof.ALIQ_II                := NVL( MRETURN1.v.ALIQ_II, 0 );
                   mdof.VL_PIS_RET              := NVL( MRETURN1.v.VL_PIS_RET, 0 );
                   mdof.VL_COFINS_RET           := NVL( MRETURN1.v.VL_COFINS_RET, 0 );
                   mdof.VL_CSLL_RET             := NVL( MRETURN1.v.VL_CSLL_RET  , 0 );
                   mdof.VL_BASE_SEST          := NVL( MRETURN1.v.VL_BASE_SEST  , 0 );
                   mdof.VL_BASE_SENAT         := NVL( MRETURN1.v.VL_BASE_SENAT  , 0 );
                   mdof.ALIQ_SEST             := NVL( MRETURN1.v.ALIQ_SEST  , 0 );
                   mdof.ALIQ_SENAT            := NVL( MRETURN1.v.ALIQ_SENAT  , 0 );
                   mdof.VL_SEST               := NVL( MRETURN1.v.VL_SEST  , 0 );
                   mdof.VL_SENAT              := NVL( MRETURN1.v.VL_SENAT  , 0 );
                   mdof.ind_iss_retido_fonte := NVL( MRETURN1.v.ind_iss_retido_fonte  , 'N' );
                   mdof.VL_BASE_PIS_ST := NVL( MRETURN1.v.VL_BASE_PIS_ST  , 0 );
                   mdof.ALIQ_PIS_ST := NVL( MRETURN1.v.ALIQ_PIS_ST  , 0 );
                   mdof.VL_PIS_ST := NVL( MRETURN1.v.VL_PIS_ST  , 0 );
                   mdof.VL_BASE_COFINS_ST := NVL( MRETURN1.v.VL_BASE_COFINS_ST  , 0 );
                   mdof.ALIQ_COFINS_ST := NVL( MRETURN1.v.ALIQ_COFINS_ST  , 0 );
                   mdof.VL_COFINS_ST := NVL( MRETURN1.v.VL_COFINS_ST  , 0 );
                   mdof.ALIQ_ICMS_FCPST      := NVL(MRETURN1.v.ALIQ_ICMS_FCPST,0);
                   mdof.VL_BASE_ICMS_FCPST   := NVL(MRETURN1.v.VL_BASE_ICMS_FCPST,0);
                   mdof.VL_ICMS_FCPST        := NVL(MRETURN1.v.VL_ICMS_FCPST,0);
				   
                   if mdof.VL_PIS_ST > 0 then
                       mdof.IND_INCIDENCIA_PIS_ST := 'S';
                   end if;
                   if mdof.VL_COFINS_ST > 0 then
                      mdof.IND_INCIDENCIA_COFINS_ST := 'S';
                   end if;
				   
                   IF mdof.codigo_especie = 'NI' THEN
                      IF NVL( MRETURN1.v.VL_BASE_ICMS,0 ) = 0 AND 
					     mdof.VL_ICMS <> 0 THEN
                         mdof.VL_BASE_ICMS := NVL( MRETURN1.v.VL_OUTROS_ICMS, 0 );
                      END IF;
                      IF NVL( MRETURN1.v.VL_TRIBUTAVEL_ICMS,0 ) = 0 AND 
					     mdof.VL_ICMS <> 0 THEN
                         mdof.VL_TRIBUTAVEL_ICMS := NVL( MRETURN1.v.VL_OUTROS_ICMS, 0  );
                      END IF;
                   END IF;
				   
                   mdof.VL_BASE_INSS := NVL( MRETURN1.v.VL_BASE_INSS, 0 );
                   mdof.ALIQ_INSS    := NVL( MRETURN1.v.ALIQ_INSS, 0 );
                   mdof.VL_INSS      := NVL( MRETURN1.v.VL_INSS, 0 );
                   mdof.ALIQ_II    := NVL( MRETURN1.v.ALIQ_II, 0 );
                   mdof.VL_II      := NVL( MRETURN1.v.VL_II, 0 );
                   mdof.VL_BASE_II      := NVL( MRETURN1.v.VL_BASE_II, 0 );
                   mdof.QTD_BASE_PIS      := NVL( MRETURN1.v.QTD_BASE_PIS, 0 );
                   mdof.QTD_BASE_COFINS      := NVL( MRETURN1.v.QTD_BASE_COFINS, 0 );
                   mdof.VL_BASE_INSS_RET    := NVL( MRETURN1.v.VL_BASE_INSS_RET, 0 );
                   mdof.VL_INSS_RET         := NVL( MRETURN1.v.VL_INSS_RET, 0 );
                   mdof.ALIQ_INSS_RET       := NVL( MRETURN1.v.ALIQ_INSS_RET, 0 );
                   mdof.VL_BASE_PIS_RET     := NVL( MRETURN1.v.VL_BASE_PIS_RET, 0 );
                   mdof.VL_PIS_RET          := NVL( MRETURN1.v.VL_PIS_RET, 0 );
                   mdof.ALIQ_PIS_RET         := NVL( MRETURN1.v.ALIQ_PIS_RET, 0 );
                   mdof.VL_BASE_COFINS_RET  := NVL( MRETURN1.v.VL_BASE_COFINS_RET, 0 );
                   mdof.VL_COFINS_RET       := NVL( MRETURN1.v.VL_COFINS_RET, 0 );
                   mdof.ALIQ_COFINS_RET     := NVL( MRETURN1.v.ALIQ_COFINS_RET, 0 );
                   mdof.VL_BASE_CSLL_RET    := NVL( MRETURN1.v.VL_BASE_CSLL_RET, 0 );
                   mdof.VL_CSLL_RET         := NVL( MRETURN1.v.VL_CSLL_RET, 0 );
                   mdof.ALIQ_CSLL_RET       := NVL( MRETURN1.v.ALIQ_CSLL_RET, 0 );
                   mdof.VL_BASE_ICMS_DESC_L        := NVL( MRETURN1.v.VL_BASE_ICMS_DESC_L, 0 );
                   mdof.VL_TRIBUTAVEL_ICMS_DESC_L  := NVL( MRETURN1.v.VL_TRIBUTAVEL_ICMS_DESC_L, 0 );
                   mdof.ALIQ_ICMS_DESC_L           := NVL( MRETURN1.v.ALIQ_ICMS_DESC_L, 0 );
                   mdof.VL_ICMS_DESC_L             := NVL( MRETURN1.v.VL_ICMS_DESC_L, 0 );
                   mdof.vl_base_stf_fronteira      := NVL( MRETURN1.v.vl_base_stf_fronteira, 0 );
                   mdof.vl_stf_fronteira           := NVL( MRETURN1.v.vl_stf_fronteira, 0 );
                   mdof.vl_base_stf_ido            := NVL( MRETURN1.v.vl_base_stf_ido, 0 );
                   mdof.vl_stf_ido                 := NVL( MRETURN1.v.vl_stf_ido, 0 );
                   mdof.tipo_stf                   := NVL( MRETURN1.v.tipo_stf, 'N' );
                   mdof.perc_icms_part_rem         := NVL( MRETURN1.v.perc_icms_part_rem, 0 );
                   mdof.vl_base_icms_part_rem      := NVL( MRETURN1.v.vl_base_icms_part_rem, 0 );
                   mdof.aliq_icms_part_rem         := NVL( MRETURN1.v.aliq_icms_part_rem, 0 );
                   mdof.vl_icms_part_rem           := NVL( MRETURN1.v.vl_icms_part_rem, 0 );
                   mdof.perc_icms_part_dest        := NVL( MRETURN1.v.perc_icms_part_dest, 0 );
                   mdof.vl_base_icms_part_dest     := NVL( MRETURN1.v.vl_base_icms_part_dest, 0 );
                   mdof.aliq_icms_part_dest        := NVL( MRETURN1.v.aliq_icms_part_dest, 0 );
                   mdof.vl_icms_part_dest          := NVL( MRETURN1.v.vl_icms_part_dest, 0 );
                   mdof.aliq_icms_fcp              := NVL( MRETURN1.v.aliq_icms_fcp, 0 );
                   mdof.vl_icms_fcp                := NVL( MRETURN1.v.vl_icms_fcp, 0 );
                   mdof.vl_base_icms_fcp           := NVL( MRETURN1.v.vl_base_icms_fcp, 0 );
                   mdof.VL_SERVICO_AE15            := NVL(MRETURN1.v.VL_SERVICO_AE15,0);
                   mdof.ALIQ_INSS_AE15_RET         := NVL(MRETURN1.v.ALIQ_INSS_AE15_RET,0);
                   mdof.VL_INSS_AE15_RET           := NVL(MRETURN1.v.VL_INSS_AE15_RET,0);
                   mdof.VL_SERVICO_AE20            := NVL(MRETURN1.v.VL_SERVICO_AE20,0);
                   mdof.ALIQ_INSS_AE20_RET         := NVL(MRETURN1.v.ALIQ_INSS_AE20_RET,0);
                   mdof.VL_INSS_AE20_RET           := NVL(MRETURN1.v.VL_INSS_AE20_RET,0);
                   mdof.VL_SERVICO_AE25            := NVL(MRETURN1.v.VL_SERVICO_AE25,0);
                   mdof.ALIQ_INSS_AE25_RET         := NVL(MRETURN1.v.ALIQ_INSS_AE25_RET,0);
                   mdof.VL_INSS_AE25_RET           := NVL(MRETURN1.v.VL_INSS_AE25_RET,0);                
                 End;

                 mdof.VL_FISCAL   :=   mdof.PRECO_TOTAL_ITEM + mdof.VL_AJUSTE_SOMA + 0/*mdof.VL_RATEIO_AJUSTE_PRECO*/  ;

                 mdof.VL_CONTABIL :=   mdof.PRECO_TOTAL_CONTABIL + mdof.VL_AJUSTE_SOMA
                                                                 + mdof.VL_IPI
                                                                 + NVL(mdof.VL_STT, 0)
                                                                 + NVL(mdof.VL_STF, 0)
                                                                 + NVL(mdof.VL_II, 0)
                                                                 ;
                 If mdof.AJUSTA_VL_CONTABIL = 'S' Then
                   mdof.VL_CONTABIL :=         mdof.VL_CONTABIL
                                               + mdof.VL_RATEIO_FRETE   +  mdof.VL_RATEIO_SEGURO
                                               + mdof.VL_RATEIO_ODA     ;
                 End if;

                 mdof.VL_FATURADO :=   mdof.VL_CONTABIL ;
                 IF NVL ( mdof.VL_STT, 0 ) > 0 then
                   mdof.VL_CONTABIL  :=  ( mdof.VL_CONTABIL - mdof.VL_STT );
                   mdof.VL_FATURADO  :=   mdof.VL_CONTABIL;
                 end if;

                 Begin
                   SELECT NOP_CODIGO
                   INTO v_nop
                   FROM COR_NATUREZA_DE_OPERACAO
                   WHERE NOP_CODIGO = mdof.nop_codigo_idf;
                 Exception when no_data_found then
                   raise_application_error(-20114, 'A NOP ' || mdof.nop_codigo_idf ||  ' n�o est� cadastrada!' );
                 End;

                 If mtipo_complemento_aux = 'I' OR mtipo_complemento_aux = 'P' OR mtipo_complemento_aux = 'PI' Then
                   mdof.subclasse_idf := 'O';
                 End if;

                 mdof.VL_ICMS_DESC_L := mdof.VL_ICMS_DESC_L + abs(mdof.vicmsdeson);

                 if mdof.charg is not null and v_SAP_UTILIZA_LOTE_MED = 'N' then
                     mdof.charg := null;
                 end if;

                 if mdof.ind_entrada_saida = 'E' and mdof.situacao = 'X' Then
                   v_nao_carregar := true;
                   Begin
                     UPDATE sap_itf_dof_cor
                     SET dh_critica = SYSDATE,
                     ctrl_critica = ctrl_critica + 1,
                     msg_critica = 'ERROR - NFE NAO DEVE SER IMPORTADA'
                     WHERE id = mID;
                    Exception When others then
                      null;
                    end;
                 else
                   if (v_carimbador_dirf = 'N') Then
                     SRF_CARIMBADOR_DIRF_PKG.disable;
                   end if;

                  begin
                      grv_COR_IDF;
                  Exception when others then
                       merror := SQLERRM;
                       merror_code := SQLCODE;
                       if ( mdof.SUBCLASSE_IDF = 'M') and sap_fis_util.v_split_valuation = 'S' Then
                          merror := sap_fis_util.grava_merc_split_valuation(merror,mdof.merc_codigo,mdof.matnr );
                          if merror is null then
                             grv_COR_IDF;
                          Else
                            raise;
                          End if;
                       Else
                         raise;
                       end if;
                  End;

                if (v_SAP_UTILIZA_LOTE_MED = 'S' and
                   mdof.lote_med is not null) Then
                  grv_COR_LOTE_MED;
                end if;
                
                IF mdof.DOF_IMPORT_CONS_CIVIL IS NOT NULL AND mdof.INFORMANTE_CONS_CIVIL IS NOT NULL AND mdof.IDF_NUM_CONS_CIVIL IS NOT NULL THEN
                  grv_COR_IDF_CONS_CIVIL;
                END IF;
              
                IF mdof.NUM_PROC IS NOT NULL AND mdof.SUSPENSAO_CODIGO IS NOT NULL Then
                  grv_COR_DOF_PROCESSO;
                End if;
              
                grv_COR_DOF_VOLUME_CARGA;
                grv_COR_DOF_ASSOCIADO;
              end if;

              End if;

            exception when others then
              raise;
            End;
          End if; /* final de Cancelamento de DOF*/

          mcontador := mcontador + 1;
          sap_dof_util.del_reg_dof ( mID, mdof.ID_IDF, mdof.XML_ID, 'COR','YSDMMPDOF',  mdof.DOF_IMPORT_NUMERO);
           mold_docnum := mdof.DOF_IMPORT_NUMERO;

           if mcontador > 500 Then
             commit;
             mcontador := 0;
           end if;

           RETURN TRUE;

          Else
             RETURN FALSE;
          End if; /* nao processa os itens restantes de DOF's criticados no primeiro item*/

      exception when others then
        t_dof_criticados(v_idx).CODIGO_DO_SITE	:= mdof.CODIGO_DO_SITE;
        t_dof_criticados(v_idx).DOF_SEQUENCE	  := mdof.DOF_SEQUENCE;
        v_idx                                   := v_idx + 1;
              
        mold_docnum := mdof.DOF_IMPORT_NUMERO;
      
        merror      := SQLERRM;
        merror_code := SQLCODE;
        merror      := syn_traduz_erro_db(NULL, NULL, merror, false);
      
        if syn_erro_dba(merror_code) = TRUE then
          merror := 'Erro interno do banco. Contate seu DBA:' ||
                    merror_code || ' - ' || merror;
          put_line(merror);
          commit;
          raise_application_error(-20001, merror);
        End if;
      
        srf_carimbador_dirf_pkg.enable;
      
        put_line('Registro Rejeitado No.: ' || mregs_rejeitados);
        put_line('  NUMERO = ' || mdof.NUMERO);
        put_line('  INFORMANTE_EST_CODIGO = ' ||
                 mdof.INFORMANTE_EST_CODIGO);
        put_line('  DOF_IMPORT_NUMERO = ' || mdof.DOF_IMPORT_NUMERO);
        put_line('  IDF_NUM = ' || mdof.IDF_NUM);
        put_line('  NOP_CODIGO = ' || mdof.NOP_CODIGO_IDF);
      
        put_line('Mensagem Oracle: ' || merror);
        put_line(null);
      
        mcontador := mcontador + 1;
      
        SAP_ITF_XML_UTIL.log_erro_itf('SAP_ITF_DOF_COR', mdof.ID, merror);
        SAP_ITF_XML_UTIL.log_erro_itf('SAP_ITF_IDF_COR',
                                      mdof.ID_IDF,
                                      merror);
      
        if (mcontador > 500) Then
          COMMIT;
          mcontador := 0;
        end if;
        RETURN FALSE;
    End;

      -- *******************************************************************************
      -- ***********************  INICIO DOS PROCEDIMENTOS  ****************************
      -- *******************************************************************************

      BEGIN

        loadParameters;           --Inicializa os parametro globais
        limpa_duplicado_itf;      --Limpa duplicidade de registros na ITF caso exista

        mprocesso := p_prc_id;
        mregs_importados := 0;
        mregs_rejeitados := 0;

        If v_deduz_mun_codigo_iss is null then
           raise_application_error ( -20222, 'Parametro "GLOBAL" : DEDUZ_MUNICIPIO_ISS nao preenchido.  ' );
        end if;

        IF v_deduz_mun_codigo_iss <> 'S' AND v_deduz_mun_codigo_iss <> 'N' then
           raise_application_error ( -20333, 'Valor do Parametro "GLOBAL" : DEDUZ_MUNICIPIO_ISS nao Aceito.  Valor = ' || v_deduz_mun_codigo_iss  );
        end if;

        open c_indicador_titulo ;
        loop
            fetch c_indicador_titulo into r_indicador_titulo;
            exit when c_indicador_titulo%notfound;
            IF r_indicador_titulo.ind_tit IS NOT NULL THEN
               sap_cor_cad_ind_titulo ( r_indicador_titulo.ind_tit, r_indicador_titulo.desc_titulo );
            END IF;

        end loop;
        close c_indicador_titulo;

            DECLARE
              v_iva_cfop VARCHAR2(100) := '';
              v_cfop     VARCHAR2(10)  := '';
            BEGIN
              FOR lst IN (SELECT i.id, d.docnum, i.cfop cfop_idf, i.mwskz
                            FROM sap_itf_idf_cor i, sap_itf_dof_cor d
                           WHERE i.yj1bnfdoc_id = d.id )
              LOOP
                v_cfop     := SUBSTR(lst.cfop_idf,1,INSTR(lst.cfop_idf,'/',1)-1);
                v_iva_cfop := sap_itf_xml_util.sap_get_par_associacao('HON_IVA_X_NOP_ATIVO_PARA_CIAP_COR', lst.mwskz||'@'||v_cfop);

                IF v_iva_cfop IS NOT NULL THEN
                  UPDATE sap_itf_idf_cor
                     SET cfop = v_cfop||'/'||v_iva_cfop
                   WHERE id = lst.id;
                END IF;
              END LOOP;
              COMMIT;
            END;
         Case to_char(p_critica)
      When '0' then
        v_critica_i := 0;
        v_critica_f := 0;
      When '1 ' then
        v_critica_i := 1;
        v_critica_f := 1;
      When '2 ' then
        v_critica_i := 1;
        v_critica_f := 2;
      When 'C ' then
        v_critica_i := 1;
        v_critica_f := 10000;
      When '%' then
        v_critica_i := 0;
        v_critica_f := 10000;
      Else
        v_critica_i := 0;
        v_critica_f := 10000;
    End case;

         OPEN cdof(v_critica_i, v_critica_f);
         LOOP
            FETCH cdof INTO mdof;
            EXIT WHEN cdof%NOTFOUND;
            IF NOT grava_registro THEN
               mregs_rejeitados := mregs_rejeitados + 1;
            Else
               mregs_importados := mregs_importados + 1;
            END IF;
         END LOOP;
         CLOSE cdof;

    forall i in t_dof_criticados.first .. t_dof_criticados.last
       delete cor_dof
       where CODIGO_DO_SITE = t_dof_criticados(i).CODIGO_DO_SITE
       AND DOF_SEQUENCE =  t_dof_criticados(i).DOF_SEQUENCE;
      
    commit;

         IF v_ult_dof_gravado IS NOT NULL THEN
           Declare
           merror varchar2(2000);
           merror_code integer;
           Begin
             Calcula_Totais( v_ult_dof_gravado);

             Exception when others then

               merror := SQLERRM;
               merror_code := SQLCODE;

               merror := syn_traduz_erro_db (NULL, NULL,merror,false); -- Traducao de erros Oracle

               if syn_erro_dba(merror_code) = TRUE then
                 merror := 'Erro interno do banco. Contate seu DBA:' || merror_code || ' - ' || merror;
                 commit;
                 raise_application_error(-20001,merror);
               End if;

               UPDATE sap_itf_dof_cor
               SET dh_critica = SYSDATE,
               ctrl_critica = ctrl_critica + 1,
               msg_critica = substr(replace(merror,'','"') ,1,1500)
               WHERE id = mID;
               COMMIT;
             End;
         END IF;
         pregs_importados := mregs_importados;
         pregs_rejeitados := mregs_rejeitados;

         if v_nao_carregar Then
           Begin
             DELETE FROM sap_itf_dof_cor
             WHERE msg_critica = 'ERROR - NFE NAO DEVE SER IMPORTADA';
             COMMIT;
           Exception When others then
             null;
           end;
         end if;
   END process_dof;
END;

/
